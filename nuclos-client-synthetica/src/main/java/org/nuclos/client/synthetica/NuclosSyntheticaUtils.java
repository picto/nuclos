//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.synthetica;

import java.net.URL;
import java.text.ParseException;

import javax.swing.*;

import org.apache.log4j.Logger;

public class NuclosSyntheticaUtils {

	private static final Logger LOG = Logger.getLogger(NuclosSyntheticaUtils.class);

	private static final byte[] CRYPT = new byte[] {
		(byte) 0xfa, (byte) 0x79, (byte) 0xa1, (byte) 0x2c, (byte) 0x81,
		(byte) 0x4b, (byte) 0x6f, (byte) 0x03, (byte) 0xae, (byte) 0xd8,
		(byte) 0x70, (byte) 0x04, (byte) 0xd7, (byte) 0xd2, (byte) 0x6a,
		(byte) 0x9c
	};
	
	public static void setLookAndFeel() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, ParseException {
		setLookAndFeel(null, null);
	}
	
	
	public static void registerNuclosTheme(String themeName, URL pathToXml) {
		LOG.info("trying to register theme " + themeName + " of " + pathToXml);
		if (NuclosSyntheticaThemeableLookAndFeel.registerNuclosTheme(themeName, pathToXml)) {
			LOG.info("Nuclos theme \"" + themeName + "\" found (" + pathToXml + ")");
		}
	}

    public static void setLookAndFeel(String nuclosTheme, String nuclosFontFamily) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException, ParseException {
		UIManager.put(
			dec("a900cf58e92e1b6acdb95e68beb10ff2891c8f45ef2d00"),
			new String[] {
				dec("b610c249ef380a6693961f72b6b003e8da30cf4aee390262dab11f6aa4a113ef8e1ccc49a10c0261e6"),
				dec("b610c249ef380a51cbbf1977a3a00be89316cf62f4260d66dce54134e5e258a9"),
				dec("aa0bce48f4281b3efda11e70bfb71ef59918"),
				dec("b610c249ef380a57d7a8153992bc1ef98809d345f22e4f50c7ac15249bbb09f9940ac4"),
				dec("bf01d145f32e2b62dabd4d29fafc47b1d4548c01ac"),
				dec("b718d97ae4391c6ac1b64d36f9eb53a5d4409815")
			});
		UIManager.put(
			dec("a900cf58e92e1b6acdb95e68beb10ff2891c8f47e432"),
			dec("be4b9668b10f563a83ea4736e6e52bdfcc54e31fc709563098e85d31e0e05fa8cb4a9501b37c5a309ae93234"));
		
		LOG.info("setLookAndFeel(" + nuclosTheme + ")");
		NuclosSyntheticaThemeableLookAndFeel laf = new NuclosSyntheticaThemeableLookAndFeel();
		laf.setNuclosTheme(nuclosTheme, nuclosFontFamily);
		if (nuclosTheme != null) {
			LOG.info("Chosen Nuclos Theme: " + nuclosTheme);
		}
	}

	private static String dec(String d) {
		return decrypt(fromHex(d), CRYPT);
	}
	
	private  static byte[] fromHex (String s) {
        String s2;
        byte[] b = new byte[s.length() / 2];
        int i;
        for (i = 0; i < s.length() / 2; i++) {
            s2 = s.substring(i * 2, i * 2 + 2);
            b[i] = (byte)(Integer.parseInt(s2, 16) & 0xff);
        }
        return b;
    }

	private static String decrypt(final byte[] input, final byte[] secret) {
	    if (secret.length == 0) {
	        throw new IllegalArgumentException("empty security key");
	    }
	    
	    try {
	    	final byte[] output = new byte[input.length];
	    	int spos = 0;
		    for (int pos = 0; pos < input.length; ++pos) {
		        output[pos] = (byte) (input[pos] ^ secret[spos]);
		        ++spos;
		        if (spos >= secret.length) {
		            spos = 0;
		        }
		    }
			return new String(output, "UTF-8");
		} catch (Exception e) {
			throw new RuntimeException("decyption failure", e);
		}
	}
	
}
