package org.nuclos.server.customcode.codegenerator;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class WsdlCodeGeneratorTest {

	@Test
	public void testDetectWsdlVersion() throws URISyntaxException {
		File wsdl1 = getFile("/wsdlExample_v1.xml");
		File wsdl2 = getFile("/wsdlExample_v2.0.xml");

		assert "1.1".equals(WsdlCodeGenerator.detectWsdlVersion(wsdl1));
		assert "2".equals(WsdlCodeGenerator.detectWsdlVersion(wsdl2));
	}
	
	private File getFile(String resourceName) throws URISyntaxException {
		URL resourceUrl = getClass().getResource(resourceName);
		Path resourcePath = Paths.get(resourceUrl.toURI());
		
		return resourcePath.toFile();
	}

}
