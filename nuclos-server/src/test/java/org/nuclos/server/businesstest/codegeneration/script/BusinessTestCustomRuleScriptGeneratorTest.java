package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestCustomRuleScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException, InterruptedException {
		assert "CUSTOMRULE B TestRule".equals(generator.getTestName());

		AbstractBusinessTestScriptGeneratorTest.ScriptResult script = parseScript();

		assert script.getGroovySource().contains(".executeTestRule()");

		assert script != null;
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new B()).newCustomRuleScriptGenerator("TestRule");
	}
}