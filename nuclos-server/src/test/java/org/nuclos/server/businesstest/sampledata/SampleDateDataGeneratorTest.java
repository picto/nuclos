package org.nuclos.server.businesstest.sampledata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SampleDateDataGeneratorTest {

	@Test
	public void testDataGeneration() {
		SampleDateDataGenerator generator = new SampleDateDataGenerator();

		assert generator.newValue() == null;

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.DATE, 1);

		cal = DateUtils.truncate(cal, Calendar.DATE);

		Date date1 = cal.getTime();

		generator.addSampleValues(Arrays.asList(date1));

		assert generator.newValue().equals(date1);
		assert generator.newValue().equals(date1);
		assert generator.newValue().equals(date1);
		assert generator.newValue().equals(date1);
		assert generator.newValue().equals(date1);


		cal.set(Calendar.YEAR, 2017);

		Date date2 = cal.getTime();

		generator.addSampleValues(Arrays.asList(date2));

		List<Date> dates = new ArrayList(Arrays.asList(date1, date2));
		while (!dates.isEmpty()) {
			dates.remove(generator.newValue());
		}
	}
}