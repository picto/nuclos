package org.nuclos.server.businesstest.codegeneration.script;

import java.io.IOException;

import org.junit.Test;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestInsertScriptGeneratorTest extends AbstractBusinessTestScriptGeneratorTest {

	@Test
	public void testGenerator() throws CommonFinderException, IOException, CommonPermissionException, InterruptedException {
		assert "INSERT A".equals(generator.getTestName());

		ScriptResult script = parseScript();

		final String groovySource = script.getGroovySource();

		assert groovySource.contains(".name = 'Sample Data'");

		// Mandatory fields
		assert groovySource.contains(".integerField = 42");
		assert groovySource.contains(".doubleField = 42.0");
		assert groovySource.contains(".bigDecimalField = 0");
		assert groovySource.contains(".dateField = new Date().parse('dd.MM.yyyy', '01.01.2016')");
		assert groovySource.contains(".refFieldId = B.first()?.id");

		assert groovySource.contains(".save()");
	}

	@Override
	protected AbstractBusinessTestScriptGenerator newGenerator() {
		return getFactory(new A()).newInsertScriptGenerator();
	}
}