package org.nuclos.server.businesstest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import org.junit.Test;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassLoader;
import org.nuclos.server.businesstest.codegeneration.source.AbstractGroovyClassSource;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;

import groovy.lang.GroovyClassLoader;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestClassGeneratorTest extends BusinessTestTestBase {
	private Map<UID, BusinessTestEntitySource> entitySources = generateClasses();

	public BusinessTestClassGeneratorTest() throws CommonFinderException, CommonPermissionException {
	}

	/**
	 * Runs the business test class generator with a simple example class and checks if the result is compilable.
	 *
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 * @throws IOException
	 */
	@Test
	public void testClassGeneration() throws CommonFinderException, CommonPermissionException, IOException {
		assert !entitySources.isEmpty();
		assert entitySources.size() == getEntities().size();

		BusinessTestEntitySource source = entitySources.values().iterator().next();

		assert containsMethod(source, "get");
		assert containsMethod(source, "save");
		assert containsMethod(source, "first");
		assert containsMethod(source, "list");
		assert containsMethod(source, "query");
		assert containsMethod(source, "toString");

		for (AbstractGroovyClassSource.Method method : source.getMethods()) {
			assert !method.getName().equals("setChangedAt");
			assert !method.getName().equals("setDeleted");
		}

		File outputDir = Files.createTempDirectory("BusinessTestClassGeneratorTest").toFile();
		BusinessTestClassLoader loader = new BusinessTestClassLoader(outputDir);
		GroovyClassLoader groovyClassLoader = loader.compileSources(entitySources);

		assert groovyClassLoader.getLoadedClasses().length > 0;
		assert groovyClassLoader.getLoadedClasses().length == entitySources.size();
	}

	@Test
	public void testProcessMethods() {
		BusinessTestEntitySource sourceA = findSource("A");

		assert containsMethod(sourceA, "unsetProcess");
		assert containsMethod(sourceA, "changeProcessAktion1");
		assert containsMethod(sourceA, "changeProcessAktion2");
		assert !containsMethod(sourceA, "changeProcessAktion3");

		BusinessTestEntitySource sourceB = findSource("B");
		assert !containsMethod(sourceB, "unsetProcess");
	}

	@Test
	public void testReferenceLong() {
		BusinessTestEntitySource sourceA = findSource("A");


		AbstractGroovyClassSource.Method getter = findMethod(sourceA, "getRefField");
		AbstractGroovyClassSource.Method idGetter = findMethod(sourceA, "getRefFieldId");
		AbstractGroovyClassSource.Method idSetter = findMethod(sourceA, "setRefFieldId");

		assert getter.getReturnType().equals("String");

		assert idGetter.getReturnType().equals("Long");

		assert idSetter.getParams().size() == 1;
		assert idSetter.getParams().iterator().next().getType().equals("Long");
	}

	@Test
	public void testReferenceUID() {
		BusinessTestEntitySource sourceA = findSource("A");

		AbstractGroovyClassSource.Method getter = findMethod(sourceA, "getRefUser");
		AbstractGroovyClassSource.Method idGetter = findMethod(sourceA, "getRefUserId");
		AbstractGroovyClassSource.Method idSetter = findMethod(sourceA, "setRefUserId");

		assert getter.getReturnType().equals("String");

		assert idGetter.getReturnType().equals("UID");

		assert idSetter.getParams().size() == 1;
		assert idSetter.getParams().iterator().next().getType().equals("UID");
	}

	@Test
	public void testDocumentAttachmentAttribute() {
		BusinessTestEntitySource sourceB = findSource("B");

		AbstractGroovyClassSource.Method getter = findMethod(sourceB, "getDocumentAttachment");
		AbstractGroovyClassSource.Method setter = findMethod(sourceB, "setDocumentAttachment");
		AbstractGroovyClassSource.Method idGetter = findMethod(sourceB, "getDocumentAttachmentId");
		AbstractGroovyClassSource.Method idSetter = findMethod(sourceB, "setgetDocumentAttachmentId");

		assert setter.getTypeParameter().equals("<T extends org.nuclos.api.common.NuclosFileBase>");
		assert setter.getParams().size() == 1;
		assert setter.getParams().get(0).getType().equals("T");
		assert getter.getReturnType().equals("NuclosFile");

		assert idGetter == null;
		assert idSetter == null;
	}

	public BusinessTestEntitySource findSource(String name) {
		for (BusinessTestEntitySource source : entitySources.values()) {
			if (source.getClassName().equals(name)) {
				return source;
			}
		}

		return null;
	}

	public AbstractGroovyClassSource.Method findMethod(BusinessTestEntitySource source, String methodName) {
		for (AbstractGroovyClassSource.Method method : source.getMethods()) {
			if (method.getName().equals(methodName)) {
				return method;
			}
		}

		return null;
	}

	public boolean containsMethod(BusinessTestEntitySource source, String methodName) {
		return findMethod(source, methodName) != null;
	}
}
