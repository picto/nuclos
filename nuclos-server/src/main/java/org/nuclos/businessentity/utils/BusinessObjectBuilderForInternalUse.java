//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.businessentity.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;

/**
 * Use this class for a Nuclos development enviroment only, and if you need updates of the SystemBO classes registered in 'getEntityMetas'.
 * The environment variable 'DEV_SYSTEMBOSRC_OUTPUTDIR' should point to the source code e.g.
 * '/Volumes/nuclos_1/nuclos/nuclos-server/src/main/java/'
 */
public class BusinessObjectBuilderForInternalUse {

	private static Boolean enabled = null;
	
	private static File outputDir = null;

	/*
		Set this environmant variable in your IDE to something like this
		'/Volumes/nuclos_1/nuclos/nuclos-server/src/main/java/'
	 */
	public static final String DEV_SYSTEMBOSRC_OUTPUTDIR = "DEV_SYSTEMBOSRC_OUTPUTDIR";

	public static Collection<EntityMeta<?>> getEntityMetas() {
		Collection<EntityMeta<?>> result = new ArrayList<>();
		if (isEnabled()) {
			result.add(E.ENTITY);
			result.add(E.ENTITYFIELD);
			result.add(E.NUCLET);
			result.add(E.NUCLET_INTEGRATION_POINT);
			result.add(E.NUCLET_INTEGRATION_FIELD);
			result.add(E.NUCLET_INTEGRATION_PROBLEM);
		}
		return result;
	}
	
	public static boolean isEnabled() {
		getOutputDir();
		return Boolean.TRUE.equals(enabled);
	}
	
	public static File getOutputDir() {
		if (enabled == null) {
			final String dir = System.getenv().get("DEV_SYSTEMBOSRC_OUTPUTDIR");
			if (dir != null) {
				File result = new File(dir);
				if (result.isDirectory() && result.exists()) {
					outputDir = result;
					enabled = true;
					return result;
				}
			}
			enabled = false;
		}
		return outputDir;
	}

}
