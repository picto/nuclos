//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.


package org.nuclos.businessentity;


import org.nuclos.businessentity.*; 
import org.nuclos.api.businessobject.attribute.Attribute; 
import org.nuclos.api.businessobject.attribute.PrimaryKeyAttribute; 
import org.nuclos.api.businessobject.attribute.NumericAttribute; 
import org.nuclos.api.businessobject.attribute.StringAttribute; 
import org.nuclos.api.businessobject.Dependent; 
import org.nuclos.api.businessobject.Flag; 
import org.nuclos.api.businessobject.attribute.ForeignKeyAttribute; 
import org.nuclos.api.UID; 
import org.nuclos.server.nbo.AbstractBusinessObject; 
import org.nuclos.api.businessobject.facade.Modifiable; 
import java.util.Date; 
import java.util.List; 
import java.util.ArrayList; 
import java.util.HashSet; 

/**
 * BusinessObject: nuclos_nuclet
 *<br>
 *<br>Nuclet: org.nuclos.businessentity
 *<br>DB-Name: T_AD_APPLICATION
 *<br>Writable: true
 *<br>Localized: false
 *<br>Statemodel: false
**/
public class Nuclet extends AbstractBusinessObject<org.nuclos.common.UID> implements Modifiable<org.nuclos.common.UID> {
private static final long serialVersionUID = 1L;



/**
 * Attribute: primaryKey
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRUID
 *<br>Data type: org.nuclos.common.UID
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 128
 *<br>Precision: null
**/
public static final PrimaryKeyAttribute<org.nuclos.common.UID> Id = 
	new PrimaryKeyAttribute<org.nuclos.common.UID>("Id", "org.nuclos.businessentity", "xojr", "xojr0", org.nuclos.common.UID.class );


/**
 * Attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Source = 
	new Attribute<java.lang.Boolean>("Source", "org.nuclos.businessentity", "xojr", "xojrh", java.lang.Boolean.class );


/**
 * Attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final Attribute<java.lang.Boolean> Nuclon = 
	new Attribute<java.lang.Boolean>("Nuclon", "org.nuclos.businessentity", "xojr", "xojrg", java.lang.Boolean.class );


/**
 * Attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Localidentifier = new StringAttribute<java.lang.String>("Localidentifier", "org.nuclos.businessentity", "xojr", "xojrf", java.lang.String.class );


/**
 * Attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public static final NumericAttribute<java.lang.Integer> NucletVersion = 
	new NumericAttribute<java.lang.Integer>("NucletVersion", "org.nuclos.businessentity", "xojr", "xojre", java.lang.Integer.class );


/**
 * Attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> ChangedBy = new StringAttribute<java.lang.String>("ChangedBy", "org.nuclos.businessentity", "xojr", "xojr4", java.lang.String.class );


/**
 * Attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Package = new StringAttribute<java.lang.String>("Package", "org.nuclos.businessentity", "xojr", "xojrd", java.lang.String.class );


/**
 * Attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> ChangedAt = new NumericAttribute<java.util.Date>("ChangedAt", "org.nuclos.businessentity", "xojr", "xojr3", java.util.Date.class );


/**
 * Attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> CreatedBy = new StringAttribute<java.lang.String>("CreatedBy", "org.nuclos.businessentity", "xojr", "xojr2", java.lang.String.class );


/**
 * Attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Description = new StringAttribute<java.lang.String>("Description", "org.nuclos.businessentity", "xojr", "xojrb", java.lang.String.class );


/**
 * Attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public static final NumericAttribute<java.util.Date> CreatedAt = new NumericAttribute<java.util.Date>("CreatedAt", "org.nuclos.businessentity", "xojr", "xojr1", java.util.Date.class );


/**
 * Attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public static final StringAttribute<java.lang.String> Name = new StringAttribute<java.lang.String>("Name", "org.nuclos.businessentity", "xojr", "xojra", java.lang.String.class );

public static final Dependent<org.nuclos.businessentity.Entity> _Entity = 
	new Dependent<org.nuclos.businessentity.Entity>("_Entity", "null", "nuclos_entity", "5E8q", "nuclet", "5E8qc", org.nuclos.businessentity.Entity.class );

public static final Dependent<org.nuclos.businessentity.NucletIntegrationPoint> _NucletIntegrationPoint1 = 
	new Dependent<org.nuclos.businessentity.NucletIntegrationPoint>("_NucletIntegrationPoint1", "null", "nuclos_nucletIntegrationPoint", "kIL5", "nuclet", "kIL5a", org.nuclos.businessentity.NucletIntegrationPoint.class );

public static final Dependent<org.nuclos.businessentity.NucletIntegrationPoint> _NucletIntegrationPoint2 = 
	new Dependent<org.nuclos.businessentity.NucletIntegrationPoint>("_NucletIntegrationPoint2", "null", "nuclos_nucletIntegrationPoint", "kIL5", "targetNuclet", "kIL5d", org.nuclos.businessentity.NucletIntegrationPoint.class );


public Nuclet() {
		super("xojr");
}


/**
 * Getter-Method for attribute: entity
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRENTITY
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public org.nuclos.common.UID getEntityUid() {
		return new org.nuclos.common.UID("xojr");
}


/**
 * Getter-Method for attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getSource() {
		return getField("xojrh", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: source
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNSOURCE
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setSource(java.lang.Boolean pSource) {
		setField("xojrh", pSource); 
}


/**
 * Getter-Method for attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.lang.Boolean getNuclon() {
		return getField("xojrg", java.lang.Boolean.class); 
}


/**
 * Setter-Method for attribute: nuclon
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: BLNNUCLON
 *<br>Data type: java.lang.Boolean
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void setNuclon(java.lang.Boolean pNuclon) {
		setField("xojrg", pNuclon); 
}


/**
 * Getter-Method for attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
public java.lang.String getLocalidentifier() {
		return getField("xojrf", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: localidentifier
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRLOCALIDENTIFIER
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4
 *<br>Precision: null
**/
public void setLocalidentifier(java.lang.String pLocalidentifier) {
		setField("xojrf", pLocalidentifier); 
}


/**
 * Getter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public java.lang.Integer getNucletVersion() {
		return getField("xojre", java.lang.Integer.class); 
}


/**
 * Setter-Method for attribute: nucletVersion
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: INTNUCLETVERSION
 *<br>Data type: java.lang.Integer
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 9
 *<br>Precision: null
**/
public void setNucletVersion(java.lang.Integer pNucletVersion) {
		setField("xojre", pNucletVersion); 
}


/**
 * Getter-Method for attribute: changedBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCHANGED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getChangedBy() {
		return getField("xojr4", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getPackage() {
		return getField("xojrd", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: package
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRPACKAGE
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setPackage(java.lang.String pPackage) {
		setField("xojrd", pPackage); 
}


/**
 * Getter-Method for attribute: changedAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCHANGED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getChangedAt() {
		return getField("xojr3", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: createdBy
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: STRCREATED
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getCreatedBy() {
		return getField("xojr2", java.lang.String.class); 
}


/**
 * Getter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public java.lang.String getDescription() {
		return getField("xojrb", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: description
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DESCRIPTION
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 4000
 *<br>Precision: null
**/
public void setDescription(java.lang.String pDescription) {
		setField("xojrb", pDescription); 
}


/**
 * Getter-Method for attribute: createdAt
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: DATCREATED
 *<br>Data type: org.nuclos.common2.InternalTimestamp
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public java.util.Date getCreatedAt() {
		return getField("xojr1", java.util.Date.class); 
}


/**
 * Getter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public java.lang.String getName() {
		return getField("xojra", java.lang.String.class); 
}


/**
 * Setter-Method for attribute: name
 *<br>
 *<br>Entity: nuclos_nuclet
 *<br>DB-Name: NAME
 *<br>Data type: java.lang.String
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: 255
 *<br>Precision: null
**/
public void setName(java.lang.String pName) {
		setField("xojra", pName); 
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.Entity> getEntity(Flag... flags) {
		return getDependents(_Entity, flags); 
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertEntity(org.nuclos.businessentity.Entity pEntity) {
		insertDependent(_Entity, pEntity);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_entity
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteEntity(org.nuclos.businessentity.Entity pEntity) {
		deleteDependent(_Entity, pEntity);
}


/**
 * Getter-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationPoint> getNucletIntegrationPoint1(Flag... flags) {
		return getDependents(_NucletIntegrationPoint1, flags); 
}


/**
 * Insert-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationPoint1(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		insertDependent(_NucletIntegrationPoint1, pNucletIntegrationPoint);
}


/**
 * Delete-Method for attribute: nuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_T_MD_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationPoint1(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		deleteDependent(_NucletIntegrationPoint1, pNucletIntegrationPoint);
}


/**
 * Getter-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public List<org.nuclos.businessentity.NucletIntegrationPoint> getNucletIntegrationPoint2(Flag... flags) {
		return getDependents(_NucletIntegrationPoint2, flags); 
}


/**
 * Insert-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void insertNucletIntegrationPoint2(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		insertDependent(_NucletIntegrationPoint2, pNucletIntegrationPoint);
}


/**
 * Delete-Method for attribute: targetNuclet
 *<br>
 *<br>Entity: nuclos_nucletIntegrationPoint
 *<br>DB-Name: STRUID_TARGET_NUCLET
 *<br>Data type: org.nuclos.common.UID
 *<br>Reference entity: nuclos_nuclet
 *<br>Reference field: package
 *<br>Localized: false
 *<br>Output format: null
 *<br>Scale: null
 *<br>Precision: null
**/
public void deleteNucletIntegrationPoint2(org.nuclos.businessentity.NucletIntegrationPoint pNucletIntegrationPoint) {
		deleteDependent(_NucletIntegrationPoint2, pNucletIntegrationPoint);
}
 }
