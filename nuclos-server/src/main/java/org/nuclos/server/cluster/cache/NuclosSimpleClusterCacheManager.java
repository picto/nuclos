//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.cache;

import org.nuclos.common.spring.GuavaCache;
import org.nuclos.server.cluster.NuclosClusterRegisterHelper;
import org.springframework.cache.Cache;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;

/*
 * CLUSTERING
 * Class decorates the spring configured caches to 
 * say hello to the other cluster node's
 */
public class NuclosSimpleClusterCacheManager extends SimpleCacheManager {

	public NuclosSimpleClusterCacheManager() {
		super();
	}

	@Override
	protected Cache decorateCache(Cache cache) {
		if(!NuclosClusterRegisterHelper.runInClusterMode()) {
			return cache;
		}
		
		if(cache instanceof ConcurrentMapCache) {
			ConcurrentMapCache c = (ConcurrentMapCache)cache;
			NuclosConcurrentMapCache decoratedCache = new NuclosConcurrentMapCache(c.getName(), c.getNativeCache(), c.isAllowNullValues());			
			return decoratedCache;			
		}
		else if(cache instanceof GuavaCache) {
			GuavaCache<?, ?> c = (GuavaCache<?, ?>)cache;
			GuavaNuclosClusterCache<?, ?> decoratedCache = new GuavaNuclosClusterCache(c.getName(), (com.google.common.cache.Cache) c.getNativeCache());
			return decoratedCache;
		}
		
		return cache;
	}	
	
}
