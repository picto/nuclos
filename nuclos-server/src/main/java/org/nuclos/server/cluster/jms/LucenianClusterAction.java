//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.IDalVO;
import org.nuclos.common.lucene.ILucenian;

public class LucenianClusterAction implements NuclosClusterAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7703361400322638921L;
	int mode;
	ILucenian<?> lucenian;
	Object actionObject;
	
	public LucenianClusterAction(ILucenian<?> lucenian, Object object, int mode) {
		this.lucenian = lucenian;
		this.actionObject = object;
		this.mode = mode;
	}

	@Override
	public void doAction() {
		lucenian.notifyClusterCloud(false);
		
		switch (mode) {
		case ILucenian.DEL:
			lucenian.del((Delete) actionObject);
			break;
		case ILucenian.INDEX:
			lucenian.index((IDalVO) actionObject);
			break;
		case ILucenian.STORE:
			lucenian.store();
			break;
		default:
			break;
		}		
	}

	@Override
	public boolean doOnMaster() {
		return false;
	}

}
