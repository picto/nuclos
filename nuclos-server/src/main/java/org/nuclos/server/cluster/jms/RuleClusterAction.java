//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.cluster.jms;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeBean;


public class RuleClusterAction implements NuclosClusterAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5762109420104676800L;

	@Override
	public void doAction() {
		
		EventSupportFacadeBean bean = (EventSupportFacadeBean)SpringApplicationContextHolder.getBean("eventSupportService");
		try {
			bean.forceEventSupportCompilation();
		} catch (CommonBusinessException e) {
			throw new NuclosFatalException(e);
		}
		 
	}

	@Override
	public boolean doOnMaster() {
		return false;
	}

}
