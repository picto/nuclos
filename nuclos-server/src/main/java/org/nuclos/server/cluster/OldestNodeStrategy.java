package org.nuclos.server.cluster;

import java.util.Comparator;
import java.util.List;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.server.cluster.cache.ClusterNodeCache;

/*
 * CLUSTERING
 * the oldest node raise to master when master shutdown or dies
 */
public class OldestNodeStrategy implements NewMasterNodeStrategy {

	@Override
	public ClusterNode getNewMasterNode() {
		
		ClusterNodeCache nodeCache = SpringApplicationContextHolder.getBean(ClusterNodeCache.class);
		
		List<ClusterNode> lstSorted = CollectionUtils.sorted(nodeCache.getNodes(), new Comparator<ClusterNode>() {

			@Override
			public int compare(ClusterNode node1, ClusterNode node2) {				
				return node1.getDatRegistered().compareTo(node2.getDatRegistered()) * (-1);
			}			
			
		});
		
		if(lstSorted.size() > 0) {
			return lstSorted.get(0);
		}
		
		throw new NuclosClusterExecption("No nodes are registered!");
	}

}
