//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport.ejb3;

import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.objectimport.ImportResult;
import org.nuclos.api.objectimport.ImportStructureDefinition;

public interface CsvImportFacadeLocal extends ImportFacadeLocal {

	/**
	 * Main method to start a file import without context. this method is used for executing imports via rule management
	 */
	ImportResult doImport(NuclosFile file, boolean isAtomic, Class<? extends ImportStructureDefinition>... structureDefClasses) throws BusinessException;
}
