//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.fileimport;

import java.util.Map;

import javax.xml.stream.Location;

class XmlParseObject {
	
	private static class MyLocation implements Location {
		
		private final int lineNumber;
		
		private final int columnNumber;
		
		private final int characterOffset;
		
		private final String publicId;
		
		private final String systemId;
		
		MyLocation(Location toClone) {
			this.lineNumber = toClone.getLineNumber();
			this.columnNumber = toClone.getColumnNumber();
			this.characterOffset = toClone.getCharacterOffset();
			this.publicId = toClone.getPublicId();
			this.systemId = toClone.getSystemId();
		}

		@Override
		public int getLineNumber() {
			return lineNumber;
		}

		@Override
		public int getColumnNumber() {
			return columnNumber;
		}

		@Override
		public int getCharacterOffset() {
			return characterOffset;
		}

		@Override
		public String getPublicId() {
			return publicId;
		}

		@Override
		public String getSystemId() {
			return systemId;
		}
		
	}
	
	private final Map<String,String> entityMap;
	
	private final Location location;
	
	/**
	 * @param entityMap - relative XPath -> value
	 * @param location
	 */
	XmlParseObject(Map<String,String> entityMap, Location location) {
		if (entityMap == null || location == null) {
			throw new NullPointerException();
		}
		this.entityMap = entityMap;
		this.location = new MyLocation(location);
	}

	/**
	 * @return relative XPath -> value
	 */
	Map<String, String> getEntityMap() {
		return entityMap;
	}

	Location getLocation() {
		return location;
	}

}
