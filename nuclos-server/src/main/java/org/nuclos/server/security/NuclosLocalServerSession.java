//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.security;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.ejb3.SecurityFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.intercept.RunAsUserToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * Utilitiy class for setting up a new local server session context, for instance in job execution or server startup.
 *
 * TODO: set users locale or default locale to <code>LocaleContextHolder</code>
 *
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Component
public class NuclosLocalServerSession {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosLocalServerSession.class);

	private static NuclosLocalServerSession INSTANCE;
	
	//

	private Long sessionId;
	
	private SecurityFacadeLocal securityFacadeLocal;
	
	private NuclosAuthenticationProvider nuclosAuthenticationProvider;
	
	private UserDetailsService userDetailsService;
	
	/**
	 * Spring injected.
	 */
	private NuclosUserDetailsContextHolder userContext;
	
	NuclosLocalServerSession() {
		INSTANCE = this;
	}
	
	public static NuclosLocalServerSession getInstance() {
		return INSTANCE;
	}
	
	@Autowired
	void setSecurityFacadeLocal(SecurityFacadeLocal securityFacadeLocal) {
		this.securityFacadeLocal = securityFacadeLocal;
	}
	
	@Autowired
	void setNuclosAuthenticationProvider(NuclosAuthenticationProvider nuclosAuthenticationProvider) {
		this.nuclosAuthenticationProvider = nuclosAuthenticationProvider;
	}
	
	@Autowired
	void setNuclosUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}
	
	@Autowired
	public void setNuclosUserDetailsContextHolder(NuclosUserDetailsContextHolder userContext) {
		this.userContext = userContext;
	}

	public void login(String username, String password) {
		Authentication auth = new NuclosLocalServerAuthenticationToken(username, password);
		SecurityContextHolder.getContext().setAuthentication(nuclosAuthenticationProvider.authenticate(auth));
		sessionId = securityFacadeLocal.login();
	}
	
	
	public void loginAsUser(String username) {
		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		loginAs(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
	}

	public void setUserContextThreadLocal() {
		userContext.setSavePoint(UUID.randomUUID().toString());
		userContext.setSavePointTime(new InternalTimestamp(System.currentTimeMillis()));
	}

	public void clearUserContextThreadLocal() {
		userContext.clear();
	}

	public String getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && auth.isAuthenticated()) {
			return auth.getName();
		}
		return null;
	}

	private void loginAs(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		Authentication auth = new NuclosLocalServerAuthenticationToken(username, password, authorities);
		SecurityContextHolder.getContext().setAuthentication(auth);
		sessionId = securityFacadeLocal.login();
	}

	public void logout() {
		try {
			securityFacadeLocal.logout(sessionId);
			SecurityContextHolder.getContext().setAuthentication(null);
		} catch (BeanCreationException e) {
			final String s = e.toString();
			if (s.indexOf("scopedTarget.") >= 0) {
				LOG.debug("Logout failed: {}", s);
			} else {
				LOG.error("Logout failed: {}", e);
			}
		} catch (Exception e) {
			LOG.error("Logout failed: {}", e);
		}
	}

	private List<GrantedAuthority> getSuperUserAuthorities() {
		return CollectionUtils.transform(NuclosUserDetailsService.getSuperUserActions(), new Transformer<String, GrantedAuthority>() {
			@Override
			public GrantedAuthority transform(String i) {
				return new SimpleGrantedAuthority(i);
			}
		});
	}
}
