package org.nuclos.server.user;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class UserRoleObjectCompiler extends AbstractNuclosObjectCompiler {

	public UserRoleObjectCompiler() {
		super(SourceType.USERROLE,
		      NuclosCodegeneratorConstants.USERROLEJARFILE,
		      NuclosCodegeneratorConstants.USERROLE_SRC_DIR_NAME);
	}

}
