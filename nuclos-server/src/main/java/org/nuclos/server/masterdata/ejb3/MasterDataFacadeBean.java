//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.masterdata.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.rule.CustomRule;
import org.nuclos.api.rule.DeleteFinalRule;
import org.nuclos.api.rule.DeleteRule;
import org.nuclos.api.rule.InsertFinalRule;
import org.nuclos.api.rule.InsertRule;
import org.nuclos.api.rule.UpdateFinalRule;
import org.nuclos.api.rule.UpdateRule;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIdListCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.ReferencingCollectableSearchCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.EntityObjectToEntityTreeViewVO;
import org.nuclos.common.collection.MasterDataToEntityObjectTransformer;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.DependentDataMap.DependentKey;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common.metadata.NotifyObject;
import org.nuclos.common.report.valueobject.ReportVO.ReportType;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.TruncatableCollectionDecorator;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.exception.WriteProxyEmptyIdException;
import org.nuclos.common2.layoutml.LayoutMLParser;
import org.nuclos.common2.layoutml.exception.LayoutMLException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.autonumber.AutoNumberHelper;
import org.nuclos.server.autosync.XMLEntities;
import org.nuclos.server.common.EventSupportCache;
import org.nuclos.server.common.LocalCachesUtil;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.ejb3.EntityObjectFacadeLocal;
import org.nuclos.server.common.ejb3.LocaleFacadeLocal;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.communication.CommunicationInterfaceLocal;
import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.nuclos.server.customcode.codegenerator.WsdlCodeGenerator;
import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.RecompileCheckerRegistry;
import org.nuclos.server.customcode.codegenerator.recompile.txsync.CompileGeneratedCodeTransactionSynchronization;
import org.nuclos.server.dal.processor.jdbc.impl.PreferenceProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbBusinessException;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.util.DbObjectUtils;
import org.nuclos.server.dblayer.util.DbObjectUtils.LocalIdentifierStore;
import org.nuclos.server.dbtransfer.TransferFacadeLocal;
import org.nuclos.server.eventsupport.ejb3.EventSupportCustomResult;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.nuclos.server.maintenance.MaintenanceFacadeLocal;
import org.nuclos.server.masterdata.MasterDataProxyList;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.parameter.NuclosParameterProvider;
import org.nuclos.server.printservice.PrintServiceRepository;
import org.nuclos.server.report.ejb3.ReportFacadeBean;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.nuclos.server.statemodel.valueobject.StateModelUsagesCache;
import org.nuclos.server.validation.ValidationSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Facade bean for all master data management functions. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
public class MasterDataFacadeBean extends NuclosFacadeBean implements MasterDataFacadeRemote, BeanFactoryAware {

	private static final Logger LOG = LoggerFactory.getLogger(MasterDataFacadeBean.class);

	private boolean bServerValidatesMasterDataValues;
	
	// Spring injection
	
	/**
	 * No @Autowired, must be lazy.
	 */
	private EventSupportFacadeLocal _eventSupportFacade;
	
	/**
	 * No @Autowired, must be lazy.
	 */
	private LayoutFacadeLocal _layoutFacade;
	
	/**
	 * No @Autowired, must be lazy.
	 */
	private LocaleFacadeLocal _localeFacadeLocal;
	
	// BeanFactoryAware
	protected BeanFactory beanFactory;
		
	@Autowired
	private ValidationSupport validationSupport;
	
	@Autowired
	private AutoNumberHelper autoNumberHelper;
	
	@Autowired
	private NuclosJavaCompilerComponent compiler;
	
	@Autowired
	private MasterDataFacadeHelper masterDataFacadeHelper;

	@Autowired
	private ServerParameterProvider serverParameterProvider;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private MandatorUtils mandatorUtils;

	@Autowired
	private RecompileCheckerRegistry recompileCheckerRegistry;

	@Autowired
	private NuclosJarGeneratorManager nuclosJarGeneratorManager;

	@Autowired
	private MaintenanceFacadeLocal maintenanceFacade;

	private ThreadLocal<CompileGeneratedCodeTransactionSynchronization> transactionSyncs =
		new ThreadLocal<>();


    // end of Spring injection
	
	public MasterDataFacadeBean() {
	}
	
	@PostConstruct
	@RolesAllowed("Login")
	public void postConstruct() {
		this.bServerValidatesMasterDataValues = "1".equals(serverParameterProvider.getValue(
			ParameterProvider.KEY_SERVER_VALIDATES_MASTERDATAVALUES));
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
	
	protected EventSupportFacadeLocal getEventSupportFacade() {
		if (_eventSupportFacade == null) {
			_eventSupportFacade = beanFactory.getBean(EventSupportFacadeLocal.class);
		}
		return _eventSupportFacade;
	}
	
	protected LayoutFacadeLocal getLayoutFacade() {
		if (_layoutFacade == null) {
			_layoutFacade = beanFactory.getBean(LayoutFacadeLocal.class);
		}
		return _layoutFacade;
	}
	
	protected LocaleFacadeLocal getLocaleFacade() {
		if (_localeFacadeLocal == null) {
			_localeFacadeLocal = beanFactory.getBean(LocaleFacadeLocal.class);
		}
		return _localeFacadeLocal;
	}

	/**
	 * @return Is the server supposed to validate master data values before
	 *         storing them?
	 */
	private boolean getServerValidatesMasterDataValues() {
		return this.bServerValidatesMasterDataValues;
	}
	
	private void appendMandator(CollectableSearchExpression cse, EntityMeta<?> entity) {
		cse.setSearchCondition(mandatorUtils.append(cse, entity));
	}

	/**
	 * �todo restrict permissions by entity name
	 * 
	 * @param entity
	 * @param clctexpr
	 * @return a proxy list containing the search result for the given search
	 *         expression.
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> ProxyList<PK,MasterDataVO<PK>> getMasterDataProxyList(UID entity, Collection<UID> fields, CollectableSearchExpression clctexpr) {
		final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
		appendMandator(clctexpr, eMeta);
		final ProxyListProvider plProvider = new ProxyListProvider(serverParameterProvider, metaProvider);
		return new MasterDataProxyList<PK>(entity, fields, clctexpr, plProvider);
	}

    @RolesAllowed("Login")
    @Override
	public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(EntityMeta<PK> entity, 
		CollectableSearchCondition cond, boolean bAll) {
    	return getMasterData(entity.getUID(), cond, bAll);
    }
    
	@RolesAllowed("Login")
	@Cacheable(value = "userData", key = "#p0")
	public MasterDataVO<UID> getUserData(String sUserName) {
		final CollectableComparison cond = SearchConditionUtils.newComparison(E.USER.name, ComparisonOperator.EQUAL, sUserName);
		final TruncatableCollection<MasterDataVO<UID>> collmdvo = getMasterData(E.USER, cond, false);
		return CollectionUtils.getUnique(collmdvo);
	}

	/**
	 * method to get master data records for a given entity and search condition
	 * 
	 * �postcondition result != null
	 * �todo restrict permissions by entity name
	 *
	 * @param entityUid name of the entity to get master data records for
	 * @param cond search condition
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data
	 *         value objects
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(UID entityUid,
			CollectableSearchCondition cond, boolean bAll) {
		return getMasterData(entityUid, new CollectableSearchExpression(cond), bAll);
	}

	public <PK> Collection<MasterDataVO<PK>> getMasterData(UID entityUid, boolean bAll) {
		return getMasterData(entityUid, new CollectableSearchExpression(null), bAll);
	}

	/**
	 *
	 * @param entityUid
	 * @param search
	 * @param bAll
	 * @return
	 */
	@RolesAllowed("Login")
	public <PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(UID entityUid, 
		CollectableSearchExpression search, boolean bAll) {
		if (E.REPORT.checkEntityUID(entityUid) || E.REPORTEXECUTION.checkEntityUID(entityUid)) {
			bAll = true;
		}

		final TruncatableCollection<MasterDataVO<PK>> result;
		
		if (E.ENTITYFIELDGROUP.checkEntityUID(entityUid)) {
			if (search != null && search.getSearchCondition() != null) {
				throw new CommonFatalException("Conditions for entity " + entityUid + " are not supported.");
			}
			final Collection<MasterDataVO<PK>> colResult = new ArrayList<MasterDataVO<PK>>();
			for (EntityObjectVO<UID> eo : nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELDGROUP).getBySearchExpression(search)) {
				colResult.add((MasterDataVO<PK>) DalSupportForMD.wrapEntityObjectVO(eo));
			}
			result = new TruncatableCollectionDecorator<MasterDataVO<PK>>(colResult, false, colResult.size());
		} else {
			final TruncatableCollection<MasterDataVO<PK>> truncoll = masterDataFacadeHelper.getGenericMasterData(entityUid, search, bAll);
			// permissions on reports and forms are given explicitly on a record per
			// record basis
			if (E.REPORT.checkEntityUID(entityUid) || E.REPORTEXECUTION.checkEntityUID(entityUid)) {
				result = filterReports(truncoll,
						SecurityCache.getInstance().getReadableReports(getCurrentUserName(), getCurrentMandatorUID()));
			} else {
				result = truncoll;
			}
		}

		assert result != null;
		return result;
	}

	/**
	 * gets the ids of all masterdata objects that match a given search
	 * expression (ordered, when necessary)
	 *
	 * @param cse condition that the masterdata objects to be found must satisfy
	 * @return List&lt;Integer&gt; list of masterdata ids
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> List<PK> getMasterDataIds(UID entity, CollectableSearchExpression cse) {
		IEntityObjectProcessor<PK> eoProcessor = (IEntityObjectProcessor<PK>) 
				nucletDalProvider.getEntityObjectProcessor(entity);
		appendMandator(cse, metaProvider.getEntity(entity));
		List<PK> masterDataIds = eoProcessor.getIdsBySearchExpression(cse);
		/*
		List<Object> masterDataIds = CollectionUtils.transform(eoIds, new Transformer<Long, Object>() {
			@Override public Object transform(Long l) { return l.intValue(); }
		});
		 */

		boolean bAdditionalSorting = false;
		if (cse != null && cse.isIncludingSystemData()) {
			Collection<PK> systemObjects = (Collection<PK>) XMLEntities.getSystemObjectIds(entity, cse.getSearchCondition());
			masterDataIds.addAll(systemObjects);
			bAdditionalSorting = !systemObjects.isEmpty();
		}

		if (E.isNuclosEntity(entity) && cse.getSortingOrder() != null && !cse.getSortingOrder().isEmpty() && bAdditionalSorting) {
			final UID fieldForSorting = cse.getSortingOrder().get(0).getField();
			this.sortUidList((List<UID>) masterDataIds, entity, fieldForSorting, cse.getSortingOrder().get(0).isAscending());
		}

		return masterDataIds;
	}

	/**
	 * WORKAROUND for XML entities and sorting of mixed lists with DB records
	 * Better: Send fields for sorting to DA-Layer. He has already read the DB records!
	 * @param list
	 * @param sEntityName
	 * @param sEntityFieldForSorting
	 * @param bAsc
	 */
	private void sortUidList(List<UID> list, final UID sEntityName, final UID sEntityFieldForSorting, final boolean bAsc) {
		final IEntityObjectProcessor<UID> proc = nucletDalProvider.<UID>getEntityObjectProcessor(sEntityName);
		final Collection<MasterDataVO<UID>> systemObjects = XMLEntities.getSystemObjects(sEntityName, null);
		final Collection<UID> systemObjectIds = getIds(systemObjects);
		Collections.sort(list, new Comparator<UID>() {

			@Override
			public int compare(UID o1, UID o2) {
				final boolean o1_isSystem = systemObjectIds.contains(o1);
				final boolean o2_isSystem = systemObjectIds.contains(o2);

				final Object o1_value;
				final Object o2_value;

				if (o1_isSystem) {
					o1_value = getMDVOFromList(systemObjects, o1).getFieldValue(sEntityFieldForSorting);
				} else {
					EntityObjectVO<?> eo = null;
					try {
						eo = proc.getByPrimaryKey(o1);
					} catch (Exception e) {
						// ignore
					}
					o1_value = (eo == null) ? null : eo.getFieldValue(sEntityFieldForSorting);
				}

				if (o2_isSystem) {
					o2_value = getMDVOFromList(systemObjects, o2).getFieldValue(sEntityFieldForSorting);
				} else {
					EntityObjectVO<?> eo = null;
					try {
						eo = proc.getByPrimaryKey(o2);
					} catch (Exception e) {
						// ignore
					}
					o2_value = (eo == null) ? null : eo.getFieldValue(sEntityFieldForSorting);
				}

				if (o1_value != null && o2_value != null && o1_value instanceof String && o2_value instanceof String) {
					return ((String)o1_value).compareToIgnoreCase((String) o2_value) * (bAsc ? 1 : -1);
				} else {
					return LangUtils.compare(o1_value, o2_value) * (bAsc ? 1 : -1);
				}
			}});
	}

	private <PK> Collection<PK> getIds(Collection<MasterDataVO<PK>> list) {
		Collection<PK> result = new ArrayList<PK>();
		if (list != null) {
			for (MasterDataVO<PK> mdvo : list) {
				result.add(mdvo.getPrimaryKey());
			}
		}

		return result;
	}

	private <PK> MasterDataVO<PK> getMDVOFromList(Collection<MasterDataVO<PK>> list, Object id) {
		if (list != null) {
			for (MasterDataVO<PK> mdvo : list) {
				if (mdvo.getPrimaryKey().equals(id)) {
					return mdvo;
				}
			}
		}

		return null;
	}

	/**
	 * gets the ids of all masterdata objects
	 *
	 * @return List&lt;PK&gt; list of masterdata ids
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> List<PK> getMasterDataIds(UID entityUid) {
		final EntityMeta mdmetacvo = metaProvider.getEntity(entityUid);

		// String dbEntity = mdmetacvo.getDBEntity();

		if (!dataBaseHelper.isObjectAvailable(mdmetacvo)) {
			throw new CommonFatalException(
				StringUtils.getParameterizedExceptionMessage(
					"masterdata.error.missing.table", mdmetacvo.getEntityName(), E.ENTITY.getEntityName()));
			// "Die Basistabelle/-view '"+dbEntity+"' der Entit\u00e4t '"+mdmetacvo.getEntityName()+"' existiert nicht!");
		}

		final IEntityObjectProcessor<PK> eoProcessor = nucletDalProvider.getEntityObjectProcessor(entityUid);
		final List<PK> masterDataIds = eoProcessor.getAllIds();
		/*
		List<Object> masterDataIds = CollectionUtils.transform(eoIds, new Transformer<Long, Object>() {
			@Override public Object transform(Long l) { return l.intValue(); }
		});
		 */
		masterDataIds.addAll(XMLEntities.<PK>getSystemObjectIds(entityUid, null));

		return masterDataIds;
	}

	@RolesAllowed("Login")
	@Override
	public <PK> List<MasterDataVO<PK>> getMasterDataChunk(UID sEntityName, final CollectableSearchExpression clctexpr, ResultParams resultParams) {
		List<MasterDataVO<PK>> lmdvo = masterDataFacadeHelper.getMasterDataChunk(sEntityName, clctexpr, resultParams);
		List<MasterDataVO<PK>> mdwd = new ArrayList<MasterDataVO<PK>>();
		List<EntityObjectVO<PK>> eos = new ArrayList<>();
		for (MasterDataVO<PK> mdvo : lmdvo) {
			eos.add(mdvo.getEntityObject());
			mdwd.add(mdvo);
		}
		fillDependentsForSubformColumns(eos, resultParams.getFields(), sEntityName, null);
		return mdwd;
	}
	
    @RolesAllowed("Login")
    @Override
	public Long countMasterDataRows(UID sEntity, final CollectableSearchExpression clctexpr) {
    	return masterDataFacadeHelper.countMasterDataRows(sEntity, clctexpr);
	}
    
	/**
	 * Convenience function to get all reports or forms used in
	 * AllReportsCollectableFieldsProvider.
	 *
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data
	 *         value objects
	 * @throws CommonFinderException if a row was deleted in the time between
	 *            executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
    @RolesAllowed("Login")
    @Override
	public TruncatableCollection<MasterDataVO<UID>> getAllReports() throws CommonFinderException, CommonPermissionException {
		this.checkReadAllowed(E.ROLE);
		return masterDataFacadeHelper.getGenericMasterData(E.REPORT.getUID(), CollectableSearchExpression.TRUE_SEARCH_EXPR, true);
	}
    
	/**
	 * convinience function to get all generations used in
	 * AllGenerationsCollectableFieldsProvider.
	 *
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data
	 *         value objects
	 * @throws CommonFinderException if a row was deleted in the time between
	 *            executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
    @RolesAllowed("Login")
    @Override
	public TruncatableCollection<MasterDataVO<UID>> getAllGenerations() throws CommonFinderException, CommonPermissionException {
		this.checkReadAllowed(E.ROLE);
		return masterDataFacadeHelper.getGenericMasterData(E.GENERATION.getUID(), CollectableSearchExpression.TRUE_SEARCH_EXPR, true);
	}
    
	/**
	 * convinience function to get all recordgrants used in
	 * AllRecordgrantsCollectableFieldsProvider.
	 *
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data
	 *         value objects
	 * @throws CommonFinderException if a row was deleted in the time between
	 *            executing the search and fetching the single rows.
	 * @throws CommonPermissionException
	 */
    @RolesAllowed("Login")
    @Override
	public TruncatableCollection<MasterDataVO<UID>> getAllRecordgrants() throws CommonFinderException, CommonPermissionException {
		this.checkReadAllowed(E.ROLE);
		return masterDataFacadeHelper.getGenericMasterData(E.RECORDGRANT.getUID(), CollectableSearchExpression.TRUE_SEARCH_EXPR, true);
	}
    
    /**
	 * execute a list of rules for the given Object
	 * 
	 * �todo restrict permission - check module id!
	 *
	 * @param lstRuleVO
	 * @param mdvo
	 * @param isCollectiveProcessing
     * @return 
	 * @throws CommonBusinessException
	 */
    @RolesAllowed("ExecuteRulesManually")
	public <PK> EntityObjectVO<PK> executeBusinessRules(List<EventSupportSourceVO> lstRuleVO,
		MasterDataVO<PK> mdvo, String customUsage, boolean isCollectiveProcessing)
		throws CommonBusinessException {
		
    	boolean bSaveAfterRuleExecution = false;
    	
    	EntityObjectVO<PK> eoVO = mdvo.getEntityObject();
    	
    	for (EventSupportSourceVO eseVO : lstRuleVO) {
    		EventSupportCustomResult<PK> result = getEventSupportFacade().fireCustomEventSupport(eoVO, eseVO, false);
    		eoVO = result.getEntityObjectVO();
    		if (!bSaveAfterRuleExecution) {
    			bSaveAfterRuleExecution = result.getUpdateAfterExecution();
    		}
    	}
    	
		if(bSaveAfterRuleExecution) {
			if (eoVO.isFlagNew()) {
				this.create(new MasterDataVO(eoVO), customUsage);
			} else {
				this.modifyVO(new MasterDataVO(eoVO), customUsage, isCollectiveProcessing);
			}
		}
		
		return eoVO;
	}
    
	/**
	 * filter MasterDataVO records from collmdvoReports where the id is not in
	 * collIds
	 * <p>
	 * ATTENTION: Generic tweak to use-case. They should be <UID>. (tp)
	 * </p>
	 * �postcondition result != null
	 * �postcondition !result.isTruncated()
	 *
	 * @param collmdvoReports
	 * @param mpReports
	 * @return filtered Collection&lt;MasterDataVO&gt;
	 */
	private <PK> TruncatableCollection<MasterDataVO<PK>> filterReports(
		Collection<MasterDataVO<PK>> collmdvoReports,
		final Map<ReportType, Collection<UID>> mpReports) {
		final Collection<MasterDataVO<PK>> collmdvoResult = CollectionUtils.select(
			collmdvoReports, new Predicate<MasterDataVO<PK>>() {
				@Override
                public boolean evaluate(MasterDataVO<PK> mdvo) {
					for (ReportType rt : mpReports.keySet()) {
						if (mpReports.get(rt).contains(mdvo.getPrimaryKey())) {
							return true;
						}
					}
					return false;
				}
			});
		final TruncatableCollection<MasterDataVO<PK>> result = new TruncatableCollectionDecorator<MasterDataVO<PK>>(
			collmdvoResult, false, collmdvoResult.size());
		assert result != null;
		assert !result.isTruncated();
		return result;
	}
	
	@RolesAllowed("Login")
    public IDependentDataMap getDependents(UID foreignKeyField, Object oRelatedId, List<EntityAndField> lstChildSubform) {
    	final IDependentDataMap result = new DependentDataMap();
    	final FieldMeta<?> fieldMeta = metaProvider.getEntityField(foreignKeyField);
    	
    	Collection<EntityObjectVO<Object>> colSubformData = CollectionUtils.transform(getDependantMasterData(fieldMeta.getEntity(), foreignKeyField, oRelatedId), 
                new MasterDataToEntityObjectTransformer());
    	
    	result.addAllData(fieldMeta, colSubformData);
    	
    	List<Object> lstParentIds = new ArrayList<Object>();
    	for(EntityObjectVO voSub : colSubformData) {
    		lstParentIds.add(voSub.getId());
    	}
    	 	
    	for(EntityAndField eafn : lstChildSubform) {
    		UID subParEntity = eafn.getEntity();
    		//Performance Tweak. If Subform Parent is not dynamic we can bundle the sub-subdata and fetch it once.
    		if (!metaProvider.getEntity(subParEntity).isDynamic()) {
    			Collection<EntityObjectVO<Object>> colChildSubformData = CollectionUtils.transform(getDependantMasterData(subParEntity, eafn.getField(), lstParentIds), 
	                    new MasterDataToEntityObjectTransformer());    			
	    		result.addAllData(eafn.getDependentKey(), colChildSubformData);
    		} else {
    	    	for(Object lSubParentId : lstParentIds) {
    	    		Collection<EntityObjectVO<Object>> colChildSubformData = CollectionUtils.transform(getDependantMasterData(subParEntity, eafn.getField(), lSubParentId), 
    	                    new MasterDataToEntityObjectTransformer());
    	    		result.addAllData(eafn.getDependentKey(), colChildSubformData);
    	    	}    			
    		}
    	}		 
		return result;
    }

    /**
	 * gets the dependant master data records for the given entity, using the
	 * given foreign key field and the given id as foreign key.
	 * 
	 * �precondition oRelatedId != null
	 * �todo restrict permissions by entity name
	 *
	 * @param entity UID of the entity to get all dependant master data
	 *           records for
	 * @param sForeignKeyField UID of the field relating to the foreign entity
	 * @param oRelatedId id by which sEntityName and sParentEntity are related
	 * @param mpParams optional parameters (for charts)
	 * @param limit set limit for number of dependents
	 */
    @RolesAllowed("Login")
    @Override
	public <PK, F> Collection<EntityObjectVO<PK>> getDependentDataCollection(UID entity, UID sForeignKeyField, Map<String, Object> mpParams, Integer limit, F... oRelatedId) {
    	Collection<EntityObjectVO<PK>> collEo = masterDataFacadeHelper.getDependantMasterDataWithLimit(entity,
    			sForeignKeyField, this.getCurrentUserName(), mpParams, limit, oRelatedId);
    	
		return collEo;
	}
    
    /**
     * Only used with by local facade without optional parameters (for charts)
     * @param sEntityName
     * @param sForeignKeyField
     * @param oRelatedId
     * @return
     */
	public <PK, F> Collection<EntityObjectVO<PK>> getDependantMasterData(UID sEntityName, UID sForeignKeyField, F oRelatedId) {
		return getDependentDataCollection(sEntityName, sForeignKeyField, null, null, oRelatedId);
	}
	
	/**
	 * getDependantMd4FieldMeta - gets the dependant master data records for the given fieldmeta, using the
	 * the given id as foreign key.
	 * Only used within server (Local-Facade)
	 * @param fkField
	 * @param oRelatedId
	 * @return
	 */
	public <PK, F> Collection<EntityObjectVO<PK>> getDependantMd4FieldMeta(FieldMeta<?> fkField, F oRelatedId) {
		return getDependentDataCollection(fkField.getEntity(), fkField.getUID(), null, null, oRelatedId);
	}

	@RolesAllowed("Login")
	@Override
	public <PK> Collection<EntityTreeViewVO> getDependantSubnodes(
		UID sEntityName, UID sForeignKeyField, Object oRelatedId) {
		Collection<EntityTreeViewVO> result = CollectionUtils.transform(masterDataFacadeHelper.getDependantMasterData(sEntityName,
			sForeignKeyField, this.getCurrentUserName(), null, oRelatedId), new EntityObjectToEntityTreeViewVO());
		return result;
	}
	

	@RolesAllowed("Login")
	public
	<PK> MasterDataVO<PK> get(EntityMeta<PK> entity, PK pk)
			throws CommonFinderException, CommonPermissionException {
		return get(entity.getUID(), pk);
	}

	/**
	 * method to get a master data value object for given primary key id
	 *
	 * @param entityUid name of the entity to get record for
	 * @param oId primary key id of master data record
	 * @return master data value object
	 * @throws CommonPermissionException
	 * @throws CommonPermissionException
	 */
    @RolesAllowed("Login")
    @Override
    public <PK> MasterDataVO<PK> get(UID entityUid, PK oId)
    		throws CommonFinderException, CommonPermissionException {
    	return get(entityUid, oId, false);
    }
    
    @RolesAllowed("Login")
    public <PK> MasterDataVO<PK> getThin(UID entityUid, PK oId)
    		throws CommonFinderException, CommonPermissionException {
    	return get(entityUid, oId, true);
    }
    
	private <PK> MasterDataVO<PK> get(UID entityUid, PK oId, boolean thin)
		throws CommonFinderException, CommonPermissionException {
    	
		// @todo This doesn't work for entities with composite primary keys
    	// Note: MasterDataLayoutCache is using this Method from the Client, thus allow :///
    	if (!E.LAYOUT.getUID().equals(entityUid)) {
    		checkReadAllowed(entityUid);    		
    	}
		
		grantUtils.checkInternal(entityUid, oId);

		if ("attributegroup".equals(entityUid.toString()) || E.ENTITYFIELDGROUP.getEntityName().equals(entityUid.toString())) {
			/**
			 * @TODO auch NuclosDalProvider? z.B. fuer Grunddaten...
			 */
			final EntityObjectVO<PK> eo = nucletDalProvider.<PK>getEntityObjectProcessor(entityUid).getByPrimaryKey(oId);
			return DalSupportForMD.wrapEntityObjectVO(eo);
		} else {
			final EntityMeta<?> em = metaProvider.getEntity(entityUid);
			final MasterDataVO<PK> result; 
			if (thin) {
				result = masterDataFacadeHelper.getMasterDataCVOThinById(em, oId, false);
			} else {
				result = masterDataFacadeHelper.getMasterDataCVOById(em, oId, false);
			}
			mandatorUtils.checkReadAllowed(result.getEntityObject(), em);
			return result;
		}
	}

	/**
	 * @param sEntityName
	 * @param oId
	 * @return the version of the given masterdata id.
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
    @RolesAllowed("Login")
    @Override
	public Integer getVersion(UID sEntityName, Object oId)
		throws CommonFinderException, CommonPermissionException {
		return this.get(sEntityName, oId).getVersion();
	}

	/**
	 * create a new master data record
	 *
	 * �precondition sEntityName != null
	 * �precondition mdvo.getId() == null
	 * �precondition (mpDependants != null) --&gt;
	 *               mpDependants.areAllDependantsNew()
	 * �nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record to be created
	 * @return master data value object containing the newly created record
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> MasterDataVO<PK> create(MasterDataVO<PK> mdvo, String customUsage) throws CommonCreateException,
			CommonPermissionException, NuclosBusinessRuleException {
    	EntityObjectVO<PK> eo = mdvo.getEntityObject();
    	MasterDataVO<PK> result=createVO(mdvo, customUsage);
        UID entityUid = mdvo.getEntityObject().getDalEntity();
	    triggerRecompileOnInsertOrDeleteIfRequired(mdvo);

    	return result;
    }
    
    /*
     * This is for server-intern use only.
     */
	private <PK> MasterDataVO<PK> createVO(MasterDataVO<PK> mdvo,
		String customUsage) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException {
		try {
			final UID entityUid = mdvo.getEntityObject().getDalEntity();
			checkWriteAllowed(entityUid);
			
			final EntityMeta<?> eMeta = metaProvider.getEntity(entityUid);

			if (eMeta.isMandator() && SecurityCache.getInstance().isMandatorPresent()) {
				getEventSupportFacade().fireInitMandator(mdvo.getEntityObject());
				mandatorUtils.checkWriteAllowed(mdvo.getEntityObject(), eMeta);
			}
			
			final boolean useRuleEngineSave = getEventSupportFacade().getUsesEventSupportForEntity(entityUid, InsertRule.class.getCanonicalName());
			
			if (useRuleEngineSave) {
				mdvo = fireSaveEvent(InsertRule.class.getCanonicalName(), mdvo, customUsage, false);
			}

			if (E.RELATIONTYPE.checkEntityUID(entityUid)) {
				final LocaleFacadeLocal localeFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
				LocaleInfo localeInfo = localeFacade.getUserLocale();
				String sText = mdvo.getFieldValue(E.RELATIONTYPE.name.getUID(), String.class);
				String sResourceId = localeFacade.setResourceForLocale(null, localeInfo, sText);
				mdvo.setFieldValue(E.RELATIONTYPE.labelres.getUID(), sResourceId);
				if (!localeFacade.getUserLocale().equals(localeFacade.getDefaultLocale())) {
					localeFacade.setDefaultResource(sResourceId, sText);
				}
			} else if (E.NUCLET.checkEntityUID(entityUid)) {
				if (mdvo.getFieldValue(E.NUCLET.localidentifier) == null) {
					// new nuclet -> create new li
					String li = DbObjectUtils.createUniqueLocalIdentifier(null/*nucletUID->not necessary here*/, new LocalIdentifierStore() {
						@Override
						public void set(UID nucletUID, String li) {
							// nothing to do here;
						}
						@Override
						public boolean exist(String li) {
							// search 
							for (EntityObjectVO<UID> eo : nucletDalProvider.getEntityObjectProcessor(E.NUCLET).getAll()) {
								if (li.equals(eo.getFieldValue(E.NUCLET.localidentifier))) {
									return true;
								}
							}
							return false;
						}
					});
					mdvo.setFieldValue(E.NUCLET.localidentifier, li);
				}
			} else if (E.MANDATOR.checkEntityUID(entityUid)) {
				validateMandator((EntityObjectVO<UID>) mdvo.getEntityObject());
				mdvo.setFieldUid(E.MANDATOR.level, getMandatorLevel(mdvo.getFieldUid(E.MANDATOR.parentMandator)));
			} else if (E.MANDATOR_LEVEL.checkEntityUID(entityUid)) {
				validateMandatorLevel((EntityObjectVO<UID>) mdvo.getEntityObject());
			}
			
			final EntityObjectVO<PK> eoval = mdvo.getEntityObject();
			if (!entityUid.equals(E.HISTORY.getUID()))
				validationSupport.validate(eoval, eoval.getDependents());

			// NUCLOS-1477
			handleAutoNumber(mdvo.getEntityObject());
			
			// create the row:
			PK iId = mdvo.getPrimaryKey();
			mdvo.setPrimaryKey(null);
			try {
				iId = masterDataFacadeHelper.createSingleRow(mdvo, this.getCurrentUserName(), this.getServerValidatesMasterDataValues(), iId);

				if(metaProvider.getEntity(entityUid).isWriteProxy())
				{
					// Wird von dieser Methode keine ID geliefert so muss Nuclos, wenn insert keine Exception liefert in die Ergebnislistenansicht springen
					if (iId == null) {
						throw new WriteProxyEmptyIdException("Es wurde von dem Proxy Objekt keine ID zurückgeliefert");
					}
					
				}else
				{
					// This happens only in a special case where a trigger set the primary key
					if (iId == null) {
						iId = masterDataFacadeHelper.getLastMasterDataCVOByMasterDataVO(mdvo).getPrimaryKey();
					}
				}
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			} catch (CommonFinderException ex) {
				throw new CommonFatalException(ex);
			}

			// NUCLOS-5981 First create dependencies
			if (mdvo.getDependents() != null && !mdvo.getDependents().isEmpty()) {
				if (!mdvo.getDependents().areAllDependentsNew()) {
					throw new IllegalArgumentException("Dependants must be new (must have empty ids).");
				}

				// create dependent rows:
				flagNew(mdvo.getDependents());
				// Note that this currently works for intids only, not for composite
				// primary keys:
				final PK iParentId = iId;
				try {
					masterDataFacadeHelper.createOrModifyDependants(mdvo.getDependents(), this.getCurrentUserName(), this.getServerValidatesMasterDataValues(), iParentId, customUsage);
				} catch (DbBusinessException ex) {
					throw new NuclosBusinessRuleException(ex.getMessage());
				}
			}

			// NUCLOS-5981 Now get the row again (including record grants)
			MasterDataVO<PK> result;
			try {
				result = masterDataFacadeHelper.getMasterDataCVOById(metaProvider.getEntity(mdvo.getEntityObject().getDalEntity()), iId);
			}
			catch(CommonFinderException ex) {
				throw new CommonFatalException(ex);
			}

			if (E.isNuclosEntity(entityUid) && mdvo.getResources() != null) {
				final LocaleFacadeLocal localeFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
				localeFacade.setResources(entityUid, (MasterDataVO<UID>) mdvo);
			}

			if (E.WEBSERVICE.getUID().equals(entityUid)) {
				compiler.invalidateManifest();
				try {
					compiler.check(new WsdlCodeGenerator(mdvo), false);
				}
				catch(NuclosCompileException e) {
					throw new CommonCreateException(e);
				}
			} else if (E.NUCLET.getUID().equals(entityUid)) {
				final TransferFacadeLocal transferFacade = SpringApplicationContextHolder.getBean(TransferFacadeLocal.class);
				transferFacade.checkCircularReference((UID) mdvo.getPrimaryKey());
			} else if (E.SERVERCODE.getUID().equals(entityUid)) {
				compiler.invalidateManifest();
			}
			
			boolean useRuleEngineSaveAfter = getEventSupportFacade().getUsesEventSupportForEntity(entityUid, InsertFinalRule.class.getCanonicalName());
			if(useRuleEngineSaveAfter) {
				try {
					fireSaveEvent(InsertFinalRule.class.getCanonicalName(), result, customUsage, false);
					result = masterDataFacadeHelper.getMasterDataCVOById(eMeta, iId);
				} catch (CommonFinderException | NuclosCompileException e) {
					throw new CommonFatalException(e);
				}
			}

			evictCaches(entityUid, result, customUsage);
			
			if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
				SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class).startupPort((UID) mdvo.getPrimaryKey());				
				// refresh to show the state
				result = get(entityUid, result.getPrimaryKey());
			}
			
			return result;
		}
		catch(CommonFinderException | CommonStaleVersionException | NuclosCompileException |
			CommonValidationException e) {
			throw new CommonCreateException(e.getMessage(), e);
		}
	}
	
	private <PK> void evictCaches(UID entityUid, MasterDataVO<PK> mdvo, String customUsage) {
		if (!E.isNuclosEntity(entityUid)) {
			return;
		}

		LOG.debug("invalidateCaches({}, {})", entityUid, mdvo );

		if (E.ROLE.checkEntityUID(entityUid) || E.ACTION.checkEntityUID(entityUid) || E.REPORT.checkEntityUID(entityUid)
				|| E.TASKLIST.checkEntityUID(entityUid) || E.PROCESS.checkEntityUID(entityUid) || E.GENERATION.checkEntityUID(entityUid) 
				|| E.MANDATOR.checkEntityUID(entityUid) || E.MANDATOR_LEVEL.checkEntityUID(entityUid)) {
			securityCache.invalidate();			
		}
		
		if (E.USER.checkEntityUID(entityUid)) {
			securityCache.invalidate(mdvo.getFieldValue(E.USER.name), true, customUsage);
		} 
		else if (E.DYNAMICENTITY.checkEntityUID(entityUid)) {
			notifyClients(E.DYNAMICENTITY, true);			
		}
		else if (E.ROLE.checkEntityUID(entityUid)) {
			notifyClients(E.ROLE, true);			
		}
		else if (E.ROLEUSER.checkEntityUID(entityUid) || E.ROLEPREFERENCE.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(PreferenceProcessor.class).evictPreferences();
		}
		else if (E.LAYOUT.checkEntityUID(entityUid)) {
			metaProvider.revalidate(true, true);
			SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).evictCaches();
			notifyClients(E.LAYOUT, true);			
		}
		else if (E.LAYOUTUSAGE.checkEntityUID(entityUid)) {
			metaProvider.revalidate(true, true);
		}
		else if (E.RESOURCE.checkEntityUID(entityUid)) {
			ResourceCache.getInstance().invalidateCache(true, false);
		}
		else if (E.PARAMETER.checkEntityUID(entityUid)) {
			serverParameterProvider.invalidateCache(true, true);
		}
		else if (E.DYNAMICENTITY.checkEntityUID(entityUid) || E.DYNAMICENTITYUSAGE.checkEntityUID(entityUid)
				|| E.CHART.checkEntityUID(entityUid) || E.CHARTUSAGE.checkEntityUID(entityUid)) {
			metaProvider.revalidate(true, false);
		}
		else if (E.DYNAMICTASKLIST.checkEntityUID(entityUid) || E.DYNAMICTASKLISTUSAGE.checkEntityUID(entityUid)
				|| E.TASKLIST.checkEntityUID(entityUid)) {
			metaProvider.revalidate(true, true);
		}
		else if (E.ENTITYLAFPARAMETER.checkEntityUID(entityUid) ||
				E.ENTITY_GENERIC_IMPLEMENTATION.checkEntityUID(entityUid) ||
				E.ENTITY_GENERIC_FIELDMAPPING.checkEntityUID(entityUid)) {
			metaProvider.revalidate(false, false);
		} 
		else if (E.ENTITYFIELDGROUP.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(EntityObjectFacadeLocal.class).evictGroupNamesCache();
		}
		else if (E.MANDATOR_LEVEL.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class).flushInternalCaches(true);
		}
		else if (E.NUCLET.checkEntityUID(entityUid) || E.MANDATOR.checkEntityUID(entityUid) || E.JOBCONTROLLER.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(NuclosParameterProvider.class).evictNucletParameterCaches();
		}
		else if (E.NUCLET_INTEGRATION_POINT.checkEntityUID(entityUid)) {
			metaProvider.revalidate(false, false);
			SpringApplicationContextHolder.getBean(EventSupportCache.class).invalidate();
		}
		else if (E.PRINTSERVICE.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(PrintServiceRepository.class).evictCaches();
		}
		else if (E.PROCESS.checkEntityUID(entityUid)) {
			evictProcessesFromCache(mdvo.getFieldUid(E.PROCESS.module));
		} 
		else if (E.FORM.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(ReportFacadeBean.class).evictFormMasterDataByReportUid((UID)mdvo.getPrimaryKey());
		} 
		else if (E.REPORTUSAGE.checkEntityUID(entityUid)) {
			UsageCriteria uc = new UsageCriteria(mdvo.getFieldUid(E.REPORTUSAGE.module), mdvo.getFieldUid(E.REPORTUSAGE.process), 
					mdvo.getFieldUid(E.REPORTUSAGE.state), customUsage);
			SpringApplicationContextHolder.getBean(ReportFacadeBean.class).evictReportUsagesByUsageCriteria(uc);
			notifyClients(E.REPORTUSAGE, true);
		}
		else { 
			LOG.debug("invalidateCaches: Nothing to do for {}", entityUid);
		}	
	}
	
	private UID getMandatorLevel(UID parentMandator) throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException {
		int createLevel = 0;
		UID result = null;
		UID parentLevel = null;
		if (parentMandator == null) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t = query.from(E.MANDATOR_LEVEL);
			query.select(t.basePk());
			query.where(builder.isNull(E.MANDATOR_LEVEL.parentLevel));
			try {
				result = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			} catch (DbInvalidResultSizeException dbe) {
				// no root level
				createLevel = 1;
			}
		} else {
			parentLevel = nucletDalProvider.getEntityObjectProcessor(E.MANDATOR).getByPrimaryKey(parentMandator).getFieldUid(E.MANDATOR.level);
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<UID> query = builder.createQuery(UID.class);
			DbFrom<UID> t = query.from(E.MANDATOR_LEVEL);
			query.select(t.basePk());
			query.where(builder.equalValue(t.baseColumn(E.MANDATOR_LEVEL.parentLevel), parentLevel));
			try {
				result = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
			} catch (DbInvalidResultSizeException dbe) {
				Long count = nucletDalProvider.getEntityObjectProcessor(E.MANDATOR_LEVEL).count(new CollectableSearchExpression());
				createLevel = count.intValue() + 1;
			}
		}
		if (createLevel > 0) {
			EntityObjectVO<UID> levelVO = new EntityObjectVO<UID>(E.MANDATOR_LEVEL);
			String userLanguage = getLocaleFacade().getUserLocale().getLanguage();
			levelVO.setFieldUid(E.MANDATOR_LEVEL.parentLevel, parentLevel);
			levelVO.setFieldValue(E.MANDATOR_LEVEL.name, (("DE".equalsIgnoreCase(userLanguage)?
					"Ebene ":"Level ") + createLevel));
			levelVO.setFieldValue(E.MANDATOR_LEVEL.showName, false);
			levelVO = createVO(new MasterDataVO<UID>(levelVO), null).getEntityObject();
			result = levelVO.getPrimaryKey();
		}
		return result;
	}
	
	private void validateMandator(EntityObjectVO<UID> mandator) {
		if (mandator.getPrimaryKey() != null) {
			if (mandator.getPrimaryKey().equals(mandator.getFieldUid(E.MANDATOR.parentMandator))) {
				mandator.removeFieldUid(E.MANDATOR.parentMandator.getUID());
			}
		}
		String sPath = ""; 
		if (mandator.getFieldUid(E.MANDATOR.parentMandator) != null) {
			MandatorVO parent = SecurityCache.getInstance().getMandator(mandator.getFieldUid(E.MANDATOR.parentMandator));
			sPath = parent.getPath() + MandatorVO.PATH_SEPARATOR;
		}
		sPath = sPath + mandator.getFieldValue(E.MANDATOR.name);
		mandator.setFieldValue(E.MANDATOR.path, sPath);
	}
	
	private void validateMandatorLevel(EntityObjectVO<UID> mandatorLevel) throws NuclosBusinessRuleException {
		UID parentLevel = mandatorLevel.getFieldUid(E.MANDATOR_LEVEL.parentLevel);
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom<UID> t = query.from(E.MANDATOR_LEVEL);
		query.select(query.getBuilder().countRows());
		query.where(builder.not(builder.equalValue(t.basePk(), mandatorLevel.getPrimaryKey())));
		if (parentLevel == null) {
			query.addToWhereAsAnd(builder.isNull(E.MANDATOR_LEVEL.parentLevel));
		} else {
			query.addToWhereAsAnd(builder.equalValue(t.baseColumn(E.MANDATOR_LEVEL.parentLevel), parentLevel));
		}
		
		Long count = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		if (count > 0) {
			throw new NuclosBusinessRuleException("The hierarchy is no longer linear");
		}
	}

	private void validateGenericBusinessObjectImplementation(EntityObjectVO<UID> gboImpl) throws NuclosBusinessRuleException {
		final UID genericEntityUID = gboImpl.getFieldUid(E.ENTITY_GENERIC_IMPLEMENTATION.genericEntity);
		final Collection<FieldMeta<?>> genericFieldList = metaProvider.getAllEntityFieldsByEntity(genericEntityUID).values();
		final Collection<EntityObjectVO<?>> fieldMappingList = gboImpl.getDependents().getData(DependentDataMap.createDependentKey(E.ENTITY_GENERIC_FIELDMAPPING.genericImplementation));
		for (FieldMeta<?> genFieldMeta : genericFieldList) {
			if (!genFieldMeta.isNullable()) {
				// check mandatory
				boolean mandatoryFieldFound = false;
				for (EntityObjectVO<?> fieldMapping : fieldMappingList) {
					if (fieldMapping.isFlagRemoved()) {
						continue;
					}
					final UID fieldMappingGenericFieldUID = fieldMapping.getFieldUid(E.ENTITY_GENERIC_FIELDMAPPING.genericField);
					if (genFieldMeta.getUID().equals(fieldMappingGenericFieldUID)) {
						mandatoryFieldFound = true;
						break;
					}
				}
				if (!mandatoryFieldFound) {
					throw new NuclosBusinessRuleException(SpringLocaleDelegate.getInstance().getMsg("mandatory.attribute.not.implemented", genFieldMeta.getFieldName()));
				}
			}
		}
	}

	/**
	 * modifies an existing master data record.
	 * 
	 * �precondition sEntityName != null
	 * �nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
    @RolesAllowed("Login")
	@Deprecated
	@Override
	public <PK> PK modify(MasterDataVO<PK> mdvo) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException {
    	return modify(mdvo, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), false);
    }
	
	/**
	 * modifies an existing master data record.
	 * 
	 * �precondition sEntityName != null
	 * �nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
    @RolesAllowed("Login")
	@Deprecated
	@Override
	public <PK> PK modify(MasterDataVO<PK> mdvo, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException {
    	return modify(mdvo, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY), isCollectiveProcessing);
    }
    
    /**
	 * modifies an existing master data record.
	 * 
	 * �precondition sEntityName != null
	 * �nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> PK modify(MasterDataVO<PK> mdvo, String customUsage, boolean isCollectiveProcessing) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException {
    	
    	PK result = modifyVO(mdvo, customUsage, isCollectiveProcessing);
    	return result;
    }
    

    /**
	 * modifies an existing master data record.
	 * 
	 * �precondition sEntityName != null
	 * �nucleus.permission checkWriteAllowed(sEntityName)
	 *
	 * @param mdvo the master data record
	 * @return id of the modified master data record
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> PK modify(MasterDataVO<PK> mdvo, String customUsage) throws CommonCreateException,
			CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonValidationException,
			CommonPermissionException, NuclosBusinessRuleException {
    	PK result = modifyVO(mdvo, customUsage, false);
    	return result;
    }
    
	protected <PK> PK modifyVO(MasterDataVO<PK> mdvo, String customUsage, boolean isCollectiveProcessing) throws CommonCreateException,
		CommonFinderException, CommonRemoveException,
		CommonStaleVersionException, CommonValidationException,
		CommonPermissionException, NuclosBusinessRuleException {

		final UID entityUid = mdvo.getEntityObject().getDalEntity();
		checkWriteAllowed(entityUid);
		grantUtils.checkWriteInternal(entityUid, mdvo.getPrimaryKey());
		lockUtils.checkLockedByCurrentUserInternal(entityUid, mdvo.getPrimaryKey());
		
		final MasterDataVO<PK> mdvoOld = getThin(entityUid, mdvo.getPrimaryKey());
                IRecompileChecker recompileChecker = recompileCheckerRegistry
                    .getRecompileChecker(entityUid);
                // optionally fill old mdvo with required dependents (for recompile required check):
                recompileChecker.loadRequiredDependentsOf(mdvoOld);

                mandatorUtils.checkWriteAllowed(mdvoOld.getEntityObject(), MetaProvider.getInstance().getEntity(entityUid));
                mandatorUtils.checkMandatorChange(mdvoOld.getEntityObject(), mdvo.getEntityObject(), MetaProvider.getInstance().getEntity(entityUid));
		
		final boolean useRuleEngineSave = getEventSupportFacade().getUsesEventSupportForEntity(
				entityUid, UpdateRule.class.getCanonicalName());

		if (useRuleEngineSave) {
			LOG.debug("Modifying (Start rules)");
			// In the modify case the changes from the rules must be reflected.
			// This is the same as in create. (tp)
			try {
				mdvo = fireSaveEvent(UpdateRule.class.getCanonicalName(), mdvo, customUsage, isCollectiveProcessing);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireSaveEvent failed", e);
			}
		}

		final String user = getCurrentUserName();
		if (E.ROLE.checkEntityUID(entityUid)
			&& SecurityCache.getInstance().isReadAllowedForMasterData(user, E.ROLE.getUID(), getCurrentMandatorUID())) {
			if (hasUserRole(user, mdvo.getDependents())) {
				masterDataFacadeHelper.validateRoleDependants(mdvo.getDependents());

				for (EntityObjectVO<UID> mdvo_dep : mdvo.getDependents().<UID>getDataPk(E.ROLEMASTERDATA.role)) {
					if (mdvo_dep.isFlagRemoved() && E.ROLE.checkEntityUID(mdvo_dep.getFieldUid(E.ROLEMASTERDATA.entity))) {
						throw new CommonFatalException("masterdata.error.role.permission");
						// "Sie d\u00fcrfen sich selber keine Rechte entziehen.");
					}
				}
			}
		}
		if (E.RELATIONTYPE.checkEntityUID(entityUid)) {
			LocaleFacadeLocal localeFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
			LocaleInfo localeInfo = localeFacade.getUserLocale();
			String sResourceId = mdvo.getFieldValue(E.RELATIONTYPE.labelres);
			String sText = mdvo.getFieldValue(E.RELATIONTYPE.name);
			sResourceId = localeFacade.setResourceForLocale(sResourceId, localeInfo, sText);
			mdvo.setFieldValue(E.RELATIONTYPE.labelres, sResourceId);
		} else if (E.MANDATOR.checkEntityUID(entityUid)) {
			validateMandator((EntityObjectVO<UID>) mdvo.getEntityObject());
			mdvo.setFieldUid(E.MANDATOR.level, getMandatorLevel(mdvo.getFieldUid(E.MANDATOR.parentMandator)));
		} else if (E.MANDATOR_LEVEL.checkEntityUID(entityUid)) {
			validateMandatorLevel((EntityObjectVO<UID>) mdvo.getEntityObject());
		} else if (E.ENTITY_GENERIC_IMPLEMENTATION.checkEntityUID(entityUid)) {
			validateGenericBusinessObjectImplementation((EntityObjectVO<UID>) mdvo.getEntityObject());
		}
		
		EntityObjectVO<PK> validation = mdvo.getEntityObject();
		validationSupport.validate(validation, mdvo.getDependents());
		
		if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
			try {
				SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class).
						shutdownPort((UID) mdvo.getPrimaryKey());
			} catch (BusinessException e) {
				throw new NuclosBusinessRuleException(e);
			}
		}

		// modify the row itself:
		final PK result;
		try {
			result = masterDataFacadeHelper.modifySingleRow(entityUid, mdvo, user, this.getServerValidatesMasterDataValues(), customUsage);
		} catch (DbBusinessException ex) {
			throw new NuclosBusinessRuleException(ex.getMessage());
		}
			
		if (E.getByUID(entityUid) != null && mdvo.getResources() != null) {
			LocaleFacadeLocal localeFacade = SpringApplicationContextHolder.getBean(LocaleFacadeLocal.class);
			localeFacade.setResources(entityUid, (MasterDataVO<UID>) mdvo);
		}
		
		if(mdvo.getDependents() != null) {
			try {
				modifyDependants(entityUid, mdvo.getPrimaryKey(), mdvo.isRemoved(),
					mdvo.getDependents(), customUsage);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			}
		}
		
		if (E.WEBSERVICE.getUID().equals(entityUid)) {
			compiler.invalidateManifest();
			try {
				compiler.check(new WsdlCodeGenerator(mdvo), false);
			}
			catch(NuclosCompileException e) {
				throw new CommonCreateException(e);
			}
		} else if (E.NUCLET.getUID().equals(entityUid)) {
			TransferFacadeLocal transferFacade = SpringApplicationContextHolder.getBean(TransferFacadeLocal.class);
			transferFacade.checkCircularReference((UID) mdvo.getPrimaryKey());
		} else if (E.SERVERCODE.getUID().equals(entityUid)) {
			compiler.invalidateManifest();
		}
		final boolean useRuleEngineSaveAfter = 
				getEventSupportFacade().getUsesEventSupportForEntity(entityUid, UpdateFinalRule.class.getCanonicalName());
		
		if (useRuleEngineSaveAfter) {
			try {
				LOG.debug("Modifying (Start rules after save)");
				MasterDataVO<PK> updated = get(entityUid, result);
				this.fireSaveEvent(UpdateFinalRule.class.getCanonicalName(), updated, customUsage, isCollectiveProcessing);
			} catch (CommonFinderException ex) {
				throw new CommonFatalException(ex);
			} catch (NuclosCompileException e) {
				throw new CommonCreateException("fireSaveEvent failed", e);
			}
		}
		 
		if (entityUid.equals(E.STATE.getUID())
				|| entityUid.equals(E.GENERATION.getUID())) {
			modifyParentLayouts(entityUid, mdvo, mdvoOld);
		}

		evictCaches(entityUid, mdvo, customUsage);

		if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
			SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class).startupPort((UID) mdvo.getPrimaryKey());				
		}

		// trigger recompile if required: (mdvoOld was filled up with required dependents)
		if (recompileChecker.isRecompileRequiredOnUpdate(mdvoOld, mdvo)) {
			triggerRecompile();
		}
		
		return result;
	}

	/**
	 * Triggers a recompile if the entity type may require a recompile.
	 *
	 */
	private void triggerRecompileOnInsertOrDeleteIfRequired(MasterDataVO<?> mdvo) throws CommonPermissionException {
		if (recompileCheckerRegistry.getRecompileChecker(mdvo.getEntityObject().getDalEntity())
			.isRecompileRequiredOnInsertOrDelete(mdvo)) {
			triggerRecompile();
		}
	}

	/**
	 * Causes a (async) recompilation after the tx commits successfully.
	 */
	private void triggerRecompile() throws CommonPermissionException {
		final boolean isSuperUser = SecurityCache.getInstance().isSuperUser(getCurrentUserName());
		if (!isSuperUser) {
			throw new CommonPermissionException("recompile.not.allowed");
		}
		maintenanceFacade.throwRecompileOutsideMaintenanceIfNecessary();

		CompileGeneratedCodeTransactionSynchronization transactionSync = transactionSyncs.get();
		if (transactionSync == null) {
			transactionSync =
				new CompileGeneratedCodeTransactionSynchronization(nuclosJarGeneratorManager);
			transactionSyncs.set(transactionSync);
		}
		transactionSync.increaseCallCount();
		TransactionSynchronizationManager.registerSynchronization(transactionSync);
	}

    private void modifyParentLayouts(UID sEntity, MasterDataVO<?> mdvo, MasterDataVO<?> mdvoold) {
		final String newArgument;
		final String oldArgument;
		final List<UID> collEntityUsages = new ArrayList<UID>(); 
		if (sEntity.equals(E.GENERATION.getUID())) {
			newArgument = mdvo.getFieldValue(E.GENERATION.name); 
			oldArgument = mdvoold.getFieldValue(E.GENERATION.name); 
			collEntityUsages.add(mdvo.getFieldUid(E.GENERATION.sourceModule));
		} else if (sEntity.equals(E.STATE.getUID())) {
			for (UID iStateModelId : StateModelUsagesCache.getInstance().getStateUsages().getStateModelUIDsByStatusUID((UID) mdvo.getPrimaryKey())) {
				for (UsageCriteria uc : StateModelUsagesCache.getInstance().getStateUsages().getUsageCriteriasByStateModelUID(iStateModelId)) {
					collEntityUsages.add(uc.getEntityUID());
				}
			}
			newArgument = mdvo.getFieldValue(E.STATE.numeral).toString(); 
			oldArgument = mdvoold.getFieldValue(E.STATE.numeral).toString(); 
		} else
			throw new NuclosFatalException(sEntity.toString());
		
		// check if arguments has changed.
		if (LangUtils.equal(oldArgument, newArgument))
				return; // nothing to do
		
		for (UID sParentEntity : collEntityUsages) {
			Set<UID> lstLayouts = new HashSet<UID>();
			CollectableComparison compare = SearchConditionUtils.newComparison(
					E.LAYOUTUSAGE.entity, ComparisonOperator.EQUAL, sParentEntity);
			for (MasterDataVO<?> layout : getMasterData(E.LAYOUTUSAGE.getUID(), compare, true)) {
				lstLayouts.add(layout.getFieldUid(E.LAYOUTUSAGE.layout));
			}
			for (UID iLayoutId : lstLayouts) {
				try {
					MasterDataVO<UID> voLayout = get(E.LAYOUT.getUID(), iLayoutId);

					String sLayout = (String)voLayout.getFieldValue(E.LAYOUT.layoutML);
					
					try {
						sLayout = new LayoutMLParser().replaceButtonArguments(sLayout, sEntity, newArgument, oldArgument);
					} catch (LayoutMLException e) {
						throw new NuclosFatalException(e);
					}
					
					voLayout.setFieldValue(E.LAYOUT.layoutML, sLayout);
					
					try {
						//TODO collective or not?
			            modifyVO(voLayout, null, false);
		            }
		            catch(CommonBusinessException e) {
		            	throw new NuclosFatalException(e);
		            }
				}
				catch(Exception e) {
					// don't modify layout
					LOG.info("searchParentLayouts failed: {}", e);
				}
			}
		}
	}	
	
	/**
	 * notifies clients that the contents of an entity has changed.
	 * 
	 * �precondition sCachedEntityName != null
	 *
	 * @param sCachedEntityName name of the cached entity.
	 */
	public void notifyClients(UID sCachedEntityName) {
		masterDataFacadeHelper.notifyClients(sCachedEntityName);
	}

	/**
	 * notifies clients that the meta data has changed, so they can invalidate their local caches.
	 * <p>
	 * TODO: Why on hell does this method sends to TOPICNAME_METADATACACHE but the above <code>notifyClients</code>
	 * sends to TOPICNAME_MASTERDATACACHE???
	 * </p>
	 */
	protected void notifyClients(EntityMeta<?> entity, boolean refreshMenus) {
		LOG.info("JMS send: notify clients that entity {} changed: {}",
		         entity.getEntityName(), this);
		LocalCachesUtil.getInstance().updateLocalCacheRevalidation(
			JMSConstants.TOPICNAME_METADATACACHE);
		NuclosJMSUtils.sendOnceAfterCommitDelayed(new NotifyObject(entity.getUID(), refreshMenus), JMSConstants.TOPICNAME_METADATACACHE);
	}
	
	private boolean hasUserRole(String sUser, IDependentDataMap mpDependants) {
//		final UserFacadeLocal ufl = SpringApplicationContextHolder.getApplicationContext().getBean(UserFacadeLocal.class);
		final UID userUid = SecurityCache.getInstance().getUserUid(sUser);
		if (userUid != null && mpDependants != null) {
			for (EntityObjectVO<UID> mdvo : mpDependants.<UID>getDataPk(E.ROLEUSER.role)) {
				if (userUid.equals(mdvo.getFieldUid(E.ROLEUSER.user))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * method to delete an existing master data record
	 * 
	 * �precondition sEntityName != null
	 * �nucleus.permission checkDeleteAllowed(sEntityName)
	 *
	 * @param pk Primary Key to be deleted
	 * @param bRemoveDependants remove all dependants if true, else remove only
	 *           given (single) mdvo record this is helpful for entities which
	 *           have no layout
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> void remove(UID entity, PK pk,
		boolean bRemoveDependants) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException,
		CommonPermissionException, NuclosBusinessRuleException {
    	remove(entity, pk, bRemoveDependants, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
    }

	/**
	 * method to delete an existing master data record
	 * 
	 * precondition sEntityName != null
	 * nucleus.permission checkDeleteAllowed(sEntityName)
	 *
	 * @param entityUid
         * @param pk
         * @param bRemoveDependants remove all dependants if true, else remove only
	 *           given (single) mdvo record this is helpful for entities which
	 *           have no layout
         * @param customUsage
	 */
    @RolesAllowed("Login")
    @Override
	public <PK> void remove(UID entityUid, PK pk,
		boolean bRemoveDependants, String customUsage) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException,
		CommonPermissionException, NuclosBusinessRuleException {
    	
    	EntityMeta<?> eMeta = metaProvider.getEntity(entityUid);
		checkDeleteAllowed(entityUid);
		grantUtils.checkDeleteInternal(entityUid, pk);
		lockUtils.checkLockedByCurrentUser(entityUid, pk);
		mandatorUtils.checkWriteAllowedFromDb(pk, eMeta);
		
    	MasterDataVO<PK> mdvo = masterDataFacadeHelper.getMasterDataCVOThinById(eMeta, pk, false);
    	
		EntityObjectVO<?> eo = mdvo.getEntityObject();
		mdvo.remove();
		
		try {
			this.fireDeleteEvent(mdvo, false, customUsage);
		} catch (NuclosCompileException e) {
			throw new CommonRemoveException("fireDeleteEvent failed: " + e, e);
		}
		
		if (bRemoveDependants) {
			Map<EntityAndField, UID> mpEntityAndParentEntityName = getLayoutFacade().getSubFormEntityAndParentSubFormEntityNames(
				entityUid, pk, false, customUsage);
			IDependentDataMap mdp = masterDataFacadeHelper.readAllDependents(entityUid, pk,
				mdvo.getDependents(), mdvo.isRemoved(), null, mpEntityAndParentEntityName);
			try {
				masterDataFacadeHelper.removeDeletedDependants(mdp, customUsage, true);
			} catch (DbBusinessException ex) {
				throw new NuclosBusinessRuleException(ex.getMessage());
			}
		}

		if (E.WEBSERVICE.getUID().equals(entityUid)) {
			compiler.invalidateManifest();
			try {
				compiler.check(new WsdlCodeGenerator(mdvo), true);
			}
			catch(NuclosCompileException e) {
				throw new CommonRemoveException("fireDeleteEvent failed: " + e, e);
			}
		} else if (E.SERVERCODE.getUID().equals(entityUid)) {
			compiler.invalidateManifest();
		}
		if (E.COMMUNICATION_PORT.checkEntityUID(entityUid)) {
			try {
				SpringApplicationContextHolder.getBean(CommunicationInterfaceLocal.class).
						shutdownPort((UID) mdvo.getPrimaryKey());
			} catch (BusinessException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		if (E.MANDATOR.checkEntityUID(entityUid)) {
			dataBaseHelper.execute(new DbDeleteStatement<>(E.MANDATOR_ACCESSIBLE, new DbMap()));
		}

		try {
			masterDataFacadeHelper.removeSingleRow(mdvo, customUsage);
		} catch (DbBusinessException ex) {
			throw new NuclosBusinessRuleException(ex.getMessage());
		}

		// Note that the dependants are removed via cascading delete in the database.
		masterDataFacadeHelper.removeDependantTaskObjects(pk); 
		//explicit delete, because it is not a reference, so no db constraint available

		if (E.RELATIONTYPE.checkEntityUID(entityUid)) {
			String sResourceId = mdvo.getFieldValue(E.RELATIONTYPE.labelres);
			getLocaleFacade().deleteResource(sResourceId);
		}
		try {
			fireDeleteEvent(mdvo, true, customUsage);
		} catch (NuclosCompileException e) {
			throw new CommonRemoveException("fireDeleteEvent failed: " + e, e);
		} 
		
		evictCaches(entityUid, mdvo, customUsage);
		
	    triggerRecompileOnInsertOrDeleteIfRequired(mdvo);

    }

	/**
	 * fires a Save event, executing the corresponding business rules.
	 *
	 * @param event
	 * @param mdvo
         * @param customUsage
	 * @return
	 * @throws NuclosBusinessRuleException
	 * @throws NuclosCompileException 
	 */
	private <T> MasterDataVO<T> fireSaveEvent(String event, MasterDataVO<T> mdvo, String customUsage, boolean isCollectiveProcessing)
		throws NuclosBusinessRuleException, NuclosCompileException {
		
		EntityObjectVO<T> eoVO = mdvo.getEntityObject();
		try {
			eoVO = getEventSupportFacade().fireSaveEventSupport(mdvo.getEntityObject(), event, isCollectiveProcessing);
		} catch (NuclosCompileException compileEx) {
			if (E.isNuclosEntity(eoVO.getDalEntity())) {
				// ignore compile exceptions in case of a nuclos system entity
				LOG.warn(compileEx.getMessage(), compileEx);
			}
		}
		return new MasterDataVO<T>(eoVO);
	}

	private static <T> UsageCriteria extractUsageCriteria(EntityObjectVO<T> eoVO, String customUsage) {
		
		final EntityMeta<T> eMeta = MetaProvider.getInstance().getEntity(eoVO.getDalEntity());	
		
		UID process = null;
		UID status = null;
		if (eMeta.isStateModel()) {
			process = SF.PROCESS.getUID(eoVO.getDalEntity());
			status = SF.STATE.getUID(eoVO.getDalEntity());
		}
		
		return new UsageCriteria(eoVO.getDalEntity(), process, status, customUsage);
	}
	/**
	 * fires a Delete event, executing the corresponding business rules.
	 *
	 * @param mdvo
	 * @param after
         * @param customUsage
	 * @return
	 * @throws NuclosBusinessRuleException
	 * @throws CommonFatalException 
	 * @throws NuclosCompileException 
	 */
	private <PK> void fireDeleteEvent(MasterDataVO<PK> mdvo, boolean after, String customUsage) throws NuclosBusinessRuleException, NuclosCompileException, CommonFatalException {

		final EntityObjectVO<PK> eoVO = mdvo.getEntityObject();
		final UsageCriteria usage = extractUsageCriteria(eoVO, customUsage);	
				
		// EventSupports
		String sSupportType = null;
		if (after) sSupportType = DeleteFinalRule.class.getCanonicalName();
		if (!after) sSupportType = DeleteRule.class.getCanonicalName();

		try {
			getEventSupportFacade().fireDeleteEventSupport(eoVO, sSupportType, usage, true);
		} catch (NuclosCompileException compileEx) {
			if (E.isNuclosEntity(eoVO.getDalEntity())) {
				// ignore compile exceptions in case of a nuclos system entity
				LOG.warn(compileEx.getMessage(), compileEx);
			}
		}
	}
	
	/**
	 * Get all subform entities of a masterdata entity
	 */
    @RolesAllowed("Login")
    @Override
	public Set<EntityAndField> getSubFormEntitiesByMasterDataEntity(UsageCriteria usage) {
		if (getLayoutFacade().isDetailLayoutAvailable(usage)) {
			Map<EntityAndField, UID> mpEntityAndParentEntityName = 
					getLayoutFacade().getSubFormEntityAndParentSubFormEntityNames(
							usage.getEntityUID(), usage.getEntityUID(), false, usage.getCustom());
			return new HashSet<EntityAndField>(mpEntityAndParentEntityName.keySet());
		}
		return Collections.emptySet();
	}
    
    /**
     * Reads sub-form data for multiple foreign-keys at once. Much faster and less resource consuming than iterating through
     * each data row. Specifically interesting for loading sub-sub-form data.
     * 
     * @param lstIds - List of foreign keys
     * @param master - master BO
     * @param subform - sub-form BO
     * @return A map of the foreign keys holding a collection of the complete sub-form data.
     */
    public <PK> Map<PK, Collection<EntityObjectVO<PK>>> readMultiSubFormData(List<PK> lstIds, UID master, DependentKey subform) {
    	return masterDataFacadeHelper.readMultiSubFormData(lstIds, master, subform);
    }

    /**
	 * create the given dependants (local use only).
	 *
	 * �precondition mpDependants != null
	 */
    public void createDependants(UID entityUid, Object id, Boolean removed,
		IDependentDataMap dependants, String customUsage) throws CommonCreateException, CommonPermissionException {
    	try {
    		flagNew(dependants);
    		createOrModifyDependants(entityUid, id, removed, dependants, false, customUsage);
    	}
		catch (CommonFinderException | CommonRemoveException | CommonStaleVersionException ex) {
			// This must never happen when inserting a new object:
			throw new CommonFatalException(ex);
		}
    }
    
    private void flagNew(IDependentDataMap dependants) {
    	if (dependants == null) {
    		return;
    	}
    	for (List<EntityObjectVO<?>> list : dependants.getRoDataMap().values()) {
    		for (EntityObjectVO<?> eovo : list) {
    			if (!eovo.isFlagRemoved()) {
		    		eovo.flagNew();
		    		eovo.setVersion(1);
		    		eovo.setPrimaryKey(null);
		    		eovo.setCreatedBy(null);
		    		eovo.setCreatedAt(null);
    			}
	    		flagNew(eovo.getDependents());
    		}
    	}
    }
    
    /**
	 * modifies the given dependants (local use only).
	 *
	 * �precondition mpDependants != null
	 */
    public void modifyDependants(UID entityName, Object id, Boolean removed,
		IDependentDataMap dependants, String customUsage) throws CommonCreateException,
		CommonFinderException, CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException {
    	createOrModifyDependants(entityName, id, removed, dependants, true, customUsage);
    }
   
	private void createOrModifyDependants(UID entityName, Object id, Boolean removed,
		IDependentDataMap dependants, boolean removeDeleted, String customUsage) throws CommonCreateException,
		CommonFinderException, CommonRemoveException, CommonPermissionException,
		CommonStaleVersionException {
		
		if (dependants == null) {
			throw new NullArgumentException("dependants");
		}

		if (removeDeleted) {
			masterDataFacadeHelper.removeDeletedDependants(dependants, customUsage, false);
		}

		try {
			masterDataFacadeHelper.createOrModifyDependants(dependants, this.getCurrentUserName(), this.getServerValidatesMasterDataValues(), id, customUsage);
		}
		catch(CommonValidationException ex) {
			// @todo check this exception handling
			throw new CommonCreateException(ex.getMessage(), ex);
		}
	}

	/**
	 * revalidates the cache. This may be used for development purposes only, in
	 * order to rebuild the cache after metadata entries in the database were
	 * changed.
	 */
    @RolesAllowed("UseManagementConsole")
    @Override
	public void revalidateMasterDataMetaCache(boolean otherCaches, boolean refreshMenus) {
		metaProvider.revalidate(otherCaches, refreshMenus);
	}

	/**
	 * value list provider function (get processes by usage)
	 *
	 * @param entityUid module id of usage criteria
	 * @param bSearchMode when true, validity dates and/or active sign will not
	 *           be considered in the search.
	 * @return collection of master data value objects
	 */
    @RolesAllowed("Login")
    @Override
	@Cacheable(value = "processByEntity", key = "#p0.getString() + (#p1 ? '1' : '0')")
	public List<CollectableField> getProcessByEntity(UID entityUid, boolean bSearchMode) {
		// @todo Try to replace with getDependantMasterData

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<?> process = query.from(E.PROCESS, "process");
		query.multiselect(process.basePk(), process.baseColumn(E.PROCESS.name));
		DbCondition condition = builder.equalValue(process.baseColumn(E.PROCESS.module), entityUid);
		if (!bSearchMode) {
			DbColumnExpression<Date> datValidFrom = process.baseColumn(E.PROCESS.validFrom);
			DbColumnExpression<Date> datValidUntil = process.baseColumn(E.PROCESS.validUntil);
			condition = builder.and(
				condition,
				builder.or(builder.lessThanOrEqualTo(datValidFrom, builder.currentDate()), datValidFrom.isNull()),
				builder.or(builder.greaterThanOrEqualTo(datValidUntil, builder.currentDate()), datValidUntil.isNull()));
		}
		query.where(condition);

		return dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, CollectableField>() {
			@Override
			public CollectableField transform(DbTuple t) {
				return new CollectableValueIdField(t.get(0, Object.class), t.get(1, String.class));
			}
		});
	}
	@Caching(evict = { 
		@CacheEvict(value="processByEntity", key="#p0.getString() + '1'"),
		@CacheEvict(value="processByEntity", key="#p0.getString() + '0'")
	})
    private void evictProcessesFromCache(UID entityUid) {    	
    }
	
	@CacheEvict(value="processByEntity", allEntries = true)
	public void evictAllProcessesFromCache() {
	}

	/**
	 * @param iModuleId the id of the module whose subentities we are looking for
	 * @return Collection&lt;MasterDataMetaVO&gt; the masterdata meta information for
	 *         all entities having foreign keys to the given module.
	 */
    @Override
    public List<CollectableField> getSubEntities(UID iModuleId) {

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom m = query.from(E.ENTITY, "m");
		DbFrom mf = m.joinOnBasePk(E.ENTITYFIELD, JoinType.INNER, E.ENTITYFIELD.entity, "mf");
		DbFrom p = mf.joinOnJoinedPk(E.ENTITY, JoinType.INNER, E.ENTITYFIELD.foreignentity, "p");
		query.multiselect(m.basePk(),	m.baseColumn(E.ENTITY.entity));
		query.where(builder.equalValue(p.basePk(), iModuleId));
		query.orderBy(builder.asc(m.baseColumn(E.ENTITY.entity)));

		return dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, CollectableField>() {
			@Override
			public CollectableField transform(DbTuple t) {
				return new CollectableValueIdField(t.get(0, Object.class), t.get(1, String.class));
			}
		});
	}

    @Override
    public Map<String, String> getRuleEventsWithLocaleResource() {
		Map<String, String> mp = CollectionUtils.newHashMap();
		// TODO_AUTOSYNC: Re-merge with database, add resourceId to metadata
		for(MasterDataVO<UID> mdvo : XMLEntities.getData(E.EVENT).getAll()) {
			mp.put(mdvo.getFieldValue(E.EVENT.name), mdvo.getFieldValue(E.EVENT.labelres));
		}
		return mp;
	}

	/**
	 * Validate all masterdata entries against their meta information (length,
	 * format, min, max etc.). The transaction type is "not supported" here in
	 * order to avoid a transaction timeout, as the whole operation may take some
	 * time.
	 *
	 * @param sOutputFileName the name of the csv file to which the results are
	 *           written.
	 *           
	 * @deprecated Very old cruft. Validate is not implemented any more. (tp)
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED, noRollbackFor= {Exception.class})
	@RolesAllowed("UseManagementConsole")
	public void checkMasterDataValues(String sOutputFileName) {
		throw new UnsupportedOperationException();
		
	}

	
	/**
	 * @param sEntityName
	 * @param iId the object's id (primary key)
	 * @return the masterdata object with the given entity and id.
	 * @throws CommonFinderException
	 * @throws CommonPermissionException
	 * @deprecated use with customUsage
	 */
    @Deprecated
    public <PK> MasterDataVO<PK> getWithDependants(UID sEntityName,
    		PK iId) throws CommonFinderException, NuclosBusinessException,
    		CommonPermissionException {
    	return getWithDependants(sEntityName, iId, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
    }
    
    // @Override
    public <PK> MasterDataVO<PK> getWithDependants(UID entityUid,
		PK iId, String customUsage) throws CommonFinderException, NuclosBusinessException,
		CommonPermissionException {
		if (iId == null) {
			throw new NullArgumentException("iId");
		}
		List<EntityAndField> lsteafn = new ArrayList<EntityAndField>();

		for(EntityAndField eafn : getLayoutFacade().getSubFormEntityAndParentSubFormEntityNames(
			entityUid, iId, false, customUsage).keySet()) {
			lsteafn.add(eafn);
		}

		final MasterDataVO<PK> result = get(entityUid, iId);
		result.setDependents(getDependants(iId, lsteafn));
		return result;
		// return new MasterDataVO(get(sEntityName, iId), getDependants(iId, lsteafn));
	}
    
	public <PK> Collection<MasterDataVO<PK>> getWithDependantsByCondition(
			EntityMeta<PK> entity, CollectableSearchCondition cond, String customUsage) {
		return getWithDependantsByCondition(entity.getUID(), cond, customUsage);
	}

	/**
	 * @param sEntityName
	 * @param cond search condition
	 * @return the masterdata objects for the given entityname and search
	 *         condition.
	 */
    public <PK> Collection<MasterDataVO<PK>> getWithDependantsByCondition(
		UID sEntityName, CollectableSearchCondition cond, String customUsage) {
		Collection<MasterDataVO<PK>> result = new ArrayList<MasterDataVO<PK>>();

		for(MasterDataVO<PK> mdVO : this.<PK>getMasterData(sEntityName, cond, true)) {
			List<EntityAndField> lsteafn = new ArrayList<EntityAndField>();

			for(EntityAndField eafn : getLayoutFacade().getSubFormEntityAndParentSubFormEntityNames(
				sEntityName, mdVO.getPrimaryKey(), false, customUsage).keySet()) {
				lsteafn.add(eafn);
			}
			mdVO.setDependents(getDependants(mdVO.getPrimaryKey(), lsteafn));
			// result.add(new MasterDataVO(mdVO, getDependants(mdVO.getPrimaryKey(), lsteafn)));
			result.add(mdVO);
		}
		return result;
	}

    // @Override
	public IDependentDataMap reloadDependants(UID entityname, MasterDataVO<?> mdvo, boolean bAll, String customUsage) throws CommonFinderException {
		final Map<EntityAndField, UID> collSubEntities = getLayoutFacade().getSubFormEntityAndParentSubFormEntityNames(
				entityname, mdvo.getPrimaryKey(), false, customUsage);
		return getDependants(mdvo.getPrimaryKey(), new ArrayList<EntityAndField>(collSubEntities.keySet()));
	}

    // @Override
	public <PK> IDependentDataMap getDependants(Object oId, List<EntityAndField> lsteafn) {
		final DependentDataMap result = new DependentDataMap();
		for (EntityAndField eafn : lsteafn) {
			final UID entity = eafn.getEntity();
			Collection<EntityObjectVO<PK>> col = getDependantMasterData(entity, eafn.getField(), oId);
			result.addAllData(eafn.getDependentKey(), col);
		}
		return result;
	}
	
	public DependentDataMap getDependantsAnonymously(Object oId, List<EntityAndField> lsteafn) {
			final DependentDataMap result = new DependentDataMap();
			for (EntityAndField eafn : lsteafn) {
				final UID entity = eafn.getEntity();
				Collection<EntityObjectVO<Object>> col = masterDataFacadeHelper.getDependantMasterData(entity,
						eafn.getField(), NuclosSystemParameters.ANONYMOUS_LOGIN, null, oId);
				result.addAllData(eafn.getDependentKey(), col);
			}
			return result;
		}

	/**
	 * @param user - the user for which to get subordinated users
	 * @return List&lt;MasterDataVO&gt; list of masterdata valueobjects
	 */
    @Override
	public Collection<MasterDataVO<UID>> getUserHierarchy(String user) {
		boolean isSuperUser = SecurityCache.getInstance().isSuperUser(user);
		if (!isSuperUser) {
			List<UID> roles = new ArrayList<UID>();
			roles.addAll(getRolesHierarchyForUser(user));
			return getUsersForRoles(roles);
		} else {
			return new ArrayList<MasterDataVO<UID>>((Collection)getMasterData(E.USER.getUID(), false));
		}
	}

	/**
	 * Gets the roles for the given user recursively (including inherited roles).
	 *
	 * @param user
	 * @return
	 */
	public Set<UID> getRolesHierarchyForUser(final String user) {
		Set<UID> result = new HashSet<>();

		if (user == null) {
			return result;
		}

		final UID userUID = SecurityCache.getInstance().getUserUid(user);

		Collection<MasterDataVO<Object>> userRoles = getMasterData(
				E.ROLEUSER.getUID(),
				SearchConditionUtils.newComparison(
						E.ROLEUSER.user,
						ComparisonOperator.EQUAL,
						userUID
				),
				false
		);
		for (MasterDataVO voRole : userRoles) {
			result.add(voRole.getFieldUid(E.ROLEUSER.role));
			addSubordinateRoles(voRole.getFieldUid(E.ROLEUSER.role), result);
		}
		return result;
	}

	private void addSubordinateRoles(UID role, Set<UID> alreadyCollectedRoles) {
		Set<UID> roles = new HashSet<UID>();
		Collection<MasterDataVO<Object>> subordinateRoles = getMasterData(E.ROLE.getUID(), 
				SearchConditionUtils.newUidComparison(E.ROLE.parentrole, ComparisonOperator.EQUAL, role), false);
		for (MasterDataVO<?> voRole : subordinateRoles) {
			if (!alreadyCollectedRoles.contains(voRole.getPrimaryKey())) {
				roles.add((UID) voRole.getPrimaryKey());
				addSubordinateRoles((UID) voRole.getPrimaryKey(), roles);
			}
		}
		alreadyCollectedRoles.addAll(roles);
	}

	private List<MasterDataVO<UID>> getUsersForRoles(List<UID> roles) {
		final CollectableEntityField entityFieldRole = new CollectableMasterDataEntity(
				E.ROLEUSER).getEntityField(E.ROLEUSER.role.getUID());
		final ReferencingCollectableSearchCondition refCond = new ReferencingCollectableSearchCondition(
				entityFieldRole, new CollectableIdListCondition(roles));
		final Set<MasterDataVO<UID>> users = CollectionUtils.unsaveConvertToSet(
				getMasterData(E.ROLEUSER.getUID(), refCond, false));
		final IEntityObjectProcessor<UID> userProcessor = nucletDalProvider.getEntityObjectProcessor(E.USER);
		
		return CollectionUtils.transform(users, new Transformer<MasterDataVO<UID>, MasterDataVO<UID>>() {
			@Override
			public MasterDataVO transform(MasterDataVO roleuser) {
				return DalSupportForMD.wrapEntityObjectVO(userProcessor.getByPrimaryKey(roleuser.getFieldUid(E.ROLEUSER.user)));
			}
		});
	}
	
	/**
	 * handle autonumber generation
	 * 
	 * @param evo	entity vo
	 * @throws NuclosBusinessRuleException 
	 */
	private final void handleAutoNumber(final EntityObjectVO evo) throws NuclosBusinessRuleException {
		// find autonumber field
		// @TODO ignore System entities?
		final FieldMeta<Integer> metaFieldVO = autoNumberHelper.findAutoNumberFieldIfAny(evo.getDalEntity());
		if (null == metaFieldVO) {
			return;
		}
		// next autonumber if autonumberfield exists
		evo.setFieldValue(metaFieldVO.getUID(), autoNumberHelper.findNextAutoNumber(evo, metaFieldVO));
	}
	
	/**
	 * @return Does the entity with the given name use the rule engine?
	 */
    @RolesAllowed("Login")
	public boolean getUsesRuleEngine(UID entityUID) {
		return this.getUsesRuleEngine(entityUID, CustomRule.class.getCanonicalName());
	}

	private boolean getUsesRuleEngine(UID entityUID, String event) {
		return getEventSupportFacade().getUsesEventSupportForEntity(entityUID, event);
	}
	
	@RolesAllowed("Login")
	@Override
	public boolean exist(UID entityUid, Object id) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom t = query.from(metaProvider.getEntity(entityUid));
		query.select(builder.countRows());
		query.where(builder.equalValue(t.basePk(), id));
		List<Long> result = dataBaseHelper.getDbAccess().executeQuery(query);
		return result.get(0).longValue() > 0l;
	}
	
	public <PK> void fillDependentsForSubformColumns(List<EntityObjectVO<PK>> eos, Collection<UID> fields, UID entity, String customUsage) {	
		if (fields == null) {
			return;
		}
		
		Set<UID> fFromEntity = metaProvider.getAllEntityFieldsByEntity(entity).keySet();
		final Set<IDependentKey> subforms = new HashSet<IDependentKey>();
		
		for (UID field : fields) {
			if (fFromEntity.contains(field) || SF.VERSION.checkField(entity, field)) {
				//Checking for Version, too, because of Rest.getAllEntityFieldsByEntityIncludingVersion(...)
				continue;
			}
			
			FieldMeta<?> f = metaProvider.getEntityField(field);
			IDependentKey depkey = getLayoutFacade().getDependentKeyBetween(entity, f.getEntity(), null);
			if (depkey != null) {
				subforms.add(depkey);
			}
		}

		if (!subforms.isEmpty()) {
			// fill in (dependent) subforms
				
			try {
				fillDependants(eos, subforms, null);
			} catch (CommonFinderException e) {
				throw new NuclosFatalException(e);
			}
		}
	}

	/**
	 * fills the dependants of <code>lowdcvo</code> with the data from the required sub entities.
	 * 
	 * §precondition stRequiredSubEntityNames != null
	 */
	private <PK> void fillDependants(List<EntityObjectVO<PK>> eos, Set<IDependentKey> stRequiredSubEntities, String customUsage) throws CommonFinderException {
		for (IDependentKey s: stRequiredSubEntities) {
			
			UID master = null;
			List<PK> lstIds = new ArrayList<PK>();
			for (EntityObjectVO<PK> base : eos) {
				if (master == null) {
					master = base.getDalEntity();
				}
				lstIds.add(base.getPrimaryKey());
				
			}
			
			Map<PK, Collection<EntityObjectVO<PK>>> mpData = masterDataFacadeHelper.readMultiSubFormData(lstIds, master, s);
			
			for (EntityObjectVO<PK> base : eos) {
				
				final Collection<EntityObjectVO<PK>> col = mpData.get(base.getPrimaryKey());
				if (col != null) {
					FieldMeta<?> refFieldMeta = metaProvider.getEntityField(s.getDependentRefFieldUID());
					base.getDependents().addAllData(refFieldMeta, col);					
				}
			}			
		}
	}

}
