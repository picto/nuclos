//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.processor.ColumnToFieldIdVOMapping;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.impl.standard.StandardSqlDBAccess;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.structure.DbColumnType;
import org.nuclos.server.dblayer.structure.DbColumnType.DbGenericType;

/**
 * Column mapping for compound (one-)table expressions. This is used for nuclos 'stringified'
 * references.
 *
 * @author Thomas Pasch
 * @since Nuclos 3.2.01
 * @see org.nuclos.server.dal.processor.ColumnToRefFieldVOMapping#getDbColumn(DbFrom)
 *
 * @param <T> java type of the expression result, must be String for real compound
 * 		expressions but maybe different for 'singleton' expressions.
 */
public class DbCompoundColumnExpression<T> extends DbExpression<T> implements IDbCompoundColumnExpression {

	public DbCompoundColumnExpression(DbFrom<?> from, FieldMeta<?> field, boolean setAlias, boolean bSearch) {
		super(from.getQuery().getBuilder(), (Class<T>) DbUtils.getDbType(field.getDataType()), setAlias ? field.getDbColumn() : null, mkConcat(from, field, bSearch, null));
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
	}
	
	static final PreparedStringBuilder mkConcat(DbFrom<?> from, FieldMeta<?> field, boolean bSearch, Map<UID, String> aliasMap) {
		return mkConcat(from, field, bSearch, null, aliasMap);
	}
	
	private static final PreparedStringBuilder mkConcat(DbFrom<?> from, FieldMeta<?> field, boolean bSearch, final String extJoinAlias, Map<UID, String> aliasMap) {
		if (!field.hasAnyForeignEntity()) {
			throw new IllegalArgumentException();
		}
		if (field.getDbColumn().startsWith("INTID_T_")) {
			return DbColumnExpression.mkQualifiedColumnName(SystemFields.BASE_ALIAS, field.getDbColumn(), false);
		}
		final MetaProvider mp = MetaProvider.getInstance();
		final TableAliasSingleton tas = TableAliasSingleton.getInstance();
		final boolean isInheritReference = extJoinAlias != null;
		
		final List<String> qualifiedNames = new ArrayList<String>();
		final List<String> toConcat = new ArrayList<String>();
		final StandardSqlDBAccess dbAccess = from.getQuery().getBuilder().getDBAccess();
		
		for (IFieldUIDRef ref: new ForeignEntityFieldUIDParser(field, mp, bSearch)) {
			if (ref.isConstant()) {
				toConcat.add("'" + ref.getConstant() + "'");
			}
			else {		
				final FieldMeta<?> mdField = mp.getEntityField(ref.getUID());
				PreparedStringBuilder reference;
				String qualifiedName;
				if ((mdField.getForeignEntity() != null || mdField.getUnreferencedForeignEntity() != null) && 
						(mdField.getDbColumn().startsWith("STRVALUE_") || mdField.getDbColumn().startsWith("INTVALUE_") || mdField.getDbColumn().startsWith("OBJVALUE_"))) {
					// RSWORGA-105
					if (isInheritReference) {
						continue;
					}
					String joinAlias = tas.getAlias(field, mdField);
					reference = mkConcat(from, mdField, bSearch, joinAlias, aliasMap);
					if (reference == null) {
						continue;
					}
					qualifiedName = reference.toString();
				} else {
					reference = new PreparedStringBuilder(mdField.getDbColumn());
					String tableAliasToUse = aliasMap == null || !aliasMap.containsKey(mdField.getUID()) ? tas.getAlias(field) : aliasMap.get(mdField.getUID());
					qualifiedName = DbColumnExpression.mkQualifiedColumnName(RigidUtils.defaultIfNull(extJoinAlias, tableAliasToUse), reference, false).toString();
				}				
				qualifiedNames.add(qualifiedName);
				
				PreparedStringBuilder sqlColumn = null;
				final DbGenericType dgt = DbUtils.getDbColumnType(mdField).getGenericType();
				if (dgt == DbGenericType.DATE || dgt == DbGenericType.DATETIME) {
					sqlColumn = new PreparedStringBuilder(dbAccess.getSqlForSubstituteNull(dbAccess.getSqlForCastAsString(qualifiedName, DbUtils.getDbColumnType(mdField)), "''"));
				} else {
					final DbColumnType type = DbUtils.getDbColumnType(mdField).getGenericType() != DbGenericType.VARCHAR 
							? new DbColumnType(DbGenericType.VARCHAR, 255) : DbUtils.getDbColumnType(mdField);
					sqlColumn = new PreparedStringBuilder(dbAccess.getSqlForSubstituteNull(dbAccess.getSqlForCast(qualifiedName, type), "''"));
				}
				
				if (mdField.getForeignEntity() != null || mdField.getUnreferencedForeignEntity() != null) {
					String tableAliasToUse = aliasMap == null || !aliasMap.containsKey(mdField.getUID()) ? tas.getAlias(field) : aliasMap.get(mdField.getUID());
					String idSql = null;
					if (SF.OWNER.checkField(mdField.getEntity(), mdField.getUID())) {
						idSql = ColumnToFieldIdVOMapping.getOwnerIdColumn(mdField, tableAliasToUse, from.getQuery().getBuilder(), false).getSqlColumnExpr();
					}					
					sqlColumn = DbReferencedCompoundColumnExpression.mkConcat(sqlColumn, tableAliasToUse, mdField, idSql);
				}
				toConcat.add(sqlColumn.toString());
			}
		}
		if (qualifiedNames.size() == 1) {
			return new PreparedStringBuilder(qualifiedNames.get(0));
		}
		else {
			if (toConcat.isEmpty() && isInheritReference) {
				return null;
			}
			return new PreparedStringBuilder(dbAccess.getSqlForConcat(toConcat));
		}
	}


}
