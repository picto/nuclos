//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dal.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.NucletFieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.dal.DalUtils;
import org.nuclos.server.dal.processor.ProcessorFactorySingleton;
import org.nuclos.server.dal.processor.jdbc.impl.ChartMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DataLanguageMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicMetaDataProcessor;
import org.nuclos.server.dal.processor.jdbc.impl.DynamicTasklistMetaDataProcessor;
import org.nuclos.server.dal.processor.nuclet.IEOGenericObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityLafParameterProcessor;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.IWorkspaceProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityFieldMetaDataProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityMetaDataProcessor;

/**
 * TODO Replace with pure Spring solution.
 */
public class NucletDalProvider {
	
	/**
	 * Singleton der auch in einer MultiThreading-Umgebung Threadsafe ist...
	 */
	private static NucletDalProvider INSTANCE;
	
	
	// instance variables
	
	private final Map<UID, IEntityObjectProcessor<?>> mapEntityObject = new HashMap<UID, IEntityObjectProcessor<?>>();
	
	// Spring injection
	
	private JdbcEntityMetaDataProcessor entityMetaDataProcessor;
	private JdbcEntityFieldMetaDataProcessor entityFieldMetaDataProcessor;
	private IEntityLafParameterProcessor entityLafParameterProcessor;
	private IEOGenericObjectProcessor eoGenericObjectProcessor;
	private IWorkspaceProcessor workspaceProcessor;
	private DynamicMetaDataProcessor dynMetaDataProcessor;
	private ChartMetaDataProcessor crtMetaDataProcessor;
	private DynamicTasklistMetaDataProcessor dynTasklistMetaDataProcessor;
	private ProcessorFactorySingleton processorFac;
	private DataLanguageMetaDataProcessor dataLangMetaDataProcessor;
	
	
	// end of Spring injection
	
	private ThreadLocal<Stack<Set<UID>>> accessibleMandators = new ThreadLocal<Stack<Set<UID>>>() {
		@Override
		protected Stack<Set<UID>> initialValue() {
			return new Stack<Set<UID>>();
		}
	};
	
	private ThreadLocal<Stack<UID>> accessibleMandatorsOrigin = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};
	
	private ThreadLocal<Stack<UID>> runningJob = new ThreadLocal<Stack<UID>>() {
		@Override
		protected Stack<UID> initialValue() {
			return new Stack<UID>();
		}
	};
	
	NucletDalProvider() {
		INSTANCE = this;
	}
	
	/**
	 * @deprecated Use Spring injection instead.
	 */
	public static NucletDalProvider getInstance() {
		if (INSTANCE == null || INSTANCE.eoGenericObjectProcessor == null || INSTANCE.mapEntityObject.isEmpty()) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}	
	
	/**
	 * Spring property.
	 */
	public void setEntityMetaDataProcessor(JdbcEntityMetaDataProcessor processor) {
		this.entityMetaDataProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setEntityFieldMetaDataProcessor(JdbcEntityFieldMetaDataProcessor processor) {
		this.entityFieldMetaDataProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setEntityLafParameterProcessor(IEntityLafParameterProcessor processor) {
		this.entityLafParameterProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setGenericObjectProcessor(IEOGenericObjectProcessor processor) {
		this.eoGenericObjectProcessor = processor;
	}	
	
	/**
	 * Spring property.
	 */
	public void setDataLanguageMetaDataProcessor(DataLanguageMetaDataProcessor processor) {
		this.dataLangMetaDataProcessor = processor;
	}	
	
	/**
	 * Spring property.
	 */
	public void setWorkspaceProcessor(IWorkspaceProcessor processor) {
		this.workspaceProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setDynamicMetaDataProcessor(DynamicMetaDataProcessor processor) {
		this.dynMetaDataProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setChartMetaDataProcessor(ChartMetaDataProcessor processor) {
		this.crtMetaDataProcessor = processor;
	}

	/**
	 * Spring property.
	 */
	public void setDynamicTasklistMetaDataProcessor(DynamicTasklistMetaDataProcessor processor) {
		this.dynTasklistMetaDataProcessor = processor;
	}
	
	/**
	 * Spring property.
	 */
	public void setProcessorFactorySingleton(ProcessorFactorySingleton processorFac) {
		this.processorFac = processorFac;
	}

	/**
	 * Rebuilds all EO processors.
	 *
	 * That's not always necessary: If only a single entity was changed via the entity wizard,
	 * only the corresponding processor needs to be rebuilt.
	 * TODO: Make it possible to rebuild only one certain processor.
	 */
	public void buildEOProcessors() {
		synchronized (mapEntityObject) {
			mapEntityObject.clear();
			try {
				// Constructor<?> entityObjectProcessorConstructor 
				// 		= LangUtils.getClassLoaderThatWorksForWebStart().loadClass(getDalProperties().getProperty("entity.object.nuclet"))
				//			.getConstructor(EntityMetaDataVO.class, Collection.class);
				
				Map<UID, EntityMeta<?>> mpAllEntitiesByUID = new HashMap<UID, EntityMeta<?>>();
				
				/**
				 * Systementitäten
				 */
				for (EntityMeta<?> eMeta : E.getAllEntities()) {
					Collection<FieldMeta<?>> entityFields = eMeta.getFields();
					mpAllEntitiesByUID.put(eMeta.getUID(), eMeta);
					mapEntityObject.put(eMeta.getUID(), (IEntityObjectProcessor<?>) processorFac.newEntityObjectProcessor(eMeta, entityFields, true));
				}
				
				/**
				 * Konfigurierte Entitäten
				 */
				List<NucletEntityMeta> allEntities = entityMetaDataProcessor.getAll();
				List<NucletFieldMeta<?>> allFields = entityFieldMetaDataProcessor.getAll();
				MultiListMap<UID, FieldMeta<?>> fieldMetaMap = new MultiListHashMap<UID, FieldMeta<?>>(allEntities.size()); // for performance
				for (NucletFieldMeta<?> fieldMeta : allFields) {
					fieldMetaMap.addValue(fieldMeta.getEntity(), fieldMeta);
				}
				for (NucletEntityMeta eMeta : allEntities) {
					if (eMeta.isGeneric()) {
						continue;
					}
//					Collection<FieldMeta<?>> entityFields = NucletFieldMeta.toFieldMetas(entityFieldMetaDataProcessor.getByParent(eMeta.getUID()));
					Collection<FieldMeta<?>> entityFields = new ArrayList<FieldMeta<?>>(fieldMetaMap.getValues(eMeta.getUID()));
					DalUtils.addStaticFields(entityFields, eMeta);
					entityFields.remove(SF.STATEICON.getMetaData(eMeta)); // will be added at client. performance issues...
					mapEntityObject.put(eMeta.getUID(), processorFac.newEntityObjectProcessor(eMeta, entityFields, true));
					eMeta.setFields(entityFields);
					mpAllEntitiesByUID.put(eMeta.getUID(), eMeta);
				}
				
				/**
				 * Data Language Entities
				 */
				for (NucletEntityMeta eMeta : dataLangMetaDataProcessor.getAll(mpAllEntitiesByUID)) {
					Collection<FieldMeta<?>> entityFields = eMeta.getFields();
					mapEntityObject.put(eMeta.getUID(), processorFac.newEntityObjectProcessor(eMeta, entityFields, true));
				}
				
				/**
				 * 
				 */
				for(EntityMeta<?> eMeta : getDynamicEntityMetaProcessor().getAll()) {
					Collection<FieldMeta<?>> entityFields = eMeta.getFields();
					
					mapEntityObject.put(eMeta.getUID(), processorFac.newDynamicEntityObjectProcessor(eMeta, entityFields));
				}
				
				/**
				 * 
				 */
				for(EntityMeta<?> eMeta : getChartEntityMetaProcessor().getAll()) {
					Collection<FieldMeta<?>> entityFields = eMeta.getFields();
					
					mapEntityObject.put(eMeta.getUID(), processorFac.newChartEntityObjectProcessor(eMeta, entityFields));
				}

				/**
				 *
				 */
				for(EntityMeta<?> eMeta : getDynamicTasklistEntityMetaProcessor().getAll()) {
					Collection<FieldMeta<?>> entityFields = eMeta.getFields();

					mapEntityObject.put(eMeta.getUID(), processorFac.newDynamicTasklistEntityObjectProcessor(eMeta, entityFields));
				}
	
				
			} catch (Exception ex) {
				throw new CommonFatalException(ex);
			}
		}
	}
	
	public JdbcEntityMetaDataProcessor getEntityMetaDataProcessor() {
		return entityMetaDataProcessor;
	}
	
	public JdbcEntityFieldMetaDataProcessor getEntityFieldMetaDataProcessor() {
		return entityFieldMetaDataProcessor;
	}
	
	public IEntityLafParameterProcessor getEntityLafParameterProcessor() {
		return entityLafParameterProcessor;
	}
	
	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(EntityMeta<PK> entity) {
		return (IEntityObjectProcessor<PK>) getEntityObjectProcessor(entity.getUID());
	}
	
	public <PK> IEntityObjectProcessor<PK> getEntityObjectProcessor(UID entity) {
		IEntityObjectProcessor<PK> proc;
		synchronized (mapEntityObject) {
			proc = (IEntityObjectProcessor<PK>) mapEntityObject.get(entity);
		}
		if (proc == null) {
			throw new CommonFatalException("No processor for entity " + entity + " registered.");
		}
		return proc;
	}
	
	public IEOGenericObjectProcessor getEOGenericObjectProcessor() {
		return eoGenericObjectProcessor;
	}
	
	public IWorkspaceProcessor getWorkspaceProcessor() {
		return workspaceProcessor;
	}
	
	public DynamicMetaDataProcessor getDynamicEntityMetaProcessor() {
		if (dynMetaDataProcessor == null) {
			// dynMetaDataProcessor = new DynamicMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
	    return dynMetaDataProcessor;
    }

	public ChartMetaDataProcessor getChartEntityMetaProcessor() {
		if (crtMetaDataProcessor == null) {
			// crtMetaDataProcessor = new ChartMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
	    return crtMetaDataProcessor;
    }

	public DynamicTasklistMetaDataProcessor getDynamicTasklistEntityMetaProcessor() {
		if (dynTasklistMetaDataProcessor == null) {
			// dynMetaDataProcessor = new DynamicMetaDataProcessor();
			throw new IllegalStateException("too early");
		}
		return dynTasklistMetaDataProcessor;
	}

	public void revalidate() {
		buildEOProcessors();
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public Set<UID> getAccessibleMandators() {
		try {
			return accessibleMandators.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public DataLanguageMetaDataProcessor getDataLanguageMetaDataProcessor() {
		return this.dataLangMetaDataProcessor;
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getAccessibleMandatorsOrigin() {
		try {
			return accessibleMandatorsOrigin.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isAccessibleMandatorsSet() {
		try {
			return accessibleMandators.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param mandators
	 * @param mandatorOrigin
	 */
	public void setAccessibleMandators(Set<UID> mandators, UID mandatorOrigin) {
		accessibleMandators.get().push(mandators);
		accessibleMandatorsOrigin.get().push(mandatorOrigin);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeAccessibleMandators() {
		accessibleMandators.get().pop();
		accessibleMandatorsOrigin.get().pop();
	}
	
	/**
	 * only available during rule execution
	 * @return 
	 */
	public UID getRunningJob() {
		try {
			return runningJob.get().peek();
		} catch (EmptyStackException ese) {
			return null;
		}
	}
	
	public boolean isRunningJobSet() {
		try {
			return runningJob.get().peek() != null;
		} catch (EmptyStackException ese) {
			return false;
		}
	}
	
	/** 
	 * set from rule engine only
	 * @param mandators
	 * @param mandatorOrigin
	 */
	public void setRunningJob(UID job) {
		runningJob.get().push(job);
	}
	
	/**
	 * called from rule engine only
	 */
	public void removeRunningJob() {
		runningJob.get().pop();
	}
}
