//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;

import org.apache.commons.collections15.map.HashedMap;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.ObjectUtils;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.AttributeProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.MandatorVO;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ModuleConstants;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.common.ejb3.NuclosFacadeBean;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.nuclos.server.genericobject.GeneratorFailedException;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.genericobject.valueobject.GeneratorVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for all generic object generator functions. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 */
@Transactional(noRollbackFor= {Exception.class})
public class GeneratorFacadeBean extends NuclosFacadeBean implements GeneratorFacadeRemote {
	
	@Autowired
	private GenericObjectFacadeLocal genericObjectFacade;
	
	@Autowired
	private GeneratorObjectBuilder generatorObjectBuilder;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacade;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	public GeneratorFacadeBean() {
	}

	@PostConstruct
	@RolesAllowed("Login")
	public void postConstruct() {}

	/**
	 * @return all generator actions allowed for the current user.
	 */
	@RolesAllowed("Login")
	public GeneratorVO getGeneratorActions() throws CommonPermissionException {
		List<GeneratorActionVO> actions = new LinkedList<GeneratorActionVO>();
		boolean bIsSuperUser = securityCache.isSuperUser(getCurrentUserName());
		final UID currentMandatorUID = getCurrentMandatorUID();
		
		Collection<UID> assignedGenerations = securityCache.getAssignedGenerations(getCurrentUserName(), currentMandatorUID);		
		Collection<MasterDataVO<UID>> mdGenerationsVO = masterDataFacade.getMasterData(E.GENERATION, null, true);
		
		for (MasterDataVO<UID> mdVO : mdGenerationsVO) {
			if (bIsSuperUser || assignedGenerations.contains(mdVO.getPrimaryKey())) {
				boolean add = true;
				if (securityCache.isMandatorPresent() && currentMandatorUID != null) {
					final UID sourceMandatorUID = mdVO.getFieldUid(E.GENERATION.sourceMandator);
					final UID targetMandatorUID = mdVO.getFieldUid(E.GENERATION.targetMandator);
					final Set<UID> accessibleMandators = securityCache.getAccessibleMandators(currentMandatorUID);
					if (sourceMandatorUID != null && !accessibleMandators.contains(sourceMandatorUID)) {
						add = false;
					}
					if (targetMandatorUID != null && !accessibleMandators.contains(targetMandatorUID)) {
						add = false;
					}
				}
				if (add) {
					actions.add(MasterDataWrapper.getGeneratorActionVO(mdVO, getGeneratorUsages(mdVO.getPrimaryKey())));
				}
			}
		}
		
		return new GeneratorVO(actions);
	}

	/**
	 * @return object grouped by grouping function within generation
	 */
	@RolesAllowed("Login")
	public <PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjects(Collection<PK> sourceIds, GeneratorActionVO generatoractionvo) throws CommonPermissionException {
		final EntityMeta<?> meta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		
		checkReadAllowed(meta);
		
		if (isCalledRemotely() && meta.isStateModel()) {
			for (PK sourceId : sourceIds) {
				checkReadAllowedForModule(meta.getUID(), (Long)sourceId);
			}
		}
		
		Collection<EntityObjectVO<PK>> objects = CollectionUtils.transform(sourceIds, new Transformer<PK, EntityObjectVO<PK>>() {
			@Override
			public EntityObjectVO<PK> transform(PK i) {
				return nucletDalProvider.<PK>getEntityObjectProcessor(meta.getUID()).getByPrimaryKey(i);
			}
		});
		
		return groupGenerationSources(objects, generatoractionvo);
	}

	@RolesAllowed("Login")
	public <PK> GenerationResult<PK> generateGenericObjects(Collection<EntityObjectVO<PK>> sourceObjects,
			Long parameterObjectId, GeneratorActionVO generatoractionvo, final String customUsage, boolean bIsCloningAction)
			throws CommonBusinessException {
				
		if (isCalledRemotely()) {
			//1. Check if user can write into the target entity.
			final EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());
			
			checkWriteAllowed(targetMeta);
			
			//2. Check if user is allowed to use this generation at all.
			Collection<UID> assignedGenerations = securityCache.getAssignedGenerations(getCurrentUserName(), getCurrentMandatorUID());
			if (!assignedGenerations.contains(generatoractionvo.getId())) {
				throw new CommonPermissionException("Generation '" + generatoractionvo + "' not allowed for User " + getCurrentUserName());
			}

			final EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
			
			if (sourceMeta.isStateModel()) {
				//3. Check if state and process fits the assigned usage of this generation
				Collection<GeneratorUsageVO> generatorUsages = getGeneratorUsages(generatoractionvo.getId());
				if (!generatorUsages.isEmpty()) {
					
					for (EntityObjectVO<PK> eo : sourceObjects) {
						GeneratorUsageVO usage2Test = new GeneratorUsageVO(eo.getFieldUid(SF.STATE_UID), eo.getFieldUid(SF.PROCESS_UID));
						boolean usageOkay = false;
						for (GeneratorUsageVO usageVO : generatorUsages) {
							if (usageVO.test(usage2Test)) {
								usageOkay = true;
								break;
							}
						}
						if (!usageOkay) {
							throw new CommonPermissionException("Generation '" + generatoractionvo + "' not allowed for this usage: " + usage2Test);
						}
					}
					
				}
			}
		}
		
		return generateGenericObjectsImpl(sourceObjects, parameterObjectId, generatoractionvo, customUsage, bIsCloningAction);	
	}

	/**
	 * generate one or more generic objects from an existing generic object
	 * (copying selected attributes and subforms)
	 * 
	 * §nucleus.permission mayWrite(generatoractionvo.getTargetModuleId())
	 *
	 * @param iSourceObjectId
	 *            source generic object id to generate from
	 * @param generatoractionvo
	 *            generator action value object to determine what to do
	 * @return id of generated generic object (if exactly one object was
	 *         generated)
	 * @throws NuclosBusinessRuleException 
	 */
	@RolesAllowed("Login")
	public GenerationResult generateGenericObject(Long iSourceObjectId, Long parameterObjectId, GeneratorActionVO generatoractionvo, String customUsage) 
			throws CommonBusinessException {
		
		EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());

		this.checkWriteAllowed(targetMeta);

		// We need generic objects, so the entity must be of type PK == Long
		if (sourceMeta.isUidEntity())
			throw new IllegalArgumentException("SourceModule is an UID entity. Only entities of type Long are allowed.");
		
		IEntityObjectProcessor<Long> proc = (IEntityObjectProcessor<Long>) nucletDalProvider.getEntityObjectProcessor(sourceMeta);
		EntityObjectVO<Long> eo = proc.getByPrimaryKey(iSourceObjectId);
		
		return generateGenericObjectsImpl(Collections.singletonList(eo), parameterObjectId, generatoractionvo, customUsage, false);
	}

	/**
	 * Generate a single new object w.r.t. the object generation definition.
	 * <p>
	 * If the generator action is configured with attribute grouping, the 
	 * grouping has to be done in advance.
	 * <p>
	 * The general problem with this method is that we need the generated object even if 
	 * the generations fails! This is because the client GUI needs to display the 
	 * (unfinished) object for the user the find out what has failed.
	 * <p>
	 * We have a long history of problems reports (e.g. http://support.nuclos.de/browse/NUCLOS-3311)
	 * due to the fact that is PLAIN WRONG to throw an exception and bail out here.
	 * 
	 * @author Thomas Pasch (javadoc)
	 * @throws NuclosBusinessRuleException 
	 */
	private <PK> GenerationResult<PK> generateGenericObjectsImpl(Collection<EntityObjectVO<PK>> sourceObjects,
			Long parameterObjectId, GeneratorActionVO generatoractionvo, final String customUsage, boolean bIsCloningAction)
			throws CommonBusinessException {

		final EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		final EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());

		// Create a template for the target object (from single or multiple objects)
		final int sourceSize = sourceObjects.size();
		EntityObjectVO<PK> target;
		if (sourceSize > 1) {
			target = getNewObject(sourceObjects, generatoractionvo);
		} else if (sourceSize == 1) {
			final EntityObjectVO<PK> source = sourceObjects.iterator().next();
			target = getNewObject(source, generatoractionvo);

			// Create target origin
			if (targetMeta.isStateModel() && sourceMeta.isStateModel()) {
				target.setFieldValue(SF.ORIGIN, source.getFieldValue(SF.SYSTEMIDENTIFIER));
			}

			// link target with source object if possible
			if (generatoractionvo.isCreateRelationBetweenObjects()) {
				UID targetAttribute = getTargetFieldIdIfAny(generatoractionvo, generatoractionvo.getSourceModule());
				if (targetAttribute != null) {
					if (source.getPrimaryKey() instanceof UID) {
						target.setFieldUid(targetAttribute, (UID)source.getPrimaryKey());						
					} else {
						target.setFieldId(targetAttribute, (Long)source.getPrimaryKey());						
					}
					
					// NUCLOS-3311
					// find ref field
					FieldMeta<?> relationFieldMeta = null;
					for (FieldMeta<?> mf: targetMeta.getFields()) {
						if (sourceMeta.getUID().equals(mf.getForeignEntity())) {
							relationFieldMeta = mf;
							break;
						}
					}
					if (relationFieldMeta != null) {
						final String value = RefValueExtractor.get(source, relationFieldMeta.getUID(), null);
						target.setFieldValue(targetAttribute, value);
					}
				}
			}
		} else {
			throw new NuclosFatalException("No source objects for generation");
		}

		// Create target process
		if (targetMeta.isStateModel() && generatoractionvo.getTargetProcess() != null) {
			final String sTargetProcess = (String)
					masterDataFacade.get(E.PROCESS, generatoractionvo.getTargetProcess()).getFieldValue(E.PROCESS.name);
			target.setFieldValue(SF.PROCESS, sTargetProcess);
			target.setFieldUid(SF.PROCESS_UID, generatoractionvo.getTargetProcess());
		}
		
		Collection<UsageCriteria> sourceUsages = new ArrayList<UsageCriteria>();
		for (EntityObjectVO<PK> sourceObject : sourceObjects) {
			sourceUsages.add(UsageCriteria.createUsageCriteriaFromEO(sourceObject, customUsage));
		}
		final UsageCriteria sourceUsage = UsageCriteria.getGreatestCommonUsageCriteria(sourceUsages); 
		final UsageCriteria targetUsage = UsageCriteria.createUsageCriteriaFromEO(target, customUsage);

		// Load parameter object (if available)
		final UID parameterEntityUID;
		final EntityObjectVO<?> parameterObject;
		if (generatoractionvo.getParameterEntity() != null) {
			if (parameterObjectId == null) {
				// TODO: exception that parameter object is missing
				throw new NuclosFatalException("Missing parameter object");
			}

			parameterEntityUID = generatoractionvo.getParameterEntity();
			parameterObject = nucletDalProvider.getEntityObjectProcessor(parameterEntityUID).getByPrimaryKey(
					parameterObjectId);

			if (generatoractionvo.isCreateRelationToParameterObject()) {
				UID targetAttribute = getTargetFieldIdIfAny(generatoractionvo, generatoractionvo.getParameterEntity());
				if (targetAttribute != null) {
					target.setFieldId(targetAttribute, parameterObjectId);
					// NUCLOS-2559: create stringified value
					target.setFieldValue(targetAttribute,
							RefValueExtractor.get(parameterObject, targetAttribute, null, metaProvider));
				}
			}
		} else {
			parameterEntityUID = null;
			parameterObject = null;
		}

		// Copy parameter attributes
		if (parameterObject != null) {
			copyParameterAttributes(parameterObject, parameterEntityUID, target, generatoractionvo);
		}

		// add dependants
		final IDependentDataMap dependants = new DependentDataMap();
		final Collection<EntityObjectVO<UID>> subentities =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONSUBENTITY.generation,
						generatoractionvo.getId());

		// copy dependants (grouping == false)
		for (EntityObjectVO<UID> subentity : subentities) {
			if (LangUtils.defaultIfNull((Boolean) subentity.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes),
					Boolean.FALSE)) {
				continue;
			}
			copyDependants(sourceObjects, parameterObject, generatoractionvo, subentity, target, dependants,
					customUsage);
		}

		// aggregate dependants
		for (EntityObjectVO<UID> subentity : subentities) {
			if (!LangUtils.defaultIfNull((Boolean) subentity.getFieldValue(E.GENERATIONSUBENTITY.groupAttributes),
					Boolean.FALSE)) {
				continue;
			}
			Collection<PK> sourceObjectIds = CollectionUtils.transform(sourceObjects,
					new Transformer<EntityObjectVO<PK>, PK>() {
						@Override
						public PK transform(EntityObjectVO<PK> i) {
							return i.getPrimaryKey();
						}
					});
			aggregateDependants(sourceObjectIds, sourceUsage, generatoractionvo, subentity, target, targetUsage, dependants);
		}
		
		// restore current mandator later
		final UID mandatorInContext = getCurrentMandatorUID();
		
		if (targetMeta.isMandator()) {
			UID targetMandator = generatoractionvo.getTargetMandator();
			if (targetMandator != null) {
				// change current mandator in context if generator is mandator specific
				setCurrentMandatorUID(targetMandator);
			}
			if (targetMandator == null) {
				if (sourceMeta.isMandator()) {
					final Set<UID> sourceMandators = new HashSet<UID>();
					for (EntityObjectVO<?> source : sourceObjects) {
						sourceMandators.add(source.getFieldUid(SF.MANDATOR_UID));
					}
					if (targetMeta.getMandatorLevel().equals(sourceMeta.getMandatorLevel())) {
						if (sourceMandators.size() == 1) {
							targetMandator = sourceMandators.iterator().next();
						}
					} else if (!sourceMandators.isEmpty()) {
						// Gemeinsamen Nenner finden (vielleicht sind die sourceMandators alle in einer niedrigeren Ebene, und es gibt einen gemeinsamen Mandanten oberhalb)
						Set<UID> accessibleMandators = new HashSet<UID>();
						for (UID sourceMandator : sourceMandators) {
							accessibleMandators.addAll(SecurityCache.getInstance().getAccessibleMandators(sourceMandator));
						}
						// Nach Target Ebene filtern
						Iterator<UID> itAccMan = accessibleMandators.iterator();
						while (itAccMan.hasNext()) {
							MandatorVO mandatorVO = SecurityCache.getInstance().getMandator(itAccMan.next()); 
							if (!LangUtils.equal(mandatorVO.getLevelUID(), targetMeta.getMandatorLevel())) {
								itAccMan.remove();
							}
						}
						if (accessibleMandators.size() == 1) {
							targetMandator = accessibleMandators.iterator().next();
						}
					}
				}
			}
			if (targetMandator != null) {
				target.setFieldUid(SF.MANDATOR_UID, targetMandator);
				target.setFieldValue(SF.MANDATOR, SecurityCache.getInstance().getMandator(targetMandator).getName());
			}
		}

		try {
			if (targetMeta.isStateModel()) {
				//TODO: Generics, assuming that Generic Objects will always have Long ids.
				return createGeneratedGO(targetMeta, (EntityObjectVO<Long>)target, sourceMeta, (Collection<EntityObjectVO<Long>>)(Object)sourceObjects, dependants, parameterObject,
						generatoractionvo, customUsage, bIsCloningAction);
			} else {
				return createGeneratedMD(targetMeta, target, sourceObjects, parameterObject, generatoractionvo,
						customUsage, bIsCloningAction);
			}
		} catch (GeneratorFailedException gfex) {
			EntityObjectVO<?> eo = gfex.getGenerationResult().getGeneratedObject();
			fillInMissingRefValues(eo);
			throw gfex;
		} finally {
			setCurrentMandatorUID(mandatorInContext);
		}
	}
	
	private GenerationResult createGeneratedGO(EntityMeta<?> targetMeta, EntityObjectVO<Long> target,
			EntityMeta<?> sourceMeta, Collection<EntityObjectVO<Long>> sourceObjects, IDependentDataMap dependants,
			EntityObjectVO<?> parameterObject, GeneratorActionVO generatoractionvo, String customUsage,
			boolean bIsCloningAction) throws GeneratorFailedException, NuclosBusinessRuleException {

		target.setFieldValue(SF.LOGICALDELETED, Boolean.FALSE);
		final GenericObjectVO go = DalSupportForGO.getGenericObjectVO(target);

		final GenericObjectVO result =
				new GenericObjectVO(generatoractionvo.getTargetModule(), GenericObjectMetaDataCache.getInstance());

		final Set<UID> targetAttributes =
				metaProvider.getAllEntityFieldsByEntity(targetMeta.getUID()).keySet();

		for (UID targetAttribute : targetAttributes) {
			DynamicAttributeVO davo = go.getAttribute(targetAttribute);
			if (davo != null) {
				result.setAttribute(new DynamicAttributeVO(targetAttribute, davo.getValueId(), davo.getValueUid(),
						davo.getValue()));
			}
		}

		GenericObjectWithDependantsVO targetGODep = new GenericObjectWithDependantsVO(result, dependants, result.getDataLanguageMap());
		// Create the new object
		EntityObjectVO<Long> executeGenerationEventSupports = DalSupportForGO.wrapGenericObjectVO(targetGODep);
		try {
			// execute rules (before)
			final List<String> lstActions = new ArrayList<String>();
			executeGenerationEventSupports = executeGenerationEventSupports(generatoractionvo,
							executeGenerationEventSupports, sourceObjects, parameterObject,
							lstActions, false);
			targetGODep = DalSupportForGO.getGenericObjectWithDependants(executeGenerationEventSupports);

			if (!bIsCloningAction) {
				GenericObjectVO created = genericObjectFacade.create(targetGODep, customUsage, false);
				performDeferredActionsFromRules(lstActions, created.getId(), genericObjectFacade);
				if (sourceMeta.isStateModel() && generatoractionvo.isCreateRelationBetweenObjects()) {
					for (EntityObjectVO<Long> source : sourceObjects) {
						relateCreatedGenericObjectToParent(source.getPrimaryKey(), genericObjectFacade,
								generatoractionvo, created.getId());
					}
				}
				// execute rules (after)
				executeGenerationEventSupports = DalSupportForGO.wrapGenericObjectVO(created);
				executeGenerationEventSupports = executeGenerationEventSupports(
						generatoractionvo, executeGenerationEventSupports, sourceObjects,
						parameterObject, lstActions, true);

				return new GenerationResult(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						DalSupportForGO.wrapGenericObjectVO(genericObjectFacade.get(created.getId())), null);
			} else {
				return new GenerationResult(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer()),
						DalSupportForGO.wrapGenericObjectVO(targetGODep), null);
			}
		} catch (NuclosBusinessRuleException e) {
			// needed for http://project.nuclos.de/browse/LIN-116
			final BusinessException cause = e.getCausingBusinessException();
			final EntityObjectVO<Long> temp = executeGenerationEventSupports;
			temp.setPrimaryKey(null);
			if (cause != null) {
				final GenerationResult gr = new GenerationResult(CollectionUtils.transform(
						sourceObjects, new ExtractIdTransformer()), temp, cause.getMessage());
				throw new GeneratorFailedException("Generation of generic object failed: " + temp
						+ " action: " + generatoractionvo, gr, cause);
			} else {
				final GenerationResult gr = new GenerationResult(CollectionUtils.transform(
						sourceObjects, new ExtractIdTransformer()), temp, e.getMessage());
				throw new GeneratorFailedException("Generation of generic object failed: " + temp
						+ " action: " + generatoractionvo, gr, e);
			}
		} catch (CommonBusinessException ex) {
			final EntityObjectVO<Long> temp = executeGenerationEventSupports;
			temp.setPrimaryKey(null);
			final GenerationResult gr = new GenerationResult(CollectionUtils.transform(
					sourceObjects, new ExtractIdTransformer()), temp, ex.getMessage());
			throw new GeneratorFailedException("Generation of generic object failed: " + temp
					+ " action: " + generatoractionvo, gr, ex);
		}
	}
	
	private <PK> GenerationResult<PK> createGeneratedMD(EntityMeta<?> targetMeta, EntityObjectVO<PK> target,
			Collection<EntityObjectVO<PK>> sourceObjects, EntityObjectVO<?> parameterObject,
			GeneratorActionVO generatoractionvo, String customUsage,
			boolean bIsCloningAction) throws GeneratorFailedException, NuclosBusinessRuleException {
		
		MasterDataVO<PK> md = new MasterDataVO<PK>(target);
		// Create the new object
		final UID uid = targetMeta.getUID();
		try {
			// execute rules (before)
			final List<String> lstActions = new ArrayList<String>();
			EntityObjectVO<PK> executeGenerationEventSupports = executeGenerationEventSupports(
					generatoractionvo, target, sourceObjects, parameterObject, lstActions, false);
			md = new MasterDataVO<PK>(executeGenerationEventSupports);

			if (!bIsCloningAction) {
				target = masterDataFacade.create(md, customUsage).getEntityObject();

				// execute rules (after)
				executeGenerationEventSupports(generatoractionvo, target, sourceObjects, parameterObject, new ArrayList<String>(), true);

				return new GenerationResult<PK>(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer<PK>()),
						masterDataFacade.get(uid, target.getPrimaryKey()).getEntityObject(), null);
			} else {
				return new GenerationResult<PK>(CollectionUtils.transform(sourceObjects, new ExtractIdTransformer<PK>()),
						md.getEntityObject(), null);
			}
		} catch (NuclosBusinessRuleException e) {
			// needed for http://project.nuclos.de/browse/LIN-116
			final BusinessException cause = e.getCausingBusinessException() != null ? e.getCausingBusinessException() : new BusinessException(e);
			md.setPrimaryKey(null);
			EntityObjectVO<PK> temp = md.getEntityObject();
			temp.setDependents(md.getDependents());
			final GenerationResult<PK> gr = new GenerationResult<PK>(CollectionUtils.transform(
					sourceObjects, new ExtractIdTransformer<PK>()), temp, cause.getMessage());
			throw new GeneratorFailedException("Generation of MD object failed: " + temp
					+ " action: " + generatoractionvo, gr, cause);
		} catch (CommonBusinessException ex) {
			// this is required, because the id is already generated
			md.setPrimaryKey(null);
			EntityObjectVO<PK> temp = md.getEntityObject();
			temp.setDependents(md.getDependents());
			final GenerationResult<PK> gr = new GenerationResult<PK>(CollectionUtils.transform(
					sourceObjects, new ExtractIdTransformer<PK>()), temp, ex.getMessage());
			throw new GeneratorFailedException("Generation of MD object failed: " + temp
					+ " action: " + generatoractionvo, gr, ex);
		}
		
	}
	
	private <PK> EntityObjectVO<PK> executeGenerationEventSupports(GeneratorActionVO generatoractionvo, 
			EntityObjectVO<PK> loccvoTargetBeforeRules, Collection<EntityObjectVO<PK>> loccvoSourceObjects, 
			EntityObjectVO<?> loccvoParameter, List<String> lstActions, boolean after) 
			throws NuclosBusinessRuleException, NuclosCompileException, CommonPermissionException {
		
		final EventSupportFacadeLocal facade = SpringApplicationContextHolder.getBean(EventSupportFacadeLocal.class);
		return facade.fireGenerationEventSupport(generatoractionvo.getId(), loccvoTargetBeforeRules, 
				loccvoSourceObjects, loccvoParameter, lstActions, generatoractionvo.getProperties(), after);
	}

	private UID getTargetFieldIdIfAny(GeneratorActionVO generatoractionvo, UID sourceEntityUid) {
		final UID iTargetModuleId = generatoractionvo.getTargetModule();
		final Map<UID, FieldMeta<?>> mp = metaProvider.getAllEntityFieldsByEntity(iTargetModuleId);
		for (UID sFieldName : mp.keySet()) {
			FieldMeta<?> voField = mp.get(sFieldName);
			if (sourceEntityUid.equals(voField.getForeignEntity())) {
				return voField.getUID();
			}
		}
		return null;
	}

	/**
	 * Relate the new target object to the source object. If source object is an
	 * invoice section relate the new object to the invoice:
	 *
	 * @param sourceId
	 * @param lofacade
	 * @param generatoractionvo
	 * @param targetGenericObjectId
	 * @throws CommonFinderException
	 */
	private void relateCreatedGenericObjectToParent(Long sourceId, GenericObjectFacadeLocal lofacade, GeneratorActionVO generatoractionvo, Long targetGenericObjectId) throws CommonFinderException, CommonPermissionException, NuclosBusinessRuleException {
		if (sourceId != null) {
			try {
				lofacade.relate(generatoractionvo.getTargetModule(), targetGenericObjectId, sourceId, GenericObjectTreeNode.SystemRelationType.PREDECESSOR_OF.getValue());
			} catch (CommonCreateException ex) {
				throw new NuclosFatalException(ex);
			}
		}
	}

	/**
	 * Executes a list of commands prepared by the rule interface for actions,
	 * which cannot be performed before object creation, e.g. object relation
	 * with the freshly created object.
	 *
	 * @param lstActionsFromRules
	 *            List<String> of commands from deferred methods
	 * @param iTargetGenericObjectId
	 *            id of the freshly created object (if any)
	 * @param lofacade
	 *            (for performance)
	 * @throws NuclosBusinessRuleException
	 * @throws CommonCreateException
	 * @throws CommonFinderException
	 */
	private void performDeferredActionsFromRules(List<String> lstActionsFromRules, Long iTargetGenericObjectId, GenericObjectFacadeLocal lofacade) throws NuclosBusinessRuleException, CommonCreateException, CommonFinderException, CommonPermissionException {

		for (String sAction : lstActionsFromRules) {
			final String[] asAction = sAction.split(":", 4);
			if ("relate".equals(asAction[0])) {
				if (asAction[1].equals(asAction[2])) {
					throw new NuclosBusinessRuleException("generator.facade.exception.1");// "Ein Objekt darf nicht mit sich selbst verkn\u00fcpft werden.");
				}
				final Long iTargetId = "this".equals(asAction[1]) ? iTargetGenericObjectId : Long.parseLong(asAction[1]);
				final UID iTargetModuleId = lofacade.getModuleContainingGenericObject(iTargetId);
				String relationType = asAction[3];
				lofacade.relate(iTargetModuleId, iTargetId, "this".equals(asAction[2]) ? iTargetGenericObjectId : Integer.parseInt(asAction[2]), relationType);
			}
		}
	}

	private <PK> Map<String, Collection<EntityObjectVO<PK>>> groupGenerationSources(Collection<EntityObjectVO<PK>> col, GeneratorActionVO generatoractionvo) {
		Set<UID> groupingAttributes = new HashSet<UID>();

		Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generatoractionvo.getId());
		
		for (EntityObjectVO<UID> attribute : attributes) {
			if (!StringUtils.isNullOrEmpty(attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType))) {
				continue;
			}
			String groupFunction = attribute.getFieldValue(E.GENERATIONATTRIBUTE.groupfunction);
			if (!StringUtils.isNullOrEmpty(groupFunction) && !"group by".equals(groupFunction)) {
				continue;
			}
			UID source = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
			if (E.isNuclosEntity(source)) {
				continue;
			}
			groupingAttributes.add(source);
		}

		Map<String, Collection<EntityObjectVO<PK>>> mp = new Hashtable<String, Collection<EntityObjectVO<PK>>>();

		for (EntityObjectVO<PK> eo : col) {
			StringBuffer sb = new StringBuffer();
			for (UID attribute : groupingAttributes) {
				Object valueToUse = null;
				// This UID can identify an UID, a Long or a simple value object
				if (eo.getFieldUid(attribute) != null) {
					valueToUse = eo.getFieldUid(attribute);
				}
				else if (eo.getFieldId(attribute) != null) {
					valueToUse = eo.getFieldId(attribute);
				}
				else if (eo.getFieldValue(attribute) != null) {
					valueToUse = eo.getFieldValue(attribute);
				}
			
				sb.append(valueToUse);
				sb.append(".");
			}
			final String key = sb.toString();
			if (!mp.containsKey(key)) {
				mp.put(key, new ArrayList<EntityObjectVO<PK>>());
				mp.get(key).add(eo);
			}
			else {
				mp.get(key).add(eo);
			}
		}

		return mp;
	}
	
	private <PK> EntityObjectVO<PK> getGroupedGeneratedObject(GeneratorActionVO generator, Collection<PK> sourceIds) {
		
		final UID sourceEntity =
				metaProvider.getEntity(generator.getSourceModule()).getUID();
		
		final UID targetEntity = 
				metaProvider.getEntity(generator.getTargetModule()).getUID();
		
		final Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generator.getId());

		EntityMeta<?> entity = metaProvider.getEntity(sourceEntity);
		// NUCLOSINT-1358
	
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<?> t = query.from(entity);

		List<DbSelection<?>> selection = new ArrayList<DbSelection<?>>();
		List<DbExpression<?>> groupby = new ArrayList<DbExpression<?>>();

		for (EntityObjectVO<UID> attribute : attributes) {
			String fieldSourceType = attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType);
			
		//	final String type = (String) attribute.getFieldValue("sourceType");
			// only process source entity attributes (skip parameter entity attributes)
			if (StringUtils.isNullOrEmpty(fieldSourceType)) {
				
				final String function = attribute.getFieldValue(E.GENERATIONATTRIBUTE.groupfunction);
				final UID source = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
				
				final FieldMeta<?> meta = metaProvider.getEntityField(source);
				final String column = meta.getDbColumn();
				DbColumnExpression<?> c;
				c = t.baseColumn(meta);

				//@see NUCLOS-1436
				final String sField = meta.getFieldName().equals("name") ? "\"" + meta.getFieldName() + "\"" : meta.getFieldName();
				if (function == null || "group by".equals(function)) {
					if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
						c = t.baseColumn(SimpleDbField.createRef(column, false));
					}
					c.alias(sField);
					selection.add(c);
					groupby.add(c);
				}
				else if ("summate".equals(function)) {
					selection.add(builder.sum(c).alias(sField));
				}
				else if ("minimum value".equals(function)) {
					selection.add(builder.min(c).alias(sField));
				}
				else if ("maximum value".equals(function)) {
					selection.add(builder.max(c).alias(sField));
				}
			}
		}

		query.multiselect(selection);
		if (groupby.size() > 0) {
			query.groupBy(groupby);
		}

		ArrayList<DbCondition> conditions = new ArrayList<DbCondition>();
		for (PK sourceId : sourceIds) {
			if (sourceId instanceof UID) {
				conditions.add(builder.equalValue(t.baseColumn(SF.PK_UID), (UID)sourceId));				
			} else {
				conditions.add(builder.equalValue(t.baseColumn(SF.PK_ID), (Long)sourceId));				
			}
		}
		query.where(builder.or(conditions.toArray(new DbCondition[conditions.size()])));

		DbTuple tuple = dataBaseHelper.getDbAccess().executeQuerySingleResult(query);

		final EntityObjectVO<PK> result = new EntityObjectVO<PK>(targetEntity);
		
		if (metaProvider.getEntity(targetEntity).isStateModel()) {
			result.setFieldValue(SF.LOGICALDELETED, Boolean.FALSE);
		}

		for (EntityObjectVO<UID> attribute : attributes) {
			final String type = attribute.getFieldValue(E.GENERATIONATTRIBUTE.sourceType);
			if (!StringUtils.isNullOrEmpty(type)) {
				continue;
			}
			final UID source = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
			final UID target = attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeTarget);

			//@see NUCLOS-1436
			FieldMeta<?> meta = metaProvider.getEntityField(attribute.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource));
			final String sField = meta.getFieldName().equals("name") ? "\"" + meta.getFieldName() + "\"" : meta.getFieldName();
			if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
				Long refId = tuple.get(sField, Long.class);
				final IEntityObjectProcessor<Object> eop = nucletDalProvider.getEntityObjectProcessor(meta.getForeignEntity());
				final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
				try {
					eop.setIgnoreRecordGrantsAndOthers(true);
					EntityObjectVO<?> referencedObject = eop.getByPrimaryKey(refId);
					String refValue = RefValueExtractor.get(referencedObject, target, null, metaProvider);
					result.setFieldValue(target, refValue);
					result.setFieldId(target, refId);
				} finally {
					eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
				}
			} else {
				result.setFieldValue(target, tuple.get(sField));
			}
		}

		return result;
	}

	private <PK> void aggregateDependants(Collection<PK> sourceIds, UsageCriteria sourceUsage, GeneratorActionVO gavo, EntityObjectVO<UID> subentity,
			EntityObjectVO<PK> target, UsageCriteria targetUsage, IDependentDataMap dependants) {
		
		final Collection<EntityObjectVO<UID>> attributes =
				masterDataFacade.getDependantMd4FieldMeta(
						E.GENERATIONSUBENTITYATTRIBUTE.entity, subentity.getPrimaryKey());
		if (attributes.size() == 0) {
			return;
		}
		
		final EntityMeta<?> sourceEntity = 
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entitySource));
		final EntityMeta<?> targetEntity = 
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entityTarget));
		
		final DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		final DbFrom<?> t = query.from(sourceEntity);

		final List<DbSelection<?>> selection = new ArrayList<DbSelection<?>>();
		final List<DbExpression<?>> groupby = new ArrayList<DbExpression<?>>();
		final Map<EntityObjectVO<?>, String> aliases = new HashedMap<EntityObjectVO<?>, String>();
		for (EntityObjectVO<?> attribute : attributes) {
			final String function = (String) attribute.getFieldValue(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeGrouping);
			final UID source = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);
			
			final FieldMeta<?> meta = metaProvider.getEntityField(source);
			// final String column = meta.getDbColumn();
			DbColumnExpression<?> c = t.baseColumn(meta);
			
			if ("group by".equals(function)) {
				if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
					// c = t.baseColumn(SimpleDbField.createRef(column, meta.getJavaClass()));
					// c.alias(meta.getFieldName() + "Id");
					// selection.add(c);
					// groupby.add(c);
					c = t.baseColumnRef(meta, sourceEntity.isUidEntity());
					c.alias(meta.getFieldName());
					selection.add(c);
					groupby.add(c);					
				} else {
					selection.add(c);
					groupby.add(c);
				}
			}
			else if ("summate".equals(function)) {
				selection.add(builder.sum(c).alias(meta.getDbColumn()));
			}
			else if ("minimum value".equals(function)) {
				selection.add(builder.min(c).alias(meta.getDbColumn()));
			}
			else if ("maximum value".equals(function)) {
				selection.add(builder.max(c).alias(meta.getDbColumn()));
			}
			aliases.put(attribute, c.getAlias());
		}

		query.multiselect(selection);
		if (groupby.size() > 0) {
			query.groupBy(groupby);
		}

		final IDependentKey foreignFieldSource = getForeignKeyField(sourceEntity, sourceUsage);
		final IDependentKey foreignFieldTarget = getForeignKeyField(targetEntity, targetUsage);
		
		final ArrayList<DbCondition> conditions = new ArrayList<DbCondition>();
		for (PK sourceId : sourceIds) {
			conditions.add(builder.equalValue((DbColumnExpression<PK>) t.baseColumnRef(metaProvider.getEntityField(foreignFieldSource.getDependentRefFieldUID()), false), sourceId));
		}
		query.where(builder.or(conditions.toArray(new DbCondition[conditions.size()])));

		final List<EntityObjectVO<Long>> aggregated = dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, EntityObjectVO<Long>>() {
			@Override
			public EntityObjectVO<Long> transform(DbTuple tuple) {
				final EntityObjectVO<Long> result = new EntityObjectVO<Long>(targetEntity.getUID());
				
				for (EntityObjectVO<?> attribute : attributes) {
					final UID source = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);
					final UID target = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeTarget);
					final FieldMeta<?> meta = metaProvider.getEntityField(source);
					// result.setFieldValue(target, tuple.get(meta.getDbColumn()));
					result.setFieldValue(target, tuple.get(aliases.get(attribute)));
					if (meta.getForeignEntity() != null && meta.getForeignEntityField() != null) {
						result.setFieldId(target, tuple.get(meta.getFieldName(), Long.class));
					}
				}
				return result;
			}
		});

		dependants.addAllData(foreignFieldTarget, aggregated);
	}
	
	private IDependentKey getForeignKeyField(EntityMeta<?> mdmetavo, UsageCriteria foreignEntityUsage) {
		// try to get from layout first...
		IDependentKey dependentKey = SpringApplicationContextHolder.getBean(LayoutFacadeLocal.class).getDependentKeyBetween(foreignEntityUsage, mdmetavo.getUID());
		if (dependentKey != null) {
			return dependentKey;
		}
		
		final UID foreignKeyField;
		final Set<UID> foreignKeyFields = new TreeSet<UID>();
		for (FieldMeta fieldMeta : mdmetavo.getFields()) {
			if (foreignEntityUsage.getEntityUID().equals(fieldMeta.getForeignEntity())) {
				foreignKeyFields.add(fieldMeta.getUID());
			}
		}
		switch (foreignKeyFields.size()) {
		case 0:
			foreignKeyField = ModuleConstants.DEFAULT_FOREIGNKEYFIELDNAME;
			break;
		case 1:
			foreignKeyField = foreignKeyFields.iterator().next();
			break;
		default:
			// more than one...
			if (foreignKeyFields.contains(ModuleConstants.DEFAULT_FOREIGNKEYFIELDNAME)) {
				foreignKeyField = ModuleConstants.DEFAULT_FOREIGNKEYFIELDNAME;
			} else {
				foreignKeyField = foreignKeyFields.iterator().next();
			}
		}
		
		return DependentDataMap.createDependentKey(foreignKeyField);
	}
	
	/**
	 * copies all dependant records from source object to target object. Copies
	 * also a basekey and ordernumber attribute value as dependant into target,
	 * if appropriate and desired
	 *
	 * @param sources
	 *            source generic object with dependant masterdata
	 * @param gavo
	 *            details about generation action
	 * @param dependants
	 *            DependantMasterDataMap for the the result
	 * @throws CommonPermissionException
	 */
	private <PK> void copyDependants(Collection<EntityObjectVO<PK>> sources, EntityObjectVO parameterObject, GeneratorActionVO gavo, EntityObjectVO<UID> subentity,
			EntityObjectVO<PK> target, IDependentDataMap dependants, String customUsage) throws CommonPermissionException, CommonFinderException {
		final GenericObjectMetaDataCache lometacache = GenericObjectMetaDataCache.getInstance();
		final AttributeProvider attrprovider = AttributeCache.getInstance();

		final UsageCriteria criteria = new UsageCriteria(gavo.getTargetModule(), 
				target.getFieldUid(SF.PROCESS_UID),
				target.getFieldUid(SF.STATE_UID), customUsage);
		
		final UID layoutUidTarget = lometacache.getBestMatchingLayout(criteria, false);
		final Set<UID> setEntityNamesTarget = lometacache.getSubFormEntityByLayout(layoutUidTarget);

		final EntityMeta<?> sourceMeta = 
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entitySource));
		final EntityMeta<?> targetMeta = 
				metaProvider.getEntity(subentity.getFieldUid(E.GENERATIONSUBENTITY.entityTarget));

		UID sSource = sourceMeta.getUID();
		UID sTarget = targetMeta.getUID();

		if (!setEntityNamesTarget.contains(sTarget)) {
			return;
		}
		if (!targetMeta.isEditable()) {
			throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("generator.dependant.noneditable", sTarget));
		}

		final Collection<EntityObjectVO<UID>> attributes = masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONSUBENTITYATTRIBUTE.entity, subentity.getPrimaryKey());
		String sourceType = subentity.getFieldValue(E.GENERATIONSUBENTITY.sourceType);
		
		UsageCriteria targetUsage = UsageCriteria.createUsageCriteriaFromEO(target, customUsage);
		
		if (StringUtils.isNullOrEmpty(sourceType)) {
			for (EntityObjectVO<?> source : sources) {
				final UsageCriteria sourcecriteria = new UsageCriteria(gavo.getSourceModule(),
						source.getFieldUid(SF.PROCESS_UID), source.getFieldUid(SF.STATE_UID), customUsage);
		
				final UID iLayoutIdSource = lometacache.getBestMatchingLayout(sourcecriteria, false);
				final Set<UID> setEntityNamesSource = lometacache.getSubFormEntityByLayout(iLayoutIdSource);

				if (!setEntityNamesSource.contains(sSource)) {
					continue;
				}

				final Collection<EntityAndField> eafns = lometacache.getSubFormEntityAndForeignKeyFieldsByLayout(iLayoutIdSource);

				UID foreignfield = null;
				for (EntityAndField eafn : eafns) {
					if (eafn.getEntity().equals(sSource)) {
						foreignfield = eafn.getField();
					}
				}
				if (foreignfield == null) {
					throw new NuclosFatalException();
				}

				copyAttributes(gavo, attributes, sSource, foreignfield, source.getPrimaryKey(), targetUsage, sTarget, dependants);
			}
		}
		else {
			EntityMeta<?> parameterEntity = metaProvider.getEntity(gavo.getParameterEntity());
			LayoutFacadeLocal layoutLocal = ServerServiceLocator.getInstance().getFacade(LayoutFacadeLocal.class);
			
			Map<EntityAndField, UID> eafns = 
					layoutLocal.getSubFormEntityAndParentSubFormEntityNames(parameterEntity.getUID(), parameterObject.getId(), false, customUsage);
			
			UID foreignfield = null;
			for (Map.Entry<EntityAndField, UID> e : eafns.entrySet()) {
				if (e.getValue() == null) {
					if (e.getKey().getEntity().equals(sSource)) {
						foreignfield = e.getKey().getField();
					}
				}
			}

			if (foreignfield == null) {
				throw new NuclosFatalException();
			}

			copyAttributes(gavo, attributes, sSource, foreignfield, parameterObject.getPrimaryKey(), targetUsage, sTarget, dependants);
		}
	}

	private void copyAttributes(GeneratorActionVO gavo, Collection<EntityObjectVO<UID>> attributes, UID source, UID fef,
			Object sourceId, UsageCriteria targetUsage, UID target, IDependentDataMap dependants) {
		
		EntityMeta<Object> subformMeta = metaProvider.getEntity(target);
		final IDependentKey foreignFieldTarget = getForeignKeyField(subformMeta, targetUsage);
		
		Collection<EntityObjectVO<Object>> data = masterDataFacade.getDependantMasterData(source, fef, sourceId);
		if (attributes.size() == 0) {
			for (EntityObjectVO<?> mdvoOriginal : data) {
				final EntityObjectVO<?> mdvo = mdvoOriginal.copyFlat();
				mdvo.setDependents(new DependentDataMap());

				final EntityMeta<?> eMetaSource = metaProvider.getEntity(gavo.getSourceModule());
				for (FieldMeta<?> fieldmeta : metaProvider.getAllEntityFieldsByEntity(source).values()) {
					if (eMetaSource.getUID().equals(fieldmeta.getForeignEntity())) {
						if (!eMetaSource.isUidEntity())
							mdvo.setFieldId(fieldmeta.getUID(), null);
						else
							mdvo.setFieldUid(fieldmeta.getUID(), null);
					}
				}
				dependants.addData(foreignFieldTarget, mdvo);
			}
		}
		else {
			for (EntityObjectVO<?> mdvoOriginal : data) {
				final MasterDataVO<?> mdvo = new MasterDataVO(metaProvider.getEntity(target),true);
				for (EntityObjectVO<?> attribute : attributes) {
					final UID fieldAttSource = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeSource);
					final UID fieldAttTarget = attribute.getFieldUid(E.GENERATIONSUBENTITYATTRIBUTE.subentityAttributeTarget);
					
					final Object value = mdvoOriginal.getFieldValue(fieldAttSource);
					mdvo.setFieldValue(fieldAttTarget, value);
					
					final FieldMeta<?> metaField = metaProvider.getEntityField(fieldAttSource);
					if (metaField.getForeignEntity() != null) {
						if (!metaProvider.getEntity(metaField.getForeignEntity()).isUidEntity()) {
							mdvo.setFieldId(fieldAttTarget, mdvoOriginal.getFieldId(fieldAttSource));
						} else {
							mdvo.setFieldUid(fieldAttTarget, mdvoOriginal.getFieldUid(fieldAttSource));
						}
					}
				}
				dependants.addData(foreignFieldTarget, mdvo.getEntityObject());
			}
		}
	}

	/**
	 * transfers (copies) a specified set of attributes from one generic object
	 * to another. Called from within rules. Attention: because this is called
	 * within a rule, the source genericobject and its attributes were not saved
	 * until now -&gt; the consequence is, that the old attribute values of the
	 * source genericobject were transfered to the target genericobject this is
	 * very ugly -&gt; todo
	 * 
	 * §precondition asAttributes != null
	 *
	 * @param govoSource
	 *            source generic object id to transfer data from
	 * @param iTargetGenericObjectId
	 *            target generic object id to transfer data to
	 * @param asAttributes
	 *            Array of attribute names to specify transferred data
	 */
	public void transferGenericObjectData(GenericObjectVO govoSource, Long iTargetGenericObjectId, String[][] asAttributes, String customUsage) {
		if (asAttributes == null) {
			throw new NullArgumentException("asAttributes");
		}
		LOG.debug("Entering transferGenericObjectData()");
		try {
			final Collection<UID> stSourceAttributeIds = getAttributeIdsByModuleId(govoSource.getModule());
			final GenericObjectVO govoTarget = genericObjectFacade.get(iTargetGenericObjectId);
			final Collection<UID> stTargetAttributeIds = getAttributeIdsByModuleId(govoTarget.getModule());

			final UID[][] aiIncludedAttributeIds = getAttributeIdsFromNames(asAttributes, govoSource.getModule(), govoTarget.getModule());

			final Collection<UID> stExcludedAttributeUIdsSource = this.getExcludedAttributes(govoSource.getModule());
			final Collection<UID> stExcludedAttributeUIdsTarget = this.getExcludedAttributes(govoTarget.getModule());
			
			for (int i = 0; i < aiIncludedAttributeIds.length; i++) {
				if (stSourceAttributeIds.contains(aiIncludedAttributeIds[i][0]) && !stExcludedAttributeUIdsSource.contains(aiIncludedAttributeIds[i][0])) {
					if (stTargetAttributeIds.contains(aiIncludedAttributeIds[i][1]) && !stExcludedAttributeUIdsTarget.contains(aiIncludedAttributeIds[i][1])) {
						copyAttribute(aiIncludedAttributeIds[i][0], aiIncludedAttributeIds[i][1], govoSource, govoTarget);
					}
				}
			}

			// todo: avoid using modify here as it triggers another rule!
			genericObjectFacade.modify(new GenericObjectWithDependantsVO(govoTarget, new DependentDataMap(), govoTarget.getDataLanguageMap()), customUsage);
		} catch (CommonBusinessException ex) {
			throw new NuclosFatalException(ex);
		}
		LOG.debug("Leaving transferGenericObjectData()");
	}

	/**
	 * Copy the attribute with the given ID from the source generic object to
	 * the target generic object.
	 *
	 * @param iAttributeSourceId
	 * @param iAttributeTargetId
	 * @param govoSource
	 * @param govoTarget
	 */
	private void copyAttribute(UID iAttributeSourceId, UID iAttributeTargetId, GenericObjectVO govoSource, GenericObjectVO govoTarget) {
		LOG.debug("Copying attribute {} from {} to attribute {} from {}",
		          govoSource.toString(),
		          AttributeCache.getInstance().getAttribute(iAttributeTargetId).getName(),
		          govoTarget.toString(),
				AttributeCache.getInstance().getAttribute(iAttributeSourceId).getName());
		final DynamicAttributeVO attrvoSource = govoSource.getAttribute(iAttributeSourceId);
		final DynamicAttributeVO attrvoTarget = govoTarget.getAttribute(iAttributeTargetId);
		if (attrvoSource == null) {
			if (attrvoTarget != null) {
				// remove existent attribute:
				attrvoTarget.remove();
			}
		} else {
			if (attrvoTarget != null) {
				// update existent attribute:
				attrvoTarget.setValueId(attrvoSource.getValueId());
				attrvoTarget.setValue(attrvoSource.getValue());
			} else {
				// add nonexistent attribute:
				govoTarget.setAttribute(new DynamicAttributeVO(iAttributeTargetId, attrvoSource.getValueId(), attrvoSource.getValueUid(), attrvoSource.getValue()));
			}
		}
	}

	/**
	 * §precondition asAttributeNames != null
	 * §postcondition result != null
	 */
	private static UID[][] getAttributeIdsFromNames(String[][] asAttributeNames, UID iSourceModuleId, UID iTargetModuleId) {
		if (asAttributeNames == null) {
			throw new NullArgumentException("asAttributeNames");
		}
		final AttributeCache attrcache = AttributeCache.getInstance();
		final UID[][] aiAttributeIds = new UID[asAttributeNames.length][2];

		for (int i = 0; i < asAttributeNames.length; i++) {
			String sSourceAttributeName = asAttributeNames[i][0];
			String sTargetAttributeName = asAttributeNames[i][1];
			
			Map<UID, FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(iSourceModuleId);
			for (UID field : fields.keySet()) {
				if (sSourceAttributeName.equals(fields.get(field).getFieldName())) {
					aiAttributeIds[i][0] = field;
					break;
				}
			}
			Map<UID, FieldMeta<?>> fieldTargets = MetaProvider.getInstance().getAllEntityFieldsByEntity(iTargetModuleId);
			for (UID field : fieldTargets.keySet()) {
				if (sTargetAttributeName.equals(fieldTargets.get(field).getFieldName())) {
					aiAttributeIds[i][1] = field;
					break;
				}
			}
		}

		assert aiAttributeIds != null;
		return aiAttributeIds;
	}

	/**
	 * copies all attributes from source object to target template
	 *
	 * @param govoSource
	 *            source generic object
	 * @param govoTargetTemplate
	 *            target generic object template
	 * @param generatoractionvo
	 *            generator vo to determine attributes
	 * @throws CommonPermissionException
	 */
	private <PK> EntityObjectVO<PK> getNewObject(EntityObjectVO<PK> eoSource, GeneratorActionVO generatoractionvo) throws CommonFinderException, CommonValidationException, CommonPermissionException {
		final EntityMeta<?> sourceMeta = metaProvider.getEntity(generatoractionvo.getSourceModule());
		final EntityMeta<?> targetMeta = metaProvider.getEntity(generatoractionvo.getTargetModule());

		final EntityObjectVO<PK> result = new EntityObjectVO<PK>(targetMeta);
		
		final List<Pair<UID, UID>> includedAttributes = getIncludedAttributes(generatoractionvo, null);

		final Set<UID> stSourceAttributes = metaProvider.getAllEntityFieldsByEntity(sourceMeta.getUID()).keySet();
		final Set<UID> stTargetAttribute = metaProvider.getAllEntityFieldsByEntity(targetMeta.getUID()).keySet();

		for (Pair<UID, UID> p : includedAttributes) {
			if (stSourceAttributes.contains(p.x)) {
				if (stTargetAttribute.contains(p.y)) {
					if (eoSource.getFieldValue(p.x) != null) {
						result.setFieldValue(p.y, eoSource.getFieldValue(p.x));
					}
					if (eoSource.getFieldId(p.x) != null) {
						result.setFieldId(p.y, eoSource.getFieldId(p.x));
					}
					if (eoSource.getFieldUid(p.x) != null) {
						result.setFieldUid(p.y, eoSource.getFieldUid(p.x));
					}
				}
			}
		}
		return result;
	}

	private <PK> EntityObjectVO<PK> getNewObject(Collection<EntityObjectVO<PK>> sourceObjects, GeneratorActionVO generatoractionvo) throws CommonFinderException, CommonValidationException, CommonPermissionException {
		final EntityObjectVO<PK> object = getGroupedGeneratedObject(generatoractionvo, CollectionUtils.transform(sourceObjects, new Transformer<EntityObjectVO<PK>, PK>() {
			@Override
			public PK transform(EntityObjectVO<PK> i) {
				return i.getPrimaryKey();
			}
		}));
		return object;
	}

	private void copyParameterAttributes(EntityObjectVO<?> sourceVO, UID sourceEntityUID, EntityObjectVO<?> target, GeneratorActionVO generatoractionvo) {
		final List<Pair<UID, UID>> includedAttributes = getIncludedAttributes(generatoractionvo, "parameter");

		final Set<UID> stSourceAttributes = metaProvider.getAllEntityFieldsByEntity(sourceEntityUID).keySet();
		final Set<UID> stTargetAttribute = metaProvider.getAllEntityFieldsByEntity(target.getDalEntity()).keySet();

		for (Pair<UID, UID> p : includedAttributes) {
			if (stSourceAttributes.contains(p.x)) {
				if (stTargetAttribute.contains(p.y)) {
					if (sourceVO.getFieldValue(p.x) != null) {
						target.setFieldValue(p.y, sourceVO.getFieldValue(p.x));
					}
					if (sourceVO.getFieldId(p.x) != null) {
						target.setFieldId(p.y, sourceVO.getFieldId(p.x));
					}
					if (sourceVO.getFieldUid(p.x) != null) {
						target.setFieldUid(p.y, sourceVO.getFieldUid(p.x));
					}
				}
			}
		}
	}

	/**
	 * Extract the IDs of the attributes to copy from the given
	 * GeneratorActionVO as a collection.
	 *
	 * @param generatoractionvo
	 * @param sourceType
	 * @return list of pairs (source id, target id)
	 */
	private List<Pair<UID, UID>> getIncludedAttributes(GeneratorActionVO generatoractionvo, String sourceType) {
		Collection<EntityObjectVO<UID>> colmdvo =
				masterDataFacade.getDependantMd4FieldMeta(E.GENERATIONATTRIBUTE.generation, generatoractionvo.getId());
		List<Pair<UID, UID>> result = new ArrayList<Pair<UID, UID>>(colmdvo.size());
		for (EntityObjectVO<UID> mdvo : colmdvo) {
			if (!ObjectUtils.equals(sourceType, mdvo.getFieldValue(E.GENERATIONATTRIBUTE.sourceType)))
				continue;
			UID source = mdvo.getFieldUid(E.GENERATIONATTRIBUTE.attributeSource);
			UID target = mdvo.getFieldUid(E.GENERATIONATTRIBUTE.attributeTarget);
			
			if (E.isNuclosEntity(source) || E.isNuclosEntity(target))
				continue;
			result.add(Pair.makePair(source, target));
		}
		return result;
	}
	
	private Collection<UID> getExcludedAttributes(UID uid) {
		final Collection<UID> collExcludedAttributeIds = new HashSet<UID>();
		
		collExcludedAttributeIds.add(SF.SYSTEMIDENTIFIER.getUID(uid));
		collExcludedAttributeIds.add(SF.ORIGIN.getUID(uid));
		collExcludedAttributeIds.add(SF.CHANGEDAT.getUID(uid));
		collExcludedAttributeIds.add(SF.CHANGEDBY.getUID(uid));
		collExcludedAttributeIds.add(SF.CREATEDAT.getUID(uid));
		collExcludedAttributeIds.add(SF.CREATEDBY.getUID(uid));
		collExcludedAttributeIds.add(SF.PROCESS.getUID(uid));
		collExcludedAttributeIds.add(SF.STATE.getUID(uid));
		collExcludedAttributeIds.add(SF.STATENUMBER.getUID(uid));
		
		return collExcludedAttributeIds;
	}
	
	/**
	 * get generator usages for specified GeneratorId
	 *
	 * @return Collection&lt;GeneratorUsageVO&gt;
	 * @throws CommonFatalException
	 */
	public Collection<GeneratorUsageVO> getGeneratorUsages(UID id) throws CommonFatalException {
		List<GeneratorUsageVO> usages = new ArrayList<GeneratorUsageVO>();
		CollectableComparison cond = 
				SearchConditionUtils.newUidComparison(E.GENERATIONUSAGE.generation, ComparisonOperator.EQUAL, id);
		Collection<MasterDataVO<UID>> mdUsagesVO = 
				masterDataFacade.getMasterData(E.GENERATIONUSAGE, cond, true);

		for (MasterDataVO md : mdUsagesVO)
			usages.add(MasterDataWrapper.getGeneratorUsageVO(md));

		return usages;
	}

	// Some private helpers...

	private GeneratorActionVO getGeneratorActionByName(String name) throws CommonFinderException {
		CollectableComparison cond = SearchConditionUtils.newComparison(
				E.GENERATION.name, ComparisonOperator.EQUAL, name);
		Collection<MasterDataVO<UID>> mdGenerationsVO = masterDataFacade.getMasterData(E.GENERATION, cond, true);

		if (mdGenerationsVO == null || mdGenerationsVO.isEmpty())
			throw new CommonFinderException(StringUtils.getParameterizedExceptionMessage("generator.facade.exception.3", name));
		// "Es ist ein Fehler bei der Objektgenerierung aufgetreten. Objektgenerator mit dem Namen "
		// + name + " kann nicht gefunden werden.");

		MasterDataVO<UID> mdVO = mdGenerationsVO.iterator().next();

		return MasterDataWrapper.getGeneratorActionVO(mdVO, getGeneratorUsages(mdVO.getPrimaryKey()));
	}

	/**
	 * generate one or more generic objects from an existing generic object
	 * (copying selected attributes and subforms)
	 * 
	 * §ejb.interface-method view-type="both"
	 * §ejb.permission role-name="Login"
	 *
	 * @param iSourceObjectId
	 *            source generic object id to generate from
	 * @param generatoractionvo
	 *            generator action value object to determine what to do
	 * @return id of generated generic object (if exactly one object was
	 *         generated)
	 * @throws NuclosBusinessRuleException 
	 */
	public EntityObjectVO generateGenericObjectWithoutCheckingPermission(Long iSourceObjectId, GeneratorActionVO generatoractionvo, String customUsage) 
			throws CommonBusinessException {
		
		return (EntityObjectVO)generateGenericObject(iSourceObjectId, null, generatoractionvo, customUsage).getGeneratedObject();
	}

	private Collection<UID> getAttributeIdsByModuleId(UID iModuleId) {
		final Collection<UID> stAttributesIds = new HashSet<UID>();
		for (AttributeCVO attrcvo : GenericObjectMetaDataCache.getInstance().getAttributeCVOsByModule(iModuleId, false)) {
			stAttributesIds.add(attrcvo.getId());
		}
		return stAttributesIds;
	}

	private class ExtractIdTransformer<PK> implements Transformer<EntityObjectVO<PK>, PK> {
		@Override

		public PK transform(EntityObjectVO<PK> i) {
			return (PK) i.getPrimaryKey();
		}

	}
	
	private void fillInMissingRefValues(EntityObjectVO<?> eo) {
		EntityMeta<?> eMeta = metaProvider.getEntity(eo.getDalEntity());
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			if (fMeta.getForeignEntity() != null) {
				Long refId = eo.getFieldId(fMeta.getUID());
				Object refValue = eo.getFieldValue(fMeta.getUID());
				if (refId != null && refValue == null) {
					final IEntityObjectProcessor<Object> eop = nucletDalProvider.getEntityObjectProcessor(fMeta.getForeignEntity());
					final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
					try {
						eop.setIgnoreRecordGrantsAndOthers(true);
						EntityObjectVO<?> refEo = eop.getByPrimaryKey(refId);
						String missingStringifiedValue = RefValueExtractor.get(refEo, fMeta.getForeignEntityField(), null, metaProvider);
						eo.setFieldValue(fMeta.getUID(), missingStringifiedValue);
					} finally {
						eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
					}
				}
			}
		}
		
		for (IDependentKey dependentKey : eo.getDependents().getKeySet()) {
			for (EntityObjectVO<?> eoDep : eo.getDependents().getData(dependentKey)) {
				fillInMissingRefValues(eoDep);
			}
		}
	}

	
}
