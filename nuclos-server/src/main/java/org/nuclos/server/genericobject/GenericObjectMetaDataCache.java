//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.nuclos.common.AttributeProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.GenericObjectMetaDataProvider;
import org.nuclos.common.GenericObjectMetaDataVO;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.nuclos.server.attribute.valueobject.LayoutUsageVO;
import org.nuclos.server.common.AttributeCache;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.statemodel.ejb3.StateFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Server side leased object meta data cache (singleton).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@Component
public class GenericObjectMetaDataCache implements GenericObjectMetaDataProvider {

	private static final Logger LOG = LoggerFactory.getLogger(GenericObjectMetaDataCache.class);

	private static GenericObjectMetaDataCache INSTANCE;
	
	//

	private GenericObjectMetaDataVO lometacvo;
	
	private MetaProvider metaProvider;
	
	private SpringDataBaseHelper dataBaseHelper;

	public static GenericObjectMetaDataCache getInstance() {
		return INSTANCE;
	}

	private GenericObjectMetaDataCache() {
		INSTANCE = this;
	}

	@PostConstruct
	final void init() {
		this.lometacvo = newGenericObjectMetaDataCVO();
	}
	
	@Autowired
	void setMetaProvider(MetaProvider metaProvider) {
		this.metaProvider = metaProvider;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}

	private GenericObjectMetaDataVO newGenericObjectMetaDataCVO() {
		LOG.debug("Rebuilding generic object meta data cache");

		Map<UID, Set<UID>> mapModuleAtributes = new HashMap<>();
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			mapModuleAtributes.put(eMeta.getUID(), new HashSet<UID>());
			for (FieldMeta<?> efMeta : metaProvider.getAllEntityFieldsByEntity(eMeta.getUID()).values()) {
				mapModuleAtributes.get(eMeta.getUID()).add(efMeta.getUID());
			}
		}

		return new GenericObjectMetaDataVO(mapModuleAtributes, getLayoutMap(), getLayoutUsageVOs());
	}

	/**
	 * @postcondition result != null
	 * 
	 * @return Map&lt;Integer iLayoutId, String sLayoutML&gt;
	 */
	public static Map<UID, String> getLayoutMap() {
		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.LAYOUT);
		query.multiselect(t.baseColumn(E.LAYOUT), t.baseColumn(E.LAYOUT.layoutML));

		Map<UID, String> result = new HashMap<UID, String>();
		for (DbTuple tuple : SpringDataBaseHelper.getInstance().getDbAccess().executeQuery(query)) {
			result.put(tuple.get(0, UID.class), tuple.get(1, String.class));
		}
		return result;
	}

	/**
	 * @return Map&lt;UID, UID&gt;
	 */
	public Map<UID, UID> getResourceMap() {
		Map<UID, UID> result = new HashMap<>();

		for (EntityMeta eMeta : metaProvider.getAllEntities()) {
			if (eMeta.isStateModel()) {
				if (eMeta.getResource() != null) {
					result.put(eMeta.getUID(), eMeta.getResource());
				}
			}
		}

		return result;
	}

	/**
	 * @postcondition result != null
	 * 
	 * @return all layout usages
	 */
	private Collection<LayoutUsageVO> getLayoutUsageVOs() {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom t = query.from(E.LAYOUTUSAGE);
		query.multiselect(
			t.baseColumn(E.LAYOUTUSAGE.layout),
			t.baseColumn(E.LAYOUTUSAGE.entity),
			t.baseColumn(E.LAYOUTUSAGE.process),
			t.baseColumn(E.LAYOUTUSAGE.searchScreen),
			t.baseColumn(E.LAYOUTUSAGE.state),
			t.baseColumn(E.LAYOUTUSAGE.custom));
		List<LayoutUsageVO> result = dataBaseHelper.getDbAccess().executeQuery(query, new Transformer<DbTuple, LayoutUsageVO>() {
			@Override
			public LayoutUsageVO transform(DbTuple tuple) {
				try {
					return new LayoutUsageVO(
						tuple.get(0, UID.class),
						new UsageCriteria(tuple.get(1, UID.class), tuple.get(2, UID.class), tuple.get(4, UID.class), tuple.get(5, String.class)),
						Boolean.TRUE.equals(tuple.get(3, Boolean.class)));
				}
				catch (Exception ex) {
					return null;
				}
			}
		});

		CollectionUtils.retainAll(result, new Predicate<LayoutUsageVO>() {
			@Override
            public boolean evaluate(LayoutUsageVO t) {
	            if (t == null) {
	            	return false;
	            }
	            return true;
            }
		});
		return result;
	}
	/**
	 * Returns all Layout UIDs for a specific Module
	 * @param module UID
	 * @return List of Layout UIDs
	 */
	public List<UID> getLayoutsForModule(UID module){
		List<UID> ret = new ArrayList<UID>();
		Collection<LayoutUsageVO> col = getLayoutUsageVOs();
		Iterator<LayoutUsageVO> it = col.iterator();
		while(it.hasNext()){
			LayoutUsageVO luvo = it.next();
			if(luvo.getUsageCriteria().getEntityUID() != null && luvo.getUsageCriteria().getEntityUID().equals(module))
				ret.add(luvo.getLayout());
		}
		return ret;
	}

	public GenericObjectMetaDataVO getMetaDataCVO() {
		return this.lometacvo;
	}

	private AttributeProvider getAttributeProvider() {
		return AttributeCache.getInstance();
	}

	@Override
	public AttributeCVO getAttribute(UID attribute) {
		return this.getAttributeProvider().getAttribute(attribute);
	}

	@Override
	public FieldMeta getEntityField(UID field) throws NuclosAttributeNotFoundException {
		return metaProvider.getEntityField(field);
	}

	@Override
	public Collection<AttributeCVO> getAttributes() {
		return this.getAttributeProvider().getAttributes();
	}

	@Override
	public Collection<AttributeCVO> getAttributeCVOsByModule(UID module, Boolean bSearchable) {
		return this.getMetaDataCVO().getAttributeCVOsByModule(this.getAttributeProvider(), module, bSearchable);
	}

	@Override
	public Set<UID> getSubFormEntityByLayout(UID layoutUid) {
		return this.getMetaDataCVO().getSubFormEntityByLayout(layoutUid, metaProvider);
	}

	@Override
	public Set<UID> getAllSubFormEntitiesByEntity(UID entityUid) {
		return this.getMetaDataCVO().getSubFormEntityNamesByModuleId(entityUid, metaProvider);
	}

	@Override
	public Collection<EntityAndField> getSubFormEntityAndForeignKeyFieldsByLayout(UID layoutUid) {
		return this.getMetaDataCVO().getSubFormEntityAndForeignKeyFieldsByLayout(layoutUid, metaProvider);
	}

	@Override
	public Set<UID> getBestMatchingLayoutAttributes(UsageCriteria usagecriteria) throws CommonFinderException {
		return this.getMetaDataCVO().getBestMatchingLayoutAttributes(usagecriteria, metaProvider);
	}

	public Set<UID> getBestMatchingLayoutSubformEntities(UsageCriteria usagecriteria) throws CommonFinderException {
		return this.getMetaDataCVO().getBestMatchingLayoutSubformEntities(usagecriteria, metaProvider);
	}

	@Override
	public UID getBestMatchingLayout(UsageCriteria usagecriteria, boolean bSearchScreen) throws CommonFinderException {
		UID iBestMatchingLayoutId;
		try {
			iBestMatchingLayoutId = this.getMetaDataCVO().getBestMatchingLayout(usagecriteria, false);			
		} catch (CommonFinderException cfe) {
			if (usagecriteria.getStatusUID() != null) {
				throw cfe;
			}
			LOG.warn("Unable to get best matching layout:", cfe);
			UID statusUID = SpringApplicationContextHolder.getBean(StateFacadeLocal.class).getInitialState(usagecriteria).getId();
			UsageCriteria usage2 = new UsageCriteria(usagecriteria.getEntityUID(), usagecriteria.getProcessUID(), statusUID, usagecriteria.getCustom());
			iBestMatchingLayoutId = this.getMetaDataCVO().getBestMatchingLayout(usage2, false);	
		}
		return iBestMatchingLayoutId;
	}

	@Override
	public Set<UID> getLayoutsByModule(UID layout, boolean bSearchScreen) {
		return this.getMetaDataCVO().getLyoutsByModule(layout, bSearchScreen);
	}

	@Override
	public String getLayoutML(UID layout) {
		return this.getMetaDataCVO().getLayoutML(layout);
	}
	
	public void revalidate() {
		// revalidate layout and module caches:
		this.lometacvo = newGenericObjectMetaDataCVO();
		// es gibt keine GO Views mehr --> GO Refactoring
		//this.refreshViews();
	}


}	// class GenericObjectLayoutCache
