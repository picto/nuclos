//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for GenericObjectGroup.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class GenericObjectGroupFacadeBean implements GenericObjectGroupFacadeRemote {

	private SpringDataBaseHelper dataBaseHelper;

	private MasterDataFacadeLocal masterDataFacade;

	@Autowired
	private NucletDalProvider nucletDalProvider;
	
	public GenericObjectGroupFacadeBean() {
	}
	
	@Autowired
	final void setDataBaseHelper(SpringDataBaseHelper springDataBaseHelper) {
		this.dataBaseHelper = springDataBaseHelper;
	}

	private MasterDataFacadeLocal getMasterDataFacade() {
		if (masterDataFacade == null) {
			masterDataFacade = SpringApplicationContextHolder.getBean(MasterDataFacadeLocal.class);			
		}
		return masterDataFacade;
	}
	
	@Cacheable(value = "countOfGenericObjectGroup")
	private Long getCountOfGenericObjectGroup() {
		return nucletDalProvider.getEntityObjectProcessor(E.GENERICOBJECTGROUP).count(CollectableSearchExpression.TRUE_SEARCH_EXPR);
	}
	
	@CacheEvict(value = "countOfGenericObjectGroup", allEntries = true)
	private void evictCountOfGenericObjectGroup() {
	}
	
	/**
	 * @return the ids of the object group, the genericobject is assigned to
	 */
	public Set<UID> getObjectGroupUid(Long lGenericObjectId) {
		if (getCountOfGenericObjectGroup().equals(0L)) {
			return Collections.EMPTY_SET;
		}
		
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.GENERICOBJECTGROUP);
		query.select(t.baseColumn(E.GENERICOBJECTGROUP.group));
		query.where(builder.equalValue(t.baseColumn(E.GENERICOBJECTGROUP.genericObject), lGenericObjectId));
		return new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
	}

	/**
	 * @param groupUID
	 * @return the group name
	 */
	public String getObjectGroupName(UID groupUID) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<String> query = builder.createQuery(String.class);
		DbFrom<UID> t = query.from(E.GROUP);
		query.select(t.baseColumn(E.GROUP.name));
		query.where(builder.equalValue(t.baseColumn(E.GROUP), groupUID));
		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
	}

	/**
	 * @return the ids of the genericobjects, which are assigned to the given object group.
	 */
	public Set<Long> getGenericObjectIdsForGroup(UID moduleUid, UID groupUid) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
	
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom<UID> t = query.from(E.GENERICOBJECTGROUP);
		query.select(t.baseColumn(E.GENERICOBJECTGROUP.genericObject));
		query.where(builder.equalValue(t.baseColumn(E.GENERICOBJECTGROUP.group), groupUid));
		
		return new HashSet<Long>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
	}

	/**
	 * @return the ids of the genericobjects, which are assigned to the given object group. 
	 * ??? Not right?
	 */
	public Set<Long> getGenericObjectIdsForNoGroup(UID moduleUid) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
	
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom<Long> t = query.from(E.GENERICOBJECT);
		query.select(t.baseColumn(SF.PK_ID));
		query.where(builder.equalValue(t.baseColumn(E.GENERICOBJECT.module), moduleUid));
		
		return new HashSet<Long>(dataBaseHelper.getDbAccess().executeQuery(query.distinct(true)));
	}
	
	/**
	 * removes the generic object with the given id from the group with the given id.
	 *
	 * @param genericObjectId generic object id to be removed. Must be a main module object.
	 * @param groupUid id of group to remove generic object from
	 */
	public void removeFromGroup(Long genericObjectId, UID groupUid)
		throws NuclosBusinessRuleException, CommonFinderException, CommonRemoveException,
			CommonStaleVersionException, CommonPermissionException, CommonCreateException
	{
		UID id = findId(genericObjectId, groupUid);
		getMasterDataFacade().remove(E.GENERICOBJECTGROUP.getUID(), id, false, null);
		evictCountOfGenericObjectGroup();
	}

	private static UID findId(long genericObjectId, UID groupUid) {
		DbQueryBuilder builder = SpringDataBaseHelper.getInstance().getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.GENERICOBJECTGROUP);
		query.select(t.baseColumn(SF.PK_UID));
		query.where(builder.and(
			builder.equalValue(t.baseColumn(E.GENERICOBJECTGROUP.genericObject), genericObjectId),
			builder.equalValue(t.baseColumn(E.GENERICOBJECTGROUP.group), groupUid)));
		return SpringDataBaseHelper.getInstance().getDbAccess().executeQuerySingleResult(query);
	}

	/**
	 * adds the generic object with the given id to the group with the given id.
	 *
	 * @param genericObjectId generic object id to be grouped.  Must be a main module object.
	 * @param groupUid id of group to add generic object to
	 */
	public void addToGroup(Long genericObjectId, UID groupUid)
		throws NuclosBusinessRuleException, CommonCreateException, CommonPermissionException
	{
		MasterDataVO<UID> mdvo = new MasterDataVO<UID>(E.GENERICOBJECTGROUP, true);
		mdvo.setFieldUid(E.GENERICOBJECTGROUP.group, groupUid);
		mdvo.setFieldId(E.GENERICOBJECTGROUP.genericObject, genericObjectId);
		mdvo = getMasterDataFacade().create(mdvo, null);
		evictCountOfGenericObjectGroup();
	}
}
