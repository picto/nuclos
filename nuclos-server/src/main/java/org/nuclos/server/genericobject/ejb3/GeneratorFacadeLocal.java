//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.genericobject.ejb3;

import java.util.Collection;
import java.util.Map;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.valueobject.GeneratorActionVO;
import org.nuclos.server.genericobject.valueobject.GeneratorUsageVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;

// @Local
public interface GeneratorFacadeLocal {

	/**
	 * transfers (copies) a specified set of attributes from one generic object to another.
	 * Called from within rules.
	 * Attention: because this is called within a rule, the source genericobject and its attributes were not saved until now -&gt;
	 * the consequence is, that the old attribute values of the source genericobject were transfered to the target genericobject
	 * this is very ugly -&gt; :TODO:
	 * 
	 * §precondition asAttributes != null
	 *
	 * @param govoSource source generic object id to transfer data from
	 * @param iTargetGenericObjectId target generic object id to transfer data to
	 * @param asAttributes Array of attribute names to specify transferred data
	 */
	void transferGenericObjectData(GenericObjectVO govoSource,
		Long iTargetGenericObjectId, String[][] asAttributes, String customUsage);

	/**
	 * get generator usages for specified GeneratorId
	 * 
	 * @return Collection&lt;GeneratorUsageVO&gt;
	 * @throws CommonFatalException
	 */
	Collection<GeneratorUsageVO> getGeneratorUsages(UID uid)
		throws CommonFatalException;

	EntityObjectVO<?> generateGenericObjectWithoutCheckingPermission(Long iSourceObjectId, GeneratorActionVO generatoractionvo, String customUsage)
			throws CommonBusinessException;
	
	GenerationResult generateGenericObject(Long iSourceObjectId, Long parameterObjectId, GeneratorActionVO generatoractionvo, String customUsage) 
			throws CommonBusinessException;

	<PK> Map<String, Collection<EntityObjectVO<PK>>> groupObjects(Collection<PK> sourceIds, GeneratorActionVO generatoractionvo) throws CommonPermissionException;
	
	@RolesAllowed("Login")
	<PK> GenerationResult generateGenericObjects(Collection<EntityObjectVO<PK>> sourceObjects, Long parameterObjectId,
			GeneratorActionVO generatoractionvo, String customUsage, boolean bIsCloningAction)
			throws CommonBusinessException;

}

