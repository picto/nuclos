//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.validation;

import java.math.BigDecimal;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.validation.annotation.Validation;
import org.springframework.beans.factory.annotation.Autowired;

@Validation(order = 3)
public class FieldDimensionValidator implements Validator {
	
	private IMetaProvider metaProvider;
	
	@Autowired
	public void setMetaDataProvider(IMetaProvider metaDataProvider) {
		this.metaProvider = metaDataProvider;
	}
	
	@Override
	public void validate(EntityObjectVO<?> object, ValidationContext c) {
		EntityMeta<?> meta = metaProvider.getEntity(object.getDalEntity());		
		for (FieldMeta<?> fieldmeta : metaProvider.getAllEntityFieldsByEntity(object.getDalEntity()).values()) {
			if (fieldmeta.getForeignEntity() == null) {				
				final Object oValue = object.getFieldValue(fieldmeta.getUID());
				if (oValue != null && fieldmeta.getScale() != null) {
					int maxlength = fieldmeta.getScale();

					if (oValue instanceof Integer) {
						if (((Integer) oValue).toString().length() > maxlength) {
							String error = StringUtils.getParameterizedExceptionMessage("CollectableUtils.6", oValue, fieldmeta.getLocaleResourceIdForLabel());
							c.addFieldError(meta.getUID(), fieldmeta.getUID(), error, FieldValidationError.ValidationErrorType.FIELD_DIMENSION_ERROR);
						}
					} else if (oValue instanceof String) {
						if (((String) oValue).length() > maxlength) {
							String error = StringUtils.getParameterizedExceptionMessage("CollectableUtils.7", oValue, fieldmeta.getLocaleResourceIdForLabel());
							c.addFieldError(meta.getUID(), fieldmeta.getUID(), error, FieldValidationError.ValidationErrorType.FIELD_DIMENSION_ERROR);
						}
					} else if (oValue instanceof Double){
						BigDecimal bd = BigDecimal.valueOf((Double) oValue);
						int digitsBeforeSep = bd.precision() - bd.scale();
						Integer precision = fieldmeta.getPrecision() != null ? fieldmeta.getPrecision() : 0;
						if (digitsBeforeSep > maxlength - precision) {
							String error = StringUtils.getParameterizedExceptionMessage("CollectableUtils.8", oValue, fieldmeta.getLocaleResourceIdForLabel());
							c.addFieldError(meta.getUID(), fieldmeta.getUID(), error, FieldValidationError.ValidationErrorType.FIELD_DIMENSION_ERROR);
						}
					}
				}
			}
		}	
	}
}
