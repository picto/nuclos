package org.nuclos.server.validation;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.validation.annotation.Validation;

@Validation(entity="nuclos_nucletReleasenote")
public class NucletReleaseNoteValidator implements Validator {
	
	// Spring injection
	/*
	@Autowired
	private TransferFacadeLocal transferFacade;
	
	// end of Spring injection
	
	private final Map<UID,TransferNuclet> nuclets = new HashMap<UID, TransferNuclet>();
	
	NucletReleaseNoteValidator() {
	}
	
	@PostConstruct
	void init() {
		for (TransferNuclet tn: transferFacade.getAvailableNuclets()) {
			nuclets.put(tn.getUID(), tn);
		}
	}*/
	
	@Override
	public void validate(EntityObjectVO<?> releaseNote, ValidationContext context) {
		final UID nuclet = releaseNote.getFieldUid(E.NUCLETRELEASENOTE.nuclet.getUID());
		final Integer nucletVersion = releaseNote.getFieldValue(E.NUCLETRELEASENOTE.nucletVersion.getUID(), Integer.class);
		if (nucletVersion == null) {
			context.addError("nuclet.release.note.validation.empty");
		} else {
			final int nv = nucletVersion.intValue();
			if (nv < 0) {
				context.addError("nuclet.release.note.validation.negative");
			}
			// TODO check against the updated nucletEO, not the old cached value!
			/*final TransferNuclet tn = nuclets.get(nuclet);
			final Integer realNucletVersion = tn.getVersion();
			if (realNucletVersion != null && nv > realNucletVersion) {
				context.addError("nuclet.release.note.validation.version.toobig");
			}*/
		}
	}

}
