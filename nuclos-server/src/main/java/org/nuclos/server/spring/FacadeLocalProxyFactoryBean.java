//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.spring;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.TargetClassAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.util.Assert;

/**
 * Used in conjuction with {@link FacadeLocalProxyBeanFactoryPostProcessor}. 
 * Do not annotate as Component!
 * 
 * @author Thomas Pasch
 */
public class FacadeLocalProxyFactoryBean<T> implements FactoryBean<T> {
	
	private static final Logger LOG = LoggerFactory.getLogger(FacadeLocalProxyFactoryBean.class);
	
	private static final Class<?> FACTORY_INTERFACE;
	
	static {
		Class<?> fi = null;
		try {
			fi = LangUtils.getClassLoaderThatWorksForWebStart().loadClass("org.mockito.cglib.proxy.Factory");
		} catch (ClassNotFoundException e) {
			// Class is only know when running tests.
			// throw new ExceptionInInitializerError(e);
		}
		FACTORY_INTERFACE = fi;
	}
	
	private Object facadeBean;
	
	private Class<T> facadeLocalInterface;
	
	private NuclosRemoteContextHolder ctx;
	
	public FacadeLocalProxyFactoryBean() {
	}
	
	public void setFacadeBean(Object facadeBean) throws ClassNotFoundException {
		Assert.notNull(facadeBean);
		final String classname = extractClassname(facadeBean);
		Assert.isTrue(classname.endsWith("FacadeBean"), "Facade bean name does not end with 'FacadeBean': " 
				+ classname + " (" + Arrays.asList(facadeBean.getClass().getInterfaces()));
		this.facadeBean = facadeBean;
	}
	
	private String extractClassname(Object o) throws ClassNotFoundException {
		return extractClass(o).getName();
	}
	
	private <C> Class<C> extractClass(C o) throws ClassNotFoundException {
		Class<C> result;
		if (o instanceof TargetClassAware) {
			final TargetClassAware adv = (TargetClassAware) o;
			final Class<?> targetClazz = adv.getTargetClass();
			if (targetClazz != null) {
				result = (Class<C>) targetClazz;
			} else {
				throw new IllegalStateException();
			}
		} else {
			result = (Class<C>) o.getClass();
		}
		// Springockito support
		if (FACTORY_INTERFACE != null && FACTORY_INTERFACE.isAssignableFrom(result)) {
			String name = result.getName();
			final int idx = name.indexOf("$$");
			if (idx >= 0) {
				result = (Class<C>) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(name.substring(0, idx));
			}
		}
		return result;
	}
	
	public void setFacadeLocalInterface(Class<T> facadeLocalInterface) {
		Assert.notNull(facadeLocalInterface);
		this.facadeLocalInterface = facadeLocalInterface;
	}
	
	public void setNuclosRemoteContextHolder(NuclosRemoteContextHolder ctx) {
		Assert.notNull(ctx);
		this.ctx = ctx;
	}

	@Override
	public T getObject() throws Exception {
		if (facadeLocalInterface == null) {
			throw new NullPointerException("local interface is null on " + facadeBean);
		}
		
		// final Class<?> fbc = extractClass(facadeBean);
		final Class<?> fbc = facadeBean.getClass();
		
		final Object proxy = Proxy.newProxyInstance(LangUtils.getClassLoaderThatWorksForWebStart(), 
				new Class<?>[] { facadeLocalInterface }, new InvocationHandler() {
			
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				final Method realMethod;
				try {
					realMethod = fbc.getMethod(method.getName(), method.getParameterTypes());
				}
				catch (NoSuchMethodException e) {
					LOG.warn("no equivalent method to {} on {} interfaces: {}",
					         method, facadeBean, fbc.getInterfaces());
					throw e;
				}
				try {
					ctx.setRemotly(false);
					return realMethod.invoke(facadeBean, args);
				}
				catch (InvocationTargetException ex) {
					throw ex.getTargetException();
				}
				catch (RuntimeException e) {
					LOG.warn("local call to {} on {} failed: {}",
					         realMethod, facadeBean, e);
					throw e;
				}
				finally {
					ctx.pop();
				}
			}
		});
		return (T) proxy;
	}

	@Override
	public Class<?> getObjectType() {
		return facadeLocalInterface;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
