//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.job;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.validation.ValidationContext;
import org.nuclos.server.validation.Validator;
import org.nuclos.server.validation.annotation.Validation;

@Validation(entity="nuclos_jobcontroller")
public class JobValidator implements Validator {
	
	private static final Pattern EMAIL_PAT = Pattern.compile("^\\s*\\S+@\\S+\\.\\S+\\s*$");

	@Override
	public void validate(EntityObjectVO<?> object, ValidationContext context) {
		// TODO: old notification, obsolete
		final UID user = object.getFieldUid(E.JOBCONTROLLER.user);
		final Object option = object.getFieldValue(E.JOBCONTROLLER.level);
		if ((user != null && option == null) || (user == null && option != null)) {
			context.addError("job.validation.user.and.level");
		}
		
		// new notification
		final Collection<EntityObjectVO<UID>> notifications = object.getDependents().getDataPk(E.JOBNOTIFICATION.jobController);
		for (EntityObjectVO<UID> eo: notifications) {
			final UID usr = eo.getFieldUid(E.JOBNOTIFICATION.userRef);
			final String opt = eo.getFieldValue(E.JOBNOTIFICATION.level);
			final String email = eo.getFieldValue(E.JOBNOTIFICATION.email);
			if (StringUtils.isBlank(opt)) {
				context.addError("job.validation.level.empty");
			}
			if (StringUtils.isBlank(email)) {
				if (usr == null) {
					context.addError("job.validation.user.and.email.empty");
				}
			} else {
				if (usr != null) {
					context.addError("job.validation.user.and.email.nonempty");
				}
				final Matcher m = EMAIL_PAT.matcher(email.trim());
				if (!m.matches()) {
					context.addError("job.validation.email.invalid");
				}
			}
		}
	}

}
