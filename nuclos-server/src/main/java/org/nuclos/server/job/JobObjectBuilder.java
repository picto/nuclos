package org.nuclos.server.job;

import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.INucletCache;
import org.nuclos.server.nbo.NuclosObjectBuilder;

public class JobObjectBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.job";
	private static final String DEFFAULT_ENTITY_PREFIX = "JOB";
	private static final String DEFFAULT_ENTITY_POSTFIX = "JOB";
	
	public JobObjectBuilder() {}
	
	public static String getNameForFqn(String sName) {
		return formatMethodName(NuclosEntityValidator.escapeJavaIdentifier(sName, DEFFAULT_ENTITY_PREFIX)) + DEFFAULT_ENTITY_POSTFIX;
	}
	
	public static String getNucletPackageStatic(UID nucletUID, INucletCache nucletCache) {
		return NuclosObjectBuilder.getNucletPackageStatic(nucletUID, nucletCache, DEFFAULT_PACKAGE_NUCLET);
	}

	@Override
	protected void createObjects() throws CommonBusinessException {}
	
}
