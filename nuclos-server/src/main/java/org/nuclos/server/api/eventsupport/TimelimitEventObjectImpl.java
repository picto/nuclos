package org.nuclos.server.api.eventsupport;

import org.apache.log4j.Logger;
import org.nuclos.api.context.JobContext;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.job.ejb3.JobControlFacadeRemote;

public class TimelimitEventObjectImpl implements JobContext {

	private Long iSessionId;
	private static JobControlFacadeRemote FACADE;
	private String sRulename;
	private String eventClassname;
	private Object transactionalObject;
	private boolean bLastTransactionalObject = false;
	
	public TimelimitEventObjectImpl(Long pSessionId, EventSupportSourceVO esEvent) {
		this.iSessionId = pSessionId;
		this.sRulename = esEvent.getName() != null ? esEvent.getName() : esEvent.getClassname();
		this.eventClassname = esEvent.getClassname();
	}
	
	@Override
	public void setSessionId(Long pSessionId) {
		this.iSessionId = pSessionId;
	}

	@Override
	public Long getSessionId() {
		return this.iSessionId;
	}

	@Override
	public void joblog(String message) {
		getJobControlFacade().writeToJobRunMessages(getSessionId(), "INFO", message, getRulename());
	}

	@Override
	public void joblogWarn(String message) {
		getJobControlFacade().writeToJobRunMessages(getSessionId(), "WARNING", message, getRulename());
	}

	@Override
	public void joblogError(String message) {
		getJobControlFacade().writeToJobRunMessages(getSessionId(), "ERROR", message, getRulename());
	}

	@Override
	public void log(String message) {
		Logger.getLogger(this.eventClassname).info(message);
	}

	@Override
	public void logWarn(String message) {
		Logger.getLogger(this.eventClassname).warn(message);
	}

	@Override
	public void logError(String message, Exception ex) {
		Logger.getLogger(this.eventClassname).error(message, ex);
	}

	private JobControlFacadeRemote getJobControlFacade() {
		if (FACADE == null) {
			try {
				FACADE = SpringApplicationContextHolder.getBean(JobControlFacadeRemote.class);
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
		}
		return FACADE;
	}
	
	public String getRulename() {
		return this.sRulename;
	}

	@Override
	public Object getTransactionalObject() {
		return transactionalObject;
	}
	
	public void setTransactionalObject(Object transactionalObject) {
		this.transactionalObject = transactionalObject;
	}

	@Override
	public boolean isLastTransactionalObject() {
		return bLastTransactionalObject;
	}
	
	public void setLastTransactionalObject(boolean last) {
		this.bLastTransactionalObject = last;
	}
	
}
