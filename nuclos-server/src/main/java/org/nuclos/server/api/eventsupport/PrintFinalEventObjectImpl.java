//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.businessobject.facade.Modifiable;
import org.nuclos.api.context.PrintResult;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.UID;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
@Configurable
public class PrintFinalEventObjectImpl extends AbstractEntityEventObject implements org.nuclos.api.context.PrintFinalContext {
	
	private final BusinessObject bo;
	private final Collection<PrintResult> printResult;
	
	public PrintFinalEventObjectImpl(Map<String, Object> eventCache, UID entity, Modifiable bo, String pRuleClassname, UID dataLanguage) {
		super(eventCache, entity, pRuleClassname, dataLanguage);
		
		this.bo = (BusinessObject) bo;
		this.printResult = new ArrayList<PrintResult>();
	}
	
	public void addPrintresult(final PrintResult printresult) {
		printResult.add(printresult);
	}

	@Override
	public <T extends BusinessObject> T getBusinessObject(Class<T> t) {
		return super.getBusinessObject(bo, t);
	}

	@Override
	public <T extends GenericBusinessObject> T getGenericBusinessObject(final Class<T> t) throws BusinessException {
		return super.wrapBusinessObject(this.bo, t);
	}

	public void notify(String message, Priority prio) {
		super.notify(message, prio, bo, null);
	}
	
	public void notify(String message) {
		notify(message, Priority.NORMAL, bo, null);
	}

	@Override
	public Collection<PrintResult> getPrintResults() {
		return printResult;
	}

}
