//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.api.eventsupport;

import java.util.Map;

import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.GenericBusinessObject;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.notification.Priority;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.server.common.MetaProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class CustomEventObjectImpl extends AbstractEntityEventObject implements org.nuclos.api.context.CustomContext {

	private static final Logger LOG = LoggerFactory.getLogger(CustomEventObjectImpl.class);
	
	private final BusinessObject busiObject;
	private final String entityName;
	private final boolean entityIsWritable;
	private boolean updateAfterRuleExecution;
	
	public CustomEventObjectImpl(Map<String, Object> eventCache, UID entity, BusinessObject busiObject, String pRuleClassname, UID dataLanguage) {
		super(eventCache, entity, pRuleClassname, dataLanguage);
		this.busiObject = busiObject;
		final EntityMeta<Object> entityMeta = SpringApplicationContextHolder.getBean(MetaProvider.class).getEntity(entity);
		this.entityName = entityMeta.getEntityName();
		this.entityIsWritable = entityMeta.isWritable();
		this.updateAfterRuleExecution = entityMeta.isWritable();
	}

	@Override
	public <T extends BusinessObject> T getBusinessObject(Class<T> t) {
		return super.getBusinessObject(busiObject, t);
	}

	@Override
	public <T extends GenericBusinessObject> T getGenericBusinessObject(final Class<T> t) throws BusinessException {
		return super.wrapBusinessObject(this.busiObject, t);
	}

	public void notify(String message, Priority prio) {
		super.notify(message, prio,  busiObject, null);
	}
	
	public void notify(String message) {
		notify(message, Priority.NORMAL, busiObject, null);
	}

	@Override
	public void setUpdateAfterExecution(boolean bUpdateAfterExecution) {
		if (this.updateAfterRuleExecution == bUpdateAfterExecution) {
			return;
		}
		if (entityIsWritable) {
			this.updateAfterRuleExecution = bUpdateAfterExecution;
		} else {
			LOG.warn(super.getRuleClassname() + ": context.setUpdateAfterExecution(" + bUpdateAfterExecution + ") for readonly entity \"" + entityName + "\" ignored.");
		}
	}

	@Override
	public boolean getUpdateAfterExecution() {
		return this.updateAfterRuleExecution;
	}
	
}
