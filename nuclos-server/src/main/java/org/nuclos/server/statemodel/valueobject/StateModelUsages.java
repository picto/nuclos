//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.genericobject.Modules;

/**
 * class for getting initial states and state models by usage usagecriteria.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
public class StateModelUsages implements Serializable {

	/**
	 * A state (model) usage.
	 */
	public static class StateModelUsage implements Serializable {

		private final UID stateModelUID;
		private final UID initialStateUID;
		private final UsageCriteria usagecriteria;

		public StateModelUsage(UID stateModelUID, UID initialStateUID, UsageCriteria usagecriteria) {
			this.stateModelUID = stateModelUID;
			this.initialStateUID = initialStateUID;
			this.usagecriteria = usagecriteria;
		}

		public UID getStateModelUID() {
			return stateModelUID;
		}

		public UID getInitialStateUID() {
			return initialStateUID;
		}

		public UsageCriteria getUsageCriteria() {
			return this.usagecriteria;
		}

	}	// inner class StateModelUsage

	/**
	 * §todo Is the order in this list relevant?
	 */
	private final List<StateModelUsage> lstStateUsage = new LinkedList<StateModelUsage>();

	public StateModelUsages() {
	}

	public void add(StateModelUsage stateModelUsage) {
		lstStateUsage.add(stateModelUsage);
	}

	public UID getStateModel(UsageCriteria usagecriteria) {
		final StateModelUsage stateModelUsage = this.getStateModelUsage(usagecriteria);
		if (stateModelUsage == null) {
			throw new NuclosFatalException();
		}
		return stateModelUsage.getStateModelUID();
	}

	/**
	 * §precondition usagecriteria != null
	 * §precondition usagecriteria.getModuleId() != null
	 */
	public UID getInitialStateUID(UsageCriteria usagecriteria) {
		if (usagecriteria == null) {
			throw new NullArgumentException("usagecriteria");
		}
		if (usagecriteria.getEntityUID() == null) {
			throw new NullArgumentException("usagecriteria.getModuleId()");
		}
		final StateModelUsage stateModelUsage = this.getStateModelUsage(usagecriteria);
		if (stateModelUsage == null) {
			throw new NuclosFatalException(StringUtils.getParameterizedExceptionMessage("statemodel.usages.error.null", Modules.getInstance().getLabel(usagecriteria.getEntityUID()), usagecriteria));
				//"F\u00fcr die Verwendung des Moduls " + Modules.getInstance().getEntityLabelByModuleId(usagecriteria.getModuleId()) + " " + usagecriteria + " existiert kein Statusmodell.");
		}
		return stateModelUsage.getInitialStateUID();
	}

	/**
	 * §todo refactor: use this algorithm in UsageCriteria.getBestMatchingUsageCriteria - it's clearer.
	 * 
	 * @param usagecriteria
	 * @return the usage for the state model with the given usagecriteria
	 */
	public StateModelUsage getStateModelUsage(final UsageCriteria usagecriteria) {
		// 1. find matching usages (candidates):
		final List<StateModelUsage> lstCandidates = CollectionUtils.select(lstStateUsage, new Predicate<StateModelUsage>() {
			@Override
			public boolean evaluate(StateModelUsage o) {
				return o.getUsageCriteria().isMatchFor(usagecriteria);
			}
		});

		final StateModelUsage result;
		if (lstCandidates.isEmpty()) {
			/** @todo rather throw an exception here */
			result = null;
		}
		else {
			// 2. These candidates are totally ordered with respect to isLessOrEqual(). The result is the greatest of these
			// candidates.
			result = Collections.max(lstCandidates, new Comparator<StateModelUsage>() {
				@Override
				public int compare(StateModelUsage su1, StateModelUsage su2) {
					return su1.getUsageCriteria().compareTo(su2.getUsageCriteria());
				}
			});
		}
		return result;
	}

	public List<UID> getStateModelUIDsByStatusUID(final UID statusUID) {
		final List<UID> result = new ArrayList<UID>();
		for (StateModelUsage smo : lstStateUsage) {
			for(StateVO stateVO : StateCache.getInstance().getStatesByModel(smo.getStateModelUID())) {
				if (stateVO.getId().equals(statusUID)) {
					result.add(smo.getStateModelUID());
				}
			}
		}
		return result;
	}

	public Set<UID> getStateModelUIDsByEntity(final UID entityUID) {
		final Set<UID> result = new HashSet<UID>();
		for (StateModelUsage smo : lstStateUsage) {
			if (entityUID.equals(smo.getUsageCriteria().getEntityUID())) {
				result.add(smo.getStateModelUID());
			}
		}
		return result;
	}
	
	public Set<UsageCriteria> getUsageCriteriasByStateModelUID(final UID stateModelUID) {
		final Set<UsageCriteria> result = new HashSet<UsageCriteria>();
		for (StateModelUsage smo : lstStateUsage) {
			if (stateModelUID.equals(smo.getStateModelUID())) {
				result.add(smo.getUsageCriteria());
			}
		}
		return result;
	}

}	// class StateModelUsages