//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.statemodel.valueobject;

import javax.annotation.PostConstruct;

import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.server.cluster.cache.ClusterCache;
import org.nuclos.server.cluster.jms.ClusterActionFactory;
import org.nuclos.server.cluster.jms.NuclosClusterAction;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Singleton class for getting initial states and state models by usage criteria.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @version 00.01.000
 */
@Component
public class StateModelUsagesCache implements ClusterCache {

	private static StateModelUsagesCache INSTANCE;
	
	// 

	private StateModelUsages stateModelUsages;
	
	private SpringDataBaseHelper dataBaseHelper;

	public static StateModelUsagesCache getInstance() {
		if (INSTANCE.stateModelUsages == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	StateModelUsagesCache() {
		INSTANCE = this;
	}
	
	@Autowired
	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
		this.dataBaseHelper = dataBaseHelper;
	}
	
	@PostConstruct
	final void init() {
		validate();
	}

	/**
	 * �postcondition result != null
	 */
	public synchronized StateModelUsages getStateUsages() {
		if (this.stateModelUsages == null) {
			this.validate();
		}
		final StateModelUsages result = this.stateModelUsages;
		assert result != null;
		return result;
	}

	private synchronized void revalidate() {
		this.invalidate();
		this.validate();
	}

	private synchronized void invalidate() {
		this.stateModelUsages = null;
	}

	private synchronized void validate() {
		this.stateModelUsages = buildCache();
	}

	/**
	 * postcondition result != null
	 */
	private StateModelUsages buildCache() {
		final StateModelUsages result = new StateModelUsages();

		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom s = query.from(E.STATE, "s");
		DbJoin t = s.join(E.STATETRANSITION, JoinType.INNER, E.STATETRANSITION.state2, "t");
		DbJoin u = s.join(E.STATEMODELUSAGE, JoinType.INNER, "u").on(E.STATE.model, E.STATEMODELUSAGE.statemodel);
		query.multiselect(
		   s.baseColumn(E.STATE.model),
		   s.baseColumn(E.STATE),
		   u.baseColumn(E.STATEMODELUSAGE.nuclos_module),
		   u.baseColumn(E.STATEMODELUSAGE.process));
		query.where(t.baseColumn(E.STATETRANSITION.state1).isNull());
		query.orderBy(builder.asc(u.baseColumn(E.STATEMODELUSAGE.nuclos_module)),
		   builder.asc(u.baseColumn(E.STATEMODELUSAGE.process)));
		
		for (DbTuple tuple : dataBaseHelper.getDbAccess().executeQuery(query)) {
			final UsageCriteria usagecriteria = new UsageCriteria(
				tuple.get(2, UID.class), tuple.get(3, UID.class), null, null);
			result.add(new StateModelUsages.StateModelUsage(
				tuple.get(0, UID.class), tuple.get(1, UID.class), usagecriteria));
		}

		assert result != null;
		return result;
	}

	@Override
	public void invalidateCache(boolean notifyClients, boolean notifyClusterCloud) {
		revalidate();
		if(notifyClusterCloud) { 
			notifyClusterCloud();
		}
	}

	@Override
	public void notifyClusterCloud() {
		NuclosClusterAction action = ClusterActionFactory.createClusterAction(NuclosClusterAction.STATEMODELUSAGE_ACTION);
		NuclosJMSUtils.sendObjectMessage(action, JMSConstants.TOPICNAME_CLUSTER, null);		 
	}

	@Override
	public void registerCache() {
	}

	@Override
	public void deregisterCache() {
	}

	@Override
	public String getName() {
		return null;
	}

}	// class StateModelUsagesCache
