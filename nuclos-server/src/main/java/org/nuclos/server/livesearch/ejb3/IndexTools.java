package org.nuclos.server.livesearch.ejb3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.nuclos.common.collection.Pair;
import org.nuclos.server.common.NuclosSystemParameters;

/**
 * @author Oliver Brausch
 */

public final class IndexTools {
	private static final Logger LOG = Logger.getLogger(IndexTools.class);

	private static IndexTools INSTANCE;
	
	public static IndexTools INSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new IndexTools();
		}
		return INSTANCE;
	}
	
	private IndexTools() {
	}
	
	private final CharArraySet stopWords = StopFilter.makeStopSet(new ArrayList<String>());
	private final StandardAnalyzer analyzer = new StandardAnalyzer(stopWords);

	private final File indexDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.INDEX_PATH);
	private final File indexHomeDir = new File(System.getProperty("user.home") + "/.index");

	private boolean enabled = true;
	
	public Analyzer getAnalyzer() {
		return analyzer;
	}
	
	public NuclosIndexWriter createWriter() throws IOException {
		FSDirectory fsdir = FSDirectory.open(getIndexDir().toPath());
		IndexWriterConfig config = new IndexWriterConfig(getAnalyzer());
		
		return new NuclosIndexWriter(fsdir, config);
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public boolean switchedOff() {
		return !enabled || (indexDir != null && "off".equals(indexDir.getName()));
	}
	
	public File getIndexDir() throws IOException {
		if (switchedOff()) {
			throw new IOException("IndexDir: Index is switched off.");
		}
		
		File dir = indexDir != null ? indexDir : indexHomeDir;
		if (dir.exists() && !dir.isDirectory() && !dir.delete()) {
			throw new IOException("IndexDir: Could not remove File: " + dir);
		}
		
		if (!dir.exists() && !dir.mkdirs()) {
			throw new IOException("IndexDir: Could not create lucene directory: " + dir);
		}
		
		return dir;		
	}
	
	public boolean checkIndexDir() {
		File dir = indexDir != null ? indexDir : indexHomeDir;
		return dir.exists();
	}
	
	public void removeIndexDir() throws IOException {
		File renamedDir = new File("todelete" + Math.random());
		getIndexDir().renameTo(renamedDir);
		FileUtils.deleteDirectory(renamedDir);			
	}
	
	private String getIndexFileName() {
		return ".nuclosindex" + NuclosIndexWriter.getVersion();
	}
	
	public void touchVersionFile() throws IOException {
		FileUtils.touch(new File(getIndexDir(), getIndexFileName()));
	}
	
	/**
	 * 
	 * @return boolean false, if it doesn't exist. returns true if version file exists
	 * @throws IOException
	 */
	public boolean versionFileExists() throws IOException {
		return FileUtils.directoryContains(getIndexDir(), new File(getIndexDir(), getIndexFileName()));
	}
	
	public List<Pair<String, String>> search(String searchString, int max, boolean references) {
        List<Pair<String, String>> ret = new ArrayList<Pair<String, String>>();
        
		final String s;
		if (references) {
			s = searchString.toLowerCase();
		} else {
			s = prepareSearchString(searchString, false);
		}
		
	    try {
			Directory index = FSDirectory.open(getIndexDir().toPath());
			IndexReader reader = DirectoryReader.open(index);
			IndexSearcher searcher = new IndexSearcher(reader);
			TopScoreDocCollector collector = TopScoreDocCollector.create(max);
			
			Query q = new QueryParser(references ? NuclosIndexWriter.IDREF : NuclosIndexWriter.IDTEXT, getAnalyzer()).parse(s);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
		
			for (ScoreDoc hit: hits) {
				Document d = searcher.doc(hit.doc);
				String sPk = d.get(NuclosIndexWriter.IDPK);
				String sField = d.get(NuclosIndexWriter.IDFIELD);
				
				ret.add(new Pair<String, String>(sPk, sField));
			}
			
		} catch (IOException | ParseException e) {
			LOG.warn(e.getMessage(), e);
		}
        
        return ret;
	}
	
	private static String prepareSearchString(String searchString, boolean expert) {
	    String s = searchString.trim();
	    
	    if (!expert) {
		    boolean quotated = s.startsWith("\"") && s.endsWith("\"") && s.length() >= 2;
		    
		    if (quotated) {
		    	s = s.substring(1, s.length() - 1);
		    }
		    
	    	s = QueryParser.escape(s);
	    	
	    	if (!quotated) {
	    		s += "*";
	    	} else {
		    	s = "\"" + s + "\"";	    		
	    	}
	    }
	    
	    return s;
	}

}