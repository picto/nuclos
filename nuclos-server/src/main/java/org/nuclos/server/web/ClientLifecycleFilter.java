//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * A servlet filter used to track download (client) JARs and JNLP files.
 * <p>
 * This runs within the main spring context with the help of 
 * {@link org.springframework.web.filter.DelegatingFilterProxy}.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.0.10
 */
// startup context: @Component does NOT work (tp)
public class ClientLifecycleFilter implements Filter {
	
	@Autowired
	private ClientLifecycle clientLifecycle;
	
	ClientLifecycleFilter() {
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// Attention: not invoked by Spring (tp)
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		doHttpFilter((HttpServletRequest) request, (HttpServletResponse) response, chain);
	}
	
	private void doHttpFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		
		clientLifecycle.webStartRequest(request);
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// Attention: not invoked by Spring (tp)
	}

}
