//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.web;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.nuclos.api.ide.SourceItemFacadeRemote;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;

public class SourceItemDownloadServlet extends FileServlet {
	
	private static final String BASE = NuclosCodegeneratorConstants.GENERATOR_FOLDER.toString();
	private static final String BASE_URI_STRING = (new File(BASE)).toURI().toString();
	
	private static final int BASE_URI_STRING_LENGTH = BASE_URI_STRING.length();
	
	//

	private SourceItemFacadeRemote sourceItemFacadeRemote;
	
	public SourceItemDownloadServlet() {
	}
	
	@Override
	public void init() throws ServletException {
		super.init();
	}
	
	@Override
	protected String getBasePath() {
		return BASE;
	}

	private SourceItemFacadeRemote getSourceItemFacadeRemote() {
		if (sourceItemFacadeRemote == null) {
			sourceItemFacadeRemote = SpringApplicationContextHolder.getBean(SourceItemFacadeRemote.class);
		}
		return sourceItemFacadeRemote;
	}
	
	/*
	 * http://stackoverflow.com/questions/132052/servlet-for-serving-static-content
	 */
	/*
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if (!ApplicationProperties.getInstance().isFunctionBlockDev()) {
			resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}		
		
		final RequestDispatcher rd = getServletContext().getNamedDispatcher("default");
		final HttpServletRequest wrapped = new HttpServletRequestWrapper(req) {
			@Override
			public String getServletPath() {
				return "";
			}
		};
		rd.forward(wrapped, resp);
	}
	 */

	protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean content)
			throws IOException {
		if (!NuclosSystemParameters.is(NuclosSystemParameters.ENVIRONMENT_DEVELOPMENT)) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}
		String pi = request.getPathInfo();
		boolean src;
		if (pi.endsWith(".java")) {
			pi = pi.substring(1, pi.length() - 5);
			src = true;
		}
		else if (pi.endsWith(".class")){
			pi = pi.substring(1, pi.length() - 6);
			src = false;
		}
		else if (pi.endsWith(".jar")){
			pi = pi.substring(1);
			src = false;
		}
		else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		pi = pi.replace('/', '.');
		final URL url;
		try {
			url = getSourceItemFacadeRemote().getDownloadURL(pi, src);
		} catch (IllegalAccessException e) {
			throw new NuclosFatalException(e);
		}
		if (url == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		final HttpServletRequestWrapper wrapper = new HttpServletRequestWrapper(request) {
			@Override
			public String getPathInfo() {
				String result = url.toExternalForm();
				final int idx = result.indexOf(BASE_URI_STRING);
				if (idx >= 0) {
					result = result.substring(idx + BASE_URI_STRING_LENGTH);
				}
				return result;
			}
		};
		super.processRequest(wrapper, response, content);
	}
	
}
