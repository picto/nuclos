//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import org.nuclos.common2.LangUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.resource.valueobject.ResourceFile;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class FileSerializable implements Converter {
	
	public static void register(XStream xstream) {
		xstream.alias("file", ResourceFile.class);
		xstream.alias("file", GenericObjectDocumentFile.class);
		xstream.registerConverter(new FileSerializable());
	}

	@Override
	public boolean canConvert(Class type) {
		return ResourceFile.class.equals(type) || GenericObjectDocumentFile.class.equals(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer,	MarshallingContext context) {
		org.nuclos.common2.File file = (org.nuclos.common2.File) source;
		writer.addAttribute("class", (LangUtils.equal(ResourceFile.class, file.getClass()) ? 0 : 1) + "");
		writer.startNode("filename");
		writer.setValue(file.getFilename());
		writer.endNode();
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		Object result = null;
		int iClass = Integer.parseInt(reader.getAttribute("class"));
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			String field = reader.getNodeName();
			if ("filename".equals(field)) {
				String filename = reader.getValue();
				switch (iClass) {
					case 0:
						result = new ResourceFile(filename, new byte[] {});
						break;
					default:
						result = new GenericObjectDocumentFile(filename, new byte[] {});
				}
			} 
			reader.moveUp();
		}
		return result;
	}
	
}
