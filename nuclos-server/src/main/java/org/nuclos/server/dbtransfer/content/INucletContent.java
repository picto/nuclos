//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common.dbtransfer.TransferOption;

public interface INucletContent {
	
	public EntityMeta<UID> getEntity();
	
	public EntityMeta<UID> getParentEntity();
	
	public FieldMeta<UID> getFieldToParent();
	
	public boolean canUpdate();
	
	public boolean canDelete();
	
	public boolean isEnabled();
	
	public void setEnabled(boolean enabled);
	
	public boolean validate(TransferEO teo, ValidationType type, NucletContentMap importContentMap, 
			Set<UID> existingNucletUIDs, ValidityLogEntry log, Map<TransferOption, Serializable> transferOptions, boolean usesDataLanguages);
	
	public List<EntityObjectVO<UID>> getNcObjects(Set<UID> nucletUIDs);
	
	public List<EntityObjectVO<UID>> getNcObjectsByParent(EntityObjectVO<UID> parent);
	
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly);
	
	public void deleteNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean testOnly);
	
	public String getIdentifier(EntityObjectVO<UID> eo, NucletContentMap importContentMap);

	public boolean hasNameIdentifier(EntityObjectVO<UID> eo);

	public UID getIdentifierField();
	
	public boolean isIgnoreReferenceToNuclet();

	public EntityObjectVO<UID> readNc(EntityObjectVO<UID> ncObject);
	
	public CollectableSearchCondition cachingCondition();

}
