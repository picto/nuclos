//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.serializable;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JsonUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dbtransfer.NucletContentMap;
import org.nuclos.common.dbtransfer.TransferConstants;
import org.nuclos.common.dbtransfer.TransferEO;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dbtransfer.TransferUtils;
import org.nuclos.server.dbtransfer.content.IExternalizeBytes;
import org.nuclos.server.dbtransfer.content.INucletContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class NucletContentSerializableController implements Converter, TransferConstants {
	
	private static final Logger LOG = LoggerFactory.getLogger(
		NucletContentSerializableController.class);
	
	/**
	 * for unmarshal
	 */
	private final Map<String, byte[]> externalizedFiles;
	private final List<INucletContent> contentTypes;
	private final NucletContentMap importContentMap;
	private final Collection<Runnable> afterDeserialize;

	private NucletContentSerializableController(Map<String, byte[]> externalizedFiles, List<INucletContent> contentTypes, NucletContentMap importContentMap,
			Collection<Runnable> afterDeserialize) {
		super();
		this.externalizedFiles = externalizedFiles;
		this.contentTypes = contentTypes;
		this.importContentMap = importContentMap;
		this.afterDeserialize = afterDeserialize;
	}

	public static void register(XStream xstream) {
		xstream.alias("eo", TransferEO.class);
		xstream.registerConverter(new NucletContentSerializableController(null, null, null, null));
	}
	
	public static void register(XStream xstream, Map<String, byte[]> externalizedFiles, List<INucletContent> contentTypes, NucletContentMap importContentMap,
			Collection<Runnable> afterDeserialize) {
		xstream.alias("eo", TransferEO.class);
		xstream.registerConverter(new NucletContentSerializableController(externalizedFiles, contentTypes, importContentMap, afterDeserialize));
	}
	
	@Override
	public boolean canConvert(Class type) {
		return TransferEO.class.equals(type);
	}

	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer,	MarshallingContext context) {
		TransferEO teo = (TransferEO) source;
		EntityObjectVO<UID> eo = teo.eo;
		UID entityUID = eo.getDalEntity();
		EntityMeta<UID> eMeta = MetaProvider.getInstance().getEntity(entityUID);
		if (entityUID == null) {
			throw new IllegalArgumentException("entity of object must not be null");
		}
		writer.addAttribute("entity", eMeta.getEntityName());
		writer.addAttribute("entityuid", entityUID.getString());
		writer.addAttribute("uid", teo.eo.getPrimaryKey().getString());
		writer.addAttribute("version", Integer.toString(eo.getVersion()));
		
		// Nuclet content marshal
		if (E.STATEMODEL.checkEntityUID(entityUID)) {
			StateModelContentSerializable.marshal(teo, writer, context);
		} else {
			DefaultContentSerializable.marshal(teo, writer, context);
		}
		
		if (!teo.externalizedFiles.isEmpty()) {
			writer.startNode(FIELD_FOR_EXTERNALIZED_FILES);
			for (FieldMeta<?> externalizedField : CollectionUtils.sorted(teo.externalizedFiles.keySet(), new Comparator<FieldMeta<?>>() {
				@Override
				public int compare(FieldMeta<?> o1, FieldMeta<?> o2) {
					return LangUtils.compare(o1.getUID(), o2.getUID());
				}})) {
				writer.startNode(externalizedField.getFieldName());
				writer.addAttribute("fielduid", externalizedField.getUID().getString());
				writer.setValue(teo.externalizedFiles.get(externalizedField));
				writer.endNode();
			}
			writer.endNode();
		}
	}

	@Override
	public Object unmarshal(final HierarchicalStreamReader reader, UnmarshallingContext context) {
		final String entityName = reader.getAttribute("entity");
		final UID entityUID = UID.parseUID(reader.getAttribute("entityuid"));
		final UID uid = UID.parseUID(reader.getAttribute("uid"));
		final EntityObjectVO<UID> eo = new EntityObjectVO<UID>(entityUID);
		eo.setPrimaryKey(uid);
		final TransferEO teo = new TransferEO(eo);
		
		try {
			final IExternalizeBytes exby;
			if (E.isNuclosEntity(entityUID)) {
				IExternalizeBytes tmpExby = null;
				try {
					Object nc = TransferUtils.getContentType(contentTypes, E.<UID>getByUID(entityUID));
					if (nc instanceof IExternalizeBytes) {
						tmpExby = (IExternalizeBytes) nc;
					}
				} catch (Exception ex) {
					// ignore... nc for nuclos_parameter is not present during prepare.
				}
				exby = tmpExby;
			} else {
				exby = null;
			}
//			EntityMeta<UID> eMeta = MetaProvider.getInstance().getEntity(entityUID);
			final Map<UID, FieldMeta<?>> efMetas = MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUID);
			int version = Integer.parseInt(reader.getAttribute("version"));
			eo.setVersion(version);
			
			Map<String, Runnable> childRunners = new HashMap<String, Runnable>();
			Runnable externalizedFilesRunnable = new Runnable() {
				@Override
				public void run() {
					while (reader.hasMoreChildren()) {
						reader.moveDown();
						try {
//							String externalizedFieldName = reader.getNodeName();
							UID externalizedFieldUID = UID.parseUID(reader.getAttribute("fielduid"));
							if (efMetas.containsKey(externalizedFieldUID)) {
								FieldMeta<?> efMeta = efMetas.get(externalizedFieldUID);
								String externalizedPath = reader.getValue();
								if (externalizedFiles.containsKey(externalizedPath)) {
									byte[] bytes = externalizedFiles.get(externalizedPath);
									if (String.class.getName().equals(efMeta.getDataType())) {
										// clob
										String sValue = new String(bytes, "UTF-8");
										eo.setFieldValue(externalizedFieldUID, sValue);
									} else if (JsonObject.class.equals(efMeta.getJavaClass())) {
										// json object
										String prettyValue = new String(bytes, "UTF-8");
										JsonObject jsonObject = JsonUtils.stringToObject(prettyValue);
										eo.setFieldValue(externalizedFieldUID, jsonObject);
									} else {
										// byte content
										if (exby != null) {
											exby.importBytes(eo, efMeta, bytes);
										}
									}
								}
							}
						} catch (Exception ex) {
							LOG.error("Error during unmarshal eoml. entity={}, uid={}",
							                        entityName, teo.getUID().getString(), ex);
						} finally {
							reader.moveUp();
						}
					}
				}
			};
			childRunners.put(FIELD_FOR_EXTERNALIZED_FILES, externalizedFilesRunnable);
			
			// Nuclet content marshal
			if (E.STATEMODEL.checkEntityUID(entityUID)) {
				StateModelContentSerializable.unmarshal(teo, reader, context, childRunners, importContentMap, afterDeserialize);
			} else {
				DefaultContentSerializable.unmarshal(teo, reader, context, childRunners);
			}
			
			return teo;
		} catch (Exception ex) {
			LOG.error("Error during unmarshal eoml. entity={}, uid={}",
			          entityName, teo.getUID().getString(), ex);
			return null;
		} 
	}
	
}
