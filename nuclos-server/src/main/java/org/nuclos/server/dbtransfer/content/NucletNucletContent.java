//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dbtransfer.content;

import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.EntityObjectVO;


public class NucletNucletContent extends DefaultNucletContent {

	public NucletNucletContent(List<INucletContent> contentTypes) {
		super(E.NUCLET, contentTypes, true);
	}
	
	@Override
	public boolean insertOrUpdateNcObject(DalCallResult result, EntityObjectVO<UID> ncObject, boolean isNuclon, boolean testOnly) {
//		EntityObjectVO<UID> existing = NucletDalProvider.getInstance().getEntityObjectProcessor(E.NUCLET).getByPrimaryKey(ncObject.getPrimaryKey());
		EntityObjectVO<UID> existing = getEO(E.NUCLET.getUID(), ncObject.getPrimaryKey());
		if (existing != null) {
			ncObject.setFieldValue(E.NUCLET.source, existing.getFieldValue(E.NUCLET.source));
		} else {
			ncObject.setFieldValue(E.NUCLET.source, false);
		}
		
		return super.insertOrUpdateNcObject(result, ncObject, isNuclon, testOnly);
	}

	@Override
	public List<EntityObjectVO<UID>> getNcObjects(final Set<UID> nucletUIDs) {
		return CollectionUtils.select(
					super.getNcObjects(nucletUIDs), 
					new Predicate<EntityObjectVO<UID>>() {
						@Override
						public boolean evaluate(EntityObjectVO<UID> t) {
							return nucletUIDs.contains(t.getPrimaryKey());
						}
					});
	}
	
}
