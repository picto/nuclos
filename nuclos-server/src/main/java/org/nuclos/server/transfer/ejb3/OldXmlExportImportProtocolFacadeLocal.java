//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.transfer.ejb3;

import java.util.Date;

import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;

// @Local
public interface OldXmlExportImportProtocolFacadeLocal {

	/**
	 * creates the protocol header for an export/import transaction
	 */
	Integer writeExportImportLogHeader(String sType,
		String sUserName, Date dImportDate, String sImportEntityName,
		String sImportFileName) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * creates a protocol entry for the given protocol header id
	 */
	void writeExportImportLogEntry(Integer iParentId,
		String sMessageLevel, String sImportAction, String sImportMessage,
		Integer iActionNumber) throws CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException;

	/**
	 * add content of xml file to protocol header of given id
	 */
	void addFile(Integer iParentId, byte[] ba);

}
