package org.nuclos.server.rest.layout.uicomponent;

public class Label extends Element {
    public String value;
    public String boAttrId;

    public Label(String boAttrId, String value) {
        this.boAttrId = boAttrId;
        this.value = value;
    }

    public String toString() {
        return "<span>" + value + "</span>";
    }
}
