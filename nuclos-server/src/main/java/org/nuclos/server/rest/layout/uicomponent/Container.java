package org.nuclos.server.rest.layout.uicomponent;

import java.util.ArrayList;
import java.util.List;

public class Container extends Element {
    public List<Element> children = new ArrayList<Element>();

    public String printChildren() {
        String result = "";
        for (Element child : children) {
            result += child;
        }
        return result;
    }

    public String toString() {
        return "<div>" + printChildren() + "</div>";
    }
}
