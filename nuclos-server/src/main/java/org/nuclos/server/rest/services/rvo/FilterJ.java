package org.nuclos.server.rest.services.rvo;

import static org.nuclos.server.rest.services.helper.WebContext.DEFAULT_RESULT_LIMIT;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.ws.rs.core.Response.Status;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableSorting;
import org.nuclos.common.collect.collectable.searchcondition.AbstractCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparisonWithOtherField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableInCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSubCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.entityobject.CollectableEOEntityField;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.searchfilter.EntitySearchFilter2;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.rest.ejb3.IFilterJ;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.IWebContext;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.AllComparisonExpression;
import net.sf.jsqlparser.expression.AnyComparisonExpression;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.CaseExpression;
import net.sf.jsqlparser.expression.DateValue;
import net.sf.jsqlparser.expression.DoubleValue;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.expression.InverseExpression;
import net.sf.jsqlparser.expression.JdbcParameter;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.TimeValue;
import net.sf.jsqlparser.expression.TimestampValue;
import net.sf.jsqlparser.expression.WhenClause;
import net.sf.jsqlparser.expression.operators.arithmetic.Addition;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseAnd;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseOr;
import net.sf.jsqlparser.expression.operators.arithmetic.BitwiseXor;
import net.sf.jsqlparser.expression.operators.arithmetic.Concat;
import net.sf.jsqlparser.expression.operators.arithmetic.Division;
import net.sf.jsqlparser.expression.operators.arithmetic.Multiplication;
import net.sf.jsqlparser.expression.operators.arithmetic.Subtraction;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.Between;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.ExistsExpression;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.expression.operators.relational.GreaterThan;
import net.sf.jsqlparser.expression.operators.relational.GreaterThanEquals;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.IsNullExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsListVisitor;
import net.sf.jsqlparser.expression.operators.relational.LikeExpression;
import net.sf.jsqlparser.expression.operators.relational.Matches;
import net.sf.jsqlparser.expression.operators.relational.MinorThan;
import net.sf.jsqlparser.expression.operators.relational.MinorThanEquals;
import net.sf.jsqlparser.expression.operators.relational.NotEqualsTo;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.replace.Replace;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.FromItemVisitor;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.statement.select.SelectItemVisitor;
import net.sf.jsqlparser.statement.select.SelectVisitor;
import net.sf.jsqlparser.statement.select.SubJoin;
import net.sf.jsqlparser.statement.select.SubSelect;
import net.sf.jsqlparser.statement.select.Union;
import net.sf.jsqlparser.statement.truncate.Truncate;
import net.sf.jsqlparser.statement.update.Update;

public class FilterJ implements IFilterJ {

	private static final Logger LOG = LoggerFactory.getLogger(FilterJ.class);
	
	private final IWebContext webContext;
	private final JsonObject queryContext;
	
	private String search;
	private String searchFilterId;
	
	private Long offset;
	private Long chunkSize;
	private Boolean countTotal = false;
	
	@Deprecated
	private String fields;
	@Deprecated
	private String searchCondition;
	@Deprecated
	private String sortExpression;
	
	private String attributes;
	private String where;
	private String orderBy;
	
	private Boolean withMetaLink = null;
	private Boolean withTitleAndInfo = null;
	private Boolean withLayoutLink = null;
	
	
	public FilterJ(IWebContext webContext, JsonObject queryContext, Long defaultChunkSize) {
		this.webContext = webContext;
		this.queryContext = queryContext;
		
		this.offset = get("offset", Long.class);
		this.chunkSize = get("chunkSize", Long.class);
		if (this.chunkSize == null) {
			// old style
			this.chunkSize = getWebContextValue("chunksize", Long.class);
		}
		if (this.chunkSize == null) {
			this.chunkSize = defaultChunkSize;
		}
		
		this.countTotal = get("countTotal", Boolean.class);
		if (countTotal == null) {
			// old style
			this.countTotal = getWebContextValue("gettotal", Boolean.class);
		}
		
		this.search = get("search", String.class);
		this.searchFilterId = get("searchFilterId", String.class);
		if (searchFilterId == null) {
			this.searchFilterId = getWebContextValue("searchFilter", String.class);
		}
		
		this.fields = getWebContextValue("fields", String.class);
		this.sortExpression = getWebContextValue("sort", String.class);
		this.searchCondition = getWebContextValue("searchCondition", String.class);
		
		this.where = getWhereFromQuery();
		if (this.where == null) {
			this.where = getWebContextValue("where", String.class);
		}
		this.attributes = getAttributesFromQuery();
		if (this.attributes == null) {
			this.attributes = getWebContextValue("attributes", String.class);
		}
		if (this.attributes != null && this.attributes.indexOf('_') == -1) { // attributes are not FQNs
			String boMetaId = Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID());
			List<String> attributes = new ArrayList<String>();
			if (this.attributes.length() > 0) {
				for(String attribute : this.attributes.split(",")) {
					attributes.add(boMetaId + "_" + attribute);
				}
				this.attributes = org.apache.commons.lang.StringUtils.join(attributes, ",");
			} else {
				this.attributes = null;
			}
		}
		this.orderBy = getOrderByFromQuery();
		if (this.orderBy == null) {
			this.orderBy = getWebContextValue("orderBy", String.class);
		}
		
		this.withTitleAndInfo = get("withTitleAndInfo", Boolean.class);
		this.withMetaLink = get("withMetaLink", Boolean.class);
		this.withLayoutLink = get("withLayoutLink", Boolean.class);
	}
	
	private <T> T get(String key, Class<T> clzz) {
		return get(key, key, clzz);
	}
	
	private <T> T get(String keyWeb, String keyReq, Class<T> clzz) {
		T valQuery = getQueryContextValue(keyReq, clzz);
		if (valQuery != null) {
			return valQuery;
		}
		T valWeb = getWebContextValue(keyWeb, clzz);
		return valWeb;
	}
	
	private <T> T getWebContextValue(String key, Class<T> clzz) {
		return webContext.getFirstParameter(key, clzz);
	}
	
	private <T> T getQueryContextValue(String key, Class<T> clzz) {
		if (queryContext != null) {
			if (queryContext.containsKey(key) && !queryContext.isNull(key)) {
				JsonValue jsonValue = queryContext.get(key);
				if (String.class.equals(clzz)) {
					if (jsonValue instanceof JsonString) {
						return (T)(((JsonString) jsonValue).getString());
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a number!");
					}
				} else 
				if (Long.class.equals(clzz)) {
					if (jsonValue instanceof JsonNumber) {
						return (T)new Long(((JsonNumber) jsonValue).longValue());
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a number!");
					}
				} else 
				if (Boolean.class.equals(clzz)) {
					if (jsonValue.equals(JsonValue.TRUE)) {
						return (T)Boolean.TRUE;
					} else if (jsonValue.equals(JsonValue.FALSE)) {
						return (T)Boolean.FALSE;
					} else {
						throw new IllegalArgumentException("JsonValue for " + key + " is not a boolean!");
					}
				} else {
					throw new IllegalArgumentException("JsonValue type " + clzz.getCanonicalName() + " is not supported!");
				}
			}
		}
		return null;
	}
	
	/** translates the JSON value for the key 'attributes'. Something like this...
	[
		{"boAttrId": "example_rest_Order_customer"},
		{"boAttrId": "example_rest_Order_orderDate"}
	]
	*/
	private String getAttributesFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("attributes") && !queryContext.isNull("attributes")) {
				JsonValue jsonAttributes = queryContext.get("attributes");
				if (jsonAttributes instanceof JsonArray) {
					JsonArray attributesArray = (JsonArray) jsonAttributes;
					StringBuilder result = new StringBuilder();
					for (int i = 0; i < attributesArray.size(); i++) {
						JsonValue jsonValue = attributesArray.get(i);
						if (jsonValue instanceof JsonObject) {
							String boAttrId = ((JsonObject) jsonValue).getString("boAttrId");							
							if (result.length() > 0) {
								result.append(", ");
							}
							result.append(boAttrId);
						}
					}
					return result.toString();
					
				} else if (jsonAttributes instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonAttributes).getString();
				}
			}
		}
		return null;
	}
		
	/** translates the JSON value for the key 'where'. Something like this...
	{
		"clause": "customerId = 40000294 AND orderDate >= '2014-06-05' AND self.id IN (SELECT orderPositionRef FROM orderPosition WHERE price > 800)",
		"aliases": {
			"customerId": {"boAttrId": "example_rest_Order_customer"},
			"orderDate": {"boAttrId": "example_rest_Order_orderDate"},
			"orderPositionRef": {"boAttrId": "example_rest_OrderPosition_order"},
			"price": {"boAttrId": "example_rest_OrderPosition_price"},
			"self": {"boMetaId": "example_rest_Order"},
			"orderPosition": {"boMetaId": "example_rest_OrderPosition"}
		}
	}
	*/
	private String getWhereFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("where") && !queryContext.isNull("where")) {
				JsonValue jsonWhere = queryContext.get("where");
				
				if (jsonWhere instanceof JsonObject) {
					JsonObject whereObject = (JsonObject) jsonWhere;
					String whereString = whereObject.getString("clause");
					JsonObject aliasesObject = whereObject.getJsonObject("aliases");
					
					// longer keys first
					ArrayList<String> sortedKeys = CollectionUtils.sorted(aliasesObject.keySet(), new Comparator<String>() {
						@Override
						public int compare(String s1, String s2) {
							int l1 = s1.length();
							int l2 = s2.length();
							if (l1 > l2) {
								return -1;
							} else if (l1 < l2) {
								return 1;
							}
							return 0;
						}
					});
					
					for (String key : sortedKeys) {
						JsonValue jsonValue = aliasesObject.get(key);
						if (jsonValue instanceof JsonObject) {
							JsonObject aliasObject = (JsonObject) jsonValue;
							if (aliasObject.containsKey("boAttrId")) {
								String boAttrId = aliasObject.getString("boAttrId");
								if (boAttrId.indexOf('_') == -1) { // attribute is not a FQN
									String boMetaId = Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID());
									boAttrId = boMetaId + "_" + boAttrId;
								}
								// TODO move to parser
								whereString = whereString.replaceAll(key, boAttrId);
							} else if (aliasObject.containsKey("boMetaId")) {
								String boMetaId = aliasObject.getString("boMetaId");
								// TODO move to parser
								whereString = whereString.replaceAll(key, boMetaId);
							}
						}
					}
					
					return whereString;
					
				} else if (jsonWhere instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonWhere).getString();
				}
			}
		}
		return null;
	}
	
	/** translates the JSON value for the key 'orderBy'. Something like this...
	[
		{"boAttrId": "example_rest_Order_customer", "asc": true},
		{"boAttrId": "example_rest_Order_orderDate", "asc": false}
	]
	*/
	private String getOrderByFromQuery() {
		if (queryContext != null) {
			if (queryContext.containsKey("orderBy") && !queryContext.isNull("orderBy")) {
				JsonValue jsonOrderBy = queryContext.get("orderBy");
				
				if (jsonOrderBy instanceof JsonArray) {
					JsonArray orderByArray = (JsonArray) jsonOrderBy;
					
					StringBuilder result = new StringBuilder();
					for (int i = 0; i < orderByArray.size(); i++) {
						JsonValue jsonValue = orderByArray.get(i);

						if (jsonValue instanceof JsonObject) {
							String boAttrId = ((JsonObject) jsonValue).getString("boAttrId");
							boolean asc = true;
							if (((JsonObject) jsonValue).containsKey("asc")) {
								asc = ((JsonObject) jsonValue).getBoolean("asc", true);
							}
							
							if (result.length() > 0) {
								result.append(", ");
							}
							result.append(boAttrId);
							result.append(" ");
							result.append(asc ? CollectableSorting.SORTING_ASCENDING : CollectableSorting.SORTING_DESCENDING);
						}
					}
					return result.toString();
					
				} else if (jsonOrderBy instanceof JsonString) {
					// simple String? This is only a fallback!
					return ((JsonString) jsonOrderBy).getString();
				}
			}
		}
		return null;
	}
	
	private String getSearch() {
		return search;
	}
	
	public String getAttributes() {
		return attributes;
	}
	
	public boolean showAllFields() {
		if ("all".equalsIgnoreCase(fields)) {
			return true;
		}
		if ("tableview".equalsIgnoreCase(fields)) {
			return true;
		}
		if (StringUtils.looksEmpty(attributes)) {
			return true;
		}
		return false;
	}

	@Override
	public Long getStart() {
		if (offset == null) {
			return 0L;
		}
		return offset;
	}

	@Override
	public Long getEnd() {
		return getStart() + getChunkSize() - 1;
	}

	private long getChunkSize() {
		if (chunkSize < 0) {
			return 0L;
		} else if (chunkSize > DEFAULT_RESULT_LIMIT) {
			return DEFAULT_RESULT_LIMIT;
		}
		return chunkSize;
	}
	
	public boolean countTotal() {
		if (countTotal == null) {
			return false;
		}
		return countTotal;
	}
	
	public JsonBuilderConfiguration getJsonConfig() {
		JsonBuilderConfiguration result = JsonBuilderConfiguration.getList(getAttributes());
		if (this.withMetaLink != null) {
			result.withMetaLink = this.withMetaLink;
		}
		if (this.withTitleAndInfo != null) {
			result.withTitleAndInfo = this.withTitleAndInfo;
		}
		if (this.withLayoutLink != null) {
			result.withLayoutLink = this.withLayoutLink;
		}
		return result;
	}
	
	@Override
	public void setSortingOrder(CollectableSearchExpression cse, UID bometa) {
		
		String sorting = this.orderBy;
		if (StringUtils.looksEmpty(sorting)) {
			sorting = this.sortExpression;
		}
		
		List<CollectableSorting> sorts = new ArrayList<CollectableSorting>();
		
		if (!StringUtils.looksEmpty(sorting)) {
			for (String fieldFqn : sorting.split(",")) {
				String fieldFqnTrim = fieldFqn.trim();
				String sortString = CollectableSorting.SORTING_ASCENDING;
				
				//This happens twice, because the fields arrive here with full qualified names and have to translated first.
				//TODO: Find a more elegant way with parsing the asc/desc only once
				
				int n = fieldFqnTrim.indexOf(' ');
				if (n > -1) {
					sortString = fieldFqnTrim.substring(n + 1);
					fieldFqnTrim = fieldFqnTrim.substring(0, n);
				}
				
				UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, fieldFqnTrim);
				boolean asc = !sortString.equals(CollectableSorting.SORTING_DESCENDING);
				sorts.add(CollectableSorting.createSorting(fieldUid, asc));
			}
		}
		
		if (sorts.isEmpty()) {
			if (Rest.getEntity(bometa).isDatasourceBased()) {
				// no default sorting for datasources...
				return;
			} else {
				sorts.add(CollectableSorting.createSorting(Rest.getEntity(bometa).getPk().getMetaData(bometa).getUID(), false));
			}
		}
		cse.setSortingOrder(sorts);
	}
	
	public static class RestSQLParser implements ExpressionVisitor, StatementVisitor,
								SelectVisitor, FromItemVisitor, SelectItemVisitor, ItemsListVisitor {
		
		private final static String INTID_SUFFIX = ".id";

		public CollectableSearchCondition getCondition() {
			return condition;
		}

		private CollectableSearchCondition condition;
		
		private FieldMeta<?> fieldColumn;
		
		private EntityMeta<?> entityTable;
		
		private Object value;
		
		@Override
		public void visit(AndExpression andExpression) {
			RestSQLParser leftParser = new RestSQLParser();
			andExpression.getLeftExpression().accept(leftParser);
			RestSQLParser rightParser = new RestSQLParser();
			andExpression.getRightExpression().accept(rightParser);
			if (leftParser.condition != null && rightParser.condition != null) {
				condition = SearchConditionUtils.and(leftParser.condition, rightParser.condition);
			} else if (leftParser.condition != null) {
				condition = leftParser.condition;
			} else if (rightParser.condition != null) {
				condition = rightParser.condition;
			}
		}
		
		@Override
		public void visit(OrExpression orExpression) {
			RestSQLParser leftParser = new RestSQLParser();
			orExpression.getLeftExpression().accept(leftParser);
			RestSQLParser rightParser = new RestSQLParser();
			orExpression.getRightExpression().accept(rightParser);
			if (leftParser.condition != null && rightParser.condition != null) {
				condition = SearchConditionUtils.or(leftParser.condition, rightParser.condition);
			} else if (leftParser.condition != null) {
				condition = leftParser.condition;
			} else if (rightParser.condition != null) {
				condition = rightParser.condition;
			}
		}
		
		@Override
		public void visit(InExpression inExpression) {
			RestSQLParser leftParser = new RestSQLParser();
			inExpression.getLeftExpression().accept(leftParser);
			
			RestSQLParser inParser = new RestSQLParser();
			// for date values:
			inParser.fieldColumn = leftParser.fieldColumn;
			inExpression.getItemsList().accept(inParser);
			
			if (leftParser.fieldColumn != null) {
				if (inParser.value instanceof List) {
					List inComparands = (List) inParser.value;
					condition = new CollectableInCondition(SearchConditionUtils.newEntityField(leftParser.fieldColumn), inComparands);
				} else if (inParser.condition != null) {
					if (SF.PK_ID.checkField(leftParser.fieldColumn.getEntity(), leftParser.fieldColumn.getUID())) {
						// special handling for .INTID IN (..)
						condition = new CollectableSubCondition(
								inParser.fieldColumn.getEntity(), 
								inParser.fieldColumn.getUID(), 
								inParser.condition);
					} else {
						condition = new CollectableSubCondition(
								leftParser.fieldColumn.getUID(), 
								inParser.fieldColumn.getEntity(), 
								inParser.fieldColumn.getUID(), 
								inParser.condition);
					}
				}
			}
			
			if (inExpression.isNot() && condition != null) {
				condition = SearchConditionUtils.not(condition);
			}
		}
		
		public void visit(SubSelect subSelect) {
			subSelect.getSelectBody().accept(this);
		}
		
		public void visit(ExpressionList expressionList) {
			List items = new ArrayList();
			for (Object object : expressionList.getExpressions()) {
				if (object instanceof Expression) {
					RestSQLParser itemParser = new RestSQLParser();
					itemParser.fieldColumn = fieldColumn;
					((Expression) object).accept(itemParser);
					if (itemParser.value != null) {
						items.add(itemParser.value);
					}
				}
			}
			value = items;
		}
		
		@Override
		public void visit(ExistsExpression existsExpression) {}
		
		@Override
		public void visit(Parenthesis parenthesis) {
			RestSQLParser expParser = new RestSQLParser();
			parenthesis.getExpression().accept(expParser);
			if (expParser.condition != null) {
				if (parenthesis.isNot()) {
					condition = SearchConditionUtils.not(expParser.condition);
				} else {
					condition = expParser.condition;
				}
			}
		}
		
		@Override
		public void visit(Column tableColumn) {
			if (tableColumn.getTable() != null && tableColumn.getWholeColumnName().endsWith(INTID_SUFFIX)) {
				String entityFqn = tableColumn.getTable().getName();
				UID entityUid = Rest.translateFqn(E.ENTITY, entityFqn);
				fieldColumn = Rest.getEntity(entityUid).isUidEntity() ? SF.PK_UID.getMetaData(entityUid) : SF.PK_ID.getMetaData(entityUid);
			} else {
				UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, tableColumn.getColumnName());
				fieldColumn = Rest.getEntityField(fieldUid);
			}
		}
		
		private static class BinaryParser {
			
			FieldMeta<?> fieldLeft;
			FieldMeta<?> fieldRight;
			Object valueLeft;
			Object valueRight;
			
			private BinaryParser parse(BinaryExpression exp) {
				RestSQLParser leftParser = new RestSQLParser();
				exp.getLeftExpression().accept(leftParser);
				RestSQLParser rightParser = new RestSQLParser();
				exp.getRightExpression().accept(rightParser);
				
				if (leftParser.fieldColumn != null) {
					fieldLeft = leftParser.fieldColumn;
				} else {
					// for date values:
					leftParser.fieldColumn = rightParser.fieldColumn;
					exp.getRightExpression().accept(leftParser);
					valueLeft = leftParser.value;
				}
				if (rightParser.fieldColumn != null) {
					fieldRight = rightParser.fieldColumn;
				} else {
					// for date values:
					rightParser.fieldColumn = leftParser.fieldColumn;
					exp.getRightExpression().accept(rightParser);
					valueRight = rightParser.value;
				}
				return this;
			}
			
		}
		
		private void setFieldComparison(BinaryExpression exp, ComparisonOperator op) {
			BinaryParser p = new BinaryParser().parse(exp);
			
			if (p.fieldLeft != null && p.fieldRight != null) {
				condition = new CollectableComparisonWithOtherField(
						SearchConditionUtils.newEntityField(p.fieldLeft), 
						op, 
						SearchConditionUtils.newEntityField(p.fieldRight));
			} else if (p.fieldLeft != null && p.valueRight != null) {
				if (op == ComparisonOperator.LIKE) {
					condition = SearchConditionUtils.newLikeCondition(p.fieldLeft, (String)p.valueRight);
				} else {
					if (p.fieldLeft.getForeignEntity() != null) {
						EntityMeta<Object> foreignEntityMeta = MetaProvider.getInstance().getEntity(p.fieldLeft.getForeignEntity());
						if (foreignEntityMeta.isUidEntity()) {
							UID refUid = Rest.translateFqn(foreignEntityMeta, p.valueRight.toString());
							condition = SearchConditionUtils.newUidComparison(p.fieldLeft.getUID(), op, refUid);
						} else if (p.valueRight instanceof Long) {
							condition = SearchConditionUtils.newIdComparison(p.fieldLeft.getUID(), op, (Long) p.valueRight);
						}
					} else {
						condition = SearchConditionUtils.newComparison(p.fieldLeft, op, p.valueRight);
					}
				}
			} else if (p.fieldRight != null && p.valueLeft != null) {
				if (op == ComparisonOperator.LIKE) {
					condition = SearchConditionUtils.newLikeCondition(p.fieldRight, (String)p.valueLeft);
				} else {
					if (p.fieldRight.getForeignEntity() != null) {
						EntityMeta<Object> foreignEntityMeta = MetaProvider.getInstance().getEntity(p.fieldRight.getForeignEntity());
						if (foreignEntityMeta.isUidEntity()) {
							UID refUid = Rest.translateFqn(foreignEntityMeta, p.valueLeft.toString());
							condition = SearchConditionUtils.newUidComparison(p.fieldRight.getUID(), op, refUid);
						} else if (p.valueLeft instanceof Long) {
							condition = SearchConditionUtils.newIdComparison(p.fieldRight.getUID(), op, (Long) p.valueLeft);
						}
					} else {
						condition = SearchConditionUtils.newComparison(p.fieldRight, op, p.valueLeft);
					}
				}
			} else if (p.fieldLeft != null && p.valueRight == null) {
				condition = SearchConditionUtils.newIsNullCondition(p.fieldLeft);
			} else if (p.fieldRight != null && p.valueLeft == null) {
				condition = SearchConditionUtils.newIsNullCondition(p.fieldRight);
			}
			
			if (exp.isNot() && condition != null) {
				condition = SearchConditionUtils.not(condition);
			}
		}
		
		@Override
		public void visit(EqualsTo equalsTo) {
			setFieldComparison(equalsTo, ComparisonOperator.EQUAL);
		}
		
		@Override
		public void visit(NotEqualsTo notEqualsTo) {
			setFieldComparison(notEqualsTo, ComparisonOperator.NOT_EQUAL);
		}
		
		@Override
		public void visit(LikeExpression likeExpression) {
			setFieldComparison(likeExpression, ComparisonOperator.LIKE);
		}
		
		@Override
		public void visit(GreaterThan greaterThan) {
			setFieldComparison(greaterThan, ComparisonOperator.GREATER);
		}
		
		@Override
		public void visit(GreaterThanEquals greaterThanEquals) {
			setFieldComparison(greaterThanEquals, ComparisonOperator.GREATER_OR_EQUAL);
		}
		
		@Override
		public void visit(MinorThan minorThan) {
			setFieldComparison(minorThan, ComparisonOperator.LESS);
		}
		
		@Override
		public void visit(MinorThanEquals minorThanEquals) {
			setFieldComparison(minorThanEquals, ComparisonOperator.LESS_OR_EQUAL);
		}
		
		@Override
		public void visit(IsNullExpression isNullExpression) {
			RestSQLParser p = new RestSQLParser();
			isNullExpression.getLeftExpression().accept(p);
			
			if (p.fieldColumn != null) {
				condition = SearchConditionUtils.newIsNullCondition(p.fieldColumn);
			}
		
			if (isNullExpression.isNot() && condition != null) {
				condition = SearchConditionUtils.not(condition);
			}
		}
		
		@Override
		public void visit(StringValue stringValue) {
			value = stringValue.getValue();
			if (fieldColumn != null && stringValue.getValue() != null) {
				if (java.util.Date.class.isAssignableFrom(fieldColumn.getJavaClass())) {
					try {
						Date date = new SimpleDateFormat("yyyy-MM-dd").parse(stringValue.getValue());
						value = date;
					} catch (ParseException e) {
						throw new NuclosFatalException("Date value " + stringValue.getValue() + " is not parseable with yyyy-MM-dd: " + e.getMessage()); 
					}
				} else if (java.lang.Boolean.class.equals(fieldColumn.getJavaClass())) {
					value = Boolean.parseBoolean(stringValue.getValue());
				}
			}
		}
		
		@Override
		public void visit(DoubleValue doubleValue) {
			value = doubleValue.getValue();
		}
		
		@Override
		public void visit(LongValue longValue) {
			value = longValue.getValue();
		}

		public void visit(NullValue nullValue) {}
		public void visit(Function function) {}
		public void visit(InverseExpression inverseExpression) {}
		public void visit(JdbcParameter jdbcParameter) {}
		public void visit(DateValue dateValue) {}
		public void visit(TimeValue timeValue) {}
		public void visit(TimestampValue timestampValue) {}
		public void visit(Addition addition) {}
		public void visit(Division division) {}
		public void visit(Multiplication multiplication) {}
		public void visit(Subtraction subtraction) {}
		public void visit(Between between) {}
		public void visit(CaseExpression caseExpression) {}
		public void visit(WhenClause whenClause) {}
		public void visit(AllComparisonExpression allComparisonExpression) {}
		public void visit(AnyComparisonExpression anyComparisonExpression) {}
		public void visit(Concat concat) {}
		public void visit(Matches matches) {}
		public void visit(BitwiseAnd bitwiseAnd) {}
		public void visit(BitwiseOr bitwiseOr) {}
		public void visit(BitwiseXor bitwiseXor) {}
		
		public void visit(Table tableName) {
			String entityFqn = tableName.getName();
			UID entityUid = Rest.translateFqn(E.ENTITY, entityFqn);
			entityTable = Rest.getEntity(entityUid);
		}
		
		public void visit(SubJoin subjoin) {}
		
		private void visitSelectItems(List selectItems) {
			if (selectItems.size() > 0) {
				SelectItem select = (SelectItem) selectItems.get(0);
				select.accept(this);
			}
		}
		
		@Override
		public void visit(PlainSelect plainSelect) {
			plainSelect.getWhere().accept(this);
			FromItem from = plainSelect.getFromItem();
			from.accept(this);
			visitSelectItems(plainSelect.getSelectItems());
		}
		public void visit(Union union) {}
		
		public void visit(AllColumns allColumns) {}
		public void visit(AllTableColumns allTableColumns) {}
		public void visit(SelectExpressionItem selectExpressionItem) {
			selectExpressionItem.getExpression().accept(this);
		}
		
		@Override
		public void visit(Select select) {
			select.getSelectBody().accept(this);
		}
		public void visit(CreateTable createTable) {}
		public void visit(Truncate truncate) {}
		public void visit(Drop drop) {}
		public void visit(Replace replace) {}
		public void visit(Insert insert) {}
		public void visit(Update update) {}
		public void visit(Delete delete) {}
	}
	
	private static class WhereParser {
		
		private final IWebContext webContext;
		
		private final String where;
		
		public WhereParser(IWebContext webContext, String where) {
			this.webContext = webContext;
			this.where = where;
		}
		
		public CollectableSearchCondition getCollectableSearchCondition() throws JSQLParserException {
			Statement statement = new CCJSqlParserManager().parse(new StringReader(
					"SELECT * FROM " + Rest.translateUid(E.ENTITY, webContext.getBOMeta().getUID()) + " WHERE + " + where));
			RestSQLParser parser = new RestSQLParser();
			statement.accept(parser);
			return parser.condition;
		}
		
		
	}
	
	@Override
	public CollectableSearchExpression getSearchExpression(UID entity, Collection<FieldMeta<?>> fields) {
		CollectableSearchCondition cond1 = null;
		
		if (!StringUtils.isNullOrEmpty(where)) {
			try {
				cond1 = new WhereParser(webContext, where).getCollectableSearchCondition();
			} catch (JSQLParserException e) {
				LOG.error("{} is not parseable: {}", where, e.getMessage(), e);
				throw new NuclosWebException(Status.NOT_ACCEPTABLE, e.getLocalizedMessage());
			}
		}
		
		//NUCLOS-4111: Example syntax for a field-referencing search-condition:
		//searchCondition = "CompositeCondition:AND:[Comparison:EQUAL:seriennummer:x12,LikeCondition:LIKE:lagereinheitentyp:*LE*]";
		
		//TODO: Searches with ':,[]'  are not supported yet, as they are the separators of the arguments
		//Also nested CompositeConditions are not possible at this point (see AbstactCollectableSearchCondition.java Line 136)
		//For Nuclos-4111 there is no need for nested CompositeConditions, so this is postponed.
		
		else if (!StringUtils.isNullOrEmpty(searchCondition)) {
			Map<UID, FieldMeta<?>> mpFields = Rest.getAllEntityFieldsByEntityIncludingVersion(entity);
			Map<FieldMeta<?>, String> mpFieldsByFQN = new HashMap<FieldMeta<?>, String>();
			for (FieldMeta<?> fm : mpFields.values()) {
				mpFieldsByFQN.put(fm, Rest.translateUid(E.ENTITYFIELD, fm.getUID()));
			}
			
			cond1 = AbstractCollectableSearchCondition.createCondition(searchCondition, mpFieldsByFQN);
		}
		
		CollectableSearchCondition cond2 = null;
		
		EntityMeta<?> em = Rest.getEntity(entity);
		if (em.isStateModel()) {
			cond2 = SearchConditionUtils.newComparison(entity,
					SF.LOGICALDELETED, ComparisonOperator.EQUAL, Boolean.FALSE);
			
		}
		
		CollectableSearchCondition cond;
		if (cond1 == null && cond2 == null) {
			cond = null;
		} else if (cond1 == null) {
			cond = cond2;
		} else if (cond2 == null) {
			cond = cond1;
		} else {
			cond = SearchConditionUtils.and(cond1, cond2);
		}
		
		CollectableSearchExpression clctexpr = new CollectableSearchExpression(cond);
		
		String isp = getSearch();
		if (isp != null && !isp.isEmpty()) {
			List<CollectableEntityField> cefs = new ArrayList<CollectableEntityField>();
			for (FieldMeta<?> fm : fields) {
				if (SF.isEOField(fm.getEntity(), fm.getUID())) {
					continue;
				}
				if (fm.getJavaClass() == Date.class) {
					continue;
				}
				cefs.add(new CollectableEOEntityField(fm));
			}
			SearchConditionUtils.addTextSearchToSearchExpression(isp, cefs, clctexpr, entity);
		}
		
		setSortingOrder(clctexpr, entity);
		return addSearchFilterToExpression(clctexpr);
	}
	
	private CollectableSearchExpression addSearchFilterToExpression(CollectableSearchExpression clctexpr) {
		if (searchFilterId == null || searchFilterId.trim().length() == 0) {
			return clctexpr;
		}
		try {
			EntitySearchFilter2 sf = Rest.facade().getSearchFilterByPk(Rest.translateFqn(E.SEARCHFILTER, searchFilterId));
			if (sf == null) {
				throw new CommonFinderException("SearchFilter with id " + searchFilterId + " not found!");
			}
			final CollectableSearchCondition csc;
			if (clctexpr.getSearchCondition() != null) {
				csc = SearchConditionUtils.and(clctexpr.getSearchCondition(), sf.getSearchExpression().getSearchCondition());
			} else {
				csc = sf.getSearchExpression().getSearchCondition();
			}
			
			// don't add filter if not correct
			try {
				csc.isSyntacticallyCorrect();
			} catch(Exception e) {
				throw new NuclosFatalException("SearchFilter with id " + searchFilterId + " is syntactically not correct: " + e.getMessage(), e);
			}
			
			CollectableSearchExpression clctexpr2 = new CollectableSearchExpression(csc);
			
			List<CollectableSorting> sorts = clctexpr.getSortingOrder();
			sorts.addAll(sf.getSearchExpression().getSortingOrder());
			clctexpr2.setSortingOrder(sorts);
			
			return clctexpr2;
		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(cbe, Rest.translateFqn(E.SEARCHFILTER, searchFilterId));
		}
	}
	
}
