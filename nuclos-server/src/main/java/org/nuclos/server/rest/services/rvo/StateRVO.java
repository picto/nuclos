package org.nuclos.server.rest.services.rvo;

import org.nuclos.common.UID;
import org.nuclos.server.statemodel.valueobject.StateVO;

public class StateRVO {
	private final StateVO stateVO;
	private final String boMetaId;
	private final String boId;
	private final boolean bNonStop;
	
	public StateRVO(StateVO stateVO, String boMetaId, String boId, UID sourceStateUid) {
		this.stateVO = stateVO;
		this.boMetaId = boMetaId;
		this.boId = boId;
		this.bNonStop = stateVO.isTransitionNonstop(sourceStateUid);
	}

	public StateVO getStateVO() {
		return stateVO;
	}

	public String getBoMetaId() {
		return boMetaId;
	}

	public String getBoId() {
		return boId;
	}
	
	public boolean isNonStop() {
		return bNonStop;
	}

}