//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.text.MessageFormat;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.mail.NuclosMail;
import org.nuclos.common.security.UserVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.mail.NuclosMailServiceProvider;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.UserManagementHelper;
import org.nuclos.server.rest.services.rvo.UserRVO;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;



@Path("/admin")
@Produces(MediaType.APPLICATION_JSON)
public class AdminService extends UserManagementHelper {

	// TODO: Within this service it appears as there were two entities "admin" and "user" in the DB, which is not true.
	// FIXME: Refactor and use only "/user" for anything involed with the creating/update/deletaion of an user.

	@Autowired
	UserFacadeLocal userFacade;
	
	@Autowired
	ServerParameterProvider parameterProvider;

	@Autowired
	private NuclosMailServiceProvider mailServiceProvider;
	
	@Autowired
	private SecurityCache securityCache;

	//FIXME: Rename the service. /user and /account is the same persistent object and thus must not have different names


	@GET
	@Path("/user/{username}")
	public UserRVO getUser(@PathParam("username") String username) throws CommonBusinessException {
		checkSuperUser();
		UserVO userVO = userFacade.getByUserName(username);
		if(userVO==null){
			throw new NotFoundException("username" + username + "not found");
		}else{
			return new UserRVO(userVO);
		}
	}
	
	@POST
	@Path("/user")
	public UserRVO createUser(UserRVO userRVO) throws CommonBusinessException {
		checkSuperUser();
		if (StringUtils.isNullOrEmpty(userRVO.getFirstname())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.firstname");
		if (StringUtils.isNullOrEmpty(userRVO.getLastname())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.lastname");
		if (StringUtils.isNullOrEmpty(userRVO.getName())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.username");
		if (StringUtils.isNullOrEmpty(userRVO.getNewPassword())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.password");
		
		UserVO userVO = userRVO.toUserVO();
		
		IDependentDataMap mpDependants = new DependentDataMap();
		List<String> rolenames = userRVO.getRoles();
		for (String rolename : rolenames) {
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if(roleVO==null) throw new NuclosWebException(Status.BAD_REQUEST, "Rolename '"+rolename+"' not exist.");
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<UID>(E.ROLEUSER);
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}
		
		userFacade.create(userVO, mpDependants );
		userFacade.setPassword(userRVO.getName(), userRVO.getNewPassword(), null);
		return userRVO;
	}
	
	@PUT
	@Path("/user/{username}")
	public UserRVO updateUser(@PathParam("username") String username, UserRVO userRVO) throws CommonBusinessException {
		throw new NotImplementedException("update user is not supported");
	}
	
	//DATEN ANGEBEN
	//-->NOCH EIN LETZER SCHRITT ZUR ANMELDUNG. WIR HABEN IHNEN EINE EMAIL GESCHICKT. BITTE AKTIVIEREN SIE IHREN ACCOUNT. 
	@POST
	@RestServiceInfo(identifier="account", validateSession=false, isFinalized=false, description="User registration")
	@Path("/account")
	public Response createAccount(UserRVO userRVO) throws CommonBusinessException, BusinessException {
		// replaced by org.nuclos.server.rest.SessionValidationRequestFilter: validateSession();
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_SMTP_SERVER))) throw new NuclosWebException(Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT))) throw new NuclosWebException(Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		if (StringUtils.isNullOrEmpty(parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE))) throw new NuclosWebException(Status.SERVICE_UNAVAILABLE, "webclient.account.email.server.not.available");
		
		if (StringUtils.isNullOrEmpty(userRVO.getEmail())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.email");
		if (StringUtils.isNullOrEmpty(userRVO.getFirstname())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.firstname");
		if (StringUtils.isNullOrEmpty(userRVO.getLastname())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.lastname");
		if (StringUtils.isNullOrEmpty(userRVO.getName())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.username");
		if (StringUtils.isNullOrEmpty(userRVO.getNewPassword())) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.missing.password");
		
		UserRVO user = new UserRVO();
		user.setEmail(userRVO.getEmail());
		user.setName(userRVO.getName());
		user.setFirstname(userRVO.getFirstname());
		user.setLastname(userRVO.getLastname());
		user.setNewPassword(userRVO.getNewPassword());
		user.setActivationcode(UUID.randomUUID().toString().replaceAll("-", ""));
		user.setLocked(true);
		user.setSuperuser(false);
		user.setPrivacyconsent(userRVO.isPrivacyconsent());
		
		UserVO userVO = user.toUserVO();
		String rolenamesParameter = parameterProvider.getValue(ParameterProvider.KEY_ROLE_FOR_SELF_REGISTERED_USERS);
		if(StringUtils.isNullOrEmpty(rolenamesParameter)) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.self.registration.is.not.active");
		String[] rolenames;
		if(rolenamesParameter!=null && rolenamesParameter.contains(",")){
			rolenames = rolenamesParameter.split(",");
		}else{
			rolenames = new String[]{rolenamesParameter};
		}
		
		if(userFacade.getByUserEmail(userRVO.getEmail())!=null) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.email.already.in.use");
		if(userFacade.getByUserName(userRVO.getName())!=null) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.username.already.in.use");
		
		
		// CREATE USER
		IDependentDataMap mpDependants = new DependentDataMap();
		for (String rolename : rolenames) {
			rolename = rolename.trim();
			EntityObjectVO<UID> eoRoleUser = new EntityObjectVO<UID>(E.ROLEUSER);
			MasterDataVO<UID> roleVO = userFacade.getRoleByName(rolename);
			if(roleVO==null) throw new NuclosWebException(Status.BAD_REQUEST, "Rolename '"+rolename+"' not exist.");
			eoRoleUser.setFieldUid(E.ROLEUSER.role, roleVO.getPrimaryKey());
			mpDependants.addData(E.ROLEUSER.user, eoRoleUser);
		}
		userFacade.create(userVO, mpDependants);
		userFacade.setPassword(userVO.getName(), userVO.getNewPassword(), null);
		
		// SEND ACTIVATION LINK
		String subject = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_SUBJECT);
		String message = parameterProvider.getValue(ParameterProvider.KEY_ACTIVATION_EMAIL_MESSAGE);
		String signature = parameterProvider.getValue(ParameterProvider.EMAIL_SIGNATURE);

		NuclosMail mail;
		createMail:
		{
			org.nuclos.api.mail.NuclosMail apiMail = new org.nuclos.api.mail.NuclosMail();
			apiMail.addRecipient(user.getEmail());
			apiMail.setSubject(subject);
			message = MessageFormat.format(
					message,
					user.getFirstname(),
					user.getLastname(),
					user.getName(),
					user.getActivationcode()
			);
			apiMail.setMessage(message);
			boolean isHtml = (message + signature).matches("(?s).*\\<[^>]+>.*");
			apiMail.setContentType(isHtml ? org.nuclos.api.mail.NuclosMail.HTML : org.nuclos.api.mail.NuclosMail.PLAIN);
			mail = new NuclosMail(apiMail);
		}

		mailServiceProvider.send(mail);
		return Response.ok().build();
	}
	
	//ERFOLGREICH AKTIVERT
	@GET
	@Path("/account/activate/{username}/{code}")
	public Response activateAccount(
			@PathParam("username") String username,
			@PathParam("code") String code
	) throws CommonBusinessException, BusinessException {
		// replaced by org.nuclos.server.rest.SessionValidationRequestFilter: validateSession();
		
		// VALIDATE
		if(StringUtils.isNullOrEmpty(code)) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.no.activationcode.found");
		if(StringUtils.isNullOrEmpty(username)) throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.no.username.found");
		
		// CHECK EXISTING ALREADY
		UserVO user = userFacade.getByActivationCode(code);
				
		if(user==null || !user.getName().equals(username)){
			throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.username.or.activationcode.not.found");
		}else{
			user.setActivationcode(null);
			user.setLocked(false);
			userFacade.modify(user,null,null);
			return Response.ok().build();
		}
	}
	
	//GEBEN SIE IHR NEUES PASSWORD EIN
	//--> PASSWORD ERFOLGREICH GEÄNDERT
	@GET
	@Path("/account/resetpassword/{username}/{code}")
	public Response resetPasswordPrepare(@PathParam("username") String username, @PathParam("code") String code) throws CommonBusinessException, BusinessException {
		if(username==null || username.length()==0 || code==null || code.length()==0) throw new WebApplicationException("webclient.account.username.or.activationcode.faulty");
		UserVO user = userFacade.getByUserName(username);
		if(user!=null &&  user.getLocked() != null && user.getLocked() == false && user.getActivationcode() != null && user.getActivationcode().equals(code)){
			return Response.ok().build();
		}else{
			throw new NuclosWebException(Status.BAD_REQUEST, "webclient.account.username.or.activationcode.faulty");
		}
	}

	//PASSWORT ERFOLGREICH GEÄNDERT
	@POST
	@Path("/account/resetpassword")
	public Response resetPassword(UserRVO userRVO) throws CommonBusinessException, BusinessException {
		if(userRVO==null || userRVO.getNewPassword()==null || userRVO.getName()==null || userRVO.getActivationcode()==null) throw new WebApplicationException("webclient.account.username.or.activationcode.faulty");
		UserVO user = userFacade.getByUserName(userRVO.getName());
		if(user.getActivationcode()!=null && user.getActivationcode().equals(userRVO.getActivationcode())){
			user.setNewPassword(userRVO.getNewPassword());
			user.setActivationcode(null);
			userFacade.modify(user, null, null);
		}
		throw new WebApplicationException("webclient.account.username.or.activationcode.faulty");
	}

	
	
	//FORMULAR BITTE USERNAME UND EMAIL ANGEBEN...
	//--> IHNEN WURDE PER EMAIL EIN LINK ZUGESENDET, ÜBER DIESEN LINK KÖNNEN SIE IHR PASSWORT ZURÜCKSETZEN
	@GET
	@Path("/account/forgetpassword/{username}/{email}")
	public Response forgetPassword(@PathParam("username") String username, @PathParam("email") String email) throws CommonBusinessException, BusinessException {
		UserVO user = userFacade.getByUserName(username);
		if(user!=null && user.getEmail()!=null){
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_RESET_PW_EMAIL_MESSAGE);
			NuclosMail mail = new NuclosMail(user.getEmail(), subject, MessageFormat.format(message, user.getFirstname(), user.getLastname(), user.getName(), user.getActivationcode()));
			mailServiceProvider.send(mail);
		}
		return Response.ok().build();
	}
	
	//FORMULAR BITTE EMAIL-ADRESSE ANGEBEN
	//--> IHNEN WURDE PER EMAIL DER USERNAME ZUGESENDET. SOLLTE SIE KEINE MAIL ERHALTEN HABEN, PRÜFEN SICH BITTE AUCH IHREN SPAM-ORDNER
	@GET
	@Path("/account/rememberusername/{email}")
	public Response forgetUsername(@PathParam("email") String email) throws CommonBusinessException, BusinessException {
		UserVO user = userFacade.getByUserEmail(email);
		if(user!=null && user.getEmail()!=null){
			// SEND ACTIVATION LINK
			String subject = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_SUBJECT);
			String message = parameterProvider.getValue(ParameterProvider.KEY_USERNAME_EMAIL_MESSAGE);
			NuclosMail mail = new NuclosMail(user.getEmail(), subject, MessageFormat.format(message, user.getFirstname(), user.getLastname(), user.getName()));
			mailServiceProvider.send(mail);
		}
		return Response.ok().build();
	}

}
