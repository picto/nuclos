package org.nuclos.server.rest.layout.uicomponent;

import java.util.ArrayList;
import java.util.List;


public class Table extends Element {
    public List<Tr> trs = new ArrayList<Tr>();

    public Table(int columns, int rows) {
        trs = new ArrayList<Tr>();
        for (int i = 0; i < rows; i++) {
            Tr tr = new Tr();
            for (int j = 0; j < columns; j++) {
                tr.tds.add(new Td());
            }
            trs.add(tr);
        }
    }

    public Table(List<Double> columns, List<Double> rows) {
        trs = new ArrayList<Tr>();
        for (int i = 0; i < rows.size(); i++) {
            Tr tr = new Tr();
            for (int j = 0; j < columns.size(); j++) {
                if (i == 0) {
                    tr.tds.add(new Td(columns.get(j)));
                } else {
                    tr.tds.add(new Td());
                }
            }
            trs.add(tr);
        }
    }

    public String printChildren() {
        String result = "";
        for (Element child : trs) {
            result += child;
        }
        return result;
    }

    public void addElement(Element element, int column, int row) {
        trs.get(column).tds.get(row).element = element;
    }

    public String toString() {
        return "<table>" + printChildren() + "</table>";
    }
}
