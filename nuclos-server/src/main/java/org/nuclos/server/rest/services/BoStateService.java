package org.nuclos.server.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.services.helper.DataServiceHelper;
import org.nuclos.server.rest.services.helper.RestServiceInfo;

@Path("/boStateChanges")
@Produces(MediaType.APPLICATION_JSON)
public class BoStateService extends DataServiceHelper {

	@PUT
	@Path("/{boMetaId}/{boId}/{stateId}")
	@RestServiceInfo(identifier="boStateChange", isFinalized=true, description="Change state of BO")
	public Response boStateChange(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("stateId") String stateId) {
		try {
			Integer stateNumeral = Integer.parseInt(stateId);
			return stateChange(boMetaId, boId, stateNumeral);
		}
		catch (NumberFormatException ex) {
		}

		return stateChange(boMetaId, boId, stateId);
	}

	@Deprecated
	@GET
	@Path("/{boMetaId}/{boId}/{stateId}")
	@RestServiceInfo(identifier="boStateChangeDepr", isFinalized=false, description="Change state of BO with GET")
	public Response boStateChangeDepr(@PathParam("boMetaId") String boMetaId, @PathParam("boId") String boId, @PathParam("stateId") String stateId) {
		return boStateChange(boMetaId, boId, stateId);
	}

}