package org.nuclos.server.rest.misc;

import java.util.Iterator;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.validation.FieldValidationError;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.common2.exception.NuclosExceptions;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.rest.ejb3.Rest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class NuclosWebException extends WebApplicationException {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosWebException.class);
	
	public NuclosWebException(Status status) {
		super(status);
	}

	public NuclosWebException(Status status, String msg) {
		super(getResponse(status, msg));
	}

	public NuclosWebException(Exception ex, UID entity) {
		super(getResponse(ex, entity));
	}
	
	private static Response getResponse(Status status, String msg) {
		LOG.error(msg);
		return Response.status(status).entity(msg).type(MediaType.TEXT_PLAIN).build();
	}

	private static Response getResponse(Exception ex, UID entity) {
		// No details for DbExceptions because it might expose internal SQL statements!
		if (ex instanceof DbException) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		LOG.error(ex.getMessage(), ex);
		String shortMessage;
		String fullMessage;
		if (ex.getCause() != null) {
			shortMessage = ex.getCause().getMessage();
			fullMessage = getStackeTrace(ex.getCause(), 10);
		} else {
			shortMessage = ex.getMessage();
			fullMessage = getStackeTrace(ex, 10);
		}
		Status status = Response.Status.NOT_ACCEPTABLE;
	
		JsonObjectBuilder json = Json.createObjectBuilder();
		if (ex instanceof NuclosBusinessException) {
			status = Response.Status.PRECONDITION_FAILED;
			
		} else if (ex instanceof CommonBusinessException || ex instanceof CommonFatalException) {
			status = ex instanceof CommonBusinessException ? Response.Status.FORBIDDEN : Response.Status.EXPECTATION_FAILED;
			CommonValidationException cve = NuclosExceptions.getCause(ex, CommonValidationException.class);
			if (cve != null) {
				status = Response.Status.PRECONDITION_FAILED;
				shortMessage = Rest.getFullMessage(cve, entity);

				JsonArrayBuilder validationErrorsJSON = Json.createArrayBuilder();
				for (Iterator<FieldValidationError> iterator = cve.getFieldErrors().iterator(); iterator.hasNext();) {
					FieldValidationError validationError = iterator.next();
					JsonObjectBuilder validationErrorJSON = Json.createObjectBuilder();
					validationErrorJSON.add("entity", Rest.translateUid(E.ENTITY, validationError.getEntity()) );
					validationErrorJSON.add("field", Rest.translateUid(E.ENTITYFIELD, validationError.getField()));
					validationErrorJSON.add("errortype", validationError.getValidationErrorType().toString());
					validationErrorsJSON.add(validationErrorJSON);
				}
				json.add("validationErrors", validationErrorsJSON);
			}
			
		} else if (ex instanceof CommonStaleVersionException) {
			status = Response.Status.CONFLICT;
		}

		shortMessage = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class).getMessageFromResource(shortMessage);

		json.add("message", shortMessage != null ? shortMessage : fullMessage);
		json.add("stacktrace", fullMessage);
		return Response.status(status).entity(json.build()).type(MediaType.APPLICATION_JSON).build();
	}
	
	private static String getStackeTrace(Throwable e, int maxEl) {
		String msg = e.getMessage();
		if (msg == null) msg = e.toString();
		for (int i = 0; i < e.getStackTrace().length && i < maxEl; i++) {
			msg += "\n";
			msg += e.getStackTrace()[i].toString();
		}
		return msg;
	}
}
