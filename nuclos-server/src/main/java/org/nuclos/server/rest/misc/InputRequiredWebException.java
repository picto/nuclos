package org.nuclos.server.rest.misc;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.api.context.InputSpecification;
import org.nuclos.server.rest.services.rvo.JsonFactory;

@SuppressWarnings("serial")
public class InputRequiredWebException extends WebApplicationException {

	public InputRequiredWebException(InputSpecification inputSpecification) {
		
		super(
			Response.status(Response.Status.EXPECTATION_FAILED)
				.entity(buildJson(inputSpecification).build())
				.type(MediaType.APPLICATION_JSON)
				.build()
		);
	}
	
	private static JsonObjectBuilder buildJson(InputSpecification inputSpecification) {
		JsonObjectBuilder inputRequiredJson = Json.createObjectBuilder();
		JsonObjectBuilder specification = inputRequiredJson = Json.createObjectBuilder();
		specification.add("key", inputSpecification.getKey());
		specification.add("message", inputSpecification.getMessage());
		
		String type = "";
		switch (inputSpecification.getType()) {
		case InputSpecification.INPUT_OPTION:
			type = "input_option";
			break;
		case InputSpecification.INPUT_VALUE:
			type = "input_value";
			break;
		case InputSpecification.CONFIRM_YES_NO:
			type = "confirm_yes_no";
			break;
		case InputSpecification.CONFIRM_OK_CANCEL:
			type = "confirm_ok_cancel";
			break;
		}
		specification.add("type", type);
		
		if (inputSpecification.getOptions() != null) {
			JsonArrayBuilder options = Json.createArrayBuilder();
			for (Object o : inputSpecification.getOptions()) {
				options.add(JsonFactory.buildJsonValue(o));
			}
			specification.add("options", options);
		}

		if (inputSpecification.getDefaultOption() != null) {
			specification.add("defaultoption", inputSpecification.getDefaultOption().toString());
		}
		
		inputRequiredJson.add("specification", specification);
		
    	JsonObjectBuilder json = Json.createObjectBuilder();
    	json.add("inputrequired", inputRequiredJson);
    	
    	return json;
	}
	
}
