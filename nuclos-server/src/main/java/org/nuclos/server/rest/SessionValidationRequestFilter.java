package org.nuclos.server.rest;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;

import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.AbstractSessionIdLocator;
import org.nuclos.server.security.SessionContext;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class SessionValidationRequestFilter extends AbstractSessionIdLocator implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext creq) throws IOException {
		validateSessionIfNecessary();
	}

	protected final void validateSessionIfNecessary() {
		boolean sessionValidationEnabled = NuclosRestApplication.isSessionValidationEnabledForRelativePath(uriInfo.getPath(), httpRequest.getMethod());
		HttpSession httpSession = httpRequest.getSession();
		if (httpSession.isNew()) {
			if (sessionValidationEnabled) {
				throw new NuclosWebException(Response.Status.UNAUTHORIZED);
			}
		} else if(!httpSession.getId().equals(httpRequest.getRequestedSessionId())) {
			// tomcat session has been invalidated - remove session from SecurityCache
			Rest.facade().webLogout(httpRequest.getSession().getId());
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		} else {
			if (sessionValidationEnabled) {
				String sessionId = getSessionId();
				SessionContext session = Rest.facade().validateSessionAuthenticationAndInitUserContext(sessionId);
				if (session == null) {
					throw new NuclosWebException(Response.Status.UNAUTHORIZED);
				}		
			}
		}
	}
}
