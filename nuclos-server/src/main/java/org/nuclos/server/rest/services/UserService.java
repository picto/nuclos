//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest.services;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.rest.services.helper.UserManagementHelper;
import org.nuclos.server.rest.services.rvo.PasswordChangeRVO;
import org.nuclos.server.rest.services.rvo.RoleRVO;
import org.nuclos.server.rest.services.rvo.UserRVO;
import org.nuclos.server.security.UserFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends UserManagementHelper {
	@Autowired
	private UserFacadeLocal userFacade;

	@GET
	public List<UserRVO> getUserList(@PathParam("username") String username) throws CommonBusinessException {
		throw new NotImplementedException("get user list is not supported");
	}

	@POST
	public UserRVO createUser(UserRVO userRVO) throws CommonBusinessException {
		throw new NotImplementedException("create user is not supported");
	}
	
	@GET
	@Path("/{username}")
	public UserRVO getUser(@PathParam("username") String username) throws CommonBusinessException {
		throw new NotImplementedException("get user is not supported");
	}
	
	@PUT
	@Path("/{username}")
	public UserRVO updateUser(@PathParam("username") String username, UserRVO userRVO) throws CommonBusinessException {
		throw new NotImplementedException("update user is not supported");
	}
	
	@DELETE
	@Path("/{username}")
	public UserRVO deleteUser(@PathParam("username") String username) throws CommonBusinessException {
		throw new NotImplementedException("delete user is not supported");
	}
	
	@PUT
	@Path("/{username}/password")
	public Response setPassword(@PathParam("username") String username, PasswordChangeRVO passwordChange) {
		return super.setPassword(username, passwordChange);
	}

	@GET
	@Path("/roles")
	@Produces(MediaType.APPLICATION_JSON)
	public Set<RoleRVO> getRoles() throws CommonBusinessException {
		final Set<RoleRVO> result = new HashSet<>();

		final String username = getUser();
		Map<UID, String> allRolesForUser = userFacade.getAllRolesForUser(username);
		for (Map.Entry<UID,String> entry: allRolesForUser.entrySet()) {
			final RoleRVO roleRVO = new RoleRVO(
					entry.getKey(),
					entry.getValue()
			);
			result.add(roleRVO);
		}

		return result;
	}
}
