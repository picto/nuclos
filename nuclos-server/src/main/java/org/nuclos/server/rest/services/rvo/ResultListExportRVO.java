package org.nuclos.server.rest.services.rvo;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.server.rest.misc.IWebContext;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultListExportRVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private IWebContext webContext;

	private Map<String, Object> params;

	private OutputFile file;

	public static class RestLinks {
		private RestLink export;

		@JsonInclude(Include.NON_NULL)
		public RestLink getExport() {
			return export;
		}
	}

	public static class RestLink {
		private String href;

		public RestLink(String href) {
			super();
			this.href = href;
		}

		public String getHref() {
			return href;
		}
	}

	public RestLinks getLinks() {
		RestLinks restLinks = new RestLinks();
		restLinks.export = new RestLink(webContext.getRestURI("boListExportFile", file.getBoMetaId(), file.outputFormatId, file.fileId));
		return restLinks;
	}

	public ResultListExportRVO(IWebContext webContext) {
		this.webContext = webContext;
	}

	public OutputFile getFile() {
		return file;
	}

	public void setFile(OutputFile file) {
		this.file = file;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public static class OutputFile implements Serializable {

		private static final long serialVersionUID = 1L;

		private final String boMetaId;
		private final String fileName;
		private final String fileId;
		private final String outputFormatId;

		public OutputFile(String boMetaId, String fileName, String outputFormatId, String fileId) {
			super();
			this.boMetaId = boMetaId;
			this.fileName = fileName;
			this.outputFormatId = outputFormatId;
			this.fileId = fileId;
		}

		public String getBoMetaId() {
			return boMetaId;
		}

		public String getFileName() {
			return fileName;
		}

		public String getOutputFormatId() {
			return outputFormatId;
		}

		public String getFileId() {
			return fileId;
		}

	}
}
