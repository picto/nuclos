package org.nuclos.server.rest.layout.uicomponent;


public class Checkbox extends Element {
    public String value;
    public String boAttrId;

    public Checkbox(String boAttrId) {
        this.boAttrId = boAttrId;
    }

    public String toString() {
        return "<input " +
				"type=\"checkbox\"" +
                "[(ngModel)]=\"model.bo.eoData.attributes." + boAttrId + "\" " +
				"(ngModelChange)=\"model.bo.setAttribute('" + boAttrId + "', $event)\" " +
                "[ngModelOptions]=\"{standalone: true}\" " +
				"class=\"form-control\" " +
                ">";
    }
}
