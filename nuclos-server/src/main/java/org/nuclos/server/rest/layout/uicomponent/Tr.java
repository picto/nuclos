package org.nuclos.server.rest.layout.uicomponent;

import java.util.ArrayList;
import java.util.List;


public class Tr extends Element {
    Double height;

    public Tr(Double height) {
        this.height = height;
    }
    public Tr() {

    }

    public List<Td> tds = new ArrayList<Td>();

    public String printChildren() {
        String result = "";
        for (Element child : tds) {
            result += child;
        }
        return result;
    }

    private String getHeight() {
        // TODO height -1 --> use available space --> flexbox
        return height != null ? "style=\"width: " + height.toString().replace(".0", "") + "px\" " : "";
    }
    public String toString() {
        return "<tr " + getHeight() + ">" + printChildren() + "</tr>";
    }
}
