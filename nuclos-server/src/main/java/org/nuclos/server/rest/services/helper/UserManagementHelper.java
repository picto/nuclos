package org.nuclos.server.rest.services.helper;

import javax.ws.rs.core.Response;

import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.rvo.PasswordChangeRVO;


public class UserManagementHelper extends WebContext {
	
	protected Response setPassword(String username, PasswordChangeRVO passwordChange) {
		// replaced by org.nuclos.server.rest.SessionValidationRequestFilter: validateSession();
		
		String oldPassword = passwordChange.getOldPassword();
		String newPassword = passwordChange.getNewPassword();
		
		if (oldPassword == null) {
			oldPassword = "";
		}
		
		if (newPassword == null) {
			newPassword = "";
		}
		
		Response.Status respStatus = Rest.facade().changePassword(username, oldPassword, newPassword, getSessionId());
		
		if (respStatus == Response.Status.OK) {
			return Response.ok().build();
		}
		
		throw new NuclosWebException(respStatus);
	}

}
