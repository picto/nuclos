package org.nuclos.server.rest.ejb3;

import java.util.Collection;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;

public interface IFilterJ {

	void setSortingOrder(CollectableSearchExpression cse, UID bometa);
	
	Long getStart();
	
	Long getEnd();

	CollectableSearchExpression getSearchExpression(UID entity, Collection<FieldMeta<?>> fields);
	
}
