package org.nuclos.server.rest.layout.uicomponent;

public class Td extends Element {
    public Element element;

    Double width;

    public Td(Double width) {
        this.width = width;
    }
    public Td() {

    }

    private String getWidth() {
        // TODO width -1 --> use available space --> flexbox
        return width != null ? "style=\"width: " + width.toString().replace(".0", "") + "px\" " : "";
    }

    public String toString() {
        return "<td " + getWidth() + ">" + (element != null ? element : "") + "</td>";
    }
}
