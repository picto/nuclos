package org.nuclos.server.rest.services.helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.collections.CollectionUtils;
import org.nuclos.api.NuclosImage;
import org.nuclos.api.context.InputRequiredException;
import org.nuclos.api.context.InputSpecification;
import org.nuclos.common.Actions;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.NuclosFile;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common.dblayer.IFieldUIDRef;
import org.nuclos.common.report.NuclosReportException;
import org.nuclos.common.report.ejb3.ReportFacadeRemote;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common.security.EntityPermission;
import org.nuclos.common.security.SubformPermission;
import org.nuclos.common2.ForeignEntityFieldUIDParser;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.layoutml.ButtonConstants;
import org.nuclos.common2.layoutml.WebValueListProvider;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.utils.NuclosFileUtils;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.rest.ejb3.BoGenerationResult;
import org.nuclos.server.rest.ejb3.IRValueObject.JsonBuilderConfiguration;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.InputRequiredWebException;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache;
import org.nuclos.server.rest.misc.RestPrintoutFileDownloadCache.CacheResult;
import org.nuclos.server.rest.misc.SessionEntityContext;
import org.nuclos.server.rest.services.DependenceService.DependenceBoLinksFactory;
import org.nuclos.server.rest.services.rvo.FilterJ;
import org.nuclos.server.rest.services.rvo.JsonFactory;
import org.nuclos.server.rest.services.rvo.LayoutAdditions;
import org.nuclos.server.rest.services.rvo.RValueObject;
import org.nuclos.server.rest.services.rvo.RValueObject.UsageProperties;
import org.nuclos.server.rest.services.rvo.ReferenceListParameterJson;
import org.nuclos.server.rest.services.rvo.ResultListExportRVO;
import org.nuclos.server.rest.services.rvo.ResultRVO;
import org.nuclos.server.ruleengine.NuclosInputRequiredException;
import org.nuclos.server.statemodel.NuclosSubsequentStateNotLegalException;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class DataServiceHelper extends MetaDataServiceHelper implements ButtonConstants {
	
	private static final Logger LOG = LoggerFactory.getLogger(DataServiceHelper.class);
	
	public static final String RECEIVER_ID = "receiverId";
	
	final ReportFacadeRemote reportFacadeRemote = SpringApplicationContextHolder.getBean(ReportFacadeRemote.class);
	
	protected final JsonObjectBuilder getBoSelf(String bom, String id) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom, id);
		
		JsonObjectBuilder row = null;
		try {
			if (info.isUidEntity()) {
				return getRow(info, Rest.translateFqn(getBOMeta(), id));
			}
			
			row = getRow(info, Long.parseLong(id));
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));			
		}
		
		if (row == null) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}
		
		return row;
	}
	
	protected final Response deleteBo(String bom, String id) { 
		SessionEntityContext info =  checkPermission(E.ENTITY, bom, id);
		
        try {
        	if (info.isUidEntity()) {
        		Rest.facade().delete(info.getEntity(), Rest.translateFqn(getBOMeta(), id), false);
        		
        	} else {
        		Rest.facade().delete(info.getEntity(), Long.parseLong(id), false);
        		
        	}
            return Response.ok().build();
        } catch (Exception ex) {
        	throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
        }
	}
	
	protected final Response unlockBo(String bom, String id) { 
		SessionEntityContext info =  checkPermission(E.ENTITY, bom, id);
		
        try {
        	Rest.facade().unlock(info.getEntity(), Long.parseLong(id));
            return Response.ok().build();
        } catch (Exception ex) {
        	throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, bom));
        }
	}

	protected JsonObjectBuilder executeRule(
			final String boMetaId,
			final String boId,
			final String ruleId,
			final JsonObject postData
	) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);

		updateInputContext(postData);

		EntityObjectVO<?> eo = null;
		JsonObjectBuilder result = null;
		try {
			if (info.isUidEntity()) {
				eo = Rest.facade().get(info.getEntity(), Rest.translateFqn(getBOMeta(), boId), true);
			} else {
				eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);
			}

			result = executeRule(eo, ruleId);

		} catch (Exception ex) {
			handleInputRequiredException(ex, info);
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}

		if (result == null) {
			throw new NuclosWebException(Status.NOT_FOUND);
		}

		return result;
	}

	private <PK> JsonObjectBuilder executeRule(EntityObjectVO<PK> eo, final String ruleId) throws CommonBusinessException {
		if (!Rest.facade().isActionAllowed(Actions.ACTION_EXECUTE_RULE_BY_USER)) {
			throw new NuclosWebException(Status.FORBIDDEN);
		}

		try {
			eo = Rest.facade().fireCustomEventSupport(eo, ruleId);
		} finally {
			clearInputContext();
		}

		return getJsonObjectBuilder(eo);
	}

	protected final Response stateChange(String boMetaId, String id, String stateId) {
		SessionEntityContext info =  checkPermission(E.ENTITY, boMetaId, id);
		try {
			Rest.facade().changeState(info.getEntity(), Long.parseLong(id), Rest.translateFqn(E.STATE, stateId));
            return Response.ok().build();
        } catch (Exception ex) {
        	throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
        }
	}
	
	protected final Response stateChange(String boMetaId, String id, Integer numeral) {
		SessionEntityContext info =  checkPermission(E.ENTITY, boMetaId, id);
		try {
			UID entityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			List<StateVO> states = Rest.getSubsequentStates(entityUid, Long.parseLong(id));
			for (StateVO state: states) {
				if (state.getNumeral() == numeral) {
					Rest.facade().changeState(info.getEntity(), Long.parseLong(id), state.getId());
					return Response.ok().build();
				}
			}
			return Response.status(Status.BAD_REQUEST).build();
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));
		}
	}

	
	private void handleInputRequiredException(Exception ex, SessionEntityContext info) throws NuclosWebException {
		InputSpecification inputSpecification = null;
		if (ex instanceof NuclosInputRequiredException) {
			inputSpecification = ((InputRequiredException)ex.getCause()).getInputSpecification();
		} else if (ex instanceof InputRequiredException) {
			inputSpecification = ((InputRequiredException)ex).getInputSpecification();	
		}

		if (inputSpecification != null) {
			throw new InputRequiredWebException(inputSpecification);
		}
	}

	protected final <PK> JsonObjectBuilder updateBo(String id, JsonObject object) {
		SessionEntityContext info = checkPermission(object);
		try {

			extractReceiverId(object);
			updateInputContext(object);
			EntityObjectVO<PK> eo = prepareEOForModification(object, NuclosMethod.UPDATE, info);

			if (object.get(ACTION_EXECUTE_CUSTOM_RULE) != null) {
				return executeRule(eo, object.getString(ACTION_EXECUTE_CUSTOM_RULE));

			} else {
				return insertUpdateDelete(eo, NuclosMethod.UPDATE, info);

			}
			
		} catch (NuclosSubsequentStateNotLegalException ex) {
			final String sErrorMsg = Rest.getMessage("GenericObjectCollectController.34", "Der Statuswechsel konnte nicht vollzogen werden.");
			throw new NuclosWebException(Response.Status.METHOD_NOT_ALLOWED, sErrorMsg);
        } catch (CommonStaleVersionException ex) {
            throw new NuclosWebException(Response.Status.CONFLICT);
		} catch (Exception ex) {
			handleInputRequiredException(ex, info);
			throw new NuclosWebException(ex, info.getEntity());
		} finally {
			// reset inputContext after save
	     	clearInputContext();
		}
	}

	protected <PK> JsonObjectBuilder getBoWithAllDefaultValues(String bom) {
		//NUCLOS-4221: Sub-forms grant implicitly the privileges for certain entities. Bo-Generation of a subform-row is needed and thus the direct check is not possible.
		//ValidationInfo info =  validateSession(E.ENTITY, bom, null);

		//TODO: Evaluate if this means a security breach. Until now it doesn't look very dangerous as any request for real data is not possible.
		SessionEntityContext info = checkPermission(E.ENTITY, bom, null, false);
		
		try {
			EntityObjectVO<PK> eo = Rest.facade().getBoWithDefaultValues(info.getEntity());
			UsageCriteria criteria;
			try {
				criteria = Rest.getUsageCriteriaFromEO(eo);				
			} catch (NuclosFatalException nfe) {
				//NUCLOS-5417 g)
				if (nfe.getMessage().startsWith("statemodel.usages.error.null")) {
					SpringLocaleDelegate localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
					String msg = localeDelegate.getMessage("NuclosCollectControllerFactory.2", "No statemodel defined");
					throw new NuclosBusinessException(msg);
				}
				throw nfe;
			}
			
			//NUCLOS-4324: DefaultValueFromVLPWithDefault.
			List<LayoutAdditions> lstAdditions = getComponentsFromLayoutByContext(criteria, true);
			for (LayoutAdditions addition : lstAdditions) {
				WebValueListProvider wvlp = addition.getValueListProvider();
				
				if (wvlp != null && wvlp.hasDefaultMarker() && !wvlp.hasDatasourceParameters()) {
					List<CollectableField> coll = Rest.facade().getVLPData(
							addition.getUID(),
							wvlp,
							null,
							null,
							DEFAULT_RESULT_LIMIT,
							true,
							Rest.facade().getCurrentMandatorUID()
					);
					
					if (!coll.isEmpty()) {
						CollectableField cf = coll.get(0);
						eo.setFieldValue(addition.getUID(), cf.getValue());
						
						if (cf.getValueId() instanceof Long) {
							eo.setFieldId(addition.getUID(), (Long)cf.getValueId());
							
						} else if (cf.getValueId() instanceof UID) {
							eo.setFieldUid(addition.getUID(), (UID)cf.getValueId());
						
						}
					}
				}
			}
			
			UsageProperties up = new UsageProperties(criteria, this);
			RValueObject<PK> rvobj = new RValueObject<PK>(eo, JsonBuilderConfiguration.getNoAddsAndSkipStates(), up, null);

			return rvobj.getJSONObjectBuilder();
		} catch (CommonBusinessException | CommonFatalException ex) {
			throw new NuclosWebException(ex, info.getEntity());		
		}
	}
	
	protected JsonObjectBuilder generateBo(String boMetaId, String id, String generationId, JsonObject data) {
		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, id);
		
		try {
			
			updateInputContext(data);
			
			BoGenerationResult genResult = Rest.facade().generateBo(info.getEntity(), Collections.singletonList(Long.parseLong(id)), Rest.translateFqn(E.GENERATION, generationId));
			return JsonFactory.buildJsonObject(genResult, this);			
		} catch (CommonBusinessException ex) {
			throw new NuclosWebException(ex, info.getEntity());		
		} catch (Exception ex) {

			InputSpecification inputSpecification = null;
			if (ex instanceof NuclosInputRequiredException) {
				inputSpecification = ((InputRequiredException)ex.getCause()).getInputSpecification();
			} else if (ex instanceof InputRequiredException) {
				inputSpecification = ((InputRequiredException)ex).getInputSpecification();	
			}

			if (inputSpecification != null) {
				throw new InputRequiredWebException(inputSpecification);
			} else {
				throw new NuclosWebException(ex, info.getEntity());
			}
			
		} finally {
			// reset inputContext after save
	     	clearInputContext();
		}
	}

	protected final <PK> JsonObjectBuilder insertBo(String bom, JsonObject object) {
		SessionEntityContext info = checkPermission(object);
		try {
			updateInputContext(object);
			EntityObjectVO<PK> eo = prepareEOForModification(object, NuclosMethod.INSERT, info);
			return insertUpdateDelete(eo, NuclosMethod.INSERT, info);
			
		} catch (Exception ex) {
			handleInputRequiredException(ex, info);
			throw new NuclosWebException(ex, info.getEntity());
		}			
	}
	
	protected final NuclosFile byteAttributeValueWithValidation(String boMetaId, String id, String attrId) {
		SessionEntityContext info =  checkPermission(E.ENTITY, boMetaId, id);
		return byteAttributeValue(info.getMasterMeta(), id, attrId, true);
	}

	protected final NuclosFile byteAttributeValue(EntityMeta<?> eMeta, String id, String attrId, boolean check) {
        try {
        	EntityObjectVO<?> eo;
        	if (eMeta.isUidEntity()) {
        		eo = Rest.facade().get(eMeta.getUID(), id, check);
        		
        	} else {
        		eo = Rest.facade().get(eMeta.getUID(), Long.parseLong(id), check);
        		
        	}
        	UID fieldUID = Rest.translateFqn(E.ENTITYFIELD, attrId);
        	
        	NuclosFile result = null;
        	Object fieldValue = eo.getFieldValue(fieldUID);
       		if (fieldValue instanceof NuclosImage) {
       			NuclosImage ni = (NuclosImage) fieldValue;
       			result = new NuclosFile(ni.getName(), ni.getContent());
        	} else if (fieldValue instanceof GenericObjectDocumentFile) {
        		GenericObjectDocumentFile godf = (GenericObjectDocumentFile) fieldValue;
        		try {
        			String filePath = NuclosFileUtils.getPathNameForDocument(godf);
        			if (new File(filePath).exists()) {
        				result = new NuclosFile(godf.getFilename(), godf.getContents());
        			}
        		} catch (Exception e) {
        			LOG.warn("Unable to get generic object document file:", e);
        		}
        	}
       		return result;
        	
        } catch (Exception ex) {
        	throw new NuclosWebException(ex, eMeta.getUID());
        }
	}

	private <PK> JsonObjectBuilder getRow(SessionEntityContext info, PK pk) throws CommonBusinessException {
		EntityObjectVO<PK> eo = Rest.facade().get(info.getEntity(), pk, true);
		if (eo == null) {
			return null;
		}
   		UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this);
    	return new RValueObject<PK>(eo, JsonBuilderConfiguration.getFull(), up, null).getJSONObjectBuilder();
	}
	
	private FilterJ getFilterJ(JsonObject queryContext, Long defaultChunkSize) {
		return new FilterJ(this, queryContext, defaultChunkSize);
	}
	
	protected <PK> JsonObjectBuilder getBoList(String bom, JsonObject queryContext) {
		return getBoList(bom, queryContext, 40L);
	}	
	
	protected <PK> JsonObjectBuilder getBoList(String bom, JsonObject queryContext, Long defaultChunkSize) {
		SessionEntityContext info = checkPermission(E.ENTITY, bom);
		FilterJ filter = getFilterJ(queryContext, defaultChunkSize);
		boolean bAllFields = filter.showAllFields(); //"all".equalsIgnoreCase(filter.getFields());
		
    	try {
   		    Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(getBOMeta().getUID()).values() :
   		    		Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
   		    
   		    Collection<UID> fieldUids = new HashSet<UID>();
   		    for (FieldMeta<?> fm : fields) {
   		    	fieldUids.add(fm.getUID());
   		    }
   		    
    		Collection<EntityObjectVO<PK>> eos;
    		EntityMeta<?> eMeta = getBOMeta();
    		CollectableSearchExpression clctse = filter.getSearchExpression(getBOMeta().getUID(), fields);
    		
   			Long istart = filter.getStart();
   			long chunksize = filter.getEnd() - istart + 1;
   			
   			Boolean all;
   		    Integer count;
   		        		
    		if (!filter.countTotal()) {
				count = null;

				if (chunksize == 0) {
					eos = Collections.emptyList();
					all = null;
				} else {

					// Try to fetch chunksize + 1 to determine if there is more data than chunksize
					ResultParams resultParams = new ResultParams(fieldUids, istart, chunksize + 1, true);
					eos = Rest.facade().getEntityObjectsChunk(eMeta.getUID(), clctse, resultParams);

					// If we fetched more than chunksize EOs remove the additional EO
					all = eos.size() <= chunksize;
					if (!all && eos.size() > 0) {
						Object last = CollectionUtils.get(eos, eos.size()-1);
						eos.remove(last);
					}
				}

    		} else {
        		count = Rest.facade().countEntityObjectRows(eMeta.getUID(), clctse).intValue();
        		all = count < chunksize;
        		if (count == 0) {
        			eos = new ArrayList<EntityObjectVO<PK>>();
        			
        		} else {
        			if (istart == 0L && all) {
        				istart = -1L;
        			}
    				ResultParams resultParams = new ResultParams(fieldUids, istart, chunksize, true);
        			eos = Rest.facade().getEntityObjectsChunk(eMeta.getUID(), clctse, resultParams);   
        			
        		}    			
    		}
    		
			JsonObjectBuilder json = new ResultRVO<PK>(eos, all, count, filter.getJsonConfig(), this).getJSONObjectBuilder();

			EntityPermission permission = Rest.getEntityPermission(info.getEntity());
			boolean canCreateBo = info.getMasterMeta().isEditable() && permission.isInsertAllowed();
			json.add("canCreateBo", canCreateBo);

    		return json;

    	} catch (Exception ex) {
			throw new NuclosWebException(ex, info.getEntity());    		
    	}
    }
	
	
	/**
	 * see ReportExportController.export(...)
	 */
	protected ResultListExportRVO getBoListExport(String boMetaId, String format, boolean pageOrientationLandscape, boolean isColumnScaled, JsonObject queryContext) {
		
		if (!Rest.facade().isActionAllowed(Actions.ACTION_EXPORT_SEARCHRESULT)) {
			throw new NuclosWebException(Response.Status.UNAUTHORIZED);
		}			

		SessionEntityContext info = checkPermission(E.ENTITY, boMetaId);
		FilterJ filter = getFilterJ(queryContext, Long.MAX_VALUE);
		boolean bAllFields = filter.showAllFields(); //"all".equalsIgnoreCase(filter.getFields());
		
		try {
			Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(getBOMeta().getUID()).values() :
				Rest.getFieldsFromQueryAttributesAndSystem(getBOMeta().getUID(), filter.getAttributes());
			
			CollectableSearchExpression clctexpr = filter.getSearchExpression(getBOMeta().getUID(), fields);
			
			return exportBoList(boMetaId, format, pageOrientationLandscape, isColumnScaled, fields, clctexpr);
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, info.getEntity());    		
		}
	}

	private ResultListExportRVO exportBoList(String boMetaId, String format, boolean pageOrientationLandscape,
			boolean isColumnScaled, Collection<FieldMeta<?>> fields, CollectableSearchExpression clctexpr)
			throws NuclosReportException, IOException {
			final List<CollectableEntityField> lstclctefweSelected = new ArrayList<>();
			List<Integer> selectedFieldWidth = new ArrayList<>();
			ReportOutputVO.Format outputFormat = ReportOutputVO.Format.CSV;
			if ("xls".equals(format)) {
				outputFormat = ReportOutputVO.Format.XLS;
			} else if ("xlsx".equals(format)) {
				outputFormat = ReportOutputVO.Format.XLSX;
			} else if ("pdf".equals(format)) {
				outputFormat = ReportOutputVO.Format.PDF;
			}
			String customUsage = null;
			ReportOutputVO.PageOrientation orientation = pageOrientationLandscape ? ReportOutputVO.PageOrientation.LANDSCAPE : ReportOutputVO.PageOrientation.PORTRAIT;
			
			for (FieldMeta<?> fm : fields) {
				if (fm.isHidden() 
					|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
					|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
					|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
					|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
					|| fm.getUID().equals(SF.VERSION.getUID(fm.getEntity()))
					|| fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())
					) {
					continue;
				}
				lstclctefweSelected.add(SearchConditionUtils.newEntityField(fm));
				selectedFieldWidth.add(200);
			}

			NuclosFile file = reportFacadeRemote.prepareSearchResult(clctexpr, lstclctefweSelected, selectedFieldWidth, getBOMeta().getUID(), outputFormat, customUsage, orientation, isColumnScaled);
			
			RestPrintoutFileDownloadCache downloadCache = SpringApplicationContextHolder.getBean(RestPrintoutFileDownloadCache.class);

			String boId = null; 
			String fileId = downloadCache.cacheFile(boMetaId, boId, format, file);
			CacheResult cached = downloadCache.get(boMetaId, boId, format, fileId);
			
			ResultListExportRVO result = new ResultListExportRVO(this);
			result.setFile(new ResultListExportRVO.OutputFile(boMetaId, cached.fileName, format, fileId));
			return result;
	}

    protected JsonObjectBuilder getDependenceList(String boMetaId, String reffield, String fk, DependenceBoLinksFactory linksFactory) {
    	return getDependenceList(boMetaId, reffield, fk, linksFactory, null, null);
    }
    
    protected JsonObjectBuilder getDependenceList(String boMetaId, String reffield, String fk, DependenceBoLinksFactory linksFactory, String sortString, JsonObject queryContext) {
		UID fieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);
		
		try {
			UID parentEntityUid = Rest.translateFqn(E.ENTITY, boMetaId);
			FieldMeta<?> fieldMeta = Rest.getEntityField(fieldUid);
			EntityMeta<?> subformMeta = Rest.getEntity(fieldMeta.getEntity());
			EntityMeta<?> parentEntityMeta = Rest.getEntity(parentEntityUid);
			
			//TODO implement the chart BO-Metas and switch it on in WebLayout
			if (subformMeta.isChart() && subformMeta.getParentEntity() == null) {
				return Json.createObjectBuilder();
			}
			
			SessionEntityContext info = checkPermissionForSubform(parentEntityMeta, fieldMeta, fk, true);
			
			return subloadPK(info, fk, linksFactory, sortString, queryContext);
			
		} catch (Exception cbe) {
			throw new NuclosWebException(cbe, fieldUid);
		}
    }

    protected ResultListExportRVO exportDependenceList(
    		String boMetaId, 
    		String reffield, 
    		String fk, 
    		DependenceBoLinksFactory linksFactory, 
    		String sortString, 
    		JsonObject queryContext,
    		String outputFormat,
    		boolean pageOrientationLandscape,
			boolean isColumnScaled
    		) {
    	
    	UID refFieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);
    	try {
    		FieldMeta<?> refFieldMeta = Rest.getEntityField(refFieldUid);
    		EntityMeta<?> subformMeta = Rest.getEntity(refFieldMeta.getEntity());
    		
    		SessionEntityContext info = checkPermissionForSubform(subformMeta, refFieldMeta, fk, true);
	
			FilterJ filter = getFilterJ(queryContext, 5000L);
			
			boolean bAllFields = filter.showAllFields();
			Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(subformMeta.getUID()).values() :
					Rest.getFieldsFromQueryAttributesAndSystem(subformMeta.getUID(), filter.getAttributes());
			Collection<UID> fieldUids = new ArrayList<UID>();
			
			List<FieldMeta<?>> f = new ArrayList<FieldMeta<?>>();
			for (FieldMeta<?> fm : fields) {
				if (fm.isHidden() 
						|| fm.getUID().equals(SF.CREATEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CREATEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDBY.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.CHANGEDAT.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.VERSION.getUID(fm.getEntity()))
						|| fm.getUID().equals(SF.STATEICON.getUID(fm.getEntity()))
						|| fm.getDbColumn().equalsIgnoreCase(SF.PK_ID.getDbColumn())
						) {
						continue;
					}
				
				f.add(fm);
				fieldUids.add(fm.getUID());
			}

			UID subform = subformMeta.getUID();
			final CollectableSearchExpression clctexpr = filter.getSearchExpression(subform, MetaProvider.getInstance().getAllEntityFieldsByEntity(subform).values());
			
			Long relatedPK = new Long(fk);
			final CollectableSearchCondition cond = SearchConditionUtils.newPkComparison(refFieldUid, ComparisonOperator.EQUAL, relatedPK);
			clctexpr.setSearchCondition(cond);
			return exportBoList(boMetaId, outputFormat, pageOrientationLandscape, isColumnScaled, f, clctexpr);
    		
    	} catch (Exception cbe) {
    		throw new NuclosWebException(cbe, refFieldUid);
    	}
    }
    
    protected <PK> JsonObjectBuilder subloadPK(SessionEntityContext info, String fk, DependenceBoLinksFactory linksFactory, final String sortString, JsonObject queryContext) throws CommonBusinessException {
    	EntityMeta<?> subformMeta = Rest.getEntity(info.getReffieldMeta().getEntity());
		String boType = null;
		if (subformMeta instanceof E._Generalsearchdocument) {
			boType = "document";
		}
			
		FilterJ filter = getFilterJ(queryContext, 5000L);
		
		boolean bAllFields = filter.showAllFields();
		Collection<FieldMeta<?>> fields = bAllFields ? Rest.getAllEntityFieldsByEntityIncludingVersion(subformMeta.getUID()).values() :
	    		Rest.getFieldsFromQueryAttributesAndSystem(subformMeta.getUID(), filter.getAttributes());

		Collection<UID> notAllowed = getNotReadAllowedFieldsForSubform(subformMeta, info.getUsage());

		Collection<UID> fieldUids = new ArrayList<UID>();
		for (FieldMeta<?> fMeta : fields) {
			if (!notAllowed.contains(fMeta.getUID())) {
				fieldUids.add(fMeta.getUID());
			}
		}
		
		Boolean all;
		Collection<EntityObjectVO<PK>> eos;
		if (Rest.facade().isTemporaryId(fk)) {
			EntityObjectVO<Long> temporaryObject = Rest.facade().getTemporaryObject(fk);
			@SuppressWarnings("unchecked")
			Set<IDependentKey> alreadyLoaded = (Set<IDependentKey>) temporaryObject.getFieldValue(RValueObject.TEMPORARY_DEPENDENTS_ALREADY_LOADED_FIELD);
			IDependentKey dependentKey = DependentDataMap.createDependentKey(info.getReffieldMeta());
			if (alreadyLoaded.contains(dependentKey)) {
				throw new NuclosWebException(Response.Status.NOT_FOUND);
			}
			eos = temporaryObject.getDependents().getDataPk(dependentKey);
			alreadyLoaded.add(dependentKey);
			all = true;
			
		} else {
			PK fkpk = (PK) (subformMeta.isUidEntity() ? Rest.translateFqn(E.ENTITYFIELD, fk): Long.parseLong(fk));
			
			final CollectableSearchExpression filterExpr = filter.getSearchExpression(subformMeta.getUID(), MetaProvider.getInstance().getAllEntityFieldsByEntity(subformMeta.getUID()).values());
			
			Long chunksize = filter.getEnd();
			if (chunksize != null && filter.getStart() != null) {
				chunksize = chunksize - filter.getStart() + 1;
			}
			
			ResultParams resultParams = new ResultParams(fieldUids, filter.getStart(), chunksize + 1, true);

			eos = Rest.facade().getSubformData(subformMeta.getUID(), info.getReffieldMeta().getUID(), fkpk, filterExpr, resultParams);
			
			all = eos.size() <= chunksize;
			if (!all && eos.size() > 0) {
				Object last = CollectionUtils.get(eos, eos.size()-1);
				eos.remove(last);
			}
		}
		
		List<EntityObjectVO<PK>> eol = new ArrayList<>(eos);
		if (sortString != null) {
			
			List<String> sortFields;
			if (sortString != null && sortString.length() != 0) {
				sortFields = new ArrayList<>();
				for (String sortingFieldUid : sortString.split(",")) {
					sortFields.add(sortingFieldUid);
				}

				sortResultList(eol, sortFields);
			}
		}

		JsonBuilderConfiguration jsonConfig = filter.getJsonConfig();
		boolean canOpenDetail = false;
		if (subformMeta.isDynamic()) {
			UID detailEntity = subformMeta.getDetailEntity();
			canOpenDetail = detailEntity != null && isReadAllowedForEntity(subformMeta.getDetailEntity());
		} else {
			canOpenDetail = isReadAllowedForEntity(info.getReffieldMeta().getEntity());
		}
		jsonConfig.setWithDetailLink(canOpenDetail);

		ResultRVO<PK> resultRvo = new ResultRVO<>(
				eol,
				all,
				eol.size(),
				jsonConfig,
				this,
				boType,
				linksFactory
		);

		return resultRvo.getJSONObjectBuilder();
	}
	
	protected JsonObjectBuilder getDependencyDetails(String bometa, String parentBoId, String reffield, String subBoId, DependenceBoLinksFactory linksFactory) {
		UID entityUid = Rest.translateFqn(E.ENTITY, bometa);
		UID reffieldUid = Rest.translateFqn(E.ENTITYFIELD, reffield);

		try {
			IDependentKey dependentKey = DependentDataMap.createDependentKey(reffieldUid);
			
			EntityObjectVO<?> eo;
			
			if (Rest.facade().isTemporaryId(parentBoId)) {
				EntityObjectVO<Long> temporaryObject = Rest.facade().getTemporaryObject(parentBoId);
				if (!getUser().equals(temporaryObject.getCreatedBy())) {
					throw new NuclosWebException(Response.Status.FORBIDDEN);
				}
				
				Collection<EntityObjectVO<?>> tempEos = temporaryObject.getDependents().getData(dependentKey);
				for (EntityObjectVO<?> tempEo : tempEos) {
					String temporaryId = tempEo.getFieldValue(RValueObject.TEMPORARY_ID_FIELD, String.class);
					if (subBoId.equals(temporaryId)) {
						eo = tempEo;
						break;
					}
				}
			} else {
				// NUCLOS-5983 2)
				checkPermissionForSubform(Rest.getEntity(entityUid), Rest.getEntityField(reffieldUid), parentBoId, true);
			}

			eo = subGetEoAndSetMasterUsage(entityUid, parentBoId, reffieldUid, subBoId);
	   		UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this);
	    	return new RValueObject(eo, JsonBuilderConfiguration.getFull(), up, linksFactory).getJSONObjectBuilder();
	    	
		} catch (CommonBusinessException cbe) {
			throw new NuclosWebException(cbe, reffieldUid);
		}
	}
	
	protected EntityObjectVO<?> subGetEoAndSetMasterUsage(UID parentUid, String parentBoId, UID reffieldUid, String pk) throws CommonBusinessException {
		SessionEntityContext subInfo = checkPermissionForReference(parentUid, parentBoId, reffieldUid);
		EntityMeta<?> subformMeta = Rest.getEntity(subInfo.getReffieldMeta().getEntity());
		EntityObjectVO<?> eo;
		if (subformMeta.isUidEntity()) {
			// TODO
			EntityMeta<?> TODO = null;
			eo = Rest.facade().get(subformMeta.getUID(), Rest.translateFqn(TODO, pk), false);
		} else {
			eo = Rest.facade().get(subformMeta.getUID(), Long.parseLong(pk), false);
		}
		
		if (subInfo.isUidEntity()) {
			UID masterPK = eo.getFieldUid(subInfo.getReffieldMeta().getUID());
			pk = Rest.translateUid(E.ENTITYFIELD, masterPK);
		} else {
			Long masterPK = eo.getFieldId(subInfo.getReffieldMeta().getUID());
			pk = masterPK.toString();
		}
		SessionEntityContext info = checkPermission(E.ENTITY, Rest.translateUid(E.ENTITY, subInfo.getEntity()), pk);

		Collection<UID> fields2Clear = getNotReadAllowedFieldsForSubform(subformMeta, info.getUsage());
		eo.clearFields(fields2Clear);
		
		return eo;
	}

	// TODO: NUCLOS-6140 Centralize
	// NUCLOS-6134
	private Collection<UID> getNotReadAllowedFieldsForSubform(EntityMeta<?> subformMeta, UsageCriteria uc) {
		Collection<UID> notAllowedFields = new HashSet<>();
		SubformPermission subformPermission = Rest.facade().getSubformPermission(subformMeta.getUID(), uc);
		for (FieldMeta<?> fMeta : subformMeta.getFields()) {
			if (subformPermission == null) {
				notAllowedFields.add(fMeta.getUID());
			}
			// NUCLOS-6183 Do not add Systemfields
			else if (!fMeta.isSystemField() && !subformPermission.getGroupPermission(fMeta.getFieldGroup()).includesReading()) {
				notAllowedFields.add(fMeta.getUID());
			}
		}
		return notAllowedFields;
	}

	/**
	 * evaluates a field expression like '<b>uid{Li4MRC8s0YNQkrVPW10E}</b> uid{CgOmkIMnskz4iYXYHpRT}' 
	 */
	protected final <PK> String evaluateExpressionString(UID boMetaId, EntityObjectVO<PK> eo, String expression) {
		
		ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(expression, null, null);
		Iterator<IFieldUIDRef> itParser =  uidParser.iterator();
		
		final StringBuffer result = new StringBuffer(expression.length());
		while (itParser.hasNext()) {
			IFieldUIDRef part = itParser.next();
			if (part.isConstant()) {
				result.append(part.getConstant());
			} else {
				UID fieldUID = part.getUID();
				Object fieldValue = eo.getFieldValue(fieldUID);
				if (fieldValue != null) {
					if (fieldValue instanceof NuclosImage) {

						String imageString = "<img src='" + this.getRestURI("imageresource", Rest.translateUid(E.ENTITY, boMetaId), eo.getPrimaryKey().toString(), Rest.translateUid(E.ENTITYFIELD, fieldUID)) + "?sessionid=" + getSessionId() + "'>";
						result.append(imageString);
						
					} else {
						result.append(StringUtils.xmlEncode(fieldValue.toString()));
					}
				}
			}
		}
		
		return result.toString();
	}	
		
	protected JsonArrayBuilder vlpdata(String field, ReferenceListParameterJson paramJson) {
    	try {
    		WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny();
    		String pk = paramJson.getPK();

			List<CollectableField> lstCF = Rest.facade().getVLPData(
					field != null ? Rest.translateFqn(E.ENTITYFIELD, field) : null,
					wvlp,
					pk,
					paramJson.getQuickSearchInput(),
					DEFAULT_RESULT_LIMIT,
					false,
					UID.parseUID(paramJson.getMandatorId())
			);
			return JsonFactory.buildJsonArray(Rest.transformToValueIdField(lstCF), this);
			
    	} catch (Exception ex) {
			throw new NuclosWebException(ex, field != null ? Rest.translateFqn(E.ENTITYFIELD, field) : null);    		
    	}
	}
	
	protected final String evaluateExpressionString(String boMetaId, String boId, String expression) {
		
		try {

			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
			EntityObjectVO eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);
			
			ForeignEntityFieldUIDParser uidParser = new ForeignEntityFieldUIDParser(expression, null, null);
			Iterator<IFieldUIDRef> itParser =  uidParser.iterator();
			
			final StringBuffer result = new StringBuffer(expression.length());
			while (itParser.hasNext()) {
				IFieldUIDRef part = itParser.next();
				if (part.isConstant()) {
					result.append(part.getConstant());
				} else {
					UID fieldUID = part.getUID();
					Object fieldValue = eo.getFieldValue(fieldUID);
					if (fieldValue != null) {
						if (fieldValue instanceof NuclosImage) {
							
							/*
							NuclosImage nuclosImage = (NuclosImage)fieldValue;
							byte[] imageBytes = nuclosImage.getContent();
							if (imageBytes != null)  {
								String base64Image =  new String(Base64.encodeBase64(imageBytes));
								result.append(base64Image);
							}
							*/
							
							String imageString = "<img src='" + this.getRestURI("imageresource", boMetaId, boId, fieldUID.toString()) + "?sessionid=" + getSessionId() + "'>";
							result.append(imageString);
							
						} else {
							result.append(StringUtils.xmlEncode(fieldValue.toString()));
						}
					}
				}
			}

			return result.toString();
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));			
		}
	}
	
	
	protected final byte[] getImageResource(String boMetaId, String boId, String fieldUID) {
		try {

			SessionEntityContext info = checkPermission(E.ENTITY, boMetaId, boId);
			EntityObjectVO eo = Rest.facade().get(info.getEntity(), Long.parseLong(boId), true);
			
			Object fieldValue = eo.getFieldValue(Rest.translateFqn(E.ENTITYFIELD, fieldUID));
			if (fieldValue != null) {
				if (fieldValue instanceof NuclosImage) {
					NuclosImage nuclosImage = (NuclosImage)fieldValue;
					byte[] imageBytes = nuclosImage.getContent();
					return imageBytes;
				}
			}
			
		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITY, boMetaId));			
		}
		return null;
	}

	/**
	 * Queries only the changedAt timestamp for the given entity object from the DB.
	 *
	 * @param entity
	 * @param boId
	 * @return
	 */
	protected InternalTimestamp getChangedAt(final EntityMeta entity, final Long boId) {
		FieldMeta<InternalTimestamp> fieldMeta = SF.CHANGEDAT.getMetaData(entity);
		EntityObjectVO<Long> data = Rest.facade().getDataForField(boId, fieldMeta);
		return data.getFieldValue(SF.CHANGEDAT);
	}

	protected void extractReceiverId(JsonObject object){
		if(object.containsKey(RECEIVER_ID)){
	 		Integer receiverId = object.getInt(RECEIVER_ID, 0);
	 		LOG.debug("RequestContext-->receiverId={}", receiverId);
		 	HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
	 		request.setAttribute(RECEIVER_ID, receiverId);
 		}else{
 			LOG.debug("RequestContext-->receiverId=NULL");
 		}
	}

	protected JsonArrayBuilder referencelistarray(String field) {
		UID fieldUid = checkPermissionForReferenceField(Rest.translateFqn(E.ENTITYFIELD, field));
		try {
			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(getQueryParameters(), this);
			WebValueListProvider wvlp = paramJson.getWebValueListProviderIfAny();
			String pk = paramJson.getPK();

			long chunkSize = DEFAULT_RESULT_LIMIT;
			if (paramJson.getChunkSize() != null && paramJson.getChunkSize() > 0) {
				chunkSize = paramJson.getChunkSize();
			}

			List<CollectableValueIdField> lstCVIF = Rest.facade().getReferenceList(
					fieldUid,
					paramJson.getQuickSearchInput(),
					chunkSize,
					wvlp,
					pk,
					UID.parseUID(paramJson.getMandatorId())
			);
			return JsonFactory.buildJsonArray(lstCVIF, this);

		} catch (Exception ex) {
			throw new NuclosWebException(ex, Rest.translateFqn(E.ENTITYFIELD, field));
		}
	}

	protected static <PK> void sortResultList(List<EntityObjectVO<PK>> resultList, final List<String> sortFieldList) {
		if (sortFieldList.isEmpty()) {
			return;
		}

		Collections.sort(resultList, new Comparator<EntityObjectVO<PK>>() {
			@Override
			public int compare(EntityObjectVO<PK> o1, EntityObjectVO<PK> o2) {
				for(String sortFieldString : sortFieldList) {
					String [] sfArray = sortFieldString.trim().split(":");

					UID sortField = Rest.translateFqn(E.ENTITYFIELD, sfArray[0].trim());
					boolean asc = sfArray.length > 1 ? "asc".equals(sfArray[1].trim().toLowerCase()) : true;
					String s1 = o1.getFieldValue(sortField) != null ? o1.getFieldValue(sortField).toString() : "";
					String s2 = o2.getFieldValue(sortField) != null ? o2.getFieldValue(sortField).toString() : "";
					if (s1.compareTo(s2) != 0) {
						return asc ? s1.compareTo(s2) : s2.compareTo(s1);
					}
				}
				return 0;
			}
		});
	}

	protected JsonArrayBuilder getMatrixColumns(String entity_x, JsonObject data, boolean bCamelCase, String boId) throws CommonBusinessException {
		String fieldVlp = bCamelCase ? "entityXVlpId" : "entity_x_vlp_id";
		String entity_x_vlp_id = data.containsKey(fieldVlp) ? data.getString(fieldVlp) : null;

		String fieldSorting = bCamelCase ? "entityXSortingFields" : "entity_x_sorting_fields";
		String entity_x_sorting_fields = data.containsKey(fieldSorting) ? data.getString(fieldSorting) : null;

		String fieldXHeader = bCamelCase ? "entityXHeader" : "entity_x_header";
		String entity_x_header = data.containsKey(fieldXHeader) ? data.getString(fieldXHeader) : null;

		// filter columns by VLP
		JsonArray xArray;

		if (entity_x_vlp_id != null && entity_x_vlp_id.length() != 0) { // with VLP

			String entity_x_vlp_idfieldname = data.getString(bCamelCase ? "entityXVlpIdfieldname" : "entity_x_vlp_idfieldname");
			String entity_x_vlp_fieldname = data.getString(bCamelCase ? "entityXVlpFieldname" : "entity_x_vlp_fieldname");
			String entity_x_vlp_reference_param_name = data.getString(bCamelCase ? "entityXVlpReferenceParamName" : "entity_x_vlp_reference_param_name");

			MultivaluedMap<String, String> vlpInputParameter = new MultivaluedHashMap<String, String>();
			vlpInputParameter.add("vlpvalue", "uid{" + entity_x_vlp_id + "}");
			vlpInputParameter.add("vlptype", "ds");
			vlpInputParameter.add(ValuelistProviderVO.DATASOURCE_IDFIELD, entity_x_vlp_idfieldname);
			vlpInputParameter.add(ValuelistProviderVO.DATASOURCE_NAMEFIELD, entity_x_vlp_fieldname);
			vlpInputParameter.add(entity_x_vlp_reference_param_name, boId);

			ReferenceListParameterJson paramJson = new ReferenceListParameterJson(vlpInputParameter);
			//TODO: Sling out Build()
			xArray = vlpdata(null, paramJson).build();

		} else { // without VLP

			JsonObject entityXList = getBoList(entity_x, null, 100000L).build();
			xArray = entityXList.getJsonArray("bos");
		}

		Set<Long> vlpIds =  new HashSet<Long>();
		List<Long> boIds = new ArrayList<Long>();

		for(JsonValue boJsonValue : xArray) {
			JsonObject jo = (JsonObject)boJsonValue;
			Long id = jo.containsKey("id") ? jo.getJsonNumber("id").longValue()
					: jo.getJsonNumber("boId").longValue();
			if (!vlpIds.contains(id)) {
				vlpIds.add(id);
				boIds.add(id);
			}
		}

		UID entityX = Rest.translateFqn(E.ENTITY, entity_x);

		List<EntityObjectVO<Long>> xAxisBoList = new ArrayList<EntityObjectVO<Long>>();

		IEntityObjectProcessor<Long> proc = NucletDalProvider.getInstance().getEntityObjectProcessor(entityX);
		for (EntityObjectVO<Long> eo : proc.getByPrimaryKeys(boIds)) {
			xAxisBoList.add(eo);
		}

		final List<String> sortFieldList = getSortedFieldListFqn(entity_x_sorting_fields, entity_x);

		sortResultList(xAxisBoList, sortFieldList);

		boolean evaluateHeader = !StringUtils.isNullOrEmpty(entity_x_header);

		JsonArrayBuilder xAxisJsonBoList = Json.createArrayBuilder();
		for (EntityObjectVO<Long> eovo : xAxisBoList) {

			JsonObjectBuilder dummy = Json.createObjectBuilder();
			dummy.add("boId", eovo.getPrimaryKey());
			if (evaluateHeader) {
				String title = evaluateExpressionString(Rest.translateFqn(E.ENTITY, entity_x), eovo, entity_x_header);
				dummy.add("_title", title);
			} else {
				JsonObject bo = getListEoJson(eovo).build();
				// TODO: GET TITLE AND INFO FROM RVALUEOBJECT without instanciating
				if (bo.containsKey("title")) {
					dummy.add("_title", bo.get("title"));
				}
			}
			// dummy.add("_info", bo.get("info"));
			xAxisJsonBoList.add(dummy);
		}

		return xAxisJsonBoList;
	}

	protected static List<String> getSortedFieldListFqn(String sortingFields, String entity) {
		final List<String> sortFieldList = new ArrayList<String>();

		// make full qualified name of x sorting fields
		if (!StringUtils.isNullOrEmpty(sortingFields)) {
			String[] sa = sortingFields.split(",");
			for (int i =0; i < sa.length; i++) {
				sortFieldList.add( entity + "_" + sa[i]);
			}
		}

		return sortFieldList;
	}

	protected  JsonArrayBuilder getMatrixRows(String entity_y, JsonObject data, boolean bCamelCase, String boId, List<EntityObjectVO<Long>> lstEos) throws CommonBusinessException {
		JsonArrayBuilder yAxisBoList = Json.createArrayBuilder();
		if (StringUtils.isNullOrEmpty(boId) || "null".equals(boId)) {
			return yAxisBoList;
		}

		String entity_y_parent_field = data.getString(bCamelCase ? "entityYParentField" : "entity_y_parent_field");

		String fieldYSorting = bCamelCase ? "entityYSortingFields" : "entity_y_sorting_fields";
		String entity_y_sorting_fields = data.containsKey(fieldYSorting) ? data.getString(fieldYSorting) : null;

		String fieldYHeader = bCamelCase ? "entityYHeader" : "entity_y_header";
		String entity_y_header = data.containsKey(fieldYHeader) ? data.getString(fieldYHeader) : null;

		UID entityY = Rest.translateFqn(E.ENTITY, entity_y);
		UID entityYRefField = Rest.translateFqn(E.ENTITYFIELD, entity_y_parent_field);
		Long lBoId = Long.parseLong(boId);

		Collection<EntityObjectVO<Long>> eos = Rest.facade().getDependentData(entityY, entityYRefField, lBoId);
		lstEos.clear();
		lstEos.addAll(eos);

		List<String> lstSorting = getSortedFieldListFqn(entity_y_sorting_fields, entity_y);
		sortResultList(lstEos, lstSorting);

		for (EntityObjectVO<Long> eo : eos) {
			Long id = eo.getPrimaryKey();
			String sid = String.valueOf(id);

			JsonObjectBuilder dummy = Json.createObjectBuilder();
			dummy.add("boId", id);
			if (!StringUtils.isNullOrEmpty(entity_y_header)) {
				dummy.add("evaluatedHeaderTitle", evaluateExpressionString(entityY, eo, entity_y_header));
			} else {
				JsonObject bo = getListEoJson(eo).build();
				// TODO: GET TITLE AND INFO FROM RVALUEOBJECT without instanciating
				if (bo.containsKey("title")) {
					dummy.add("evaluatedHeaderTitle", bo.get("title"));
				}
			}

			yAxisBoList.add(dummy);
		}

		return yAxisBoList;
	}

	protected <PK> JsonObjectBuilder getListEoJson(EntityObjectVO<PK> eo) throws CommonBusinessException {
		UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this);
		return new RValueObject<PK>(eo, JsonBuilderConfiguration.getList(null), up, null).getJSONObjectBuilder();
	}

	protected <PK> JsonObjectBuilder getThinEoJson(EntityObjectVO<PK> eo) throws CommonBusinessException {
		UsageProperties up = new UsageProperties(Rest.getUsageCriteriaFromEO(eo), this);
		return new RValueObject<PK>(eo, JsonBuilderConfiguration.getNoAddsAndSkipStates(), up, null).getJSONObjectBuilder();
	}

	protected <PK> JsonArrayBuilder getJsonBos(List<EntityObjectVO<PK>> lstEoRows) throws CommonBusinessException {
		JsonArrayBuilder bos = Json.createArrayBuilder();

		for (EntityObjectVO<PK> eo : lstEoRows) {
			bos.add(getThinEoJson(eo));
		}

		return bos;
	}

}
