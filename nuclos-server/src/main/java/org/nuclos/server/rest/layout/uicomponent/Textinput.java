package org.nuclos.server.rest.layout.uicomponent;


public class Textinput extends Element {
    public String value;
    public String boAttrId;

    public Textinput(String boAttrId) {
        this.boAttrId = boAttrId;
    }

    public String toString() {
        return "<input " +
				"id=\"attribute-" + boAttrId + "\" " +
                "type=\"text\" " +
                "[ngModel]=\"model.bo.getAttribute('" + boAttrId + "')\" " +
				"(ngModelChange)=\"model.bo.setAttribute('" + boAttrId + "', $event)\">";
    }
}
