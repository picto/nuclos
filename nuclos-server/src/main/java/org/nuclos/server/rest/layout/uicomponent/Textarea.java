package org.nuclos.server.rest.layout.uicomponent;


public class Textarea extends Element {
    public String value;
    public String boAttrId;

    public Textarea(String boAttrId) {
        this.boAttrId = boAttrId;
    }

    public String toString() {
        return "<textarea " +
				"id=\"attribute-" + boAttrId + "\" " +
                "[ngModel]=\"model.bo.getAttribute('" + boAttrId + "')\" " +
				"(ngModelChange)=\"model.bo.setAttribute('" + boAttrId + "', $event)\" " +
                "[ngModelOptions]=\"{standalone: true}\" " +
                "class=\"form-control\" " +
                "></textarea>";
    }
}
