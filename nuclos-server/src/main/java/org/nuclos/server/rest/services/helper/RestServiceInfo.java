package org.nuclos.server.rest.services.helper;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RestServiceInfo {
	
	/**
	 * Identifier. Optional
	 */
	String identifier() default "";
	
	/**
	 * REST service description
	 */
	String description() default "";
	
	/**
	 * example GET_POST Data (only reasonable for GET_POST methods) 
	 */
	String examplePostData() default "";

	/**
	 *  finalized version
	 */
	boolean isFinalized() default false;
	
	/**
	 * Validates current session if true (default). 
	 * Works only for paths without path parameter.
	 * (See org.nuclos.server.rest.SessionValidationRequestFilter)
	 */
	boolean validateSession() default true;
	
}
