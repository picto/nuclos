package org.nuclos.server.rest.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.nuclos.common.E;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.UID;
import org.nuclos.server.common.StateCache;
import org.nuclos.server.resource.ResourceCache;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.nuclos.server.rest.services.helper.WebContext;
import org.nuclos.server.statemodel.valueobject.StateVO;

@Path("/resources")
@Produces(MediaType.APPLICATION_JSON)
public class ResourceService extends WebContext {
	
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/stateIcons/{stateId}")
	@RestServiceInfo(identifier="stateIcon", isFinalized=true, description="List icon of state")
	public Response stateIcon(@PathParam("stateId") String stateId) {
		try {
			UID stateUID = Rest.translateFqn(E.STATE, stateId);
			StateVO state = StateCache.getInstance().getState(stateUID);
			NuclosImage image = state.getIcon();
			if (image != null) {
				byte[] imageBytes = image.getContent();
				return Response.ok(imageBytes).type("image/png").build();
			} else {
				return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
			}
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.STATE, stateId));    		
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/images/{resourceId}")
	@RestServiceInfo(identifier="resourceImage", isFinalized=true, description="Resource Image")
	public Response resourceImage(@PathParam("resourceId") String resourceId) {
		try {
			//TODO: E.RESOURCE correct? Compare with JsonFacory WebStaticComponent
			UID resourceUid = Rest.translateFqn(E.RESOURCE, resourceId);
			byte[] imageBytes = ResourceCache.getInstance().getResourceBytes(resourceUid);
			if (imageBytes != null) {
				return Response.ok(imageBytes).type("image/png").build();
			} else {
				return Response.ok(Rest.EMPTY_IMAGE).type("image/gif").build();
			}
		} catch (Exception e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.RESOURCE, resourceId));    		
		}
	}
	
}
