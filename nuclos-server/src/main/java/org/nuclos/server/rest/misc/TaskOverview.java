package org.nuclos.server.rest.misc;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.security.EntityPermission;

public class TaskOverview extends BoMetaOverview{
	private final UID taskUid;
	
	public TaskOverview(EntityMeta<?> eMeta, UID taskUid, String label, EntityPermission permission) {
		super(eMeta, label, permission, true);
		this.taskUid = taskUid;
	}

	public UID getTaskUID() {
		return taskUid;
	}
	
	@Override
	public String toString() {
		return "TaskUid=" + taskUid + "\n" + super.toString();
	}

}
