//Copyright (C) 2016  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.rest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.rest.mapper.NuclosObjectMapperProvider;
import org.nuclos.server.rest.services.AdminService;
import org.nuclos.server.rest.services.BoDocumentService;
import org.nuclos.server.rest.services.BoGenerationService;
import org.nuclos.server.rest.services.BoImageService;
import org.nuclos.server.rest.services.BoMetaService;
import org.nuclos.server.rest.services.BoPrintoutService;
import org.nuclos.server.rest.services.BoService;
import org.nuclos.server.rest.services.BoStateService;
import org.nuclos.server.rest.services.BusinessTestRestService;
import org.nuclos.server.rest.services.DataService;
import org.nuclos.server.rest.services.DependenceService;
import org.nuclos.server.rest.services.LayoutService;
import org.nuclos.server.rest.services.LoginService;
import org.nuclos.server.rest.services.MaintenanceRestService;
import org.nuclos.server.rest.services.MessageService;
import org.nuclos.server.rest.services.MetaDataService;
import org.nuclos.server.rest.services.NucletService;
import org.nuclos.server.rest.services.PreferenceService;
import org.nuclos.server.rest.services.ResourceService;
import org.nuclos.server.rest.services.UserService;
import org.nuclos.server.rest.services.helper.RestServiceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath("/rest/")
public class NuclosRestApplication extends ResourceConfig {

	private final static Logger LOG = LoggerFactory.getLogger(NuclosRestApplication.class);
	
	private static Set<Pair<String, String>> setSessionValidationDisabledForRelativePath;

	public NuclosRestApplication() {

		// Disable WADL generation for the REST service
		property("jersey.config.server.wadl.disableWadl", "true");

		register(LoginService.class);
		register(BoMetaService.class);
		register(BoService.class);
		register(BoGenerationService.class);
		register(DependenceService.class);
		register(BoImageService.class);
		register(BoDocumentService.class);
		register(BoStateService.class);
		register(BoPrintoutService.class);
		register(ResourceService.class);
		register(MetaDataService.class);
		register(DataService.class);
		register(MaintenanceRestService.class);
		register(MultiPartFeature.class);
		register(PreferenceService.class);
		register(MessageService.class);
		register(NuclosObjectMapperProvider.class);
		register(AdminService.class);
		register(BusinessTestRestService.class);
		register(UserService.class);
		register(LayoutService.class);
		register(NucletService.class);

		final String sAdditionalServicesClassName = LangUtils.defaultIfNull(ApplicationProperties.getInstance().getAdditionalRestServices(), AdditionalRestServices.class.getName());

		try {
			final Class<? extends AdditionalRestServices> clsAdditionalServicesClass = (Class<? extends AdditionalRestServices>) Class.forName(sAdditionalServicesClassName);
			AdditionalRestServices additionalRestServices = clsAdditionalServicesClass.newInstance();
			Set<Class<?>> classes = additionalRestServices.getClasses();
			for (Class<?> clazz : classes) {
				register(clazz);
			}
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException cnfe) {
			LOG.warn(cnfe.getMessage(), cnfe);
		}

		// REGISTER SINGLETON
		registerInstances(
				new CacheHeaderFilter(),
				new CrossOriginResourceSharingFilter(),
				new SessionValidationRequestFilter()
		);
	}
	
	private static Set<Pair<String, String>> getSetSessionValidationDisabledForRelativePath() {
		if (setSessionValidationDisabledForRelativePath == null) {
			Set<Pair<String, String>> setDisabled = new HashSet<>();
			
			for(Class<?> restServiceClass : new NuclosRestApplication().getClasses()) {

				String pathValueFromClass = "";
				for (Annotation annotation : restServiceClass.getAnnotations()) {
				    if (annotation instanceof Path) {
				    	Path path = (Path) annotation;
				    	pathValueFromClass = path.value();
				    }
				}

				for (Method method : restServiceClass.getMethods()) {
					if (!method.isAnnotationPresent(RestServiceInfo.class)) {
						continue;
					}

					boolean validateSession = method.getAnnotation(RestServiceInfo.class).validateSession();
					if (validateSession) {
						// cache only false, default is always true 
						continue;
					}
					
					boolean bGET = method.isAnnotationPresent(GET.class);
					boolean bPUT = method.isAnnotationPresent(PUT.class);
					boolean bPOST = method.isAnnotationPresent(POST.class);
					boolean bDELETE = method.isAnnotationPresent(DELETE.class);

					String spath = pathValueFromClass;
					if (method.isAnnotationPresent(Path.class)) {
						spath += method.getAnnotation(Path.class).value();
					}
					// only relative paths
					if (spath.startsWith("/")) {
						spath = spath.substring(1);
					}
					
					if (bGET) {
						setDisabled.add(new Pair<String, String>(spath, "GET"));
					}
					if (bPUT) {
						setDisabled.add(new Pair<String, String>(spath, "PUT"));
					}
					if (bPOST) {
						setDisabled.add(new Pair<String, String>(spath, "POST"));
					}
					if (bDELETE) {
						setDisabled.add(new Pair<String, String>(spath, "DELETE"));
					}
				}
			}
			
			setSessionValidationDisabledForRelativePath = Collections.unmodifiableSet(setDisabled);
		}
		return setSessionValidationDisabledForRelativePath;
	}
	
	/**
	 * Works only for paths without path parameter!
	 * 
	 * @param sRelativePath
	 * @param sMethod
	 * @return
	 */
	public static boolean isSessionValidationEnabledForRelativePath(String sRelativePath, String sMethod) {
		if (sRelativePath == null) {
			throw new IllegalArgumentException("sRelativePath must not be null");
		}
		if (sMethod == null) {
			throw new IllegalArgumentException("sMethod must not be null");
		}
		sMethod = sMethod.toUpperCase();
		if (!"GET".equals(sMethod) && !"PUT".equals(sMethod) && !"POST".equals(sMethod) && !"DELETE".equals(sMethod)) {
			// like "OPTIONS
			return false;
		}
		
		// Default is true
		boolean result = true;
		
		if (getSetSessionValidationDisabledForRelativePath().contains(new Pair<String, String>(sRelativePath, sMethod))) {
			// Validaten is disabled
			result = false;
		}
		return result;
	}
}
