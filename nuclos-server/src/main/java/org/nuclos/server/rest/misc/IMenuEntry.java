package org.nuclos.server.rest.misc;

import org.nuclos.common.UID;


public interface IMenuEntry {
	
	UID getUID();
	
	UID getProcessUid();

	String getLabel();

	String getIcon();
	
	boolean asMenuTree();
	
	boolean canCreateBO();
	
	boolean canReadBO();
	
	boolean isCreateNew();
}
