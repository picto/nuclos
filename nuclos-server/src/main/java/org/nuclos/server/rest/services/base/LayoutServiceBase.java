package org.nuclos.server.rest.services.base;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.JaxbMarshalUnmarshalUtil;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.schema.layout.layoutml.Layoutml;
import org.nuclos.schema.layout.rule.Rules;
import org.nuclos.schema.layout.web.WebCell;
import org.nuclos.schema.layout.web.WebComponent;
import org.nuclos.schema.layout.web.WebContainer;
import org.nuclos.schema.layout.web.WebLayout;
import org.nuclos.schema.layout.web.WebRow;
import org.nuclos.schema.layout.web.WebTab;
import org.nuclos.schema.layout.web.WebTabcontainer;
import org.nuclos.schema.layout.web.WebTable;
import org.nuclos.layout.transformation.rule.LayoutmlToRuleTransformer;
import org.nuclos.layout.transformation.weblayout.LayoutmlToWeblayoutTransformer;
import org.nuclos.layout.transformation.weblayout.calculated.LayoutmlToWeblayoutCalcTransformer;
import org.nuclos.layout.transformation.weblayout.fixed.LayoutmlToWeblayoutFixedTransformer;
import org.nuclos.layout.transformation.weblayout.responsive.LayoutmlToWeblayoutResponsiveTransformer;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.rest.ejb3.Rest;
import org.nuclos.server.rest.misc.NuclosWebException;
import org.xml.sax.SAXException;

/**
 * Provides base methods used by the Layout service.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class LayoutServiceBase {

	protected String getLayoutML(String sLayoutId) {
		try {
			UID layoutUID = Rest.translateFqn(E.LAYOUT, sLayoutId);
			return Rest.facade().getLayoutML(layoutUID);
		} catch (CommonBusinessException e) {
			throw new NuclosWebException(e, Rest.translateFqn(E.LAYOUT, sLayoutId));
		}
	}

	protected Layoutml getLayoutMLObj(String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		String layoutML = getLayoutML(layoutId);
		return JaxbMarshalUnmarshalUtil.unmarshal(layoutML, Layoutml.class);
	}

	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutFixed(String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutFixedTransformer(
				MetaProvider.getInstance(),
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutCalculated(String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutCalcTransformer(
				MetaProvider.getInstance(),
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	protected org.nuclos.schema.layout.web.WebLayout getWebLayoutResponsive(String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToWeblayoutTransformer transformer = new LayoutmlToWeblayoutResponsiveTransformer(
				MetaProvider.getInstance(),
				layoutML
		);
		org.nuclos.schema.layout.web.WebLayout webLayout = transformer.transform();

		return webLayout;
	}

	protected Rules getRules(String layoutId) throws JAXBException, SAXException, ParserConfigurationException {
		Layoutml layoutML = getLayoutMLObj(layoutId);
		LayoutmlToRuleTransformer transformer = new LayoutmlToRuleTransformer(MetaProvider.getInstance(), layoutML);
		Rules rules = transformer.transform();

		return rules;
	}
}
