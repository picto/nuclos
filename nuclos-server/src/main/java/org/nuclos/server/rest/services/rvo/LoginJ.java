package org.nuclos.server.rest.services.rvo;

import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginJ {

	public static final Logger LOG = LoggerFactory.getLogger("REST");

	String username;
	String password;
	String locale;
	boolean autologin;

	public LoginJ(JsonObject data) {
		try {
			username = data.getString("username");
			password = data.containsKey("password") ? data.getString("password") : null; 
			locale = data.containsKey("locale") ? data.getString("locale") : null;
			autologin = data.containsKey("autologin") ? data.getBoolean("autologin") : false;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new WebApplicationException(406);
		}
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		if (password == null) password = "";
		return password;
	}

	public String getLocale() {
		if (locale == null || locale.isEmpty()) locale = "de_DE";
		return locale;
	}

	public boolean isAutologin() {
		return autologin;
	}
}
