package org.nuclos.server.common.ejb3;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.nuclos.common.CommonEntityObjectFacade;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface EntityObjectFacadeLocal extends CommonEntityObjectFacade {

	<PK> void createOrUpdatePlainWithoutPermissionCheck(EntityObjectVO<PK> entity) throws CommonPermissionException;
	
	List<CollectableValueIdField> getReferenceList(FieldMeta<?> efMeta, String search, UID vlpUID, Map<String, Object> vlpParameter, Long iMaxRowCount, UID mandator) throws CommonBusinessException;
	
	<PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID subform, UID field, PK relatedId, Long maxRowCount);
	
	String getFieldGroupName(UID groupID);
	
	void evictGroupNamesCache();
	
    List<CollectableField> getProcessByEntity(UID entityUid, boolean bSearchMode);
    
    <PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entityUID, String customUsage) throws CommonBusinessException;
    
    <PK> void lockInNewTransaction(UID entityUID, PK pk, UID userUID) throws CommonPermissionException;
	
	<PK> UID lockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException;
	
	<PK> void unlockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException;
	
	<PK> void unlockInNewTransactionAfterCommit(UID entityUID, PK pk) throws CommonPermissionException;
	
}
