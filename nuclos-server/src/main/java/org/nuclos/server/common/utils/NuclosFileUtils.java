package org.nuclos.server.common.utils;

import java.io.File;
import java.io.IOException;

import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.NuclosSystemParameters;
import org.nuclos.server.documentfile.DocumentFileUtils;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class NuclosFileUtils {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosFileUtils.class);
	
	private NuclosFileUtils() {
	}

	public static String getPathNameForDocument(GenericObjectDocumentFile documentFile) {
		UID documentFileUID = null;
		Object documentFilePk = documentFile.getDocumentFilePk();
		if (documentFilePk instanceof UID) {
			documentFileUID = (UID) documentFilePk;
		}
		return getPathNameForDocument(documentFile, documentFileUID);
	}
	
	public static String getPathNameForDocument(GenericObjectDocumentFile documentFile, UID documentFileUID) {
		try {
			if (documentFile == null) {
				throw new CommonFatalException("godocumentfile.invalid.file");
			}
			if (documentFileUID == null) {
				throw new NuclosFatalException("godocumentfile.invalid.id");
			}
			File documentDir = NuclosSystemParameters.getDirectory(NuclosSystemParameters.DOCUMENT_PATH);
			documentDir.mkdirs();
			
			String sFilename = DocumentFileUtils.getDocumentFileName(documentFileUID, documentFile.getFilename());
			File file = new File(documentDir, sFilename);
			LOG.debug("Calculated path for document attachment: {}", file.getCanonicalPath());
			return file.getCanonicalPath();
		}
		catch (IOException e) {
			throw new NuclosFatalException(e);
		}
	}
}
