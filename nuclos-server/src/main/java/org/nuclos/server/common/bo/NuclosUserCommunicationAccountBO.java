//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.bo;

import org.nuclos.api.UID;
import org.nuclos.api.common.NuclosUserCommunicationAccount;
import org.nuclos.common.E;
import org.nuclos.common.NuclosPassword;
import org.nuclos.server.nbo.AbstractBusinessObject;

public class NuclosUserCommunicationAccountBO extends AbstractBusinessObject<UID>
		implements NuclosUserCommunicationAccount {

	public NuclosUserCommunicationAccountBO() {
		super(E.USER_COMMUNICATION_ACCOUNT.getUID().getString());
	}

	@Override
	public UID getUserId() {
		return getFieldUid(E.USER_COMMUNICATION_ACCOUNT.user.getUID().getString());
	}

	@Override
	public UID getCommunicationPortId() {
		return getFieldUid(E.USER_COMMUNICATION_ACCOUNT.communicationPort.getUID().getString());
	}

	@Override
	public String getAccount() {
		return getField(E.USER_COMMUNICATION_ACCOUNT.account.getUID().getString(), String.class);
	}

	@Override
	public String getPassword() {
		Object pw = getField(E.USER_COMMUNICATION_ACCOUNT.password.getUID().getString(), Object.class);
		if (pw instanceof NuclosPassword) {
			return ((NuclosPassword) pw).getValue();
		}
		if (pw instanceof String) {
			return (String) pw;
		}
		return null;
	}

	@Override
	public String getCustom1() {
		return getField(E.USER_COMMUNICATION_ACCOUNT.custom1.getUID().getString(), String.class);
	}

	@Override
	public String getCustom2() {
		return getField(E.USER_COMMUNICATION_ACCOUNT.custom2.getUID().getString(), String.class);
	}

	@Override
	public String getCustom3() {
		return getField(E.USER_COMMUNICATION_ACCOUNT.custom3.getUID().getString(), String.class);
	}

	@Override
	public void setAccount(String account) {
		setField(E.USER_COMMUNICATION_ACCOUNT.account.getUID().getString(), account);
	}

	@Override
	public void setPassword(String password) {
		setField(E.USER_COMMUNICATION_ACCOUNT.password.getUID().getString(), password);
	}

	@Override
	public void setCustom1(String custom1) {
		setField(E.USER_COMMUNICATION_ACCOUNT.custom1.getUID().getString(), custom1);
	}

	@Override
	public void setCustom2(String custom2) {
		setField(E.USER_COMMUNICATION_ACCOUNT.custom2.getUID().getString(), custom2);
	}

	@Override
	public void setCustom3(String custom3) {
		setField(E.USER_COMMUNICATION_ACCOUNT.custom3.getUID().getString(), custom3);
	}

}
