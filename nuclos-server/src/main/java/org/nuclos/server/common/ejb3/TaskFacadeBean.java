//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.CompositeCollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.LogicalOperator;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.common2.ServiceLocator;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.valueobject.TaskVO;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbInvalidResultSizeException;
import org.nuclos.server.dblayer.DbStatementUtils;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbId;
import org.nuclos.server.dblayer.expression.DbNull;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Facade bean for managing private tasks in the todolist.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class TaskFacadeBean extends NuclosFacadeBean implements TaskFacadeRemote {
	
	@Autowired
	private MasterDataFacadeHelper masterDataFacadeHelper;
	
	@Autowired
	private MasterDataFacadeLocal masterDataFacadeLocal;

	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	public TaskFacadeBean() {
	}
	
	private Transformer<MasterDataVO<Long>, TaskVO> mdToTaskTransformer = new Transformer<MasterDataVO<Long>, TaskVO>() {
		@Override
		public TaskVO transform(MasterDataVO<Long> mdvo) {
			String sDelegatorName = null;
			String sStatusName = null;
			if (mdvo.getFieldUid(E.TODOLIST.taskdelegator) != null) {
				try {
					MasterDataVO<UID> user = getMasterDataFacade().get(E.USER, mdvo.getFieldUid(E.TODOLIST.taskdelegator));
					sDelegatorName = user.getFieldValue(E.USER.name);
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);
				}
			}
			if (mdvo.getFieldUid(E.TODOLIST.taskstatus) != null) {
				try {
					MasterDataVO<UID> todostatus = getMasterDataFacade().get(E.TODOSTATUS, mdvo.getFieldUid(E.TODOLIST.taskstatus));
					sStatusName = todostatus.getFieldValue(E.TODOSTATUS.name);
				} catch (Exception e) {
					LOG.warn(e.getMessage(), e);
				}
			}
			return MasterDataWrapper.getTaskVO(mdvo, getObjectIdentifier(mdvo), sDelegatorName, sStatusName);
		}
	};
	
	
	/*
	private final MasterDataFacadeLocal getMasterDataFacade() {
		return masterDataFacade;
	}
	*/

   /**
    * get all tasks (or only unfinished tasks)
    * @param sOwner task owner to get tasks for
    * @param bUnfinishedOnly get only unfinished tasks
    * @return collection of task value objects
    */
   public Collection<TaskVO> getTasksByOwner(String sOwner, boolean bUnfinishedOnly, Integer iPriority) throws NuclosBusinessException {
	   	final Collection<TaskVO> result = new HashSet<TaskVO>();
		final UID userId = getUserUid(sOwner);
		try {
			for (TaskVO taskVO : getTaskVOsByCondition(null)) {
				if (existOwnerForTask(taskVO.getPrimaryKey(), userId)) {
					if ((!bUnfinishedOnly || taskVO.getCompleted() == null) && (iPriority.equals(0) || iPriority.equals(taskVO.getPriority()))){
						result.add(augmentDisplayNames(taskVO));
					}
				}
			}
		}
		catch (CommonPermissionException ex) {
			throw new NuclosBusinessException();
		}
		catch (CommonFinderException ex) {
			throw new NuclosBusinessException();
		}

		return result;
	}

   /**
    * get all delegated tasks (or only own tasks)
    * @param sDelegator task delegator to get tasks for
    * @return collection of task value objects
    */
   public Collection<TaskVO> getTasksByDelegator(String sDelegator, boolean bUnfinishedOnly, Integer iPriority) throws NuclosBusinessException {
	   	final Collection<TaskVO> result = new HashSet<TaskVO>();
		UID delegatorId = getUserUid(sDelegator);

		try {
			CollectableComparison cond = SearchConditionUtils.newKeyComparison(
				E.TODOLIST.taskdelegator, ComparisonOperator.EQUAL, delegatorId);

			for (TaskVO taskVO : getTaskVOsByCondition(cond)) {
				if ((!bUnfinishedOnly || taskVO.getCompleted() == null) && (iPriority.equals(0) || iPriority.equals(taskVO.getPriority()))){
					result.add(augmentDisplayNames(taskVO));
				}
			}
		}
		catch (CommonPermissionException ex) {
			throw new NuclosBusinessException();
		}
		catch (CommonFinderException ex) {
			throw new NuclosBusinessException();
		}
		return result;
	}

   /**
    * get all tasks for specified visibility and owners
    *
    * @param owners - task owners to get tasks for
    * @param visibility
    * @return collection of task value objects
    */
   public Collection<TaskVO> getTasksByVisibilityForOwners(List<String> owners, Integer visibility, boolean bUnfinishedOnly, Integer iPriority) throws NuclosBusinessException {
      final Collection<TaskVO> result = new HashSet<TaskVO>();

	  CompositeCollectableSearchCondition condDelegator = new CompositeCollectableSearchCondition(LogicalOperator.OR);
	  for(String owner : owners){
		  //condDelegator.addOperand(SearchConditionUtils.newMDComparison(MasterDataMetaCache.getInstance().getMetaData(E.TODOLIST.getEntityName()),"delegator",ComparisonOperator.EQUAL,owner));
		  condDelegator.addOperand(SearchConditionUtils.newComparison(E.TODOLIST.delegator, ComparisonOperator.EQUAL, owner));
	  }
	  //CollectableComparison condVisibility = SearchConditionUtils.newMDComparison(MasterDataMetaCache.getInstance().getMetaData(E.TODOLIST.getEntityName()),"visibility",ComparisonOperator.EQUAL,visibility);
	  CollectableComparison condVisibility = SearchConditionUtils.newComparison(E.TODOLIST.visibility,ComparisonOperator.EQUAL,visibility);
	  CompositeCollectableSearchCondition cond = new CompositeCollectableSearchCondition(LogicalOperator.AND);
	  cond.addOperand(condDelegator);
	  cond.addOperand(condVisibility);

  	  try {
		for (TaskVO taskVO : getTaskVOsByCondition(cond)) {
			if ((!bUnfinishedOnly || taskVO.getCompleted() == null) && (iPriority.equals(0) || iPriority.equals(taskVO.getPriority())))
				result.add(augmentDisplayNames(taskVO));
		}
	  }
	  catch (CommonPermissionException ex) {
		throw new NuclosBusinessException();
	  }
	  catch (CommonFinderException ex) {
		throw new NuclosBusinessException();
	  }
	  return result;
	}

   /**
    * get all tasks (or only unfinished tasks)
    * @param sUser task owner/delegator to get tasks for
    * @param bUnfinishedOnly get only unfinished tasks
    * @return collection of task value objects
    */
   public Collection<TaskVO> getTasks(String sUser, boolean bUnfinishedOnly, Integer iPriority) throws NuclosBusinessException {
      final Collection<TaskVO> colltaskvobyowner = this.getTasksByOwner(sUser, bUnfinishedOnly, iPriority);
      final Collection<TaskVO> colltaskvobydelegator = this.getTasksByDelegator(sUser, bUnfinishedOnly, iPriority);

      Set<TaskVO> result = CollectionUtils.union(colltaskvobyowner, colltaskvobydelegator);

      final Collection<TaskVO> colltaskvobyvisibility = getPublicTasks(sUser, bUnfinishedOnly, iPriority);
      result = CollectionUtils.union(result, colltaskvobyvisibility);

      return result;
   }

   private Collection<TaskVO> getPublicTasks(String sUser, boolean bUnfinishedOnly, Integer iPriority) throws NuclosBusinessException {
	  Collection<TaskVO> result = new HashSet<TaskVO>();
	  Collection<MasterDataVO<UID>> allowedUsersForRolesHierarchy = masterDataFacadeLocal.getUserHierarchy(sUser);
      List<String> allowedUsersForRolesHierarchyIds = CollectionUtils.transform(allowedUsersForRolesHierarchy, new Transformer<MasterDataVO<UID>,String>(){
		@Override
		public String transform(MasterDataVO<UID> mvo) {
			return mvo.getFieldValue(E.USER.name);
		}}
      );
      result = this.getTasksByVisibilityForOwners(allowedUsersForRolesHierarchyIds, TaskVO.TaskVisibility.PUBLIC.getValue(), bUnfinishedOnly, iPriority);
	  return result;
   }

   /**
    * create a new task in the database
    * @param taskvo containing the task data
    * @return same task as value object
    */
   public TaskVO create(TaskVO taskvo, Set<UID> stOwners) throws CommonValidationException, NuclosBusinessException, CommonPermissionException {
      TaskVO dbTaskVO = null;
		taskvo.validate();
		if(stOwners.size() == 0){
			UID userUid = SecurityCache.getInstance().getUserUid(getCurrentUserName());
			if(userUid != null){
				stOwners.add(userUid);
			}
		}
		if (taskvo.getDelegator() == null) {
			taskvo.setDelegator(SecurityCache.getInstance().getUserUid(getCurrentUserName()));
		}
		try {
			MasterDataVO<Long> mdvo = MasterDataWrapper.wrapTaskVO(taskvo);
			MasterDataVO<Long> newMdvo = masterDataFacadeLocal.create(mdvo, null);
			dbTaskVO = getCompleteTaskVO(newMdvo.getPrimaryKey());
			setOwnersForTask(dbTaskVO.getPrimaryKey(), stOwners);
		} catch (CommonCreateException ex) {
			throw new CommonFatalException(ex);
		} catch (CommonFinderException ex) {
			throw new CommonFatalException(ex);
		}
		return dbTaskVO;
	}

	/**
     * create a new task in the database
	 * @param taskvo containing the task data
	 * @param splitforowners true/false - shows if this task will be transformed to tasks for each owner
	 * @return new task ids
	 */
   	 @Override
	 public Collection<TaskVO> create(TaskVO taskvo, Set<UID> stOwners, boolean splitforowners) throws CommonValidationException, NuclosBusinessException, CommonPermissionException {
		List<TaskVO> listTaskVO = new ArrayList<TaskVO>();
		if(!splitforowners){
			listTaskVO.add(create(taskvo, stOwners));
		} else {
			for(UID currentOwnerId : stOwners){
				Set<UID> currentOwnerSet = new HashSet<UID>();
				currentOwnerSet.add(currentOwnerId);
				listTaskVO.add(create(taskvo, currentOwnerSet));
			}
		}
		return listTaskVO;
	 }

   /**
    * modify an existing task in the database
    * @param taskvo containing the task data
    * @return new task id
    */
   @Override
   public TaskVO modify(TaskVO taskvo, Set<UID> collOwners) 
   		throws CommonFinderException, CommonStaleVersionException, 
   		CommonValidationException, NuclosBusinessException {
		TaskVO newTaskVO = null;
		taskvo.validate();

		if (collOwners != null) {
			if ((collOwners.size() > 1) || (collOwners.size() == 1 && !getUserNamesById(collOwners).contains(getCurrentUserName()))) {
				taskvo.setDelegator(SecurityCache.getInstance().getUserUid(getCurrentUserName()));
			}
		}
		try {
			MasterDataVO<Long> mdvo = MasterDataWrapper.wrapTaskVO(taskvo);
			getMasterDataFacade().modify(mdvo, null);
			Long iTaskId = taskvo.getId();
			newTaskVO = getCompleteTaskVO(iTaskId);
			if (collOwners != null) {
				Set<UID> stNewOwners = new HashSet<UID>();
				for (UID iUserId : collOwners) {
					if (!this.getOwnerIdsByTask(iTaskId).contains(iUserId)) {
						stNewOwners.add(iUserId);
					}
				}
				setOwnersForTask(iTaskId, collOwners);
			}
		} catch (CommonPermissionException ex) {
			throw new NuclosBusinessException(ex);
		} catch(CommonCreateException ex) {
			throw new NuclosBusinessException(ex);
		} catch(CommonRemoveException ex) {
			throw new NuclosBusinessException(ex);
		}
		return newTaskVO;
	}

	private MasterDataFacadeLocal getMasterDataFacade() {
		return ServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
	}

	/**
	 * modify an existing task in the database
	 * @param taskvo containing the task data
	 * @param splitforowners true/false - shows if this task will be transformed to tasks for each owner
	 * @return new task ids
	 */
	 @Override
	 public Collection<TaskVO> modify(TaskVO taskvo, Set<UID> collOwners, boolean splitforowners) 
			 throws CommonFinderException, CommonStaleVersionException, 
			 CommonValidationException, NuclosBusinessException {
		 
		List<TaskVO> listTaskVO = new ArrayList<TaskVO>();
		if(!splitforowners){
			listTaskVO.add(modify(taskvo, collOwners));
		} else {
			UID firstOwnerId = null;
			for(UID currentOwnerId : collOwners){
				Set<UID> currentOwnerSet = new HashSet<UID>();
				currentOwnerSet.add(currentOwnerId);
				if(firstOwnerId != null){
					try {
						listTaskVO.add(create(taskvo.cloneTaskVO(), currentOwnerSet));
					}
					catch(CommonPermissionException e) {
						throw new NuclosBusinessException(e);
					}
				} else {
					listTaskVO.add(modify(taskvo, currentOwnerSet));
					firstOwnerId = currentOwnerId;
				}
			}
		}
		return listTaskVO;
	}

	/**
	 * delete task from database
	 * @param taskvo containing the task data
	 */
	public void remove(TaskVO taskvo) 
			throws CommonFinderException, CommonRemoveException, CommonStaleVersionException, 
			NuclosBusinessException {
		
		try {
			deleteOwnersForTask(taskvo.getId());
			getMasterDataFacade().remove(E.TODOLIST.getUID(), taskvo.getPrimaryKey(), true, null);
		} catch (CommonPermissionException ex) {
			throw new NuclosBusinessException(ex);
		}
	}

	/**
	 * add owners fot the given task
	 * @param taskId
	 * @param stUserUids
	 */
	private void setOwnersForTask(Long taskId, Set<UID> stUserUids) {
		if (stUserUids != null && stUserUids.isEmpty()) {
			stUserUids.add(getUserUid(this.getCurrentUserName()));
		}
		for (final UID userUid : stUserUids) {
			if (!this.existOwnerForTask(taskId, userUid)) {
				dataBaseHelper.execute(DbStatementUtils.insertIntoUnsafe(E.TASKOWNER,
					 E.TASKOWNER.getPk(), new DbId(),
					 E.TASKOWNER.user, userUid,
					 E.TASKOWNER.tasklist, DbNull.escapeNull(taskId, Long.class),
					 SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME,
					 SF.CHANGEDBY, getCurrentUserName(),
					 SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME,
					 SF.CHANGEDBY, getCurrentUserName(),
					 SF.VERSION, 1));
			}
		}
		for (final UID userUid : this.getOwnerIdsByTask(taskId)) {
			if (!stUserUids.contains(userUid)) {
				dataBaseHelper.execute(DbStatementUtils.deleteFrom(E.TASKOWNER, E.TASKOWNER.tasklist, taskId, E.TASKOWNER.user, userUid));
			}
		}
	}

	private void deleteOwnersForTask(Long taskId) {
		dataBaseHelper.execute(DbStatementUtils.deleteFrom(E.TASKOWNER, E.TASKOWNER.tasklist, taskId));
	}

	public List<String> getOwnerNamesByTask(TaskVO taskvo) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<Long> t = query.from(E.TASKOWNER);
		query.select(t.baseColumn(E.TASKOWNER.user));
		query.where(builder.equalValue(t.baseColumn(E.TASKOWNER.tasklist), taskvo.getId()));
		List<String> lstOwners = new ArrayList<String>();
		for (UID idUser : dataBaseHelper.getDbAccess().executeQuery(query)) {
			try {
				MasterDataVO<UID> userVO = getMasterDataFacade().get(E.USER, idUser);
				String sUser = userVO.getFieldValue(E.USER.name);
				lstOwners.add(sUser);
			} catch(Exception ex) {
				throw new CommonFatalException(ex);
			}
		}
		return lstOwners;
	}

	public Set<UID> getOwnerIdsByTask(final Long taskId) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<Long> t = query.from(E.TASKOWNER);
		query.select(t.baseColumn(E.TASKOWNER.user));
		query.where(builder.equalValue(t.baseColumn(E.TASKOWNER.tasklist), taskId));
		return new HashSet<UID>(dataBaseHelper.getDbAccess().executeQuery(query));
	}

	private boolean existOwnerForTask(final Long taskId, UID ownerUid) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<Long> query = builder.createQuery(Long.class);
		DbFrom<Long> t = query.from(E.TASKOWNER);
		query.select(builder.count(t.basePk()));
		query.where(builder.and(
			builder.equalValue(t.baseColumn(E.TASKOWNER.user), ownerUid),
			builder.equalValue(t.baseColumn(E.TASKOWNER.tasklist), taskId)));
		return dataBaseHelper.getDbAccess().executeQuerySingleResult(query) > 0L;
	}

	public UID getUserUid(String sUserName) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		DbQuery<UID> query = builder.createQuery(UID.class);
		DbFrom<UID> t = query.from(E.USER);
		query.select(t.basePk());
		query.where(builder.equal(builder.upper(t.baseColumn(E.USER.name)), builder.upper(builder.literal(sUserName))));
		try {
			return dataBaseHelper.getDbAccess().executeQuerySingleResult(query);
		} catch (DbInvalidResultSizeException ex) {
			LOG.error("Unable to get user UID of username {}", sUserName, ex);
			return null;
		}
	}
	
	@Override
	public List<String> getUserNamesById(Set<UID> userUids) {
		List<String> lstUserNames = new ArrayList<String>();
		for (final UID userUid : userUids) {
			DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
			DbQuery<String> query = builder.createQuery(String.class);
			DbFrom<UID> t = query.from(E.USER);
			query.select(t.baseColumn(E.USER.name));
			query.where(builder.equalValue(t.basePk(), userUid));
			lstUserNames.addAll(dataBaseHelper.getDbAccess().executeQuery(query));
		}
		return lstUserNames;
	}

	public MasterDataVO getUserAsVO(Object oId) throws CommonFinderException {
		throw new UnsupportedOperationException();
		/*
		return masterDataFacadeHelper.getMasterDataCVOById(
				MasterDataMetaCache.getInstance().getMetaData(E.USER), oId);
		*/
	}

	private Collection<TaskVO> getTaskVOsByCondition(CollectableSearchCondition cond) throws CommonFinderException, NuclosBusinessException, CommonPermissionException {
		return CollectionUtils.transform(getMasterDataFacade().getWithDependantsByCondition(E.TODOLIST, cond, null), mdToTaskTransformer);
	}

	private TaskVO getCompleteTaskVO(Long iId) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		MasterDataVO<Long> mdwdVO = getMasterDataFacade().getWithDependants(E.TODOLIST.getUID(), iId, null);
		return augmentDisplayNames(mdToTaskTransformer.transform(mdwdVO));
	}

	private TaskVO augmentDisplayNames(TaskVO taskVO) {
		DbQueryBuilder builder = dataBaseHelper.getDbAccess().getQueryBuilder();
		if (taskVO.getDelegator() != null) {
			DbQuery<DbTuple> queryOwner = builder.createTupleQuery();
			DbFrom<UID> userOwner = queryOwner.from(E.USER);
			//DbFrom owner = userOwner.join("T_UD_TASKOWNER", JoinType.INNER).alias("t2").on("INTID", "INTID_T_MD_USER", Integer.class);
			DbFrom<Long> owner = userOwner.join(E.TASKOWNER, JoinType.INNER, Long.class, "t2").on(E.USER.getPk(), E.TASKOWNER.user);
			queryOwner.where(builder.equalValue(owner.baseColumn(E.TASKOWNER.tasklist), taskVO.getId()));
			queryOwner.multiselect(userOwner.baseColumn(E.USER.firstname), userOwner.baseColumn(E.USER.lastname));
			taskVO.setAssignees(StringUtils.join("; ", dataBaseHelper.getDbAccess().executeQuery(queryOwner, new UserDisplayNameTransformer())));
		}
		return taskVO;
	}

	private static class UserDisplayNameTransformer implements Transformer<DbTuple, String> {
		@Override
		public String transform(DbTuple t) {
			String firstName = t.get(0, String.class);
			String lastName = t.get(1, String.class);
			return lastName + (firstName != null ? ", " + firstName : "");
		}
	}

	private Map<Long, String> getObjectIdentifier(MasterDataVO<Long> mdwdVO) {
		Map<Long, String> result = new HashMap<Long, String>();
		for (EntityObjectVO<Long> md : mdwdVO.getDependents().<Long>getDataPk(E.TODOOBJECT.tasklist)) {
			UID entity = md.getFieldUid(E.TODOOBJECT.entity);
			Long iObjectId = md.getFieldId(E.TODOOBJECT.objectId);
			
			EntityObjectVO<?> eObject = null;
			if (entity != null) {
				eObject = NucletDalProvider.getInstance().getEntityObjectProcessor(entity).getByPrimaryKey(iObjectId);
			}
			if (eObject != null) {
				// display treeview label for task objects. @see RSWORGA-14.
				String identifier = SpringLocaleDelegate.getInstance().getTreeViewLabel(eObject, metaProvider, null);
				if (StringUtils.isNullOrEmpty(identifier)) // fallback
					identifier = eObject.getFieldValue(SF.SYSTEMIDENTIFIER);
				if (identifier == null) {
					identifier = eObject.getDalEntity() + ": " + eObject.getId();
				}
				result.put(iObjectId, identifier);
			} else {
				result.put(iObjectId, null);
			}
		}
		return result;
	}

	@Override
	public Collection<TaskVO> create(MasterDataVO<Long> mdvo, Set<UID> stOwners,
		boolean splitforowners) throws CommonValidationException,
		NuclosBusinessException, CommonPermissionException {
		return create(MasterDataWrapper.getTaskVO(mdvo, null, null, null), stOwners, splitforowners);
	}

	@Override
	public Collection<TaskVO> modify(MasterDataVO<Long> mdvo,
		Set<UID> collOwners, boolean splitforowners)
		throws CommonFinderException, CommonStaleVersionException,
		CommonValidationException, NuclosBusinessException {
		return modify(MasterDataWrapper.getTaskVO(mdvo, null, null, null), collOwners, splitforowners);
	}
}
