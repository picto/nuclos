//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.nuclos.common.E;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Node cache containing meta information about entity nodes.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:ramin.goettlich@novabit.de">ramin.goettlich</a>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph Radig</a>
 * @version 00.01.000
 */
public class NodeCache {
	
	private static final Logger LOG = LoggerFactory.getLogger(NodeCache.class);
	
	private static NodeCache INSTANCE;
	
	// 
	
	private final Map<UID, Collection<MasterDataVO<Object>>> mpSubNodes 
		= new ConcurrentHashMap<UID, Collection<MasterDataVO<Object>>>();
	
	private final Map<UID, Collection<EntityTreeViewVO>> subNodes 
		= new ConcurrentHashMap<UID, Collection<EntityTreeViewVO>>();
	
//	private SpringDataBaseHelper dataBaseHelper;
//	
//	private MetaProvider metaProvider;

	NodeCache() {
		INSTANCE = this;
	}

	/**
	 * @return the one and only instance of the <code>MasterDataMetaCache</code>.
	 */
	public static NodeCache getInstance() {
		return INSTANCE;
	}
	
//	@Autowired
//	void setDataBaseHelper(SpringDataBaseHelper dataBaseHelper) {
//		this.dataBaseHelper = dataBaseHelper;
//	}
//	
//	@Autowired
//	void setMetaProvider(MetaProvider metaProvider) {
//		this.metaProvider = metaProvider;
//	}

	/**
	 * revalidates the cache. This may be used for development purposes only, in order to rebuild the cache
	 * after metadata entries in the database were changed.
	 */
	public synchronized void revalidate() {
		mpSubNodes.clear();
		subNodes.clear();
	}

	public Collection<EntityTreeViewVO> getSubnodesETV(UID sEntity, Object oId) {
		if(subNodes.get(sEntity) == null) {
			LOG.info("Initilizing SubnodeETV Metainformation for entity {}", sEntity);

			final MasterDataFacadeLocal mdLocal = ServerServiceLocator.getInstance().getFacade(MasterDataFacadeLocal.class);
			final Collection<EntityTreeViewVO> colSubNodes = mdLocal.getDependantSubnodes(E.ENTITYSUBNODES.getUID(), E.ENTITYSUBNODES.originentityid.getUID(), oId);
			subNodes.put(sEntity, colSubNodes);
		}
		return subNodes.get(sEntity);
	}

}	// class MasterDataMetaCache
