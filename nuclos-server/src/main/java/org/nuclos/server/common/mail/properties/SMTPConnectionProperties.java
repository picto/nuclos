package org.nuclos.server.common.mail.properties;

import java.util.Properties;

import org.nuclos.common.ParameterProvider;
import org.nuclos.server.common.ServerParameterProvider;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SMTPConnectionProperties extends MailConnectionProperties {
	/**  */
	private static final long serialVersionUID = 1L;

	public SMTPConnectionProperties() {
		super("smtp");
	}

	public boolean isUsingIMAP() {
		return true;
	}

	@Override
	public Properties toProperties() {
		Properties p = new Properties();

		boolean auth = "Y".equalsIgnoreCase(ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_AUTHENTICATION));
		String smtpHost = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_SERVER);
		String smtpPort = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_PORT);
		String login = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_USERNAME);
		String sender = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_SENDER);
		String password = ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_SMTP_PASSWORD);

		p.put("mail.imap.host", getHost());
		p.put("mail.imap.port", getPort());
		p.put("mail.imap.auth", "true");

		return p;
	}

	public void setUser(String user) {
		this.username = user;
	}
}
