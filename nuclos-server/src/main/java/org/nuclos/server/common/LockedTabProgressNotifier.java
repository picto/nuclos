//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common;

import java.io.Serializable;

import org.nuclos.common.JMSConstants;
import org.nuclos.common.LockedTabProgressNotification;
import org.nuclos.server.jms.NuclosJMSUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to notify clients of locked tab progress.
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public class LockedTabProgressNotifier {
	
	private static final Logger LOG = LoggerFactory.getLogger(LockedTabProgressNotifier.class);

	public static void notify(String message, Integer percent) {
		if (MessageReceiverContext.getInstance().getId() != null) {
			LockedTabProgressNotification notification = new LockedTabProgressNotification(message, percent);
			LOG.debug("JMS send LockedTabProgressNotification {}: {}",
			          notification, MessageReceiverContext.getInstance().getId());
			NuclosJMSUtils.sendObjectMessage(notification,
			                                 JMSConstants.TOPICNAME_LOCKEDTABPROGRESSNOTIFICATION,
			                                 MessageReceiverContext.getInstance().getId()+"");
		}
	}
	
	public static void notify(Serializable object) {
		if (MessageReceiverContext.getInstance().getId() != null) {
			LOG.debug("JMS send LockedTabProgressNotification {}: {}",
			          object, MessageReceiverContext.getInstance().getId());
			NuclosJMSUtils.sendObjectMessage(object,
			                                 JMSConstants.TOPICNAME_LOCKEDTABPROGRESSNOTIFICATION,
			                                 MessageReceiverContext.getInstance().getId()+"");
		}
	}

}
