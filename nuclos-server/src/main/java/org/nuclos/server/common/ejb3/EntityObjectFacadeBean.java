//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.security.RolesAllowed;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.UnlockMode;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.TrueCondition;
import org.nuclos.common.dal.DalSupportForMD;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.format.RefValueExtractor;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.RelativeDate;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonCreateException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonRemoveException;
import org.nuclos.common2.exception.CommonStaleVersionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.attribute.ejb3.LayoutFacadeLocal;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.LockUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.dal.processor.nuclet.IEntityObjectProcessor;
import org.nuclos.server.dal.processor.nuclet.JdbcEntityObjectProcessor;
import org.nuclos.server.dal.processor.proxy.impl.ProxyEntityObjectProcessor;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.entityobject.EntityObjectProxyList;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.genericobject.ProxyList;
import org.nuclos.server.genericobject.ProxyListProvider;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeRemote;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.genericobject.searchcondition.ResultParams;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.ruleengine.NuclosBusinessRuleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Server implementation of the EntityObjectFacadeRemote interface.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author Thomas Pasch
 * @since Nuclos 3.1.01
 */
@Transactional(noRollbackFor= {Exception.class})
@RolesAllowed("Login")
public class EntityObjectFacadeBean extends NuclosFacadeBean implements EntityObjectFacadeRemote {

	private static final Logger LOG = LoggerFactory.getLogger(EntityObjectFacadeBean.class);

	private LayoutFacadeLocal layoutFacade;

	@Autowired
	@Qualifier("masterDataService")
	private MasterDataFacadeRemote masterDataFacade;

	@Autowired
	private MasterDataFacadeLocal masterDataFacadeLocal;

	@Autowired
	private GenericObjectFacadeRemote genericObjectFacade;

	@Autowired
	private SecurityCache securityCache;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	@Autowired
	private RecordGrantUtils grantUtils;
	
	@Autowired
	private LockUtils lockUtils;
	
	@Autowired
	private DatasourceServerUtils datasourceServerUtils;
	
	@Autowired
	private SessionUtils sessionUtils;
	
	@Autowired
	private ServerParameterProvider serverParameter;
	
	@Autowired
	private MandatorUtils mandatorUtils;

	@Autowired
	private NuclosRemoteContextHolder remoteContext;
	
	public EntityObjectFacadeBean() {
	}
	
	private LayoutFacadeLocal getLayoutFacade() {
		if (layoutFacade == null)
			layoutFacade = (LayoutFacadeLocal) SpringApplicationContextHolder.getBean("layoutFacadeLocal");
		return layoutFacade;
	}

	private final MasterDataFacadeRemote getMasterDataFacade() {
		return masterDataFacade;
	}
	
	private void appendMandator(CollectableSearchExpression cse, EntityMeta<?> entity) {
		cse.setSearchCondition(mandatorUtils.append(cse, entity));
	}
	
	@Override
	public void cancelRunningStatements(UID entity) {
		nucletDalProvider.getEntityObjectProcessor(metaProvider.getEntity(entity)).cancelRunningStatements();
	}
	
	@Override
	public <PK> EntityObjectVO<PK> get(UID entity, PK id) throws CommonPermissionException {
		EntityMeta<PK> meta = (EntityMeta<PK>) metaProvider.getEntity(entity);
		if (meta.isStateModel()) {
			checkReadAllowedForModule(meta.getUID(), (Long) id);
		} else {
			checkReadAllowed(meta);
		}

		grantUtils.checkInternal(entity, id);
		IEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(entity);
		EntityObjectVO<PK> result = (EntityObjectVO<PK>)eop.getByPrimaryKey(id);
		if (result != null) {
			mandatorUtils.checkReadAllowed(result, meta);
		}
		return result;
	}

	@Override
	public <PK> EntityObjectVO<PK> getReferenced(UID referencingEntity, UID referencingEntityField, PK id) throws CommonBusinessException {
		FieldMeta<?> fieldmeta = metaProvider.getEntityField(referencingEntityField);
		if (fieldmeta.getForeignEntity() == null && fieldmeta.getLookupEntity() == null) {
			throw new NuclosFatalException("Field " + referencingEntity + "." + referencingEntityField + " is not a reference or lookup field.");
		} else {
			final IEntityObjectProcessor<PK> eop = nucletDalProvider.getEntityObjectProcessor(
					fieldmeta.getForeignEntity() != null ? fieldmeta.getForeignEntity() : fieldmeta.getLookupEntity());
			final boolean ignoreRecordGrantsAndOthers = eop.getIgnoreRecordGrantsAndOthers();
			try {
				eop.setIgnoreRecordGrantsAndOthers(true);
				return (EntityObjectVO<PK>)eop.getByPrimaryKey(id);
			} finally {
				eop.setIgnoreRecordGrantsAndOthers(ignoreRecordGrantsAndOthers);
			}
		}
	}
	
	public List<CollectableValueIdField> getReferenceList(FieldMeta<?> efMeta, String search, UID vlpUID, Map<String, Object> vlpParameter, Long iMaxRowCount, 
			UID mandator) throws CommonBusinessException {
		return super.getReferenceList(dataBaseHelper, datasourceServerUtils, sessionUtils, efMeta, 
				StringUtils.defaultIfNull(search, ""), vlpUID, vlpParameter, iMaxRowCount, mandator, false);
	}

	@Override
	public <PK> List<PK> getEntityObjectIds(UID entity, CollectableSearchExpression cse) throws CommonPermissionException {
		final List<PK> ids;
		final String user = getCurrentUserName();
		boolean allowed = true;
		if (isCalledRemotely()) {
			allowed = securityCache.isReadAllowedForMasterData(user, entity, getCurrentMandatorUID());
		}
		if (allowed) {				
			final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
			appendMandator(cse, eMeta);
			if (eMeta.isStateModel()) {
				ids = nucletDalProvider.<PK>getEntityObjectProcessor(entity).getIdsBySearchExprUserGroups(cse, entity, user);
			} else {
				ids = nucletDalProvider.<PK>getEntityObjectProcessor(entity).getIdsBySearchExpression(cse);					
			}				
		} else {
			throw new CommonPermissionException("User " + user + " has not access to " + entity.debugString());
		}
		return ids;
	}

	@Override
	public <PK> EntityObjectVO<PK> getByIdWithDependents(UID entity, PK id, Collection<UID> fields, String customUsage) {
		final EntityMeta<PK> eMeta = metaProvider.getEntity(entity);
		
		EntityObjectVO<PK> eo = nucletDalProvider.getEntityObjectProcessor(eMeta).getByPrimaryKey(id);
		
		if (eo != null && fields != null) {
			masterDataFacadeLocal.fillDependentsForSubformColumns(Collections.singletonList(eo), fields, entity, customUsage);
		}

		return eo;
	}
	
	//NOTE: istart == -1L is the correct way to mark that we don't have any offset at all.
	@Override
	public <PK> Collection<EntityObjectVO<PK>> getEntityObjectsChunk(UID entity, CollectableSearchExpression clctexpr, 
			ResultParams resultParams, String customUsage) {
		
		final EntityMeta<PK> eMeta = (EntityMeta<PK>) metaProvider.getEntity(entity);
		
		IEntityObjectProcessor ieop=nucletDalProvider.getEntityObjectProcessor(eMeta);
		
		if(ieop instanceof JdbcEntityObjectProcessor)
		{
			JdbcEntityObjectProcessor<PK> eop = (JdbcEntityObjectProcessor<PK>) ieop;
			appendMandator(clctexpr, eMeta);
			
			final List<EntityObjectVO<PK>> eos = eop.getChunkBySearchExpressionImpl(clctexpr, resultParams);
			
			if (resultParams.getFields() != null) {
				masterDataFacadeLocal.fillDependentsForSubformColumns(eos, resultParams.getFields(), entity, customUsage);
			}
			
			return eos;
		}else if(ieop instanceof ProxyEntityObjectProcessor)
		{			
			throw new NotImplementedException("ProxyEntityObjectProcessor unterstützt keine Chunks");
			
		}
		throw new NotImplementedException("EntityObjectProcessor wird nicht unterstützt");
		
	}
	
	@Override
	public Long countEntityObjectRows(UID entity, CollectableSearchExpression clctexpr) {
		final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
		IEntityObjectProcessor<?> eop = nucletDalProvider.getEntityObjectProcessor(eMeta);
		appendMandator(clctexpr, eMeta);
		return eop.count(clctexpr);
	}

	public List<CollectableField> getProcessByEntity(UID entityUid, boolean bSearchMode) {
		return getMasterDataFacade().getProcessByEntity(entityUid, bSearchMode);
	}

	/**
	 * @deprecated Use getEntityObjectsChunk(UID, Collection, String, CollectableSearchExpression, Long, Long, Boolean).
	 */
	@RolesAllowed("Login")
	@Override
	public <PK> ProxyList<PK,EntityObjectVO<PK>> getEntityObjectProxyList(UID entity,
			CollectableSearchExpression clctexpr,
			Collection<UID> fields, String customUsage) {
		
		final EntityMeta<?> eMeta = metaProvider.getEntity(entity);
		final ProxyListProvider plProvider = new ProxyListProvider(serverParameter, metaProvider);
		
		final CollectableSearchCondition search = getSearchCondition(clctexpr.getSearchCondition());
		clctexpr.setSearchCondition(search);

		// Do not add record grants here! (NUCLOS-3517)
		
		EntityObjectProxyList<PK> result = new EntityObjectProxyList<PK>(eMeta.getUID(), clctexpr, 
				fields, plProvider, customUsage);
		
		return result;
	}
	
	private CollectableSearchCondition getSearchCondition(CollectableSearchCondition constrain) {
		if (constrain == null) {
			return TrueCondition.TRUE;
		}
		
		return constrain;
	}

	@Override
	public <PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID subform, UID field, PK relatedId) {
		return getDependentEntityObjects(subform, field, relatedId, null);
	}

	public <PK> Collection<EntityObjectVO<PK>> getDependentEntityObjects(UID subform, UID field, PK relatedId, Long maxRowCount) {
		final CollectableSearchCondition cond;
		if (relatedId instanceof Long) {
			cond = SearchConditionUtils.newIdComparison(
					field, ComparisonOperator.EQUAL, (Long)relatedId);
		} else {
			cond = SearchConditionUtils.newUidComparison(
					field, ComparisonOperator.EQUAL, (UID)relatedId);
		}
		CollectableSearchExpression cse = new CollectableSearchExpression(cond);
		final EntityMeta<?> eMeta = metaProvider.getEntity(subform);
		appendMandator(cse, eMeta);
		return nucletDalProvider.<PK>getEntityObjectProcessor(subform).getBySearchExprResultParams(
				cse, new ResultParams(maxRowCount, false));
	}

	@Override
	public <PK> void delete(UID entity, PK pk, boolean logicalDeletion) throws CommonFinderException,
		CommonRemoveException, CommonStaleVersionException, NuclosBusinessException,
		CommonPermissionException, NuclosBusinessRuleException, CommonCreateException {

		final EntityMeta<?> mdEntity = metaProvider.getEntity(entity);
		if (mdEntity.isStateModel()) {
			final Long id = (Long) pk;
			genericObjectFacade.remove(entity, id, true, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		} else {
			masterDataFacade.remove(entity, pk, false, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
		}
	}
	
	//TODO: What does this function? Remove an entity or a data set from an entity? Anyway, the cache key looks wrong.
	@Override
	@CacheEvict(value="goMetaFields", key="#p1.intValue()")
	public void removeEntity(UID entity, Object id) throws CommonPermissionException {
		final EntityMeta<?> mdEntity = metaProvider.getEntity(entity);

        checkDeleteAllowed(mdEntity);

		final IEntityObjectProcessor<Object> processor = nucletDalProvider.getEntityObjectProcessor(entity);
		processor.delete(new Delete<Object>(id));
	}

	@Override
	public void remove(EntityObjectVO<?> entity) throws CommonPermissionException {
		removeEntity(entity.getDalEntity(), entity.getPrimaryKey());
	}

	@Override
	public <PK> EntityObjectVO<PK> insert(EntityObjectVO<PK> eoVO) throws CommonPermissionException, CommonCreateException,
		CommonPermissionException, NuclosBusinessRuleException {

		final UID uid = eoVO.getDalEntity();

		EntityObjectVO<PK> retVal;
		if (metaProvider.getEntity(uid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eoVO);
			go = genericObjectFacade.create(new GenericObjectWithDependantsVO(go, eoVO.getDependents() != null ?  eoVO.getDependents() : new DependentDataMap(), eoVO.getDataLanguageMap()),
					serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			retVal = (EntityObjectVO<PK>) DalSupportForGO.wrapGenericObjectVO(go);
		} else {
			MasterDataVO<PK> md = DalSupportForMD.wrapEntityObjectVO(eoVO);
			md = getMasterDataFacade().create(md, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			retVal = md.getEntityObject();
		}

		return retVal;

	}

	// NUCLOS-5995
	// TODO Instead of forking, implement stuff here and remove from GOFacade and MDFacade
	public <PK> EntityObjectVO<PK> executeBusinessRules(List<EventSupportSourceVO> lstRuleVO, EntityObjectVO<PK> eoVO, String customUsage, boolean isCollectiveProcessing)
		throws CommonBusinessException {
		if (metaProvider.getEntity(eoVO.getDalEntity()).isStateModel()) {
			GenericObjectWithDependantsVO go = DalSupportForGO.getGenericObjectWithDependantsVO((EntityObjectVO<Long>) eoVO);
			return (EntityObjectVO<PK>)genericObjectFacade.executeBusinessRules(lstRuleVO, go, customUsage, isCollectiveProcessing);
		}

		return getMasterDataFacade().executeBusinessRules(lstRuleVO, new MasterDataVO<PK>(eoVO), customUsage, isCollectiveProcessing);
	}

	@Override
	public <PK> EntityObjectVO<PK> update(EntityObjectVO<PK> eoVO) throws CommonCreateException, CommonFinderException, CommonRemoveException, 
		NuclosBusinessException, NoSuchElementException, CommonPermissionException, CommonStaleVersionException, CommonValidationException {

		EntityObjectVO<PK> retVal = eoVO;
		final UID uid = eoVO.getDalEntity();
		
		if (metaProvider.getEntity(uid).isStateModel()) {
			GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eoVO);
			GenericObjectWithDependantsVO modify = genericObjectFacade.modify(uid,
					new GenericObjectWithDependantsVO(go, eoVO.getDependents() != null ? eoVO.getDependents() : null, eoVO.getDataLanguageMap()), 
					serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			retVal = (EntityObjectVO<PK>) DalSupportForGO.wrapGenericObjectVO(go);
		}
		else {
			MasterDataVO<PK> md = DalSupportForMD.wrapEntityObjectVO(eoVO);
			PK pk = (PK) masterDataFacade.modify(md, serverParameter.getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
			md.setPrimaryKey(pk);
			retVal = md.getEntityObject();
		}
		
		return retVal;
	}
	
	@Override
	public <PK> void createOrUpdatePlain(EntityObjectVO<PK> entity) throws CommonPermissionException {
		final UID uid = entity.getDalEntity();
		final String user = getCurrentUserName();
		final PK id = entity.getPrimaryKey();
		final MetaProvider mdProv = metaProvider;
		final EntityMeta<PK> mdEntity = mdProv.getEntity(uid);
		
		checkPermissions(entity, mdEntity, user);
		grantUtils.checkWriteInternal(uid, entity.getPrimaryKey());
		lockUtils.checkLockedByCurrentUserInternal(uid, entity.getPrimaryKey());
		if (entity.isFlagNew()) {
			mandatorUtils.checkWriteAllowed(entity, mdEntity);
		} else {
			mandatorUtils.checkWriteAllowedFromDb(id, mdEntity);
		}
		setFlagsAndId(entity, mdEntity);
		setSystemFields(entity, user);

		final IEntityObjectProcessor<PK> processor = nucletDalProvider.getEntityObjectProcessor(uid);
		processor.insertOrUpdate(entity);
	}
	
	public <PK> void createOrUpdatePlainWithoutPermissionCheck(EntityObjectVO<PK> entity) throws CommonPermissionException {
		final UID uid = entity.getDalEntity();
		final String user = getCurrentUserName();
		final MetaProvider mdProv = metaProvider;
		final EntityMeta<PK> mdEntity = mdProv.getEntity(uid);
		
		// checkPermissions(entity, mdEntity, user);
		setFlagsAndId(entity, mdEntity);
		setSystemFields(entity, user);

		final IEntityObjectProcessor<PK> processor = nucletDalProvider.getEntityObjectProcessor(uid);
		processor.insertOrUpdate(entity);
	}
	
	private <PK> void setId(EntityObjectVO<PK> entity, EntityMeta<?> mdEntity) {
		final boolean isInsertWoId = entity.isbInsertWoId();
		final PK intid = entity.getPrimaryKey();
		final PK result;
		if (intid != null || isInsertWoId) {
			result = intid;
		} else {
			final String idFactory = mdEntity.getIdFactory();
			if (idFactory == null) {
				if (mdEntity.getPkClass().equals(UID.class)) {
					result = (PK) new UID();
				} else {
					result = (PK) dataBaseHelper.getNextIdAsLong(SpringDataBaseHelper.DEFAULT_SEQUENCE);
				}
			} else {
				result = (PK) dataBaseHelper.getDbAccess().executeFunction(idFactory, Long.class);
			}
		}
		if (!isInsertWoId) {
			entity.setPrimaryKey(result);
		}
	}
	
	private void setSystemFields(EntityObjectVO entity, String user) {
		final InternalTimestamp now = new InternalTimestamp(System.currentTimeMillis());
		if (entity.getCreatedBy() == null) {
			entity.setCreatedBy(user);
		}
		entity.setChangedBy(user);
		if (entity.getCreatedAt() == null) {
			entity.setCreatedAt(now);
		}
		entity.setChangedAt(now);
	}
	
	private <PK> void setFlagsAndId(EntityObjectVO<PK> entity, EntityMeta<?> mdEntity) throws CommonPermissionException {
		final PK intid = entity.getPrimaryKey();
		if (mdEntity.isStateModel()) {
			if (intid != null) {
				entity.flagUpdate();
			}
			else {
				entity.flagNew();
				setId(entity, mdEntity);
				entity.setVersion(1);
			}
		}
		else {
			if (intid != null) {
				entity.flagUpdate();
			}
			else {
				entity.flagNew();
				setId(entity, mdEntity);
				entity.setVersion(1);
			}
		}		
	}

	private void checkPermissions(EntityObjectVO entity, EntityMeta<?> mdEntity, String user) throws CommonPermissionException {
		final UID entityUID = entity.getDalEntity();
		final Object intid = entity.getPrimaryKey();
		final SecurityCache sc = SecurityCache.getInstance();
		securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
        checkWriteAllowed(mdEntity);
	}
	
	@Override
	public Integer getVersion(UID entity, Object id) throws CommonPermissionException {
		checkReadAllowed(entity);
		return nucletDalProvider.getEntityObjectProcessor(entity).getVersion(id);
	}
	
	@Cacheable(value="fieldGroupNames", key="#p0")
	public String getFieldGroupName(UID groupID) {
		EntityObjectVO<?> eo = nucletDalProvider.getEntityObjectProcessor(E.ENTITYFIELDGROUP.getUID()).getByPrimaryKey(groupID);
		if (eo == null) {
			return null;
		}
		return eo.getFieldValue(E.ENTITYFIELDGROUP.name);
	}
	
	@CacheEvict(value="fieldGroupNames", allEntries=true)
	public void evictGroupNamesCache() {
	}
	
	public <PK> UsageCriteria getUsageCriteriaForPK(PK pk, UID entityUID, String customUsage) throws CommonBusinessException {
		EntityMeta<?> eMeta = metaProvider.getEntity(entityUID);
		if (eMeta.isStateModel()) {
			return genericObjectFacade.getGOMeta((Long)pk, entityUID, customUsage);
		}
		
		return new UsageCriteria(entityUID, null, null, customUsage);
	}
	
	/*
	 * taken from 
	 * - org.nuclos.client.masterdata.ClientEnumeratedDefaultValueProvider
	 * - org.nuclos.client.common.Utils.setDefaultValues(Collectable<PK>, CollectableEntity)
	 */
	public <PK> void setDefaultValues(EntityObjectVO<PK> eo) {
		EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(eo.getDalEntity());
		for (FieldMeta<?> fMeta : eMeta.getFields()) {
			// fill the map with null values
			eo.setFieldValue(fMeta.getUID(), null);
			
			String sDefault = fMeta.getDefaultValue();
			Long defaultId = fMeta.getDefaultForeignId();
			UID defauUID = fMeta.getDefaultForeignUid();
			try {
				if (fMeta.getForeignEntity() != null || fMeta.getLookupEntity() != null) {
					if ((defaultId != null || defauUID != null)) {
						EntityObjectVO<?> referencedEO = getReferenced(null, fMeta.getUID(), RigidUtils.defaultIfNull(defaultId, defauUID));
						if (referencedEO != null) {
							if (defaultId != null) {
								eo.setFieldId(fMeta.getUID(), defaultId);
							}
							if (defauUID != null) {
								eo.setFieldUid(fMeta.getUID(), defauUID);
							}
							String stringifiedValue = RefValueExtractor.get(referencedEO, fMeta.getUID(), null, metaProvider);
							eo.setFieldValue(fMeta.getUID(), stringifiedValue);
						}
					}
				} else {
					if (StringUtils.looksEmpty(sDefault)) {
						//NUCLOS-5488
						if (fMeta.getJavaClass() == Boolean.class && !fMeta.isNullable()) {
							eo.setFieldValue(fMeta.getUID(), Boolean.FALSE);
						}
					} else {
						if (fMeta.getJavaClass() == Double.class) {
							eo.setFieldValue(fMeta.getUID(), Double.parseDouble(sDefault.replace(',', '.')));
						}
						else if (fMeta.getJavaClass() == Integer.class) {
							eo.setFieldValue(fMeta.getUID(), Integer.parseInt(sDefault));
						}
						else if (fMeta.getJavaClass() == Boolean.class) {						
							if("ja".equals(sDefault)) {
								eo.setFieldValue(fMeta.getUID(), Boolean.TRUE);
							} else {
								eo.setFieldValue(fMeta.getUID(), Boolean.FALSE);
							}
						}
						else if (fMeta.getJavaClass() == Date.class) {
							if (RelativeDate.today().toString().equals(sDefault)) {
								eo.setFieldValue(fMeta.getUID(), DateUtils.today());
							}
							else {
								// NUCLOS-1914
								final String format = fMeta.getFormatInput();
								final DateFormat formatter;
								if (format != null) {
									formatter = new SimpleDateFormat(format);
								} else {
									formatter = SpringLocaleDelegate.getInstance().getDateFormat();
								}
								eo.setFieldValue(fMeta.getUID(), formatter.parse(sDefault));
							}
						}
						else {
							eo.setFieldValue(fMeta.getUID(), sDefault);
						}
					}
				}
			} catch (Exception ex) {
				String s = "[sDefault=" + sDefault + ", defaultId=" + defaultId +
				           ", defauUID=" + defauUID==null?null:defauUID.getString() + "]";
				LOG.warn("Setting default {} for field {}.{} failed:",
				         s, eMeta.getEntityName(), fMeta.getFieldName(),
				         ex);
			}
		} 
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> UID lockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException {
		return this.lock(entityUID, pk);
	}
	
	public <PK> UID lock(UID entityUID, PK pk) throws CommonPermissionException {
		String userName = sessionUtils.getCurrentUserName();
		UID userUid = SecurityCache.getInstance().getUserUid(userName);
		lock(entityUID, pk, userUid);
		return userUid;
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> void lockInNewTransaction(UID entityUID, PK pk, UID userUID) throws CommonPermissionException {
		this.lock(entityUID, pk, userUID);
	}
	
	public <PK> void lock(UID entityUID, PK pk, UID userUID) throws CommonPermissionException {
		if (pk == null) {
			throw new NuclosFatalException("Primary key must not be null");
		}
		EntityMeta<Object> eMeta = metaProvider.getEntity(entityUID);
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not owner enabled");
		}
		boolean lock = false;
		if (SecurityCache.getInstance().isSuperUser(getCurrentUserName())) {
			lock = true;
		} else {
			lock = !sessionUtils.isCalledRemotely();
		}
		if (lock) {
			lockUtils.lock(entityUID, pk, userUID);
		} else {
			throw new CommonPermissionException("Locking is not allowed");
		}
	}
	
	public <PK> void unlockInNewTransactionAfterCommit(final UID entityUID, final PK pk) throws CommonPermissionException {
		this.unlock(entityUID, pk, true);
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
			@Override public void suspend() {}
			@Override public void resume() {}
			@Override public void flush() {}
			@Override public void beforeCompletion() {}
			@Override public void beforeCommit(boolean arg0) {}
			@Override public void afterCompletion(int arg0) {}
			@Override public void afterCommit() {
				try {
					unlockInNewTransaction(entityUID, pk);
				} catch (CommonPermissionException e) {
					// permission changed during call?
					LOG.error(e.getMessage(), e);
				}
			}
		});
	}
	
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public <PK> void unlockInNewTransaction(UID entityUID, PK pk) throws CommonPermissionException {
		this.unlock(entityUID, pk);
	}
	
	@Override
	public <PK> void unlock(UID entityUID, PK pk) throws CommonPermissionException {
		this.unlock(entityUID, pk, false);
	}
	
	public <PK> void unlock(UID entityUID, PK pk, boolean checkPermissionOnly) throws CommonPermissionException {
		if (pk == null) {
			throw new NuclosFatalException("Primary key must not be null");
		}
		EntityMeta<Object> eMeta = metaProvider.getEntity(entityUID);
		if (!eMeta.isOwner()) {
			throw new NuclosFatalException("Entity " + eMeta + " is not owner enabled");
		}
		boolean unlock = false;
		if (SecurityCache.getInstance().isSuperUser(getCurrentUserName())) {
			unlock = true;
		} else {
			if (sessionUtils.isCalledRemotely()) {
				if (eMeta.getUnlockMode() == UnlockMode.ALL_USERS_MANUALLY) {
					unlock = true;
				}
			} else {
				unlock = true;
			}
		}
		if (unlock) {
			if (!checkPermissionOnly) {
				lockUtils.unlock(entityUID, pk);
			}
		} else {
			throw new CommonPermissionException("Unlocking is not allowed");
		}
	}
	
}
