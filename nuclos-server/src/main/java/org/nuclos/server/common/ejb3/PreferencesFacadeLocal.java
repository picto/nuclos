package org.nuclos.server.common.ejb3;

import java.util.List;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceVO;
import org.nuclos.common.preferences.Preference;
import org.nuclos.common.preferences.PreferenceShareVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;

public interface PreferencesFacadeLocal extends CommonPreferencesFacade {

	WorkspaceVO getDefaultWorkspace(String user) throws CommonBusinessException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * List of preferences for the given user.
	 *
	 * @param app
	 * @param type
	 * @param entityUID
	 * @param layoutUID
	 * 			return layout dependent preferences.
	 * 			if null and returnDepEntities is true, all layouts are used.
	 * @param orLayoutIsNull
	 *  		return preferences without layout definition (for all layouts)
	 * @param userUID
	 * @param menuRelevant
	 * @param returnDepEntities
	 * 			return dependent entities instead of "entityUID" direct. Uses layouts for determination.
	 * @return a list of PreferenceVOs (not null)
	 */
	@RolesAllowed("Login")
	List<Preference> getPreferences(String app, String type, UID entityUID, UID layoutUID, boolean orLayoutIsNull, UID userUID, Boolean menuRelevant, Boolean returnDepEntities);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Un-share the preference for the given user role
	 *
	 * @param prefUID
	 * @param roleUID
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void unSharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Share the preference with the given user role
	 *
	 * @param prefUID
	 * @param roleUID
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void sharePreference(UID prefUID, UID roleUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a list of usergroups to share prefernces with
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	List<PreferenceShareVO> getPreferenceShares(UID prefUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Get a single preference of the current user.
	 *
	 * @param prefUID
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	Preference getPreference(UID prefUID) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Insert a single preference for the current user.
	 * App is optional. Would be 'nuclos' if not set
	 * Type is mandatory. In case of app-'nuclos' it must be registered in org.nuclos.common.NuclosPreferenceType
	 *
	 * @param wpref
	 * @return
	 * @throws CommonPermissionException
	 */
	@RolesAllowed("Login")
	Preference insertPreference(Preference.WritablePreference wpref) throws CommonPermissionException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update a single preference of the current user.
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void updatePreference(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Update the shared preference with the given customization. (User publish changes.)
	 *
	 * @param wpref
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void updatePreferenceShare(Preference.WritablePreference wpref) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete multiple preference items.
	 * Shared preferences will be unshared.
	 *
	 * @param prefUIDs
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	@RolesAllowed("Login")
	void deletePreference(UID ...prefUIDs) throws CommonPermissionException, CommonFinderException;

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Delete all preferences of the current user.
	 *
	 */
	@RolesAllowed("Login")
	void deleteMyPreferences();

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Selects the preference for the current user and layout.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	@RolesAllowed("Login")
	void selectPreference(UID prefUID, UID layoutUID);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * De-Select the preference for the current user.
	 *
	 * @param prefUID
	 * @param layoutUID
	 */
	void deselectPreference(UID prefUID, UID layoutUID);

	/**
	 * Belongs to the 'new' (web) preferences
	 *
	 * Resets all customized preferences of these types and entity for the current user.
	 *
	 * @param entity
	 * @param type
	 */
	void resetCustomizedPreferences(UID entity, String ...type);

}
