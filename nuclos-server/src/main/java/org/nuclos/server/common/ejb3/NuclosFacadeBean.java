//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.common.ejb3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.collect.collectable.searchcondition.RefJoinCondition;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.valueobject.DatasourceVO;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.DatasourceCache;
import org.nuclos.server.common.DatasourceServerUtils;
import org.nuclos.server.common.LockUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.NuclosRemoteContextHolder;
import org.nuclos.server.common.NuclosUserDetailsContextHolder;
import org.nuclos.server.common.RecordGrantUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.common.SessionUtils;
import org.nuclos.server.dal.processor.jdbc.TableAliasSingleton;
import org.nuclos.server.dal.processor.jdbc.impl.EOSearchExpressionUnparser;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.impl.SQLUtils2;
import org.nuclos.server.dblayer.query.DbCompoundColumnExpression;
import org.nuclos.server.dblayer.query.DbCondition;
import org.nuclos.server.dblayer.query.DbExpression;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbOrder;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbStructureChange;
import org.nuclos.server.dblayer.statements.DbStructureChange.Type;
import org.nuclos.server.dblayer.structure.DbConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbForeignKeyConstraint;
import org.nuclos.server.dblayer.structure.DbConstraint.DbUniqueConstraint;
import org.nuclos.server.dblayer.structure.DbIndex;
import org.nuclos.server.dblayer.structure.DbTable;
import org.nuclos.server.genericobject.Modules;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.ejb3.GenericObjectGroupFacadeLocal;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class NuclosFacadeBean {
	
	protected static final Logger LOG = LoggerFactory.getLogger(NuclosFacadeBean.class);

	// Spring injected

	@Autowired
	protected SecurityCache securityCache;
	
	@Autowired
	protected MetaProvider metaProvider;
	
	@Autowired
	protected NucletDalProvider nucletDalProvider;

	@Autowired
	protected RecordGrantUtils grantUtils;
	
	@Autowired
	protected LockUtils lockUtils;
	
	@Autowired
	private NuclosUserDetailsContextHolder userCtx;
	
	@Autowired
	private NuclosRemoteContextHolder remoteCtx;
	
	@Autowired
	private Modules modules;
	
	@Autowired
	private DataLanguageCache dlCache;

	// end Spring injected
	
	public NuclosFacadeBean() {
	}
	
	/**
	 * @return the name of the current user. Shortcut for <code>this.getSessionContext().getCallerPrincipal().getName()</code>.
	 */
	public final String getCurrentUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
	}

	public boolean isCalledRemotely() {
		Boolean peek = remoteCtx.peek();
		return peek != null ? remoteCtx.peek() : Boolean.FALSE;
	}

	public Object getSavePoint() {
		return userCtx.getSavePoint();
	}

	public InternalTimestamp getSavePointTime() {
		return userCtx.getSavePointTime();
	}
	
	public UID getCurrentMandatorUID() {
		return userCtx.getMandatorUID();
	}
	
	public void setCurrentMandatorUID(UID mandatorUID) {
		userCtx.setMandatorUID(mandatorUID);
	}

	/**
	 * checks if it is allowed to read a genericobject
	 * @param iModuleId
	 * @param iGenericObjectId
	 * @throws CommonPermissionException if reading of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkReadAllowedForModule(UID iModuleId, Long iGenericObjectId) throws CommonPermissionException {
		if (isCalledRemotely()) {
			if (iGenericObjectId == null) {
				throw new NullArgumentException("iGenericObjectId");
			}

			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			if (!securityCache.isReadAllowedForModule(user, iModuleId, iGenericObjectId, userCtx.getMandatorUID())) {
				throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.1", 
					user, getSystemIdentifier(iGenericObjectId), modules.getLabel(iModuleId)));
			}
		}
	}

	/**
	 * checks if it is allowed to write a genericobject
	 * @param iModuleId
	 * @param iGenericObjectId
	 * @throws CommonPermissionException if writing of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkWriteAllowedForModule(UID iModuleId, Long iGenericObjectId) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			if (iGenericObjectId == null) {
				throw new NullArgumentException("iGenericObjectId");
			}

			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			if (!securityCache.isWriteAllowedForModule(user, iModuleId, iGenericObjectId, userCtx.getMandatorUID())) {
				throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.2",
					user, getSystemIdentifier(iGenericObjectId), modules.getLabel(iModuleId)));
			}
		}
	}

	/**
	 * checks if it is allowed to write in an objectgroup
	 * @param iModuleId
	 * @param iObjectGroupId
	 * @throws CommonPermissionException if writing to the entity and the objectgroup is not allowed for the current user.
	 */
	protected void checkWriteAllowedForObjectGroup(UID iModuleId, UID iObjectGroupId) throws CommonPermissionException {
		if (isCalledRemotely()) {
			final String user = getCurrentUserName();
			if (!securityCache.isWriteAllowedForObjectGroup(user, iModuleId, iObjectGroupId, userCtx.getMandatorUID())) {
				if (iObjectGroupId == null) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.3",
						user, modules.getLabel(iModuleId)));
				}
				else {
					final GenericObjectGroupFacadeLocal genericObjectGroupFacade = SpringApplicationContextHolder.getBean(GenericObjectGroupFacadeLocal.class);
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.4",
						user, genericObjectGroupFacade.getObjectGroupName(iObjectGroupId)));
				}
			}
		}
	}

	/**
	 * checks if it is allowed to delete a genericobject
	 * @param iModuleId
	 * @param iGenericObjectId
	 * @param bDeletePhysically
	 * @throws CommonPermissionException if deleting of the entity and the special genericobject is not allowed for the current user.
	 */
	protected void checkDeleteAllowedForModule(UID iModuleId, Long iGenericObjectId, boolean bDeletePhysically) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			if (iGenericObjectId == null) {
				throw new NullArgumentException("iGenericObjectId");
			}
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			String sMessage;
			if (!securityCache.isDeleteAllowedForModule(user, iModuleId, iGenericObjectId, bDeletePhysically, userCtx.getMandatorUID())) {
				if (!bDeletePhysically) {
					sMessage = StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.5",
						user, getSystemIdentifier(iGenericObjectId), modules.getLabel(iModuleId));
				}
				else {
					sMessage = StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.6",
						user, getSystemIdentifier(iGenericObjectId), modules.getLabel(iModuleId));
				}
				throw new CommonPermissionException(sMessage);
			}
		}
	}

	/**
	 * checks if the current user is allowed to read at least one of the given entities (masterdata or genericobject)
	 * 
	 * @param entities
	 * @throws CommonPermissionException if reading of the entity (masterdata or genericobject) is not allowed for the current user.
	 */
	protected void checkReadAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkReadAllowed(toUidArray(entities));
	}
	
	/**
	 * checks if the current user is allowed to read at least one of the given entities (masterdata or genericobject)
	 * 
	 * @param entities
	 * @throws CommonPermissionException if reading of the entity (masterdata or genericobject) is not allowed for the current user.
	 */
	protected void checkReadAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			boolean isReadAllowed = false;
			for (int i = 0; i < entities.length; i++) {
				
				final UID entity = entities[i];
				String sEntityLabel;
				if (modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if(securityCache.isReadAllowedForModule(user, entity, null, userCtx.getMandatorUID())) {
					  isReadAllowed = true;
					  break;
					}
				}
				else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if(securityCache.isReadAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
					  isReadAllowed = true;
					  break;
					}
				}
				if (!isReadAllowed) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage(
							"nucleus.facade.permission.exception.7", user, sEntityLabel));
				}
			}

		}
	}

	protected void checkWriteAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkWriteAllowed(toUidArray(entities));
	}
	
	protected void checkWriteAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			boolean isWriteAllowed = false;
			for (int i = 0; i < entities.length; i++) {
				final UID entity = entities[i];
				String sEntityLabel;
				if (modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if(securityCache.isWriteAllowedForModule(user, entity, null, userCtx.getMandatorUID())) {
					  isWriteAllowed = true;
					  break;
					}
				}
				else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if(securityCache.isWriteAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
					  isWriteAllowed = true;
					  break;
					}
				}
				if (!isWriteAllowed) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage(
							"nucleus.facade.permission.exception.8", user, sEntityLabel));
				}
			}
		}
	}
	
	protected void checkDeleteAllowed(EntityMeta<?>...entities) throws CommonPermissionException {
		checkDeleteAllowed(toUidArray(entities));
	}

	protected void checkDeleteAllowed(UID...entities) throws CommonPermissionException {
		if (this.isCalledRemotely()) {
			final String user = getCurrentUserName();
			securityCache.checkMandatorLoginPermission(user, getCurrentMandatorUID());
			boolean isDeleteAllowed = false;
			for (int i = 0; i < entities.length; i++) {
				final UID entity = entities[i];
				String sEntityLabel;
				if(modules.isModule(entity)) {
					sEntityLabel = modules.getLabel(entity);
					if (securityCache.isDeleteAllowedForModule(user, entity, null, true, userCtx.getMandatorUID())) {
					  isDeleteAllowed = true;
					  break;
					}
				}
				else {
					sEntityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(metaProvider.getEntity(entity));
					if (securityCache.isDeleteAllowedForMasterData(user, entity, userCtx.getMandatorUID())) {
					  isDeleteAllowed = true;
					  break;
					}
				}
				if (!isDeleteAllowed) {
					throw new CommonPermissionException(StringUtils.getParameterizedExceptionMessage("nucleus.facade.permission.exception.9", user, sEntityLabel));
				}
			}
		}
	}
	
	private static UID[] toUidArray(EntityMeta<?>...entities) {
		return CollectionUtils.transformArray(entities, UID.class, new Transformer<EntityMeta<?>, UID>(){
			@Override
			public UID transform(EntityMeta<?> e) {
				return e.getUID();
			}
		});
	}

	protected boolean isInRole(String sRoleName) {
		for (GrantedAuthority ga : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (ga.getAuthority().equals(sRoleName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @deprecated
	 */
	private String getSystemIdentifier(Long iGenericObjectId) throws CommonPermissionException {
		try {
			return SpringApplicationContextHolder.getBean(GenericObjectFacadeLocal.class).get(iGenericObjectId).getSystemIdentifier();
		}
		catch (CommonFinderException e) {
			throw new CommonFatalException(e);
		}
	}
		
	protected List<DbConstraint> getConstraints(DbAccess dbAccess) {
		MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), dbAccess, metaProvider);
		
		List<DbConstraint> result = new ArrayList<DbConstraint>();
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			DbTable dbtable = helper.getDbTable(eMeta);
			if (dbtable != null) {
				result.addAll(dbtable.getTableArtifacts(DbForeignKeyConstraint.class));
				result.addAll(dbtable.getTableArtifacts(DbUniqueConstraint.class));
			}
		}
		return result;
	}
	
	protected void createOrDropConstraints(List<DbConstraint> lstConstraints, boolean bCreate, DbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} constraints.", action, lstConstraints.size());
		
		int iFailed = 0;
		for (DbConstraint c : lstConstraints) {
			String name = c.getConstraintName();
			String details = c.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, c));
					if (result != null) {
						LOG.info("{} successfully dropped.", name );
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
				
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, c));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
				
			}
		}
		LOG.info("Finished {} of {} constraints. Failed: {}",
		         action, lstConstraints.size(), iFailed);
	}
	
	protected void createOrDropConstraints(List<DbConstraint> lstConstraints, boolean bCreate, PersistentDbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} constraints.", action, lstConstraints.size());
		
		int iFailed = 0;
		for (DbConstraint c : lstConstraints) {
			String name = c.getConstraintName();
			String details = c.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, c));
					if (result != null) {
						LOG.info("{} successfully dropped.", name);
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
				
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, c));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
				
			}
		}
		LOG.info("Finished {} of {} constraints. Failed: {}", action, lstConstraints.size(), iFailed);
	}
	
	protected List<DbIndex> getIndexes(DbAccess dbAccess) {
		MetaDbHelper helper = new MetaDbHelper(E.getSchemaHelperVersion(), dbAccess, metaProvider);
		
		List<DbIndex> result = new ArrayList<DbIndex>();
		for (EntityMeta<?> eMeta : metaProvider.getAllEntities()) {
			DbTable dbtable = helper.getDbTable(eMeta);
			if (dbtable != null) {
				result.addAll(dbtable.getTableArtifacts(DbIndex.class));
			}
		}
		return result;
	}
	
	protected void createOrDropIndexes(List<DbIndex> indexes, boolean bCreate, PersistentDbAccess dbAccess, List<String> result) {
		String action = bCreate ? "CREATE" : "DROP";
		LOG.info("Starting {} of {} indexes.", action, indexes.size());
		
		int iFailed = 0;
		for (DbIndex index : indexes) {
			String name = index.getIndexName();
			String details = index.toString();
			if (!bCreate) {
				try {
					dbAccess.execute(new DbStructureChange(Type.DROP, index));
					if (result != null) {
						LOG.info("{} successfully dropped.", name);
						result.add(name + " successfully dropped.");						
					} else {
						LOG.debug("{} successfully dropped.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not dropped! {}", name, details, ex);
						result.add(name + " not dropped! " + details + " (" + ex.getMessage() + ")");						
					} else {
						//Failed droppings from import/exportDB  are logged without complete Stack-Trace
						LOG.warn("{} not dropped! {}", name, details);
					}
					iFailed++;
				}
			} else {
				try {
					dbAccess.execute(new DbStructureChange(Type.CREATE, index));
					if (result != null) {
						LOG.info("{} successfully created.", name);
						result.add(name + " successfully created.");						
					} else {
						LOG.debug("{} successfully created.", name);
					}
				} catch (Exception ex) {
					if (result != null) {
						LOG.error("{} not created! {}", name, details, ex);
						result.add(name + " not created! " + details + " (" + ex.getMessage() + ")");						
					} else {
						LOG.warn("{} not created! {}", name, details, ex);
					}
					iFailed++;
				}			
			}
		}
		LOG.info("Finished {} of {} indexes. Failed:{}",
		         action, indexes.size(), iFailed);
	}
	
	protected List<CollectableValueIdField> getReferenceList(SpringDataBaseHelper dataBaseHelper, DatasourceServerUtils datasourceServerUtils, 
			SessionUtils sessionUtils, FieldMeta<?> efMeta, String search, UID vlpUID, Map<String, Object> vlpParameter, Long iMaxRowCount, 
			UID mandator, boolean bComesFromQuickSearch) 
			throws CommonBusinessException {
		List<CollectableValueIdField> result = new ArrayList<CollectableValueIdField>();
		
		//NUCLOS-4687 the root cause is fixed. This is a fallback.
		if (efMeta.getEntity() == null) {
			efMeta = metaProvider.getEntityField(efMeta.getUID());
		}
		
		final EntityMeta<?> eForeignMeta = metaProvider.getEntity(efMeta.getForeignEntity() != null ? efMeta.getForeignEntity() : efMeta.getLookupEntity());
		final TableAliasSingleton tas = TableAliasSingleton.getInstance();
		final String alias = tas.getAlias(efMeta);
		final DbAccess access = dataBaseHelper.getDbAccess();
		final DbQueryBuilder builder = access.getQueryBuilder();
		final DbQuery<DbTuple> query = builder.createTupleQuery();
		
		final DbFrom<?> from = query.from(eForeignMeta, alias);
		final DbExpression<?> id = from.baseColumn(eForeignMeta.getPk());
		
		//NOAINT-634: "bComesFromQuickSearch == false" is the correct 4th parameter here when coming from , 
		//as this stringifiedRef is just used for the displaying text
		//NUCLOS-4684: "bComesFromQuickSearch == true" is correct from 
		final DbExpression<String> stringifiedRef = new DbCompoundColumnExpression<String>(from, efMeta, false, bComesFromQuickSearch);
		final DbOrder order = builder.asc(stringifiedRef);
		
		query.multiselect(id, stringifiedRef);

		final FieldMeta<?> efDeleted = SF.LOGICALDELETED.getMetaData(eForeignMeta.getUID());
		final EOSearchExpressionUnparser unparser = new EOSearchExpressionUnparser(query, eForeignMeta, sessionUtils, dlCache);
		if (efDeleted != null && metaProvider.getEntity(eForeignMeta.getUID()).getFields().contains(efDeleted)) {
			final CollectableSearchCondition condSearchDeleted = SearchConditionUtils.newComparison(efDeleted, ComparisonOperator.EQUAL, Boolean.FALSE);
			unparser.unparseSearchCondition(condSearchDeleted);
			
		}
		
		query.orderBy(order);
		
		if (iMaxRowCount != null) {
			query.limit(iMaxRowCount);	
		} else {
			query.limit(10000L);
		}

		if (search != null) {
			final String wildcard = access.getWildcardLikeSearchChar();
			search = search.replace("*", wildcard);
			search = search.replace("?", "_");
			search = wildcard + SQLUtils2.escape(StringUtils.toUpperCase(search)) + wildcard;
			
			final DatasourceVO dsvo = vlpUID != null ? DatasourceCache.getInstance().getValuelistProvider(vlpUID, true) : null;
			if (dsvo != null && dsvo.getValid()) {
				query.addToWhereAsAnd(builder.in(id, 
						datasourceServerUtils.getSqlLikeWithIdForInClause(dsvo, vlpParameter, search, mandator, userCtx.getDataLocal())));
				
			} else {
				final DbCondition condLike = builder.like(builder.upper(stringifiedRef), search);
				query.addToWhereAsAnd(condLike);
			}
			
			for (RefJoinCondition rjc : TableAliasSingleton.getInstance().getRefJoinCondition(efMeta)) {
				unparser.addRefJoinCondition(rjc);
			}
		}
		
		if (eForeignMeta.isMandator()) {
			final Set<UID> mandators;
			if (mandator == null) {
				mandators = securityCache.getAccessibleMandators(getCurrentMandatorUID());
			} else {
				mandators = MandatorUtils.getAccessibleMandatorsByLogin(mandator, getCurrentMandatorUID());
			}
			query.addToWhereAsAnd(builder.in(from.baseColumn(SF.MANDATOR_UID), mandators));
		}
		
		for (DbTuple tuple : access.executeQuery(query)) {
			result.add(new CollectableValueIdField(
				tuple.get(0, Object.class),
				tuple.get(1, efMeta.getJavaClass())));
			
		}
		
		return result;
	}
}
