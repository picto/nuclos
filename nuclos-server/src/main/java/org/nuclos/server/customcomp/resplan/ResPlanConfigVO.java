//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.customcomp.resplan;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.AbstractResPlanConfigVO;
import org.nuclos.common.customcomp.resplan.PlanElement;

//Version
@SuppressWarnings("serial")
@XmlType
@XmlRootElement(name="resplan")
public class ResPlanConfigVO<PK,R,C extends Collectable<PK>> extends AbstractResPlanConfigVO<PK,R,C,PlanElement<R>> {

	public ResPlanConfigVO() {
	}
	
	public PlanElement<R> newPlanElementInstance() {
		return new PlanElement<R>();
	}
		
}

