package org.nuclos.server.test;

import org.nuclos.common.SpringApplicationContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class IntegrationTestHelper {

	@Transactional(propagation=Propagation.REQUIRES_NEW, rollbackFor= {Exception.class})
	public void runInNewTransaction(Runnable runnable) {
		runnable.run();
	}
	
	public static IntegrationTestHelper getInstance() {
		return SpringApplicationContextHolder.getBean(IntegrationTestHelper.class);
	}
	
}
