package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.Collection;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.helper.OutputFormatRecompileCheckerHelper;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a report (including output formats) must cause a recompile.
 */
@Component
public class ReportRecompileChecker extends CompositeRecompileChecker {

    @Autowired
    private MasterDataFacadeHelper masterDataFacadeHelper;

    private final OutputFormatRecompileCheckerHelper outputFormatRecompileCheckerHelper;

    public ReportRecompileChecker() {
        super(
            new PKRecompileChecker(),
            new FieldRecompileChecker<>(E.REPORT.name),
            new UIDFieldRecompileChecker(E.REPORT.nuclet),
			new FieldRecompileChecker<>(E.REPORT.withRuleClass)
        );
        outputFormatRecompileCheckerHelper = new OutputFormatRecompileCheckerHelper(
            E.REPORTOUTPUT, E.REPORTOUTPUT.description, E.REPORTOUTPUT.parent);
    }


    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        String username =
            SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        Collection<EntityObjectVO<UID>> reportOutputEOs =
            masterDataFacadeHelper.getDependantMasterData(E.REPORTOUTPUT.getUID(), E.REPORTOUTPUT.parent.getUID(),
                                                         username, null,  (UID)mdvo.getPrimaryKey());
        mdvo.getDependents().addAllData(E.REPORTOUTPUT.parent, reportOutputEOs);
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldReport,
        MasterDataVO<?> updatedReport) {
		if (!oldReport.getFieldValue(E.REPORT.withRuleClass) && !updatedReport.getFieldValue(E.REPORT.withRuleClass)) {
			return false;
		}
        if (super.isRecompileRequiredOnUpdate(oldReport, updatedReport)) {
            return true;
        }
        if (outputFormatRecompileCheckerHelper.isRecompileRequired(oldReport, updatedReport)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(MasterDataVO<?> report) {
    	if (report.getFieldValue(E.REPORT.withRuleClass)) {
			// new/removed reports must trigger recompile
    		return true;
		} else {
    		return false;
		}
    }

}
