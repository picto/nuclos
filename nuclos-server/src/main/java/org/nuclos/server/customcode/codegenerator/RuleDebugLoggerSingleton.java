package org.nuclos.server.customcode.codegenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Singleton used by classes enhanced by {@link ClassDebugAdapter} to log debug output.
 * <p>
 * In Nuclos this is used for (server) Rules that have the debug flag enabled.
 * </p>
 * @author Thomas Pasch
 * @since Nuclos 3.0.11
 */
public final class RuleDebugLoggerSingleton {
	
	private static final RuleDebugLoggerSingleton INSTANCE = new RuleDebugLoggerSingleton();
	
	private Logger LOG = LoggerFactory.getLogger(RuleDebugLoggerSingleton.class);
	
	private RuleDebugLoggerSingleton() {
	}
	
	public static RuleDebugLoggerSingleton getInstance() {
		return INSTANCE;
	}
	
	public void log(String prefix, String var, Object o) {
		if (!LOG.isInfoEnabled()) return;
		final StringBuilder sb = new StringBuilder();
		try {
			sb.append(prefix).append(var).append(" => ").append(o);
			LOG.info("{}", sb);
		}
		catch (Exception e) {
			LOG.debug("Failed to report rule step in debug mode", e);
		}
	}
	
}
