package org.nuclos.server.customcode.codegenerator;

import java.util.Date;

import org.nuclos.server.eventsupport.ejb3.EventSupportFacadeLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.google.common.collect.EvictingQueue;

/**
 * Single point of code generation.<p/> Any code generation must be triggered via this
 * singleton.</p>
 *
 * @see EventSupportFacadeLocal#createBusinessObjects(boolean)
 */
@Component
public class NuclosJarGeneratorManager {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosJarGeneratorManager.class);

	private static final String THREAD_DESCRIPTION = "Nuclos jar generator thread";

	@Autowired
	@Lazy
	private EventSupportFacadeLocal eventSupportFacadeLocal;

	private final EvictingQueue<NuclosJarGeneratorThread> threadQueue = EvictingQueue.create(1);

	private NuclosJarGeneratorManagerThread managerThread;

	private Date lastTriggered = new Date();

	/**
	 * Cancels the current generator (if running) and starts a new generator thread.
	 */
	public synchronized void triggerCancelingOfCurrentGeneratorAndStartNewGenerator() {
		lastTriggered = new Date();

		if (managerThread == null || !managerThread.isAlive()) {
			managerThread = new NuclosJarGeneratorManagerThread(threadQueue);
			managerThread.start();
		}
		NuclosJarGeneratorThread nextNuclosJarGeneratorThread = threadQueue.poll();
		if (nextNuclosJarGeneratorThread != null) {
			LOG.debug("Disposed enqueued (unstarted) {}.", THREAD_DESCRIPTION);
		} else {
			LOG.debug("{} queue is empty.", THREAD_DESCRIPTION);
		}
		threadQueue.add(new NuclosJarGeneratorThread(eventSupportFacadeLocal));
		LOG.debug("Added {} to queue.", THREAD_DESCRIPTION);
	}


	private static class NuclosJarGeneratorManagerThread extends Thread {

		private final EvictingQueue<NuclosJarGeneratorThread> threadQueue;

		private NuclosJarGeneratorThread currentNuclosJarGeneratorThread;

		public NuclosJarGeneratorManagerThread(
			EvictingQueue<NuclosJarGeneratorThread> threadQueue) {
			this.threadQueue = threadQueue;
		}

		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(1_000L);
				} catch (InterruptedException e) {
					return;
				}
				NuclosJarGeneratorThread nextNuclosJarGeneratorThread;
				synchronized (threadQueue) {
					nextNuclosJarGeneratorThread = threadQueue.poll();
				}
				if (nextNuclosJarGeneratorThread != null) {
					LOG.debug("Picking a new {} from queue.", THREAD_DESCRIPTION);
					try {
						if (currentNuclosJarGeneratorThread != null
						    && currentNuclosJarGeneratorThread.isAlive()) {
							shutdownCurrentNuclosJarGeneratorThread();
						}
						currentNuclosJarGeneratorThread = nextNuclosJarGeneratorThread;
						currentNuclosJarGeneratorThread.start();
						LOG.debug("Started previously picked {}.", THREAD_DESCRIPTION);
					} catch (InterruptedException e) {
						return;
					}
				}
				if (isInterrupted()) {
					return;
				}

			}
		}

		private void shutdownCurrentNuclosJarGeneratorThread() throws InterruptedException {
			if (currentNuclosJarGeneratorThread.isAlive()) {
				LOG.info("Interrupting {} ...", THREAD_DESCRIPTION);
				currentNuclosJarGeneratorThread.interrupt();
				long waitTime = 1_000L;
				while (currentNuclosJarGeneratorThread.isAlive()) {
					LOG.debug("{} is still alive. "
					          + "Waiting {} ms for it to die ...", THREAD_DESCRIPTION, waitTime);
					Thread.sleep(waitTime);
				}
				LOG.debug("{} is not alive anymore.", THREAD_DESCRIPTION);
			}
		}
	}

	public Date getLastTriggered() {
		return lastTriggered == null ? null : new Date(lastTriggered.getTime());
	}
}
