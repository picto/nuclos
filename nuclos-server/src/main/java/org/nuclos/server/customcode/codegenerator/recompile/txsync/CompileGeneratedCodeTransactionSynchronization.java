package org.nuclos.server.customcode.codegenerator.recompile.txsync;

import java.util.concurrent.atomic.AtomicInteger;

import org.nuclos.server.customcode.codegenerator.NuclosJarGeneratorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronization;

/**
 * Compiles the generated code after the transaction completes.
 */
public class CompileGeneratedCodeTransactionSynchronization implements TransactionSynchronization {

	private static final Logger LOG =
		LoggerFactory.getLogger(CompileGeneratedCodeTransactionSynchronization.class);

	private final NuclosJarGeneratorManager nuclosJarGeneratorManager;

	/**
	 * Counts how many times this synchronizer was called.
	 */
	private AtomicInteger callCount = new AtomicInteger(0);

	public CompileGeneratedCodeTransactionSynchronization(
		NuclosJarGeneratorManager nuclosJarGeneratorManager) {
		this.nuclosJarGeneratorManager = nuclosJarGeneratorManager;
	}

	public void increaseCallCount() {
		callCount.incrementAndGet();
	}

	private void clearCallCount() {
		callCount.set(0);
	}

	@Override
	public void suspend() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void flush() {
	}

	@Override
	public void beforeCommit(boolean readOnly) {
	}

	@Override
	public void beforeCompletion() {
	}

	@Override
	public void afterCommit() {
		LOG.debug("tx afterCommit ...");

		if (callCount.get() <= 0) {
			LOG.debug("Not triggering recompile of generated code after tx commit "
			          + "because call count is <= 0.");
			return;
		}

		// Imagine the compiling process either as singleton:
		//  => cancel current compile job and restart it or:
		nuclosJarGeneratorManager.triggerCancelingOfCurrentGeneratorAndStartNewGenerator();
	}

	@Override
	public void afterCompletion(int status) {
		clearCallCount();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[callCount=" + callCount.get() + "]";
	}
}