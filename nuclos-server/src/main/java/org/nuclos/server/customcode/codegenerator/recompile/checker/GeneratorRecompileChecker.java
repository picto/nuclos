package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.UIDFieldRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if a modification to a generator must cause a recompile.
 */
@Component
public class GeneratorRecompileChecker extends CompositeRecompileChecker {

    public GeneratorRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.GENERATION.name),
              new UIDFieldRecompileChecker(E.GENERATION.nuclet));
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(MasterDataVO<?> oldGenerator, MasterDataVO<?> updatedGenerator) {

        if (super.isRecompileRequiredOnUpdate(oldGenerator, updatedGenerator)) {
            return true;
        }

        // source module
        if (checkUnequality(oldGenerator, updatedGenerator, E.GENERATION.sourceModule)) {
        	return true;
        }
        
        // target module
        if (checkUnequality(oldGenerator, updatedGenerator, E.GENERATION.targetModule)) {
        	return true;
        }
        
        return false;
    }
    
    private static boolean checkUnequality(MasterDataVO<?> oldGenerator, MasterDataVO<?> updatedGenerator, FieldMeta<UID> fm) {
        UID oldModule = oldGenerator.getFieldUid(fm);
        UID newModule = updatedGenerator.getFieldUid(fm);
        if (oldModule != null && newModule != null) {
            EntityMeta<?> oldEntity = MetaProvider.getInstance().getEntity(oldModule);
            EntityMeta<?> newEntity = MetaProvider.getInstance().getEntity(newModule);
            if (oldEntity != null && newEntity != null && !oldEntity.equals(newEntity)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        return true;
    }

}
