package org.nuclos.server.customcode.codegenerator.recompile.checker;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeHelper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Checks if the nuclet require a recompile.
 */
@Component
public class NucletRecompileChecker extends CompositeRecompileChecker {

    @Autowired
    private MasterDataFacadeHelper masterDataFacadeHelper;

    public NucletRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.NUCLET.name),
              new FieldRecompileChecker<>(E.NUCLET.packagefield));
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        super.loadRequiredDependentsOf(mdvo);

        // load nuclet params:
        String username =
            SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        Collection<EntityObjectVO<UID>> nucletParamEOs =
            masterDataFacadeHelper.getDependantMasterData(E.NUCLETPARAMETER.getUID(), E.NUCLETPARAMETER.nuclet.getUID(),
                                                          username, null, (UID)mdvo.getPrimaryKey());
        mdvo.getDependents().addAllData(E.NUCLETPARAMETER.nuclet, nucletParamEOs);
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldNuclet,
        MasterDataVO<?> updatedNuclet) {

        if (super.isRecompileRequiredOnUpdate(oldNuclet, updatedNuclet)) {
            return true;
        }

        // check package:



        // check nuclet params: name + PK
        Map<UID, String> oldNucletParamNamesByUID = new HashMap<>();
        Map<UID, String> newNucletParamNamesByUID = new HashMap<>();

        Collection<EntityObjectVO<?>> newNucletParmas =
            updatedNuclet.getDependents().getData(E.NUCLETPARAMETER.nuclet);
        Collection<EntityObjectVO<?>> oldNucletParams =
            oldNuclet.getDependents().getData(E.NUCLETPARAMETER.nuclet);
        for (EntityObjectVO<?> oldNucletParam : oldNucletParams) {
            UID nucletParamPK = (UID) oldNucletParam.getPrimaryKey();
            String outputName = oldNucletParam.getFieldValue(E.NUCLETPARAMETER.name);
            oldNucletParamNamesByUID.put(nucletParamPK, outputName);
        }
        for (EntityObjectVO<?> newNucletParam : newNucletParmas) {
            UID outputReportPK = (UID) newNucletParam.getPrimaryKey();
            String outputName = newNucletParam.getFieldValue(E.NUCLETPARAMETER.name);
            newNucletParamNamesByUID.put(outputReportPK, outputName);
        }

        if (oldNucletParamNamesByUID.size() != newNucletParamNamesByUID.size()) {
            return true;
        } else {
            for (Map.Entry<UID, String> entry : newNucletParamNamesByUID.entrySet()) {
                UID newKey = entry.getKey();
                String newOutputName = entry.getValue();
                String oldOutputName = oldNucletParamNamesByUID.get(newKey);
                if (oldOutputName == null) {
                    return true;
                }
                if (!newOutputName.equals(oldOutputName)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // new or deleted nuclet force a recompile
        return true;
    }

}
