package org.nuclos.server.customcode.codegenerator.recompile.checker;

import org.nuclos.common.E;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.CompositeRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.FieldRecompileChecker;
import org.nuclos.server.customcode.codegenerator.recompile.checker.composite.PKRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.stereotype.Component;

/**
 * Checks if the system parameters require a recompile.
 */
@Component
public class SystemParameterRecompileChecker extends CompositeRecompileChecker {


    public SystemParameterRecompileChecker() {
        super(new PKRecompileChecker(),
              new FieldRecompileChecker<>(E.PARAMETER.name));
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        // when a new system parameter is created or deleted we must recompile it:
        return true;
    }

}
