package org.nuclos.server.customcode.codegenerator.recompile.checker.bo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.NucletEntityMeta;
import org.nuclos.common.UID;

/**
 * Checks if the diff in the 2 given BO meta data must cause a recompile. If no old BO meta is
 * provided then a recompile is forced.
 */
public class BORecompileChecker {

	public static boolean isRecompileRequired(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		if (oldBo == null || updatedBo == null) {
			// A new BO is created => always recompile
			return true;
		}
		return isRecompileRequiredOnUpdate(oldBo, updatedBo);
	}

	private static boolean isRecompileRequiredOnUpdate(NucletEntityMeta oldBo,
	                                                   NucletEntityMeta updatedBo) {
		if (packageDiffers(oldBo, updatedBo)
		    || nameDiffers(oldBo, updatedBo)
		    || uidDiffers(oldBo, updatedBo)
		    || proxyDiffers(oldBo, updatedBo)
			|| genericDiffers(oldBo, updatedBo)
		    || mandatorDiffers(oldBo, updatedBo)
		    || stateModelDiffers(oldBo, updatedBo)
		    || editableDiffers(oldBo, updatedBo)
		    || virtualDiffers(oldBo, updatedBo)
		    || fieldsDiffer(oldBo, updatedBo)
		    || ownerDiffers(oldBo, updatedBo)
		    || thinDiffers(oldBo, updatedBo)
			) {
			return true;
		}
		return false;
		// processes are checked in ProcessRecompileChecker
	}

	private static boolean packageDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		if (oldBo.getNuclet() == updatedBo.getNuclet()) {
			return false;
		}
		return !oldBo.getNuclet().equals(updatedBo.getNuclet());

	}

	private static boolean nameDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return !oldBo.getEntityName().equals(updatedBo.getEntityName());
	}

	private static boolean uidDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return !oldBo.getUID().equals(updatedBo.getUID());
	}

	private static boolean proxyDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isProxy() != updatedBo.isProxy();
	}

	private static boolean genericDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isGeneric() != updatedBo.isGeneric();
	}

	private static boolean mandatorDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isMandator() != updatedBo.isMandator();
	}
	
	private static boolean ownerDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isOwner() != updatedBo.isOwner();
	}

	private static boolean stateModelDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isStateModel() != updatedBo.isStateModel();
	}

	private static boolean editableDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isEditable() != updatedBo.isEditable();
	}

	private static boolean virtualDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isVirtual() != updatedBo.isVirtual();
	}
	
	private static boolean thinDiffers(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		return oldBo.isThin() != updatedBo.isThin();
	}

	private static boolean fieldsDiffer(NucletEntityMeta oldBo, NucletEntityMeta updatedBo) {
		if (oldBo.getFields().size()
		    != updatedBo.getFields().size()) {
			return true;
		}
		Map<UID, FieldMeta<?>> oldFields = mapByUid(oldBo.getFields());
		Map<UID, FieldMeta<?>> updatedFields = mapByUid(updatedBo.getFields());
		for (Map.Entry<UID, FieldMeta<?>> entry : oldFields.entrySet()) {
			UID oldUID = entry.getKey();
			if (!updatedFields.containsKey(oldUID)) {
				return true;
			}
			FieldMeta<?> oldFieldMeta = entry.getValue();
			FieldMeta<?> updatedFieldMeta = updatedFields.get(oldUID);
			if (fieldDiffer(oldFieldMeta, updatedFieldMeta)) {
				return true;
			}
		}
		return false;
	}

	private static boolean fieldDiffer(FieldMeta<?> oldFieldMeta, FieldMeta<?> updatedFieldMeta) {
		if (!oldFieldMeta.getFieldName().equals(updatedFieldMeta.getFieldName())) {
			return true;
		}
		if (!oldFieldMeta.getUID().equals(updatedFieldMeta.getUID())) {
			return true;
		}
		String oldFieldDataType = oldFieldMeta.getDataType();
		if (!oldFieldDataType.equals(updatedFieldMeta.getDataType())) {
			return true;
		}
		// Double/BigDecimal precision: (old and new data type must be equal here)
		if (
			oldFieldDataType.equals(Double.class.getCanonicalName())
			|| oldFieldDataType.equals(BigDecimal.class.getCanonicalName())) {

			int oldScale = oldFieldMeta.getPrecision() != null ? oldFieldMeta.getPrecision() : 0;
			int updatedScale = updatedFieldMeta.getPrecision() != null
			                   ? updatedFieldMeta.getPrecision() : 0;
			if (oldScale != updatedScale) {
				return true;
			}
		}
		if (oldFieldMeta.isLocalized() != updatedFieldMeta.isLocalized()) {
			return true;
		}
		return false;
	}

	private static Map<UID, FieldMeta<?>> mapByUid(Collection<FieldMeta<?>> fields) {
		Map<UID, FieldMeta<?>> mappedFields = new HashMap<>(fields.size());
		for (FieldMeta<?> field : fields) {
			mappedFields.put(field.getUID(), field);
		}
		return mappedFields;
	}

}
