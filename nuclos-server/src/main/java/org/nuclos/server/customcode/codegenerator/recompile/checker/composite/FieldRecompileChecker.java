package org.nuclos.server.customcode.codegenerator.recompile.checker.composite;

import org.nuclos.common.FieldMeta;
import org.nuclos.server.customcode.codegenerator.recompile.checker.IRecompileChecker;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * Checks if the field value was modified (by using {@code equals}).
 */
public class FieldRecompileChecker<T> implements IRecompileChecker {

    private FieldMeta.Valueable<T> fieldMeta;

    public FieldRecompileChecker() {
    }

    public FieldRecompileChecker(FieldMeta.Valueable<T> fieldMeta) {
        this.fieldMeta = fieldMeta;
    }

    private boolean isFieldValueEqual(
        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        T oldValue = oldEntity.getFieldValue(fieldMeta);
        T newValue = updatedEntity.getFieldValue(fieldMeta);

        if (oldValue == null && newValue == null) {
            return true;
        } else if (oldValue == null) {
            return false;
        }
        return oldValue.equals(newValue);
    }

    @Override
    public void loadRequiredDependentsOf(MasterDataVO<?> mdvo) {
        // nop
    }

    @Override
    public boolean isRecompileRequiredOnUpdate(
        MasterDataVO<?> oldEntity,
        MasterDataVO<?> updatedEntity) {

        return !isFieldValueEqual(oldEntity, updatedEntity);
    }

    @Override
    public boolean isRecompileRequiredOnInsertOrDelete(final MasterDataVO<?> mdvo) {
        return false;
    }

    @Override
    public String toString() {
        return "FieldRecompileChecker[" + fieldMeta.getFieldName() + "]";
    }
}
