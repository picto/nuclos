//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.communication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.nuclos.api.annotation.Communicator;
import org.nuclos.api.communication.CommunicationInterface;
import org.nuclos.api.communication.CommunicationInterface.Registration;
import org.nuclos.api.communication.CommunicationPort;
import org.nuclos.api.communication.CommunicationPortFactory;
import org.nuclos.api.communication.CommunicationPortFactory.InstanceContext;
import org.nuclos.api.context.communication.RequestContext;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.common.E;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.SpringApplicationContextHolder.SpringReadyListener;
import org.nuclos.common.UID;
import org.nuclos.common.dblayer.JoinType;
import org.nuclos.server.database.SpringDataBaseHelper;
import org.nuclos.server.dblayer.DbAccess;
import org.nuclos.server.dblayer.DbTuple;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbJoin;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopInfrastructureBean;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@Component
public class CommunicationInterfaceBean
	implements BeanPostProcessor, ApplicationListener<ContextClosedEvent>,
	           CommunicationInterfaceLocal {
    
	private static final Logger LOG = LoggerFactory.getLogger(CommunicationInterfaceBean.class);
	
	private static final String EDITING_USER = "CommunicationInterfaceBean";
	
	private final ConcurrentMap<InterfaceStorage, CommunicationPortFactory<CommunicationPort>> activeInterfaces = 
			new ConcurrentHashMap<InterfaceStorage, CommunicationPortFactory<CommunicationPort>>();
	
	private boolean initiated = false;
	
	@Autowired
	private SpringDataBaseHelper dataBaseHelper;
	
	private static class InterfaceStorage {
		Communicator com;
		Class<? extends CommunicationInterface> comClass;
		UID comInterfaceUID;
		boolean isMultiInstanceable;
		Set<Class<? extends RequestContext<?>>> supportedRequestContexts;
		final ConcurrentMap<UID, CommunicationPort> activePorts = new ConcurrentHashMap<UID, CommunicationPort>();
		
		@Override
		public boolean equals(Object that) {
			if (this == that) {
				return true;
			}
			if (that instanceof InterfaceStorage) {
				return RigidUtils.equal(com.domainName(), ((InterfaceStorage) that).com.domainName()) &&
					   RigidUtils.equal(com.interfaceName(), ((InterfaceStorage) that).com.interfaceName());
			}
			return false;
		}
		
		@Override
		public String toString() {
			return "Communication interface " + comClass + "[manufacturer=" + com.manufacturer() + ", domainName=" + com.domainName() + ", interfaceName=" + com.interfaceName() + ", interfaceVersion=" + com.interfaceVersion() + "]";
		}
	}
	
	private static class RegistrationImpl implements Registration {

		boolean isMultiInstanceable;
		String description;
		String helpUrl;
		Map<String, String> parameters = new HashMap<String, String>();
		Set<Class<? extends RequestContext<?>>> supportedRequestContexts = new HashSet<Class<? extends RequestContext<?>>>();
		
		@Override
		public void setMultiInstanceable(boolean isMultiInstanceable) {
			this.isMultiInstanceable = isMultiInstanceable;
		}

		@Override
		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public void setHelpUrl(String helpUrl) {
			this.helpUrl = helpUrl;
		}

		@Override
		public void addSystemParameter(String name, String description) {
			parameters.put(name, description);
		}

		@Override
		public <C extends RequestContext<?>> void addSupportedRequestContext(Class<C> requestContextClass) {
			supportedRequestContexts.add(requestContextClass);
		}
		
	}
	
	private static class InstanceContextImpl implements InstanceContext {

		private DbTuple port;
		private Map<String, String> paramValues;
		
		public InstanceContextImpl(DbTuple port, Map<String, String> paramValues) {
			this.port = port;
			this.paramValues = paramValues;
		}

		@Override
		public org.nuclos.api.UID getPortId() {
			return port.get(SF.PK_UID);
		}

		@Override
		public Map<String, String> getSystemParameterValues() {
			return paramValues;
		}
		
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		synchronized (activeInterfaces) {
			if (!initiated) {
				DbMap initPorts = new DbMap();
				initPorts.put(E.COMMUNICATION_PORT.active, false);
				initPorts.putNull(E.COMMUNICATION_PORT.startupError);
				dataBaseHelper.getDbAccess().execute(
						new DbUpdateStatement<UID>(E.COMMUNICATION_PORT, initPorts, new DbMap()));
				initiated = true;
			}
		}
		if (bean instanceof AopInfrastructureBean) {
			// Ignore AOP infrastructure such as scoped proxies.
			return bean;
		}
		Class<?> targetClass = AopUtils.getTargetClass(bean);
		if (targetClass.isAnnotationPresent(Communicator.class) && bean instanceof CommunicationInterface) {
			final InterfaceStorage iStore = new InterfaceStorage();
			final CommunicationInterface comInterface = (CommunicationInterface) bean;
			iStore.comClass = comInterface.getClass();
			iStore.com = targetClass.getAnnotation(Communicator.class);
			
			LOG.info("{}", iStore);
			
			try {
				RegistrationImpl reg = new RegistrationImpl();
				comInterface.register(reg);
				
				for (InterfaceStorage otherStore : activeInterfaces.keySet()) {
					if (otherStore.equals(iStore)) {
						// found interface with other version
						LOG.error("Communication interface is not unique. Remove the old version!");
						shutdownPorts(otherStore);
						activeInterfaces.remove(otherStore);
						return bean;
					}
				}
				
				iStore.comInterfaceUID = syncDb(reg, iStore.com);
				iStore.isMultiInstanceable = reg.isMultiInstanceable;
				iStore.supportedRequestContexts = reg.supportedRequestContexts;
				
				@SuppressWarnings("unchecked")
				CommunicationPortFactory<CommunicationPort> portFactory = iStore.com.connectionFactory().newInstance();
				
				activeInterfaces.put(iStore, portFactory);
				startupPorts(iStore);
				
			} catch (Exception ex) {
				LOG.error("Error during registration and startup of communication interface {}",
				          iStore, ex);
			}
		}
		return bean;
	}
	
	@Override
	public Set<Class<? extends RequestContext<?>>> getSupportedRequestContextClasses(UID port) {
		Set<Class<? extends RequestContext<?>>> result = null;
	
		for (InterfaceStorage iStore : activeInterfaces.keySet()) {
			if (iStore.activePorts.containsKey(port)) {
				result = iStore.supportedRequestContexts;
			}
		}
		if (result == null) {
			result = new HashSet<Class<? extends RequestContext<?>>>(); 
		}
		return Collections.unmodifiableSet(result);
	}
	
	@Override
	public CommunicationPort getPort(UID port) throws BusinessException {
		for (InterfaceStorage iStore : activeInterfaces.keySet()) {
			if (iStore.activePorts.containsKey(port)) {
				return iStore.activePorts.get(port);
			}
		}
		throw new BusinessException("Communication port is not active!");
	}
	
	private void shutdownPorts(InterfaceStorage iStore) {
		for (DbTuple port : getAllPorts(iStore.comInterfaceUID)) {
			try {
				shutdownPort(iStore, port);
			} catch (Exception ex) {
				LOG.error("Error during shutdown of port {}",
				          port.get(E.COMMUNICATION_PORT.portName), ex);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.communication.CommunicationInterfaceLocal#shutdownPort(org.nuclos.common.UID)
	 */
	@Override
	public boolean shutdownPort(UID portUID) throws BusinessException {
		DbTuple port = getTuplePort(portUID);
		UID comInterfaceUID = port.get(E.COMMUNICATION_PORT.communicationInterface);
		for (InterfaceStorage iStore : activeInterfaces.keySet()) {
			if (RigidUtils.equal(iStore.comInterfaceUID, comInterfaceUID) &&
				iStore.activePorts.containsKey(portUID)) {
				shutdownPort(iStore, port);
				return true;
			}
		}
		return false;
	}
	
	private void shutdownPort(InterfaceStorage iStore, DbTuple port) throws BusinessException {
		UID portUID = port.get(SF.PK_UID);
		CommunicationPort communicationPort = iStore.activePorts.get(portUID);
		if (communicationPort != null) {
			activeInterfaces.get(iStore).shutdown(communicationPort);
		}
		iStore.activePorts.remove(portUID);
	}
	
	private void startupPorts(InterfaceStorage iStore) {
		for (DbTuple port : getAllPorts(iStore.comInterfaceUID)) {
			startupPort(iStore, port);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.nuclos.server.communication.CommunicationInterfaceLocal#startupPort(org.nuclos.common.UID)
	 */
	@Override
	public void startupPort(UID portUID) {
		DbTuple port = getTuplePort(portUID);
		UID comInterfaceUID = port.get(E.COMMUNICATION_PORT.communicationInterface);
		for (InterfaceStorage iStore : activeInterfaces.keySet()) {
			if (RigidUtils.equal(iStore.comInterfaceUID, comInterfaceUID)) {
				startupPort(iStore, port);
				return;
			}
		}
		updatePortState(portUID, "Interface is not active / Class not found", false);
	}
	
	private void startupPort(final InterfaceStorage iStore, final DbTuple port) {
		SpringApplicationContextHolder.addSpringReadyListener(new SpringReadyListener() {
			@Override
			public int getMinReadyState() {
				return 3;
			}
			@Override
			public void springIsReady() {
				try {
					if (!iStore.activePorts.isEmpty() && !iStore.isMultiInstanceable) {
						throw new BusinessException("Communication interface is not multi instanceable: " + iStore);
					}
					CommunicationPortFactory<CommunicationPort> portFactory = activeInterfaces.get(iStore);
					InstanceContext context = new InstanceContextImpl(port, getParameterValues(port.get(SF.PK_UID)));
					CommunicationPort communicationPort = portFactory.newInstance(context);
					if (!RigidUtils.equal(communicationPort.getId(), context.getPortId())) {
						portFactory.shutdown(communicationPort);
						throw new BusinessException("Port is not using the generated UID " + context.getPortId());
					}
					iStore.activePorts.put(port.get(SF.PK_UID), communicationPort);
					updatePortState(port.get(SF.PK_UID), null, true);
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
					updatePortState(port.get(SF.PK_UID), ex.getMessage(), false);
				}
			}
		});
	}
	
	private void updatePortState(UID portUID, String startupError, boolean online) {
		DbMap update = new DbMap();
		update.put(E.COMMUNICATION_PORT.active, online);
		if (startupError == null) {
			update.putNull(E.COMMUNICATION_PORT.startupError);
		} else {
			update.put(E.COMMUNICATION_PORT.startupError, startupError.substring(0, Math.min(startupError.length(), 3800)));
		}
		DbMap cond = new DbMap();
		cond.put(SF.PK_UID, portUID);
		dataBaseHelper.getDbAccess().execute(
				new DbUpdateStatement<UID>(E.COMMUNICATION_PORT, update, cond));
	}
	
	private Map<String, String> getParameterValues(UID portUID) {
		Map<String, String> result = new HashMap<String, String>();
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.COMMUNICATION_INTERFACE_PARAMETER);
		DbJoin<UID> t2 = t.joinOnBasePk(
				E.COMMUNICATION_PORT_PARAMETER, 
				JoinType.INNER, 
				E.COMMUNICATION_PORT_PARAMETER.communicationInterfaceParameter, "t2");
		query.multiselect(t.baseColumn(E.COMMUNICATION_INTERFACE_PARAMETER.name),
						 t2.baseColumn(E.COMMUNICATION_PORT_PARAMETER.value));
		query.where(
				builder.equalValue(t2.baseColumn(E.COMMUNICATION_PORT_PARAMETER.communicationPort), portUID));
		for (DbTuple portParam : dbAccess.executeQuery(query)) {
			String name = portParam.get(E.COMMUNICATION_INTERFACE_PARAMETER.name);
			String value = portParam.get(E.COMMUNICATION_PORT_PARAMETER.value); 
			result.put(name, value);
		}
		return result;
	}
	
	private DbTuple getTuplePort(UID portUID) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.COMMUNICATION_PORT);
		query.multiselect(t.baseColumns(E.COMMUNICATION_PORT));
		query.where(
				builder.equalValue(t.baseColumn(E.COMMUNICATION_PORT.getPk()), portUID));
		
		return dbAccess.executeQuerySingleResult(query);
	}
	
	private List<DbTuple> getAllPorts(UID comInterfaceUID) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.COMMUNICATION_PORT);
		query.multiselect(t.baseColumns(E.COMMUNICATION_PORT));
		query.where(
				builder.equalValue(t.baseColumn(E.COMMUNICATION_PORT.communicationInterface), comInterfaceUID));
		
		return dbAccess.executeQuery(query);
	}
	
	private UID syncDb(RegistrationImpl reg, Communicator com) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.COMMUNICATION_INTERFACE);
		query.multiselect(t.baseColumns(E.COMMUNICATION_INTERFACE));
		query.where(
				builder.equalValue(t.baseColumn(E.COMMUNICATION_INTERFACE.domainName), com.domainName()));
		query.addToWhereAsAnd(
				builder.equalValue(t.baseColumn(E.COMMUNICATION_INTERFACE.interfaceName), com.interfaceName()));
		
		DbMap interfaceDbMap = getInterfaceDbMap(reg, com);
		UID comInterfaceUID;
		
		List<DbTuple> existingInterface = dbAccess.executeQuery(query);
		if (existingInterface.isEmpty()) {
			// create one
			comInterfaceUID = new UID();
			interfaceDbMap.put(SF.PK_UID, comInterfaceUID);
			interfaceDbMap.put(SF.VERSION, 1);
			interfaceDbMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			interfaceDbMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
			interfaceDbMap.put(SF.CREATEDBY, EDITING_USER);
			interfaceDbMap.put(SF.CHANGEDBY, EDITING_USER);
			dbAccess.execute(new DbInsertStatement<UID>(E.COMMUNICATION_INTERFACE, interfaceDbMap));
			insertUpdateParameters(reg, com, comInterfaceUID);
		} else {
			// update if necessary
			DbTuple dbInterface = existingInterface.get(0);
			comInterfaceUID = dbInterface.get(SF.PK_UID);
			interfaceDbMap.put(SF.PK_UID, comInterfaceUID);
			if (!RigidUtils.equal(com.interfaceVersion(), dbInterface.get(E.COMMUNICATION_INTERFACE.interfaceVersion))) {
				interfaceDbMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				interfaceDbMap.put(SF.CHANGEDBY, EDITING_USER);
				interfaceDbMap.put(SF.VERSION, DbIncrement.INCREMENT);
				DbMap condition = new DbMap();
				condition.put(SF.PK_UID, dbInterface.get(SF.PK_UID));
				dbAccess.execute(new DbUpdateStatement<UID>(E.COMMUNICATION_INTERFACE, interfaceDbMap, condition));
				insertUpdateParameters(reg, com, comInterfaceUID);
			}
		}
		
		return comInterfaceUID;
	}
	
	private void insertUpdateParameters(RegistrationImpl reg, Communicator com, UID comInterfaceUID) {
		DbAccess dbAccess = dataBaseHelper.getDbAccess();
		DbQueryBuilder builder = dbAccess.getQueryBuilder();
		DbQuery<DbTuple> query = builder.createTupleQuery();
		DbFrom<UID> t = query.from(E.COMMUNICATION_INTERFACE_PARAMETER);
		query.multiselect(t.baseColumns(E.COMMUNICATION_INTERFACE_PARAMETER));
		query.where(
				builder.equalValue(t.baseColumn(E.COMMUNICATION_INTERFACE_PARAMETER.communicationInterface), 
						comInterfaceUID));
		
		List<DbTuple> existingParameters = new ArrayList<DbTuple>(dbAccess.executeQuery(query));
		
		for (Entry<String, String> parameter : reg.parameters.entrySet()) {
			String name = parameter.getKey();
			String description = parameter.getValue();
			
			Iterator<DbTuple> itExist = existingParameters.iterator();
			DbTuple existingParameter = null;
			while (itExist.hasNext()) {
				DbTuple i = itExist.next();
				if (RigidUtils.equal(name, i.get(E.COMMUNICATION_INTERFACE_PARAMETER.name))) {
					existingParameter = i;
					itExist.remove();
					break;
				}
			}
			
			DbMap parameterDbMap = new DbMap();
			parameterDbMap.put(E.COMMUNICATION_INTERFACE_PARAMETER.communicationInterface, comInterfaceUID);
			parameterDbMap.put(E.COMMUNICATION_INTERFACE_PARAMETER.name, name);
			parameterDbMap.put(E.COMMUNICATION_INTERFACE_PARAMETER.description, description);
			
			if (existingParameter == null) {
				// create one
				parameterDbMap.put(SF.PK_UID, new UID());
				parameterDbMap.put(SF.VERSION, 1);
				parameterDbMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				parameterDbMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				parameterDbMap.put(SF.CREATEDBY, EDITING_USER);
				parameterDbMap.put(SF.CHANGEDBY, EDITING_USER);
				dbAccess.execute(new DbInsertStatement<UID>(E.COMMUNICATION_INTERFACE_PARAMETER, parameterDbMap));
			} else {
				// update
				parameterDbMap.put(SF.PK_UID, existingParameter.get(SF.PK_UID));
				parameterDbMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
				parameterDbMap.put(SF.CHANGEDBY, EDITING_USER);
				parameterDbMap.put(SF.VERSION, DbIncrement.INCREMENT);
				DbMap condition = new DbMap();
				condition.put(SF.PK_UID, existingParameter.get(SF.PK_UID));
				dbAccess.execute(new DbUpdateStatement<UID>(E.COMMUNICATION_INTERFACE_PARAMETER, parameterDbMap, condition));
			}
		}
		
		// remove unused
		for (DbTuple existingParameter : existingParameters) {
			DbMap condition = new DbMap();
			condition.put(SF.PK_UID, existingParameter.get(SF.PK_UID));
			dbAccess.execute(new DbDeleteStatement<UID>(E.COMMUNICATION_INTERFACE_PARAMETER, condition));
		}
	}
	
	private DbMap getInterfaceDbMap(RegistrationImpl reg, Communicator com) {
		DbMap map = new DbMap();
		map.put(E.COMMUNICATION_INTERFACE.domainName, com.domainName());
		map.put(E.COMMUNICATION_INTERFACE.interfaceName, com.interfaceName());
		map.put(E.COMMUNICATION_INTERFACE.interfaceVersion, com.interfaceVersion());
		map.put(E.COMMUNICATION_INTERFACE.manufacturer, com.manufacturer());
		map.put(E.COMMUNICATION_INTERFACE.multiInstanceable, reg.isMultiInstanceable);
		if (reg.description != null) {
			map.put(E.COMMUNICATION_INTERFACE.description, reg.description);
		}
		if (reg.helpUrl != null) {
			map.put(E.COMMUNICATION_INTERFACE.helpUrl, reg.helpUrl);
		}
		return map;
	}
	
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}
	
	public void onApplicationEvent(ContextClosedEvent event) {
		for (InterfaceStorage iStore : activeInterfaces.keySet()) {
			shutdownPorts(iStore);
		}
		activeInterfaces.clear();
    }

}
