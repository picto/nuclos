package org.nuclos.server.businesstest.sampledata;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.time.DateUtils;

/**
 * Generates new sample date data by re-using the given sample values based on their relative probability.
 * Sample values are truncated to simple date values (without time).
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class SampleDateDataGenerator extends SampleDataGenerator<Date> {
	private final Random random = new Random();

	final Map<Date, Integer> dates = new HashMap<>();
	int total = 0;

	SampleDateDataGenerator() {
		super(Date.class);
	}

	@Override
	public void addSampleValues(final Collection<Date> values) {
		for (Date value : values) {
			value = DateUtils.truncate(value, Calendar.DATE);
			if (!dates.containsKey(value)) {
				dates.put(value, 0);
			}
			dates.put(value, dates.get(value) + 1);
			total++;
		}
	}

	/**
	 * Generates a new Boolean value, with the probability of the value being tru, false or null
	 * based on the respective distribution in the input values.
	 *
	 * @return
	 */
	@Override
	public Date newValue() {
		if (dates.isEmpty()) {
			return null;
		}

		int i = random.nextInt(total) + 1;

		int sum = 0;
		for (Map.Entry<Date, Integer> entry : dates.entrySet()) {
			sum += entry.getValue();
			if (sum >= i) {
				return entry.getKey();
			}
		}

		return null;
	}
}
