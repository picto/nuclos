package org.nuclos.server.businesstest;

import java.math.BigDecimal;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public interface IBusinessTestLogger {
	boolean isDebug();

	void setDebug(boolean debug);

	void debugln(String message);

	void println(String message);

	void print(String message);

	void printError(String message, Throwable t);

	void printProgress(BigDecimal progress);

	void printProgress(int current, int max);
}
