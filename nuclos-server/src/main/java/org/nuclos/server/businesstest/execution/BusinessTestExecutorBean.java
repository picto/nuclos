package org.nuclos.server.businesstest.execution;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.businesstest.BusinessTestVO;
import org.nuclos.common.businesstest.BusinessTestVO.STATE;
import org.nuclos.common2.DateTime;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.businesstest.BusinessTestLogger;
import org.nuclos.server.businesstest.BusinessTestManagementBean;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassGenerator;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassGeneratorBean;
import org.nuclos.server.businesstest.codegeneration.BusinessTestClassLoaderBean;
import org.nuclos.server.businesstest.codegeneration.BusinessTestScriptGeneratorBean;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestEntitySource;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptWriterBean;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.masterdata.ejb3.MetaDataFacadeBean;
import org.nuclos.server.rest.services.BusinessTestRestService.BusinessTestLogWriter;
import org.nuclos.server.statemodel.ejb3.StateFacadeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;

/**
 * Runs business tests.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestExecutorBean {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestClassGenerator.class);

	@Autowired
	MetaDataFacadeBean metaDataFacadeBean;

	@Autowired
	StateFacadeBean stateFacadeBean;

	@Autowired
	ServerParameterProvider parameterProvider;

	@Autowired
	BusinessTestClassGeneratorBean businessTestClassGeneratorBean;

	@Autowired
	BusinessTestManagementBean businessTestManagementBean;

	@Autowired
	BusinessTestClassLoaderBean businessTestClassLoaderBean;

	@Autowired
	IBusinessTestNucletCache nucletCache;

	@Autowired
	BusinessTestScriptWriterBean scriptWriter;

	@Autowired
	BusinessTestScriptGeneratorBean scriptGenerator;

	private static List<Pattern> scriptExceptionPatterns = Arrays.asList(
			Pattern.compile("^\tat Script1(?:\\.|\\$).+?\\(Script1\\.groovy:(\\d+)\\)"),
			Pattern.compile("^Script1\\.groovy: (\\d+):")
	);

	/**
	 * Trigger synchronous execution of a single business test.
	 *
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	public void runTest(UID businessTestId, BusinessTestLogger logger) {
		BusinessTestVO test;
		try {
			test = businessTestManagementBean.get(businessTestId);
			runTests(Collections.singleton(test), logger, true);
		} catch (Exception e) {
			logger.printError("Could not run test", e);
			throw new RuntimeException("Could not run test", e);
		}
	}

	/**
	 * Trigger synchronous execution of a single business test.
	 *
	 * @return
	 */
	@Transactional(propagation = Propagation.REQUIRED, noRollbackFor = Throwable.class)
	private void runTest(final BusinessTestVO test,
						 final GroovyClassLoader groovyClassLoader,
						 final IBusinessTestLogger logger,
						 final boolean printScriptLog) {
		logger.print("Running test " + test.getName() + "...");

		if (printScriptLog) {
			logger.println("\n");
		}

		final CompilerConfiguration config = new CompilerConfiguration();
		config.setScriptBaseClass(BusinessTestScript.class.getCanonicalName());

		final StringWriter buffer = new StringWriter();
		final BusinessTestBinding binding = new BusinessTestBinding(buffer, logger, printScriptLog);
		final GroovyShell groovyShell = new GroovyShell(groovyClassLoader, binding, config);

		logger.debugln("\n\nRunning script:\n" + test.getSource() + "\n");

		BusinessTestScript script = null;
		try {
			test.reset();
			test.setStartdate(new DateTime());

			script = (BusinessTestScript) groovyShell.parse(test.getSource());

			runAndRollback(script);
			binding.flush();

			logger.println("   PASSED");
			test.setState(STATE.GREEN);
			test.setResult("PASSED");
			test.appendLog(buffer.toString());

			if (script.getLastError() != null) {
				handleException(test, script.getLastError(), logger, printScriptLog, false);
				test.setResult("PASSED with errors");
			}
		} catch (Throwable t) {
			binding.flush();
			logger.println("   FAILED: " + t.getMessage());
			test.appendLog(buffer.toString());
			handleException(test, t, logger, printScriptLog, true);
		}

		if (script != null) {
			test.setWarningCount(test.getWarningCount() + script.getWarningCount());
			test.setErrorCount(test.getErrorCount() + script.getErrorCount());
		}

		test.setEnddate(new DateTime());
		test.setDuration(test.getEnddate().getTime() - test.getStartdate().getTime());

		try {
			businessTestManagementBean.modify(test);
		} catch (Exception e) {
			logger.printError("Could not update test data", e);
			LOG.error("Could not update test data", e);
		}
	}

	/**
	 * Sets the state of a test based on occurred Exceptions, logs the Stacktrace, increases warning/error count,
	 * sets the test result message.
	 *
	 * TODO: Refactor this.
	 *
	 * @param test
	 * @param t
	 * @param logger
	 * @param printScriptLog
	 * @param incrementCounters
	 */
	private void handleException(
			final BusinessTestVO test,
			final Throwable t,
			final IBusinessTestLogger logger,
			final boolean printScriptLog,
			final boolean incrementCounters
	) {
		if (CommonBusinessException.class.isAssignableFrom(t.getClass())) {
			test.setState(STATE.YELLOW);
			test.appendLog(ExceptionUtils.getFullStackTrace(t));
			if (printScriptLog) {
				logger.println(ExceptionUtils.getFullStackTrace(t));
			}
			if (incrementCounters) {
				test.setWarningCount(test.getWarningCount() + 1);
			}
		} else if (t instanceof BusinessTestException) {
			BusinessTestException bte = (BusinessTestException) t;
			test.setState(bte.getState());
			test.appendLog(bte.getStacktrace());
			if (printScriptLog) {
				logger.println(bte.getStacktrace());
			}
			if (incrementCounters) {
				test.setWarningCount(test.getWarningCount() + 1);
			}
		} else {
			test.setState(STATE.RED);
			test.appendLog(ExceptionUtils.getFullStackTrace(t));
			if (printScriptLog) {
				logger.println(ExceptionUtils.getFullStackTrace(t));
			}
			if (incrementCounters) {
				test.setErrorCount(test.getErrorCount() + 1);
			}
		}
		test.setResult(getExceptionMessage(t));

		switch (test.getState()) {
			case YELLOW:
				test.setWarningLine(getExceptionLine(t));
				break;
			case RED:
				test.setErrorLine(getExceptionLine(t));
				break;
		}
	}

	/**
	 * Returns the line number as found in the given Throwable for "Script1.groovy".
	 *
	 * @param t
	 * @return
	 */
	private Integer getExceptionLine(final Throwable t) {
		String[] stackFrames = ExceptionUtils.getStackFrames(t);
		for (String frame : stackFrames) {
			Integer line = getLineMatch(frame);
			if (line != null) {
				return line;
			}
		}

		return null;
	}

	/**
	 * Matches the given line against all Exception patterns.
	 *
	 * @param line
	 * @return
	 */
	private Integer getLineMatch(final String line) {
		for (Pattern p : scriptExceptionPatterns) {
			Matcher m = p.matcher(line);
			if (m.find()) {
				try {
					Integer lineNumber = Integer.parseInt(m.group(1));
					return lineNumber;
				} catch (Exception ex) {
					LOG.warn("Unable to parse line", ex);
				}
			}
		}
		return null;
	}

	/**
	 * Creates a savepoint, runs the script and rolls back to the savepoint.
	 *
	 * @param script
	 */
	private void runAndRollback(BusinessTestScript script) {
		boolean rollback = !parameterProvider.isEnabled("BUSINESS_TEST_NO_ROLLBACK");
		if (!rollback) {
			script.run();
			return;
		}

		Object savepoint = null;
		try {
			savepoint = TransactionAspectSupport.currentTransactionStatus().createSavepoint();
			script.run();
		} finally {
			if (savepoint != null) {
				TransactionAspectSupport.currentTransactionStatus().rollbackToSavepoint(savepoint);
			}
		}
	}

	/**
	 * Returns a readable Exception message for the given Throwable.
	 *
	 * @param t
	 * @return
	 */
	private String getExceptionMessage(Throwable t) {
		final CommonValidationException cve = findValidationException(t);

		String message = null;
		if (cve != null) {
			final String htmlMessage = cve.getFullMessage();
			final String plainMessage = new HtmlToPlainText().getPlainText(Jsoup.parse(htmlMessage));
			message = plainMessage;
		} else {
			message = t.getMessage();
		}

		return StringUtils.isNotBlank(message) ? message : t.toString();
	}

	/**
	 * Looks for a CommonValidationException in the Stacktrace of the given Throwable.
	 *
	 * @param t
	 * @return
	 */
	private CommonValidationException findValidationException(Throwable t) {
		while (!(t instanceof CommonValidationException) && t != null) {
			t = t.getCause();
		}
		return (CommonValidationException) t;
	}

	/**
	 * Run all business tests synchronously.
	 *
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	public void runAllTests(final BusinessTestLogger logger) {
		List<BusinessTestVO> tests = businessTestManagementBean.getAll();

		if (tests.isEmpty()) {
			logger.println("No tests");
			return;
		}

		try {
			logger.println("Running all business tests...");
			runTests(tests, logger, false);
			logger.println("Running all business tests: Done");
		} catch (Exception ex) {
			logger.printError("Failed to execute business tests", ex);
		}
	}

	/**
	 * Run the given business tests synchronously.
	 *
	 * @return
	 * @throws CommonPermissionException
	 * @throws CommonFinderException
	 */
	private void runTests(final Collection<BusinessTestVO> tests,
						  final BusinessTestLogger logger,
						  final boolean printScriptLogs) {
		final Map<UID, BusinessTestEntitySource> classes = generateEntityClasses(logger);
		final GroovyClassLoader groovyClassLoader = compileSources(logger.getWriter(), classes);
		writeScriptsToFilesystem(tests);

		int testsRun = 0;
		for (BusinessTestVO test : tests) {
			runTest(test, groovyClassLoader, logger, printScriptLogs);

			testsRun++;
			logger.printProgress(testsRun, tests.size());
		}
	}

	/**
	 * Writes the given scripts to the filesystem (data/codegenerator/btsrc).
	 *
	 * @param tests
	 */
	private void writeScriptsToFilesystem(final Collection<BusinessTestVO> tests) {
		try {
			scriptWriter.writeScriptsIncremental(tests);
		} catch (IOException e) {
			LOG.error("Could not write test scripts to output directory", e);
		}
	}

	private GroovyClassLoader compileSources(final BusinessTestLogWriter writer, final Map<UID, BusinessTestEntitySource> classes) {
		GroovyClassLoader groovyClassLoader = null;
		try {
			groovyClassLoader = businessTestClassLoaderBean.compileSources(classes);
		} catch (IOException e) {
			writer.logError("Could not compile business test classes", e);
			LOG.error("Could not compile business test classes", e);
			throw new NuclosFatalException(e);
		}
		return groovyClassLoader;
	}

	private Map<UID, BusinessTestEntitySource> generateEntityClasses(final IBusinessTestLogger logger) {
		Map<UID, BusinessTestEntitySource> classes;
		try {
			logger.println("Generating test classes...");
			classes = businessTestClassGeneratorBean.generateSources();
		} catch (CommonFinderException | CommonPermissionException e) {
			logger.printError("Could not generate business test classes", e);
			LOG.error("Could not generate business test classes", e);
			throw new NuclosFatalException(e);
		}
		return classes;
	}

	/**
	 * Groovy-Binding with a custom PrintWriter, which receives all output from inside the script
	 * via print(...) and println(...).
	 * The output is possibly written to the global BusinessTestLogWriter, to be immediately streamed to the client.
	 */
	private class BusinessTestBinding extends Binding {
		private final PrintWriter out;

		public BusinessTestBinding(
				final StringWriter buffer,
				final IBusinessTestLogger logger,
				final boolean printScriptLog
		) {
			if (printScriptLog) {
				out = new PrintWriter(buffer, true) {
					StringBuffer logBuffer = new StringBuffer();

					@Override
					public void write(final int c) {
						super.write(c);
						logBuffer.append(c);
					}

					@Override
					public void write(final char[] buf, final int off, final int len) {
						super.write(buf, off, len);
						logBuffer.append(buf, off, len);
					}

					@Override
					public void write(final String s, final int off, final int len) {
						super.write(s, off, len);
						logBuffer.append(s, off, len);
					}

					@Override
					public void println() {
						super.println();
						logger.println(logBuffer.toString());
						logBuffer.setLength(0);
					}

					@Override
					public void close() {
						flush();
						super.close();
					}

					@Override
					public void flush() {
						super.flush();
						if (logBuffer.length() > 0) {
							logger.print(logBuffer.toString());
							logBuffer.setLength(0);
						}
					}
				};
			} else {
				out = new PrintWriter(buffer);
			}

			setProperty("out", out);
		}

		public void flush() {
			try {
				out.flush();
			} catch (Exception ex) {
				// ignore
			}
		}
	}
}
