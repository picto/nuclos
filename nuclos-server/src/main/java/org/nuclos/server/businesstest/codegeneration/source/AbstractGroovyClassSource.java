package org.nuclos.server.businesstest.codegeneration.source;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Defines a Groovy class.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class AbstractGroovyClassSource extends AbstractGroovySource {
	private final String className;
	protected Set<Method> methods = new TreeSet<>();
	protected final List<String> fields = new ArrayList<>();
	private String baseClass = null;
	private String[] baseClassTypeParams = null;
	private boolean compileStatic = false;

	public AbstractGroovyClassSource(final String pkg, final String className) {
		super(pkg);
		this.className = className;
	}

	public void addMethod(Method method) {
		methods.add(method);
	}

	public void addField(String field) {
		fields.add(field);
	}

	public String getBaseClass() {
		return baseClass;
	}

	public void setBaseClass(String baseClass) {
		this.baseClass = baseClass;
	}

	public void setBaseClass(Class<?> baseClass, String... baseClassTypeParams) {
		this.baseClass = baseClass.getCanonicalName();
		this.baseClassTypeParams = baseClassTypeParams;
	}

	public boolean isCompileStatic() {
		return compileStatic;
	}

	public void setCompileStatic(final boolean compileStatic) {
		this.compileStatic = compileStatic;
	}

	public String getGroovySource() {
		String source = super.getGroovySource();

		if (isCompileStatic()) {
			source += "@groovy.transform.CompileStatic\n";
		}

		source += "class " + className;

		if (baseClass != null) {
			source += " extends " + baseClass;
			if (baseClassTypeParams != null && baseClassTypeParams.length > 0) {
				source += "<" + StringUtils.join(baseClassTypeParams, ", ") + ">";
			}
		}

		source += " {\n";

		for (String field : fields) {
			source += "\n" + indent(field);
		}

		for (Method method : methods) {
			source += "\n\n" + indent(method.toString());
		}

		source += "\n}\n";

		return source;
	}

	public String getClassName() {
		return className;
	}

	public Set<Method> getMethods() {
		return Collections.unmodifiableSet(methods);
	}

	public List<String> getFields() {
		return fields;
	}

	/**
	 * Indents the given text by 1 Tab.
	 * <p>
	 * WARNING: This might alter Groovy multi-line Strings!
	 *
	 * @param text
	 * @return
	 */
	public static String indent(String text) {
		return text.replaceAll("(?m)^", "\t");
	}

	/**
	 * Creates Methods via the Builder pattern.
	 */
	public static class MethodBuilder {
		protected String comment = null;
		protected int modifier = 0;
		protected final String name;
		protected String returnType = "";
		protected String typeParameter = null;
		protected final List<Param> params = new ArrayList<>();
		protected final List<Class<?>> exceptionTypes = new ArrayList<>();
		protected String source;

		public MethodBuilder(final String name) {
			this.name = name;
		}

		/**
		 * See {@link java.lang.reflect.Modifier} for possible values.
		 *
		 * @param modifier
		 * @return
		 */
		public MethodBuilder addModifier(int modifier) {
			this.modifier |= modifier;
			return this;
		}

		public MethodBuilder setReturnType(final String returnType) {
			this.returnType = returnType;
			return this;
		}

		public MethodBuilder setTypeParameter(final String typeParameter) {
			this.typeParameter = typeParameter;
			return this;
		}

		public MethodBuilder addParam(final Param param) {
			this.params.add(param);
			return this;
		}

		public MethodBuilder addParam(final String type, final String name) {
			final Param param = new Param(type, name);
			return addParam(param);
		}

		public MethodBuilder addException(final Class<?> exceptionType) {
			this.exceptionTypes.add(exceptionType);
			return this;
		}

		public MethodBuilder setSource(final String source) {
			this.source = source;
			return this;
		}

		public MethodBuilder setComment(final String comment) {
			this.comment = comment;
			return this;
		}

		public Method build() {
			return new Method(
					comment,
					modifier,
					typeParameter,
					returnType,
					name,
					params,
					exceptionTypes,
					source
			);
		}
	}

	;

	public static class ConstructorBuilder extends MethodBuilder {
		public ConstructorBuilder(final String className) {
			super(className);
		}

		/**
		 * Does nothing, as constructors don't have a return type.
		 *
		 * @param returnType
		 * @return
		 */
		@Override
		public MethodBuilder setReturnType(final String returnType) {
			return this;
		}
	}

	public static class Method implements Comparable<Method> {
		private final String comment;

		private final int modifier;
		private final String typeParameter;
		private final String returnType;
		private final String name;

		private final List<Param> params;
		private final List<Class<?>> exceptionTypes;

		private final String source;

		Method(
				final String comment,
				final int modifier,
				final String typeParameter,
				final String returnType,
				final String name,
				final List<Param> params,
				final List<Class<?>> exceptionTypes,
				final String source) {

			if (StringUtils.isBlank(name)) {
				throw new IllegalArgumentException("name must not be blank");
			}

			this.comment = comment;

			this.modifier = modifier;
			this.name = name;
			this.returnType = returnType;
			this.typeParameter = typeParameter;
			this.params = params;
			this.exceptionTypes = exceptionTypes;
			this.source = source;
		}

		public int getModifier() {
			return modifier;
		}

		public String getTypeParameter() {
			return typeParameter;
		}

		public String getReturnType() {
			return returnType;
		}

		public String getName() {
			return name;
		}

		public List<Param> getParams() {
			return Collections.unmodifiableList(params);
		}

		public List<Class<?>> getExceptionTypes() {
			return Collections.unmodifiableList(exceptionTypes);
		}

		public String getSource() {
			return source;
		}

		public String toString() {
			String result = "";

			result = addJavadocComment(result);

			final String modifierString = Modifier.toString(modifier);
			if (StringUtils.isNotBlank(modifierString)) {
				result += modifierString + " ";
			}

			if (StringUtils.isNotBlank(typeParameter)) {
				// A modifier is necessary before a type param, or we get syntax errors.
				if (StringUtils.isBlank(modifierString)) {
					result += "public ";
				}
				result += typeParameter + " ";
			}

			// Return type can be blank for constructors.
			if (StringUtils.isNotBlank(returnType)) {
				result += returnType + " ";
			}

			result += name;
			result += "(" + StringUtils.join(params, ", ") + ")";

			if (CollectionUtils.isNotEmpty(exceptionTypes)) {
				result += " throws " + StringUtils.join(exceptionTypes, ", ");
			}

			result += " {\n";
			result += indent(source);
			result += "\n}";

			return result;
		}

		private String addJavadocComment(String result) {
			result += "/**\n";
			for (String line : comment.split("\n")) {
				result += "* " + line + "\n";
			}
			if (CollectionUtils.isNotEmpty(params)) {
				result += "*\n";
				for (Param param : params) {
					result += "* @param " + param.name;
					if (StringUtils.isNotBlank(param.getComment())) {
						result += " " + param.getComment();
					}
					result += "\n";
				}
			}
			result += "*/\n";
			return result;
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) return true;
			if (!(o instanceof Method)) return false;

			final Method method = (Method) o;

			return getSignature().equals(method.getSignature());
		}

		@Override
		public int hashCode() {
			int result = name.hashCode();
			result = 31 * result + params.hashCode();
			return result;
		}

		@Override
		public int compareTo(final Method o) {
			if (this.equals(o)) {
				return 0;
			}

			// Constructors come first
			if (this.isConstructor() && !o.isConstructor()) {
				return -1;
			} else if (o.isConstructor() && !this.isConstructor()) {
				return 1;
			}

			return this.getSignature().compareTo(o.getSignature());
		}

		public boolean isConstructor() {
			return StringUtils.isBlank(returnType);
		}

		private String getSignature() {
			String signature = "";

			signature += name;

			List<String> paramTypes = new ArrayList<>();
			for (Param param : params) {
				paramTypes.add(param.getType());
			}

			signature += "(" + StringUtils.join(paramTypes, ", ") + ")";

			return signature;
		}
	}

	public static class Param {
		final private String type;
		final private String name;
		final private String comment;

		public Param(final String type, final String name) {
			this(type, name, "");
		}

		public Param(final String type, final String name, final String comment) {
			this.type = type;
			this.name = name;
			this.comment = comment;
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		public String getComment() {
			return comment;
		}

		public String toString() {
			return type + " " + name;
		}
	}
}
