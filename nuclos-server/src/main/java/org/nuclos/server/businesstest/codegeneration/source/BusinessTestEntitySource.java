package org.nuclos.server.businesstest.codegeneration.source;

import org.nuclos.common.UID;

/**
 * Defines the Groovy source code of a business test entity class.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 *
 */
public class BusinessTestEntitySource extends AbstractGroovyClassSource {
	private final UID entityUid;

	public BusinessTestEntitySource(final String pkg, final String className, UID entityUid) {
		super(pkg, className);

		this.entityUid = entityUid;
		setCompileStatic(true);
	}
}
