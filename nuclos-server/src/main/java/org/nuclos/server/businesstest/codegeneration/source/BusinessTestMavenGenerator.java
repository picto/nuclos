package org.nuclos.server.businesstest.codegeneration.source;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Creates a pom.xml containing all necessary dependencies for the business test sources.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestMavenGenerator {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestMavenGenerator.class);

	private final String nuclosVersion;
	private final String groovyVersion;

	public BusinessTestMavenGenerator(final String nuclosVersion, final String groovyVersion) {
		this.nuclosVersion = nuclosVersion;
		this.groovyVersion = groovyVersion;
	}

	public String generatePomXml() {

		//Freemarker configuration object
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setClassForTemplateLoading(this.getClass(), "");

		try {
			//Load template from source folder
			Template template = cfg.getTemplate("pom-btsrc.xml");

			// Build the data-model
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("nuclosVersion", nuclosVersion);
			data.put("groovyVersion", groovyVersion);

			// Console output
			Writer out = new StringWriter();
			template.process(data, out);

			return out.toString();
		} catch (IOException | TemplateException e) {
			LOG.error("Failed to process template", e);
		}

		return null;
	}
}
