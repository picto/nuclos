package org.nuclos.server.businesstest.codegeneration;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.E;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.businesstest.NucletVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeBean;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
@Service
public class BusinessTestNucletCacheBean implements IBusinessTestNucletCache {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestNucletCacheBean.class);

	@Autowired
	@Qualifier("masterDataService")
	MasterDataFacadeBean masterDataFacade;

	/**
	 * Maps Nuclet UIDs to the corresponding NucletVO.
	 */
	private final Map<UID, NucletVO> nucletCache = new HashMap<UID, NucletVO>();

	/**
	 * Maps Nuclet packages to the corresponding NucletVO.
	 */
	private final Map<String, NucletVO> nucletPackageCache = new HashMap<>();

	public static IBusinessTestNucletCache getInstance() {
		return SpringApplicationContextHolder.getBean(BusinessTestNucletCacheBean.class);
	}

	/**
	 * @param nucletUID
	 * @return
	 */
	@Override
	public synchronized NucletVO getNuclet(UID nucletUID) {
		if (!nucletCache.containsKey(nucletUID)) {
			MasterDataVO<UID> mdvo;
			try {
				mdvo = masterDataFacade.get(E.NUCLET.UID, nucletUID);
				NucletVO nucletVO = new NucletVO(mdvo);
				cacheNuclet(nucletVO);
			} catch (CommonFinderException | CommonPermissionException e) {
				e.printStackTrace();
			}
		}

		return nucletCache.get(nucletUID);
	}

	/**
	 * @param pkg
	 * @return
	 */
	@Override
	public synchronized NucletVO getNucletByPackage(final String pkg) {
		if (!nucletPackageCache.containsKey(pkg)) {
			MasterDataVO<UID> mdvo;
			final CollectableComparison search = SearchConditionUtils.newComparison(
					E.NUCLET.packagefield,
					ComparisonOperator.EQUAL,
					pkg
			);
			final TruncatableCollection<MasterDataVO<UID>> data = masterDataFacade.getMasterData(E.NUCLET.UID, search, true);
			if (!data.isEmpty()) {
				NucletVO nucletVO = new NucletVO(data.iterator().next());
				cacheNuclet(nucletVO);
			}
		}

		return nucletPackageCache.get(pkg);
	}

	private synchronized void cacheNuclet(NucletVO nucletVO) {
		nucletCache.put(nucletVO.getUid(), nucletVO);
		nucletPackageCache.put(nucletVO.getPackage(), nucletVO);
	}
}
