package org.nuclos.server.businesstest.codegeneration.script;

import org.nuclos.common.EntityMeta;
import org.nuclos.server.businesstest.IBusinessTestLogger;
import org.nuclos.server.businesstest.codegeneration.BusinessTestGenerationContext;
import org.nuclos.server.businesstest.codegeneration.IBusinessTestNucletCache;
import org.nuclos.server.businesstest.codegeneration.source.BusinessTestScriptSource;
import org.nuclos.server.statemodel.valueobject.StateVO;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class BusinessTestStateChangeScriptGenerator extends AbstractBusinessTestScriptGenerator {
	public static final int DEFAULT_RECORD_COUNT = 10;
	private final StateVO sourceState;
	private final StateVO targetState;

	BusinessTestStateChangeScriptGenerator(
			final EntityMeta<?> meta,
			final IBusinessTestNucletCache nucletCache,
			final IBusinessTestLogger logger,
			final BusinessTestGenerationContext context, final StateVO sourceState,
			final StateVO targetState) {
		super(meta, nucletCache, logger, context);
		this.sourceState = sourceState;
		this.targetState = targetState;
	}

	public String getTestName() {
		return "STATECHANGE "
				+ meta.getEntityName() + " "
				+ sourceState.getNumeral() + " to "
				+ targetState.getNumeral();
	}

	@Override
	public BusinessTestScriptSource generateScriptSource() {
		final String className = getClassName();
		final BusinessTestScriptSource script = createDefaultScript();

		script.addScriptLine("// Try to find \"" + className + "\" records in state " + sourceState.getNumeral() + ", limited to " + DEFAULT_RECORD_COUNT + " records");
		script.addScriptLine("Collection<" + className + "> bos = " + className + ".findByState("
				+ sourceState.getNumeral() + ", "
				+ DEFAULT_RECORD_COUNT
				+ ")");
		script.addScriptLine("");
		script.addScriptLine("if (bos.empty) {");
		script.addScriptLine("	fail('Could not run test: no records available')");
		script.addScriptLine("}");
		script.addScriptLine("");
		script.addScriptLine("bos.each {" + className + " bo ->");
		script.addScriptLine("	attempt {");

		// TODO: Fill fields that are mandatory for the next state

		script.addScriptLine("		bo.changeState(" + targetState.getNumeral() + ")");
		script.addScriptLine("		assert bo.nuclosStateNumber == " + targetState.getNumeral());
		script.addScriptLine("	}");
		script.addScriptLine("}");

		return script;
	}
}
