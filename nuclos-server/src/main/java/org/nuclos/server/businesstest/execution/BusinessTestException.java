package org.nuclos.server.businesstest.execution;

import org.nuclos.common.businesstest.BusinessTestVO.STATE;

/**
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public class BusinessTestException extends RuntimeException {
	/**  */
	private static final long serialVersionUID = 1L;

	private final STATE state;
	private final String stacktrace;

	public BusinessTestException(String message, String stacktrace, STATE state) {
		super(message);

		this.stacktrace = stacktrace;
		this.state = state;
	}

	public String getStacktrace() {
		return stacktrace;
	}

	public STATE getState() {
		return state;
	}
}
