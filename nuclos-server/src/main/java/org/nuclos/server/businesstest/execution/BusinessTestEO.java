package org.nuclos.server.businesstest.execution;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nuclos.api.businessobject.Flag;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.common.NuclosFileBase;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDependentKey;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.genericobject.valueobject.GenericObjectDocumentFile;
import org.nuclos.server.nbo.AbstractBusinessObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.lang.GString;
import net.sf.jsqlparser.JSQLParserException;

/**
 * Base class for the entities used in business tests. All generated test entities extend this class.
 * Some method names which are mainly ment for internal use start with an underscore, so they don't possibly interfere
 * with BO attributes of the same name.
 *
 * @author Andreas Lämmlein <andreas@laemm-line.de>
 */
public abstract class BusinessTestEO<T extends BusinessTestEO> {
	private static final Logger LOG = LoggerFactory.getLogger(BusinessTestEO.class);

	protected EntityObjectVO<Long> wrappedEO;
	private final Class<T> cls;

	// TODO: DependentDataMap should know about loaded dependents.
	private Set<IDependentKey> loadedDependents = new HashSet<>();


	public static <T extends BusinessTestEO> T from(EntityObjectVO<Long> wrappedEO, Class<T> cls) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		final Constructor<T> constructor = cls.getConstructor(wrappedEO.getClass());
		constructor.setAccessible(true);
		return constructor.newInstance(wrappedEO);
	}

	protected BusinessTestEO(UID entityUID, Class<T> cls) {
		this.cls = cls;
		wrappedEO = new EntityObjectVO<>(entityUID);
		wrappedEO.flagNew();
	}

	protected BusinessTestEO(EntityObjectVO<Long> wrappedEO, Class<T> cls) {
		this.cls = cls;
		this.wrappedEO = wrappedEO;
	}

	protected static <T extends BusinessTestEO> T get(UID entityUID, Class<T> cls, final Long id) throws CommonPermissionException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
		return BusinessTestRESTFacadeBean.getInstance().get(entityUID, cls, id);
	}

	public T reload() throws CommonPermissionException {
		loadedDependents.clear();
		return (T) BusinessTestRESTFacadeBean.getInstance().reload(this);
	}

	public BusinessTestEO<T> save() throws CommonBusinessException {
		BusinessTestRESTFacadeBean.getInstance().save(this);

		// Dependents may have changed and must be reloaded
		loadedDependents.clear();

		return this;
	}

	public void changeState(int numeral) throws CommonBusinessException {
		BusinessTestRESTFacadeBean.getInstance().changeState(this, numeral);
		reload();
	}

	public void delete() throws CommonBusinessException {
		BusinessTestRESTFacadeBean.getInstance().delete(this);
	}

	protected EntityObjectVO getWrappedEO() {
		return wrappedEO;
	}

	protected void setWrappedEO(EntityObjectVO wrappedEO) {
		this.wrappedEO = wrappedEO;
	}

	protected UID getEntityUID() {
		return wrappedEO.getDalEntity();
	}

	public Long getId() {
		return wrappedEO.getPrimaryKey();
	}

	protected Object getAttribute(String fieldUID) {
		return getWrappedEO().getFieldValue(new UID(fieldUID));
	}

	protected Long getAttributeId(String fieldUID) {
		return getWrappedEO().getFieldId(new UID(fieldUID));
	}

	protected UID getAttributeUid(String fieldUID) {
		return getWrappedEO().getFieldUid(new UID(fieldUID));
	}

	protected void setAttribute(final String fieldUID, Object value) {
		if (value instanceof GString) {
			value = value.toString();
		}

		getWrappedEO().setFieldValue(new UID(fieldUID), value);
		setModified();
	}

	protected void setAttributeId(String fieldUID, Long id) {
		getWrappedEO().setFieldId(new UID(fieldUID), id);
		setModified();
	}

	protected void setAttributeUid(String fieldUID, String uidString) {
		UID uid = uidString == null ? null : new UID(uidString);
		setAttributeUid(fieldUID, uid);
	}

	protected void setAttributeUid(String fieldUID, UID uid) {
		getWrappedEO().setFieldUid(new UID(fieldUID), uid);
		setModified();
	}

	/**
	 * Flags the underlying EntityObject as updated, if it is not new or removed.
	 */
	private void setModified() {
		if(!getWrappedEO().isFlagNew() && !getWrappedEO().isFlagRemoved()) {
			getWrappedEO().flagUpdate();
		}
	}

	/**
	 * @param entityUID
	 * @param cls
	 * @param whereCondition
	 * @param <T>
	 * @return
	 * @throws JSQLParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected static <T extends BusinessTestEO> List<T> query(
			final UID entityUID,
			final Class<T> cls,
			final String whereCondition) throws JSQLParserException, IllegalAccessException, InstantiationException {
		return BusinessTestRESTFacadeBean.getInstance().query(entityUID, cls, whereCondition, 100);
	}

	/**
	 * @param entityUID
	 * @param cls
	 * @param whereCondition
	 * @param limit
	 * @param <T>
	 * @return
	 * @throws JSQLParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected static <T extends BusinessTestEO> List<T> query(
			final UID entityUID,
			final Class<T> cls,
			final String whereCondition,
			final int limit
	) throws JSQLParserException, IllegalAccessException, InstantiationException {
		return BusinessTestRESTFacadeBean.getInstance().query(entityUID, cls, whereCondition, limit);
	}

	/**
	 * Gets the dependent EOs for the given dependent.
	 * Possibly filtered by the given {@link Flag}s.
	 *
	 * @param dependentUID
	 * @param dependentFieldUID
	 * @param flags
	 * @return
	 */
	protected Collection<EntityObjectVO<?>> getDependentEOs(
			final UID dependentUID,
			final UID dependentFieldUID,
			final Flag[] flags
	) {
		IDependentKey dependentKey = DependentDataMap.createDependentKey(dependentFieldUID);

		if (!loadedDependents.contains(dependentKey) && this.getId() != null) {
			Collection<EntityObjectVO<Long>> dependentEOs = BusinessTestRESTFacadeBean.getInstance().getDependentEOs(dependentUID, dependentFieldUID, getId());
			wrappedEO.getDependents().addAllData(dependentKey, dependentEOs);
			loadedDependents.add(dependentKey);
		}

		return wrappedEO.getDependents().getData(dependentKey, flags);
	}

	protected <S extends BusinessTestEO> BusinessTestEODependents<S> getDependents(
			final UID dependentUID,
			final UID dependentFieldUID,
			final Class<S> dependentClass,
			final Flag[] flags
	) {
		return new BusinessTestEODependents<S>(this, dependentUID, dependentFieldUID, dependentClass, flags);
	}

	protected void addDependent(IDependentKey dependentKey, EntityObjectVO<Long> dependent) {
		wrappedEO.getDependents().addData(dependentKey, dependent);
	}

	/**
	 * Executes the custom rule with the given name.
	 *
	 * @param customRule
	 */
	protected T executeCustomRule(final String customRule) throws CommonBusinessException {
		// Save first, if this record is new
		if (getId() == null) {
			save();
		}

		return (T) BusinessTestRESTFacadeBean.getInstance().executeCustomRule(this, customRule);
	}

	/**
	 * Queries only the first record without explicit sorting, so this might be database dependent.
	 *
	 * @param entityUID
	 * @param cls
	 * @param <T>
	 * @return
	 * @throws JSQLParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected static <T extends BusinessTestEO> T first(
			final UID entityUID,
			final Class<T> cls
	) throws IllegalAccessException, InstantiationException {
		final T result;
		final List<T> list = list(entityUID, cls, 1);

		if (!list.isEmpty()) {
			result = list.get(0);
		} else {
			result = null;
		}

		return result;
	}

	/**
	 * Queries the first "limit" records without explicit sorting, so this might be database dependent.
	 *
	 * @param entityUID
	 * @param cls
	 * @param limit
	 * @param <T>
	 * @return
	 * @throws JSQLParserException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	protected static <T extends BusinessTestEO> List<T> list(
			final UID entityUID,
			final Class<T> cls,
			final int limit
	) throws IllegalAccessException, InstantiationException {
		return BusinessTestRESTFacadeBean.getInstance().list(entityUID, cls, limit);
	}

	protected NuclosFile getNuclosFile(final String fieldUID) {
		getWrappedEO();
		Object value = getAttribute(fieldUID);

		if (value == null) {
			return null;
		}

		org.nuclos.common.NuclosFile result = ((GenericObjectDocumentFile) value).getNuclosFile();
		UID fileId = getAttributeUid(fieldUID);
		if (fileId != null) {
			result.setId(fileId);
		}
		return result;
	}

	protected <T extends NuclosFileBase> void setNuclosFile(final String fieldUID, final T file) {
		if (file instanceof org.nuclos.common.NuclosFile) {
			Object goFile = AbstractBusinessObject.createGenericObjectDocumentFile((org.nuclos.common.NuclosFile) file);
			setAttribute(fieldUID, goFile);
		} else if (file instanceof org.nuclos.common.NuclosFileLink) {
			org.nuclos.common.NuclosFileLink fileLink = (org.nuclos.common.NuclosFileLink) file;
			setAttributeUid(fieldUID, (UID) fileLink.getFileId());
		} else if (file == null) {
			setAttributeUid(fieldUID, (UID) null);
		} else {
			throw new org.apache.commons.lang.NotImplementedException();
		}
	}
}
