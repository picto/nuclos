//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.eventsupport.ejb3;

import java.util.Collection;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.EntityObjectVO;

public class ProxyContext<PK> {
	
	public static enum Type{
		GET_ALL,
		GET_ALL_IDS,
		GET_BY_ID,
		GET_BY_IDS,
		GET_BY_FOREIGN_KEY,
		INSERT_OR_UPDATE,
		INSERT_OR_UPDATE_BATCH,
		DELETE,
		DELETE_BATCH};
	
	private final UID entityUID;
	
	private final Type type;
	
	private EntityObjectVO<PK> paramObject;
	
	private Collection<EntityObjectVO<PK>> paramObjects;
	
	private PK paramId;
	private Object pkResult;
	
	private List<PK> paramIds;
	
	private Collection<Object> paramForeignIds;
	
	private Class<?> paramForeignIdClass;
	
	private String paramForeignMethodName;
	
	private Collection<Delete<PK>> paramDeletes;
	
	private DalCallResult dalCallResult;
	
	private boolean failAfterBatch;
	
	private EntityObjectVO<PK> singleResult;
	
	private List<EntityObjectVO<PK>> multiResult;
	
	private List<PK> idsResult;
	
	public ProxyContext(UID entityUID, Class<PK> pkType, Type type) {
		this.entityUID = entityUID;
		this.type = type;
	}
	
	public UID getEntity() {
		return entityUID;
	}
	
	public Type getType() {
		return type;
	}

	public EntityObjectVO<PK> getParamObject() {
		return paramObject;
	}

	public void setParamObject(EntityObjectVO<PK> paramObject) {
		this.paramObject = paramObject;
	}

	public Collection<EntityObjectVO<PK>> getParamObjects() {
		return paramObjects;
	}

	public void setParamObjects(Collection<EntityObjectVO<PK>> paramObjects) {
		this.paramObjects = paramObjects;
	}

	public PK getParamId() {
		return paramId;
	}

	public void setParamId(PK paramId) {
		this.paramId = paramId;
	}

	public List<PK> getParamIds() {
		return paramIds;
	}

	public void setParamIds(List<PK> paramIds) {
		this.paramIds = paramIds;
	}

	public Collection<Object> getParamForeignIds() {
		return paramForeignIds;
	}

	public void setParamForeignIds(Collection<Object> paramForeignIds) {
		this.paramForeignIds = paramForeignIds;
	}

	public Class<?> getParamForeignIdClass() {
		return paramForeignIdClass;
	}

	public void setParamForeignIdClass(Class<?> paramForeignIdClass) {
		this.paramForeignIdClass = paramForeignIdClass;
	}

	public String getParamForeignMethodName() {
		return paramForeignMethodName;
	}

	public void setParamForeignMethodName(String paramForeignMethodName) {
		this.paramForeignMethodName = paramForeignMethodName;
	}

	public Collection<Delete<PK>> getParamDeletes() {
		return paramDeletes;
	}

	public void setParamDeletes(Collection<Delete<PK>> paramDeletes) {
		this.paramDeletes = paramDeletes;
	}

	public EntityObjectVO<PK> getSingleResult() {
		return singleResult;
	}

	public void setSingleResult(EntityObjectVO<PK> singleResult) {
		this.singleResult = singleResult;
	}

	public void setPKResult(Object pk) {
		this.pkResult = pk;
	}

	public Object getPKResult() {
		return this.pkResult;
	}

	public List<EntityObjectVO<PK>> getMultiResult() {
		return multiResult;
	}

	public void setMultiResult(List<EntityObjectVO<PK>> multiResult) {
		this.multiResult = multiResult;
	}

	public List<PK> getIdsResult() {
		return idsResult;
	}

	public void setIdsResult(List<PK> idsResult) {
		this.idsResult = idsResult;
	}
	
	public DalCallResult getDalCallResult() {
		if (dalCallResult == null) {
			dalCallResult = new DalCallResult();
		}
		return dalCallResult;
	}

	public boolean isFailAfterBatch() {
		return failAfterBatch;
	}

	public void setFailAfterBatch(boolean failAfterBatch) {
		this.failAfterBatch = failAfterBatch;
	}

}
