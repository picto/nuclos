//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.ejb3;

import java.util.List;

import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.RelationDirection;
import org.nuclos.server.navigation.treenode.GenericObjectTreeNode.SystemRelationType;
import org.nuclos.server.navigation.treenode.TreeNode;

// @Local
public interface TreeNodeFacadeLocal {

	/**
	 * Note that user rights are ignored here.
	 * 
	 * §postcondition result != null
	 * 
	 * @param iGenericObjectId
	 * @param moduleId the module id
	 * @param iRelationId
	 * @param relationtype
	 * @param direction
	 * @param uidNode id for {@link EntityTreeViewVO}
	 * @param idRoot id for root {@link TreeNode}
	 * @return a new tree node for the generic object with the given id.
	 * @throws CommonFinderException if the object doesn't exist (anymore).
	 */
	GenericObjectTreeNode newGenericObjectTreeNode(
		Long iGenericObjectId, UID moduleId, Long iRelationId,
		SystemRelationType relationtype, RelationDirection direction, String customUsage, Long parentId, UID uidNode, Long idRoot)
		throws CommonFinderException;
	

	/**
	 * gets the list of sub nodes for a specific generic object tree node.
	 * Note that there is a specific method right on this method.
	 * 
	 * §postcondition result != null
	 * 
	 * @param node tree node of type generic object tree node
	 * @return list of sub nodes for given tree node
	 */
	List<TreeNode> getSubNodesIgnoreUser(GenericObjectTreeNode node);

}
