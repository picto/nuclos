//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * MasterDataReadableFieldsVisitor
 * 
 * filter visible fields for {@link MasterDataVO}
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 *
 */
public class MasterDataReadableFieldsVisitor implements ReadableFieldsVisitor {

	private final MasterDataVO<UID> mdVO;
	public MasterDataReadableFieldsVisitor(final MasterDataVO<UID> mdVO) {
		this.mdVO = mdVO;
	}
	@Override
	public void visit(Map<UID, Object> mpFields, UID language) {
		mpFields.putAll(mdVO.getFieldValues());
		
		if (mdVO.getDataLanguageMap() != null && 
				mdVO.getDataLanguageMap().getLanguageMap() != null) {
			Map<UID, DataLanguageLocalizedEntityEntry> languageMap = 
					mdVO.getDataLanguageMap().getLanguageMap();

			DataLanguageLocalizedEntityEntry dataLanguageLocalizedEntityEntry = 
					languageMap.get(language);
			
			for (UID field : mpFields.keySet()) {
				try {
					FieldMeta<?> fm = MetaProvider.getInstance().getEntityField(field);
					if (fm != null && fm.isLocalized() && !fm.isCalculated()) {
						UID langField = DataLanguageServerUtils.extractFieldUID(field);
						mpFields.put(field, dataLanguageLocalizedEntityEntry.getFieldValue(langField));
					}					
				} catch (IllegalArgumentException | CommonFatalException e) {
					Logger.getLogger(MasterDataReadableFieldsVisitor.class).debug(e.getMessage(), e);
				}
			}
		}		
	}

}
