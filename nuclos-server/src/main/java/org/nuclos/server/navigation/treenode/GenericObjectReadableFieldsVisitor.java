//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.navigation.treenode;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.security.Permission;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.genericobject.GenericObjectMetaDataCache;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;
import org.nuclos.server.i18n.language.data.DataLanguageServerUtils;

/**
 * GenericObjectReadableFieldsVisitor
 * 
 * filter visible fields for {@link GenericObjectWithDependantsVO}
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 *
 */
public class GenericObjectReadableFieldsVisitor implements
		ReadableFieldsVisitor {

	private final GenericObjectWithDependantsVO gowdVO;
	private final String username;
	private final UID mandator;

	public GenericObjectReadableFieldsVisitor(final GenericObjectWithDependantsVO gowdVO, final String username, UID mandator) {
		this.gowdVO = gowdVO;
		this.username = username;
		this.mandator = mandator;
	}
	@Override
	public void visit(Map<UID, Object> mpFields, UID language) {
		mpFields.putAll(getReadableAttributes(username, gowdVO, mandator));
		
		if (gowdVO.getDataLanguageMap() != null && 
				gowdVO.getDataLanguageMap().getLanguageMap() != null) {
			Map<UID, DataLanguageLocalizedEntityEntry> languageMap = 
					gowdVO.getDataLanguageMap().getLanguageMap();

			DataLanguageLocalizedEntityEntry dataLanguageLocalizedEntityEntry = 
					languageMap.get(language);
			
			for (UID field : mpFields.keySet()) {
				if (MetaProvider.getInstance().getEntityField(field).isLocalized() && 
						!MetaProvider.getInstance().getEntityField(field).isCalculated()) {
					UID langField = DataLanguageServerUtils.extractFieldUID(field);
					mpFields.put(field, dataLanguageLocalizedEntityEntry.getFieldValue(langField));
				}
			}
		}		
	}

	private Map<UID, Object> getReadableAttributes(String sUserName, GenericObjectWithDependantsVO gowdvo, UID mandator) {
		final String sResIfNoPermission = StringUtils.defaultIfNull(
				SpringApplicationContextHolder.getBean(ParameterProvider.class).getValue(ParameterProvider.KEY_BLUR_FILTER),"***");
		final Map<UID, Object> values = new HashMap<UID, Object>();
		for (DynamicAttributeVO att : gowdvo.getAttributes()) {
			final UID attname = att.getAttributeUID();
			if (isReadAllowedForAttribute(sUserName, attname, gowdvo, mandator)) {
				values.put(attname, att.getValue());
			}
			else {
				values.put(attname, sResIfNoPermission);
			}
		}
		return values;
	}
	
	/**
	 * check whether the data of the attribute is readable for current user
	 *
	 * @return true, if attribute data is readable, otherwise false
	 */
	public boolean isReadAllowedForAttribute(String sUserName, UID sKey, GenericObjectWithDependantsVO gowdvo, UID mandator) {
		final UID iAttributeGroupId = SpringApplicationContextHolder.getBean(GenericObjectMetaDataCache.class)
				.getAttribute(sKey).getAttributegroupUID();
		final Permission permission = SecurityCache.getInstance().getAttributeGroup(
				sUserName, gowdvo.getModule(), iAttributeGroupId, mandator).get(gowdvo.getStatus());

		return (permission == null) ? false : permission.includesReading();
	}
}
