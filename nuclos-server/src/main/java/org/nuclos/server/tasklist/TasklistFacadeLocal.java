package org.nuclos.server.tasklist;

import java.util.Collection;

import org.nuclos.common.tasklist.TasklistDefinition;

public interface TasklistFacadeLocal {

	Collection<TasklistDefinition> getUsersTasklists();

	void invalidateCaches();
}
