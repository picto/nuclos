//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report;

import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.DefaultReportVO;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("reportBoResolver")
@Scope("singleton")
public class ReportVOBoResolver {
	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.printout";
	private static final String DEFFAULT_ENTITY_PREFIX = "PO";
	private static final String DEFFAULT_ENTITY_POSTFIX = "PO";
	
	@Autowired
	private MetaProvider metaCache;
	
	public String getQualifiedName(DefaultReportVO model) {
		final String sPackage = getNucletPackage(E.REPORT, model.getId());
		final String unformatedEntity = validateJavaIdentifier(model.getName(), DEFFAULT_ENTITY_PREFIX);
		final String formatEntity = unformatedEntity.substring(0, 1).toUpperCase() + unformatedEntity.substring(1) + DEFFAULT_ENTITY_POSTFIX;
		final String qualifiedName = sPackage + "." + formatEntity;
		//final String filename = NuclosCodegeneratorUtils.printoutSource(sPackage, formatEntity).toString();
		//final String filename = sPackage.replace(".", File.separator) + File.separatorChar + formatEntity + ".java";
		return qualifiedName;
	}
	
	protected <PK> String getNucletPackage(EntityMeta<PK> meta, PK entityUID) {
		if (!E.REPORT.equals(meta)) {
			throw new IllegalStateException();
		}
		String retVal = DEFFAULT_PACKAGE_NUCLET;
		
		final EntityObjectVO<PK> eoEntity = NucletDalProvider.getInstance().getEntityObjectProcessor(meta).getByPrimaryKey(entityUID);
		if (eoEntity != null) {
			UID nucletUID = eoEntity.getFieldUid(E.REPORT.nuclet);
			if (nucletUID != null) {
				String sNucletPackage = metaCache.getNuclet(nucletUID).getFieldValue(E.NUCLET.packagefield);
				if (!StringUtils.looksEmpty(sNucletPackage)) {
					retVal = sNucletPackage;
				}
			}
		}
		return retVal;
	}
	
	public String validateJavaIdentifier(String value, String defaultPrefix) {
		String retVal = value;
		
		retVal = retVal.replace("ä", "ae");
		retVal = retVal.replace("ü", "ue");
		retVal = retVal.replace("ö", "oe");
		retVal = retVal.replace("Ä", "Ae");
		retVal = retVal.replace("Ü", "Ue");
		retVal = retVal.replace("Ö", "Oe");
		retVal = retVal.replace("ß", "ss");
		
		retVal = retVal.replaceAll("[^a-zA-Z0-9]+","");
		
		if (Character.isDigit(retVal.charAt(0))) 
			retVal = defaultPrefix + retVal;
			
		return retVal;
	}
}
