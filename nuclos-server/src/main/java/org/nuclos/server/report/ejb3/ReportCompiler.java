//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report.ejb3;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import org.apache.commons.lang.SerializationUtils;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common2.XMLUtils;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

public class ReportCompiler {
	
	public static final String CHARENCODING = "UTF-8";

	/**
	 * compiles a report xml definition (jasperreports)
	 * 
	 * @param sourceFileContent
	 *            report layout definition
	 * @return compiled jasper report
	 */
	public static JasperReport createJasperReport(byte[] sourceFileContent) {
		final String sReportXML;
		try {
			sReportXML = new String(sourceFileContent, CHARENCODING);
		}
		catch (UnsupportedEncodingException ex) {
			throw new NuclosFatalException(ex);
		}
		try {
			return JasperCompileManager.compileReport(new ByteArrayInputStream(sReportXML.getBytes(XMLUtils.getXMLEncoding(sReportXML))));
		}
		catch (JRException ex) {
			throw new NuclosFatalException(ex);
		}
		catch (UnsupportedEncodingException ex) {
			throw new NuclosFatalException(ex);
		}
	}
	
	/**
	 * 
	 * @param sourceFileContent
	 * @return
	 */
	public static ByteArrayCarrier compileReport(ByteArrayCarrier sourceFileContent) {
		return compileReport(sourceFileContent.getData());
	}
	
	/**
	 * 
	 * @param sourceFileContent
	 * @return
	 */
	public static ByteArrayCarrier compileReport(byte[] sourceFileContent) {
		return new ByteArrayCarrier(SerializationUtils.serialize(createJasperReport(sourceFileContent)));
	}
	
}
