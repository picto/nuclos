package org.nuclos.server.report;

import org.nuclos.api.ide.valueobject.SourceType;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorConstants;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler;
import org.springframework.stereotype.Component;

@Component
public class ReportObjectCompiler extends AbstractNuclosObjectCompiler {

	public ReportObjectCompiler() {
		super(SourceType.REPORT,
		      NuclosCodegeneratorConstants.REPORTJARFILE,
		      NuclosCodegeneratorConstants.REPORT_SRC_DIR_NAME);
	}

}
