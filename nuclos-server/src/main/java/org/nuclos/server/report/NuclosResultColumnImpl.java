package org.nuclos.server.report;

import org.nuclos.api.datasource.DatasourceColumn;

public class NuclosResultColumnImpl implements DatasourceColumn {

	private String sColumnName;
	private Class<?> sColumnType;
	
	public NuclosResultColumnImpl(String pColName, Class<?> pColType) {
		this.sColumnName = pColName;
		this.sColumnType = pColType;
	}
	
	@Override
	public String getName() {
		return this.sColumnName;
	}

	@Override
	public Class<?> getType() {
		return this.sColumnType;
	}

}
