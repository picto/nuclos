package org.nuclos.server.report;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.datasource.Datasource;
import org.nuclos.api.datasource.DatasourceColumn;
import org.nuclos.api.datasource.DatasourceResult;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.DatasourceService;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.server.common.ServerServiceLocator;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.i18n.language.data.DataLanguageCache;
import org.nuclos.server.report.ejb3.DatasourceFacadeLocal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("DatasourceServiceProvider")
public class ReportDatasourceServiceProvider implements DatasourceService{
	
	@Autowired
	DataLanguageCache dataLangCache;
	
	@Override
	public DatasourceResult run(Class<? extends Datasource> datasourceClass) throws BusinessException {
		return this.run(datasourceClass, null);
	}

	private List<DatasourceColumn> mapResultsColumns(List<ResultColumnVO> columns) {
		 List<DatasourceColumn> retVal = new ArrayList<DatasourceColumn>();
		 
		 if (columns != null && columns.size() > 0) {
			 for (ResultColumnVO col : columns) {
				 retVal.add(new NuclosResultColumnImpl(col.getColumnLabel(),col.getColumnClass()));
			 }
		 }
		 
		 return retVal;
	}
	
	@Override
	public DatasourceResult run(Class<? extends Datasource> datasourceClass,
			Map<String, Object> params) throws BusinessException {
	DatasourceResult retVal = null;
		
		DatasourceFacadeLocal facade = ServerServiceLocator.getInstance().getFacade(DatasourceFacadeLocal.class);
		
		try {
			
			if (params == null) {
				params = new HashMap<String, Object>();
			}
			
			Field declaredField = datasourceClass.getDeclaredField("uid");
			UID uid = (UID)declaredField.get(declaredField.getName());
			/*
			 *  quick fix, change null to -1 
			 *  TODO better change methode signature to Integer
			 */
			ResultVO queryResult = facade.executeQuery(uid, params, -1, dataLangCache.getLanguageToUse(), NucletDalProvider.getInstance().getAccessibleMandatorsOrigin());
			List<ResultColumnVO> columns = queryResult.getColumns();
			retVal = new NuclosResultImpl(queryResult.getRows(), mapResultsColumns(columns));
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
		return retVal;
	}

}
