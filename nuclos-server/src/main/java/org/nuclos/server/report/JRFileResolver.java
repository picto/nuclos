//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.report;

import java.io.File;
import java.util.List;
import java.util.Set;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableComparison;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.StringUtils;
import org.nuclos.server.common.MandatorUtils;
import org.nuclos.server.common.SecurityCache;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.genericobject.searchcondition.CollectableSearchExpression;
import org.nuclos.server.resource.ResourceCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.util.FileResolver;

/**
* File resolver for report execution.
* Tries to load files from resource folder.
* <br>
* <br>Created by Novabit Informationssysteme GmbH
* <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
*/
public class JRFileResolver implements FileResolver {
	
	private static final Logger LOG = LoggerFactory.getLogger(JRFileResolver.class);
	
	private final UID nucletUID;
	private final UID mandatorUID;

	public JRFileResolver(UID nucletUID, UID mandatorUID) {
		this.nucletUID = nucletUID;
		this.mandatorUID = mandatorUID;
	}

	@Override
	public File resolveFile(String arg0) {
		String filename = arg0.substring(LangUtils.max(arg0.lastIndexOf('\\'), arg0.lastIndexOf('/')) + 1);
		try {
			if (SecurityCache.getInstance().isMandatorPresent() && mandatorUID != null) {
				UID resourceUID = ResourceCache.getInstance().getResourceUID(nucletUID, filename);
				if (resourceUID != null) {
					CollectableComparison cond1 = SearchConditionUtils.newComparison(E.MANDATOR_RESOURCE.resource, ComparisonOperator.EQUAL, resourceUID);
					List<EntityObjectVO<UID>> mandatorResources = NucletDalProvider.getInstance().getEntityObjectProcessor(E.MANDATOR_RESOURCE).
							getBySearchExpression(new CollectableSearchExpression(cond1));
					Set<UID> accessibleMandators = SecurityCache.getInstance().getAccessibleMandators(mandatorUID);
					resourceUID = MandatorUtils.getByAccessibleMandators(mandatorResources, E.MANDATOR_RESOURCE.mandatorResource, E.MANDATOR_RESOURCE.mandator, accessibleMandators);
					if (resourceUID != null) {
						File file = ResourceCache.getInstance().getResourceFile(resourceUID);
						return file;
					}
				}
			}
			
			File file = ResourceCache.getInstance().getResourceFile(nucletUID, filename);
			return file;
		} catch (Exception ex) {
			String error = StringUtils.getParameterizedExceptionMessage("report.exception.filenotresolved", filename);
			LOG.error(error, ex);
			throw new NuclosFatalException(error);
		}
	}
}