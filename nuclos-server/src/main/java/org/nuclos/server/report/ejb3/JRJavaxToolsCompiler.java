//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.report.ejb3;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.server.customcode.codegenerator.NuclosJavaCompilerComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.design.JRAbstractMultiClassCompiler;


/**
 * A JasperReports Compiler adapter which uses the Java 6's
 * javax.tools.JavaCompiler API (but only the simple variant
 * with run).
 * 
 * Based on JRJdk13Compiler.
 */
public class JRJavaxToolsCompiler extends JRAbstractMultiClassCompiler
{
	private static final Logger LOG = LoggerFactory.getLogger(JRJavaxToolsCompiler.class);

	private static final int COMPILER_SUCCESS = 0;

	public JRJavaxToolsCompiler() {
	}

	@Override
	public String compileClasses(final File[] sourceFiles, String classpath) throws JRException
	{
		final boolean xinfo = LOG.isInfoEnabled();
		String errors = null;

		final JavaCompiler javac = NuclosJavaCompilerComponent.getJavaCompilerTool();
		if (javac == null) {
			throw new JRException("No registered system Java compiler found");
		}
		
		try {
			final String cp = SpringApplicationContextHolder.getBean(ReportFacadeLocal.class).getJRClasspath();
			LOG.info("classpath from args: {} real classpath={}", classpath, cp);

			final ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// NUCLOS-2831: Use _real_ classpath (because the value store in 
			// the system property jasper.reports.compile.class.path (set in ReportFacadeBean)
			// is wrong from time to time). (tp)
			if (cp != null) {
				classpath = cp;
			}
			final int result = javac.run(null, null, baos, getOptions(sourceFiles, classpath, javac));
			
			if (result != COMPILER_SUCCESS) {
				errors = baos.toString();
			}
			else {
				LOG.info("{}", baos);
			}
		} catch (Exception e) {
			final StringBuffer files = new StringBuffer();
			for (int i = 0; i < sourceFiles.length; ++i) {
				files.append(sourceFiles[i].getPath());
				files.append(' ');
			}
			throw new JRException("Error compiling report java source files : " + files, e);
		}
		return errors;
	}
	
	private String[] getOptions(File[] sourceFiles, String classpath, JavaCompiler jc) {
		final List<String> result = new ArrayList<String>();
		
		// set source and target (if possible)
		int args = jc.isSupportedOption("-source");
		if (args == 1) {
			result.add("-source");
			result.add("1.6");
		}
		args = jc.isSupportedOption("-target");
		if (args == 1) {
			result.add("-target");
			result.add("1.6");
		}
		
		// set classpath
		result.add("-classpath");
		result.add(classpath);
		
		// set source files
		for (int i = 0; i < sourceFiles.length; i++) {
			result.add(sourceFiles[i].getPath());
		}
		
		LOG.info("JasperReports JavaCompiler options: {}", result);

		return result.toArray(new String[result.size()]);
	}
}
