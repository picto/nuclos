//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.print.PrintService;

import org.apache.lucene.queryparser.flexible.core.util.StringUtils;
import org.nuclos.api.context.PrintResult;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.report.valueobject.OutputFormatTO;
import org.nuclos.common.report.valueobject.PrintPropertiesTO;
import org.nuclos.common.report.valueobject.ReportOutputVO;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common2.exception.CommonPrintException;
import org.nuclos.common2.exception.PrintoutException;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.masterdata.MasterDataWrapper;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;



/**
 * {@link LimitedPrintJobExecutionStrategy} executes one single {@link PrintJobWorker} at the same time
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class LimitedPrintJobExecutionStrategy implements
		PrintJobExecutionStrategy {
	/**
	 * worker for for {@link MultiPrintResultJob}
	 * 
	 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
	 *
	 * @param <T> {@link MultiPrintResultJob}
	 */
	  private static class Worker<T extends MultiPrintResultJob> implements Runnable {
	        private final BlockingQueue<T> workQueue;
	        private final String name; 
	        
	        public Worker(final String name) {
	            this.workQueue = new ArrayBlockingQueue<T>(10000,true);
	            this.name = name;
	        }
	        
	        public String getName() {
	        	return this.name;
	        }
	        
	        public void queue(final T job) {
	        	this.workQueue.add(job);
	        }

	        @Override
	        public void run() {
	            while (!Thread.currentThread().isInterrupted()) {
	                try {
	                    T item = workQueue.take();
	                    if (LOG.isDebugEnabled()) {
	                    	LOG.debug("worker {} processes new item {}", this, item.toString());
	                    }
	                    item.execute();
	                } catch (final InterruptedException ex) {
	                	LOG.error(ex.getMessage(),ex);
	                    Thread.currentThread().interrupt();
	                    break;
	                } catch (final PrintoutException ex) {
	                	LOG.error(ex.getMessage(),ex);
					}
	            }
	        }

			@Override
			public String toString() {
				return "Worker [name=" + name + "]";
			}
	    }
	  
	private static final UID DEFAULT_PRINTER = new UID("<default-printer>");
	private final static Logger LOG = LoggerFactory.getLogger(LimitedPrintJobExecutionStrategy.class);
	private ExecutorService executor;
	private final Map<PrintService, Worker<MultiPrintResultJob>> printerWorker;
	
	
	private PrintServiceRepository printServiceRepository;
	
	public LimitedPrintJobExecutionStrategy() {
		this.printerWorker = new ConcurrentHashMap<PrintService, Worker<MultiPrintResultJob>>();
	}

	@Autowired
	public void setPrintServiceRepository(
			PrintServiceRepository printServiceRepository) {
		this.printServiceRepository = printServiceRepository;
	}


	@PostConstruct
	public void postConstruct() {
		LOG.info("created new LimitedPrintJob");
		//@TODO provide property to let the user decide thre threadpool size
//		int processors = Runtime.getRuntime().availableProcessors();
//		LOG.info("decided to use a thread-pool size of {}", Math.max(processors,1));
//		this.executor = Executors.newFixedThreadPool(processors);
		// ARCD-1229
		this.executor = Executors.newCachedThreadPool();
	}

	@Override
	public <T extends PrintResult> void offerJob(List<T> printResults) throws PrintoutException {
	
		if (LOG.isDebugEnabled()) {
			LOG.debug("offer printresults {}", printResults);
		}
		final Map<PrintService, List<T>> dispatchedOutputFormats = dispatchPrinter(printResults);
		for (final Entry<PrintService, List<T>> entry : dispatchedOutputFormats.entrySet()){
			if (LOG.isTraceEnabled()) {
				LOG.trace("dispatch {} jobs to {}", entry.getValue().size(), entry.getKey());
			}
			submit(entry.getKey(), (List<PrintResult>) entry.getValue());
		}
		
	}
	
	protected synchronized void submit(final PrintService printer, final List<PrintResult> printResults) {
		final MultiPrintResultJob job = new MultiPrintResultJob(printResults, printer);
		if (!printerWorker.containsKey(printer)) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("schedule new PrintJob worker for {}", printer);
			}
			final Worker<MultiPrintResultJob> worker = new Worker<MultiPrintResultJob>(StringUtils.toString(printer));
			printerWorker.put(printer, worker);
			this.executor.execute(worker);
		} else {
			if (LOG.isDebugEnabled()) {
				LOG.debug("schedule new PrintJob for existing worker {}", printer);
			}
		}
		printerWorker.get(printer).queue(job);
	}
	
	
	
	public void stop(final boolean force) {
		LOG.info("force stop: {}", force);
		if (!force) {
			this.executor.shutdown();
			try {
				this.executor.awaitTermination(10L, TimeUnit.SECONDS);
			} catch (final InterruptedException ex) {
				LOG.error("scheduler was interrupted " + ex.getMessage(), ex);
			}
		} else {
			for (final Runnable runnable : this.executor.shutdownNow()) {
				LOG.warn("job {} was canceled due to shutdown signal", runnable);
			}
		}
	}

	public void dispose() {
		if (LOG.isDebugEnabled()) {
			LOG.debug("dispose LimitedPrintJobExecutionStrategy");
		}
		stop(false);
	}
	
	private <T extends PrintResult>  Map<PrintService, List<T>> dispatchPrinter(final List<T> printResults) throws PrintoutException {
		final Map<PrintService, List<T>> mpResult = new ConcurrentHashMap<PrintService, List<T>>();
		
		for (final T result : printResults) {
			final PrintProperties properties = result.getOutputFormat().getProperties();
			Destination destination = null;
			if (result.getOutputFormat() != null) {
				if (result.getOutputFormat() instanceof OutputFormatTO) {
					OutputFormatTO formatTO = (OutputFormatTO) result.getOutputFormat();
					destination = formatTO.getDestination();
				}
				if (destination == null && result.getOutputFormat().getId() != null) {
					// org.nuclos.api.provider.FileProvider.print(NuclosFile) has no output format
					// TODO cache outputFormats
					final EntityObjectVO<UID> reportOutput = 
							NucletDalProvider.getInstance().getEntityObjectProcessor(E.REPORTOUTPUT).getByPrimaryKey((UID) result.getOutputFormat().getId());
					final ReportOutputVO reportOutputVO = MasterDataWrapper.getReportOutputVO(new MasterDataVO<UID>(reportOutput, true));
					destination = reportOutputVO.getDestination();
				}
			}
			if (destination != null) {
				if (destination == Destination.DEFAULT_PRINTER_CLIENT ||
					destination == Destination.PRINTER_CLIENT ||
					destination == Destination.FILE ||
					destination == Destination.SCREEN) {
					continue;
				}
			}
			PrintService printer;
			try {
				printer = lookupService(properties);
			} catch (CommonPrintException e) {
				throw new PrintoutException(null, result.getOutputFormat(), e);
			}
			List<T> printResultsDispatched = null;
			if (mpResult.containsKey(printer)) {
				printResultsDispatched = mpResult.get(printer);
			} else {
				printResultsDispatched = new ArrayList<T>();
				mpResult.put(printer, printResultsDispatched);
			}
			printResultsDispatched.add(result);
		}
		
		if (LOG.isDebugEnabled()) {
			for (final PrintService ps : mpResult.keySet()) {
				LOG.debug("Printer {} gets {} outputFormats", ps.getName(), mpResult.get(ps).size());
			}
		}
		return mpResult;
		
	}
	
	private PrintService lookupService(final PrintProperties properties) throws CommonPrintException {

		final PrintService printService;
		if (properties instanceof PrintPropertiesTO && ((PrintPropertiesTO)properties).getPrintService() != null) {
			
			// print service is set from file service provider directly
			printService = ((PrintPropertiesTO)properties).getPrintService();
		} else {
			
			// lookup certain print service by id (printServiceId == null --> returns system default)
			printService = printServiceRepository.lookupPrintService((UID) properties.getPrintServiceId());
		} 

		return printService;
	}

}
