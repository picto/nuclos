//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.printservice;

import java.util.List;

import org.nuclos.api.context.PrintResult;
import org.nuclos.common2.exception.PrintoutException;
import org.nuclos.server.printservice.exception.PrintSpoolerException;

/**
 * {@link PrintSpooler} for {@link PrintJob} management
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface PrintSpooler {

	/**
	 * {@link Mode} Mode
	 * 
	 * describes the current state of the {@link PrintSpooler}
	 * 
	 */
	public static enum Mode {
		STARTUP,
		RUNNING,
		MAINTENANCE
	}

	/**
	 * schedule new list of {@link PrintResult}
	 * 
	 * @param printResults list of {@link PrintResult}
	 * @throws PrintSpoolerException
	 */
	public <T extends PrintResult> void schedule(final List<T> printResults , final PrintSpoolerScheduleContext context) throws PrintoutException;

	/**
	 * cancel scheduled {@link PrintJob}
	 * 
	 * @param printJob {@link PrintJob}
	 * @return true if printJob could be canceled successfully, 
	 * false if printJob was not canceled(might be in {@link PrintJob.Mode} Running)
	 */
	public <T extends PrintJob> boolean cancel(final T printJob);

	/**
	 * get currently selected spooler {@link Mode}
	 * 
	 * @return {@link Mode}
	 */
	public Mode getMode();

	/**
	 * switch to {@link Mode}
	 * 
	 * @param mode	{@link Mode}
	 * @throws PrintSpoolerException
	 */
	public void switchToMode(Mode mode) throws PrintSpoolerException;
	
	/**
	 * get {@link PrintJobExecutionStrategy}
	 * 
	 * @return {@link PrintJobExecutionStrategy}
	 */
	public PrintJobExecutionStrategy getExecutionStrategy();
}