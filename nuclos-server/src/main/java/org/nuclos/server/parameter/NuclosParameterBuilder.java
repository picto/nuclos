package org.nuclos.server.parameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.api.parameter.NucletParameter;
import org.nuclos.api.parameter.SystemParameter;
import org.nuclos.common.E;
import org.nuclos.common.NuclosEntityValidator;
import org.nuclos.common.UID;
import org.nuclos.common.collection.multimap.MultiListHashMap;
import org.nuclos.common.collection.multimap.MultiListMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.customcode.codegenerator.NuclosCodegeneratorUtils;
import org.nuclos.server.dal.provider.NucletDalProvider;
import org.nuclos.server.nbo.AbstractNuclosObjectCompiler.NuclosBusinessJavaSource;
import org.nuclos.server.nbo.NuclosObjectBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("nuclosParameterObjectBuilder")
public class NuclosParameterBuilder extends NuclosObjectBuilder {

	private static final String DEFFAULT_PACKAGE_NUCLOS = "org.nuclos.parameter";
	private static final String DEFFAULT_PACKAGE_NUCLET = "org.nuclet.parameter";
	private static final String SYSTEMPARAMETER_JAR_FILENAME = "NuclosSystemParameter";
		
	@Autowired
	NuclosParameterObjectCompiler compiler;
	
	@Autowired
	private MetaProvider provider;

	@Autowired
	private NucletDalProvider nucletDalProvider;

	public void createObjects() throws CommonBusinessException, InterruptedException {
		// converted NuclosBusinessJavaSource
		final List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();

		// create systemparameter
		busiJavaSource.addAll(createSystemParameter());

		// create nucletparameter
		busiJavaSource.addAll(createNucletParameter());

		compiler.compileSourcesAndJar(busiJavaSource);
	}

	private Collection<? extends NuclosBusinessJavaSource> createNucletParameter()
		throws InterruptedException {
		List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();

		MultiListMap<UID, EntityObjectVO<UID>> nucletParameterMap = new MultiListHashMap<>();
		for (EntityObjectVO<UID> nucparam : nucletDalProvider.getEntityObjectProcessor(
			E.NUCLETPARAMETER).getAll()) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			nucletParameterMap.addValue(nucparam.getFieldUid(E.NUCLETPARAMETER.nuclet), nucparam);
		}

		for (EntityObjectVO<UID> nuclet : provider.getNuclets()) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			final String classname = NuclosEntityValidator.escapeJavaIdentifier(
					nuclet.getFieldValue(E.NUCLET.name), "N") + "NucletParameter";
			final String pkg = nuclet.getFieldValue(E.NUCLET.packagefield) != null ?
					nuclet.getFieldValue(E.NUCLET.packagefield) : DEFFAULT_PACKAGE_NUCLET;
			final String qname = pkg + "." + classname;
			final String sourceFilename = NuclosCodegeneratorUtils.parameterSource(
					pkg, classname).toString();
			final String content = createNucletParamsJavaSource(nuclet, pkg, classname, nucletParameterMap);

			if (content != null) {
				busiJavaSource.add(
						new NuclosBusinessJavaSource(qname, sourceFilename, content, true));
			}
		}
		return busiJavaSource;
	}

	private String createNucletParamsJavaSource(
		EntityObjectVO<UID> nuclet,
		String pkg,
		String filename,
		MultiListMap<UID, EntityObjectVO<UID>> nucletParameterMap) throws InterruptedException {

		StringBuilder content = new StringBuilder();

		List<EntityObjectVO<UID>> results = nucletParameterMap.getValues(nuclet.getPrimaryKey());

		if (results.size() > 0) {

			content.append("package ").append(pkg).append(";\n\n");
			content.append("import ").append(NucletParameter.class.getCanonicalName())
				.append(";\n\n");
			content.append("public class ").append(filename).append(" {\n\n");

			for (EntityObjectVO<UID> eo : results) {
				if (Thread.currentThread().isInterrupted()) {
					throw new InterruptedException();
				}
				content.append("\tpublic static final ")
					.append(NucletParameter.class.getSimpleName()).append(" ")
					.append(NuclosEntityValidator.escapeJavaIdentifier(eo.getFieldValue(E.NUCLETPARAMETER.name), "P"))
					.append(" = new ").append(NucletParameter.class.getSimpleName()).append("(\"")
					.append(eo.getPrimaryKey().getString()).append("\");\n");
			}

			content.append("\n}");
		}

		return content.length() == 0 ? null : content.toString();
	}

	private Collection<? extends NuclosBusinessJavaSource> createSystemParameter()
		throws InterruptedException {
		final List<NuclosBusinessJavaSource> busiJavaSource = new ArrayList<>();
		final String pkg = getNucletPackage();
		final String entity = SYSTEMPARAMETER_JAR_FILENAME;
		final String qname = pkg + "." + entity;
		final String filename = NuclosCodegeneratorUtils.parameterSource(pkg, entity).toString();

		busiJavaSource.add(
				new NuclosBusinessJavaSource(qname, filename, createSystemParamsJavaSource(), true));
		return busiJavaSource;
	}

	private String createSystemParamsJavaSource() throws InterruptedException {

		List<EntityObjectVO<UID>> results =
			nucletDalProvider.getEntityObjectProcessor(E.PARAMETER).getAll();

		StringBuilder builder = new StringBuilder();

		builder.append("package ").append(getNucletPackage()).append(";\n\n");
		builder.append("import ").append(SystemParameter.class.getCanonicalName()).append(";\n\n");
		builder.append("public class " + SYSTEMPARAMETER_JAR_FILENAME + " {\n\n");

		for (EntityObjectVO<UID> eo : results) {
			if (Thread.currentThread().isInterrupted()) {
				throw new InterruptedException();
			}
			String paramName = NuclosEntityValidator.escapeJavaIdentifier(eo.getFieldValue(E.PARAMETER.name), "P");
			builder.append("\tpublic static final SystemParameter ").append(paramName)
				.append(" = new SystemParameter(\"").append(eo.getPrimaryKey().getString())
				.append("\");\n");
		}

		builder.append("\n}");

		return builder.toString();
	}

	private String getNucletPackage() {
		return DEFFAULT_PACKAGE_NUCLOS;
	}

}
