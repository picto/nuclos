//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.server.nbo;

import java.util.ArrayList;
import java.util.Collection;

import org.nuclos.api.NuclosImage;
import org.nuclos.api.businessobject.BusinessObject;
import org.nuclos.api.businessobject.facade.LogicalDeletable;
import org.nuclos.api.common.NuclosFile;
import org.nuclos.api.exception.BusinessException;
import org.nuclos.api.service.BusinessObjectService;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonValidationException;
import org.nuclos.server.common.MetaProvider;
import org.nuclos.server.common.ServerParameterProvider;
import org.nuclos.server.dal.DalSupportForGO;
import org.nuclos.server.genericobject.ejb3.GenericObjectFacadeLocal;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.genericobject.valueobject.GenericObjectWithDependantsVO;
import org.nuclos.server.masterdata.ejb3.MasterDataFacadeLocal;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * This class provides several services for general BO handling like
 * inserting, modifying or deleting existing or new BusinessObjects
 * via Nuclos DAL-Provider
 * 
 * This class is usually called by API
 * 
 * @author reichama
 *
 */
@Component("businessObjectServiceProvider")
public class BusinessObjectServiceProvider implements BusinessObjectService {

	private MasterDataFacadeLocal mdFacade;
	private GenericObjectFacadeLocal goFacade;
	
	@Autowired
	public void setMasterDataFacadeLocal(MasterDataFacadeLocal facade) {
		this.mdFacade = facade;
	}
	
	@Autowired
	public void setGenericObjectFacadeLocal(GenericObjectFacadeLocal facade) {
		this.goFacade = facade;
	}

	@Autowired
	private MetaProvider metaProvider;
	
	@Override
	public <T extends BusinessObject> Long insert(T type)
			throws BusinessException {
		
		Long retVal = null;
		
		// Facade is needed
		if (this.mdFacade == null)
			throw new BusinessException("Cannot find EntityObjectFacade");
		
		// EO must be new
		if (!((AbstractBusinessObject<Long>) type).isInsert())
			throw new BusinessException("BusinessObject is not new and cannot be inserted.");
			
		// Insert
		try {
			EntityObjectVO<Long> eo = EOBOBridge.getEO((AbstractBusinessObject<Long>) type);

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return null;
			}
			
			if (MetaProvider.getInstance().getEntity(eo.getDalEntity()).isStateModel()) {
				
				GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eo);
				GenericObjectWithDependantsVO goInserted = 
						new GenericObjectWithDependantsVO(go, eo.getDependents() != null ? eo.getDependents() : null, eo.getDataLanguageMap());
				go = goFacade.create(goInserted, ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				eo = (EntityObjectVO<Long>) DalSupportForGO.wrapGenericObjectVO(go);
				eo.setDependents(goInserted.getDependents());
			}
			else {
				MasterDataVO<Long> masterDataVO = new MasterDataVO<Long>(eo, false);
				masterDataVO = this.mdFacade.create(masterDataVO, null);
				eo = masterDataVO.getEntityObject();
			}
		
			EOBOBridge.setEO((AbstractBusinessObject<Long>) type, eo);
			retVal = (Long) type.getId();	
		} catch (Exception e) {
			if (e instanceof CommonValidationException) {
				throw new BusinessException (e.toString(), e);
			}
			throw new BusinessException(e.getMessage(), e);
		}
		return retVal;
	}

	@Override
	public <T extends BusinessObject> void update(T type) throws BusinessException {
		// Facade is needed
		
		if (this.mdFacade == null)
			throw new BusinessException("Cannot find EntityObjectFacade");

		@SuppressWarnings("unchecked")
		boolean allowUpdate = ((AbstractBusinessObject<Long>) type).isUpdate()
				|| ((AbstractBusinessObject<Long>) type).isUnchanged()
				|| (((AbstractBusinessObject<Long>) type).isInsert() && ((AbstractBusinessObject<Long>) type).getId() != null);

		// EO must be in a modified state
		if (!allowUpdate)
			throw new BusinessException("BusinessObject is not in a modified state and cannot be updated.");

		// Update EO
		try {
			
			EntityObjectVO eo = EOBOBridge.getEO((AbstractBusinessObject) type);

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return;
			}
			
			if (MetaProvider.getInstance().getEntity(eo.getDalEntity()).isStateModel()) {
				GenericObjectVO go = DalSupportForGO.getGenericObjectVO((EntityObjectVO<Long>) eo);
				GenericObjectWithDependantsVO modify = goFacade.modify(new GenericObjectWithDependantsVO(
						go, eo.getDependents() != null ? eo.getDependents() : null, eo.getDataLanguageMap()), 
						ServerParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));
				eo = (EntityObjectVO<Long>) DalSupportForGO.wrapGenericObjectVO(modify);
				eo.setDependents(modify.getDependents());
			}
			else {
				MasterDataVO masterDataVO = new MasterDataVO(eo, false);
				masterDataVO.setPrimaryKey(this.mdFacade.modify(masterDataVO, null));
				eo = masterDataVO.getEntityObject();
			}
			EOBOBridge.setEO((AbstractBusinessObject) type, eo);
		 
		} catch (Exception e) {
			if (e instanceof CommonValidationException) {
				throw new BusinessException (e.toString(), e);
			}
			throw new BusinessException (e.getMessage(), e);
		}	
	
	}

	private <PK, T extends BusinessObject> void delete(T type, boolean deleteLogical)
			throws BusinessException {
		// Facade is needed
		if (this.mdFacade == null)
			throw new BusinessException("Cannot find EntityObjectFacade");
		
		// Set flag to removed
		((EntityObjectVO<?>)((AbstractBusinessObject<PK>) type).getEntityObjectVO()).flagRemove();
			
		// Remove EO
		try {
			EntityObjectVO<PK> eo = EOBOBridge.getEO((AbstractBusinessObject<PK>) type);

			if (!metaProvider.checkEntity(eo.getDalEntity())) {
				// not integrated optional point
				return;
			}
			
			if (MetaProvider.getInstance().getEntity(eo.getDalEntity()).isStateModel()) {
				this.goFacade.remove(eo.getDalEntity(), (Long)eo.getPrimaryKey(), !deleteLogical, null);
				
			}
			else {
				this.mdFacade.remove(eo.getDalEntity(), eo.getPrimaryKey(), true);	
				
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage(), e);
		}	
	}

	@Override
	public <T extends BusinessObject> Collection<Long> insertAll(Collection<T> type)
			throws BusinessException {

		Collection<Long> retVal = new ArrayList<Long>();
		
		if (type != null) {
			for (T t : type) {
				retVal.add(insert(t));
			}
		}
		
		return retVal;
	}

	@Override
	public <T extends BusinessObject> void updateAll(Collection<T> type)
			throws BusinessException {
		
		if (type != null) {
			for (T t : type) {
				update(t);
			}
		}
	}

	@Override
	public <T extends BusinessObject> void deleteAll(Collection<T> types)
			throws BusinessException {
		if (types != null) {
			for (T t : types) {
				delete(t, false);
			}
		}
	}

	@Override
	public <PK, T extends LogicalDeletable> void deleteLogical(T type)
			throws BusinessException {
		this.delete(type, true);
	}

	@Override
	public <PK, T extends LogicalDeletable> void deleteLogicalAll(Collection<T> types)
			throws BusinessException {
		if (types != null) {
			for (T t : types) {
				delete(t, true);
			}			
		}
	}

	@Override
	public <T extends BusinessObject> void delete(T type)
			throws BusinessException {
		this.delete(type, false);
	}

	@Override
	public NuclosFile newFile(String name, byte[] content) throws BusinessException {
		
		if (name == null)
			throw new BusinessException("filename must not be null");
		
		if (content == null)
			throw new BusinessException("content must not be null");
		
		return new org.nuclos.common.NuclosFile(name, content);
	}

	@Override
	public NuclosImage newImage(String name, byte[] content) throws BusinessException {
		
		if (name == null)
			throw new BusinessException("filename must not be null");
		
		if (content == null)
			throw new BusinessException("content must not be null");
		
		return new org.nuclos.common.NuclosImage(name, content);
	}
	
	
}
