//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common.collect.collectable.searchcondition.SearchConditionToPredicateVisitor;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Predicate;
import org.nuclos.common.collection.PredicateUtils;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class SystemDataCache {

	private final EntityMeta<?> entity;
	private final Map<Object, MasterDataVO<UID>> elements;

	public SystemDataCache(EntityMeta<?> entity) {
		this.entity = entity;
		this.elements = new HashMap<Object, MasterDataVO<UID>>();
	}

	protected void addAll(Iterable<MasterDataVO<UID>> mdvos) {
		for (MasterDataVO<UID> mdvo : mdvos) {
			Object id = mdvo.getId();
			if (id == null || elements.containsKey(id))
				throw new IllegalArgumentException("Invalid/duplicate id " + id + " for system entity " + entity.getEntityName());
			elements.put(id, mdvo);
		}
	}

	public List<MasterDataVO<UID>> getAll() {
		List<MasterDataVO<UID>> result = new ArrayList<MasterDataVO<UID>>(elements.values());
		Collections.sort(result, new Comparator<MasterDataVO<UID>>() {
			@Override
			public int compare(MasterDataVO<UID> o1, MasterDataVO<UID> o2) {
				return LangUtils.compare(o1.getId(), o2.getId());
			}});
		return Collections.unmodifiableList(result);
	}

	public Set<Object> getAllIds() {
		return Collections.unmodifiableSet(elements.keySet());
	}

	public MasterDataVO<UID> getById(Object id) {
		return elements.get(id);
	}

	public MasterDataVO<UID> findVO(UID fieldUID, Object value) {
		return CollectionUtils.findFirst(elements.values(), new FieldEqualsPredicate(fieldUID, value));
	}

	public MasterDataVO<UID> findVO(UID fieldUID1, Object value1, Object...opt) {
		return CollectionUtils.findFirst(elements.values(), makeFieldEqualsPredicate(fieldUID1, value1, opt));
	}

	public List<MasterDataVO<UID>> findAllVO(UID fieldUID1, Object value1) {
		return findAllVO(new FieldEqualsPredicate(fieldUID1, value1));
	}

	public List<MasterDataVO<UID>> findAllVO(UID fieldUID1, Object value1, Object...opt) {
		return findAllVO(makeFieldEqualsPredicate(fieldUID1, value1, opt));
	}

	private static Predicate<MasterDataVO<UID>> makeFieldEqualsPredicate(UID fieldUID, Object value, Object[] opt) {
		Predicate<MasterDataVO<UID>>[] predicates = new Predicate[1 + (opt.length / 2)];
		predicates[0] = new FieldEqualsPredicate(fieldUID, value);
		for (int i = 0, k = 1; i < opt.length; i += 2) {
			predicates[k++] = new FieldEqualsPredicate((UID) opt[i], opt[i + 1]);
		}
		return PredicateUtils.and(predicates);
	}

	public List<MasterDataVO<UID>> findAllVOIn(UID fieldUID1, Collection<?> col1) {
		return findAllVO(new FieldInPredicate(fieldUID1, col1));
	}

	// TODO: clone to restrict manipulation
	public List<MasterDataVO<UID>> findAllVO(Predicate<MasterDataVO<UID>> predicate) {
		return CollectionUtils.<MasterDataVO<UID>>applyFilter(elements.values(), predicate);
	}

	public List<MasterDataVO<UID>> findAllVO(CollectableSearchCondition cond) {
		if (cond == null)
			return getAll();
		return findAllVO(cond.accept(new SearchConditionToPredicateVisitor()));
	}
	
	private static class FieldInPredicate implements Predicate<MasterDataVO<UID>> {

		private final UID	fieldUID;
		private final Collection<?> values;

		public FieldInPredicate(UID fieldUID, Collection<?> values) {
			this.fieldUID = fieldUID;
			this.values = values;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return values.contains(mdvo.getFieldValue(fieldUID));
		}
	}
	
	private static class FieldEqualsPredicate implements Predicate<MasterDataVO<UID>> {

		private final UID	fieldUID;
		private final Object value;

		public FieldEqualsPredicate(UID fieldUID, Object value) {
			this.fieldUID = fieldUID;
			this.value = value;
		}

		@Override
		public boolean evaluate(MasterDataVO<UID> mdvo) {
			return ObjectUtils.equals(mdvo.getFieldValue(fieldUID), value);
		}
	}
}
