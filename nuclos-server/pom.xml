<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<parent>
		<groupId>org.nuclos</groupId>
		<artifactId>nuclos</artifactId>
		<version>4.20.0-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	<modelVersion>4.0.0</modelVersion>

	<artifactId>nuclos-server</artifactId>
	<packaging>jar</packaging>

	<name>nuclos-server</name>
	<url>http://www.nuclos.de/</url>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<nuclos.javadoc.prefix>../../../</nuclos.javadoc.prefix>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-rigid-server</artifactId>
			<version>${project.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-common</artifactId>
			<version>${project.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-layout-transformation</artifactId>
			<version>${project.version}</version>
		</dependency>
		<!-- SLF4J Bridge
  				If existing components use SLF4J and you want to have this logging routed 
  				to Log4j 2, then add the following but do not remove any SLF4J dependencies. -->
  		<!-- dependency>
  			<groupId>org.apache.logging.log4j</groupId>
   			<artifactId>log4j-slf4j-impl</artifactId>
 		</dependency-->
 		<dependency>
  			<!-- Web Servlet Support
  				In order to properly support and handle the ClassLoader environment and container lifecycle 
  				of a web application, an additional module is required. This module is only required at runtime. -->
   			<groupId>org.apache.logging.log4j</groupId>
   			<artifactId>log4j-web</artifactId>
   			<scope>runtime</scope>
  		</dependency>
		<dependency>
			<groupId>org.apache.activemq</groupId>
			<artifactId>activemq-broker</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.xbean</groupId>
			<artifactId>xbean-spring</artifactId>
		</dependency>
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>javax.servlet-api</artifactId>
			<version>3.1.0</version>
			<scope>provided</scope>
		</dependency>

		<dependency>
			<groupId>org.antlr</groupId>
			<artifactId>antlr</artifactId>
			<version>3.4</version>
		</dependency>
		<dependency>
			<groupId>org.aspectj</groupId>
			<artifactId>aspectjweaver</artifactId>
		</dependency>
		<dependency>
			<groupId>com.google.code.javaparser</groupId>
			<artifactId>javaparser</artifactId>
			<version>1.0.11</version>
		</dependency>
		<!-- Installer checks for JDK, no need for a fallback compiler any more (NUCLOS-4230)
		<dependency>
			<groupId>de.novabit</groupId>
			<artifactId>langtools</artifactId>
			<version>b27</version>
		</dependency>
		-->
		<dependency>
			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz</artifactId>
			<version>2.2.0</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-instrument-tomcat</artifactId>
			<version>3.0.5.RELEASE</version>
			</dependency>
			<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-instrument</artifactId>
			<version>3.0.5.RELEASE</version>
			</dependency -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-orm</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-oxm</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.ldap</groupId>
			<artifactId>spring-ldap-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-ldap</artifactId>
			<version>${spring.security.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-aspects</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.ws</groupId>
			<artifactId>spring-ws-core</artifactId>
			<version>2.2.0.RELEASE</version>
			<exclusions>
				<exclusion>
					<groupId>javax.xml.stream</groupId>
					<artifactId>stax-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>cglib</groupId>
			<artifactId>cglib</artifactId>
			<!-- attention: version 3.0 and 3.1 need asm4, which is a no-go! (tp) -->
			<version>2.2.2</version>
		</dependency>
		<!-- only needed for activemq 5.5.0 and better
			<dependency>
			<groupId>org.eclipse.jetty</groupId>
			<artifactId>jetty-server</artifactId>
			<version>8.1.0.RC0</version>
			<!-
			At present this seems to be needed because of:

			java.lang.ClassNotFoundException: org.eclipse.jetty.server.Connector
			at org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoader.java:1676)
			at org.apache.catalina.loader.WebappClassLoader.loadClass(WebappClassLoader.java:1521)
			at org.nuclos.server.web.NuclosJMSBrokerTunnelServlet.init(NuclosJMSBrokerTunnelServlet.java:63)

			This stems from:
			transportConnector = new HttpTransportServer(new URI(url), myTransportFactory);

			Real MQ integration would be in the lines of:

			http://www.tomcatexpert.com/blog/2010/12/16/integrating-activemq-tomcat-using-local-jndi
			http://activemq.apache.org/tomcat.html

			(tp)
			->
			<exclusions>
			<exclusion>
			<artifactId>servlet-api</artifactId>
			<groupId>javax.servlet</groupId>
			</exclusion>
			<exclusion>
			<artifactId>servlet-api</artifactId>
			<groupId>org.mortbay.jetty</groupId>
			</exclusion>
			</exclusions>
			</dependency>
		-->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>2.3.11</version>
			<exclusions>
				<exclusion>
					<artifactId>geronimo-javamail_1.4_spec</artifactId>
					<groupId>org.apache.geronimo.specs</groupId>
				</exclusion>
				<exclusion>
					<artifactId>jaxb-api</artifactId>
					<groupId>java.xml.bind</groupId>
				</exclusion>
				<exclusion>
					<artifactId>jaxb-impl</artifactId>
					<groupId>java.xml.bind</groupId>
				</exclusion>
				<!-- exclusion>
					<artifactId>woodstox-core-asl</artifactId>
					<groupId>org.codehaus.woodstox</groupId>
				</exclusion -->
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>2.3.11</version>
			<exclusions>
				<exclusion>
					<artifactId>geronimo-javamail_1.4_spec</artifactId>
					<groupId>org.apache.geronimo.specs</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<!-- dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-common-utilities</artifactId>
			<version>2.3.11</version>
			<exclusions>
				<exclusion>
					<artifactId>woodstox-core-asl</artifactId>
					<groupId>org.codehaus.woodstox</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<!- force dependency management version of woodstox in cxf ->
		<dependency>
			<artifactId>woodstox-core-asl</artifactId>
			<groupId>org.codehaus.woodstox</groupId>
		</dependency -->
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-server-api</artifactId>
			<version>${nuclos.api.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.nuclos</groupId>
			<artifactId>nuclos-ide-api</artifactId>
			<version>${nuclos.api.version}</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<!--
			This is only needed because of
			https://jira.springsource.org/browse/SPR-6819
		-->
		<dependency>
			<groupId>javax.persistence</groupId>
			<artifactId>persistence-api</artifactId>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>javax.xml.bind</groupId>
			<artifactId>jaxb-api</artifactId>
			<version>2.2.11</version>
			<type>jar</type>
			<scope>compile</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-analyzers-common</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-queryparser</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.pdfbox</groupId>
			<artifactId>pdfbox</artifactId>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.containers</groupId>
			<artifactId>jersey-container-servlet</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-multipart</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-server</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.core</groupId>
			<artifactId>jersey-common</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
    		<artifactId>jersey-media-sse</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.jersey.media</groupId>
			<artifactId>jersey-media-json-jackson</artifactId>
			<version>${jersey.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.hk2</groupId>
			<artifactId>hk2-api</artifactId>
			<version>${hk2.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.hk2</groupId>
			<artifactId>hk2-locator</artifactId>
			<version>${hk2.version}</version>
		</dependency>
		<dependency>
			<groupId>org.glassfish.hk2.external</groupId>
			<artifactId>javax.inject</artifactId>
			<version>${hk2.version}</version>
		</dependency>
		<dependency>
		  <groupId>jonelo</groupId>
		  <artifactId>jacksum</artifactId>
		  <version>1.7.0</version>
		</dependency>
		
		<!-- testing -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.easymock</groupId>
			<artifactId>easymock</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${spring.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.kubek2k</groupId>
			<artifactId>springockito</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.kubek2k</groupId>
			<artifactId>springockito-annotations</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>fr.opensagres.xdocreport</groupId>
			<artifactId>fr.opensagres.xdocreport.document.docx</artifactId>
			<version>${xdocreport.version}</version>
		</dependency>
		<dependency>
			<groupId>fr.opensagres.xdocreport</groupId>
			<artifactId>org.apache.poi.xwpf.converter.pdf</artifactId>
			<version>${xdocreport.version}</version>
		</dependency>
		<dependency>
			<groupId>fr.opensagres.xdocreport</groupId>
			<artifactId>fr.opensagres.xdocreport.document.odt</artifactId>
			<version>${xdocreport.version}</version>
		</dependency>
		<dependency>
			<groupId>fr.opensagres.xdocreport</groupId>
			<artifactId>org.odftoolkit.odfdom.converter.pdf</artifactId>
			<version>${xdocreport.version}</version>
		</dependency>
		
		<dependency>
			<groupId>org.apache.axis2</groupId>
			<artifactId>axis2-kernel</artifactId>
			<version>${axis2.version}</version>
			<exclusions>
				<exclusion>
					<groupId>javax.servlet</groupId>
					<artifactId>servlet-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.axis2</groupId>
			<artifactId>axis2-adb</artifactId>
			<version>${axis2.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.axis2</groupId>
			<artifactId>axis2-transport-local</artifactId>
			<version>${axis2.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.axis2</groupId>
			<artifactId>axis2-transport-http</artifactId>
			<version>${axis2.version}</version>
		</dependency>
		<dependency>
			<groupId>net.sf.jsqlparser</groupId>
			<artifactId>jsqlparser</artifactId>
			<version>0.8.0</version>
		</dependency>

		<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
		</dependency>

		<dependency>
			<groupId>org.jsoup</groupId>
			<artifactId>jsoup</artifactId>
			<version>1.9.1</version>
		</dependency>
		<dependency>
			<groupId>com.icegreen</groupId>
			<artifactId>greenmail</artifactId>
			<version>1.5.1</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
			<version>2.3.23</version>
		</dependency>

		<dependency>
			<groupId>org.atmosphere</groupId>
			<artifactId>atmosphere-jersey</artifactId>
			<version>2.4.7</version>
			<exclusions>
				<exclusion>
					<groupId>com.sun.jersey</groupId>
					<artifactId>jersey-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.jersey</groupId>
					<artifactId>jersey-server</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.sun.jersey</groupId>
					<artifactId>jersey-servlet</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>

		<!-- PPTX not supported yet
		<dependency>
			<groupId>fr.opensagres.xdocreport</groupId>
			<artifactId>fr.opensagres.xdocreport.document.pptx</artifactId>
			<version>${xdocreport.version}</version>
		</dependency>
		-->
	</dependencies>

	<repositories>
		<repository>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<id>central</id>
			<name>Maven Repository Switchboard</name>
			<url>http://repo1.maven.org/maven2</url>
		</repository>
	</repositories>

	<profiles>
		<profile>
			<id>typescript</id>
			<build>
				<plugins>
					<plugin>
						<groupId>java2typescript</groupId>
						<artifactId>java2typescript-maven-plugin</artifactId>
						<version>0.3-SNAPSHOT</version>
						<configuration>
							<serviceClass>org.nuclos.server.rest.WebclientSystemParameter</serviceClass>
							<moduleName>WebclientSystemParameter</moduleName>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>
