package org.nuclos.installer.unpack;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.nuclos.installer.Constants;
import org.nuclos.installer.InstallException;
import org.nuclos.installer.database.PostgresService;
import org.nuclos.installer.mode.Installer;
import org.nuclos.installer.util.EnvironmentUtils;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class AbstractUnpackerTest {

	@Test
	public void testJDKLookup() {
		final TestUnpacker unpacker = new TestUnpacker();

		final String javaHome = unpacker.getDefaultValue(Constants.JAVA_HOME);

		assert StringUtils.isNotBlank(javaHome);
		assert EnvironmentUtils.isValidJDK(new File(javaHome));

	}

	class TestUnpacker extends AbstractUnpacker {

		@Override
		public boolean isPrivileged() {
			return false;
		}

		@Override
		public boolean canInstall() {
			return false;
		}

		@Override
		public boolean isProductRegistered() {
			return false;
		}

		@Override
		public boolean isPostgresBundled() {
			return false;
		}

		@Override
		public List<PostgresService> getPostgresServices() {
			return null;
		}

		@Override
		public void shutdown(final Installer cb) throws InstallException {

		}

		@Override
		public void startup(final Installer cb) throws InstallException {

		}

		@Override
		public void installPostgres(final Installer cb) throws InstallException {

		}

		@Override
		public void register(final Installer cb, final boolean systemlaunch) throws InstallException {

		}

		@Override
		public void unregister(final Installer cb) throws InstallException {

		}
	}
}