//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a stand-alone Java program used during the jenkins build for 
 * switching between PDFBox 1.8 and 2.0. 
 * <p>
 * It is here for convenience only
 * as it has nothing to do with the installer build in a strict sense.
 * <p>
 * This is the snippet of a jenkins shell build step that runs this 
 * program:
 * <pre><code>
cd $WORKSPACE/nuclos-installer/src/main/java
javac org/nuclos/installer/PDFBoxVersion.java
java org/nuclos/installer/PDFBoxVersion 2.0.3 $WORKSPACE/nuclos-installer
rm org/nuclos/installer/PDFBoxVersion.class
 * </code></pre>
 * <p>
 * ATTENTION: This class can't use ANY external library/Jar.
 * 
 * @author Maik Stueker
 */
public class PDFBoxVersion {
	
	private static String targetVersion = "2.0.3";
	private static String defaultPath = ".";
	
	public static void main(String[] args) throws IOException {
		if (args.length >= 2) {
			targetVersion = args[0];
			defaultPath = args[1];
		}
		
		replaceVersions();
	}

	private static void replaceVersions() {
		Map<String,String> replacement = new HashMap<String, String>();
		
		replacement.put("targetVersion", targetVersion);
		replacement.put("pdfbox.util", "pdfbox.text");
		replacement.put("fi", "new org.apache.pdfbox.io.RandomAccessBuffer(fi)");
		replacement.put("pdfbox.pdmodel.PDP", "pdfbox.printing.PDFP");
		replacement.put("PDPageable(document, printJob", "PDFPageable(document");
		
		replacement = Collections.unmodifiableMap(replacement);
		
		replace(new String[]{"../pom.xml:<artifactId>pdfbox</artifactId><version>#targetVersion#</version>"}, replacement, "#targ", "sion#");
		replace(new String[]{"../nuclos-common/src/main/java/org/nuclos/common2/DocumentFile.java:import org.apache.#pdfbox.util#.PDFTextStripper;"}, replacement, "#pdf", "util#");
		replace(new String[]{"../nuclos-common/src/main/java/org/nuclos/common2/DocumentFile.java:new PDFParser(#fi#);"}, replacement, "#f", "i#");
		replace(new String[]{"../nuclos-common/src/main/java/org/nuclos/common/report/print/PDFPrintJob.java:import org.apache.#pdfbox.pdmodel.PDP#ageable;"}, replacement, "#pdf", "PDP#");
		replace(new String[]{"../nuclos-common/src/main/java/org/nuclos/common/report/print/PDFPrintJob.java:printJob.setPageable(new #PDPageable(document, printJob#));"}, replacement, "#PDP", "Job#");
	}
	
	private static void replace(String[] occurences, Map<String,String> replace, String prefix, String suffix) {
		Map<String, List<String>> mapOcc = new HashMap<String, List<String>>();
		for (String s : occurences) {
			int n = s.indexOf(':');
			String sfile = defaultPath + "/" + s.substring(0, n);
			String spattern = s.substring(n + 1);
			List<String> lstOcc = mapOcc.get(sfile);
			if (lstOcc == null) mapOcc.put(sfile, lstOcc = new ArrayList<String>());
			lstOcc.add(spattern);
		}
		for (String sfile : mapOcc.keySet()) {
			final int lenSuffix = suffix.length();
			File file = new File(sfile);
			if (file.exists()) {
				List<String> patterns = mapOcc.get(sfile);
				int n = patterns.size();
				String[] pattern1 = new String[n];
				String[] replcment = new String[n];
				String[] pattern2 = new String[n];
				for (int i = 0; i < n; i++) {
					String p = patterns.get(i);
					int n1 = p.indexOf(prefix);
					int n2 = p.indexOf(suffix, n1 + 1);
					pattern1[i] = p.substring(0, n1);
					replcment[i] = p.substring(n1 + 1, n2 + lenSuffix - 1);
					pattern2[i] = p.substring(n2 + lenSuffix);
 				}
				replaceIfChanged(replace, file, pattern1, replcment, pattern2);
			}
		}
	}
	
	private static void replaceIfChanged(Map<String,String> replace, File file, String[] pattern1, String[] replcment, String[] pattern2) {
		try {
			boolean bChanged = false;
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			List<String> lstLines = new ArrayList<String>();
			String line;
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < pattern1.length; i++) {
					int n1 = line.indexOf(pattern1[i]);
					if (n1 < 0) continue;
					int n1start = n1 + pattern1[i].length();
					int n2 = line.indexOf(pattern2[i], n1start);
					if (n2 < 0) continue;
					final String toBeSubstituted = line.substring(n1start, n2);
					final String repl = replcment[i];
					final String toSubstitute = replace.get(repl);
					if (toSubstitute == null || toSubstitute.length() == 0) {
						throw new IllegalStateException("No value to substitue " + repl + " at " + (i + 1) + " of " + file);
					}
					if (!toSubstitute.equals(toBeSubstituted)) {
						line = line.substring(0, n1start) + toSubstitute + line.substring(n2);
						bChanged = true;
					}
				}
				lstLines.add(line);
			}
			br.close();
			fr.close();
			if (bChanged) {
				FileWriter fw = new FileWriter(file.getPath());
				for (String s : lstLines) {
					fw.write(s + "\n");
				}
				fw.close();
			}
		} catch (IOException ioe) {
			// Ok! (tp)
			ioe.printStackTrace();
		}
	}
	
}
