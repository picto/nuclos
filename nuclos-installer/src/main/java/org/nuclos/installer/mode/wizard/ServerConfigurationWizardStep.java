// Copyright (C) 2010 Novabit Informationssysteme GmbH
//
// This file is part of Nuclos.
//
// Nuclos is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nuclos is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Nuclos. If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer.mode.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.*;

import org.nuclos.installer.ConfigContext;
import org.nuclos.installer.Constants;
import org.nuclos.installer.L10n;
import org.nuclos.installer.unpack.GenericUnpacker;
import org.nuclos.installer.unpack.Unpacker;
import org.nuclos.installer.unpack.WindowsUnpacker;
import org.pietschy.wizard.InvalidStateException;

import info.clearthought.layout.TableLayout;

/**
 * Collect server configuration
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.nuclos.de">www.nuclos.de</a>
 */
public class ServerConfigurationWizardStep extends AbstractWizardStep implements Constants {

	private JTextField txtJavaHomePath = new JTextField();
	private JButton btnJavaHomeSelect = new JButton();
	
	private JButton btnDocumentHomePathSelect= new JButton();
	private JTextField txtDocumentHomePath = new JTextField();
	private JButton btnIndexSelect= new JButton();
	private JTextField txtIndexPath = new JTextField();
	
	private JTextField txtInstance = new JTextField();
	private JTextField txtPort = new JTextField();

	private ButtonGroup group = new ButtonGroup();
	private JRadioButton optHttp = new JRadioButton();
	private JRadioButton optHttps = new JRadioButton();

	private JTextField txtHttpsPort = new JTextField();
	private JTextField txtHttpsKeystoreFile = new JTextField();
	private JButton btnHttpsKeystoreSelect = new JButton();
	private JPasswordField txtKeystorePassword1 = new JPasswordField();
	private JPasswordField txtKeystorePassword2 = new JPasswordField();
	private JTextField txtShutdownPort = new JTextField();
	private JTextField txtMemory = new JTextField();
	
	private ButtonGroup group2 = new ButtonGroup();
	private JRadioButton optProduction = new JRadioButton();
	private JRadioButton optDevelopment = new JRadioButton();

	private JTextField txtDebugport = new JTextField();
	private JTextField txtJmxport = new JTextField();

	private JCheckBox chkLaunch = new JCheckBox();
	
	private JCheckBox chkAjp = new JCheckBox();
	private JTextField txtAjpport = new JTextField();

	private JCheckBox chkClusterMode = new JCheckBox();

	private static double layout[][] = { { 20.0, TableLayout.PREFERRED, TableLayout.FILL, 100.0 }, // Columns
			{ 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, TableLayout.FILL } }; // Rows

	public ServerConfigurationWizardStep() {
		super(L10n.getMessage("gui.wizard.server.title"), L10n.getMessage("gui.wizard.server.description"));

		TableLayout layout = new TableLayout(this.layout);
		layout.setVGap(5);
		layout.setHGap(5);
		this.setLayout(layout);

		JLabel label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.javahomepath.label"));
		this.add(label, "0,0, 1,0");

		this.txtJavaHomePath.getDocument().addDocumentListener(this);
		this.add(txtJavaHomePath, "2,0");

		btnJavaHomeSelect.setText(L10n.getMessage("filechooser.browse"));
		btnJavaHomeSelect.addActionListener(new SelectJavaHomeActionListener());
		this.add(btnJavaHomeSelect, "3,0");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.instance.label"));
		this.add(label, "0,1, 1,1");

		this.txtInstance.getDocument().addDocumentListener(this);
		this.add(txtInstance, "2,1");

		JLabel lblDocPath = new JLabel();
		lblDocPath.setText(L10n.getMessage("gui.wizard.server.dochomepath.label"));
		this.add(lblDocPath, "0,2, 1,2");

		this.txtDocumentHomePath.getDocument().addDocumentListener(this);
		this.add(txtDocumentHomePath, "2,2");

		btnDocumentHomePathSelect.setText(L10n.getMessage("filechooser.browse"));
		btnDocumentHomePathSelect.addActionListener(new SelectDocumentHomeActionListener());
		this.add(btnDocumentHomePathSelect, "3,2");

		JLabel lblIndexPath = new JLabel();
		lblIndexPath.setText(L10n.getMessage("gui.wizard.server.indexpath.label"));
		this.add(lblIndexPath, "0,3, 1,3");

		this.txtIndexPath.getDocument().addDocumentListener(this);
		this.add(txtIndexPath, "2,3");

		btnIndexSelect.setText(L10n.getMessage("filechooser.browse"));
		btnIndexSelect.addActionListener(new SelectIndexActionListener());
		this.add(btnIndexSelect, "3,3");
		
		
		optHttp.addActionListener(this);
		this.add(optHttp, "0,4");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.port.label"));
		this.add(label, "1,4");

		this.txtPort.getDocument().addDocumentListener(this);
		this.add(txtPort, "2,4");

		optHttps.addActionListener(this);
		this.add(optHttps, "0,5");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpsport.label"));
		this.add(label, "1,5");

		this.txtHttpsPort.getDocument().addDocumentListener(this);
		this.add(txtHttpsPort, "2,5");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorefile.label"));
		this.add(label, "1,6");

		this.txtHttpsKeystoreFile.getDocument().addDocumentListener(this);
		this.add(txtHttpsKeystoreFile, "2,6");

		btnHttpsKeystoreSelect.setText(L10n.getMessage("filechooser.browse"));
		btnHttpsKeystoreSelect.addActionListener(new SelectKeyStoreActionListener());
		this.add(btnHttpsKeystoreSelect, "3,6");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorepassword1.label"));
		this.add(label, "1,7");

		this.txtKeystorePassword1.getDocument().addDocumentListener(this);
		this.add(txtKeystorePassword1, "2,7");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.httpskeystorepassword1.label"));
		this.add(label, "1,8");

		this.txtKeystorePassword2.getDocument().addDocumentListener(this);
		this.add(txtKeystorePassword2, "2,8");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.shutdownport.label"));
		this.add(label, "0,9, 1,9");

		this.txtShutdownPort.getDocument().addDocumentListener(this);
		this.add(txtShutdownPort, "2,9");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.memory.label"));
		this.add(label, "0,10, 1,10");

		this.txtMemory.getDocument().addDocumentListener(this);
		this.add(txtMemory, "2,10");
		
		optProduction.addActionListener(this);
		this.add(optProduction, "0,11");
		
		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.production.label"));
		this.add(label, "1,11");
		
		optDevelopment.addActionListener(this);
		this.add(optDevelopment, "0,12");
		
		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.development.label"));
		this.add(label, "1,12");
		this.txtDebugport.getDocument().addDocumentListener(this);
		this.add(txtDebugport, "2,12");

		label = new JLabel();
		label.setText(L10n.getMessage("gui.wizard.server.development.jmxport.label"));
		this.add(label, "1,13");
		this.txtJmxport.getDocument().addDocumentListener(this);
		this.add(txtJmxport, "2,13");

		chkAjp.setText("AJP Port");
		this.chkAjp.addActionListener(this);
		this.chkAjp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				txtAjpport.setEnabled(chkAjp.isEnabled());
			}
		});
		this.add(chkAjp, "0,16 1,16");
		this.txtAjpport.getDocument().addDocumentListener(this);
		this.add(txtAjpport, "2,16");

		chkLaunch.setText(L10n.getMessage("gui.wizard.server.launch.label"));
		this.chkLaunch.addActionListener(this);
		this.add(chkLaunch, "0,14 2,14");
		
		chkClusterMode.setText(L10n.getMessage("gui.wizard.server.cluster.label"));
		this.chkClusterMode.addActionListener(this);
		this.add(chkClusterMode, "0,15 2,15");

		group.add(optHttp);
		group.add(optHttps);
		group2.add(optProduction);
		group2.add(optDevelopment);
	}

	@Override
	public void prepare() {
		this.txtInstance.setEnabled(!ConfigContext.isUpdate());
		modelToView(JAVA_HOME, txtJavaHomePath);
		modelToView(NUCLOS_INSTANCE, txtInstance);
		modelToView(DOCUMENT_PATH, txtDocumentHomePath);
		modelToView(INDEX_PATH, txtIndexPath);
		this.txtDocumentHomePath.setEnabled(!ConfigContext.isUpdate());
		this.btnDocumentHomePathSelect.setEnabled(!ConfigContext.isUpdate());
		
		optHttp.setSelected("true".equals(ConfigContext.getProperty(HTTP_ENABLED)));
		modelToView(HTTP_PORT, txtPort);
		optHttps.setSelected("true".equals(ConfigContext.getProperty(HTTPS_ENABLED)));
		modelToView(HTTPS_PORT, txtHttpsPort);
		modelToView(HTTPS_KEYSTORE_FILE, txtHttpsKeystoreFile);
		modelToView(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword1);
		modelToView(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword2);
		modelToView(SHUTDOWN_PORT, txtShutdownPort);
		modelToView(HEAP_SIZE, txtMemory);
		optProduction.setSelected("true".equals(ConfigContext.getProperty(PRODUCTION_ENABLED)));
		optDevelopment.setSelected("true".equals(ConfigContext.getProperty(DEVELOPMENT_ENABLED)));
		modelToView(DEBUG_PORT, txtDebugport);
		modelToView(JMX_PORT, txtJmxport);
		modelToView(LAUNCH_STARTUP, chkLaunch);
		modelToView(CLUSTER_MODE, chkClusterMode);
		modelToView(AJP_ENABLED, chkAjp);
		txtAjpport.setEnabled("true".equals(ConfigContext.getProperty(AJP_ENABLED)));
		modelToView(AJP_PORT, txtAjpport);
		
		this.chkLaunch.setEnabled(!(getModel().getUnpacker() instanceof GenericUnpacker));
	}

	@Override
	protected void updateState() {
		txtPort.setEnabled(optHttp.isSelected());
		txtHttpsPort.setEnabled(optHttps.isSelected());
		txtHttpsKeystoreFile.setEnabled(optHttps.isSelected());
		btnHttpsKeystoreSelect.setEnabled(optHttps.isSelected());
		txtKeystorePassword1.setEnabled(optHttps.isSelected());
		txtKeystorePassword2.setEnabled(optHttps.isSelected());
		txtDebugport.setEnabled(optDevelopment.isSelected());
		txtJmxport.setEnabled(optDevelopment.isSelected());
		setComplete(true);
	}

	@Override
	public void applyState() throws InvalidStateException {
		viewToModel(JAVA_HOME, txtJavaHomePath);
		viewToModel(NUCLOS_INSTANCE, txtInstance);
		viewToModel(DOCUMENT_PATH, txtDocumentHomePath);
		viewToModel(INDEX_PATH, txtIndexPath);
		ConfigContext.setProperty(HTTP_ENABLED, optHttp.isSelected() ? "true" : "false");
		viewToModel(HTTP_PORT, txtPort);
		ConfigContext.setProperty(HTTPS_ENABLED, optHttps.isSelected() ? "true" : "false");
		viewToModel(HTTPS_PORT, txtHttpsPort);
		viewToModel(HTTPS_KEYSTORE_FILE, txtHttpsKeystoreFile);
		if (optHttps.isSelected()) {
			validatePasswordEquality(txtKeystorePassword1, txtKeystorePassword2, "gui.wizard.server.httpskeystorepassword1.label");
		}
		viewToModel(HTTPS_KEYSTORE_PASSWORD, txtKeystorePassword1);
		viewToModel(SHUTDOWN_PORT, txtShutdownPort);
		viewToModel(HEAP_SIZE, txtMemory);
		ConfigContext.setProperty(PRODUCTION_ENABLED, optProduction.isSelected() ? "true" : "false");
		ConfigContext.setProperty(DEVELOPMENT_ENABLED, optDevelopment.isSelected() ? "true" : "false");
		viewToModel(DEBUG_PORT, txtDebugport);
		viewToModel(JMX_PORT, txtJmxport);
		viewToModel(LAUNCH_STARTUP, chkLaunch);
		viewToModel(CLUSTER_MODE, chkClusterMode);
		viewToModel(AJP_ENABLED, chkAjp);
		viewToModel(AJP_PORT, txtAjpport);
		
		Unpacker unpacker = getModel().getUnpacker();
		if (unpacker instanceof WindowsUnpacker) {
			try {
				String java = ConfigContext.getProperty(JAVA_HOME) + "/bin/java.exe";
				
				if (((WindowsUnpacker)unpacker).isAmd64() && !is64BitJava(new File(java)))
					throw new InvalidStateException(L10n.getMessage("validation.javahome.use64"));
				else if (!((WindowsUnpacker)unpacker).isAmd64() && is64BitJava(new File(java)))
					throw new InvalidStateException(L10n.getMessage("validation.javahome.use32"));
					
			} catch (Exception e) {
				throw new InvalidStateException(e.getMessage());
			}
		}
	}

	private class SelectDocumentHomeActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			File pathToUse = ConfigContext.getFileProperty(DOCUMENT_PATH);
			if (!pathToUse.exists()) {
				pathToUse = ConfigContext.getFileProperty(NUCLOS_HOME);
			}
			
			if (txtDocumentHomePath.getText() != null) {
				File docPathtemp = new File(txtDocumentHomePath.getText());
				if (pathToUse.exists() && docPathtemp.isDirectory()) {
					pathToUse = docPathtemp;
				}
			}
		
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(pathToUse);
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtDocumentHomePath.setText(file.getAbsolutePath());
			}
		}
	}
	
	private class SelectIndexActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			File pathToUse = ConfigContext.getFileProperty(INDEX_PATH);
			if (!pathToUse.exists()) {
				pathToUse = ConfigContext.getFileProperty(NUCLOS_HOME);
			}
			
			if (txtIndexPath.getText() != null) {
				File docPathtemp = new File(txtIndexPath.getText());
				if (pathToUse.exists() && docPathtemp.isDirectory()) {
					pathToUse = docPathtemp;
				}
			}
		
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(pathToUse);
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtIndexPath.setText(file.getAbsolutePath());
			}
		}
	}
	
	private class SelectJavaHomeActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtJavaHomePath.setText(file.getAbsolutePath());
			}
		}
	}

	private class SelectKeyStoreActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser();
			chooser.setCurrentDirectory(new java.io.File("."));
			chooser.setDialogTitle(L10n.getMessage("gui.wizard.path.filechooser.title"));
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

			int returnVal = chooser.showOpenDialog(ServerConfigurationWizardStep.this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = chooser.getSelectedFile();
				txtHttpsKeystoreFile.setText(file.getAbsolutePath());
			}
		}
	}

	public static boolean is64BitJava(File exe) throws IOException {
	    InputStream is = new FileInputStream(exe);
	    int magic = is.read() | is.read() << 8;
	    if(magic != 0x5A4D) 
	        throw new IOException("Invalid Exe");
	    for(int i = 0; i < 58; i++) is.read(); // skip until pe offset
	    int address = is.read() | is.read() << 8 | 
	         is.read() << 16 | is.read() << 24;
	    for(int i = 0; i < address - 60; i++) is.read(); // skip until pe header+4
	    int machineType = is.read() | is.read() << 8;
	    return machineType == 0x8664;
	}
}
