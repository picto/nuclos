package org.nuclos.installer;

public final class CheckJavaVersion {
	private CheckJavaVersion() {
		// Never invoked.
	}
	
	//Java 1.7 minimum Version
	public static final String minJavaVersion = "1.7.0";
	
	public static boolean checkVersion(String version) {
		return getVersionValue(version) >= getVersionValue(minJavaVersion);
	}
	
	static int getVersionValue(String version) {
		final int vi[] = parseVersion(version);
		return vi[0]*471101 + vi[1]*511 + vi[2];
	}
	
	static int[] parseVersion(String version) {
		final String v[] = version.split("[\\.\\-\\_]");
		final int vi[] = new int[3];
		for (int i = 0; i < 3; ++i) {
			try {
				vi[i] = Integer.parseInt(v[i]);
			}
			catch (NumberFormatException e) {
				// ignore
			}
		}
		return vi;
	}

	static void checkVersion(Runnable run) {
		final String version = System.getProperty("java.version");
		final int vi[] = parseVersion(version);

		if (vi[0] < 1) {
			bailOut();
		}
		else if (vi[0] == 1) {
			if (vi[1] < 7) {
				bailOut();
			}
		}
		else {
			// Java 2.x.x ???
			warn();
		}
		try {
			run.run();
		}
		catch (UnsupportedClassVersionError e) {
			handle(e);
		}
	}
	
	static void handle(UnsupportedClassVersionError e) {
		final String msg = e.toString();
		final String minVersion;
		if (msg.indexOf("51.0") >= 0) {
			minVersion = "Java SE 7";
		}
		else if (msg.indexOf("50.0") >= 0) {
			minVersion = "Java SE 6";
		}
		else if (msg.indexOf("49.0") >= 0) {
			minVersion = "Java SE 5 (Java 1.5)";
		}
		else {
			minVersion = "Java SE 6";			
		}
		System.err.println("You must use at least " + minVersion + ".");
		System.err.println("Sie müssen mindestens " + minVersion + " verwenden.");
		System.exit(-1);
	}

	static void bailOut() {
		System.err.println("You are using an outdated version of java. " +
				"Please install a more recent version.");
		System.err.println("Sie benutzen eine veraltete Java Version. " +
						"Bitte installieren Sie eine aktuelle Java Umgebung.");
		System.exit(-1);
	}

	static void warn() {
		System.out.println("You are using a unsupported java version. " +
				"You should use the latest certified Java SE 7 for your platform.");
		System.out.println("Sie benutzen eine nicht unterstützte Java Version. " +
				"Sie sollten die neuste zertifizierte Java SE 7 Version für Ihr Betriebssystem verwenden.");
	}
	
	public static void main(String[] args) {
		checkVersion(new Runnable() {
			
			@Override
			public void run() {
				System.out.println("CheckJavaVersion passed.");
			}
		});
	}

}
