//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.installer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is a stand-alone Java program used during the jenkins build for 
 * switching between Java6 and Java7 build. 
 * <p>
 * It is here for convenience only
 * as it has nothing to do with the installer build in a strict sense.
 * <p>
 * This is the snippet of a jenkins shell build step that runs this 
 * program:
 * <pre><code>
cd $WORKSPACE/nuclos-installer/src/main/java
javac org/nuclos/installer/JavaVersion.java
java org/nuclos/installer/JavaVersion 1.6.0_20 $WORKSPACE/nuclos-installer
rm org/nuclos/installer/JavaVersion.class
 * </code></pre>
 * <p>
 * ATTENTION: This class can't use ANY external library/Jar.
 * 
 * @author Oliver Brauch
 * @author Thomas Pasch (javadoc, modifications)
 */
@Deprecated
public class JavaVersion {
	
	private static String javaFullVersion = "1.7.0_09";
	private static String defaultPath = ".";
	
	private static File defaultFilePath;
	
    /**
     * The number of bytes in a kilobyte.
     */
    public static final long ONE_KB = 1024;

    /**
     * The number of bytes in a megabyte.
     */
    public static final long ONE_MB = ONE_KB * ONE_KB;
    
    /**
     * The file copy buffer size (30 MB)
     */
    private static final long FILE_COPY_BUFFER_SIZE = ONE_MB * 30;


	public static void main(String[] args) throws IOException {
		if (args.length >= 2) {
			javaFullVersion = args[0];
			defaultPath = args[1];
			defaultFilePath = new File(defaultPath);
		}
		replaceVersionsIfChanged();
		replaceClientWebstart();
		if (javaFullVersion.startsWith("1.6.")) {
			// build multijar client for java 1.6
			doCopyFile(new File(defaultFilePath, "modjar-multijar.sh"), new File(defaultFilePath, "modjar.sh"), true);
		}
	}
	
	/**
	 * From Apache commons-io 2.4 (tp)
	 */
    private static void doCopyFile(File srcFile, File destFile, boolean preserveFileDate) throws IOException {
        if (destFile.exists() && destFile.isDirectory()) {
            throw new IOException("Destination '" + destFile + "' exists but is a directory");
        }

        FileInputStream fis = null;
        FileOutputStream fos = null;
        FileChannel input = null;
        FileChannel output = null;
        try {
            fis = new FileInputStream(srcFile);
            fos = new FileOutputStream(destFile);
            input  = fis.getChannel();
            output = fos.getChannel();
            long size = input.size();
            long pos = 0;
            long count = 0;
            while (pos < size) {
                count = size - pos > FILE_COPY_BUFFER_SIZE ? FILE_COPY_BUFFER_SIZE : size - pos;
                pos += output.transferFrom(input, pos, count);
            }
        } finally {
        	if (output != null) output.close();
            if (fos != null) fos.close();
            if (input != null) input.close();
            if (fis != null) fis.close();
        }

        if (srcFile.length() != destFile.length()) {
            throw new IOException("Failed to copy full contents from '" +
                    srcFile + "' to '" + destFile + "'");
        }
        if (preserveFileDate) {
            destFile.setLastModified(srcFile.lastModified());
        }
    }


	//Replacements for switching Java Versions. Starting path is "nuclos/nuclos-installer"
	private static String[] JAVAVERSION_OCCURENCES = new String[]{
		"../pom.xml:<java.version>#javaVersion#</java.version>",
		"build.xml:<property name=\"java.src.version\" value=\"#javaVersion#\" />",
		"build.xml:<property name=\"java.tgt.version\" value=\"#javaVersion#\" />",
		"conf/launch4j.xml:<minVersion>#javaFullVersion#</minVersion>",
		"conf/launch4j.xml:Java Runtime Environment (JRE) #javaFullVersion# (or newer)",
		"src/main/resources/org/nuclos/installer/Messages_de.properties:(JRE Version >= #javaFullVersion#)",
		"src/main/resources/org/nuclos/installer/Messages.properties:(JRE Version >= #javaFullVersion#)",
		"../nuclos-server/src/main/resources/jnlp/jnlp.xsl:<java version=\"#javaVersion#+\"",
		"../nuclos-server/src/main/resources/jnlp/extension.jnlp.xsl:<j2se version=\"#javaVersion#+\"",
		"../nuclos-server/src/main/resources/jnlp/theme.jnlp.xsl:<j2se version=\"#javaVersion#+\"",
		"src/main/java/org/nuclos/installer/JavaVersion.java:String javaFullVersion = \"#javaFullVersion#\";",
	};
	
	private static String[] CLIENT_WEBSTART = new String[] {
		"../nuclos-server/src/main/resources/jnlp/jnlp.xsl:-ea #vmOptions#\""
	};
	
	private static void replaceVersionsIfChanged() {
		final Map<String,String> replace = new HashMap<String, String>();
		replace.put("javaVersion", getJavaVersion());
		replace.put("javaFullVersion", getJavaFullVersion());
		replace.put("javaLangtoolsVersion", getLangtoolsVersion());
		replace(JAVAVERSION_OCCURENCES, Collections.unmodifiableMap(replace), "#java", "sion#");
	}
	
	private static void replaceClientWebstart() {
		final Map<String,String> replace = new HashMap<String, String>();
		final StringBuilder replacement = new StringBuilder("-XX:MaxPermSize=172m -XX:PermSize=128m -XX:+CMSClassUnloadingEnabled");
		if (javaFullVersion.startsWith("1.6.")) {
			replacement.append(" ").append("-XX:+UseConcMarkSweepGC");
		} else {
			replacement.append(" ").append("-XX:+UseG1GC");
		}
		replace.put("vmOptions", replacement.toString());
		replace(CLIENT_WEBSTART, Collections.unmodifiableMap(replace), "#vmOp", "tions#");
	}
	
	private static void replace(String[] occurences, Map<String,String> replace, String prefix, String suffix) {
		Map<String, List<String>> mapOcc = new HashMap<String, List<String>>();
		for (String s : occurences) {
			int n = s.indexOf(':');
			String sfile = defaultPath + "/" + s.substring(0, n);
			String spattern = s.substring(n + 1);
			List<String> lstOcc = mapOcc.get(sfile);
			if (lstOcc == null) mapOcc.put(sfile, lstOcc = new ArrayList<String>());
			lstOcc.add(spattern);
		}
		for (String sfile : mapOcc.keySet()) {
			final int lenSuffix = suffix.length();
			File file = new File(sfile);
			if (file.exists()) {
				List<String> patterns = mapOcc.get(sfile);
				int n = patterns.size();
				String[] pattern1 = new String[n];
				String[] replcment = new String[n];
				String[] pattern2 = new String[n];
				for (int i = 0; i < n; i++) {
					String p = patterns.get(i);
					int n1 = p.indexOf(prefix);
					int n2 = p.indexOf(suffix, n1 + 1);
					pattern1[i] = p.substring(0, n1);
					replcment[i] = p.substring(n1 + 1, n2 + lenSuffix - 1);
					pattern2[i] = p.substring(n2 + lenSuffix);
 				}
				replaceIfChanged(replace, file, pattern1, replcment, pattern2);
			}
		}
	}
	
	private static void replaceIfChanged(Map<String,String> replace, File file, String[] pattern1, String[] replcment, String[] pattern2) {
		try {
			boolean bChanged = false;
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			List<String> lstLines = new ArrayList<String>();
			String line;
			while ((line = br.readLine()) != null) {
				for (int i = 0; i < pattern1.length; i++) {
					int n1 = line.indexOf(pattern1[i]);
					if (n1 < 0) continue;
					int n1start = n1 + pattern1[i].length();
					int n2 = line.indexOf(pattern2[i], n1start);
					if (n2 < 0) continue;
					final String toBeSubstituted = line.substring(n1start, n2);
					final String repl = replcment[i];
					final String toSubstitute = replace.get(repl);
					if (toSubstitute == null || toSubstitute.length() == 0) {
						throw new IllegalStateException("No value to substitue " + repl + " at " + (i + 1) + " of " + file);
					}
					if (!toSubstitute.equals(toBeSubstituted)) {
						line = line.substring(0, n1start) + toSubstitute + line.substring(n2);
						bChanged = true;
					}
				}
				lstLines.add(line);
			}
			br.close();
			fr.close();
			if (bChanged) {
				FileWriter fw = new FileWriter(file.getPath());
				for (String s : lstLines) {
					fw.write(s + "\n");
				}
				fw.close();
			}
		} catch (IOException ioe) {
			// Ok! (tp)
			ioe.printStackTrace();
		}
	}
	
	private static String javaVersion = null;
	public static String getJavaVersion() {
		if (javaVersion == null) {
			javaVersion = javaFullVersion.substring(0, javaFullVersion.lastIndexOf('.'));
		}
 		return javaVersion;
	}
	public static int getJavaMainVersion() {
		return getJavaMainVersion(javaFullVersion);
	}
	public static int getJavaMainVersion(String javaVersion) {
		return Integer.parseInt(javaVersion.substring(0, javaVersion.indexOf('.')));
	}
	public static int getJavaMajorVersion() {
		return getJavaMajorVersion(javaFullVersion);
	}
	public static int getJavaMajorVersion(String javaVersion) {
		return Integer.parseInt(javaVersion.substring(javaVersion.indexOf('.') + 1, javaVersion.lastIndexOf('.')));
	}
	public static int getJavaMinorVersion() {
		return getJavaMinorVersion(javaFullVersion);
	}
	public static int getJavaMinorVersion(String javaVersion) {
		if (javaVersion.indexOf('_') == -1)
			return Integer.parseInt(javaVersion.substring(javaVersion.lastIndexOf('.') + 1));
		else {
			return Integer.parseInt(javaVersion.substring(javaVersion.lastIndexOf('.') + 1, javaVersion.indexOf('_')));
		}
	}
	
	private static int javaBuild = -1;
	
	public static int getJavaBuild() {
		if (javaBuild < 0) {
			if (javaFullVersion.indexOf('_') == -1)
				javaBuild = 0;
			else
				javaBuild = Integer.parseInt(javaFullVersion.substring(javaFullVersion.indexOf('_') + 1));	
		}
		return javaBuild;
	}
	
	public static int getJavaBuild(String javaVersion) {
		final int idx1 = javaVersion.indexOf('_');
		if (idx1 == -1)
			return 0;
		final int idx2 = javaVersion.indexOf('-', idx1 + 1);
		if (idx2 == -1)
			return Integer.parseInt(javaVersion.substring(idx1 + 1));
		else 
			return Integer.parseInt(javaVersion.substring(idx1 + 1, idx2));			
	}
	
	public static String getJavaFullVersion() {
		return javaFullVersion;
	}
	
	public static String getLangtoolsVersion() {
		final int major = getJavaMajorVersion();
		final String result;
		switch (major) {
		case 6:
			result = "27";
			break;
		case 7:
			result = "147";
			break;
		default:
			throw new IllegalArgumentException(Integer.toString(major));
		}
		return result;
	}
}
