﻿# Architektur

## Module / Komponenten

Mögliche Strukturierung der Komponenten und Module

- AppModule
    - MenuComponent
    - FulltextSearchComponent

- AuthenticationModule
    - LoginComponent
    - LogoutComponent
    - ChangePasswordComponent

- DashboardModule

- SideviewModule (TODO: rename)
    - SearchtemplateComponent
    - SideviewmenuComponent
        - ListExportComponent
    - PerspectiveComponent
    - DetailComponent
		- CheckboxComponent
		- ActionTextComponent
		- DatechooserComponent
		- DocumentComponent
		- ImageComponent
		- MemoComponent
		- UserButtonComponent
		- TabbedPaneComponent
		- ValueListComponent
		- TitledSeparatorComponent
		- PanelComponent
        - SubformComponent
        - MatrixComponent
        - InputRequiredComponent
    - DetailModalComponent
    - PrintoutComponent
    - GenerationComponent
    - TreeComponent
    - StateComponent

- ChartModule
    - ChartViewComponent
    - DataFilterComponent
    - ChartConfigComponent
    - CSVExportComponent

- BusinessTestModule
    - ...

- PreferencesModule
	- PreferenceListComponent
	- PreferenceListFilterComponent
	- PreferenceDetailComponent

- DevUtilModule
    - LogViewerComponent
    - RESTExplorerComponent

