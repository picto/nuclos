import { NgModule } from '@angular/core';
import { ConsoleLogService } from './shared/console-log.service';
import { Logger } from './shared/logger';

@NgModule({
	imports: [],
	declarations: [],
	providers: [
		{
			provide: Logger,
			useClass: ConsoleLogService
		}
	]
})
export class LogModule {
}
