import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LayoutModule } from '../layout/layout.module';
import { LogModule } from '../log/log.module';
import { RuleService } from './shared/rule.service';

/**
 * Contains the rule engine which is responsible for executing (LayoutML-) rules on a given
 */
@NgModule({
	imports: [
		CommonModule,
		LayoutModule,
		LogModule
	],
	declarations: [],
	providers: [
		RuleService
	]
})
export class RuleModule {
}
