import { EntityObject } from '../../entity-object/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';
import { SortModel } from '../../entity-object/shared/sort.model';

export class ReinitSubformActionExecutor extends AbstractActionExecutor<RuleActionReinitSubform> {
	constructor(
		event: RuleEvent,
		action: RuleActionReinitSubform
	) {
		super(event, action);
	}

	/**
	 * Re-initializes a subform by reloading its data.
	 *
	 * @param eo
	 */
	execute(eo: EntityObject) {
		Logger.instance.debug('Re-init subform: %o on %o', this.action, eo);
		eo.getDependents(this.action.targetcomponent).load(new SortModel([]));
	}
}
