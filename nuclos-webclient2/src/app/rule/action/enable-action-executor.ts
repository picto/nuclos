import { EntityObject } from '../../entity-object/shared/entity-object.class';
import { Logger } from '../../log/shared/logger';
import { AbstractActionExecutor } from './abstract-action-executor';

export class EnableActionExecutor extends AbstractActionExecutor<RuleActionEnable> {
	constructor(
		event: RuleEvent,
		action: RuleActionEnable
	) {
		super(event, action);
	}

	/**
	 * TODO: Implement me.
	 *
	 * @param eo
	 */
	execute(eo: EntityObject) {
		Logger.instance.debug('Enable: %o on %o', this.action, eo);
	}
}
