import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../../entity-object/shared/data.service';
import { EntityObject } from '../../entity-object/shared/entity-object.class';
import { LayoutService } from '../../layout/shared/layout.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { RuleExecutor } from './rule-executor';

@Injectable()
export class RuleService {
	private rulesCache: Map<string, Rules>;

	constructor(
		private layoutService: LayoutService,
		private http: NuclosHttpService,
		private injector: Injector
	) {
		this.rulesCache = new Map<string, Rules>();
	}

	/**
	 * Fetches rules for the current layout of the given EO.
	 *
	 * TODO: Rules should later be defined directly on the Entity class.
	 *
	 * @returns {Observable<Rules>}
	 * @param parameters
	 */
	getLayoutRules(eo: EntityObject): Observable<Rules> {
		return new Observable<Rules>(observer => {
			this.layoutService.getWebLayoutURLDynamically(eo).subscribe(url => {
					if (!this.rulesCache[url]) {
						this.rulesCache[url] = this.http
							.get(url.replace('/calculated', '/rules'))	// TODO: Avoid URL manipulations
							.map(response => response.json())
							.publishReplay(1)
							.refCount();
					}
					this.rulesCache[url].subscribe(result => {
							observer.next(result);
							observer.complete();
						},
						error => observer.error(error)
					);
				},
				error => observer.error(error)
			);
		});
	}

	/**
	 * Fetches the rules of the given EO and subscribes a new RuleExecutor to it.
	 *
	 * If a parent EO is given, the Rules are fetched from its layout, as the eo is a then
	 * a subform EO and subform rules are defined in the parent layout.
	 *
	 * @returns {Observable<boolean>}
	 */
	updateRuleExecutor(eo: EntityObject, parentEO?: EntityObject): Observable<EntityObject> {
		let layoutEO = parentEO || eo;
		return this.getLayoutRules(layoutEO).map(
			rules => {
				let ruleExecutor = new RuleExecutor(rules, this.injector.get(DataService));
				eo.removeListenersByType(RuleExecutor);
				eo.addListener(ruleExecutor);
				ruleExecutor.initializeVlps(eo);
				return eo;
			}
		);
	}
}
