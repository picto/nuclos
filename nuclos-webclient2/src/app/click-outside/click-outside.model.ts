import { ElementRef } from '@angular/core';
import { Subject } from 'rxjs';

export interface EvaluateClickOutside {
	(target: EventTarget): boolean;
}

export class OutsideClick {
	element: ElementRef;
	subject: Subject<any>;
	clickHandler: EvaluateClickOutside;
}
