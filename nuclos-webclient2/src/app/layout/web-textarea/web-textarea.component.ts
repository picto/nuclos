import { Component, Injector, OnInit } from '@angular/core';
import { AbstractInputComponent } from '../shared/abstract-input-component';

@Component({
	selector: 'nuc-web-textarea',
	templateUrl: './web-textarea.component.html',
	styleUrls: ['./web-textarea.component.css']
})
export class WebTextareaComponent extends AbstractInputComponent<WebTextarea> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
	}

	getRows() {
		let rows = this.webComponent && this.webComponent.rows;

		if (rows > 0) {
			return rows;
		}

		return 2;
	}
}
