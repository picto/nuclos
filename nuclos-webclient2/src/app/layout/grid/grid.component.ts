import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../entity-object/shared/entity-object.class';

@Component({
	selector: 'nuc-grid',
	templateUrl: './grid.component.html',
	styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {
	@Input() webGrid: WebGrid;
	@Input() eo: EntityObject;

	constructor() {
	}

	ngOnInit() {
	}

}
