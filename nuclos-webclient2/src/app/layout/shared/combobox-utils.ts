import { Observable } from 'rxjs/Observable';
import { LovEntry } from '../../entity-object/shared/bo-view.model';
import { StringUtils } from '../../shared/string-utils';

export class ComboboxUtils {
	static filter(observable: Observable<LovEntry[]>, search: string) {
		let regex = StringUtils.regexFromSearchString(search);

		return observable.map(
			entries => entries.filter(entry => {
				let match = entry.name.match(regex);
				return match;
			})
		);
	}
}
