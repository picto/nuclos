import { AbstractWebComponent } from './abstract-web-component';

export abstract class AbstractLabelComponent<T extends WebLabel | WebLabelStatic> extends AbstractWebComponent<T> {
	getStyle() {
		let style: any = {
			fontSize: this.webComponent.fontSize
		};

		if (this.webComponent.textColor) {
			style.color = this.webComponent.textColor;
		}
		if (this.webComponent.bold) {
			style.fontWeight = 'bold';
		}
		if (this.webComponent.italic) {
			style.fontStyle = 'italic';
		}
		if (this.webComponent.underline) {
			style.textDecoration = 'underline';
		}

		return style;
	}
}
