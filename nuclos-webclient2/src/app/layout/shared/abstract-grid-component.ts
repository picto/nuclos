import { DoCheck, ElementRef, Injector } from '@angular/core';
import { ColDef, GridApi, GridOptions, RowNode } from 'ag-grid';
import { Observable, Subscription } from 'rxjs';
import { EntityObjectEventListener } from '../../entity-object/shared/entity-object-event-listener';
import { EntityObject, SubEntityObject } from '../../entity-object/shared/entity-object.class';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { AbstractInputComponent } from './abstract-input-component';
import { SortModel } from '../../entity-object/shared/sort.model';
import { ValueGetterParams } from 'ag-grid/dist/lib/entities/colDef';

export abstract class AbstractGridComponent<T extends WebInputComponent> extends AbstractInputComponent<T> implements DoCheck {

	gridOptions: GridOptions = <GridOptions>{};

	/**
	 * The dependents (Sub-EOs).
	 * "undefined" means "loading".
	 */
	protected dependents: SubEntityObject[] | undefined = [];

	private _eo: EntityObject;

	protected elementRef: ElementRef;

	/**
	 * Listens for changes in the Sub-EOs.
	 * Changes might e.g. be made by Layout-Rules. The grid must then be updated.
	 */
	private eoListener: EntityObjectEventListener;

	protected eoChangeListener: EntityObjectEventListener;

	protected dependentSubscription: Subscription;

	// private softRefreshSubject = new Subject();

	constructor(
		injector: Injector,
		i18nService: NuclosI18nService,
		elementRef: ElementRef
	) {
		super(injector);

		this.elementRef = elementRef;

		this.gridOptions.localeTextFunc = (key, defaultValue) => {
			let gridKey = 'grid.' + key;
			return i18nService.getI18nOrUndefined(gridKey) || defaultValue;
		};

		this.eoListener = {
			afterSave: () => this.loadData(),
			beforeCancel: () => this.cancel(),
			afterReload: () => this.loadData(),
		};

		// TODO auskommentiert wegen NUCLOS-5922 Subforms: Tab-Steuerung nicht verwendbar
		// this.softRefreshSubject.debounceTime(100).subscribe(
		// 	() => this.doSoftRefresh()
		// );
	}

	protected abstract loadData();

	protected abstract init();

	/**
	 * will be called when the ag-grid api is available
	 */
	protected abstract onGridApiReady(api: GridApi);

	protected cancel() {

	}

	isVisible(): boolean {
		return !!this.gridOptions.columnDefs;
	}

	/**
	 * TODO: DoCheck is called very often. Maybe there is a better way to reliably detect changes of the EO.
	 */
	ngDoCheck(): void {
		if (this._eo !== this.eo) {
			// ngDoCheck is called several times when changing eo, once without subEos and then with subEos.
			// One of them with subEos is the one we want
			if (!this.eo.getSubEOLinks()) {
				return;
			}
			// Same issue applies here, only one of nearly identical eos is needed
			if (this._eo && this._eo.getId() === this.eo.getId()) {
				return;
			}
			this.getLogger().debug('EO changed');
			if (this._eo) {
				this._eo.removeListener(this.eoListener);
			}
			this._eo = this.eo;
			this._eo.addListener(this.eoListener);
			this.init();
		}
	}

	protected updateGridData() {
		this.getLogger().debug('Update grid data: %o', this.dependents);
		if (this.gridOptions.api) {
			this.gridOptions.api.setRowData(this.dependents || []);
			this.updateRowSelection();
		}
		this.getLogger().debug('Update grid data done');
	}

	/**
	 * Does a hard refresh of the view.
	 * Row DOM elements may be recreated.
	 */
	protected hardRefresh() {
		if (this.gridOptions.api) {

			// Update column defs (e.g. the "editable" flag might have changed)
			if (this.gridOptions.columnDefs) {
				this.gridOptions.api.setColumnDefs(this.gridOptions.columnDefs);
			}

			this.updateRowSelection();
			this.softRefresh();
		}
	}

	private updateRowSelection() {
		if (this.gridOptions.api) {
			this.gridOptions.api.forEachNode((rowNode: RowNode) => {
				rowNode.setSelected(rowNode.data.isSelected(), false);
			});
		}
	}

	/**
	 * Does a soft refresh of the view.
	 * Row DOM elements are not recreated. Only changed, volatile cells are redrawn.
	 */
	protected softRefresh() {

	// TODO auskommentiert wegen NUCLOS-5922 Subforms: Tab-Steuerung nicht verwendbar
	// 	this.softRefreshSubject.next();
	// }
	//
	// private doSoftRefresh() {

		if (this.gridOptions.api) {
			this.gridOptions.api.onSortChanged();
			this.gridOptions.api.softRefreshView();

			if (this.dependents === undefined) {
				this.gridOptions.api.showLoadingOverlay();
			} else if (this.dependents.length === 0) {
				this.gridOptions.api.showNoRowsOverlay();
			} else {
				this.gridOptions.api.hideOverlay();
			}
		}
	}

	/**
	 * called from the component
	 */
	gridReady() {
		if (this.gridOptions.api) {
			this.onGridApiReady(this.gridOptions.api);
		}

		if (this.dependents && this.dependents.length > 0) {
			this.updateGridData();
		}
		this.hardRefresh();
	}

	loadDependents(refAttrId: string, sortModel: SortModel): Observable<SubEntityObject[] | undefined> {
		this.getLogger().debug('Loading dependents: %o...', refAttrId);

		if (this.dependentSubscription) {
			this.dependentSubscription.unsubscribe();
		}

		return this.eo.getDependents(refAttrId, sortModel).asObservable();
	}

	/**
	 * Returns values from the underlying Sub-EO via its attribute Getter method.
	 *
	 * @param data
	 * @param node
	 * @param colDef
	 * @param api
	 * @param context
	 * @param getValue
	 */
	protected valueGetter(params: ValueGetterParams): any {
		if (!params.colDef.field) {
			this.getLogger().warn('Col has no field: %o', params);
			return undefined;
		}

		return params.node.data.getAttribute(params.colDef.field);
	}

	/**
	 * Transfers new value to the underlying Sub-EO via the Setter method.
	 *
	 * @param params
	 */
	protected newValueHandler(params: {
			node: RowNode,
			data: SubEntityObject,
			oldValue: any,
			newValue: any,
			colDef: ColDef,
			api: GridApi,
			context: any
		}): boolean {

		if (!params.colDef.field) {
			this.getLogger().warn('Field missing: %o', params.colDef);
			return false;
		}

		params.node.data.setAttributeObserved(
			params.colDef.field,
			params.newValue
		).subscribe(changed => {
			if (changed) {
				if (this.gridOptions.api) {

					// this leads to loosing the cell focus
					// (needed for row colors - otherwise gridOptions.getRowClass() is not called):
					// this.gridOptions.api.refreshRows([params.node]);

					params.node.setSelected(params.node.data.isSelected());
				}
			}
		});
		return false;
	}

	protected abstract getLogger(): Logger;

	isEmpty() {
		return !this.dependents || this.dependents.length === 0;
	}
}
