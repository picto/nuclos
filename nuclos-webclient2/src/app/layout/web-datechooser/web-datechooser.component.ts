import { Component, ElementRef, EventEmitter, HostListener, Injector, OnInit, Output, ViewChild } from '@angular/core';
import {
	NgbDateParserFormatter,
	NgbDatepickerI18n,
	NgbDateStruct,
	NgbInputDatepicker
} from '@ng-bootstrap/ng-bootstrap';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date';
import { DatetimeService } from '../../shared/datetime.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';
import { NuclosDatepickerI18n } from './web-datechooser.locale';

@Component({
	selector: 'nuc-web-datechooser',
	templateUrl: './web-datechooser.component.html',
	styleUrls: ['./web-datechooser.component.css'],
	providers: [{provide: NgbDatepickerI18n, useClass: NuclosDatepickerI18n}]
})
export class WebDatechooserComponent extends AbstractInputComponent<WebDatechooser> implements OnInit {

	dateModel: NgbDateStruct;
	datePattern: string;

	@ViewChild('datepicker') datepicker: NgbInputDatepicker;
	@ViewChild('datepickerInput') datepickerInput: ElementRef;

	@Output() onValueChange = new EventEmitter();

	constructor(
		injector: Injector,
		private datetimeService: DatetimeService,
		private dateParser: NgbDateParserFormatter,
		private ref: ElementRef
	) {
		super(injector);
	}

	ngOnInit() {
		this.datePattern = this.datetimeService.getDatePattern();
	}

	getDateModel(): NgbDateStruct | undefined {
		this.updateModel();

		return this.dateModel;
	}

	private updateModel() {
		let newDateModel = this.getNewDateModel();

		if (!this.modelEquals(this.dateModel, newDateModel)) {
			this.dateModel = newDateModel;
		}
	}

	private getNewDateModel() {
		let value = this.eo.getAttribute(this.webComponent.name);
		let newDateModel;

		if (value) {
			let date = new Date(value);
			newDateModel = new NgbDate(
				date.getFullYear(),
				date.getMonth() + 1,
				date.getDate()
			);
		}

		return newDateModel;
	}

	private modelEquals(model1: NgbDateStruct | undefined, model2: NgbDateStruct | undefined) {
		if (!model1 && !model2) {
			return true;
		} else if (model1 && model2) {
			return model1.year === model2.year
				&& model1.month === model2.month
				&& model1.day === model2.day;
		} else {
			return false;
		}
	}

	toggleDatepicker() {
		this.datepicker.toggle();
	}

	closeDatepicker() {
		this.datepicker.close();
	}

	focusInput() {
		if (this.datepickerInput) {
			this.datepickerInput.nativeElement.focus();
		}
	}

	setAttributeValue(value: NgbDate) {
		let attributeValue: any = null;
		if (value) {
			let date = moment(new Date(
				value.year,
				value.month - 1,
				value.day
			)).format('YYYY-MM-DD');
			attributeValue = date;
		}

		this.eo.setAttributeObserved(this.webComponent.name, attributeValue).subscribe(
			changed => {
				if (changed) {
					this.onValueChange.next();
				}
			}
		);
	}

	isValid(): boolean {
		let value = this.getRawInput();
		let date = this.dateParser.parse(value);
		return !isNaN(date.day) && !isNaN(date.month) && !isNaN(date.year);
	}

	isDirty(): boolean {
		let input = this.getRawInput();
		return !!input;
	}

	getRawInput() {
		return this.ref.nativeElement.querySelector('input').value;
	}

	@HostListener('keydown', ['$event'])
	closeDatepickerOnTabKey($event) {
		if ($event.key === 'Tab') {
			this.datepicker.close();
		}
	}

	/**
	 * EvaluateClickOutside
	 * prevent closing datepicker, when clicking on it
	 * @return true when not clicking on datepicker
	 */
	isClickOutside(target: EventTarget) {
		return $(target).closest('ngb-datepicker').length === 0;
	}

	isHighlighted(date: { day: number, month: number, year: number }) {
		let selected = this.getModelValue();

		if (selected) {
			return false;
		}

		let currentDate = new Date();
		let result = currentDate.getDate() === date.day
			&& currentDate.getMonth() + 1 === date.month
			&& currentDate.getFullYear() === date.year;

		return result;
	}

	commitValue() {
		this.datepickerInput.nativeElement.blur();
	}
}
