import { Component, Injector, OnInit } from '@angular/core';
import { AbstractWebComponent } from '../shared/abstract-web-component';

@Component({
	selector: 'nuc-web-tabcontainer',
	templateUrl: './web-tabcontainer.component.html',
	styleUrls: ['./web-tabcontainer.component.css']
})
export class WebTabcontainerComponent extends AbstractWebComponent<WebTabcontainer> implements OnInit {

	constructor(injector: Injector) {
		super(injector);
	}

	ngOnInit() {
		this.webComponent.selectedIndex = 0;
	}

	/**
	 * Generates a unique ID for the given Tab.
	 *
	 * @param tab
	 */
	getTabID(tab: WebTab): number {
		return this.hashCode(this.webComponent.name + '.' + tab.title);
	}

	selectTab(index: number) {
		if (this.webComponent) {
			this.webComponent.selectedIndex = index;
		}
	}

	isSelected(index: number) {
		return this.webComponent && this.webComponent.selectedIndex === index;
	}

	/**
	 * Calculates a simple numeric hash for the given string.
	 *
	 * @see http://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript-jquery
	 * @param text
	 * @returns {number}
	 */
	/* tslint:disable:no-bitwise */
	hashCode(text: string): number {
		let hash = 0, i, chr, len;

		if (text.length === 0) {
			return hash;
		}

		for (i = 0, len = text.length; i < len; i++) {
			chr = text.charCodeAt(i);
			hash = ((hash << 5) - hash) + chr;
			hash |= 0; // Convert to 32bit integer
		}

		if (hash < 0) {
			hash = -hash;
		}

		return hash;
	}

	getTabs() {
		return this.webComponent && this.webComponent.tabs;
	}
}
