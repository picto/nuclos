/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebTextfieldComponent } from './web-textfield.component';

describe('WebTextfieldComponent', () => {
	let component: WebTextfieldComponent;
	let fixture: ComponentFixture<WebTextfieldComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebTextfieldComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebTextfieldComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
