# Layout module
Responsible for rendering WebLayouts.

## HowTo: Add a new WebLayout component
New components must be added to the schema first, then all DTO classes 
are automatically generated. For the server Java classes are generated via JAXB.
For the Webclient the JAXB-generated Java classes are converted to TypeScript.

After a new type is added to the schema, a corresponding Angular component should be implemented.
This component must extend the abstract generic class WebComponent and will thereby inherit
an EntityObject and a DTO attribute to work with. For the component to be dynamically found
and instantiated it must be added to the "entryComponents" in [layout.module.ts](layout.module.ts), and to [index.ts](index.ts).
