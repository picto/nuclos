import { AgRendererComponent } from 'ag-grid-angular';
import { EntityObjectService } from '../../../entity-object/shared/entity-object.service';
import { AbstractCellComponent } from '../abstract-cell-component';
import { NuclosCellRendererParams } from '../web-subform.model';
import { EntityObjectEventService } from '../../../entity-object/shared/entity-object-event.service';
export abstract class AbstractRendererComponent extends AbstractCellComponent implements AgRendererComponent {

	nuclosCellRenderParams: NuclosCellRendererParams;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.init(params);
		this.refresh(params);

		this.nuclosCellRenderParams = params.nuclosCellRenderParams;
	}

	refresh(params: any): boolean {
		super.updateParams(params);
		return false;
	}

	getNuclosCellRenderParams(): NuclosCellRendererParams {
		return this.nuclosCellRenderParams;
	}
}
