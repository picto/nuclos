import { Component } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { EntityAttrMeta } from '../../../../entity-object/shared/bo-view.model';
import { SubEntityObject } from '../../../../entity-object/shared/entity-object.class';
import { NuclosCellRendererParams } from '../../web-subform.model';
import { FqnService } from '../../../../shared/fqn.service';

@Component({
	selector: 'nuc-subform-reference',
	templateUrl: 'subform-reference-renderer.component.html',
	styleUrls: ['subform-reference-renderer.component.scss']
})
export class SubformReferenceRendererComponent implements AgRendererComponent {

	private params: any;
	attributeMeta: EntityAttrMeta;
	subEO: SubEntityObject;
	isEditable = false;
	value: string;
	attribute: any;

	constructor(
		private fqnService: FqnService
	) {
	}

	agInit(params: any) {
		this.params = params;
		this.value = (params.value && params.value.id) ? params.value.name : undefined;

		let nuclosCellRenderParams = params.nuclosCellRenderParams as NuclosCellRendererParams;
		this.attributeMeta = nuclosCellRenderParams.attrMeta;
		this.isEditable = nuclosCellRenderParams.editable;
		this.subEO = params.api.getModel().getRow(params.rowIndex).data;

		if (this.attributeMeta) {
			let fieldName = this.fqnService.getShortAttributeName(this.subEO.getEntityClassId(), this.attributeMeta.getAttributeID());
			if (fieldName !== undefined) {
				this.attribute = this.subEO.getAttribute(fieldName);
			}
		}

	}


}
