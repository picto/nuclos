import { Component, Inject, Injector, NgZone } from '@angular/core';
import { ColDef, ICellRendererParams } from 'ag-grid';
import { AgRendererComponent } from 'ag-grid-angular';
import { DialogService } from '../../../../dialog/dialog.service';
import { BoAttr, DocumentAttribute } from '../../../../entity-object/shared/bo-view.model';
import { EntityObject, SubEntityObject } from '../../../../entity-object/shared/entity-object.class';
import { NuclosI18nService } from '../../../../i18n/shared/nuclos-i18n.service';
import { NuclosConfigService } from '../../../../shared/nuclos-config.service';
import { ImageService } from '../../../shared/image.service';
import { WebFileComponent } from '../../../web-file/web-file.component';
import { NuclosCellRendererParams } from '../../web-subform.model';

@Component({
	selector: 'nuc-subform-document-renderer',
	templateUrl: './subform-document-renderer.component.html',
	styleUrls: ['./subform-document-renderer.component.css']
})
export class SubformDocumentRendererComponent extends WebFileComponent implements AgRendererComponent {

	private params: ICellRendererParams;
	private value: any;

	private colDef: ColDef;
	private attrMeta: BoAttr;

	private imageHref: string;
	isEditable = false;

	constructor(
		injector: Injector,
		nuclosConfigService: NuclosConfigService,
		dialogService: DialogService,
		nuclosI18nService: NuclosI18nService,
		imageService: ImageService,
		@Inject(NgZone) zone: NgZone
	) {
		super(injector, nuclosConfigService, dialogService, nuclosI18nService, imageService, zone);

	}

	agInit(params: ICellRendererParams) {
		this.refresh(params);
		setTimeout(() => {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				let documentAttribute = this.getDocumentAttribute();
				if (documentAttribute === undefined) {
					this.isImage = true;
					this.documentIsSelected = false;
					let subEoData = this.getSubEntityObject().getData();
					if (subEoData && subEoData.attrImages) {
						this.imageHref = subEoData.attrImages.links[colDef.field].href;
						this.documentIsSelected = true;
					}
				} else {
					this.isImage = false;

					// if file was just uploaded use filename from attribute otherwise from params
					this.fileName = (documentAttribute && documentAttribute.name) || (params.value && params.value.name);
					this.fileIconUrl = this.getFileIconUrl();
					this.documentIsSelected = documentAttribute ? !!documentAttribute.id : !!(params.value && params.value.id);
				}
			}
		});

		let nuclosCellRenderParams = params['nuclosCellRenderParams'] as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
	}

	refresh(params: any): boolean {
		this.colDef = params.colDef;
		this.params = params;
		this.value = params.value;
		if (this.colDef.cellEditorParams) {
			this.attrMeta = this.colDef.cellEditorParams['attrMeta'];
		}
		return false;

	}

	getColDef(): ColDef {
		return this.params.colDef;
	}

	setValue(value) {
		this.params.value = value;
		let eo = this.getEntityObject();
		if (eo) {
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, value);
			}
		}
	}


	setDocumentAttribute(value): void {
		this.setValue(value);
		return this.getSubEntityObject().setAttribute(this.webComponent.name, value);
	}

	private getSubEntityObject(): SubEntityObject {
		return this.params.data;
	}

	getEntityObject(): EntityObject {
		return this.getSubEntityObject();
	}

	hasDocument(): boolean {
		// image
		if (this.imageHref) {
			return true;
		}

		// document
		let documentAttribute = this.getDocumentAttribute();
		return documentAttribute !== undefined && documentAttribute !== null && documentAttribute.id !== null;
	}

	protected openDocument() {
		let docAttrId = this.getSubEntityObject().getEntityClassId() + '_' + this.params.colDef.field;
		let selfUrl = this.getSubEntityObject().getSelfURL();
		if (selfUrl) {
			if (this.isImage) {
				if (this.imageHref) {
					window.open(this.imageHref);
				}
			} else {
				// TODO: HATEOAS
				let link = selfUrl.replace('/bos/', '/boDocuments/') + '/documents/' + docAttrId;
				window.open(link);
			}
		}
	}

	protected resetDocument(): void {
		if (!this.params.colDef.field) {
			return;
		}

		this.getSubEntityObject().setAttribute(this.params.colDef.field, null);
		this.fileName = '';
		this.documentIsSelected = false;
		delete this.imageHref;
		this.showUploadLabel = true;

		let attrImages = this.getSubEntityObject().getData().attrImages;
		if (attrImages && attrImages.links) {
			if (attrImages.links.image) {
				delete attrImages.links.image;
			}
			if (attrImages.links[this.webComponent.name]) {
				delete attrImages.links[this.webComponent.name];
			}
		}
	}

	/**
	 * @return undefined if attribute is image
	 */
	getDocumentAttribute(): DocumentAttribute | undefined {
		let colDef = this.getColDef();
		if (colDef && colDef.field) {
			return this.getSubEntityObject().getAttribute(colDef.field);
		} else {
			throw Error('Unable to get ag-grid column definition.');
		}
	}

	openImageModal() {
		let subEo = this.getSubEntityObject();

		this.getImageService().openGalleryForSubform(
			subEo,
			this.getImageAttributeId()
		);
	}

	getImageService() {
		return this.injector.get(ImageService);
	}


	protected getImageAttributeId(): string {
		let colDef = this.getColDef();
		let attributeId = colDef && colDef.field;

		if (!attributeId) {
			throw Error('Field is not defined');
		}

		return attributeId;
	}
}
