import { Component } from '@angular/core';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { NuclosCellRendererParams } from '../../web-subform.model';
import { AbstractRendererComponent } from '../abstract-renderer-component';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';

@Component({
	selector: 'nuc-subform-boolean-renderer',
	templateUrl: 'subform-boolean-renderer.component.html',
	styleUrls: ['subform-boolean-renderer.component.css']
})
export class SubformBooleanRendererComponent extends AbstractRendererComponent {

	private isEditable = false;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService
	) {
		super(entityObjectService, eoEventService);
	}

	toggleValue() {
		if (!this.isEditable) {
			return;
		}

		this.setValue(!this.getValue());
		let eo = this.getEntityObject();
		if (eo) {
			eo.setDirty(true);
			let colDef = this.getColDef();
			if (colDef && colDef.field) {
				this.getSubEntityObject().setAttribute(colDef.field, this.getValue());
			}
		}
	}

	agInit(params: any) {
		super.agInit(params);
		let nuclosCellRenderParams = params.colDef.cellRendererParams.nuclosCellRenderParams as NuclosCellRendererParams;
		this.isEditable = nuclosCellRenderParams.editable;
	}
}
