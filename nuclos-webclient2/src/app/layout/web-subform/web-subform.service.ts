import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuclosCache, NuclosCacheService } from '../../cache/shared/nuclos-cache.service';
import { SubformLayout } from './web-subform.model';

@Injectable()
export class WebSubformService {

	private subformLayoutCache: NuclosCache;

	constructor(
		private cacheService: NuclosCacheService,
	) {
		this.subformLayoutCache = this.cacheService.getCache('subformLayoutCache');
	}

	evictSubformColumnLayoutInCache(mainEntityUid: string, subformEoMetaId: string, subformLayout: SubformLayout) {
		let cacheKey = SubformLayout.getCacheKey(mainEntityUid, subformEoMetaId);

		this.subformLayoutCache.delete(cacheKey);

		this.subformLayoutCache.get(cacheKey,
			new Observable<SubformLayout>(
				observer => {
					observer.next(subformLayout);
				}
			)
		);
	}
}
