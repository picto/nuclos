import { Component, Input, OnInit } from '@angular/core';
import { WebSubformComponent } from '../web-subform.component';
import { EntityMeta } from '../../../entity-object/shared/bo-view.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {
	Preference,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../../preferences/preferences.model';
import { EntityObjectPreferenceService } from '../../../entity-object/shared/entity-object-preference.service';

@Component({
	selector: 'nuc-subform-buttons',
	templateUrl: './subform-buttons.component.html',
	styleUrls: ['./subform-buttons.component.css']
})
export class SubformButtonsComponent implements OnInit {

	@Input() subform: WebSubformComponent;
	@Input() meta: EntityMeta;

	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);
	selectedSearchtemplatePref$: BehaviorSubject<Preference<SearchtemplatePreferenceContent> | undefined> = new BehaviorSubject(undefined);

	constructor(private entityObjectPreferenceService: EntityObjectPreferenceService) {
	}

	ngOnInit() {
		this.selectedSideviewmenuPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(this.meta.getBoMetaId());
		// TODO:
		// this.selectedSearchtemplatePref$ =
		// this.entityObjectPreferenceService.getSubformSearchtemplatePreferenceSelection(this.meta.getBoMetaId());
	}

}
