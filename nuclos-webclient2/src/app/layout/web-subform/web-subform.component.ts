import { Component, DoCheck, ElementRef, Injector, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColDef, ColumnChangeEvent, GridApi, RowNode } from 'ag-grid';
import { Subject, Subscription } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Action } from '../../authentication/action.enum';
import { AuthenticationService } from '../../authentication/authentication.service';
import { EntityAttrMeta, EntityMeta } from '../../entity-object/shared/bo-view.model';
import { DetailModalService } from '../../entity-object/shared/detail-modal.service';
import { EntityObjectPreferenceService } from '../../entity-object/shared/entity-object-preference.service';
import { SubEntityObject } from '../../entity-object/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object/shared/entity-object.service';
import { MetaService } from '../../entity-object/shared/meta.service';
import { SelectableService } from '../../entity-object/shared/selectable.service';
import { SortAttribute, SortModel } from '../../entity-object/shared/sort.model';
import { ExportBoListTemplateState } from '../../entity-object/sidebar/statusbar/statusbar.component';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SelectedAttribute,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { RuleService } from '../../rule/shared/rule.service';
import { ArrayUtils } from '../../shared/array-utils';
import { BrowserDetectionService } from '../../shared/browser-detection.service';
import { FqnService } from '../../shared/fqn.service';
import { Link, SubEOLinkContainer } from '../../shared/link.model';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { AbstractGridComponent } from '../shared/abstract-grid-component';
import { SubformComboboxEditorComponent } from './cell-editors/subform-combobox-editor/subform-combobox-editor.component';
import { SubformDateEditorComponent } from './cell-editors/subform-date-editor/subform-date-editor.component';
import { SubformLovEditorComponent } from './cell-editors/subform-lov-editor/subform-lov-editor.component';
import { SubformNumberEditorComponent } from './cell-editors/subform-number-editor/subform-number-editor.component';
import { SubformStringEditorComponent } from './cell-editors/subform-string-editor/subform-string-editor.component';
import { SubformBooleanRendererComponent } from './cell-renderer/subform-boolean-renderer/subform-boolean-renderer.component';
import { SubformDateRendererComponent } from './cell-renderer/subform-date-renderer/subform-date-renderer.component';
import { SubformDocumentRendererComponent } from './cell-renderer/subform-document-renderer/subform-document-renderer.component';
import { SubformEditRowRendererComponent } from './cell-renderer/subform-edit-row-renderer/subform-edit-row-renderer.component';
import { SubformNumberRendererComponent } from './cell-renderer/subform-number-renderer/subform-number-renderer.component';
import { SubformReferenceRendererComponent } from './cell-renderer/subform-reference-renderer/subform-reference-renderer.component';
import { SubformStateIconRendererComponent } from './cell-renderer/subform-state-icon-renderer/subform-state-icon-renderer.component';
import { SubformCellValidator } from './subform-cell-validator';
import { SubformRowComparator } from './subform-row-comparator';
import { NuclosCellRendererParams, RowEditCellParams, SubformColumn } from './web-subform.model';
import { WebSubformService } from './web-subform.service';


@Component({
	selector: 'nuc-web-subform',
	templateUrl: './web-subform.component.html',
	styleUrls: ['./web-subform.component.scss']
})
export class WebSubformComponent extends AbstractGridComponent<WebSubform> implements OnInit, DoCheck {
	private subformLinks: SubEOLinkContainer | undefined;

	private subformMeta: EntityMeta;

	/**
	 * provides column layout changes
	 */
	private columnLayoutChanged: Subject<Date> = new Subject<Date>();
	private columnLayoutChangedSubscription: Subscription;

	private selectedColumnPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);

	showPrintSearchResultListButton = false;
	private tablePreferenceSubscription: Subscription;

	private sortModel: SortModel = new SortModel([]);

	hasVisibleButtons = false;

	hiddenColumns: Array<string|undefined>;

	private columnsFromLayout = new Map<string, WebSubformColumn>();

	static isUndefinedOrTrue(value: any) {
		return value === undefined || value === true;
	}

	constructor(
		injector: Injector,
		private metaService: MetaService,
		private entityObjectService: EntityObjectService,
		private webSubformService: WebSubformService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private $log: Logger,
		private ruleService: RuleService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private i18nService: NuclosI18nService,
		elementRef: ElementRef,
		private modalService: NgbModal,
		private exportBoListTemplateState: ExportBoListTemplateState,
		private authenticationService: AuthenticationService,
		private browserDetectionService: BrowserDetectionService,
		private validationService: NuclosValidationService,
		private detailModalService: DetailModalService,
	) {
		super(injector, i18nService, elementRef);

		this.eoChangeListener = {
			afterAttributeChange: () => this.softRefresh(),
			afterValidationChange: () => this.softRefresh(),
			afterReload: () => this.softRefresh()
		};

		this.showPrintSearchResultListButton = this.authenticationService.isActionAllowed(Action.PrintSearchResultList);
	}

	private getColDef(event: ColumnChangeEvent): ColDef | undefined {
		let columnDefs = this.gridOptions.columnDefs as ColDef[];
		if (columnDefs) {
			let colDef: ColDef = columnDefs.filter(col => col.colId === event.getColumn().getColId()).shift() as ColDef;
			return colDef;
		}
		return undefined;
	}

	ngOnInit() {

		this.gridOptions.onColumnResized = (event: ColumnChangeEvent) => {
			// update column model
			let colDef = this.getColDef(event);
			if (colDef) {
				colDef.width = event.getColumn().getActualWidth();
			}

			this.columnLayoutChanged.next(new Date());
		};

		this.gridOptions.onColumnMoved = (event: ColumnChangeEvent) => {
			// update column model
			let colDef = this.getColDef(event);
			if (this.gridOptions.columnDefs && colDef) {
				let fromIndex = this.gridOptions.columnDefs.indexOf(colDef);
				ArrayUtils.move(this.gridOptions.columnDefs, fromIndex, event.getToIndex());
			}

			this.columnLayoutChanged.next(new Date());
		};

		this.gridOptions.onSortChanged = () => {
			this.columnLayoutChanged.next(new Date());
		};

		// NuclosRowColor
		this.gridOptions.getRowStyle = (params: {data: SubEntityObject}) => {
			let rowColor = params.data.getRowColor();
			return rowColor !== undefined ? {
				'background-color': rowColor
			} : undefined;
		};
	}

	protected onGridApiReady(api: GridApi) {
		// set initial sorting when api is ready
		setTimeout(() => {
			api.setSortModel(this.sortModel.getColumns());
		});
	}

	isEnabled(): boolean {
		let restriction = this.eo.getSubEORestriction(this.webComponent.foreignkeyfieldToParent);
		let result = super.isEnabled() && restriction !== 'readonly' && this.subformLinks;
		return !!result;
	}

	private isColumnEnabledByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.enabled;
	}

	private isColumnVisibleByLayout(attributeId: string) {
		let subformColumn = this.columnsFromLayout.get(attributeId);

		if (!subformColumn) {
			// No definition means no restriction
			return true;
		}

		return subformColumn.visible;
	}

	isVisible(): boolean {
		return super.isVisible() && !!this.eo.getSubEOLink(this.webComponent.foreignkeyfieldToParent);
	}

	protected init() {
		this.$log.debug('Subform init...');
		this.subformLinks = this.eo.getSubEOLink(this.webComponent.foreignkeyfieldToParent);
		this.loadData();
	}

	/**
	 * Loads Subform data from the server (if the EO does not already have it yet).
	 *
	 * TODO: Refactor/simplify
	 */
	loadData() {
		this.subformLinks = this.eo.getSubEOLink(this.webComponent.foreignkeyfieldToParent);

		if (!this.subformLinks) {
			this.$log.warn('Could not load subform data - no link');
			return;
		}

		this.$log.debug('Loading Subform %o...', this.subformLinks.boMeta.href);

		this.metaService.getSubBoMeta(this.subformLinks.boMeta.href).subscribe(
			meta => {

				this.subformMeta = meta;

				// configure grid options
				this.gridOptions.enableSorting = true;
				this.gridOptions.enableColResize = true;
				this.gridOptions.rowSelection = 'multiple';
				this.gridOptions.suppressRowClickSelection = true;

				this.loadTablePreferences();
			}
		);
	}

	private loadTablePreferences() {
		let parentEntityUid = this.eo.getData().boMetaId;

		this.entityObjectPreferenceService.loadSubformPreferences(
			this.subformMeta,
			parentEntityUid,
			this.eo.getLayoutId()
		).subscribe(() => {
			this.selectedColumnPref$ = this.entityObjectPreferenceService.getSubformColumnPreferenceSelection(this.subformMeta.getBoMetaId());

			if (this.tablePreferenceSubscription) {
				this.tablePreferenceSubscription.unsubscribe();
			}
			this.tablePreferenceSubscription = this.selectedColumnPref$.subscribe(pref => {
				if (pref) {
					this.initColumns(pref.content.columns, this.subformMeta);
					this.sortModel = new SortModel(this.getSortModel(pref.content.columns.filter(col => col.sort && col.sort.direction)));
				}

				this.dependentSubscription = this.loadDependents(this.webComponent.foreignkeyfieldToParent, this.sortModel).subscribe(
					dependents => {
						this.updateDependents(dependents);
						this.refreshGrid();
						this.safariLayoutHack();
					}
				);
			});
		});
	}

	private refreshGrid() {
		if (this.gridOptions.api) {
			this.updateGridData();
			this.hardRefresh();
			if (this.gridOptions.enableSorting && this.sortModel) {
				this.gridOptions.api.setSortModel(this.sortModel.getColumns());
			}
		}
	}

	private updateDependents(dependents) {
		this.dependents = dependents;
		if (dependents) {
			dependents.forEach(eo => eo.addListener(this.eoChangeListener));
		}
		this.refreshGrid();
	}

	/**
	 * handle safari flexbox bug
	 */
	private safariLayoutHack() {
		if (this.browserDetectionService.isSafari()) {
			setTimeout(() => {
				let height = $(this.elementRef.nativeElement).closest('.tab-container-panel').height();
				$(this.elementRef.nativeElement).find('.ag-bl').height(height);
			});
		}
	}

	private initColumns(columns: ColumnAttribute[], meta: EntityMeta): void {
		if (this.columnLayoutChangedSubscription) {
			this.columnLayoutChangedSubscription.unsubscribe();
		}

		this.gridOptions.columnDefs = [];

		this.updateColumnsFromLayout();

		this.hiddenColumns = columns
			.map(column => meta.getAttributeMeta(this.fqnService.getShortAttributeName(this.subformMeta.getBoMetaId(), column.boAttrId)))
			.filter(attrMeta => attrMeta && !this.isColumnVisible(attrMeta, meta))
			.map(attrMeta => attrMeta ? attrMeta.getAttributeID() : undefined)
			.filter(attributeId => attributeId !== undefined);

		this.addSelectionColumn(this.gridOptions.columnDefs);

		let visibleColumns = this.filterVisibleColumns(columns, meta);

		for (let column of visibleColumns) {
			let colDef = this.createColumnDefinition(column, meta);
			if (colDef) {
				this.gridOptions.columnDefs.push(colDef)
			}
		}

		this.addEditRowColumnIfLayoutExists().subscribe(
			() => {
				if (this.isEnabled() && this.gridOptions.columnDefs) {
					for (let colDef of this.gridOptions.columnDefs) {
						this.addStatusClasses(colDef);
					}
				}
			}
		);

		this.subscribeToColumnChanges();
	}

	private createColumnDefinition(column, meta: EntityMeta) {
		let fieldName = this.fqnService.getShortAttributeName(this.subformMeta.getBoMetaId(), column.boAttrId);
		let attribute = meta.getAttributeMeta(fieldName);

		if (!attribute) {
			this.$log.warn('Unknown attribute: %o', fieldName);
			return;
		}

		let editable = this.isColumnEditable(attribute);

		let colDef: ColDef = {
			headerName: attribute.getName(),
			field: fieldName,
			colId: attribute.getAttributeID(),
			width: column.width,
			editable: editable,
			valueGetter: this.valueGetter,

			// allow access to this.elementRef in AbstractGridComponent
			newValueHandler: ((params) => {
				return this.newValueHandler(params);
			}),

			volatile: true,
			cellEditorParams: {}
		};

		this.addStylesAndClasses(editable, colDef, attribute);

		colDef.headerClass = this.getHeaderClass(attribute);

		colDef.cellRendererParams = {
			nuclosCellRenderParams: {
				editable: editable,
				attrMeta: attribute
			} as NuclosCellRendererParams
		};

		if (attribute.isBoolean()) {
			colDef.comparator = SubformRowComparator.booleanComparator;
			colDef.cellRendererFramework = SubformBooleanRendererComponent;
			/* use renderer instead of editor component to make 1-click editing possible */
			colDef.editable = false;
		} else if (attribute.isNumber()) {
			colDef.comparator = SubformRowComparator.numberComparator;
			colDef.cellRendererFramework = SubformNumberRendererComponent;
			colDef.cellEditorFramework = SubformNumberEditorComponent;
		} else if (attribute.isDate() || attribute.isTimestamp()) {
			colDef.cellRendererFramework = SubformDateRendererComponent;
			colDef.cellEditorFramework = SubformDateEditorComponent;
			colDef.cellClass = 'subform-date-cell';
		} else if (attribute.isStateIcon()) {
			colDef.comparator = SubformRowComparator.nameComparator;
			colDef.cellRendererFramework = SubformStateIconRendererComponent;
			colDef.editable = false;
		} else if (attribute.isDocument() || attribute.isImage()) {
			colDef.comparator = SubformRowComparator.nameComparator;
			colDef.cellRendererFramework = SubformDocumentRendererComponent;
			colDef.editable = false;
		} else if (attribute.isReference()) {
			colDef.comparator = SubformRowComparator.nameComparator;
			colDef.cellRendererFramework = SubformReferenceRendererComponent;
			if (attribute.getComponentType() === 'Combobox') {
				colDef.cellEditorFramework = SubformComboboxEditorComponent;
			} else {
				colDef.cellEditorFramework = SubformLovEditorComponent;
			}
		} else {
			colDef.cellEditorFramework = SubformStringEditorComponent;
			colDef.comparator = SubformRowComparator.textComparator;
		}

		// provide attribute metadata for render component  TODO make typesafe
		colDef.cellEditorParams = {
			attrMeta: attribute,
			subformMeta: this.subformMeta
		};

		this.addVlpToColumnDefinition(attribute, colDef);

		if (colDef.icons === undefined) {
			colDef.icons = {};
		}
		colDef.icons['sortAscending'] = '<i class="fa fa-chevron-up"/>';
		colDef.icons['sortDescending'] = '<i class="fa fa-chevron-down"/>';

		if (!colDef.comparator) {
			colDef.comparator = SubformRowComparator.textComparator;
		}

		return colDef;
	}

	private filterVisibleColumns(columns: ColumnAttribute[], meta: EntityMeta): ColumnAttribute[] {
		return columns.filter(column => {
			if (!column.selected) {
				return false;
			}

			let fieldName = this.fqnService.getShortAttributeName(this.subformMeta.getBoMetaId(), column.boAttrId);
			let attr = meta.getAttributeMeta(fieldName);

			if (!attr) {
				this.$log.warn('Unknown attribute: %o', fieldName);
				return false;
			}

			if (!this.isColumnVisible(attr, meta)) {
				return false;
			}
			return true;
		});
	}

	private isColumnVisible(attr: EntityAttrMeta, meta: EntityMeta): boolean {
		let visible = !attr.isHidden() && attr.getAttributeID() !== meta.getRefAttrId()
			&& this.isColumnVisibleByLayout(attr.getAttributeID());
		return visible;
	}

	private isColumnEditable(attr: EntityAttrMeta): boolean {
		let editable = this.isEnabled() && !attr.isReadonly() && this.isColumnEnabledByLayout(attr.getAttributeID());
		if (editable) {
			// TODO NUCLOS-6134 Find a better solution for this (Avoid a loop)
			let roAttributes = this.eo.getSubEOReadOnlyAttributes(this.webComponent.foreignkeyfieldToParent);
			if (roAttributes !== undefined) {
				for (let roAttr of roAttributes) {
					if (roAttr === attr.getAttributeID()) {
						return false;
					}
				}
			}
		}

		return editable;
	}

	private updateColumnsFromLayout() {
		this.columnsFromLayout.clear();
		if (this.webComponent.subformColumns) {
			for (let subformColumn of this.webComponent.subformColumns) {
				this.columnsFromLayout.set(subformColumn.name, subformColumn);
			}
		}
	}

	private addSelectionColumn(columnDefs: ColDef[]) {
		// row selection checkbox
		let rowSelectionCheckboxColDef: ColDef = {
			checkboxSelection: true,
			headerTooltip: this.i18nService.getI18n('webclient.checkbox.selectAll'),
			headerCheckboxSelection: true,
			headerName: '',
			width: 20,
			editable: false,
			pinned: 'left',
			suppressFilter: true,
			suppressMovable: true,
			suppressNavigable: true,
			suppressResize: true,
			suppressSorting: true,
			volatile: false
		};

		columnDefs.push(rowSelectionCheckboxColDef);
	}

	private addVlpToColumnDefinition(attr: EntityAttrMeta, colDef: ColDef) {
		let subformColumn = this.columnsFromLayout.get(attr.getAttributeID());

		if (subformColumn) {
			let vlp = subformColumn.valuelistProvider;
			if (vlp) {
				// TODO: Use a proper interface here
				colDef.cellEditorParams['valuelistProvider'] = vlp;
			}
		}
	}

	private addEditRowColumnIfLayoutExists(): Observable<any> {
		return this.getSubformLayoutLink().do(layout => {
			if (!layout) {
				this.$log.info('Subform BO has no default layout');
				return;
			}

			let couldDisplayPopupIcon = !!this.subformMeta.getPopupParameter();

			let editRowIconColDef: ColDef = {
				headerName: '',
				cellRendererFramework: SubformEditRowRendererComponent,
				width: couldDisplayPopupIcon ? 40 : 20,
				editable: false,
				pinned: 'right',
				suppressFilter: true,
				suppressMovable: true,
				suppressNavigable: true,
				suppressResize: true,
				suppressSorting: true,
				volatile: true
			};

			editRowIconColDef.cellRendererParams = new RowEditCellParams(this.webComponent.advancedProperties);
			editRowIconColDef.cellRendererParams.canOpenModal = this.canOpenModal();

			if (this.gridOptions.columnDefs) {
				this.gridOptions.columnDefs.push(editRowIconColDef);
			}
		})
	}

	private getSubformLayoutLink(): Observable<Link> {
		let detailEntityClassId = this.subformMeta.getDetailEntityClassId();
		if (detailEntityClassId) {
			return this.metaService.getEntityMeta(detailEntityClassId).map(
				detailMeta => detailMeta.getLinks().defaultLayout
			);
		}

		return Observable.of(this.subformMeta.getLinks().defaultLayout);
	}

	private getSortModel(columns: ColumnAttribute[]): { colId, sort }[] {
		return columns
			.filter(column => column.sort)
			.sort(
				(a: ColumnAttribute, b: ColumnAttribute) => {
					if (!a.sort || !b.sort || a.sort.prio === undefined || b.sort.prio === undefined) {
						return 0;
					}
					if (a.sort.prio < b.sort.prio) {
						return -1;
					}
					if (a.sort.prio > b.sort.prio) {
						return 1;
					}
					return 0;
				}
			)
			.map(column => {
				return {
					colId: column.boAttrId,
					sort: column.sort ? column.sort.direction : 'asc'
				};
			});
	}

	private addStylesAndClasses(editable: boolean, colDef: ColDef, attr: EntityAttrMeta) {
		if (this.isEnabled()) {
			this.addValidationClasses(attr, colDef);
			if (!editable) {
				this.lightenBackground(colDef);
			}
		}
	}

	private addValidationClasses(attr: EntityAttrMeta, colDef: ColDef) {
		let validator;

		if (attr.isNumber()) {
			validator = _params => this.validationService.isValidForInput(_params.value, 'number');
		}

		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}

		colDef.cellClassRules['nuc-validation'] = () => true;
		colDef.cellClassRules['nuc-validation-valid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-valid');
		colDef.cellClassRules['nuc-validation-missing'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-missing');
		colDef.cellClassRules['nuc-validation-invalid'] = params =>
			new SubformCellValidator(params, validator).hasClass('nuc-validation-invalid');
	}

	private addStatusClasses(colDef: ColDef) {
		if (!colDef.cellClassRules) {
			colDef.cellClassRules = {};
		}

		let eo = (params) => params.data;
		let rowNew = (params) => eo(params).isNew();
		let rowDeleted = (params) => eo(params).isMarkedAsDeleted();
		let rowModified = (params) => eo(params).isDirty();

		colDef.cellClassRules['row-new'] = params => rowNew(params);
		colDef.cellClassRules['row-deleted'] = params => rowDeleted(params);
		colDef.cellClassRules['row-modified'] = params => rowModified(params);

	}

	private lightenBackground(colDef: ColDef) {
		colDef.cellStyle = {'background-image': 'url("assets/lighten.png")'};
	}

	private getHeaderClass(attributeMeta: EntityAttrMeta) {
		let result = 'text-left';
		if (attributeMeta.isBoolean() || attributeMeta.isDate() || attributeMeta.isTimestamp() || attributeMeta.isStateIcon()) {
			result = 'text-center';
		} else if (attributeMeta.isNumber()) {
			result = 'text-right';
		}

		return result;
	}

	private subscribeToColumnChanges() {
		// save column layout when column order, width or sort order was changed
		// TODO sort order
		this.columnLayoutChangedSubscription = this.columnLayoutChanged
			.asObservable()
			.debounceTime(500)
			.distinctUntilChanged()
			.skip(1)	// TODO: Prevent the initial event instead of skipping it here
			.subscribe(
				() => {
					this.storeSubformColumnLayout();
				}
			);
	}

	private storeSubformColumnLayout(): void {

		if (!this.authenticationService.isActionAllowed(Action.WorkspaceCustomizeEntityAndSubFormColumn)) {
			return;
		}

		if (this.gridOptions.columnApi && this.gridOptions.api) {
			this.sortModel = new SortModel(
				this.gridOptions.api.getSortModel() as SortAttribute[]
			);

			// column order and column width
			let subformColumns: SubformColumn[] = this.gridOptions.columnApi.getColumnState()
				.filter(col => col.colId.length > 1) // filter out row selection column
				.map(
					(cs) => {
						let fieldName = this.fqnService.getShortAttributeName(this.subformMeta.getBoMetaId(), cs.colId);
						return {
							eoAttrFqn: cs.colId,
							name: this.subformMeta.getAttribute(fieldName).name,
							fieldName: fieldName,
							width: cs.width
						};
					}
				);


			// map ag-grid columns to preference columns
			subformColumns = this.gridOptions.columnApi.getAllGridColumns()
				.map(c => c.getId())
				.map((colId: string) => {
					return subformColumns.filter(col => col.eoAttrFqn === colId).shift() || {} as SubformColumn;
				});


			// column sort order
			let sortPrio = 0;
			this.gridOptions.api.getSortModel().forEach(
				(col) => {
					let subformColumn = subformColumns.filter(sc => sc.eoAttrFqn === col.colId).shift();
					if (subformColumn) {
						subformColumn.sort = {
							direction: col.sort,
							prio: sortPrio++
						};
					}
				}
			);

			let columnPref = this.selectedColumnPref$.getValue();

			if (columnPref) {
				let position = 0;
				columnPref.content.columns = subformColumns
					.filter(col => col.eoAttrFqn)
					.map(gc => {
						return {
							boAttrId: gc.eoAttrFqn,
							name: gc['name'], // TODO refactor column interfaces
							width: gc.width,
							position: position++,
							selected: true,
							sort: gc.sort
						} as SelectedAttribute;
					});


				this.webSubformService.evictSubformColumnLayoutInCache(
					this.eo.getEntityClassId(),
					this.subformMeta.getBoMetaId(),
					{ columns: subformColumns }
				);

				columnPref.layoutId = this.eo.getLayoutId();
				this.selectableService.savePreference(columnPref).subscribe();
			}
		}
	}

	/**
	 * Instantiates a new Sub-EO and adds it to the grid data.
	 */
	newEntry() {
		this.entityObjectService.createNew(this.subformMeta.getBoMetaId(), this.eo)
			.map(eo => new SubEntityObject(
				this.eo,
				this.getReferenceAttributeId(),
				eo.getData()
			))
			.do(eo => {
				this.ruleService.updateRuleExecutor(eo, this.eo).subscribe();
				eo.addListener(this.eoChangeListener);
				eo.setComplete(true);
			})
			.subscribe(subEo => {
					let refAttrId = this.getReferenceAttributeId();
					if (refAttrId) {
						// Restrict reference to main EO
						let ref = {
							id: this.eo.getId(),
							name: this.eo.getTitle()	// TODO: Respect possibly different string representation of VLP
						};
						subEo.setAttribute(refAttrId, ref);

						this.addSubEO(subEo);

						if (this.canOpenModal()) {
							this.detailModalService.openEoInModal(subEo).subscribe();
						}
					} else {
						this.$log.error('No refAttrId defined.');
					}
				}
			);
	}

	getReferenceAttributeId() {
		let attributeId = this.subformMeta.getMetaData().refAttrId;

		if (!attributeId) {
			this.$log.warn('Unknown reference attribute');
			attributeId = '';
		}

		return attributeId;
	}

	/**
	 * Marks already saved Sub-EOs for deletion.
	 * Removes new Sub-EOs completely.
	 */
	deleteSelected(): void {
		for (let subEO of this.getSelectedSubEOs()) {
			if (!subEO.isNew()) {
				// mark already saved subbos as deleted
				subEO.markAsDeleted();
				subEO.setSelected(false);
			} else if (this.dependents) {
				this.eo.getDependents(this.webComponent.foreignkeyfieldToParent).removeAll([subEO]);
			}
		}
		this.updateGridData();
	}

	/**
	 * Clones the selected Sub-EOs (for insert).
	 */
	cloneSelected(): void {
		this.addSubEOs(
			this.getSelectedSubEOs().map(subEo => subEo.clone())
		);
	}

	/**
	 * Returns the currently selected Sub-EOs.
	 *
	 * @returns {SubEntityObject[]}
	 */
	private getSelectedSubEOs(): SubEntityObject[] {
		if (!this.dependents) {
			return [];
		}
		return this.dependents.filter(subEO => subEO.isSelected());
	}

	/**
	 * adds a subbo to the subform at first row
	 * and starts editing of the first cell
	 */
	private addSubEO(subEO: SubEntityObject): void {
		this.addSubEOs([subEO]);

		if (this.gridOptions.api) {
			// focus first cell
			if (this.gridOptions.columnDefs && this.gridOptions.columnDefs.length > 0) {
				let colKey = this.gridOptions.columnDefs[0]['colId'];
				if (colKey) {
					this.gridOptions.api.startEditingCell(
						{
							rowIndex: 0,
							colKey: colKey
						}
					);
				}
			}
		}
	}

	/**
	 * add multiple subEos to the subform at first row
	 */
	private addSubEOs(subEOs: SubEntityObject[]): void {
		let dependents = this.eo.getDependents(
			this.webComponent.foreignkeyfieldToParent,
		);
		dependents.addAll(subEOs);
	}

	/**
	 * Flags the underlying Sub-EO as selected, when a row is selected.
	 *
	 * @param params
	 */
	rowSelected(params: { node: RowNode }) {
		let subEO: SubEntityObject = params.node.data;
		subEO.setSelected(params.node.isSelected());
	}

	protected getLogger(): Logger {
		return this.$log;
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = this.eo.getId();
		this.exportBoListTemplateState.refAttrId = this.subformMeta.getRefAttrId();
		this.exportBoListTemplateState.modalRef = this.modalService.open(this.exportBoListTemplateState.templateRef);
	}

	isReadonly() {
		if (!this.isEnabled()) {
			return true;
		}

		let subEORestriction = this.eo.getSubEORestriction(this.webComponent.foreignkeyfieldToParent);
		return subEORestriction === 'readonly';
	}

	isNewVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.newEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEORestriction(this.webComponent.foreignkeyfieldToParent);
		return subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete';
	}

	isNewEnabled() {
		return this.isNewVisible();
	}

	isEditEnabled() {
		if (this.isReadonly()) {
			return false;
		}

		return WebSubformComponent.isUndefinedOrTrue(this.webComponent.editEnabled);
	}

	isCloneVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.cloneEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEORestriction(this.webComponent.foreignkeyfieldToParent);
		return (subEORestriction !== 'nocreate' && subEORestriction !== 'nocreate,nodelete');
	}

	isCloneEnabled() {
		return this.isCloneVisible() && this.getSelectedSubEOs().length > 0;
	}

	isDeleteVisible() {
		if (this.isReadonly()) {
			return false;
		}

		if (!WebSubformComponent.isUndefinedOrTrue(this.webComponent.deleteEnabled)) {
			return false;
		}

		// NUCLOS-6134
		let subEORestriction = this.eo.getSubEORestriction(this.webComponent.foreignkeyfieldToParent);
		return subEORestriction !== 'nodelete' && subEORestriction !== 'nocreate,nodelete';

	}

	isDeleteEnabled() {
		return this.isDeleteVisible() && this.getSelectedSubEOs().length > 0;
	}

	hasSubformLayout() {
		if (this.subformMeta) {
			return this.subformMeta.getLinks().defaultLayout;
		}
		return false;
	}

	canOpenModal() {
		if (this.subformMeta) {
			return this.hasSubformLayout() && !this.webComponent.ignoreSubLayout;
		}
		return false;
	}

	isAnyButtonVisible() {
		return this.isNewVisible()
			|| this.isDeleteVisible()
			|| this.isCloneVisible()
			|| this.showPrintSearchResultListButton;
	}
}
