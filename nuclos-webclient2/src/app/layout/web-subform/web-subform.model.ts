import { EntityAttrMeta } from '../../entity-object/shared/bo-view.model';
export class SubformLayout {
	columns: SubformColumn[];

	static getCacheKey(parentEntityUid: string, subformEoMetaId: string): string {
		return parentEntityUid + '_' + subformEoMetaId;
	}
}

export class SubformColumn {
	displayName?: string;
	eoAttrFqn?: string;
	sort?: { direction: string, prio?: number };
	type?: string;
	uid?: string; // TODO rename in REST-service
	width: number;
}

export class RowEditCellParams {
	canOpenModal = false;

	constructor(private advancedProperties: {name, value}[]) {
	}

	getAdvancedProperty(key: string): string | undefined {
		if (!this.advancedProperties) {
			return undefined;
		}
		let property = this.advancedProperties.filter(p => p.name === key).shift();
		return property ? property.value : undefined;
	}
}

/**
 * additional parameters for CellRenderer components
 */
export interface NuclosCellRendererParams {
	editable: boolean;
	attrMeta: EntityAttrMeta;
}
