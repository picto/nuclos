import { ColDef, GridApi, RowNode } from 'ag-grid';
import { EntityObject, SubEntityObject } from '../../entity-object/shared/entity-object.class';
import { EntityObjectService } from '../../entity-object/shared/entity-object.service';
import { EntityObjectEventService } from '../../entity-object/shared/entity-object-event.service';

export abstract class AbstractCellComponent {

	private params: {
		value: any,
		api: GridApi,
		data: SubEntityObject,
		colDef: ColDef,
		node: RowNode
	};

	private eo: EntityObject | undefined;
	columnWidth;
	rowHeight;

	constructor(
		protected entityObjectService: EntityObjectService,
		protected eoEventService: EntityObjectEventService
	) {
	}

	init(params) {
		this.eoEventService.observeSelectedEo().subscribe(eo => {
			this.eo = eo;
		});
		this.params = params;
	}

	getParam(key: string) {
		return this.params[key];
	}

	updateParams(params) {
		this.params = params;
	}

	setEntityObject(entityObject: EntityObject) {
		this.eo = entityObject;
	}

	getEntityObject(): EntityObject | undefined {
		return this.eo;
	}

	setEntityObjectDirty(): void {
		if (this.eo) {
			this.eo.setDirty(true);
		}
	}

	getValue() {
		return this.params ? this.params.value : undefined;
	}

	setValue(value) {
		this.params.value = value;
	}

	getGridApi(): GridApi {
		return this.params.api;
	}

	getSubEntityObject(): SubEntityObject {
		return this.params.data || this.params.node.data;
	}

	getColDef(): ColDef {
		return this.params.colDef;
	}
}
