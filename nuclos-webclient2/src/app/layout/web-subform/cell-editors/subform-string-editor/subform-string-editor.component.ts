import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { Logger } from '../../../../log/shared/logger';
import { AbstractEditorComponent } from '../abstract-editor-component';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';

@Component({
	selector: 'nuc-subform-string-editor',
	templateUrl: './subform-string-editor.component.html',
	styleUrls: ['./subform-string-editor.component.css']
})
export class SubformStringEditorComponent extends AbstractEditorComponent {

	@ViewChild('input') input;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		private ref: ElementRef
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any): any {
		super.agInit(params);

		super.setValue(params.value);
		this.columnWidth = params.column.actualWidth + 'px';

		setTimeout(() => {
			$(this.ref.nativeElement).find('input').select();
		});
	}

	setStringValue(value) {
		this.setEntityObjectDirty();
		this.$log.debug('setStringValue(%o)', value);
		super.setValue(value);
	}
}
