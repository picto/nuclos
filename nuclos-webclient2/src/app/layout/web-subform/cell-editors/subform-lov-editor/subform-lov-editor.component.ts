import { Component, ElementRef, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { LovEntry } from '../../../../entity-object/shared/bo-view.model';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { LovDataService } from '../../../../entity-object/shared/lov-data.service';
import { FqnService } from '../../../../shared/fqn.service';
import { IdFactoryService } from '../../../../shared/id-factory.service';
import { AbstractLovEditorComponent } from '../abstract-lov-editor.component';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';
import { NuclosDefaults } from '../../../../shared/nuclos-defaults';

@Component({
	selector: 'nuc-subform-lov-editor',
	templateUrl: './subform-lov-editor.component.html',
	styleUrls: ['./subform-lov-editor.component.scss']
})
export class SubformLovEditorComponent extends AbstractLovEditorComponent {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected lovDataService: LovDataService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		fqnService: FqnService,
		injector: Injector
	) {
		super(
			entityObjectService,
			eoEventService,
			elementRef,
			idFactory,
			fqnService,
			injector
		);
	}

	loadEntries(): Observable<LovEntry[]> {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string): Observable<LovEntry[]> {
		let vlp = this.getVlp();
		return this.lovDataService.loadLovEntries(
			{
				attributeMeta: this.attributeMeta,
				eo: this.subEO,
				quickSearchInput: search,
				vlp: vlp,
				resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			}
		);
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}
}
