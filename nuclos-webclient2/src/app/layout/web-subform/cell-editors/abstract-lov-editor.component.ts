import { ElementRef, Injector, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { EntityAttrMeta, LovEntry } from '../../../entity-object/shared/bo-view.model';
import { SubEntityObject } from '../../../entity-object/shared/entity-object.class';
import { EntityObjectService } from '../../../entity-object/shared/entity-object.service';
import { FqnService } from '../../../shared/fqn.service';
import { IdFactoryService } from '../../../shared/id-factory.service';
import { AbstractLovComponent } from '../../abstract-lov/abstract-lov.component';
import { LovHandler } from '../../abstract-lov/lov-handler';
import { AbstractEditorComponent } from './abstract-editor-component';
import { EntityObjectEventService } from '../../../entity-object/shared/entity-object-event.service';

export const AUTOCOMPLETE_PANEL_SELECTOR = 'p-autocomplete .ui-autocomplete-panel';

export abstract class AbstractLovEditorComponent extends AbstractEditorComponent {

	/**
	 * Keys which must be caught (and not propagated) by the dropdown component,
	 * because they also trigger navigation in the subform.
	 */
	private static CATCH_KEYS = ['ArrowDown', 'ArrowUp', 'Enter', 'Escape', 'Tab'];

	@ViewChild('lovComponent') lovComponent: AbstractLovComponent<WebInputComponent>;

	protected lovHandler: LovHandler;

	lovEntries: LovEntry[] = [];
	protected attributeMeta: EntityAttrMeta;
	protected attribute: any;
	subEO: SubEntityObject;

	private staticValues: boolean;

	private componentId;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		protected fqnService: FqnService,
		protected injector: Injector
	) {
		super(entityObjectService, eoEventService);

		this.lovHandler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue(),
			getWidth: () => this.columnWidth,
			getHeight: () => this.rowHeight,
			searchReferenceClick: () => this.stopEditing(),
			addReferenceClick: () => this.stopEditing(),
			entrySelected: value => this.selectDropdownOption(value),
			isEnabled: () => true,
			keydownHandler: event => this.handleKey(event),
		};

		// when using the dropdown in the primeng p-autocomplete component the input element will be focused when clicking on the dropdown
		// this behaviour will always set back the focus to the input when trying to leave the input with a keyboard tab
		// disabling the dropdown button fixes this
		setTimeout(() => {
			let dropdownButton = this.elementRef.nativeElement.querySelector('button');
			if (dropdownButton) {
				$(dropdownButton).prop('disabled', true);
			}
		});
	}

	abstract loadEntries(): Observable<LovEntry[]>;

	abstract loadFilteredEntries(search: string): Observable<LovEntry[]>;

	getHandler() {
		return this.lovHandler;
	}

	protected getAutocomplete() {
		return this.lovComponent.getAutocomplete();
	}

	getEntityObject() {
		return this.subEO;
	}

	agInit(params: any) {

		super.agInit(params);

		this.attributeMeta = params.attrMeta;

		let attributeName = this.attributeMeta.getAttributeName();
		if (attributeName !== undefined && this.subEO !== undefined) {
			this.attribute = this.subEO.getAttribute(attributeName);
		}

		this.columnWidth = params.column.actualWidth + 'px';
		this.rowHeight = params.node.rowHeight + 'px';
		this.subEO = params.api.getModel().getRow(params.rowIndex).data;

		if (params.staticList) {
			this.lovEntries = params.staticList;
			this.staticValues = true;
		}

		// open dropdown when starting edit
		window.setTimeout(() => {
			this.lovComponent.showAllEntries();
			this.lovComponent.getAutocomplete().focus = true;
			this.lovComponent.getAutocomplete().focusInput();
		});
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let fieldName = this.fqnService.getShortAttributeName(this.subEO.getEntityClassId(), this.attributeMeta.getAttributeID());
		return fieldName;
	}

	getNameForHtml() {
		let result = this.getName();

		if (result) {
			result = 'attribute-' + result;
		}

		return result;
	}

	/**
	 * The ID of this component, to be used as HTML "id" attribute.
	 */
	getId(): string {
		if (!this.componentId) {
			this.componentId = this.getComponentIdOrNew();
		}

		return this.componentId;
	}

	private getComponentIdOrNew() {
		return 'generated-' + this.idFactory.getNextId();
	}

	isPopup() {
		return true;
	}

	selectDropdownOption(value) {
		let v = this.getValue();
		if (!v || v.id !== value.id) {
			this.setValue(value);
		}

		this.getGridApi().stopEditing();
	}

	getAttributeMeta(): EntityAttrMeta {
		return this.attributeMeta;
	}

	getAttribute(): any {
		return this.attribute;
	}

	stopEditing(): void {
		this.getGridApi().stopEditing();
	}

	private handleKey(event: KeyboardEvent) {
		if (AbstractLovEditorComponent.CATCH_KEYS.indexOf(event.code) >= 0) {
			event.preventDefault();
			event.stopPropagation();
		}

		if (event.code === 'Tab') {
			this.getGridApi().tabToNextCell();
		} else if (event.code === 'Escape') {
			this.stopEditing();
		}
	}
}
