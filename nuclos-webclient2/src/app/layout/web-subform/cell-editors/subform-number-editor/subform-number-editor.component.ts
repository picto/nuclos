import { Component, ElementRef, ViewChild } from '@angular/core';
import { EntityAttrMeta } from '../../../../entity-object/shared/bo-view.model';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { Logger } from '../../../../log/shared/logger';
import { NumberService } from '../../../../shared/number.service';
import { AbstractEditorComponent } from '../abstract-editor-component';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';

@Component({
	selector: 'nuc-subform-number-editor',
	templateUrl: './subform-number-editor.component.html',
	styleUrls: ['./subform-number-editor.component.css']
})
export class SubformNumberEditorComponent extends AbstractEditorComponent {

	@ViewChild('input') input;

	private attributeMeta: EntityAttrMeta;
	private stringValue = '';

	constructor(
		private numberService: NumberService,
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		private $log: Logger,
		private ref: ElementRef
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any): any {
		super.agInit(params);

		this.attributeMeta = params.attrMeta;
		this.columnWidth = params.column.actualWidth + 'px';

		let value = super.getValue();
		if (!isNaN(value)) {
			this.stringValue = this.numberService.format(value, this.attributeMeta.getPrecision());
		}

		setTimeout(() => {
			$(this.ref.nativeElement).find('input').select();
		});
	}

	getValue(): any {
		let result = this.numberService.parseNumber(this.stringValue, this.attributeMeta.getPrecision());
		this.$log.debug('getValue() = %o', result);
		return result;
	}

	getStringValue() {
		return this.stringValue;
	}

	setStringValue(value) {
		this.setEntityObjectDirty();
		this.$log.debug('setStringValue(%o)', value);
		this.stringValue = value;
	}
}
