import { AgEditorComponent } from 'ag-grid-angular';
import { EntityObjectService } from '../../../entity-object/shared/entity-object.service';
import { AbstractCellComponent } from '../abstract-cell-component';
import { EntityObjectEventService } from '../../../entity-object/shared/entity-object-event.service';
export abstract class AbstractEditorComponent extends AbstractCellComponent implements AgEditorComponent {
	value;

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.init(params);
	}

	getVlp(): WebValuelistProvider | undefined {
		return this.getParam('valuelistProvider');
	}
}
