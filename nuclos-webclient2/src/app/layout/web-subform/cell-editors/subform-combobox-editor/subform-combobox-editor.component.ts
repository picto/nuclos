import { Component, ElementRef, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { LovEntry } from '../../../../entity-object/shared/bo-view.model';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { FqnService } from '../../../../shared/fqn.service';
import { IdFactoryService } from '../../../../shared/id-factory.service';
import { ComboboxUtils } from '../../../shared/combobox-utils';
import { AbstractLovEditorComponent } from '../abstract-lov-editor.component';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';

@Component({
	selector: 'nuc-subform-combobox-editor',
	templateUrl: '../subform-lov-editor/subform-lov-editor.component.html',
	styleUrls: ['../subform-lov-editor/subform-lov-editor.component.scss']
})
export class SubformComboboxEditorComponent extends AbstractLovEditorComponent {

	constructor(
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
		protected elementRef: ElementRef,
		protected idFactory: IdFactoryService,
		protected fqnService: FqnService,
		injector: Injector
	) {
		super(
			entityObjectService,
			eoEventService,
			elementRef,
			idFactory,
			fqnService,
			injector
		);
	}

	loadEntries(): Observable<LovEntry[]> {
		return this.subEO.getLovEntries(
			this.attributeMeta.getAttributeID(),
			this.getVlp()
		).take(1);
	}

	loadFilteredEntries(search: string): Observable<LovEntry[]> {
		return ComboboxUtils.filter(this.loadEntries(), search);
	}
}
