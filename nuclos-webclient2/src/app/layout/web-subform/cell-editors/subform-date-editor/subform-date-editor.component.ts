import { Component, ViewChild } from '@angular/core';
import { NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { EntityAttrMeta } from '../../../../entity-object/shared/bo-view.model';
import { EntityObjectEventService } from '../../../../entity-object/shared/entity-object-event.service';
import { EntityObjectService } from '../../../../entity-object/shared/entity-object.service';
import { FqnService } from '../../../../shared/fqn.service';
import { WebDatechooserComponent } from '../../../web-datechooser/web-datechooser.component';
import { NuclosDatepickerI18n } from '../../../web-datechooser/web-datechooser.locale';
import { AbstractEditorComponent } from '../abstract-editor-component';

@Component({
	selector: 'nuc-subform-date-editor',
	templateUrl: 'subform-date-editor.component.html',
	styleUrls: ['subform-date-editor.component.css'],
	providers: [{provide: NgbDatepickerI18n, useClass: NuclosDatepickerI18n}]
})
export class SubformDateEditorComponent extends AbstractEditorComponent {

	@ViewChild('nuclosDatechooser') nuclosDatechooser: WebDatechooserComponent;

	protected attributeMeta: EntityAttrMeta;

	constructor(
		private fqnService: FqnService,
		entityObjectService: EntityObjectService,
		eoEventService: EntityObjectEventService,
	) {
		super(entityObjectService, eoEventService);
	}

	agInit(params: any) {
		super.agInit(params);

		this.attributeMeta = params.attrMeta;

		setTimeout(() => {
			this.nuclosDatechooser.focusInput();
			this.nuclosDatechooser.toggleDatepicker();
		});
	}

	getName() {
		if (!this.attributeMeta) {
			return undefined;
		}

		let eo = this.getSubEntityObject();
		if (!eo) {
			return;
		}
		let fieldName = this.fqnService.getShortAttributeName(eo.getEntityClassId(), this.attributeMeta.getAttributeID());
		return fieldName;
	}

	valueChanged() {
		this.updateStringValue();
		this.getGridApi().stopEditing();
	}

	private updateStringValue() {
		let value = this.nuclosDatechooser.getValue();
		let stringValue = '';

		let m = moment(value);
		if (m.isValid()) {
			stringValue = m.format('YYYY-MM-DD');
		}

		this.setValue(stringValue);
	}

	keydown(event: KeyboardEvent) {
		if (event.code === 'Enter') {
			this.nuclosDatechooser.commitValue();
			event.stopPropagation();
			event.preventDefault();
		} else if (event.code === 'Tab') {
			this.nuclosDatechooser.commitValue();
			event.stopPropagation();
			event.preventDefault();
			this.getGridApi().tabToNextCell();
		}
	}
}
