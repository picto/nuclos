/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WebEmailComponent } from './web-email.component';

describe('WebEmailComponent', () => {
	let component: WebEmailComponent;
	let fixture: ComponentFixture<WebEmailComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [WebEmailComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(WebEmailComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
