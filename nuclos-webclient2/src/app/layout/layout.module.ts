import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { BusyModule } from 'angular2-busy';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { TinymceModule } from 'angular2-tinymce';
import { NgUploaderModule } from 'ngx-uploader';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { ViewPreferencesConfigModule } from '../entity-object/view-preferences-config/view-preferences-config.module';
import { GridModule } from '../grid/grid.module';
import { I18nModule } from '../i18n/i18n.module';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { DatetimeService } from '../shared/datetime.service';
import { GridComponent } from './grid/grid.component';
import { LayoutComponent } from './layout.component';
import { ImageService } from './shared/image.service';
import { LayoutService } from './shared/layout.service';
import { WebButtonChangeStateComponent } from './web-button/web-button-change-state/web-button-change-state.component';
import { WebButtonDummyComponent } from './web-button/web-button-dummy/web-button-dummy.component';
import { WebButtonExecuteRuleComponent } from './web-button/web-button-execute-rule/web-button-execute-rule.component';
import { WebButtonGenerateObjectComponent } from './web-button/web-button-generate-object/web-button-generate-object.component';
import { WebButtonHyperlinkComponent } from './web-button/web-button-hyperlink/web-button-hyperlink.component';
import { WebCheckboxComponent } from './web-checkbox/web-checkbox.component';
import { WebComboboxComponent } from './web-combobox/web-combobox.component';
import { WebComponentComponent } from './web-component/web-component.component';
import { WebContainerComponent } from './web-container/web-container.component';
import { CustomNgbDateParserFormatter } from './web-datechooser/custom-ngbDateParserFormatter';
import { WebDatechooserComponent } from './web-datechooser/web-datechooser.component';
import { WebEmailComponent } from './web-email/web-email.component';
import { WebFileComponent } from './web-file/web-file.component';
import { WebGridCalculatedComponent } from './web-grid-calculated/web-grid-calculated.component';
import { WebHtmlEditorComponent } from './web-html-editor/web-html-editor.component';
import { WebHyperlinkComponent } from './web-hyperlink/web-hyperlink.component';
import { WebLabelStaticComponent } from './web-label-static/web-label-static.component';
import { WebLabelComponent } from './web-label/web-label.component';
import { AddReferenceTargetComponent } from './web-listofvalues/add-reference-target/add-reference-target.component';
import { EditReferenceTargetComponent } from './web-listofvalues/edit-reference-target/edit-reference-target.component';
import { SearchReferenceTargetComponent } from './web-listofvalues/search-reference-target/search-reference-target.component';
import { WebListofvaluesComponent } from './web-listofvalues/web-listofvalues.component';
import { WebMatrixComponent } from './web-matrix/web-matrix.component';
import { WebPanelComponent } from './web-panel/web-panel.component';
import { WebPasswordComponent } from './web-password/web-password.component';
import { WebPhonenumberComponent } from './web-phonenumber/web-phonenumber.component';
import {
	SubformBooleanRendererComponent,
	SubformComboboxEditorComponent,
	SubformDateEditorComponent,
	SubformDateRendererComponent,
	SubformDocumentRendererComponent,
	SubformLovEditorComponent,
	SubformNumberRendererComponent,
	SubformReferenceRendererComponent,
	SubformStateIconRendererComponent
} from './web-subform';
import { SubformNumberEditorComponent } from './web-subform/cell-editors/subform-number-editor/subform-number-editor.component';
import { SubformStringEditorComponent } from './web-subform/cell-editors/subform-string-editor/subform-string-editor.component';
import { SubformEditRowRendererComponent } from './web-subform/cell-renderer/subform-edit-row-renderer/subform-edit-row-renderer.component';
import { SubformButtonsComponent } from './web-subform/subform-buttons/subform-buttons.component';
import { WebSubformComponent } from './web-subform/web-subform.component';
import { WebSubformService } from './web-subform/web-subform.service';
import { WebTabcontainerComponent } from './web-tabcontainer/web-tabcontainer.component';
import { WebTableComponent } from './web-table/web-table.component';
import { WebTextareaComponent } from './web-textarea/web-textarea.component';
import { WebTextfieldComponent } from './web-textfield/web-textfield.component';

export function dateParserFactory(
	datetimeService: DatetimeService,
	$log: Logger,
	i18n: NuclosI18nService
) {
	return new CustomNgbDateParserFormatter(datetimeService, $log, i18n);
};

@NgModule({
	imports: [
		AgGridModule,
		AutoCompleteModule,
		CommonModule,
		FormsModule,

		AgGridModule.withComponents([
			SubformBooleanRendererComponent,
			SubformDateRendererComponent,
			SubformDocumentRendererComponent,
			SubformStateIconRendererComponent,
			SubformNumberRendererComponent,
			SubformReferenceRendererComponent,
			SubformEditRowRendererComponent,

			SubformComboboxEditorComponent,
			SubformDateEditorComponent,
			SubformLovEditorComponent,
			SubformNumberEditorComponent,
			SubformStringEditorComponent,
		]),

		I18nModule,
		ClickOutsideModule,

		BusyModule,
		NgbModule,
		NgUploaderModule,
		PrettyJsonModule,
		ViewPreferencesConfigModule,
		GridModule,

		TinymceModule.withConfig({
			selector: 'textarea',
			skin_url: 'assets/tinymce/skins/lightgray',
			plugins: ['autoheight', 'code', 'link', 'paste', 'table']
		})
	],
	declarations: [
		AddReferenceTargetComponent,
		EditReferenceTargetComponent,
		GridComponent,
		LayoutComponent,
		SearchReferenceTargetComponent,
		SubformBooleanRendererComponent,
		SubformComboboxEditorComponent,
		SubformDateEditorComponent,
		SubformDateRendererComponent,
		SubformDocumentRendererComponent,
		SubformLovEditorComponent,
		SubformNumberRendererComponent,
		SubformReferenceRendererComponent,
		SubformEditRowRendererComponent,
		WebButtonChangeStateComponent,
		WebButtonDummyComponent,
		WebButtonExecuteRuleComponent,
		WebButtonGenerateObjectComponent,
		WebButtonHyperlinkComponent,
		WebCheckboxComponent,
		WebComboboxComponent,
		WebComponentComponent,
		WebContainerComponent,
		WebDatechooserComponent,
		WebEmailComponent,
		WebFileComponent,
		WebGridCalculatedComponent,
		WebHyperlinkComponent,
		WebLabelComponent,
		WebLabelStaticComponent,
		WebListofvaluesComponent,
		WebMatrixComponent,
		WebPasswordComponent,
		WebPhonenumberComponent,
		WebSubformComponent,
		WebTabcontainerComponent,
		WebTableComponent,
		WebTextareaComponent,
		WebTextfieldComponent,
		SubformNumberEditorComponent,
		SubformStringEditorComponent,
		SubformEditRowRendererComponent,
		WebHtmlEditorComponent,
		WebPanelComponent,
		SubformStateIconRendererComponent,
		SubformButtonsComponent,
	],
	providers: [
		LayoutService,
		{
			provide: NgbDateParserFormatter,
			useFactory: dateParserFactory,
			deps: [DatetimeService, Logger, NuclosI18nService]
		},
		WebSubformService,
		ImageService,
	],
	exports: [
		LayoutComponent
	],
	// Add all dynamic WebLayout components here and in index.ts
	entryComponents: [
		WebButtonChangeStateComponent,
		WebButtonDummyComponent,
		WebButtonExecuteRuleComponent,
		WebButtonGenerateObjectComponent,
		WebButtonHyperlinkComponent,
		WebCheckboxComponent,
		WebComboboxComponent,
		WebComponentComponent,
		WebContainerComponent,
		WebDatechooserComponent,
		WebEmailComponent,
		WebFileComponent,
		WebHtmlEditorComponent,
		WebHyperlinkComponent,
		WebLabelComponent,
		WebLabelStaticComponent,
		WebListofvaluesComponent,
		WebMatrixComponent,
		WebPanelComponent,
		WebPasswordComponent,
		WebPhonenumberComponent,
		WebSubformComponent,
		WebTabcontainerComponent,
		WebTableComponent,
		WebTextareaComponent,
		WebTextfieldComponent,
	]
})
export class LayoutModule {
}
