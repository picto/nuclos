import { Component, Injector } from '@angular/core';
import { NuclosStateService } from '../../../state/shared/nuclos-state.service';
import { State } from '../../../state/shared/state';
import { WebButtonComponent } from '../web-button.component';

@Component({
	selector: 'nuc-web-button-change-state',
	templateUrl: '../web-button.component.html',
	styleUrls: ['../web-button.component.css']
})
export class WebButtonChangeStateComponent extends WebButtonComponent<WebButtonChangeState> {

	constructor(
		injector: Injector,
		private stateService: NuclosStateService
	) {
		super(injector);
	}

	buttonClicked() {
		let targetState: State = {
			nuclosStateId: this.webComponent.targetState
		} as State;
		this.stateService.changeState(this.eo, targetState).subscribe();
	}

	getCssClass(): string {
		return 'nuc-button-change-state';
	}

	/**
	 * This button is enabled if the target state is a possible next state for the EO.
	 *
	 * @returns {boolean}
	 */
	isEnabled(): boolean {
		if (!this.eo) {
			return false;
		}

		let nextStates = this.eo.getNextStates();
		if (!nextStates) {
			return false;
		}

		for (let nextState of nextStates) {
			if (nextState.nuclosStateId === this.webComponent.targetState) {
				return true;
			}
		}

		return false;
	}
}
