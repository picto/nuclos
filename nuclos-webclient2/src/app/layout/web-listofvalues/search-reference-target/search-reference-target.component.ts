import { Component, Input } from '@angular/core';
import { EntityObject } from '../../../entity-object/shared/entity-object.class';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '../../../shared/browser-refresh.service';
import { EntityAttrMeta } from '../../../entity-object/shared/bo-view.model';
import { LovDataService } from '../../../entity-object/shared/lov-data.service';
import { FqnService } from '../../../shared/fqn.service';

@Component({
	selector: 'nuc-search-reference-target',
	templateUrl: './search-reference-target.component.html',
	styleUrls: ['./search-reference-target.component.css']
})
export class SearchReferenceTargetComponent {
	@Input() private eo: EntityObject| undefined;
	@Input() private attributeMeta: EntityAttrMeta | undefined;
	@Input() private popupParameter;

	isVisible = false;

	constructor(
		private browserRefreshService: BrowserRefreshService,
		private fqnService: FqnService,
		private lovDataService: LovDataService
	) {
	}

	ngOnInit() {
		this.isVisible = this.attributeMeta !== undefined && this.lovDataService.canOpenReference(this.attributeMeta);
	}

	searchReference($event: MouseEvent) {

		let refAttrFqn = this.attributeMeta && this.attributeMeta.getReferencedEntityClassId();

		this.browserRefreshService.openEoInNewTab(
			refAttrFqn,
			undefined,
			false,
			true,
			false,
			this.popupParameter
		).subscribe(
			(message: SelectReferenceInOtherWindowEvent) => {
				if (this.eo && this.attributeMeta !== undefined) {
					let fieldName = this.fqnService.getShortAttributeName(this.eo.getEntityClassId(), this.attributeMeta.getAttributeID());
					if (fieldName !== undefined) {
						this.eo.setAttribute(fieldName, {
							id: message.eoId,
							name: message.name
						});
						this.eo.setDirty(true);
					}
				}
			}
		);
		$event.stopPropagation();
	}

}
