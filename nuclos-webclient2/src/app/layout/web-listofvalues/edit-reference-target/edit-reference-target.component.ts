import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { EntityAttrMeta, LovEntry } from '../../../entity-object/shared/bo-view.model';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '../../../shared/browser-refresh.service';
import { EntityObject } from '../../../entity-object/shared/entity-object.class';
import { LovDataService } from '../../../entity-object/shared/lov-data.service';
import { FqnService } from '../../../shared/fqn.service';


@Component({
	selector: 'nuc-edit-reference-target',
	templateUrl: './edit-reference-target.component.html',
	styleUrls: ['./edit-reference-target.component.css']
})
export class EditReferenceTargetComponent implements OnChanges {

	@Input() private attribute: LovEntry | undefined;
	@Input() private eo: EntityObject | undefined;
	@Input() private attributeMeta: EntityAttrMeta | undefined;
	@Input() private popupParameter;

	isVisible = false;

	constructor(
		private browserRefreshService: BrowserRefreshService,
		private fqnService: FqnService,
		private lovDataService: LovDataService
	) {
	}

	ngOnChanges(changes: SimpleChanges): void {
		this.isVisible = this.attributeMeta !== undefined
			&& this.attribute !== undefined && this.attribute !== null && this.attribute.id !== undefined && this.attribute.id !== null
			&& this.lovDataService.canOpenReference(this.attributeMeta);
	}

	editReference($event: MouseEvent) {
		if (this.attribute !== undefined && this.attribute.id) {

			let refAttrFqn = this.attributeMeta && this.attributeMeta.getReferencedEntityClassId();

			this.browserRefreshService.openEoInNewTab(
				refAttrFqn,
				this.attribute.id,
				true,
				false,
				true,
				this.popupParameter,
				true
			).subscribe(
				(message: SelectReferenceInOtherWindowEvent) => {
					if (this.attributeMeta !== undefined && this.eo !== undefined) {
						let fieldName = this.fqnService.getShortAttributeName(this.eo.getEntityClassId(), this.attributeMeta.getAttributeID());
						if (fieldName !== undefined) {
							this.eo.setAttributeUnsafe(fieldName, {
								id: message.eoId,
								name: message.name
							});
						}
						this.eo.setDirty(true);
					}
				}
			);

		}
		$event.stopPropagation();
	}

}
