import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../../../entity-object/shared/entity-object.class';
import { BrowserRefreshService, SelectReferenceInOtherWindowEvent } from '../../../shared/browser-refresh.service';
import { LovDataService } from '../../../entity-object/shared/lov-data.service';
import { EntityAttrMeta } from '../../../entity-object/shared/bo-view.model';
import { FqnService } from '../../../shared/fqn.service';

@Component({
	selector: 'nuc-add-reference-target',
	templateUrl: './add-reference-target.component.html',
	styleUrls: ['./add-reference-target.component.css']
})
export class AddReferenceTargetComponent implements OnInit {
	@Input() private eo: EntityObject;
	@Input() private attributeMeta: EntityAttrMeta;
	@Input() private popupParameter;

	isVisible = false;

	constructor(
		private browserRefreshService: BrowserRefreshService,
		private fqnService: FqnService,
		private lovDataService: LovDataService
	) {
	}

	ngOnInit() {
		this.lovDataService.canAddReference(this.eo, this.attributeMeta).subscribe(canAddReference => this.isVisible = canAddReference);
	}


	addReference($event: MouseEvent) {

		let refAttrFqn = this.attributeMeta && this.attributeMeta.getReferencedEntityClassId();

		this.browserRefreshService.openEoInNewTab(
			refAttrFqn,
			'new',
			true,
			false,
			true,
			this.popupParameter
		).subscribe(
			(message: SelectReferenceInOtherWindowEvent) => {
				let fieldName = this.fqnService.getShortAttributeName(this.eo.getEntityClassId(), this.attributeMeta.getAttributeID());
				if (fieldName !== undefined) {
					this.eo.setAttribute(fieldName, {
						id: message.eoId,
						name: message.name
					});
				}
				this.eo.setDirty(true);
			}
		);
		$event.stopPropagation();
	}

}

