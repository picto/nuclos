import { Component, ElementRef, Injector, OnInit } from '@angular/core';
import { LovDataService } from '../../entity-object/shared/lov-data.service';
import { FqnService } from '../../shared/fqn.service';
import { AbstractLovComponent } from '../abstract-lov/abstract-lov.component';
import { NuclosDefaults } from '../../shared/nuclos-defaults';

@Component({
	selector: 'nuc-web-listofvalues',
	templateUrl: '../abstract-lov/abstract-lov.component.html',
	styleUrls: ['../abstract-lov/abstract-lov.component.scss']
})
export class WebListofvaluesComponent extends AbstractLovComponent<WebListofvalues> implements OnInit {

	constructor(
		private lovDataService: LovDataService,
		injector: Injector,
		fqnService: FqnService,
		elementRef: ElementRef
	) {
		super(injector, fqnService, elementRef);

		this.handler = {
			loadEntries: () => this.loadEntries(),
			loadFilteredEntries: search => this.loadFilteredEntries(search),
			getValue: () => this.getValue()
		};
	}

	ngOnInit() {
		super.ngOnInit();
	}

	loadEntries() {
		return this.loadFilteredEntries('');
	}

	loadFilteredEntries(search: string) {
		return this.lovDataService.loadLovEntries(
			{
				attributeMeta: this.attributeMeta,
				eo: this.eo,
				quickSearchInput: search,
				vlp: this.webComponent.valuelistProvider,
				resultLimit: NuclosDefaults.DROPDOWN_SHOW_RESULT_LIMIT,
			}
		);
	}
}
