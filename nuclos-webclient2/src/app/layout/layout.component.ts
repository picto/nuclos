import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { EntityObjectEventListener } from '../entity-object/shared/entity-object-event-listener';
import { EntityObject } from '../entity-object/shared/entity-object.class';
import { LayoutService } from './shared/layout.service';
import { Logger } from '../log/shared/logger';
import { RuleService } from '../rule/shared/rule.service';

@Component({
	selector: 'nuc-layout',
	templateUrl: './layout.component.html',
	styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnChanges {
	@Input() eo: EntityObject | undefined;
	@Output() onSubmit = new EventEmitter<any>();

	webLayout: WebLayout | undefined;
	rules: Rules | undefined;
	busy;
	saving = false;

	private eoListener: EntityObjectEventListener;

	constructor(
		private layoutService: LayoutService,
		private ruleService: RuleService
	) {
		this.eoListener = {
			afterLayoutChange: (
				entityObject: EntityObject,
				_layoutURL: string
			) => {
				this.busy = this.updateLayout(entityObject).subscribe();
			},
			isSaving: (_eo, inProgress) => {
				this.saving = inProgress;
				Logger.instance.debug('Saving: %o', this.saving);
			}
		};
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChanges = changes['eo'];
		if (eoChanges) {
			let previousEO = changes['eo'].previousValue;

			if (previousEO) {
				previousEO.removeListener(this.eoListener);
			}

			let eo: EntityObject = changes['eo'].currentValue;
			if (eo) {
				eo.addListener(this.eoListener);
				this.busy = this.updateLayout(eo).subscribe();
			}
		}
	}

	private updateLayout(eo: EntityObject): Observable<any> {
		return this.layoutService.getWebLayout(eo).do(
			weblayout => {
				this.webLayout = weblayout;
				this.ruleService.updateRuleExecutor(eo).subscribe();
			},
		);
	}

	submit() {
		// TODO: Submitting the form somehow also triggers adding of a new row in the subform
		this.onSubmit.next();
	}
}
