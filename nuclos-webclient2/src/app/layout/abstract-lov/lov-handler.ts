import { Observable } from 'rxjs/Observable';
import { LovEntry } from '../../entity-object/shared/bo-view.model';

export abstract class LovHandler {
	static DUMMY: LovHandler = {
		loadEntries: () => Observable.of([]),
		loadFilteredEntries: () => Observable.of([]),
		getValue: () => undefined,
	};

	abstract loadEntries(): Observable<LovEntry[]>;

	abstract loadFilteredEntries(search: string): Observable<LovEntry[]>;

	abstract getValue(): any;

	abstract refreshEntries?(): any;

	abstract entrySelected?(value?: any);

	abstract isEnabled?(): boolean;

	abstract getWidth?();

	abstract getHeight?();

	abstract searchReferenceClick?();

	abstract addReferenceClick?();

	abstract keydownHandler?(event);
}
