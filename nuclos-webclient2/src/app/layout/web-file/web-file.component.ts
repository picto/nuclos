import { Component, Inject, Injector, NgZone, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NgUploaderOptions, UploadedFile } from 'ngx-uploader';
import { Observable } from 'rxjs';
import { DialogService } from '../../dialog/dialog.service';
import { AttrType, DocumentAttribute } from '../../entity-object/shared/bo-view.model';
import { EntityObjectEventListener } from '../../entity-object/shared/entity-object-event-listener';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { AbstractInputComponent } from '../shared/abstract-input-component';
import { ImageService } from '../shared/image.service';
import { Logger } from '../../log/shared/logger';

@Component({
	selector: 'nuc-web-file',
	templateUrl: './web-file.component.html',
	styleUrls: ['./web-file.component.scss']
})
export class WebFileComponent extends AbstractInputComponent<WebFile> implements OnInit, OnChanges {


	private uploadToken: string;
	hasDropZoneOver: boolean;
	isImage: boolean;
	isUploaded: boolean;
	options: NgUploaderOptions;
	documentIsSelected = false;
	showUploadLabel: boolean;

	fileName: string;
	fileIconUrl: string | undefined;
	uploadedFile: UploadedFile;

	private eoListener: EntityObjectEventListener = {
		afterSave: () => this.initDocument(),
		afterCancel: () => this.initDocument(),
		afterReload: () => this.initDocument()
	};

	constructor(
		injector: Injector,
		private nuclosConfigService: NuclosConfigService,
		private dialogService: DialogService,
		private nuclosI18nService: NuclosI18nService,
		private imageService: ImageService,
		@Inject(NgZone) private zone: NgZone
	) {
		super(injector);
	}

	ngOnInit() {
		this.options = new NgUploaderOptions({
			url: this.nuclosConfigService.getRestHost() + '/boDocuments/upload',
			withCredentials: true
		});

		if (this.eo) {
			this.eo.addListener(this.eoListener);
		}

		this.initDocument();
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			let previousEo = eoChange.previousValue;
			if (previousEo) {
				previousEo.removeListener(this.eoListener);
			}
			let currentEo = eoChange.currentValue;
			if (currentEo) {
				currentEo.addListener(this.eoListener);
			}
			this.initDocument();
		}
	}


	private initDocument() {
		this.documentIsSelected = false;
		delete this.fileName;
		delete this.uploadToken;

		this.showUploadLabel = true;
		let documentAttribute = this.getDocumentAttribute();
		if (documentAttribute) {
			this.fileName = documentAttribute.name;
			this.documentIsSelected = !!documentAttribute.id;

			this.fileIconUrl = this.getFileIconUrl();

			if (documentAttribute.id || this.getImageUrl()) {
				this.isUploaded = true;
				this.showUploadLabel = false;
			}
		}
		this.getIsImage().subscribe(isImage => {
			this.isImage = isImage;
		});

		if (this.hasHref()) {
			this.documentIsSelected = true;
		}
	}

	handleUpload(uploadedFile: UploadedFile) {

		if (uploadedFile.status && uploadedFile.status !== 200) {
			this.dialogService.alert(
				{
					title: this.nuclosI18nService.getI18n('webclient.error.title'),
					message: uploadedFile.response
				}
			);
		}

		setTimeout(() => {
			this.zone.run(() => {
				this.uploadedFile = uploadedFile;
				this.uploadToken = this.uploadedFile.response;
				this.resetDocument();

				this.fileName = this.uploadedFile.originalName;
				let fileExtension = uploadedFile.originalName.split('.').pop();
				if (fileExtension) {
					this.fileIconUrl = this.getFileIconUrlForFileType(fileExtension);
				}
				this.documentIsSelected = true;
				this.setDocumentAttribute(
					{
						'uploadToken': this.uploadToken,
						'name': this.fileName
					}
				);

				this.isUploaded = false;
				this.showUploadLabel = false;
			});
		});
	}

	fileOver(e: boolean) {
		this.hasDropZoneOver = e;
	}

	hasDocument(): boolean {
		let result = false;

		if (this.isImage) {
			result = this.hasHref();
		} else {
			let attribute = this.getDocumentAttribute();
			result = !!(attribute && attribute.id);
		}

		return result;
	}

	private hasHref() {
		let href = this.imageService.getImageHref(
			this.getEntityObject(),
			this.webComponent.name
		);
		return !!href;
	}

	openImageModal() {
		let imageUrl = this.getImageUrl();

		if (!imageUrl) {
			Logger.instance.warn('Can not open empty image URL');
			return;
		}

		this.imageService.openImage(imageUrl);
	}

	clickOnFile() {
		if (this.documentIsSelected) {
			if (this.isImage) {
				this.openImageModal();
			} else {
				if (this.hasDocument()) {
					this.openDocument();
				}
			}
		}
	}


	getFileIconUrl(): string | undefined {
		let documentAttribute = this.getDocumentAttribute();
		if (!documentAttribute || !documentAttribute.id) {
			return undefined;
		}
		let fileType = 'generic';
		if (documentAttribute && documentAttribute.fileType) {
			fileType = documentAttribute.fileType;
		}
		return this.getFileIconUrlForFileType(fileType.toLowerCase());
	}

	private guessFileTypeFromFileExtension(extension: string | undefined): string | undefined {
		switch (extension) {
			case 'generic':
				return 'generic';
			case 'pdf':
				return 'pdf';
			case 'xls':
			case 'xlsx':
				return 'excel';
			case 'ppt':
			case 'pptx':
				return 'powerpoint';
			case 'doc':
			case 'docx':
				return 'word';
			case 'txt':
				return 'text';
			default:
				return undefined;
		}
	}

	private getFileIconUrlForFileType(fileType: string): string | undefined {
		let fileTypePrefix = this.guessFileTypeFromFileExtension(fileType);
		return fileTypePrefix ? 'assets/file/' + fileTypePrefix.toLowerCase() + '-file.png' : undefined;
	}


	protected openDocument() {
		let docAttrId = this.eo.getEntityClassId() + '_' + this.webComponent.name;
		// TODO: HATEOAS
		let selfUrl = this.eo.getSelfURL();
		if (selfUrl) {
			let link;
			if (this.isImage) {
				link = selfUrl.replace('/bos/', '/boImages/') + '/images/' + docAttrId + '/' + this.eo.getAttribute('version');
			} else {
				link = selfUrl.replace('/bos/', '/boDocuments/') + '/documents/' + docAttrId;
			}
			window.open(link);
		}
	}

	removeDocument(): void {
		this.resetDocument();
		delete this.uploadToken;
		delete this.fileIconUrl;
	}

	protected resetDocument(): void {
		this.eo.setAttribute(this.webComponent.name, null);
		delete this.fileName;
		this.documentIsSelected = false;
		this.showUploadLabel = true;

		let attrImages = this.eo.getData().attrImages;
		if (attrImages && attrImages.links) {
			if (attrImages.links.image) {
				delete attrImages.links.image;
			}
			if (attrImages.links[this.webComponent.name]) {
				delete attrImages.links[this.webComponent.name];
			}
		}
	}

	private getIsImage(): Observable<boolean> {
		// TODO
		return new Observable(
			observer => {
				this.eo.getMeta().subscribe(
					meta => {
						let attribute = meta.getAttribute(this.webComponent.name);
						if (attribute) {
							observer.next(attribute.type === AttrType.IMAGE);
							observer.complete();
						}
					}
				);
			}
		);
	}

	/**
	 * @return undefined if attribute is image
	 */
	getDocumentAttribute(): DocumentAttribute | undefined {
		return this.eo.getAttribute(this.webComponent.name);
	}

	setDocumentAttribute(value): void {
		return this.eo.setAttribute(this.webComponent.name, value);
	}

	getImageUrl() {
		if (!this.documentIsSelected) {
			return;
		}

		if (this.getEntityObject().isDirty() && this.uploadToken) {
			// temporarily uploaded file
			return this.nuclosConfigService.getRestHost() + '/boImages/temp/' + this.uploadToken;
		}

		return this.imageService.getImageHref(this.getEntityObject(), this.getImageAttributeId());
	}

	protected getImageAttributeId() {
		return this.webComponent.name;
	}
}
