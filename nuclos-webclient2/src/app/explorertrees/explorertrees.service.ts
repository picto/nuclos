import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { WsTab } from './explorertrees.model';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';

@Injectable()
export class ExplorerTreesService {

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
	}

	getMenuSelector(): Observable<WsTab[]> {

		return this.http
			.get(this.nuclosConfig.getRestHost() + '/meta/sideviewmenuselector', {withCredentials: true})
			.map((response: Response) => response.json());
	}

}
