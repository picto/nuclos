import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ExplorerTreesComponent } from './explorertrees.component';
import { I18nModule } from '../i18n/i18n.module';

@NgModule({
	imports: [
		CommonModule,
		I18nModule,
		NgbModule,
	],
	exports: [
		ExplorerTreesComponent
	],
	declarations: [
		ExplorerTreesComponent
	]
})
export class ExplorerTreesModule {
}
