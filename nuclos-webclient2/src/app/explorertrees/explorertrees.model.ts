export interface WsTab {
	type: string;
	label: string;
	boMetaId: string;
	boId: string;
}
