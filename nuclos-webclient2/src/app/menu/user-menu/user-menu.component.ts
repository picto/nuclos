import { Component, Input, OnInit } from '@angular/core';
import { AuthenticationService, MandatorData } from '../../authentication/authentication.service';
import { DialogService } from '../../dialog/dialog.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PreferencesResetModalComponent } from './preferences-reset-modal/preferences-reset-modal.component';

@Component({
	selector: 'nuc-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {
	@Input() loggedIn = false;
	@Input() username: string | undefined;
	@Input() anonymous: boolean;

	mandators: Array<MandatorData>;
	currentMandator: MandatorData;

	constructor(private authenticationService: AuthenticationService,
				private dialogService: DialogService,
				private modalService: NgbModal,
				private i18n: NuclosI18nService) {
	}

	ngOnInit() {

		this.authenticationService.observeLoginStatus().subscribe(() => {
			let authData = this.authenticationService.getAuthentication();
			if (authData) {
				if (authData.mandators) {
					this.mandators = authData.mandators;
				}
				if (authData.mandator) {
					this.currentMandator = authData.mandator;
				}
			}

		});
	}

	selectMandator(mandator: MandatorData) {
		this.currentMandator = mandator;
		this.authenticationService.selectMandator(mandator).subscribe(() => {
			$('body').css('cursor', 'progress');
			this.authenticationService.navigateHome();
			window.location.reload();
		});
	}

	resetWorkspace() {
		this.dialogService.confirm({
				title: this.i18n.getI18n('webclient.user.resetworkspace'),
				message: this.i18n.getI18n('webclient.user.resetworkspace.confirm')
			}
		).then(
			() => {
				// ok, resetworkspace
				this.authenticationService.resetWorkspace();
			},
			() => {
				// cancel
			}
		);
	}

	openPreferencesResetModal() {
		this.modalService.open(PreferencesResetModalComponent, {size: 'lg'});
	}
}
