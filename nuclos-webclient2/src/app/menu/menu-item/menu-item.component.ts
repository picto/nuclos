import { Component, Input, OnInit } from '@angular/core';
import { NuclosConfigService } from '../../shared/nuclos-config.service';

@Component({
	selector: 'nuc-menu-item',
	templateUrl: './menu-item.component.html',
	styleUrls: ['./menu-item.component.css', '../menu.component.css']
})
export class MenuItemComponent implements OnInit {

	iconBaseHref: string;

	@Input() entries: any[];

	// nesting level
	@Input() level: number;

	constructor(
		private config: NuclosConfigService
	) {
	}

	ngOnInit() {
		this.iconBaseHref = this.config.getRestHost() + '/meta/icon'
	}

}
