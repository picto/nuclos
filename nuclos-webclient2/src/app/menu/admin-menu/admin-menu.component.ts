import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'nuc-admin-menu',
	templateUrl: './admin-menu.component.html',
	styleUrls: ['./admin-menu.component.css']
})
export class AdminMenuComponent implements OnInit {
	@Input() superUser = false;

	constructor() {
	}

	ngOnInit() {
	}

}
