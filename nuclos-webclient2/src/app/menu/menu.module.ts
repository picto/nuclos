import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MenuService } from '.';
import { DisclaimerModule } from '../disclaimer/disclaimer.module';
import { HttpModule } from '../http/http.module';
import { I18nModule } from '../i18n/i18n.module';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { FullTextSearchComponent } from './full-text-search/full-text-search.component';
import { FullTextSearchService } from './full-text-search/full-text-search.service';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuComponent } from './menu.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { PreferencesResetModalComponent } from './user-menu/preferences-reset-modal/preferences-reset-modal.component';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,

		HttpModule,
		I18nModule,
		DisclaimerModule
	],
	declarations: [
		MenuComponent,
		MenuItemComponent,
		UserMenuComponent,
		AdminMenuComponent,
		FullTextSearchComponent,
		PreferencesResetModalComponent
	],
	exports: [
		MenuComponent
	],
	providers: [
		MenuService,
		FullTextSearchService
	],
	entryComponents: [
		PreferencesResetModalComponent
	]
})
export class MenuModule {
}
