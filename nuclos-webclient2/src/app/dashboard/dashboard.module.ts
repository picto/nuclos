import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from '.';
import { I18nModule } from '../i18n/i18n.module';
import { DashboardRoutes } from './dashboard.routes';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,

		I18nModule,

		DashboardRoutes
	],
	declarations: [
		DashboardComponent
	]
})
export class DashboardModule {
}
