import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrintoutComponent } from './printout.component';
import { DialogModule } from '../dialog/dialog.module';
import { I18nModule } from '../i18n/i18n.module';
import { PrintoutService } from './shared/printout.service';
import { PrintoutDialogComponent } from './printout-dialog/printout-dialog.component';
import { FormsModule } from '@angular/forms';
import { AutoCompleteModule } from 'primeng/primeng';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationModule } from '../authentication/authentication.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		AuthenticationModule,
		DialogModule,
		I18nModule,

		AutoCompleteModule,
		NgbModule,
	],
	declarations: [
		PrintoutComponent,
		PrintoutDialogComponent
	],
	providers: [
		PrintoutService
	],
	exports: [
		PrintoutComponent,
	],
	entryComponents: [
		PrintoutDialogComponent
	]
})
export class PrintoutModule {
}
