import { Component, Input, OnInit } from '@angular/core';
import { EntityObject } from '../entity-object/shared/entity-object.class';
import { PrintoutService } from './shared/printout.service';

@Component({
	selector: 'nuc-printout',
	templateUrl: './printout.component.html',
	styleUrls: ['./printout.component.css']
})
export class PrintoutComponent implements OnInit {

	@Input() eo: EntityObject;

	constructor(private printoutService: PrintoutService) {
	}

	ngOnInit() {
	}

	/**
	 * open printout dialog
	 */
	openPrintoutDialog() {
		this.printoutService.openPrintoutDialog(this.eo).subscribe();
	}

}
