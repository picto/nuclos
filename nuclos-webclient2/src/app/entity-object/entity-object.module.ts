import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ResizableModule } from 'angular-resizable-element';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { CommandModule } from '../command/command.module';
import { GenerationModule } from '../generation/generation.module';
import { I18nModule } from '../i18n/i18n.module';
import { InputRequiredModule } from '../input-required/input-required.module';
import { LayoutModule } from '../layout/layout.module';
import { PerspectiveModule } from '../perspective/perspective.module';
import { PrintoutModule } from '../printout/printout.module';
import { RuleModule } from '../rule/rule.module';
import { StateModule } from '../state/state.module';
import { DetailButtonsComponent } from './detail-buttons/detail-buttons.component';
import { DetailModalComponent } from './detail-modal/detail-modal.component';
import { DetailToolbarGenerationItemComponent } from './detail-toolbar-generation-item/detail-toolbar-generation-item.component';
import { DetailToolbarItemComponent } from './detail-toolbar-item/detail-toolbar-item.component';
import { DetailToolbarLockingItemComponent } from './detail-toolbar-locking-item/detail-toolbar-locking-item.component';
import { DetailToolbarPrintoutItemComponent } from './detail-toolbar-printout-item/detail-toolbar-printout-item.component';
import { DetailToolbarStateItemComponent } from './detail-toolbar-state-item/detail-toolbar-state-item.component';
import { DetailToolbarComponent } from './detail-toolbar/detail-toolbar.component';
import { DetailComponent } from './detail/detail.component';
import { MandatorStripeComponent } from './detail/mandator-stripe/mandator-stripe.component';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityObjectComponent, EntityObjectPopupComponent } from './entity-object.component';
import { EntityObjectRoutes } from './entity-object.routes';
import { LegacyRouteComponent } from './legacy-route/legacy-route.component';
import { LockingComponent } from './locking/locking.component';
import { NavigationGuard } from './navigation-guard';
import { DataService } from './shared/data.service';
import { DetailModalService } from './shared/detail-modal.service';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';
import { EntityObjectService } from './shared/entity-object.service';
import { LovDataService } from './shared/lov-data.service';
import { MetaService } from './shared/meta.service';
import { SelectableService } from './shared/selectable.service';
import { SideviewmenuService } from './shared/sideviewmenu.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ExportBoListComponent } from './sidebar/statusbar/export-bo-list/export-bo-list.component';
import {
	ExportBoListTemplateDirective,
	ExportBoListTemplateState,
	StatusbarComponent
} from './sidebar/statusbar/statusbar.component';
import { SideTreeComponent } from './sidetree/sidetree.component';
import { ViewPreferencesConfigModule } from './view-preferences-config/view-preferences-config.module';
import { AuthenticationModule } from '../authentication/authentication.module';
import { EntityObjectResultService } from './shared/entity-object-result.service';
import { EntityObjectNavigationService } from './shared/entity-object-navigation.service';
import { EntityObjectEventService } from './shared/entity-object-event.service';
import { EntityObjectErrorService } from './shared/entity-object-error.service';
import { ExplorerTreesModule } from '../explorertrees/explorertrees.module';
import { SearchtemplateModule } from '../searchtemplate/searchtemplate.module';
import { ExplorerTreesService } from '../explorertrees/explorertrees.service';
import { EntityObjectResultUpdateService } from './shared/entity-object-result-update.service';
import { CardLayoutComponent } from './sidebar/layout/card-layout/card-layout.component';
import { StorageModule } from '../storage/storage.module';
import { GridModule } from '../grid/grid.module';
import { AutonumberService } from './shared/autonumber.service';

export function navGuardFactory(eoResultService: EntityObjectResultService) {
	return new NavigationGuard(eoResultService);
}

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		AuthenticationModule,
		CommandModule,
		EntityObjectRoutes,
		ExplorerTreesModule,
		GenerationModule,
		I18nModule,
		InfiniteScrollModule,
		InputRequiredModule,
		LayoutModule,
		PerspectiveModule,
		PrintoutModule,
		RuleModule,
		SearchtemplateModule,
		StateModule,
		ViewPreferencesConfigModule,
		StorageModule,
		GridModule,

		AgGridModule,
		PrettyJsonModule,
		ResizableModule,
	],
	declarations: [
		EntityObjectComponent,
		EntityObjectPopupComponent,
		SidebarComponent,
		SideTreeComponent,
		DetailComponent,
		StatusbarComponent,
		ExportBoListTemplateDirective,
		ExportBoListComponent,
		DetailButtonsComponent,
		LockingComponent,
		MandatorStripeComponent,
		DetailToolbarComponent,
		DetailToolbarItemComponent,
		DetailToolbarStateItemComponent,
		DetailToolbarGenerationItemComponent,
		DetailToolbarLockingItemComponent,
		LegacyRouteComponent,
		DetailToolbarPrintoutItemComponent,
		DetailModalComponent,
		CardLayoutComponent
	],
	providers: [
		MetaService,
		DataService,
		SelectableService,
		SideviewmenuService,
		ExplorerTreesService,
		ExportBoListTemplateState,
		EntityObjectEventService,
		EntityObjectErrorService,
		EntityObjectService,
		EntityObjectNavigationService,
		EntityObjectPreferenceService,
		EntityObjectResultService,
		EntityObjectResultUpdateService,
		EntityObjectRouteService,
		LovDataService,
		{
			provide: NavigationGuard,
			useFactory: navGuardFactory,
			deps: [EntityObjectResultService]
		},
		DetailModalService,
		AutonumberService,
	],
	exports: [
		DetailModalComponent
	],
	entryComponents: [
		DetailModalComponent
	]
})
export class EntityObjectModule {
}
