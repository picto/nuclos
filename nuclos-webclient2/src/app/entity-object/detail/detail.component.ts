import { Component, Input, OnInit } from '@angular/core';
import { EntityMeta } from '../shared/bo-view.model';
import { EntityObject } from '../shared/entity-object.class';
import { EntityObjectEventService } from '../shared/entity-object-event.service';

@Component({
	selector: 'nuc-detail',
	templateUrl: './detail.component.html',
	styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
	@Input()
	meta: EntityMeta;

	@Input()
	boId: number;

	@Input()
	eo: EntityObject | undefined;

	@Input()
	canCreateBo: boolean;

	@Input()
	triggerUnsavedChangesPopover: Date;

	constructor(
		private eoEventService: EntityObjectEventService
	) {
	}

	ngOnInit() {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.eo = eo
		);
	}
}
