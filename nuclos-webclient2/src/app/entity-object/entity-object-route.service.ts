import { Injectable } from '@angular/core';
import { Params } from '@angular/router';
import { Logger } from '../log/shared/logger';
import { EntityObjectNavigationService } from './shared/entity-object-navigation.service';
import { EntityObjectResultService } from './shared/entity-object-result.service';

@Injectable()
export class EntityObjectRouteService {

	constructor(
		private eoNavigationService: EntityObjectNavigationService,
		// private eoResultService: EntityObjectResultService,
	) {
	}

	/**
	 * TODO: Fix cyclic dependencies and use injection.
	 */
	private getResultService() {
		return EntityObjectResultService.instance;
	}

	handleParams(params: Params) {
		let eoId = params['entityObjectId'];

		let entityClassId = params['entityClassId'];
		this.getResultService().selectEntityClassId(entityClassId);

		// Load the selected EO or reset
		if (eoId) {
			if (eoId === 'new') {
				this.createNewEO();
			} else {
				this.getResultService().selectEoByClassAndId(entityClassId, eoId)
					// .catch(() => this)
				// TODO: Catch error and redirect to error page
					.subscribe();
			}
		}
	}

	private createNewEO() {
		this.getResultService().createNew().subscribe(
			eo => {
				eo.select();
				this.eoNavigationService.navigateToEo(eo);
			}
		);
	}
}
