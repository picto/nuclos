import { Component, Input, OnInit } from '@angular/core';
import { Preference, SideviewmenuPreferenceContent } from '../../../../preferences/preferences.model';
import { SidebarComponent } from '../../sidebar.component';
import { SideviewmenuService } from '../../../shared/sideviewmenu.service';
import { EntityObjectPreferenceService } from '../../../shared/entity-object-preference.service';
import { Logger } from '../../../../log/shared/logger';
import { EntityObjectNavigationService } from '../../../shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../../../shared/entity-object-result.service';
import { Observable } from 'rxjs/Observable';
import { EntityObject } from '../../../shared/entity-object.class';
import { DataService } from '../../../shared/data.service';
import { EntityMeta } from '../../../shared/bo-view.model';
import { FqnService } from '../../../../shared/fqn.service';

@Component({
	selector: 'nuc-card-layout',
	templateUrl: './card-layout.component.html',
	styleUrls: ['./card-layout.component.css', '../../sidebar.component.scss']
})
export class CardLayoutComponent extends SidebarComponent implements OnInit {

	/* tslint:disable:no-input-rename */
	@Input('sidebar') private sidebarComponent: SidebarComponent;

	@Input()
	public sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	constructor(sideviewmenuService: SideviewmenuService,
				eoNavigationService: EntityObjectNavigationService,
				eoResultService: EntityObjectResultService,
				entityObjectPreferenceService: EntityObjectPreferenceService,
				$log: Logger,
				private fqnService: FqnService,
				private dataService: DataService) {
		super(sideviewmenuService, eoNavigationService, eoResultService, entityObjectPreferenceService, $log);
	}

	ngOnInit() {
		this.loadEoOnScroll();
	}

	getTitle(eo: EntityObject): Observable<string> {
		if (eo.title) {
			return Observable.of(eo.title);
		}
		return new Observable<string>(observer => {
			let meta = this.getMeta();
			if (meta) {
				eo.title = this.formatTitleOrInfo(meta.getTitlePattern(), eo, meta);
				observer.next(eo.title);
				observer.complete();
			}
		});
	}

	getInfo(eo: EntityObject): Observable<string> {
		if (eo.info) {
			return Observable.of(eo.info);
		}
		return new Observable<string>(observer => {
			let meta = this.getMeta();
			if (meta) {
				eo.info = this.formatTitleOrInfo(meta.getInfoPattern(), eo, meta);
				observer.next(eo.info);
				observer.complete();
			}
		});
	}

	private formatTitleOrInfo(inputString: string | undefined, eo: EntityObject, meta: EntityMeta): string {

		if (inputString === undefined) {
			return '';
		}

		return this.fqnService.formatFqnString(inputString, (fqn) => {
			let attribute = eo.getAttribute(fqn);
			let attributeMeta = meta.getAttributeMetaByFqn(fqn);
			if (attribute && attributeMeta) {
				return this.dataService.formatAttribute(attribute, attributeMeta);
			}
			return '';
		});
	}

}
