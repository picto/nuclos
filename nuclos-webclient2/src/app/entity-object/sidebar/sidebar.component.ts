import {
	Component,
	ElementRef,
	EventEmitter,
	HostListener,
	Input,
	OnDestroy,
	OnInit,
	Output,
	ViewChild
} from '@angular/core';
import { ResizeEvent } from 'angular-resizable-element';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { Logger } from '../../log/shared/logger';
import {
	ColumnAttribute,
	Preference,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { EntityMetaData } from '../shared/bo-view.model';
import { EntityObjectNavigationService } from '../shared/entity-object-navigation.service';
import { EntityObjectPreferenceService } from '../shared/entity-object-preference.service';
import { EntityObjectResultService } from '../shared/entity-object-result.service';
import { EntityObject } from '../shared/entity-object.class';
import { SideviewmenuService } from '../shared/sideviewmenu.service';
import { LoadMoreResultsEvent } from '../entity-object.component';


export class ColumnResizing {
	column: ColumnAttribute;
	startX: number;
	startWidth: number;
	htmlElement: EventTarget;
}


@Component({
	selector: 'nuc-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {

	@Input()
	meta: EntityMetaData;

	@Input()
	sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	@Output()
	loadMoreData: EventEmitter<LoadMoreResultsEvent> = new EventEmitter<LoadMoreResultsEvent>();

	@Output()
	onSort: EventEmitter<Preference<SideviewmenuPreferenceContent>> =
		new EventEmitter<Preference<SideviewmenuPreferenceContent>>();

	@Output()
	onColumnChange: EventEmitter<Preference<SideviewmenuPreferenceContent>> =
		new EventEmitter<Preference<SideviewmenuPreferenceContent>>();

	@ViewChild('listContainer') private listContainerElement: ElementRef;
	@ViewChild('sidebar') private sidebar: ElementRef;

	sidebarLayoutType: SidebarLayoutType;

	selectedColumns: ColumnAttribute[] = [];

	private columnResizing: ColumnResizing;

	private sidebarScroll: Subject<number> = new Subject<number>();

	private subscriptions: Subscription[] = [];

	constructor(
		private sideviewmenuService: SideviewmenuService,
		private eoNavigationService: EntityObjectNavigationService,
		private eoResultService: EntityObjectResultService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private $log: Logger
	) {
	}

	/**
	 * TODO: Fix circular dependencies and use normal service injection.
	 */
	private getResultService() {
		return this.eoResultService; // EntityObjectResultService.instance;
	}

	ngOnInit() {
		if (this.sideviewmenuPreference) {
			this.selectedColumns = this.selectedSortedColumns();
		}
		this.loadEoOnScroll();

		this.subscriptions.push(
			this.entityObjectPreferenceService.selectedSideviewmenuPref$.subscribe(pref => {
				if (pref) {
					this.sideviewmenuPreference = pref

					// TODO: Handle missing column names.
					this.selectedColumns = this.selectedSortedColumns();

					this.$log.debug('Selected columns: %o', this.selectedColumns);
				}
			})
		);

		this.subscriptions.push(
			this.sideviewmenuService.onSideviewmenuPrefChange().subscribe(
				() => {
					this.onColumnChange.emit(this.sideviewmenuPreference);
				}
			)
		);

		this.subscriptions.push(
			this.sideviewmenuService.getViewType().subscribe(
				type => this.sidebarLayoutType = type
			)
		);
	}

	public moredata(event) {
		this.loadMoreData.emit(event);
	}

	protected loadEoOnScroll() {
		/* debounce scroll events but deliver first scroll event immediately */
		let debounceTime = 400;
		let scroll$ = this.sidebarScroll
			.distinctUntilChanged()
			.timeInterval();
		let firstScroll$ = scroll$.first()
			.map(ti => ti.value);

		this.subscriptions.push(
			firstScroll$
				.concat(
					scroll$
						.filter(ti => ti.interval > debounceTime)
						.map(ti => ti.value)
				).subscribe(() => {
					this.loadMoreData.emit(
						new LoadMoreResultsEvent(this.getResultService().getLoadedResultCount())
					);
				}
			)
		);
	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	public getEntityObjects() {
		return this.getResultService().getResults();
	}

	getTotalCount() {
		return this.getResultService().getTotalResultCount();
	}

	getMeta() {
		return this.getResultService().getSelectedMeta();
	}

	selectedSortedColumns(): ColumnAttribute[] {
		return this.sideviewmenuService.getSortedColumns(this.sideviewmenuPreference);
	}

	onResize(event: ResizeEvent): void {
		this.sideviewmenuPreference.content.sideviewMenuWidth = event.rectangle.width;
		$(document.body).addClass('disable-text-selection');
	}

	onResizeEnd(event: ResizeEvent): void {
		let width = event.rectangle.width;
		this.sideviewmenuPreference.content.sideviewMenuWidth = width;
		this.sidebarWidthChanged();
		setTimeout(() => {
			$(document.body).removeClass('disable-text-selection');
		});
	}

	sidebarWidthChanged(): void {
		this.sideviewmenuService.saveSideviewmenuPreference(this.sideviewmenuPreference).subscribe(
			(preferenceItem) => {
				if (preferenceItem) {
					// preference was just created
					this.sideviewmenuPreference = preferenceItem;
				}
			}
		);
	}


	/**
	 * scroll down bo list
	 */
	onScroll(): void {
		this.sidebarScroll.next(this.getResultService().getLoadedResultCount());
	}

	tdClass(eo: EntityObject) {
		let isSelected = this.getResultService().isSelected(eo);

		let classes = {
			'tdDirtyClass': eo.isDirty(),
			'tdNewClass': eo.isNew(),
			'tdDelClass': eo.isMarkedAsDeleted(),
			'tdSelectClass': isSelected,
			'tdRowColored': eo.getRowColor() !== undefined
		};

		return classes;
	}

	/**
	 * TODO: Do not build HTML fragments manually
	 *
	 * @param eo
	 * @param boAttrId
	 * @returns {any}
	 */
	displayValue(eo: EntityObject, boAttrId: string): string {
		let value = eo.getAttribute(boAttrId);
		if (value === undefined || value === null) {
			return '';
		}

		let result = value;
		if (value.name !== undefined) {
			result = value.name;
		} else if (typeof(value) === 'boolean') {
			result = '<i class="fa fa' + (value ? '-check' : '') + '-square-o"></i>';
		}
		return result;
	}

	sort(column: ColumnAttribute) {
		if (
			// this.dragInProgress ||
			this.columnResizing) {
			return;
		}

		let sortAttributes = this.sideviewmenuPreference.content.columns
			.filter(item => item.sort && item.sort.prio !== undefined);

		let highestPrio: number | undefined = 0;
		if (sortAttributes && sortAttributes.length !== 0) {
			let sideviewColumn = sortAttributes.reduce(
				(prev, current) => (
					prev && prev.sort && prev.sort.prio !== undefined
					&& current && current.sort && current.sort.prio !== undefined
					&& prev.sort.prio > current.sort.prio
				) ? prev : current
			);
			if (sideviewColumn && sideviewColumn.sort) {
				highestPrio = sideviewColumn.sort.prio;
			}
		}

		if (!column.sort) {
			column.sort = {};
		}
		if (column.sort.direction === 'asc') {
			column.sort.direction = 'desc';
			if (column.sort.prio === undefined && highestPrio !== undefined) {
				column.sort.prio = highestPrio + 1;
			}
		} else if (column.sort.direction === 'desc') {
			delete column.sort.direction;
			delete column.sort.prio;
		} else {
			column.sort.direction = 'asc';
			if (column.sort.prio === undefined && highestPrio !== undefined) {
				column.sort.prio = highestPrio + 1;
			}
		}


		// remove gaps in prio numbers
		sortAttributes
			.filter(col => col.sort && col.sort.prio !== undefined)
			.sort(
				(a, b) => (
					a && a.sort && a.sort.prio !== undefined
					&& b && b.sort && b.sort.prio !== undefined
				)
					? a.sort.prio - b.sort.prio
					: 0
			)
			.forEach((col, index) => {
				if (col && col.sort) {
					col.sort.prio = index + 1;
				}
			});


		// this.updateSideviewTable();
		this.onSort.emit(this.sideviewmenuPreference);

		this.storeColumnLayout();
	}


	private storeColumnLayout() {
		if (this.sideviewmenuPreference) {
			this.sideviewmenuService.saveSideviewmenuPreference(this.sideviewmenuPreference).subscribe(
			);
		}
	}

	/**
	 * Handles arrow key events and tries to select the corresponding next EO in the list.
	 *
	 * @param event
	 */
	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		// don't navigate if focus is on a certain component
		if (
			// subform
		$(event.target).closest('nuc-web-subform').length !== 0 ||
		// autocomplete
		$(event.target).closest('p-autocomplete').length !== 0
		) {
			return;
		}
		let selectedEo = this.getResultService().getSelectedEo();
		if (selectedEo) {
			// TODO check object identity - indexOf should work
			// let boIndex = this.bos.indexOf(this.selectedBo);
			let entityObjects = this.getResultService().getResults();
			let boIndex = entityObjects
				.map(eo => eo.getId())
				.indexOf(selectedEo.getId());
			if (boIndex !== -1) {
				let nextEO: EntityObject | undefined = undefined;
				if (event.key === 'ArrowUp') {
					if (boIndex > 0) {
						nextEO = entityObjects[boIndex - 1];
					}
				} else if (event.key === 'ArrowDown') {
					if (boIndex + 1 < entityObjects.length) {
						nextEO = entityObjects[boIndex + 1];
					}
				}

				if (nextEO) {
					this.selectEo(nextEO);
				}
			}
		}
	}

	selectEo(eo: EntityObject) {
		this.eoNavigationService.navigateToEo(eo);
	}

	columnResizeMousedown(column: ColumnAttribute, $event: MouseEvent) {
		if ($($event.target).is('th')) { // click on TH (resizer) not SPAN (column title)

			this.columnResizing = {
				startX: $event.pageX,
				startWidth: $($event.target).width(),
				htmlElement: $event.target,
				column: column
			};

			column.resizingInProgress = true;
		}
	}

	columnResizeMousemove($event) {
		if (this.columnResizing !== undefined) {
			let newWidth = this.columnResizing.startWidth + ($event.pageX - this.columnResizing.startX);
			this.columnResizing.column.width = newWidth;
		}
	}

	sideviewmenuMouseup(_event) {
		// handle column resize
		if (this.columnResizing !== undefined) {
			delete this.columnResizing;
			this.storeColumnLayout();
			this.sideviewmenuPreference.content.columns.forEach(col => delete col.resizingInProgress);
		}
	}

	isScrollbarVisible() {

		if (!this.listContainerElement || this.listContainerElement.nativeElement.clientHeight <= 0) {
			return undefined;
		}

		let listContainer = this.listContainerElement.nativeElement;
		let scrollbarVisible = listContainer.scrollHeight > listContainer.clientHeight;
		return scrollbarVisible;
	}

	public set width(width: string) {
		this.sidebar.nativeElement.style.width = width;
	}

	public get width() {
		return this.sidebar.nativeElement.style.width;
	}

	public scrollLeft() {
		$(this.listContainerElement.nativeElement).animate({
			scrollLeft: 0
		}, 500);
	}

}
