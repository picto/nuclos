import { Component, Input, OnInit } from '@angular/core';
import { Preference, SideviewmenuPreferenceContent } from '../../../../preferences/preferences.model';
import { EntityMeta } from '../../../shared/bo-view.model';
import { DataService } from '../../../shared/data.service';
import { ExportBoListTemplateState } from '../statusbar.component';

@Component({
	selector: 'nuc-export-bo-list',
	templateUrl: './export-bo-list.component.html',
	styleUrls: ['./export-bo-list.component.css']
})
export class ExportBoListComponent implements OnInit {

	format: string;
	pageOrientationLandscape = false;
	isColumnScaled = false;
	exportLink: string;

	@Input()
	meta: EntityMeta;

	@Input()
	sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	constructor(
		private state: ExportBoListTemplateState,
		private dataService: DataService
	) {
	}

	ngOnInit() {
	}

	executeListExport() {
		this.dataService.exportBoList(
			this.state.boId,
			this.state.refAttrId,
			this.meta,
			this.sideviewmenuPreference,
			this.format,
			this.pageOrientationLandscape,
			this.isColumnScaled
		).subscribe(
			exportLink => {
				this.exportLink = exportLink;
				window.location.href = exportLink;
			}
		);
	}

	close() {
		this.state.modalRef.close('confirmed');
	}

}
