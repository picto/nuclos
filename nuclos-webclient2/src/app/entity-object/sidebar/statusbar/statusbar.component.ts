import {
	Component,
	Directive,
	EventEmitter,
	Injectable,
	Input,
	OnChanges,
	OnInit,
	Output,
	TemplateRef
} from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Action } from '../../../authentication/action.enum';
import { AuthenticationService } from '../../../authentication/authentication.service';
import { Preference, SideviewmenuPreferenceContent } from '../../../preferences/preferences.model';
import { EntityMetaData } from '../../shared/bo-view.model';
import { SidebarComponent } from '../sidebar.component';


/**
 * contains reference to the modal template
 */
@Injectable()
export class ExportBoListTemplateState {
	modalRef: NgbModalRef;
	templateRef: TemplateRef<any>;
	boId: number | undefined;
	refAttrId: string | undefined;
}

@Directive({
	selector: '[nucExportBoListTemplate]'
})
export class ExportBoListTemplateDirective {
	constructor(template: TemplateRef<any>, state: ExportBoListTemplateState) {
		state.templateRef = template;
	}
}


@Component({
	selector: 'nuc-statusbar',
	templateUrl: './statusbar.component.html',
	styleUrls: ['./statusbar.component.scss']
})
export class StatusbarComponent implements OnInit, OnChanges {

	showCollapseButton = false;
	showExpandButton = false;
	showExpandFullButton = false;

	private showPrintSearchResultListButton = false;

	@Input('sidebar') private sidebar: SidebarComponent;

	@Input()
	meta: EntityMetaData;

	@Input()
	sideviewMenuWidth: number;

	@Input()
	totalEOCount: number;

	@Input()
	sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>;

	@Output()
	sideviewMenuWidthChange = new EventEmitter<number>();

	constructor(
		authenticationService: AuthenticationService,
		private modalService: NgbModal,
		private exportBoListTemplateState: ExportBoListTemplateState
	) {
		this.showPrintSearchResultListButton = authenticationService.isActionAllowed(Action.PrintSearchResultList);
	}

	ngOnInit() {
	}

	ngOnChanges(): void {
		this.updateSideviewmenuStatusbar();
	}

	private getNrOfColumns(): number {
		return $('nuc-sidebar th').length;
	}

	/**
	 * number of visible columns starting from status column
	 */
	private getNrOfVisibleColumns(): number {
		let navWidth = this.sideviewMenuWidth;
		let itemIndex = 0;
		let widthSum = 0;
		let headerElements = $('nuc-sidebar th').toArray();
		for (let headerElement of headerElements) {
			widthSum += $(headerElement).width();
			if (widthSum > navWidth - (itemIndex > 1 ? 10 : 0)) {
				return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
			}
			itemIndex++;
		}
		return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
	}

	private updateSideviewmenuStatusbar() {
		this.showExpandButton = this.getNrOfVisibleColumns() < this.getNrOfColumns();
		this.showExpandFullButton = this.showExpandButton;
		this.showCollapseButton = this.getNrOfVisibleColumns() > 1;
	}


	private showNumberOfColumns(numberOfColumns: number) {
		if (numberOfColumns > this.getNrOfColumns()) {
			numberOfColumns = this.getNrOfColumns();
		}
		let th = $($('nuc-sidebar th')[numberOfColumns - 1]);
		let newWidth = th.offset().left + th.width() - $($('nuc-sidebar tbody')).offset().left + 6;

		// Don't make the sideview wider than the document
		let documentWidth = $(document).width();
		newWidth = Math.min(documentWidth, newWidth);

		this.sidebar.width = newWidth + 'px';

			this.sideviewMenuWidth = newWidth;
		this.sideviewMenuWidthChange.emit(newWidth);

		this.updateSideviewmenuStatusbar();
	}

	private scrollLeft() {
		this.sidebar.scrollLeft();
	}

	expandSideview() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfVisibleColumns() + 1);
	}

	collapseSideview() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfVisibleColumns() - 1);
	}

	expandSideviewFull() {
		this.scrollLeft();
		this.showNumberOfColumns(this.getNrOfColumns());
	}

	collapseSideviewFull() {
		this.scrollLeft();
		this.sidebar.width = '20px';
	}

	openExportBoListModal() {
		this.exportBoListTemplateState.boId = undefined;
		this.exportBoListTemplateState.refAttrId = undefined;
		this.exportBoListTemplateState.modalRef = this.modalService.open(this.exportBoListTemplateState.templateRef);
	}

}
