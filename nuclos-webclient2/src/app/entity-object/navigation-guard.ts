import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EntityObjectComponent } from '../entity-object/entity-object.component';
import { EntityObjectResultService } from './shared/entity-object-result.service';

export class NavigationGuard implements CanDeactivate<EntityObjectComponent> {
	constructor(
		private eoResultService: EntityObjectResultService
	) {
	}

	canDeactivate(
		component: EntityObjectComponent,
		route: ActivatedRouteSnapshot,
		_state: RouterStateSnapshot
	): Observable<boolean>
		| Promise<boolean>
		| boolean {
		let result = true;
		let selectedEO = this.eoResultService.getSelectedEo();

		if (!selectedEO) {
			result = true;
		} else if (selectedEO.isNew() && route.params['entityObjectId'] !== 'new') {
			result = true;
		} else if (selectedEO.isDirty()) {
			component.warnAboutUnsavedChanges();
			result = false;
		}

		return result;
	}
}
