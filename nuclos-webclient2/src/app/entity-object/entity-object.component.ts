import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Logger } from '../log/shared/logger';
import { PerspectiveService } from '../perspective/shared/perspective.service';
import { BrowserRefreshService } from '../shared/browser-refresh.service';
import { EntityObjectRouteService } from './entity-object-route.service';
import { EntityMeta } from './shared/bo-view.model';
import { EntityObjectPreferenceService } from './shared/entity-object-preference.service';
import { EntityObjectService } from './shared/entity-object.service';
import { MetaService } from './shared/meta.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { EntityObjectEventService } from './shared/entity-object-event.service';
import { EntityObjectResultService } from './shared/entity-object-result.service';
import { WsTab } from '../explorertrees/explorertrees.model';
import { EntityObjectErrorService } from './shared/entity-object-error.service';
import { EntityObjectResultUpdateService } from './shared/entity-object-result-update.service';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../authentication/authentication.service';
import { Action } from '../authentication/action.enum';

export class LoadMoreResultsEvent {
	loadedItems: number;

	constructor(loadedItems: number) {
		this.loadedItems = loadedItems;
	}

}

@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectComponent implements OnInit, OnDestroy {

	@ViewChild('sidebar') private sidebar: SidebarComponent;

	private urlQuery: string | undefined;

	triggerUnsavedChangesPopover: Date;
	private compId = Math.random();

	private explorerTree: WsTab | undefined;

	private subscriptions: Subscription[] = [];

	constructor(
		private authenticationService: AuthenticationService,
		private route: ActivatedRoute,
		private metaService: MetaService,
		private eoEventService: EntityObjectEventService,
		private eoService: EntityObjectService,
		private eoRouteService: EntityObjectRouteService,
		private entityObjectPreferenceService: EntityObjectPreferenceService,
		private $log: Logger,
		private browserRefreshService: BrowserRefreshService,
		private perspectiveService: PerspectiveService,
		private eoResultService: EntityObjectResultService,
		private eoResultUpdateService: EntityObjectResultUpdateService,
		// TODO: Only injected here in order to be instantiated by angular
		private eoErrorService: EntityObjectErrorService,
	) {
	}

	ngOnInit() {
		this.subscriptions.push(
			this.eoEventService.observeSelectedEo().subscribe(
				eo => {
					// TODO: Find a better place for this
					this.perspectiveService.selectEo(eo);
				}
			)
		);

		this.subscriptions.push(
			this.route.params.subscribe(
				(params: Params) => {
					this.urlQuery = params['query'];
					this.$log.warn('Params from %o: %o', this.compId, params);
					this.eoRouteService.handleParams(params);
				}
			)
		);

		this.subscriptions.push(
			this.browserRefreshService.onEoChanges().subscribe(
				() => {
					// reload eo data
					let selectedEo = this.eoResultService.getSelectedEo();
					if (selectedEo && !selectedEo.isDirty()) {
						selectedEo.reload();
					}
				}
			)
		);

		this.subscriptions.push(
			this.eoResultService.observeSelectedEntityClassId().subscribe(
				(entityClassId: string) => {
					this.metaService.getEntityMeta(entityClassId).subscribe(
						meta => {
							this.perspectiveService.selectEntityClass(meta);
							this.loadPrefsAndEoListAfterPerspective(entityClassId);
						}
					);
				}
			)
		);

		this.subscriptions.push(
			this.eoResultService.observeResultListUpdate().subscribe(
				pristine => {
					this.eoResultService.waitForEoSelection().subscribe(() => {
						let selectedEo = this.eoResultService.getSelectedEo();
						if (pristine && selectedEo) {
							this.eoResultService.addSelectedEoToList();
						} else {
							this.eoResultService.selectFirstResult();
						}
					});
				}
			)
		);

	}

	ngOnDestroy(): void {
		for (let subscription of this.subscriptions) {
			subscription.unsubscribe();
		}
	}

	getSideviewmenuPref() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$.getValue();
	}

	getSearchtemplatePref() {
		return this.entityObjectPreferenceService.selectedSearchtemplatePref$.getValue();
	}

	getSideviewmenuPrefSubject() {
		return this.entityObjectPreferenceService.selectedSideviewmenuPref$;
	}

	getSearchtemplatePrefSubject() {
		return this.entityObjectPreferenceService.selectedSearchtemplatePref$;
	}

	/**
	 * TODO: Prefs and EO list are now loaded twice in some cases, because this component is
	 * not a singleton, but instantiated at least twice by angular (because of different routes).
	 *
	 * @param entityClassId
	 */
	private loadPrefsAndEoListAfterPerspective(entityClassId: string) {
		this.subscriptions.push(
			this.perspectiveService.waitForLoadingFinished().subscribe(
				() => this.loadPrefsAndEoList(entityClassId)
			)
		);
	}


	private loadPrefsAndEoList(entityClassId: string) {
		console.warn('Load searchtemplate Prefs and EO list for entity class %o', entityClassId);

		let entityMeta = this.metaService.getBoMeta(entityClassId);
		if (!entityMeta) {
			return;
		}

		this.subscriptions.push(
			entityMeta.subscribe(
				meta => {
					this.eoResultService.setSelectedMeta(meta);

					this.subscriptions.push(
						this.entityObjectPreferenceService.loadPreferences(meta, this.urlQuery).subscribe(() => {
							this.$log.warn('prefs loaded');

							// TODO: Refactoring - do not make loading of the results dependent on the child component here
							if (!this.getExplorerTree()) {
								if (this.hasResults()) {
									this.loadEoDataToFillSidebar();
								} else {
									this.reloadResults();
								}
							}
						})
					);
				}
			)
		);
	}

	private hasResults() {
		return this.eoResultService.getLoadedResultCount() > 0;
	}

	/**
	 * depending on screen resolution and browser zoom factor
	 * it is necessary to load more data to fill the complete sidebar height
	 *
	 * TODO: Refactor eo data loading into separate service and make it independent of concrete components.
	 */
	private loadEoDataToFillSidebar() {
		if (!this.sidebar) {
			return;
		}

		if (this.sidebar.isScrollbarVisible() === false) {
			this.eoResultUpdateService.loadMoreResults(
				new LoadMoreResultsEvent(this.eoResultService.getLoadedResultCount())
			).subscribe(() => {
				this.loadEoDataToFillSidebar();
			});
		}
	}

	warnAboutUnsavedChanges() {
		this.triggerUnsavedChangesPopover = new Date();
	}

	getMeta(): EntityMeta | undefined {
		return this.eoResultService.getSelectedMeta();
	}

	getSelectedEo() {
		return this.eoResultService.getSelectedEo();
	}

	canCreateBo() {
		return this.eoResultService.canCreateBo;
	}

	selectExplorerTree(tree: WsTab) {
		if (this.explorerTree === tree) {
			this.explorerTree = undefined;
			return;
		}

		this.explorerTree = tree;
	}

	getExplorerTree() {
		return this.explorerTree;
	}

	selectedTreeNode(tnode: any) {
		// NUCLOS-3783 Display of the following eo:
		this.$log.info('Selected EO, Entity:' + tnode.boMetaId + ' PK:' + tnode.boId);

		this.eoService.loadEO(tnode.boMetaId, tnode.boId).subscribe(eo => {
			this.eoResultService.selectEo(eo);
			// NUCLOS-6039
			this.eoResultService.reloadCanCreateBoIfNotSet(tnode.boMetaId);
		});
	}

	reloadResults() {
		this.subscriptions.push(
			this.eoResultUpdateService.loadInitialResults().subscribe(
				() => this.loadEoDataToFillSidebar()
			)
		);
	}

	loadMoreResults(event: LoadMoreResultsEvent) {
		this.subscriptions.push(
			this.eoResultUpdateService.loadMoreResults(event).subscribe()
		);
	}
}


/**
 * view eo in popup
 * no sidebar/toolbar/header
 */
@Component({
	selector: 'nuc-entity-object',
	templateUrl: './entity-object-popup.component.html',
	styleUrls: ['./entity-object.component.css']
})
export class EntityObjectPopupComponent extends EntityObjectComponent {
	ngOnInit() {
		super.ngOnInit();
		$('header').hide();
	}
}
