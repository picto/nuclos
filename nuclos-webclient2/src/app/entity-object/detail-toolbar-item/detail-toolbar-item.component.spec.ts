import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailToolbarItemComponent } from './detail-toolbar-item.component';

describe('DetailToolbarItemComponent', () => {
	let component: DetailToolbarItemComponent;
	let fixture: ComponentFixture<DetailToolbarItemComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DetailToolbarItemComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DetailToolbarItemComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
