import { Component, Input, OnInit } from '@angular/core';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { EntityObject } from '../shared/entity-object.class';
import { EntityObjectService } from '../shared/entity-object.service';
import { EntityObjectResultService } from '../shared/entity-object-result.service';

@Component({
	selector: 'nuc-locking',
	templateUrl: 'locking.component.html',
	styleUrls: ['locking.component.css']
})
export class LockingComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor(
		protected i18n: NuclosI18nService,
		private eoService: EntityObjectService,
		private eoResultService: EntityObjectResultService,
	) {
	}

	ngOnInit() {
	}

	/**
	 * Unlocks a locked eo
	 */
	unlock() {
		this.eoService.unlock(this.eo).subscribe(
			(eo) => eo.reload().subscribe(
				eo2 => this.eoResultService.selectEo(eo2)
			)
		);
	}
}
