import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DetailModalComponent } from '../detail-modal/detail-modal.component';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { Observable } from 'rxjs/Observable';
import { RuleService } from '../../rule/shared/rule.service';

@Injectable()
export class DetailModalService {

	constructor(
		private modalService: NgbModal,
		private ruleService: RuleService
	) {
	}

	openEoInModal(eo: EntityObject): Observable<any> {
		let ngbModalRef = this.modalService.open(
			DetailModalComponent,
			{size: 'lg', windowClass: 'fullsize-modal-window'}
		);

		let result = Observable.of(ngbModalRef.result);

		/**
		 * A SubEntityObject must be modified to behave like a main EO if it is opened via modal.
		 */
		if (eo instanceof SubEntityObject) {
			const subEo = eo;

			// After the modal is closed, layout rules may change again
			result.finally(() => this.ruleService.updateRuleExecutor(subEo)).subscribe();

			eo = subEo.toEntityObject();
		}

		ngbModalRef.componentInstance.eo = eo;

		return result;
	}
}
