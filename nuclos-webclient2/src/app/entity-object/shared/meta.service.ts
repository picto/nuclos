import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpMethod } from '../../rest-explorer/shared/rest-service-info';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { EntityMeta } from './bo-view.model';

@Injectable()
export class MetaService {
	static instance: MetaService;

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService
	) {
		MetaService.instance = this;
	}

	getBoMeta(boMetaId: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			this.nuclosConfig.getRestHost() + '/boMetas/' + boMetaId,
			json => new EntityMeta(json)
		);
	}

	getEntityMeta(entityClassId: string): Observable<EntityMeta> {
		return new Observable<EntityMeta>(observer => {
			this.getBoMeta(entityClassId).subscribe(
				emData => {
					let entityMeta = emData;
					observer.next(entityMeta);
					observer.complete();
				},
				error => observer.error(error)
			);
		});
	}

	getSubBoMeta(href: string): Observable<EntityMeta> {
		return this.http.getCachedJSON(
			href,
			json => new EntityMeta(json)
		);
	}

	canCreateEo(entityClassId: string): Observable<boolean> {
		return new Observable<boolean>(observer => {
			this.http.getCachedJSON(
				this.nuclosConfig.getRestHost() + '/bos',
				json => json.map(m => new EntityMeta(m))
			)
				.take(1)
				.do(entityMetaArray => {
					let meta: EntityMeta = entityMetaArray.filter(
						entityMeta => entityMeta.getBoMetaId() === entityClassId).shift();
					if (meta && meta.getLinks().bos) {
						let links = meta.getLinks();
						if (links && links.bos && links.bos.methods) {
							observer.next(links.bos.methods.indexOf(HttpMethod.POST) !== -1);
						}
					}
				})
				.subscribe();
		});
	}
}
