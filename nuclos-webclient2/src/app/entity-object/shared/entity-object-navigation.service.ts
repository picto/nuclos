import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '../../log/shared/logger';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectNavigationService {

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private $log: Logger
	) {
	}

	navigateToEo(eo: EntityObject) {
		this.$log.warn('(selecting) Navigating to EO: %o', eo && eo.getTitle());

		let entityClassId = eo.getEntityClassId();
		let id = eo.getId() || eo.getTemporaryId() || 'new';

		let queryParams = this.route.snapshot.queryParams;

		this.router.navigate(
			[
				this.router.url.indexOf('/popup/') !== -1 ? '/popup' : '/view'
				, entityClassId, id
			],
			{queryParams: queryParams}
		);

	}

	navigateToNew(entityClassId: string) {
		this.router.navigate(['/view', entityClassId, 'new']);
	}
}
