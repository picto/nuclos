import { TestBed, inject } from '@angular/core/testing';

import { DetailModalService } from './detail-modal.service';

describe('DetailModalServiceService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [DetailModalService]
		});
	});

	it('should ...', inject([DetailModalService], (service: DetailModalService) => {
		expect(service).toBeTruthy();
	}));
});
