import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {
	ColumnAttribute,
	Preference,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { FqnService } from '../../shared/fqn.service';
import { EntityAttrMeta, EntityMeta } from './bo-view.model';
import { SelectableService } from './selectable.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Action } from '../../authentication/action.enum';
import { AuthenticationService } from '../../authentication/authentication.service';
import { ArrayUtils } from '../../shared/array-utils';

@Injectable()
export class SideviewmenuService {

	private sideviewmenuPrefChanges: Subject<any>;
	private sidebarLayoutType: BehaviorSubject<SidebarLayoutType>;

	constructor(
		private selectableService: SelectableService,
		private authenticationService: AuthenticationService,
		private fqnService: FqnService,
	) {
		this.sideviewmenuPrefChanges = new Subject();
		this.sidebarLayoutType = new BehaviorSubject<SidebarLayoutType>(this.getViewTypeFromLocalStorage());
	}

	onSideviewmenuPrefChange(): Subject<any> {
		return this.sideviewmenuPrefChanges;
	}

	sideviewmenuPrefChanged(): void {
		this.sideviewmenuPrefChanges.next(new Date().getTime());
	}


	emptySideviewmenuPreference(
		metaData: EntityMeta,
		selectDefaultColumns: boolean,
		addNotSelectedColumns = true
	): Preference<SideviewmenuPreferenceContent> {

		let sideviewmenuPreference: Preference<SideviewmenuPreferenceContent> = {
			type: 'table',
			boMetaId: metaData.getBoMetaId(),
			selected: false,
			content: {
				columns: [],
				sideviewMenuWidth: 250
			}
		};
		if (selectDefaultColumns) {

			if (sideviewmenuPreference.type === 'table') {

				let titleAttributes: string[] = selectDefaultColumns ? this.fqnService.parseFqns(metaData.getMetaData().titlePattern) : [];

				sideviewmenuPreference.content.columns = [];
				// load all sideviewmenuColumns from meta data
				metaData.getAttributes().forEach(attributeMeta => {
					let visible = this.isVisibleAttribute(attributeMeta);
					if (visible) {
						let selected = selectDefaultColumns && titleAttributes.indexOf(attributeMeta.getAttributeID()) !== -1;
						if (selected || addNotSelectedColumns) {
							sideviewmenuPreference.content.columns.push(
								{
									name: attributeMeta.getName(),
									boAttrId: attributeMeta.getAttributeID(),
									system: attributeMeta.isSystemAttribute(),
									selected: selected,
									sort: {
										enabled: attributeMeta && !attributeMeta.isCalculated()
									}
								}
							);
						}
					}
				});

			}
		}

		return sideviewmenuPreference;
	}


	emptySubformPreference(
		metaData: EntityMeta,
		parentEntityUid: string,
		selectDefaultColumns: boolean
	): Preference<SideviewmenuPreferenceContent> {

		let sideviewmenuPreference: Preference<SideviewmenuPreferenceContent> = {
			type: 'subform-table',
			boMetaId: metaData.getBoMetaId(),
			selected: false,
			content: {
				columns: []
			}
		};

		if (selectDefaultColumns) {

			// default column layout (see ProfileUtils.java for Java-Client)

			let defaultColumnWidth = (attributeMeta: EntityAttrMeta): number => {
				switch (attributeMeta.getType()) {
					case ('String'):
						return 120;
					case ('Integer'):
					case ('Decimal'):
					case ('Date'):
						return 80;
					case ('Boolean'):
					case ('Image'):
						return 40;
				}
				return 120;
			};

			sideviewmenuPreference.content.columns = Array.from(metaData.getAttributes().values())

				// no hidden attributes
				.filter(attributeMeta => !attributeMeta.isHidden())

				// no calculated attributes
				.filter(attributeMeta => !attributeMeta.isCalculated())

				// no system attributes except state/stateIcon
				.filter(attributeMeta => !attributeMeta.isSystemAttribute() || attributeMeta.isState() || attributeMeta.isStateIcon())

				// no blobs
				.filter(attributeMeta => {
					let scale = attributeMeta.getScale();
					return !(attributeMeta.getType() === 'String' && scale && scale > 255);
				})

				// max 30 columns
				.slice(0, 30)

				.map(attributeMeta => {
					return {
						name: attributeMeta.getName(),
						boAttrId: attributeMeta.getAttributeID(),
						system: attributeMeta.isSystemAttribute(),
						selected: true,
						width: defaultColumnWidth(attributeMeta),
						sort: {
							enabled: attributeMeta && !attributeMeta.isCalculated()
						}
					}
				});

			// show state icon at first column
			let stateIconIndex = sideviewmenuPreference.content.columns.findIndex(c => c.boAttrId.endsWith('nuclosStateIcon'));
			if (stateIconIndex !== -1) {
				ArrayUtils.move(sideviewmenuPreference.content.columns, stateIconIndex, 0);
			}
		}

		return sideviewmenuPreference;
	}

	private isVisibleAttribute(attributeMeta: EntityAttrMeta) {
		let visible = !attributeMeta.isStateIcon()
			&& !attributeMeta.isDeletedFlag()
			&& !attributeMeta.isHidden();

		return visible;
	}


	private setSideviewmenuWidth(boMetaId: string, width: number | undefined) {
		if (boMetaId !== undefined && width !== undefined) {
			let key = 'table-width-' + boMetaId;
			localStorage.setItem(key, '' + width);
		}
	}

	getSideviewmenuWidthLocal(boMetaId: string) {
		let key = 'table-width-' + boMetaId;
		return localStorage.getItem(key);
	}

	saveSideviewmenuPreference(
		preferenceItem: Preference<SideviewmenuPreferenceContent>
	): Observable<Preference<SideviewmenuPreferenceContent>> {
		if (!this.authenticationService.isActionAllowed(Action.WorkspaceCustomizeEntityAndSubFormColumn)) {
			return Observable.of(preferenceItem);
		}
		this.setSideviewmenuWidth(preferenceItem.boMetaId, preferenceItem.content.sideviewMenuWidth);
		return this.selectableService.savePreference(preferenceItem);
	}


	getSortedColumns(sideviewmenuPreference: Preference<SideviewmenuPreferenceContent>): ColumnAttribute[] {
		if (!sideviewmenuPreference.content.columns) {
			return [];
		}
		return sideviewmenuPreference.content.columns
			.filter(c => c.selected)
			.sort(
				(a, b) => {
					if (a.position === undefined || b.position === undefined) {
						return 0;
					}
					if (a.position < b.position) {
						return -1;
					}
					if (a.position > b.position) {
						return 1;
					}
					return 0;
				}
			);
	}

	getViewType(): BehaviorSubject<SidebarLayoutType> {
		return this.sidebarLayoutType;
	}

	private getViewTypeFromLocalStorage(): SidebarLayoutType {
		let type = localStorage.getItem('SidebarLayoutType');
		if (type !== undefined) {
			return type as SidebarLayoutType;
		}
		return 'table';
	}

	setViewType(type: SidebarLayoutType ): void {
		localStorage.setItem('SidebarLayoutType', type);
		this.sidebarLayoutType.next(type);
	}

}
