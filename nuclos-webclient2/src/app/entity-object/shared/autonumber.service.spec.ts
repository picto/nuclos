import { TestBed, inject } from '@angular/core/testing';

import { AutonumberService } from './autonumber.service';

describe('AutonumberService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [AutonumberService]
		});
	});

	it('should be created', inject([AutonumberService], (service: AutonumberService) => {
		expect(service).toBeTruthy();
	}));
});
