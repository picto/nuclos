import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Logger } from '../../log/shared/logger';
import { BoViewModel, EntityMeta } from './bo-view.model';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObjectNavigationService } from './entity-object-navigation.service';
import { EntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';
import { MetaService } from './meta.service';

/**
 * Holds currently selected entity object(s).
 */
@Injectable()
export class EntityObjectResultService {
	static instance: EntityObjectResultService;

	/**
	 * Currently selected entity class ID (aka "boMetaId").
	 */
	private selectedEntityClassId = new BehaviorSubject<string | undefined>(undefined);

	private selectedMeta: EntityMeta | undefined;

	/**
	 * Currently selected entity object ID (aka "boId").
	 */
	private selectedEoId: number | string | undefined;

	/**
	 * Currenty selected entity object, should be always consistent with selected EO class and ID.
	 */
	private selectedEo: EntityObject | undefined;

	private entityObjects: EntityObject[] = [];

	/**
	 * Determines if there already was result data for the currently selected entity class.
	 * This is important to decide about auto-selection of the first result, when new results are loaded.
	 * Auto-selection should not occur initially if an EO was directly selected (via deep link).
	 * But it should occur if new results are loaded after search filter changes etc.
	 */
	private pristine = true;

	private totalEoCount: number | undefined;

	private eoSelectionInProgress = false;

	canCreateBo: boolean | undefined;

	private resultListUpdateSubject = new Subject<boolean>();

	private eoSelectionSubject = new Subject<void>();

	constructor(
		private eoService: EntityObjectService,
		private eoEventService: EntityObjectEventService,
		private eoNavigationService: EntityObjectNavigationService,
		private authenticationService: AuthenticationService,
		private metaService: MetaService,
		private $log: Logger,
	) {
		EntityObjectResultService.instance = this;

		this.eoEventService.observeDeletedEo().subscribe(
			(eo) => {
				this.removeEo(eo);

				// TODO: Check if the deleted EO really belonged to the currently shown EOs
				if (this.totalEoCount) {
					this.totalEoCount--;
				}
			}
		);

		this.eoEventService.observeNewEo().subscribe(
			() => {
				// TODO: Check if the created EO really belongs to the currently shown EOs
				if (this.totalEoCount !== undefined) {
					this.totalEoCount++;
				}
			}
		);

		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.selectEo(undefined);
			}
		});

		this.observeSelectedEntityClassId().subscribe(
			() => {
				this.clear();
				this.updateSelectedMeta().subscribe();
			}
		);
	}

	private getEoService() {
		return this.eoService;
	}


	getSelectedEntityClassId() {
		return this.selectedEntityClassId.getValue();
	}

	getSelectedMeta() {
		return this.selectedMeta;
	}

	updateSelectedMeta(): Observable<EntityMeta> {
		let entityClassId = this.getSelectedEntityClassId();

		if (!entityClassId) {
			this.selectedMeta = undefined;
			return Observable.empty();
		}

		return this.metaService.getEntityMeta(entityClassId).do(
			meta => this.selectedMeta = meta
		);
	}

	getSelectedEoId(): number | string | undefined {
		return this.selectedEoId;
	}

	getSelectedEo(): EntityObject | undefined {
		return this.selectedEo;
	}

	isSelected(eo: EntityObject) {
		if (eo.getEntityClassId() !== this.getSelectedEntityClassId()) {
			return false;
		}

		if (eo.isNew()) {
			if (eo.getTemporaryId()) {
				return this.getSelectedEoId() === eo.getTemporaryId();
			}
			return this.getSelectedEoId() === 'new';
		}

		return eo.getId() === this.getSelectedEoId();
	}

	selectEo(eo: EntityObject | undefined) {
		this.$log.warn('Selecting EO: %o (%o)', eo, eo && eo.getTitle());
		this.selectedEo = eo;

		this.selectedEoId = undefined;
		if (eo) {
			this.selectEntityClassId(eo.getEntityClassId());
			this.selectedEoId = eo.getId() || eo.getTemporaryId() || 'new';
			this.updateEntityObjectsWithSelectedEoData();
		}

		this.eoSelectionInProgress = false;

		this.eoEventService.emitSelectedEo(eo);
		if (eo) {
			this.eoNavigationService.navigateToEo(eo);
		}
	}

	waitForEoSelection(): Observable<void> {
		if (!this.eoSelectionInProgress) {
			return Observable.of(undefined);
		}

		return this.eoSelectionSubject.take(1);
	}

	/**
	 * Loads the EO for the given entity and ID
	 * and sets it as the selected EO.
	 * The ID could also be a temporary ID, starting with 'temp_'.
	 *
	 * @param entityClassId
	 * @param entityObjectId
	 */
	selectEoByClassAndId(entityClassId: string, entityObjectId: number | string): Observable<EntityObject> {
		let selectedEO = this.getSelectedEo();

		if (this.eoSelectionInProgress) {
			this.$log.warn('EO selection is already in progress, ignoring');
			return Observable.of(selectedEO);
		}

		if (selectedEO && selectedEO.getEntityClassId() === entityClassId && '' + selectedEO.getId() === entityObjectId) {
			this.selectedEoId = selectedEO.getId();
			return Observable.of(selectedEO);
		}

		this.eoSelectionInProgress = true;
		return new Observable<EntityObject>(observer => {
			let isTemp = ('' + entityObjectId).startsWith('temp_');

			if (isTemp) {
				let eoString = localStorage.getItem('' + entityObjectId);
				if (eoString) {
					let eo = new EntityObject(JSON.parse(eoString));
					eo.setDirty(true);
					eo.select();
					observer.next(eo);
					observer.complete();
				} else {
					observer.error('No entity object found for ID: ' + entityObjectId);
				}
			} else {
				this.getEoService().loadEO(entityClassId, entityObjectId).subscribe(
					eo => {
						eo.select();
						observer.next(eo);
						observer.complete();
					},
					error => observer.error(error)
				);
			}
		}).finally(() => {
			this.eoSelectionInProgress = false;
			this.eoSelectionSubject.next();
		});
	}

	selectEntityClassId(entityClassId: string | undefined) {
		this.$log.warn('Selecting entity class: %o', entityClassId);
		this.selectedEntityClassId.next(entityClassId);
	}

	/**
	 * replace selected eo with current changes so that sidebar shows the changes
	 */
	updateEntityObjectsWithSelectedEoData() {
		this.checkSelectedEo();

		let selectedEo = this.getSelectedEo();
		if (!selectedEo) {
			return;
		}

		let index = this.findEoIndexInResults(selectedEo);
		if (index >= 0) {
			this.entityObjects[index] = selectedEo;
		}
	}

	observeSelectedEntityClassId(): Observable<string | undefined> {
		return this.selectedEntityClassId.distinctUntilChanged();
	}

	observeResultListUpdate(): Observable<boolean> {
		return this.resultListUpdateSubject;
	}

	refreshSelectedEo() {
		if (this.selectedEo && !this.selectedEo.isDirty()) {
			let entityClassId = this.getSelectedEntityClassId();
			if (entityClassId && this.selectedEoId) {
				this.getEoService().loadEO(
					entityClassId,
					this.selectedEoId
				).subscribe(eo => {
					this.selectEo(eo);
				});
			}
		}
	}

	resetEo(eo: EntityObject) {
		eo.reset();
		this.eoEventService.emitResetEo(eo);
	}

	navigateToNew() {
		let entityClassId = this.getSelectedEntityClassId();
		if (entityClassId) {
			this.eoNavigationService.navigateToNew(entityClassId);
		}
	}

	createNew(): Observable<EntityObject> {
		let entityClassId = this.getSelectedEntityClassId();
		if (!entityClassId) {
			return Observable.empty();
		}

		return this.getEoService().createNew(
			entityClassId
		).do(
			eo => this.entityObjects.unshift(eo)
		);
	}

	/**
	 * TODO: Make private.
	 */
	setSelectedMeta(meta: EntityMeta) {
		this.selectedMeta = meta;
	}

	getResults(): EntityObject[] {
		return this.entityObjects;
	}

	getCount() {
		return this.getResults().length;
	}

	/**
	 * Removes the EO with the given ID from the list.
	 *
	 * @param eoId
	 */
	private removeEo(eo: EntityObject): void {
		let eoId = eo.getId();

		let entityObjects = this.getResults();

		let wasSelected = eo === this.selectedEo;
		if (wasSelected) {
			this.selectEo(undefined);
		}

		// May not have been loaded yet
		if (!this.entityObjects) {
			return;
		}

		let index = entityObjects.indexOf(eo);
		this.$log.debug('removeEO(%o): index = %o', eoId, index);
		if (index > -1) {
			entityObjects.splice(index, 1);
			// If the removed EO was selected, select the previous EO in the list
			if (wasSelected) {
				if (index > 0) {
					index--;
				}
				let nextEO = entityObjects[index];
				if (nextEO) {
					this.eoNavigationService.navigateToEo(nextEO);
				}
			}
		} else {
			this.$log.warn('Not found: %o', eo);
		}
	}

	setNewData(boViewModel: BoViewModel) {
		if (boViewModel.boMetaId !== this.getSelectedEntityClassId()) {
			this.$log.warn(
				'%o: Entity class %o of new results does not match selected entity class %o, ignoring new results',
				boViewModel,
				boViewModel.boMetaId,
				this.getSelectedEntityClassId()
			);
			return;
		}

		this.entityObjects = boViewModel.bos;
		this.totalEoCount = boViewModel.total;
		this.canCreateBo = boViewModel.canCreateBo;

		this.checkSelectedEo();

		this.updateEntityObjectsWithSelectedEoData();

		this.resultsUpdated();
	}

	/**
	 * Checks if the current EO selection is still valid.
	 */
	private checkSelectedEo() {
		if (!this.selectedEo) {
			return;
		}

		if (this.selectedEo.getEntityClassId() !== this.getSelectedEntityClassId()) {
			this.selectEo(undefined);
			return;
		}

		// A selected new or temporary EO must be added to the list again.
		// Persistent EOs might disappear from the list (e.g. after search filter changes).
		// In this case, a new EO will eventually be selected.
		if (!this.findEoInResults(this.selectedEo)) {
			if (this.selectedEo.isNew()) {
				this.entityObjects.unshift(this.selectedEo);
			}
		}
	}

	addNewData(boViewModel: any) {
		this.entityObjects.push(...boViewModel.bos);
		this.totalEoCount = boViewModel.total;
		this.canCreateBo = boViewModel.canCreateBo;
		this.resultsUpdated();
	}

	private resultsUpdated() {
		this.resultListUpdateSubject.next(this.pristine);
		this.pristine = false;
	}

	private findEoInResults(eo: EntityObject) {
		let index = this.findEoIndexInResults(eo);

		if (index < 0) {
			return undefined;
		}

		return this.entityObjects[index];
	}

	private findEoIndexInResults(eo: EntityObject): number {
		if (eo.isNew()) {
			return this.entityObjects.findIndex(
				resultEo => resultEo === eo || resultEo.getTemporaryId() === eo.getTemporaryId()
			);
		} else {
			return this.entityObjects.findIndex(
				resultEo => resultEo.getId() === eo.getId()
			);
		}
	}

	selectFirstResult() {
		this.$log.warn('Selecting first result...');
		if (this.entityObjects && this.entityObjects.length > 0) {
			if (this.selectedEo && this.findEoInResults(this.selectedEo)) {
				this.$log.warn('Already selected a result from the result list');
				return;
			}
			let firstResult = this.entityObjects[0];
			firstResult.select();
			firstResult.reload().subscribe();
		}
	}

	getTotalResultCount() {
		return this.totalEoCount;
	}

	/**
	 * The number of results loaded from the server.
	 * Might be smaller than the current number of EOs, as the user can add new EOs.
	 */
	getLoadedResultCount() {
		return this.entityObjects.length;
	}


	reloadCanCreateBoIfNotSet(boMetaId: string) {
		if (this.canCreateBo === undefined) {
			this.eoService.loadEmptyList(boMetaId).subscribe(model => {
				this.canCreateBo = model.canCreateBo;
			});
		}
	}

	clear() {
		this.selectedMeta = undefined;
		this.selectEo(undefined);
		this.entityObjects = [];
		this.totalEoCount = undefined;
		this.canCreateBo = undefined;
		this.pristine = true;
	}

	addSelectedEoToList() {
		if (this.selectedEo && !this.findEoInResults(this.selectedEo)) {
			this.entityObjects.unshift(this.selectedEo);
			this.resultsUpdated();
		} else {
			this.$log.warn('No EO selected - can not add to result list');
		}
	}
}
