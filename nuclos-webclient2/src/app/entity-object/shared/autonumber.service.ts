import { Injectable } from '@angular/core';
import { EntityObject, SubEntityObject } from './entity-object.class';
import * as _ from 'lodash';
import { EntityAttrMeta } from './bo-view.model';
import { EntityObjectDependents } from './entity-object-dependents';
import { Logger } from '../../log/shared/logger';

@Injectable()
export class AutonumberService {

	static instance: AutonumberService;

	private updateInProgress: EntityObjectDependents[] = [];

	constructor(
		private $log: Logger
	) {
		AutonumberService.instance = this;
	}

	updateAutonumbers(dependents: EntityObjectDependents) {
		this.$log.warn('Updating autonumbers on %o', dependents);

		let subEos = dependents.current();
		if (!subEos || subEos.length === 0) {
			return;
		}

		subEos[0].getMeta().subscribe(meta => {
			meta.getAttributes().forEach(attributeMeta => {
				if (attributeMeta.isAutonumber()) {
					this.updateAutonumbersForAttribute(dependents, attributeMeta);
				}
			});
		});
	}

	updateAutonumbersForAttribute(
		dependents: EntityObjectDependents,
		attributeMeta: EntityAttrMeta
	) {
		if (this.updateInProgress.indexOf(dependents) > -1) {
			this.$log.warn('Update of autonumbers for attribute %o on %o already in progress', attributeMeta, dependents);
			return;
		}

		this.$log.debug('Updating autonumbers for attribute %o on %o', attributeMeta, dependents);

		let subEos = dependents.current();
		if (!subEos || subEos.length === 0) {
			this.$log.warn('Could not update autonumbers - no dependents');
			return;
		}

		let attributeId = attributeMeta.getAttributeID();
		let forUpdate: SubEntityObject[] = [];
		let forDelete: SubEntityObject[] = [];

		subEos.forEach(subEo => {
			if (subEo.isMarkedAsDeleted()) {
				forDelete.push(subEo);
			} else {
				forUpdate.push(subEo);
			}
		});

		forUpdate.sort(
			(eo1, eo2) => {
				let number1 = eo1.getAttribute(attributeId);
				let number2 = eo2.getAttribute(attributeId);

				if (typeof number1 === 'number' && typeof number2 !== 'number') {
					return -1;
				} else if (typeof number1 !== 'number' && typeof number2 === 'number') {
					return 1;
				}

				return number1 - number2;
			}
		);

		this.updateInProgress.push(dependents);
		this.writeAutonumbers(forUpdate, attributeId);
		this.deleteAutonumbers(forDelete, attributeId);
		delete this.updateInProgress[this.updateInProgress.indexOf(dependents)];
	}

	private writeAutonumbers(clonedDependents: SubEntityObject[], attributeId: string) {
		try {
			clonedDependents.forEach((eo, index) => {
				eo.setAttribute(attributeId, index + 1);
			});
		} catch (e) {
			this.$log.error(e);
		}
	}

	private deleteAutonumbers(forDelete: SubEntityObject[], attributeId: string) {
		try {
			forDelete.forEach(eo => eo.setAttribute(attributeId, undefined));
		} catch (e) {
			this.$log.error(e);
		}
	}
}
