import { Injectable } from '@angular/core';
import { EntityObjectEventService } from './entity-object-event.service';
import { DialogService } from '../../dialog/dialog.service';
import { Logger } from '../../log/shared/logger';

@Injectable()
export class EntityObjectErrorService {

	constructor(
		private eoEventService: EntityObjectEventService,
		private dialog: DialogService,
		private $log: Logger,
	) {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => {
				this.$log.warn('selected eo: %o', eo);
				let error = eo && eo.getError();
				if (error) {
					this.$log.debug('Error %o in selected EO %o', error, eo);
					this.dialog.alert({
						title: 'Error',
						message: error
					});
				}
			}
		);
	}
}
