import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { EntityObjectService } from './entity-object.service';
import { SortModel } from './sort.model';
import { AutonumberService } from './autonumber.service';

export class EntityObjectDependents {
	private subject = new BehaviorSubject<SubEntityObject[] | undefined>(undefined);

	constructor(
		private parent: EntityObject,
		private referenceAttributeId: string,
	) {
	}

	asObservable(): Observable<SubEntityObject[] | undefined> {
		return this.subject;
	}

	current(): SubEntityObject[] | undefined {
		return this.subject.value;
	}

	set(subEos: SubEntityObject[]) {
		this.subject.next(subEos);
		AutonumberService.instance.updateAutonumbers(this);
	}

	/**
	 * Loads all dependents for the given attribute FQN.
	 *
	 * @param attributeFQN
	 * @returns {Subject<SubEntityObject[]>}
	 */
	load(sortModel?: SortModel) {
		if (this.parent.isNew()) {
			this.subject.next([]);
		} else {
			this.subject.next(undefined);
			this.getService().loadDependents(
				this.parent,
				this.referenceAttributeId,
				sortModel
			).subscribe(
				(dependents: SubEntityObject[]) => {
					this.subject.next(dependents);
				}
			);
		}
	}

	addAll(dependents: SubEntityObject[]) {
		let currentDependents = this.subject.value || [];
		if (dependents.length > 0) {
			this.parent.setDirty(true);
		}
		currentDependents.unshift(...dependents);
		AutonumberService.instance.updateAutonumbers(this);

		this.subject.next(currentDependents);
	}

	removeAll(dependents: SubEntityObject[]) {
		if (dependents.length === 0) {
			return;
		}

		let currentDependents = this.subject.value || [];
		dependents.forEach(dependent => {
			let index = currentDependents.indexOf(dependent);
			if (index >= 0) {
				currentDependents.splice(index, 1);
				this.parent.setDirty(true);
			}
		});

		AutonumberService.instance.updateAutonumbers(this);

		this.subject.next(currentDependents);
	}

	isEmpty(): boolean {
		let current = this.current();
		return current !== undefined && current.length > 0;
	}

	private getService() {
		return EntityObjectService.instance;
	}
}
