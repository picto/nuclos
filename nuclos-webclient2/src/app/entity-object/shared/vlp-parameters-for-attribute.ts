import { VlpParameter } from './vlp-parameter';

export class VlpParametersForAttribute {
	private parameters = new Map<string, VlpParameter>();

	set(parameter: VlpParameter) {
		this.parameters.set(parameter.getName(), parameter);
	}

	get(name: string) {
		return this.parameters.get(name);
	}

	forEach(callback: (value: VlpParameter, key: string) => any) {
		this.parameters.forEach(callback);
	}

	merge(otherParameters: VlpParametersForAttribute): VlpParametersForAttribute {
		let result = new VlpParametersForAttribute();

		this.parameters.forEach(parameter => {
			result.set(parameter);
		});

		otherParameters.parameters.forEach((parameter, key) => {
			let existing = result.get(key);
			// Overwrite if newer
			if (!existing || existing.getChangedAt().getTime() < parameter.getChangedAt().getTime()) {
				result.set(parameter);
			}
		});

		return result;
	}
}
