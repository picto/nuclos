import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Logger } from '../../log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	Preference,
	SearchtemplateAttribute,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { FqnService } from '../../shared/fqn.service';
import { EntityMeta } from './bo-view.model';

/**
 * TODO: Name makes no sense and is misleading.
 */
@Injectable()
export class SelectableService {

	constructor(
		private preferences: PreferencesService,
		private fqnService: FqnService,
		private $log: Logger
	) {
	}


	savePreference(preferenceItem: Preference<AttributeSelectionContent>): Observable<Preference<AttributeSelectionContent>> {

		return new Observable<Preference<AttributeSelectionContent>>(observer => {

			preferenceItem.content.userdefinedName =
				preferenceItem.name !== undefined && preferenceItem.name.length > 0
				&& preferenceItem.name !== this.getDefaultName(preferenceItem);

			// update preference name with default name if not overridden by user
			if (!preferenceItem.content.userdefinedName) {
				preferenceItem.name = this.getDefaultName(preferenceItem);
			}

			let preferenceItemCopy = JSON.parse(JSON.stringify(preferenceItem)) as Preference<AttributeSelectionContent>;

			let selectedColumns = preferenceItemCopy.content.columns
				.filter(col => col.selected)
				.sort((a, b) => (
						a.position === undefined || b.position === undefined
							? 0
							: a.position - b.position
					)
				);
			preferenceItemCopy.content.columns = selectedColumns;

			if (preferenceItemCopy.name && preferenceItemCopy.name.length > 50) {
				preferenceItemCopy.name = preferenceItemCopy.name.substring(0, 50) + '...';
			}

			// TODO: Ugly hack! Do not add unnecessary attributes in the first place.
			// save only necessary attributes
			let changedColumnData = preferenceItemCopy.content.columns.map(
				column => {
					if (preferenceItemCopy.type === 'table' || preferenceItemCopy.type === 'subform-table') {
						return {
							boAttrId: column.boAttrId,
							width: column.width,
							position: column.position,
							sort: column.sort ? {
								direction: column.sort.direction,
								prio: column.sort.prio,
							} : undefined
						} as ColumnAttribute
					} else if (preferenceItemCopy.type === 'searchtemplate') {
						let searchtemplateColumn = column as SearchtemplateAttribute;
						return {
							boAttrId: searchtemplateColumn.boAttrId,
							selected: searchtemplateColumn.selected,
							inputType: searchtemplateColumn.inputType,
							operator: searchtemplateColumn.operator,
							enableSearch: searchtemplateColumn.enableSearch,
							isValid: searchtemplateColumn.isValid,
							value: searchtemplateColumn.value,
						} as SearchtemplateAttribute;								}
					return column;
				}
			);

			let savePref = (pref) => {
				this.preferences.savePreferenceItem(pref).subscribe(
					(data) => {
						if (data !== undefined && data.prefId !== undefined) {
							preferenceItem.prefId = data.prefId;
						}
						observer.next(preferenceItem);
						observer.complete();
					},
					(error) => {
						this.$log.error('Unable to save preferences.', error);
						observer.error(error);
					}
				);
			};

			if (preferenceItemCopy.prefId) {
				// TODO don't load pref again
				this.preferences.getPreference(preferenceItemCopy.prefId).subscribe((existingPref: Preference<AttributeSelectionContent>) => {
					existingPref.name = preferenceItemCopy.name;
					existingPref.content.userdefinedName = preferenceItemCopy.content.userdefinedName;

					// don't overwrite / remove existing column attributes from preferences
					// the Java client uses attributes the webclient didn't know about
					existingPref.content.columns = changedColumnData.map(newColumn => {
						let existingColumn = existingPref.content.columns.filter(col => col.boAttrId === newColumn.boAttrId).shift();
						if (existingColumn) {
							return Object.assign(existingColumn,  newColumn);
						}
						return newColumn as ColumnAttribute;
					});

					if (existingPref.content['sideviewMenuWidth']) {
						(<Preference<SideviewmenuPreferenceContent>> existingPref).content.sideviewMenuWidth
							= (<Preference<SideviewmenuPreferenceContent>> preferenceItemCopy).content.sideviewMenuWidth;
					}

					savePref(existingPref);
				});
			} else {
				preferenceItemCopy.content.columns = changedColumnData;
				savePref(preferenceItemCopy);
			}
		});
	}


	getDefaultName(preferenceItem: Preference<AttributeSelectionContent>) {

		let selectedAttributes = preferenceItem.content.columns.filter(function (elem) {
			return elem.selected;
		});

		return selectedAttributes
			.map(a => a.name)
			.join(', ');
	}

	updatePreferenceName(selectedPreference: Preference<AttributeSelectionContent> | undefined) {
		if (selectedPreference && !selectedPreference.content.userdefinedName) {
			selectedPreference.name = this.getDefaultName(selectedPreference);
		}
	}

	addMetaDataToColumns(pref: Preference<AttributeSelectionContent>, meta: EntityMeta) {
		if (pref.content && pref.content.columns) {
			pref.content.columns.forEach(column => {

				// filter out columns from other EO's (at the moment only supported by the Java-client result list)
				if (column.boAttrId.startsWith(meta.getBoMetaId() + '_')) {
					let attributeMeta = meta.getAttributeMetaByFqn(column.boAttrId);
					if (attributeMeta) {
						column.name = attributeMeta.getName();
						column.nullable = attributeMeta.isNullable();
					}
				}
			});
		}
	}
}
