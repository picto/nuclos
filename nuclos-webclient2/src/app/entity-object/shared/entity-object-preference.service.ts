import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Logger } from '../../log/shared/logger';
import { PerspectiveService } from '../../perspective/shared/perspective.service';
import { SubformTablePreferenceSelection } from '../../perspective/shared/subform-table-preference-selection';
import {
	Preference,
	PreferenceType,
	SearchtemplateAttribute,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { SearchtemplateService } from '../../searchtemplate/searchtemplate.service';
import { FqnService } from '../../shared/fqn.service';
import { EntityMeta } from './bo-view.model';
import { MetaService } from './meta.service';
import { SelectableService } from './selectable.service';
import { SideviewmenuService } from './sideviewmenu.service';

/**
 * holds preference data for the actually selected EO class
 * the preference data will always be loaded when navigating to another EO class
 */
@Injectable()
export class EntityObjectPreferenceService {

	private selectedSubformColumnPreferences: Map<string, BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined>> = new Map();

	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);
	selectedSearchtemplatePref$: BehaviorSubject<Preference<SearchtemplatePreferenceContent> | undefined>
		= new BehaviorSubject(undefined);

	constructor(
		private selectableService: SelectableService,
		private sideviewmenuService: SideviewmenuService,
		private searchtemplateService: SearchtemplateService,
		private preferenceService: PreferencesService,
		private perspectiveService: PerspectiveService,
		private $log: Logger,
		private fqnService: FqnService,
		private metaService: MetaService
	) {
		this.perspectiveService.observeSelectedTablePreference()
			.filter(pref => !!pref)
			.subscribe(
				pref => this.selectTablePreference(pref)
			);
		this.perspectiveService.observeSelectedSearchtemplatePreference().subscribe(
			pref => this.selectSearchtemplate(pref)
		);
		this.perspectiveService.observeSelectedSubformTablePreferences().subscribe(
			selection => this.selectSubformTable(selection)
		);

		this.selectedSearchtemplatePref$.subscribe(
			pref => this.updateColumns(pref)
		);
	}

	getSelectedSearchtemplatePreference(): Preference<SearchtemplatePreferenceContent> | undefined {
		return this.selectedSearchtemplatePref$.getValue();
	}

	getSelectedTablePreference() {
		return this.selectedSideviewmenuPref$.getValue();
	}

	private updateColumns(searchtemplate: Preference<SearchtemplatePreferenceContent> | undefined) {
		if (searchtemplate && searchtemplate.content.columns) {
			this.metaService.getEntityMeta(searchtemplate.boMetaId).subscribe(meta => {
				searchtemplate.content.columns.forEach(
					column => {
						try {
							meta.getAttributes().forEach((_attributeMeta, attributeKey) => {
								let shortAttributeName = this.fqnService.getShortAttributeName(meta.getBoMetaId(), column.boAttrId);
								if (attributeKey === shortAttributeName) {
									column.inputType = this.searchtemplateService.getInputType(<SearchtemplateAttribute>meta.getAttribute(attributeKey));
									let operator = this.searchtemplateService.getOperatorDefinition(column);
									if (operator) {
										this.searchtemplateService.formatValue(column, operator);
									}
									throw 'break';
								}
							});
						} catch (e) {
							// Ignore
						}
					}
				);
			});
		}
	}

	loadPreferences(
		meta: EntityMeta,
		urlQuery: string | undefined
	): Observable<any> {

		return this.preferenceService.getPreferences({
			boMetaId: meta.getBoMetaId(),
			type: [PreferenceType.table.name, PreferenceType.searchtemplate.name],
			selected: true
		}).do(
			preferences => {
				this.applySideviewMenuIfNoPerspective(preferences, meta);
				this.applySearchtemplateIfNoPerspective(preferences, meta, urlQuery);
			}
		);
	}

	private applySideviewMenuIfNoPerspective(
		preferences,
		meta: EntityMeta
	) {
		let selectedPerspective = this.perspectiveService.getSelectedPerspective();
		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			this.$log.debug('Perspective is overriding user-selected result table preferences, ignoring.');
			return;
		}

		let sideviewmenuPreference;
		let sideviewmenuPrefs = preferences
			.filter(
				pref => pref.type === PreferenceType.table.name
				|| pref.type === PreferenceType.subformTable.name) as Preference<SideviewmenuPreferenceContent>[];

		// set selected preference
		sideviewmenuPreference = sideviewmenuPrefs.filter(pref => pref.selected).shift();

		// if none is selected use the first one
		if (!sideviewmenuPreference && sideviewmenuPrefs.length > 0) {
			sideviewmenuPreference = sideviewmenuPrefs.shift();
		}

		if (!sideviewmenuPreference) {
			sideviewmenuPreference = this.sideviewmenuService.emptySideviewmenuPreference(meta, true, false);
		}

		// add meta data to columns
		this.selectableService.addMetaDataToColumns(sideviewmenuPreference, meta);

		this.selectTablePreference(sideviewmenuPreference);
	}

	private applySearchtemplateIfNoPerspective(
		preferences,
		meta: EntityMeta,
		urlQuery: string | any,
	) {
		let selectedPerspective = this.perspectiveService.getSelectedPerspective();
		let searchtemplatePreference;

		if (selectedPerspective && selectedPerspective.searchTemplatePrefId) {
			this.$log.debug('Perspective is overriding default user-selected search template, ignoring.')
			return;
		}

		if (urlQuery) {
			searchtemplatePreference = this.searchtemplateService.createFromUrlSearch(meta, urlQuery);
		} else {
			searchtemplatePreference = preferences
				.filter(pref => pref.type === PreferenceType.searchtemplate.name)
				.find(pref => pref.selected as boolean);

			if (!searchtemplatePreference) {
				searchtemplatePreference = preferences
					.filter(pref => pref.type === PreferenceType.searchtemplate.name)
					.shift() as Preference<SearchtemplatePreferenceContent>;
			}
		}

		// add meta data to columns
		if (searchtemplatePreference) {
			this.selectableService.addMetaDataToColumns(searchtemplatePreference, meta);
		}
		this.$log.warn('selected searchtemplate: %o', searchtemplatePreference);

		this.selectedSearchtemplatePref$.next(searchtemplatePreference);
	}

	getSubformColumnPreferenceSelection(
		entityClassId: string
	): BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> {
		let subject = this.selectedSubformColumnPreferences.get(entityClassId);

		if (!subject) {
			subject = new BehaviorSubject(undefined);
			this.selectedSubformColumnPreferences.set(entityClassId, subject);
		}

		this.$log.debug('Subject for %o: %o', entityClassId, subject);
		return subject;
	}

	loadSubformPreferences(
		subformMeta: EntityMeta,
		parentEntityUid: string,
		layoutId: string | undefined
	): Observable<Preference<SideviewmenuPreferenceContent>> {
		let subj = this.getSubformColumnPreferenceSelection(subformMeta.getBoMetaId());

		let fromPerspective = this.perspectiveService.getSelectedSubformTablePreference(subformMeta);
		let fromPreferences = this.preferenceService.getPreferences(
			{
				boMetaId: subformMeta.getBoMetaId(),
				type: ['subform-table'],
				layoutId: layoutId,
				orLayoutIsNull: true
			},
			false
		)
			.map(preferences => {
				// selected prefs at beginning of list (should be max one) - later take the first
				preferences.sort((a, b) => {
					return a.selected ? -1 : 1;
				});
				return preferences;
			})
			.filter(preferences => preferences.length > 0)
			.map(preferences => preferences[0] as Preference<SideviewmenuPreferenceContent>);
		let createNew = Observable.of(undefined).map(() => {
			// no preference saved -> create a new one
			let newSubformTablePreference = this.sideviewmenuService.emptySubformPreference(subformMeta, parentEntityUid, true);
			newSubformTablePreference.selected = true;

			newSubformTablePreference.content.columns = newSubformTablePreference.content.columns.filter(
				c => c.selected);

			return newSubformTablePreference;
		});

		return fromPerspective
			.concat(fromPreferences)
			.filter(pref => !!pref)
			.concat(createNew)
			.take(1)
			.do(selectedSubformColumnPreference => {
					subj.next(selectedSubformColumnPreference as Preference<SideviewmenuPreferenceContent>);
				}
			);
	}

	selectTablePreference(pref: Preference<SideviewmenuPreferenceContent> | undefined
	) {
		this.$log.warn('Selecting sideview menu pref: %o', pref && pref.boMetaId);
		this.selectedSideviewmenuPref$.next(pref);
	}

	selectSearchtemplate(pref: Preference<SearchtemplatePreferenceContent> | undefined
	) {
		this.selectedSearchtemplatePref$.next(pref);
	}

	private	selectSubformTable(
		selection: SubformTablePreferenceSelection
	) {
		if (!selection) {
			return;
		}
		let subject = this.selectedSubformColumnPreferences.get(selection.entityClassId);
		if (subject) {
			this.$log.debug('Selecting subform prefs: %o', selection);
			subject.next(selection.subformTablePreference);
		}
	}
}
