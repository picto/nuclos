import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { EntityObject } from './entity-object.class';

@Injectable()
export class EntityObjectEventService {

	private selectedEo: BehaviorSubject<EntityObject | undefined>;
	private deletedEo: Subject<EntityObject>;
	private newEo: Subject<EntityObject>;
	private resetEo: Subject<EntityObject>;

	constructor() {
		this.selectedEo = new BehaviorSubject<EntityObject | undefined>(undefined);
		this.deletedEo = new Subject<EntityObject>();
		this.newEo = new Subject<EntityObject>();
		this.resetEo = new Subject<EntityObject>();
	}

	observeSelectedEo(): Observable<EntityObject | undefined> {
		return this.selectedEo.distinctUntilChanged();
	}

	/**
	 * Emits the IDs of deleted EOs.
	 *
	 * @returns {Subject<number>}
	 */
	observeDeletedEo(): Observable<EntityObject> {
		return this.deletedEo.distinctUntilChanged();
	}

	/**
	 * Emits the IDs of new EOs.
	 *
	 * @returns {Subject<number>}
	 */
	observeNewEo(): Observable<EntityObject> {
		return this.newEo;
	}

	/**
	 * Emits the IDs of reset EOs.
	 *
	 * @returns {Subject<number>}
	 */
	observeResetEo(): Subject<EntityObject> {
		return this.resetEo;
	}

	emitSelectedEo(eo: EntityObject | undefined) {
		this.selectedEo.next(eo);
	}

	emitDeletedEo(eo: EntityObject) {
		this.deletedEo.next(eo);
	}

	emitNewEo(eo: EntityObject) {
		this.newEo.next(eo);
	}

	emitResetEo(eo: EntityObject) {
		this.resetEo.next(eo);
	}
}
