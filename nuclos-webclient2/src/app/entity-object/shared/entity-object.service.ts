import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import { MandatorData } from '../../authentication/authentication.service';
import { CommandService } from '../../command/shared/command.service';
import { DialogService } from '../../dialog/dialog.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { LayoutService } from '../../layout/shared/layout.service';
import { Logger } from '../../log/shared/logger';
import { RuleService } from '../../rule/shared/rule.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { ObservableUtils } from '../../shared/observable-utils';
import { LocalStorageService } from '../../storage/shared/local-storage.service';
import { NuclosValidationService } from '../../validation/nuclos-validation.service';
import { ValidationError } from '../../validation/validation-error';
import { ValidationStatus } from '../../validation/validation-status.enum';
import { BoViewModel, EntityAttrMeta, EntityObjectData, LovEntry } from './bo-view.model';
import { EntityObjectEventService } from './entity-object-event.service';
import { EntityObject, SubEntityObject } from './entity-object.class';
import { LovDataService } from './lov-data.service';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';
import { SortModel } from './sort.model';
import { AutonumberService } from './autonumber.service';

/**
 * Provides access to the currently selected EntityObject.
 */
@Injectable()
export class EntityObjectService {
	static instance: EntityObjectService;

	static readonly EO_LOCALSTORAGE_KEY_PREFIX = 'EntityObject_';
	/* an EntityObject could be assigned to a certain mandator */
	private selectedMandator: MandatorData;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private metaService: MetaService,
		private http: NuclosHttpService,
		private $log: Logger,
		private nuclosI18nService: NuclosI18nService,
		private dialogService: DialogService,
		private ruleService: RuleService,
		private inputRequiredService: InputRequiredService,
		private lovDataService: LovDataService,
		private validationService: NuclosValidationService,
		private commandService: CommandService,
		private layoutService: LayoutService,
		private eoEventService: EntityObjectEventService,
		private localStorageService: LocalStorageService,
		private autonumberService: AutonumberService,
	) {
		EntityObjectService.instance = this;
	}

	selectMandator(mandator: MandatorData) {
		this.selectedMandator = mandator;
	}

	getSelectedMandator(): MandatorData {
		return this.selectedMandator;
	}

	listenForExternalChanges(eo: EntityObject) {
		if (eo.isNew()) {
			return;
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		this.localStorageService.observeItem(key).subscribe((data: EntityObjectData) => {
			eo.mergeData(data);
		});
	}

	publishChanges(eo: EntityObject) {
		if (eo.isNew()) {
			return;
		}

		let key = EntityObjectService.EO_LOCALSTORAGE_KEY_PREFIX + eo.getId();
		this.localStorageService.setItem(key, eo.getData());
		this.localStorageService.removeItem(key);
	}

	/**
	 * Loads the EO for the given entity and ID.
	 *
	 * @param entityClassId
	 * @param entityObjectId
	 */
	loadEO(entityClassId: string, entityObjectId: number | string): Observable<EntityObject> {
		this.$log.debug('Loading EO %o (%o)...', entityClassId, entityObjectId);
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/bos/' + entityClassId + '/' + entityObjectId
		)
			.map((response: Response) => new EntityObject(response.json()))
			.do((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo).subscribe());
	}

	reloadEo(eo: EntityObject): Observable<EntityObject> {
		let url = eo.getSelfURL();

		if (!url) {
			return Observable.throw('Could not reload EO %o - no self URL');
		}

		return this.http.get(url)
			.map((response: Response) => response.json())
			.do(data => eo.setData(data))
			.map(() => eo);
	}

	/**
	 * Saves the given EO.
	 * If it is new, it is inserted via a POST request.
	 * If it is not new, it is updated via a PUT request on its self-url.
	 *
	 * @param eo
	 * @returns {Observable<R>}
	 */
	save(eo: EntityObject, customRule?: string): Observable<EntityObject> {
		this.$log.debug('Saving: %o', eo);

		let postData: EntityObjectData = eo.serialize();

		if (customRule) {
			// TODO: Do not manipulate EO data for this!
			// Use the rule execution REST service instead and pass the rule name as parameter.
			postData.executeCustomRule = customRule;
		}

		let url = eo.isNew() ? eo.getInsertURL() : eo.getSelfURL();

		// force update of title / info
		delete eo.title;
		delete eo.info;

		// No self-link yet, if this EO is new
		if (!url) {
			// TODO: Get the INSERT-Link via HATEOAS
			url = this.nuclosConfig.getRestHost() + '/bos/' + eo.getEntityClassId();
		}

		let isNew = eo.isNew();
		let result = isNew
			? this.http.post(url, postData)
			: this.http.put(url, postData);

		return this.handleSaveRequest(
			eo,
			postData,
			result,
		).do(() => {
			if (isNew) {
				this.eoEventService.emitNewEo(eo);
			}
		});
	}

	executeCustomRule(eo: EntityObject, rule: string) {
		if (eo.isNew()) {
			this.$log.error('Cannot execute custom rule on unsaved EO.');
			return Observable.of(eo);
		}

		if (eo.isDirty()) {
			// TODO: Use the same REST service here
			return this.save(eo, rule);
		} else {
			let url = eo.getSelfURL() + '/execute/' + rule;

			let postData = {};
			let result = this.http.post(url, postData);

			return this.handleSaveRequest(
				eo,
				postData,
				result
			);
		}
	}

	private handleSaveRequest(
		eo: EntityObject,
		postData: any,
		saveRequest: Observable<any>
	) {
		return ObservableUtils.onSubscribe(
			saveRequest,
			() => eo.saving(true)
		).map(response => {
			this.$log.debug('Saving complete: %o', eo);
			let newData = response.json();
			eo.saved(newData);
			return eo;
		}).retryWhen(
			// Retry until all InputRequired exceptions are handled
			errors => {
				eo.saving(false);
				return errors.flatMap(
					error => this.inputRequiredService.handleError(error, postData)
						.finally(() => eo.saving(false))
				);
			}
		).do(
			savedEO => this.commandService.executeCommands(savedEO)
		).catch(
			error => {
				eo.saving(false);
				return this.handleError(error, eo);
			}
		).finally(
			() => eo.saving(false)
		);
	}

	/**
	 * Deletes the given EO.
	 * If it is new, it only a "delete" event is emitted.
	 * If it is not new, it is deleted from the server first.
	 *
	 * @param eo
	 * @returns {any}
	 */
	delete(eo: EntityObject): Observable<EntityObject> {
		// If the EO is new, just emit a "deleted" event and return
		if (eo.isNew()) {
			this.eoEventService.emitDeletedEo(eo);
			return Observable.of(eo);
		}

		// If the EO is not new, delete it on the server first
		let url = eo.getSelfURL();
		if (!url) {
			this.$log.error('EO has no self-URL: %o', eo);
			return Observable.of(eo);
		}

		return this.http.delete(url).do(() => {
				eo.deleted();
				this.eoEventService.emitDeletedEo(eo);
				return eo;
			}
		).catch(
			error => this.handleError(error, eo)
		);
	}

	/**
	 * Unlocks the given EO.
	 *
	 * @param eo
	 * @returns {any}
	 */
	unlock(eo: EntityObject): Observable<EntityObject> {

		let links = eo.getLinks();

		if (!links || !links.lock) {
			this.$log.error('EO has no lock-URL: %o', eo);
			return Observable.of(eo);
		}

		return this.http.delete(links.lock.href).map(
			() => {
				let wasDirty = eo.isDirty();
				eo.setAttribute('nuclosOwner', null);
				if (links) {
					delete links.lock;
				}
				eo.setDirty(wasDirty);
				return eo;
			}
		).catch(
			error => this.handleError(error, eo)
		);
	}

	/**
	 * Creates a new empty EO for the currently selected entity class,
	 * which can be used for inserts.
	 *
	 * @param entityMetaId Optional parent EO, if a subform EO is being created.
	 * @param parentEO
	 * @returns {Observable<EntityObject>|"../../Observable".Observable<EntityObject>|"../../../Observable".Observable<EntityObject>}
	 */
	createNew(
		entityMetaId: string,
		parentEO?: EntityObject
	): Observable<EntityObject> {

		// TODO cache REST calls
		return new Observable<EntityObject>(observer => {
			if (entityMetaId !== undefined) {
				this.metaService.getBoMeta(entityMetaId).subscribe(
					meta => {
						let defaultGenerationLink = meta.getLinks().defaultGeneration;
						if (defaultGenerationLink) {
							this.http.get(defaultGenerationLink.href)
								.map((response: Response) => new EntityObject(response.json()))
								.do((eo: EntityObject) => this.ruleService.updateRuleExecutor(eo, parentEO).subscribe())
								.subscribe(
									eo => {
										eo.setDirty(true);
										eo.getData()._flag = 'insert';
										if (this.selectedMandator) {
											eo.setMandator(this.selectedMandator);
										}
										observer.next(eo);
										observer.complete();
									}
								);
						}
					}
				);
			}
		});
	}

	private handleError(error: Response | any, eo: EntityObject) {
		let errorMessage = this.getErrorMessage(error);

		if (errorMessage) {
			this.showErrorMessage(errorMessage, error);
		}
		this.handleValidationErrors(error, eo);

		return Observable.throw(errorMessage);
	}

	private getErrorMessage(error: Response | any) {
		let errorMessage: string | undefined;

		if (error instanceof Response) {
			errorMessage = this.getErrorMessageFromResponse(error) || 'webclient.error.code' + error.status;
		} else {
			errorMessage = error.message ? error.message : error.toString();
		}

		if (errorMessage) {
			errorMessage = this.nuclosI18nService.getI18n(errorMessage);
		}

		return errorMessage;
	}

	private getErrorMessageFromResponse(error: Response | any) {
		let result = undefined;

		try {
			const json = error.json() || '';
			result = json.message
				.replace('<html>', '')
				.replace('</html>', '')
				.replace('<body>', '')
				.replace('</body>', '');
		} catch (e) {
		}

		return result;
	}

	private showErrorMessage(errorMessage: string, error: Response | any) {
		this.$log.error(errorMessage, error);

		this.dialogService.alert(
			{
				title: this.nuclosI18nService.getI18n('webclient.error.title'),
				message: errorMessage
			}
		);
	}

	private handleValidationErrors(error: Response | any, eo: EntityObject) {
		if (error instanceof Response) {
			let json;
			try {
				json = error.json();
			} catch (e) {
			}

			if (json && json.validationErrors) {
				let validationErrors: ValidationError[] = json.validationErrors;
				for (let validationError of validationErrors) {
					eo.setAttributeValidationError(validationError);
				}
			}
		}
	}

	/**
	 * Loads dependents which reference the given EO via the given (foreign) attribute.
	 *
	 * @param eo The parent EO for which dependents should be loaded.
	 * @param attributeFQN
	 */
	loadDependents(eo: EntityObject, attributeFQN: string, sortModel?: SortModel): Observable<SubEntityObject[]> {
		this.$log.debug('Loading dependents for ref %o...', attributeFQN);

		let url = this.nuclosConfig.getRestHost()
			+ '/bos/' + eo.getEntityClassId() + '/' + eo.getId()
			+ '/subBos/' + attributeFQN;

		if (sortModel) {
			let sortString = sortModel.toString();
			if (sortString) {
				url += '?sort=' + sortString;
			}
		}

		return this.http.get(url)
			.map((response: Response) => {
				let result: SubEntityObject[] = [];
				let eoData: EntityObjectData[] = response.json().bos;
				for (let data of eoData) {
					let subEO = new SubEntityObject(
						eo,
						attributeFQN,
						data
					);
					result.push(subEO);
					this.ruleService.updateRuleExecutor(subEO, eo).subscribe();
				}
				this.$log.debug('Successfully loaded %o dependents for %o', result.length, attributeFQN);
				return result;
			}).catch(error => {
				this.$log.error('Failed to load depenents for %o: %o', attributeFQN, error);
				return Observable.throw(error);
			});
	}

	loadLovEntries(search: LovSearchConfig): Observable<LovEntry[]> {
		return this.lovDataService.loadLovEntries(search);
	}

	/**
	 * Checks for a layout change (because of changed nuclos-process).
	 * Sets the new layout URL on the given EO.
	 *
	 * @param eo
	 */
	nuclosProcessChanged(eo: EntityObject): void {
		let nuclosProcess = eo.getNuclosProcess();
		let nuclosProcessId = nuclosProcess && nuclosProcess.id;

		// TODO: Get From HATEOAS
		let url = this.nuclosConfig.getRestHost() + '/meta/processlayout/' + nuclosProcessId + '/' + eo.getEntityClassId();
		if (!eo.isNew()) {
			url += '/' + eo.getId();
		}

		this.http.getCachedJSON(url)
			.subscribe(data => {
				let layoutURL = data.links && data.links.layout && data.links.layout.href;
				if (layoutURL) {
					eo.setLayoutURL(layoutURL);
				}
			});
	}

	validateAttributeValue(value: any, meta: EntityAttrMeta) {
		return this.validationService.validate(value, meta);
	}

	getValidationStatus(attributeMeta: EntityAttrMeta, value: any): ValidationStatus {
		return this.validationService.getValidationStatus(attributeMeta, value);
	}

	getLayoutURLDynamically(eo: EntityObject): Observable<string | undefined> {
		return this.layoutService.getWebLayoutURLDynamically(eo);
	}

	loadEmptyList(metaId: string): Observable<BoViewModel> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/bos/' + metaId + '/query?offset=0&chunkSize=0&countTotal=false&gettotal=false'
		).map((response: Response) => {
			let result: BoViewModel = response.json();
			return result;
		});
	}
}
