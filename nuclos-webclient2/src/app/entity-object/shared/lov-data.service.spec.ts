/* tslint:disable:no-unused-variable */
import { inject, TestBed } from '@angular/core/testing';
import { LovDataService } from './lov-data.service';

describe('LovDataService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [LovDataService]
		});
	});

	it('should ...', inject([LovDataService], (service: LovDataService) => {
		expect(service).toBeTruthy();
	}));
});
