import * as _ from 'lodash';
import { BehaviorSubject, Observable, ReplaySubject, Subject, Subscriber } from 'rxjs';
import { MandatorData } from '../../authentication/authentication.service';
import { Command } from '../../command/shared/command';
import { Generation } from '../../generation/shared/generation';
import { Logger } from '../../log/shared/logger';
import { FqnService } from '../../shared/fqn.service';
import { Link, LinkContainer, SubEOLinkContainer } from '../../shared/link.model';
import { NuclosDefaults } from '../../shared/nuclos-defaults';
import { StringUtils } from '../../shared/string-utils';
import { State, StateInfo } from '../../state/shared/state';
import { ValidationError } from '../../validation/validation-error';
import { ValidationStatus } from '../../validation/validation-status.enum';
import { EntityAttrMeta, EntityMeta, EntityObjectData, LovEntry } from './bo-view.model';
import { EntityObjectEventListener } from './entity-object-event-listener';
import { EntityObjectResultService } from './entity-object-result.service';
import { EntityObjectService } from './entity-object.service';
import { LovSearchConfig } from './lov-search-config';
import { MetaService } from './meta.service';
import { SortModel } from './sort.model';
import { VlpContext } from './vlp-context';
import { AutonumberService } from './autonumber.service';
import { EntityObjectDependents } from './entity-object-dependents';

/**
 * Represents an entity object.
 * Holds a reference to the actual entity object data and provides additional methods.
 */
export class EntityObject {
	/**
	 * Remembers the old data when this EO is edited.
	 * This is used for resetting to the previous state when cancelling modifications.
	 */
	private oldEOData: EntityObjectData;

	private listeners: EntityObjectEventListener[];

	/**
	 * Holds a map of dependents (subform data), where the key is the attribute-FQN of the dependent.
	 *
	 * @type {Map<string, "../../Observable".Observable<EntityObject[]>>}
	 */
	private dependents = new Map<string, EntityObjectDependents>();

	/**
	 * Holds available entries for list-of-values components.
	 *
	 * @type {Map<string, "../../Observable".Observable<LovEntry[]>>}
	 */
	private lovEntries = new Map<string, Map<string, Subject<LovEntry[]>>>();
	private vlpContext = new VlpContext();

	private attributeValidation = new Map<string, ValidationStatus>();
	private validatingAttributes = false;

	/**
	 * Emits the layout URL after layout related events (e.g. selection of a perspective).
	 * Might emit duplicates.
	 */
	private layoutSubject = new Subject<string>();

	title: string;
	info: string;

	/**
	 * Returns a new empty EntityObject which can be used for inserting new EOs.
	 */
	static createNew(entityClassId?: string): EntityObject {
		Logger.instance.debug('new EO for class: %o', entityClassId);
		let emptyData: EntityObjectData = {
			attributes: {},
			boMetaId: entityClassId || '',
			canWrite: true,
			canDelete: false,
			links: {
				self: {
					// TODO: Get link for entity class from meta data
					href: ''
				}
			}
		};

		return new EntityObject(emptyData);
	}

	constructor(private eoData: EntityObjectData) {
		this.listeners = [];

		this.layoutSubject.distinctUntilChanged().subscribe(layoutURL => {
			this.lovEntries.clear();
			this.notifyLayoutChanged(layoutURL);
		});

		this.getService().listenForExternalChanges(this);
	}

	protected getService(): EntityObjectService {
		return EntityObjectService.instance;
	}

	protected getResultService(): EntityObjectResultService {
		return EntityObjectResultService.instance;
	}

	/**
	 * Clones this EO.
	 * The clone can be used as a new EO for inserts.
	 *
	 * @returns {EntityObject}
	 */
	clone(): EntityObject {
		return new EntityObject(this.cloneData());
	}

	/**
	 * Clones the underlying data of this EO while omitting the primary key.
	 *
	 * TODO: Omit other meta data, too?
	 *
	 * @returns {EntityObjectData}
	 */
	protected cloneData(): EntityObjectData {
		let clonedData = _.cloneDeep(this.getData());
		clonedData.boId = undefined;
		clonedData.temporaryId = undefined;
		clonedData._flag = 'insert';
		return clonedData;
	}

	addListener(listener: EntityObjectEventListener) {
		if (this.listeners.indexOf(listener) < 0) {
			this.listeners.push(listener);
		}
	}

	removeListener(listener: EntityObjectEventListener) {
		let index = this.listeners.indexOf(listener);
		if (index >= 0) {
			this.listeners.splice(index, 1);
		}
	}

	removeListenersByType(type: any) {
		for (let listener of this.listeners) {
			if (listener instanceof type) {
				this.removeListener(listener);
			}
		}
	}

	/**
	 * Returns the complete underlying data object.
	 * This should normally not be accessed directly.
	 *
	 * @returns {EntityObjectData}
	 */
	getData(): EntityObjectData {
		return this.eoData;
	}

	/**
	 * Sets the complete underlying data object.
	 * This should normally not be accessed directly.
	 *
	 * @returns {EntityObjectData}
	 */
	setData(data: EntityObjectData): void {
		this.eoData = data;
	}

	mergeData(data: EntityObjectData) {
		for (let attributeName in data.attributes) {
			if (data.attributes.hasOwnProperty(attributeName)) {
				let value = data.attributes[attributeName];
				this.setAttributeUnsafe(attributeName, value);
			}
		}
		this.eoData.version = data.version;
		if (data.nextStates !== undefined) {
			this.eoData.nextStates = data.nextStates;
		}
		if (data.title !== undefined) {
			this.eoData.title = data.title;
		}
		if (data.info !== undefined) {
			this.eoData.info = data.info;
		}
		this.reloaded();
	}

	/**
	 * Returns the record ID of this EO.
	 *
	 * @returns {Number}
	 */
	getId(): number | undefined {
		return this.eoData.boId;
	}

	/**
	 * Returns the temporary ID of a new, unsaved EO.
	 *
	 * @returns {string}
	 */
	getTemporaryId(): string | undefined {
		return this.eoData.temporaryId;
	}

	/**
	 * Returns the entity class ID (aka "boMetaId") of this EO.
	 *
	 * @returns {Number}
	 */
	getEntityClassId(): string {
		return this.eoData.boMetaId;
	}

	/**
	 * Determines if this EO is new.
	 * If so, it has no ID yet and should be flagged as 'insert'.
	 *
	 * @returns {boolean}
	 */
	isNew(): boolean {
		return !this.getId();
	}

	/**
	 * Determines if this EO is dirty, i.e. it has unsaved changes.
	 *
	 * @returns {boolean}
	 */
	isDirty(): boolean {
		return !!this.eoData.dirty;
	}

	/**
	 * Marks this EO as modified.
	 *
	 * It should not be necessary to call this externally!
	 * Attribute manipulations change this flag automatically.
	 *
	 * @param dirty
	 * @deprecated DO NOT USE!
	 */
	setDirty(dirty: boolean): EntityObject {
		return this._setDirty(dirty);
	}

	protected _setDirty(dirty: boolean): EntityObject {
		// Remember the current state when this EO gets dirty.
		if (dirty && !this.isDirty()) {
			this.oldEOData = _.cloneDeep(this.eoData);
		}
		this.eoData.dirty = dirty;

		if (this.isNew()) {
			this.getData()._flag = 'insert';
		} else if (!this.isMarkedAsDeleted()) {
			this.getData()._flag = 'update';
		}

		return this;
	}

	getRowColor() {
		return this.eoData.rowcolor;
	}

	getLinks(): LinkContainer | undefined {
		return this.eoData && this.eoData.links;
	}

	getSelfURL(): string | undefined {
		let links = this.getLinks();
		return links && links.self.href;
	}

	getInsertURL(): string | undefined {
		let links = this.getLinks();
		return links && links.insert && links.insert.href;
	}

	getLayoutURL(): string | undefined {
		let links = this.getLinks();
		return links && links.layout && links.layout.href;
	}

	getLayoutId(): string | undefined {
		let layoutUrl = this.getLayoutURL();
		return layoutUrl ? layoutUrl.split('/').pop() : undefined;
	}


	getLayoutURLDynamically(): Observable<string | undefined> {
		return this.getService().getLayoutURLDynamically(this);
	}

	setLayoutURL(layoutURL: string) {
		let oldLayoutURL = this.getLayoutURL();

		let links = this.getLinks();
		if (!links) {
			links = {} as LinkContainer;
		}
		if (!links.layout) {
			links.layout = {} as Link;
		}

		links.layout.href = layoutURL;

		if (layoutURL !== oldLayoutURL) {
			this.checkLayoutChange();
		}
	}

	getPrintoutURL(): string | undefined {
		let links = this.getLinks();
		return links && links.printouts && links.printouts.href;
	}

	/**
	 * Returns all attributes.
	 * Do not use this for modifications - use {@link setAttribute} instead.
	 *
	 * @returns {Object}
	 */
	getAttributes() {
		return this.eoData.attributes;
	}

	getAttribute(attributeId: string): any {
		let attributes = this.getAttributes();
		let attributeName = FqnService.getShortAttributeNameFailsafe(this.getEntityClassId(), attributeId);
		attributeName = StringUtils.replaceAll(attributeName, '_', '');

		let result = attributes[attributeName];

		// TODO: The following hack is needed because the REST service prefixes numeric attribute names with an underscore.
		if (typeof result === 'undefined') {
			attributeName = '_' + attributeName;
			result = attributes[attributeName];
		}

		return result;
	}

	getNuclosProcess(): any {
		return this.getAttribute('nuclosProcess');
	}

	getMandatorId(): string {
		let mandatorAttr = this.getAttribute('nuclosMandator');
		return mandatorAttr ? mandatorAttr.id : undefined;
	}

	/**
	 * Clears the attribute with the given ID.
	 * Does nothing if the attribute value is already falsy
	 * or the value is a reference pointing to nothing.
	 *
	 * @param attributeId
	 * @returns {any}
	 */
	clearAttribute(attributeId: string): any {
		let value = this.getAttribute(attributeId);

		if (!value ||
			(value.hasOwnProperty('id') && !value.id)
		) {
			Logger.instance.debug('Attribute %o already undefined - not clearing');
			return;
		} else {
			Logger.instance.debug('Clearing attribute %o, old value = %o', attributeId, value);
		}

		this.setAttribute(attributeId, null);
	}

	/**
	 * Sets the value for the attribute with the given ID
	 * and marks this EO as modified.
	 * This method does nothing if the old value equals the new value.
	 *
	 * @param attributeId
	 * @param value
	 */
	setAttribute(attributeId: string, value: any): void {
		this.setAttributeWithSubscriber(attributeId, value, undefined);
	}

	setAttributeObserved(attributeId: string, value: any): Observable<boolean> {
		return new Observable<boolean>(subscriber => this.setAttributeWithSubscriber(attributeId, value, subscriber));
	}

	private setAttributeWithSubscriber(attributeId: string, value: any,
		subscriber: Subscriber<boolean> | undefined
	): void {
		let attributeName = FqnService.getShortAttributeNameFailsafe(this.getEntityClassId(), attributeId);

		this.getAttributeMeta(attributeName).subscribe(meta => {
			let validatedValue = this.getService().validateAttributeValue(value, meta);
			let changed = this.setAttributeValidated(attributeName, validatedValue);
			if (subscriber) {
				subscriber.next(changed);
				subscriber.complete();
			}
		});
	}

	protected setAttributeValidated(attributeName: string, value: any): boolean {
		let attributes = this.getAttributes();
		Logger.instance.debug('Setting value %o for attribute %o', value, attributeName);

		let oldValue = attributes[attributeName];
		if (oldValue === value || this.hasEqualID(oldValue, value)) {
			Logger.instance.debug('Old attribute value equals new value, skipping');
			return false;
		}

		// setDirty() must be called before the value is actually changed!
		this._setDirty(true);
		this.setAttributeUnsafe(attributeName, value);

		for (let listener of this.listeners) {
			if (listener.afterAttributeChange) {
				listener.afterAttributeChange(this, attributeName, oldValue, value);
			}
		}

		if (attributeName === 'nuclosProcess') {
			this.getService().nuclosProcessChanged(this);
		}

		this.updateValidationStatusForAttributeName(attributeName);

		return true;
	}

	/**
	 * Only changes the attribute value without any validations or listener notifications etc.
	 *
	 * @param attributeName
	 * @param value
	 */
	setAttributeUnsafe(attributeNameOrId: string, value: any) {
		let attributeName = FqnService.getShortAttributeNameFailsafe(this.getEntityClassId(), attributeNameOrId);
		attributeName = StringUtils.replaceAll(attributeName, '_', '');

		let attributes = this.getAttributes();
		attributes[attributeName] = value;
	}

	/**
	 * Determines if the 2 given parameters are both truthy and have an equal "id" attribute.
	 *
	 * @param o1
	 * @param o2
	 * @returns {any|boolean}
	 */
	private hasEqualID(o1: any, o2: any): boolean {
		let result = o1 && o2
			&& o1.hasOwnProperty('id')
			&& o2.hasOwnProperty('id')
			&& o1.id === o2.id;

		return result;
	}

	isAttributeWritable(attributeName: string): boolean {
		let restriction = this.getAttributeRestriction(attributeName);
		return restriction !== 'readonly'
			&& restriction !== 'hidden'
			&& restriction !== 'disabled';
	}

	protected getAttributeRestriction(attributeName: string) {
		if (this.eoData.attrRestrictions !== undefined) {
			return this.eoData.attrRestrictions[attributeName];

		}
		return undefined;
	}

	/**
	 * Determines if this EO is deletable.
	 * New objects are not deletable (only cancelable).
	 *
	 * @returns {boolean}
	 */
	canDelete(): boolean {
		return this.eoData.canDelete === true && !this.isNew();
	}

	canWrite(): boolean {
		return this.eoData.canWrite === true;
	}

	/**
	 * Sets this EO as globally selected (for display/editing).
	 */
	select(): void {
		return this.getResultService().selectEo(this);
	}

	/**
	 * Saves this EntityObject.
	 * The result is an Observable which returns the updated EO after save.
	 *
	 */
	save(): Observable<EntityObject> {
		return this.getService().save(this);
	}

	saving(inProgress: boolean) {
		for (let listener of this.listeners) {
			if (listener.isSaving) {
				listener.isSaving(this, inProgress);
			}
		}
	}

	/**
	 * Triggers the 'saved' event for all listeners.
	 * And recursively for all dependents.
	 */
	saved(data: EntityObjectData): void {
		this.saving(false);

		this.setData(data);

		this._setDirty(false);
		this.getData()._flag = undefined;

		this.dependents.forEach(dependents => {
			// TODO: It is not always necessary to reload the dependents
			dependents.load();
		});

		for (let listener of this.listeners) {
			if (listener.afterSave) {
				listener.afterSave(this);
			}
		}

		this.checkLayoutChange();

		this.getService().publishChanges(this);
	}

	/**
	 * Re-loads the data for this EO from the server.
	 *
	 */
	reload(): Observable<EntityObject> {
		Logger.instance.debug('Reload: %o', this);

		let eoId = this.getId();
		if (!eoId) {
			return Observable.throw('Can not reload (new) EO without ID');
		}

		return this.getService().reloadEo(this).do(
			() => this.reloaded()
		);
	}

	private reloaded() {
		this.dependents.clear();
		this.getVlpContext().clear();
		this.checkLayoutChange();
		this.updateValidationStatus();

		for (let listener of this.listeners) {
			if (listener.afterReload) {
				listener.afterReload(this);
			}
		}
	}

	/**
	 * Deletes this EntityObject.
	 * The result is an Observable which returns the EO again after deletion.
	 */
	delete(): Observable<EntityObject> {
		return this.getService().delete(this);
	}

	isMarkedAsDeleted(): boolean {
		return this.getData()._flag === 'delete';
	}

	markAsDeleted(): void {
		this._setDirty(true);
		this.getData()._flag = 'delete';
	}

	/**
	 * Resets this EO to the state it had last before becoming dirty.
	 * Also resets all dependents.
	 */
	reset() {
		if (!this.isDirty()) {
			return;
		}

		this.getVlpContext().clear();

		for (let listener of this.listeners) {
			if (listener.beforeCancel) {
				listener.beforeCancel(this);
			}
		}

		/**
		 * On reset all new, unsaved dependents are deleted.
		 * Modified dependents are reset.
		 */
		this.dependents.forEach(value => {
				let newDependents: SubEntityObject[] = [];
				let subEos = value.current();
				if (subEos) {
					subEos.forEach(subEO => {
						if (!subEO.isNew()) {
							subEO.reset();
							newDependents.push(subEO);
						}
					});
				}
				value.set(newDependents);
			}
		);

		if (this.oldEOData) {
			this.setData(this.oldEOData);
			this.checkLayoutChange();
		}

		this._setDirty(false);
		this.updateValidationStatus();

		for (let listener of this.listeners) {
			if (listener.afterCancel) {
				listener.afterCancel(this);
			}
		}
	}

	getMeta(): Observable<EntityMeta> {
		return MetaService.instance.getEntityMeta(this.getEntityClassId());
	}

	getDetailMeta(): Observable<EntityMeta> {
		return new Observable<EntityMeta>(observer => {
			this.getMeta().subscribe(meta => {
					let detailEntityClassId = meta.getDetailEntityClassId();
					if (detailEntityClassId) {
						MetaService.instance.getEntityMeta(detailEntityClassId).subscribe(
							detailMeta => {
								observer.next(detailMeta);
								observer.complete();
							},
							error => observer.error(error)
						);
					} else {
						observer.complete();
					}
				},
				error => observer.error(error)
			);
		});
	}

	getAttributeMeta(attributeName: string): Observable<EntityAttrMeta> {
		return this.getMeta().map(meta => meta.getAttributeMeta(attributeName));
	}

	getAttributeLabel(attributeId: string): Observable<string> {
		return new Observable<string>(observer => {
				this.getMeta().subscribe(
					meta => {
						let label = meta.getAttributeLabel(attributeId);
						observer.next(label);
						observer.complete();
					},
					error => observer.error(error)
				);
			}
		);
	}

	/**
	 * Returns the links for all available Sub-EOs.
	 *
	 * @returns {any}
	 */
	getSubEOLinks(): Map<string, { links: SubEOLinkContainer }> | undefined {
		if (!this.eoData.subBos) {
			// TODO ERROR this.eoData.subBos is undefined !!!
			return undefined;
		}
		return this.eoData && this.eoData.subBos;
	}

	setSubEOLinks(subEOLinks: Map<string, { links: SubEOLinkContainer }>) {
		this.eoData.subBos = subEOLinks;
	}

	/**
	 * Returns the links for the Sub-EO for the given attribute ID.
	 *
	 * @param attributeID
	 * @returns {undefined}
	 */
	getSubEOLink(attributeID: string): SubEOLinkContainer | undefined {
		let subEOs = this.getSubEOLinks();
		return subEOs && subEOs[attributeID] && subEOs[attributeID].links;
	}

	/**
	 * Returns the restriction for the Sub-EO for the given attribute ID.
	 *
	 * @param attributeID
	 * @returns {undefined}
	 */
	getSubEORestriction(attributeID: string): String | undefined {
		let subEOs = this.getSubEOLinks();
		return subEOs && subEOs[attributeID] && subEOs[attributeID].restriction;
	}

	/**
	 * Returns the restriction for the Sub-EO for the given attribute ID.
	 *
	 * @param attributeID
	 * @returns {undefined}
	 */
	getSubEOReadOnlyAttributes(attributeID: string): String[] | undefined {
		let subEOs = this.getSubEOLinks();
		return subEOs && subEOs[attributeID] && subEOs[attributeID].readonlyattributes;
	}


	/**
	 * Returns the current Nuclos state of this EO.
	 *
	 * @returns {any}
	 */
	getState(): State | undefined {
		let result: State | undefined = undefined;
		let state = this.getAttribute('nuclosState');

		if (state) {
			let stateNumber = this.getAttribute('nuclosStateNumber');
			let stateIconLink = this.getStateIconLink();

			result = {
				nuclosStateId: state.id,
				name: state.name,
				number: stateNumber,

				links: {
					stateIcon: stateIconLink
				}
			};
		}

		return result;
	}

	getStateIconLink(): Link | undefined {
		let links = this.getLinks();
		return links && links.stateIcon;
	}

	/**
	 * Returns infos about all Nuclos states available for the next state change.
	 *
	 * @returns {StateInfo[]}
	 */
	getNextStates(): StateInfo[] {
		return this.eoData.nextStates ? this.eoData.nextStates : [];
	}

	/**
	 * Sets the given state on the current EO and saves it, without any confirmation dialogs etc.
	 *
	 * @param state
	 */
	changeState(state: State): Observable<EntityObject> {
		// NUCLOS-4661: Backup data in case the state change fails
		let wasDirty = this.isDirty();
		let oldState = this.getAttribute('nuclosState');

		this.setAttribute('nuclosState', {id: state.nuclosStateId});

		return this.save().catch(error => {
			this.setAttribute('nuclosState', oldState);
			this._setDirty(wasDirty);
			return Observable.throw(error);
		});
	}

	/**
	 * Returns all available object generators for this EO.
	 */
	getGenerations(): Generation[] {
		let result: Generation[] = [];

		if (this.eoData && this.eoData.generations) {
			result = this.eoData.generations;
		}

		return result;
	}

	/**
	 * Filters internal generations.
	 *
	 * @returns {Generation[]}
	 */
	getGenerationsForUser(): Generation[] {
		let result = this.getGenerations();

		result = result.filter(it => it.internal !== true);

		return result;
	}

	getError(): string | undefined {
		return this.eoData.businessError;
	}

	/**
	 * Executes the CustomRule with the given name.
	 * This also saves this EO.
	 *
	 * TODO: Ensure the given rule is available for this EO?
	 *
	 * @param rule
	 */
	executeRule(rule: string): Observable<EntityObject> {
		return this.getService().executeCustomRule(this, rule);
	}

	getDependents(attributeId: string, sortModel?: SortModel): EntityObjectDependents {
		let dependents = this.dependents.get(attributeId);

		if (!dependents) {
			dependents = new EntityObjectDependents(this, attributeId);
			this.dependents.set(attributeId, dependents);
			dependents.load(sortModel);
		}

		return dependents;
	}

	getLovEntries(
		attributeFQN: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		let attributeName = FqnService.getShortAttributeName('', attributeFQN);
		let vlpMap = this.lovEntries.get(attributeName);

		if (!vlpMap) {
			vlpMap = new Map<string, Subject<LovEntry[]>>();
			this.lovEntries.set(attributeName, vlpMap);
		}

		let vlpHash = ''; // TODO

		let result = vlpMap.get(vlpHash);
		if (!result) {
			result = new ReplaySubject<LovEntry[]>(1);
			vlpMap.set(vlpHash, result);

			this.loadLovEntries(attributeName, valuelistProvider).subscribe(lovEntries => {
				if (result) {
					result.next(lovEntries);
				}
			});
		}

		return result;
	}

	protected clearLovEntries() {
		this.lovEntries.clear();
	}

	refreshVlp(
		attributeId: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		let attributeName = FqnService.getShortAttributeName('', attributeId);

		let vlpMap = this.lovEntries.get(attributeName);

		if (!vlpMap) {
			vlpMap = new Map<string, Subject<LovEntry[]>>();
		}

		let vlpHash = ''; // TODO
		const result = vlpMap.get(vlpHash);
		if (!result) {
			return this.getLovEntries(attributeName, valuelistProvider);
		}

		this.loadLovEntries(attributeName, valuelistProvider).subscribe(lovEntries => {
			result.next(lovEntries);
		});

		return result;
	}

	invalidateValuelist(attributeId: string) {
		let attributeName = FqnService.getShortAttributeName('', attributeId);
		this.lovEntries.delete(attributeName);
	}

	/**
	 * TODO: VLPs can have default values, which should be selected automatically.
	 *
	 * @param attributeName
	 * @returns {Observable<LovEntry[]>}
	 */
	protected loadLovEntries(
		attributeName: string,
		valuelistProvider?: WebValuelistProvider
	): Observable<LovEntry[]> {
		attributeName = FqnService.getShortAttributeName('', attributeName);
		return this.getAttributeMeta(attributeName)
			.mergeMap(meta => {
				let search: LovSearchConfig = {
					eo: this,
					attributeMeta: meta,
					mandatorId: this.getMandatorId(),
					vlp: valuelistProvider,
					resultLimit: NuclosDefaults.DROPDOWN_LOAD_RESULT_LIMIT,
				};
				return this.getService().loadLovEntries(search);
			});
	}

	getVlpContext() {
		return this.vlpContext;
	}

	getVlpParameters(attributeId: string) {
		let result = this.getOwnVlpParameters(attributeId);
		let entityClassId = this.getEntityClassId();

		let vlpParameters = this.getRootEo().getVlpContext().getVlpParameters(entityClassId);

		if (vlpParameters) {
			let parentParametersForAttribute = vlpParameters.getParametersForAttribute(attributeId);
			if (result) {
				if (parentParametersForAttribute) {
					result = result.merge(parentParametersForAttribute);
				}
			} else {
				result = parentParametersForAttribute;
			}
		}

		return result;
	}

	private getOwnVlpParameters(attributeId: string) {
		let entityClassId = this.getEntityClassId();
		let vlpParameters = this.vlpContext.getVlpParameters(entityClassId);

		let result;

		if (vlpParameters) {
			result = vlpParameters.getParametersForAttribute(attributeId);
		}

		return result;
	}

	/**
	 * Serializes this EO (recursively including dependents) for Update/Insert-Requests.
	 */
	serialize(): EntityObjectData {
		let data = _.cloneDeep(this.getData());

		data.subBos = {
			insert: {},
			update: {},
			delete: {}
		};

		this.dependents.forEach((value, key) => {
			let subEos = value.current();
			if (subEos) {
				subEos.forEach(subEO => {
					let flag = <string>subEO.getData()._flag;
					if (!flag) {
						return;
					}
					if (!data.subBos[flag][key]) {
						data.subBos[flag][key] = [];
					}

					if (flag === 'delete') {
						data.subBos[flag][key].push(subEO.getId());
					} else {
						data.subBos[flag][key].push(subEO.serialize());
					}
				});
			}
		});

		return data;
	}

	checkLayoutChange() {
		this.getLayoutURLDynamically().subscribe(url => {
			this.layoutSubject.next(url);
		});
	}

	/**
	 * @param layoutURL
	 */
	private notifyLayoutChanged(layoutURL: string) {
		Logger.instance.debug('Layout changed on %o to %o', this, layoutURL);
		for (let listener of this.listeners) {
			if (listener.afterLayoutChange) {
				listener.afterLayoutChange(this, layoutURL);
			}
		}
	}

	private notifyValidationChanged() {
		Logger.instance.debug('Validation changed on %o', this);
		for (let listener of this.listeners) {
			if (listener.afterValidationChange) {
				listener.afterValidationChange(this);
			}
		}
	}

	getCommands(): Command<any>[] | undefined {
		let commands = this.getData().commands;
		return commands && commands.list;
	}

	private updateValidationStatus() {
		if (this.validatingAttributes) {
			return;
		}

		Logger.instance.debug('Updating validation status...');
		this.validatingAttributes = true;

		this.getMeta().subscribe(
			meta => {
				// Using setTimeout here because another Angular change detection run must be triggered
				// after the validation status is updated.
				setTimeout(() => {
					let attributes = meta.getAttributes();
					attributes.forEach(attributeMeta => {
						this.updateValidationStatusForAttributeMeta(attributeMeta);
					});
					this.notifyValidationChanged();
					this.validatingAttributes = false;
				});
			},
			() => this.validatingAttributes = false
		);
	}

	private updateValidationStatusForAttributeName(attributeName: string) {
		this.getAttributeMeta(attributeName).subscribe(meta => {
			this.updateValidationStatusForAttributeMeta(meta);
		});
	}

	private updateValidationStatusForAttributeMeta(attributeMeta: EntityAttrMeta) {
		if (attributeMeta === undefined) {
			Logger.instance.warn('Could not update validation status for undefined meta data');
			return;
		}

		let attributeName = attributeMeta.getAttributeName();
		if (attributeName === undefined) {
			Logger.instance.warn('Attribute has no name: %o', attributeMeta);
		} else {
			let value = this.getAttribute(attributeName);
			let status = this.getService().getValidationStatus(attributeMeta, value);
			this.attributeValidation.set(attributeName, status);
		}
	}

	setAttributeValidationError(validationError: ValidationError) {
		let attributeName = FqnService.getShortAttributeName('', validationError.field);

		let status;
		if (validationError.errortype === 'MANDATORY_FIELD_ERROR') {
			status = ValidationStatus.MISSING;
		} else {
			status = ValidationStatus.INVALID;
		}

		this.attributeValidation.set(attributeName, status);
	}

	getValidationStatus(attributeId: string): ValidationStatus | undefined {
		let attributeName = FqnService.getShortAttributeName('', attributeId);
		let status = this.attributeValidation.get(attributeName)
			|| this.attributeValidation.get(attributeId);

		// TODO: Check if the given attribute really belongs to this EO, or throw an exception
		if (status === undefined && !this.isInternalAttribute(attributeName)) {
			// Logger.instance.debug('Validation status undefined for attribute %o', attributeId);
			this.attributeValidation[attributeName] = ValidationStatus.VALIDATING;
			this.updateValidationStatus();
		}

		return status;
	}

	private isInternalAttribute(attributeName: string) {
		return attributeName.startsWith('nuclos');
	}

	deleted() {
		this._setDirty(false);
	}

	getOwner(): any {
		return this.getAttribute('nuclosOwner');
	}

	isLocked(): boolean {
		let owner = this.getOwner();
		return !!(owner && owner.id);
	}

	setMandator(mandator: MandatorData) {
		this.setAttribute('nuclosMandator', {
			id: mandator.mandatorId,
			name: mandator.name,
		});
	}

	/**
	 * @deprecated client should evaluate title from titlePattern
	 */
	getTitle() {
		return this.getData().title;
	}

	/**
	 * @deprecated client should evaluate info from infoPattern
	 */
	getInfo() {
		return this.getData().info;
	}

	getRootEo(): EntityObject {
		return this;
	}
}

/**
 * Represents an EO which is used in a subform and has a
 * reference attribute to the main (selected) EO.
 */
export class SubEntityObject extends EntityObject {

	private selected = false;

	private complete = false;

	constructor(
		private parentEo: EntityObject,
		private referenceAttributeId: string,
		eoData: EntityObjectData
	) {
		super(eoData);
	}

	/**
	 * Returns a fake EntityObject which works on this SubEntityObject,
	 * but uses (almost) all methods of the EntityObject class.
	 *
	 * This is necessary e.g. when a SubEntityObject is opened in a detail modal where
	 * it must behave like a normal EntityObject.
	 *
	 * @returns {any}
	 */
	toEntityObject(): EntityObject {
		let fakeEo: any = {};

		this.clearLovEntries();

		let methodsToKeep = [
			'setDirty',
			'_setDirty',
			'getAttributeRestriction',
			// 'getVlpParameters',
			// 'getRootEo'
		];

		/* tslint:disable:forin */
		for (let x in <any>this) {
			let method = this[x];
			if (typeof method === 'function') {
				let replaceMethod = EntityObject.prototype[x];
				if (methodsToKeep.indexOf(x) >= 0) {
					replaceMethod = method;
				}
				fakeEo[x] = (...args) => {
					let result = replaceMethod.apply(fakeEo, args);
					return result;
				};
			} else {
				// Getter/Setter for properties
				Object.defineProperty(fakeEo, x, {
					get: () => {
						// console.log('get %o => %o', x, this[x]);
						return this[x];
					},
					set: value => {
						// console.log('set %o => %o', x, value);
						this[x] = value;
					}
				});
			}
		}

		return fakeEo;
	}

	getParent(): EntityObject {
		return this.parentEo;
	}

	getReferenceAttributeId() {
		return this.referenceAttributeId;
	}

	/**
	 * Clones this Sub-EO.
	 * See {@link EntityObject#clone()}
	 *
	 * @returns {SubEntityObject}
	 */
	clone(): SubEntityObject {
		let result = new SubEntityObject(
			this.getParent(),
			this.referenceAttributeId,
			this.cloneData()
		);

		result.clearAutonumbers();

		return result;
	}

	setSelected(value: boolean) {
		this.selected = value;
	}

	isSelected() {
		return this.selected;
	}

	setComplete(value: boolean) {
		this.complete = value;
	}

	isComplete() {
		return this.complete;
	}

	init(
		entityClassId: string,
		eoId: number | undefined,
		valuesMap: any	// TODO: Use a real Map.
	) {

		this.getData().boMetaId = entityClassId;
		this.getData().boId = eoId;
		this.getData().attributes = {};

		for (let field in valuesMap) {
			if (field !== undefined) {
				this.setAttributeUnsafe(field, valuesMap[field]);
			}
		}

		if (this.getData().boId === undefined) {
			this.getData()._flag = 'insert';
		}
	}

	/**
	 * @param dirty
	 * @returns {EntityObject}
	 * @deprecated USE ONLY IF ABSOLUTELY NECESSARY!
	 */
	protected _setDirty(dirty: boolean): EntityObject {
		if (dirty && this.parentEo) {
			this.parentEo.setDirty(true);
		}

		return super._setDirty(dirty);
	}


	protected getAttributeRestriction(attributeName: string) {
		let restriction = super.getAttributeRestriction(attributeName);
		if (restriction) {
			return restriction;
		}

		let referenceAttributeName = FqnService.getShortAttributeName('', this.referenceAttributeId);
		if (attributeName === referenceAttributeName) {
			return 'readonly';
		}

		return undefined;
	}

	getRootEo(): EntityObject {
		return this.parentEo.getRootEo();
	}

	protected setAttributeValidated(attributeName: string, value: any): boolean {
		let changed = super.setAttributeValidated(attributeName, value);

		this.getAttributeMeta(attributeName).subscribe(attributeMeta => {
			if (attributeMeta && attributeMeta.isAutonumber()) {
				let dependents = this.getParent().getDependents(
					this.getReferenceAttributeId()
				);
				if (dependents) {
					AutonumberService.instance.updateAutonumbersForAttribute(dependents, attributeMeta);
				}
			}
		});

		return changed;
	}

	markAsDeleted(): void {
		super.markAsDeleted();

		let dependents = this.getParent().getDependents(
			this.getReferenceAttributeId()
		);
		if (dependents) {
			AutonumberService.instance.updateAutonumbers(dependents);
		}
	}

	private clearAutonumbers() {
		this.getMeta().subscribe(meta => {
			meta.getAttributes().forEach(attributeMeta => {
				if (attributeMeta.isAutonumber()) {
					this.setAttributeUnsafe(attributeMeta.getAttributeID(), undefined);
				}
			});
		});
	}

	getDetailLink() {
		let links = this.getLinks();
		return links && links.detail;
	}
}
