import { Inject, Injectable } from '@angular/core';
import { EntityObject } from '../shared/entity-object.class';
import { EntityObjectEventService } from '../shared/entity-object-event.service';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class WindowTitleService {

	constructor(
		@Inject(DOCUMENT) private document,
		private eoEventService: EntityObjectEventService
	) {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => {
				if (eo) {
					this.setWindowTitleFromEo(eo);
				} else {
					this.setDefaultWindowTitle();
				}
			}
		);
	}

	/**
	 * TODO: The EO title is not always available:
	 * If the EO is initially selected from the result list, it must be reloaded first.
	 * Only then will the title be available.
	 */
	private setWindowTitleFromEo(eo: EntityObject) {
		if (eo.getTitle()) {
			this.document.title = eo.getTitle();
		}
	}

	private setDefaultWindowTitle() {
		this.document.title = 'Nuclos';
	}
}
