import { EntityObject, SubEntityObject } from './entity-object.class';
import { EntityAttrMeta, EntityMeta } from './bo-view.model';

export interface LovSearchConfig {
	eo: EntityObject;
	entityMeta?: EntityMeta,
	attributeMeta: EntityAttrMeta;

	resultLimit: number;

	vlp?: WebValuelistProvider;
	mandatorId?: string;
	quickSearchInput?: string;
}
