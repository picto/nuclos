import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Logger } from '../../log/shared/logger';
import { ObservableUtils } from '../../shared/observable-utils';
import { LoadMoreResultsEvent } from '../entity-object.component';
import { DataService } from './data.service';
import { EntityObjectPreferenceService } from './entity-object-preference.service';
import { EntityObjectResultService } from './entity-object-result.service';
import { Subject } from 'rxjs';

@Injectable()
export class EntityObjectResultUpdateService {

	private loading = false;

	/**
	 * Triggered when the EO list should be reloaded.
	 *
	 * @type {Subject<any>}
	 */
	private mustLoadListSubject = new Subject<any>();

	constructor(
		private eoResultService: EntityObjectResultService,
		private eoPreferenceService: EntityObjectPreferenceService,
		private dataService: DataService,
		private authenticationService: AuthenticationService,
		private $log: Logger,
	) {
		this.observeMustLoadList().debounceTime(500).subscribe(
			() => {
				if (this.authenticationService.isLoggedIn()) {
					this.loadInitialResultsAndSelectFirstRecord().subscribe();
				}
			}
		);

		this.authenticationService.observeLoginStatus().subscribe(
			loggedIn => {
				if (!loggedIn) {
					this.eoResultService.clear();
				}
			}
		);

		this.eoPreferenceService.selectedSearchtemplatePref$.distinctUntilChanged().subscribe(
			() => this.triggerLoadList()
		);
	}

	observeMustLoadList(): Observable<any> {
		return this.mustLoadListSubject;
	}

	triggerLoadList() {
		this.$log.warn('Trigger loading of eo list');

		let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
		if (selectedEntityClassId) {
			let entityMeta = this.eoResultService.getSelectedMeta();
			if (entityMeta && entityMeta.getBoMetaId() === selectedEntityClassId) {
				this.mustLoadListSubject.next();
			} else {
				this.eoResultService.updateSelectedMeta().subscribe(
					() => this.mustLoadListSubject.next()
				);
			}
		} else {
			this.$log.warn('Cannot load result list - no entity class selected');
		}
	}

	private loadInitialResultsAndSelectFirstRecord(): Observable<any> {
		return this.loadInitialResults()
			.do(
				() => {
					if (!this.eoResultService.getSelectedEo()) {
						this.eoResultService.selectFirstResult();
					}
				}
			);
	}

	loadInitialResults(): Observable<any> {
		return this.loadResultsForSelectedEntityClass(
			0,
			40,
			true
		).do(
			(boViewModel) => {
				this.eoResultService.setNewData(boViewModel);
			}
		);
	}

	/**
	 * load more data when scrolling down in sidebar
	 */
	loadMoreResults(event: LoadMoreResultsEvent): Observable<any> {
		let condition = () => {
			let totalEoCount = this.eoResultService.getTotalResultCount();
			return totalEoCount === undefined || event.loadedItems < totalEoCount;
		};

		let observable = this.loadResultsForSelectedEntityClass(
			event.loadedItems,
			40,
			false
		).do(
			(boViewModel) => {
				this.eoResultService.addNewData(boViewModel);
			}
		);

		return Observable.if(
			condition,
			observable
		);
	}

	isLoading() {
		return this.loading;
	}

	setLoading(loading) {
		this.loading = loading;
	}

	private loadResultsForSelectedEntityClass(
		offset: number,
		limit: number,
		getTotalCount: boolean
	): Observable<any> {

// TODO EntityObjectComponent.loadEoDataToFillSidebar() will not work correct
// TODO SidebarColumnTest._05dynamicLoadTest didn't fail because of small window size
// 		if (this.isLoading()) {
// 			this.$log.warn('Already loading results');
// 			return Observable.empty();
// 		}

		const meta = this.eoResultService.getSelectedMeta();
		if (!meta) {
			return Observable.empty();
		}

		let observable = this.dataService.loadList(
			meta,
			this.eoPreferenceService.getSelectedTablePreference(),
			this.eoPreferenceService.getSelectedSearchtemplatePreference(),
			offset,
			limit,
			getTotalCount
		).do(boViewModel => boViewModel.boMetaId = meta.getBoMetaId());

		return ObservableUtils.onSubscribe(
			observable,
			() => this.setLoading(true)
		).finally(
			() => this.setLoading(false)
		);
	}
}
