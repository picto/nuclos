import { FqnService } from '../../shared/fqn.service';
import { VlpParameter } from './vlp-parameter';
import { VlpParametersForAttribute } from './vlp-parameters-for-attribute';

export class VlpParameters {
	private parametersByAttribute = new Map<string, VlpParametersForAttribute>();

	setParameter(
		attributeId: string,
		parameterName: string,
		parameterValue: any
	) {
		let attributeName = FqnService.getShortAttributeName('', attributeId);

		let parameters = this.parametersByAttribute.get(attributeName);
		if (!parameters) {
			parameters = new VlpParametersForAttribute();
			this.parametersByAttribute.set(attributeName, parameters);
		}

		parameters.set(new VlpParameter(parameterName, parameterValue));
	}

	getParametersForAttribute(
		attributeId: string
	) {
		let attributeName = FqnService.getShortAttributeName('', attributeId);
		return this.parametersByAttribute.get(attributeName);
	}
}
