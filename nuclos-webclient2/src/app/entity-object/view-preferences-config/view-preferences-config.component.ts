import {
	Component,
	Directive,
	ElementRef,
	Inject,
	Injectable,
	Input, OnDestroy,
	OnInit,
	TemplateRef,
	ViewChild
} from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Logger } from '../../log/shared/logger';
import {
	AttributeSelectionContent,
	ColumnAttribute,
	IPreferenceFilter,
	Preference,
	PreferenceType,
	SearchtemplatePreferenceContent,
	SelectedAttribute,
	SidebarLayoutType,
	SideviewmenuPreferenceContent
} from '../../preferences/preferences.model';
import { PreferencesService } from '../../preferences/preferences.service';
import { FqnService } from '../../shared/fqn.service';
import { EntityMeta } from '../shared/bo-view.model';
import { SelectableService } from '../shared/selectable.service';
import { SideviewmenuService } from '../shared/sideviewmenu.service';
import { EntityObjectResultService } from '../shared/entity-object-result.service';
import { SearchtemplateService } from '../../searchtemplate/searchtemplate.service';
import { Selectable } from '../../two-side-multi-select/two-side-multi-select.model';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Action } from '../../authentication/action.enum';
import { Subscription } from 'rxjs/Subscription';


@Injectable()
export class PreferenceDialogState {

	/**
	 * last opened view preference modalRef
	 */
	modalRef: NgbModalRef;

	/**
	 * ref containing the modal component
	 */
	templateRef: TemplateRef<any>;

	meta: EntityMeta;
}

@Component({
	selector: 'nuc-view-preferences-modal',
	templateUrl: './view-preferences-modal.component.html',
	styleUrls: ['./view-preferences-modal.component.css']
})
export class ViewPreferencesModalComponent implements OnInit, OnDestroy {

	/*
	 injected via ViewPreferencesModalContainerComponent
	 */
	meta: EntityMeta;

	/* list of attribute fqns */
	hiddenColumns: string[];

	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined> = new BehaviorSubject(undefined);
	selectedSearchtemplatePref$: BehaviorSubject<Preference<SearchtemplatePreferenceContent> | undefined> = new BehaviorSubject(undefined);

	backupSelectedSideviewmenuPref: Preference<SideviewmenuPreferenceContent>;
	backupSelectedSearchtemplatePref: Preference<SearchtemplatePreferenceContent>;

	showResetSortingButton = false;
	editSideviewmenu;
	sideviewmenuColumns: Selectable[] = [];
	selectedSideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined;
	sideviewmenuPreferences: Preference<SideviewmenuPreferenceContent>[] = [];

	editSearchtemplate;
	searchtemplateColumns: Selectable[] = [];
	selectedSearchtemplatePreference: Preference<SearchtemplatePreferenceContent>;
	searchtemplatePreferences: Preference<SearchtemplatePreferenceContent>[] = [];


	viewtype: SidebarLayoutType | undefined;

	alowSidebarCustomization: boolean;

	layoutIdForSubformPref: string | undefined;

	@ViewChild('sideviewmenuSelector') sideviewmenuSelector: ElementRef;

	private sideviewMenuPrefSubscription: Subscription;

	constructor(
		@Inject(DOCUMENT) private document,
		private state: PreferenceDialogState,
		private eoResultService: EntityObjectResultService,
		private preferencesService: PreferencesService,
		private sideviewmenuService: SideviewmenuService,
		private selectableService: SelectableService,
		private fqnService: FqnService,
		private searchtemplateService: SearchtemplateService,
		private authenticationService: AuthenticationService,
		private i18n: NuclosI18nService
	) {
	}

	ngOnInit() {

		if (this.selectedSideviewmenuPref$.getValue()) {
			this.backupSelectedSideviewmenuPref = JSON.parse(JSON.stringify(this.selectedSideviewmenuPref$.getValue()));
		}
		if (this.selectedSearchtemplatePref$.getValue()) {
			this.backupSelectedSearchtemplatePref = JSON.parse(JSON.stringify(this.selectedSearchtemplatePref$.getValue()));
		}

		this.sideviewMenuPrefSubscription = this.selectedSideviewmenuPref$.subscribe(pref => {
			if (pref) {
				this.selectedSideviewmenuPreference = pref;

				this.sideviewmenuColumnsChanged(false);

				// add attributes which are not already in pref
				this.sideviewmenuPreferences.forEach(p => {
					this.addAvailableColumnsFromMeta(p, this.meta);
					this.removeHiddenColumns(p);
				});
				if (this.selectedSideviewmenuPreference) {
					this.addAvailableColumnsFromMeta(this.selectedSideviewmenuPreference, this.meta);
					this.removeHiddenColumns(this.selectedSideviewmenuPreference);

					// add meta data to columns
					this.sideviewmenuPreferences.forEach(p => this.selectableService.addMetaDataToColumns(p, this.meta));

					this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns as Selectable[];
				}
				this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns as Selectable[];
			}
		});

		// load preferences for sideview menu and searchtemplate
		let filter: IPreferenceFilter = {
			boMetaId: this.meta.getBoMetaId(),
			type: [
				this.meta.isSubform() ? PreferenceType.subformTable.name : PreferenceType.table.name,
				PreferenceType.searchtemplate.name],
		};

		let selectedEO = this.eoResultService.getSelectedEo();
		this.layoutIdForSubformPref = (selectedEO && this.meta.isSubform()) ? selectedEO.getLayoutId() : undefined;
		if (this.layoutIdForSubformPref) {
			filter.layoutId = this.layoutIdForSubformPref;
			filter.orLayoutIsNull = true;
		}

		this.preferencesService.getPreferences(filter).subscribe(
			preferences => {

				/*
				 sidebar preferences
				 */

				// set table prefs
				this.sideviewmenuPreferences = preferences
					.filter(
						p => p.type === PreferenceType.table.name
						|| p.type === PreferenceType.subformTable.name) as Preference<SideviewmenuPreferenceContent>[];

				// in preferences only selected columns are stored (without selected flag)
				this.sideviewmenuPreferences.forEach(
					pref => pref.content.columns.forEach(
						column => column.selected = true
					)
				);


				this.selectedSideviewmenuPreference = this.selectedSideviewmenuPref$.getValue();
				if (this.sideviewmenuPreferences.length === 0) {
					if (this.selectedSideviewmenuPreference && this.selectedSideviewmenuPreference.prefId === undefined) {
						// only one temporary pref exists - add it to option list
						this.sideviewmenuPreferences.push(this.selectedSideviewmenuPreference);
					}
				} else {
					// selected pref
					this.selectedSideviewmenuPreference
						= this.sideviewmenuPreferences
						.filter(p => p.selected)
						.shift();

					// if no pref is selected select the first pref in list
					if (!this.selectedSideviewmenuPreference && this.sideviewmenuPreferences.length > 0) {
						this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
					}
				}

				// add attributes which are not already in pref
				this.sideviewmenuPreferences.forEach(pref => this.addAvailableColumnsFromMeta(pref, this.meta));

				// add meta data to columns
				this.sideviewmenuPreferences.forEach(pref => this.selectableService.addMetaDataToColumns(pref, this.meta));


				this.selectSideviewmenu(this.selectedSideviewmenuPreference);

				/*
				 searchtemplate preferences
				 */

				this.searchtemplatePreferences = preferences
					.filter(
						p => p.type === PreferenceType.searchtemplate.name) as Preference<SearchtemplatePreferenceContent>[];

				// selected pref
				let selectedSearchtemplatePreference = this.searchtemplatePreferences.filter(
					p => p.selected).shift();

				// if no pref is selected select the first pref in list
				if (!selectedSearchtemplatePreference && this.searchtemplatePreferences.length > 0) {
					selectedSearchtemplatePreference = this.searchtemplatePreferences[0];
				}
				// create a new pref if none is available for this bo
				if (this.searchtemplatePreferences.length === 0) {
					// this.newSearchtemplatePref();
					selectedSearchtemplatePreference = this.searchtemplateService.emptySearchtemplatePreference(this.meta);
					if (selectedSearchtemplatePreference) {
						this.searchtemplatePreferences.push(selectedSearchtemplatePreference);
					}
				}

				// add meta data to columns
				this.searchtemplatePreferences.forEach(pref => {
					this.selectableService.addMetaDataToColumns(pref, this.meta);
				});

				if (selectedSearchtemplatePreference) {
					this.selectSearchtemplate(selectedSearchtemplatePreference);
				}

				// add attributes which are not already in pref
				this.searchtemplatePreferences.forEach(pref => this.addAvailableColumnsFromMeta(pref, this.meta));

				this.viewtype = this.sideviewmenuService.getViewType().getValue();

			}
		);

		this.checkAlowCustomization();
	}

	ngOnDestroy(): void {
		if (!!this.sideviewMenuPrefSubscription) {
			this.sideviewMenuPrefSubscription.unsubscribe();
		}
	}

	setViewtype(viewtype: SidebarLayoutType): void {
		this.viewtype = viewtype;
		this.sideviewmenuService.setViewType(viewtype);
	}

	showViewTypeConfig(): boolean {
		// TODO not implemented for subforms
		return !this.meta.isSubform();
	}

	showSearchfilterConfig(): boolean {
		// TODO not implemented for subforms
		return !this.meta.isSubform();
	}

	private removeHiddenColumns(pref: Preference<AttributeSelectionContent>) {
		if (this.hiddenColumns) {
			pref.content.columns = pref.content.columns.filter(col => this.hiddenColumns.indexOf(col.boAttrId) === -1);
		}
	}

	private addAvailableColumnsFromMeta(pref: Preference<AttributeSelectionContent>, metaData: EntityMeta): void {
		this.meta.getAttributes().forEach((_attributeMeta, attrKey) => {

			let entityAttrMeta = this.meta.getAttributeMeta(attrKey);
			if (entityAttrMeta) {
				let attributeKey = this.fqnService.getShortAttributeName(metaData.getBoMetaId(), entityAttrMeta.getAttributeID());
				let attribute = <SelectedAttribute> this.meta.getAttribute(attributeKey);
				if (attribute) {
					if (pref.content.columns && pref.content.columns.filter(
							column => column.boAttrId === attribute.boAttrId).length === 0) {

						// make a copy of the attribute object
						let attr: ColumnAttribute = Object.assign({}, attribute);

						if (attr && attr.boAttrId && metaData) {
						let fieldName = this.fqnService.getShortAttributeName(metaData.getBoMetaId(), attr.boAttrId);
							let attributeMeta = metaData.getAttributeMeta(fieldName);
							attr.sort = {
								enabled: attributeMeta && !attributeMeta.isCalculated()
							}
						}
						pref.content.columns.push(attr);
					}
				}
			}
		});
	}

	sideviewmenuColumnsChanged(dirty: boolean): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.dirty = dirty;
			// update position columns
			this.selectedSideviewmenuPreference.content.columns.forEach((column, index) => column.position = index);
		}
		this.selectableService.updatePreferenceName(this.selectedSideviewmenuPreference);
	}

	searchtemplateColumnsChanged(dirty: boolean): void {
		if (this.selectedSearchtemplatePreference) {
			this.selectedSearchtemplatePreference.dirty = dirty;
		}
		this.selectableService.updatePreferenceName(this.selectedSearchtemplatePreference);
	}


	close(): void {
		Logger.instance.debug('close view prefs - selected searchtemplate = %o', this.selectedSearchtemplatePreference);

		// reset to initial state
		if (this.backupSelectedSideviewmenuPref) {
			this.selectedSideviewmenuPref$.next(this.backupSelectedSideviewmenuPref);
		}
		if (this.backupSelectedSearchtemplatePref) {
			this.selectedSearchtemplatePref$.next(this.backupSelectedSearchtemplatePref);
		}

		this.state.modalRef.close('confirmed');
	}

	save(): void {

		if (this.selectedSideviewmenuPreference && this.selectedSideviewmenuPreference.content.columns.filter(c => c.selected).length === 0) {
			alert(this.i18n.getI18n('webclient.preferences.dialog.columnsRequired'));
			return;
		}

		let saveSideviewmenuObservable: Observable<any> = Observable.of({});
		let saveSearchtemplateObservable: Observable<any> = Observable.of({});

		if (this.selectedSideviewmenuPreference || this.selectedSearchtemplatePreference) {
			if (this.selectedSideviewmenuPreference) {
				this.selectedSideviewmenuPreference.selected = true;

				if (this.selectedSideviewmenuPreference.prefId || this.selectedSideviewmenuPreference.dirty) {
					let selectedEO = this.eoResultService.getSelectedEo();
					if (selectedEO) {
						this.selectedSideviewmenuPreference.layoutId = this.layoutIdForSubformPref;
					}
					saveSideviewmenuObservable = this.selectableService.savePreference(this.selectedSideviewmenuPreference);
				}
			}
			if (this.selectedSearchtemplatePreference) {
				this.selectedSearchtemplatePreference.selected = true;
				if (this.selectedSearchtemplatePreference.prefId || this.selectedSearchtemplatePreference.dirty) {
					saveSearchtemplateObservable = this.selectableService.savePreference(this.selectedSearchtemplatePreference);
				}
			}
		}
		Observable.merge(saveSideviewmenuObservable, saveSearchtemplateObservable).subscribe(
			() => {
				this.sideviewmenuService.sideviewmenuPrefChanged();
				if (this.selectedSideviewmenuPreference) {
					this.selectedSideviewmenuPref$.next(this.selectedSideviewmenuPreference);
				}
				if (this.selectedSearchtemplatePreference) {
					Logger.instance.warn('selectedSearchtemplatePreference: %o', this.selectedSearchtemplatePreference);
					this.selectedSearchtemplatePref$.next(this.selectedSearchtemplatePreference);
				} else {
					Logger.instance.warn('no searchtemplate pref selected');
				}
				this.state.modalRef.close('confirmed');
			}
		);

	}

	selectSideviewmenu(
		sideviewmenuPreference: Preference<SideviewmenuPreferenceContent> | undefined
	): void {
		if (!sideviewmenuPreference) {
			return;
		}

		this.showResetSortingButton = sideviewmenuPreference.content.columns
				.filter(column => column.sort && column.sort.prio)
				.length > 0;


		this.selectedSideviewmenuPreference = sideviewmenuPreference;

		if (this.selectedSideviewmenuPreference.prefId) {
			this.preferencesService.selectPreference(this.selectedSideviewmenuPreference, this.layoutIdForSubformPref).subscribe();
		}

		this.selectedSideviewmenuPref$.next(this.selectedSideviewmenuPreference);

		this.sideviewmenuColumns = sideviewmenuPreference.content.columns as Selectable[];
	}


	checkAlowCustomization(): void {
		this.authenticationService.onAuthenticationDataAvailable().subscribe(loginSuccessful => {
			this.alowSidebarCustomization = loginSuccessful
				&& this.authenticationService.isActionAllowed(Action.WorkspaceCustomizeEntityAndSubFormColumn);
		});
	}

	editSideviewmenuPref(): void {
		window.setTimeout(() => {
			this.editSideviewmenu = true;
			window.setTimeout(() => {
				// focus/select text input
				this.document.getElementById('sideviewmenu-name-input').select();
			});
		});
	}

	editSideviewmenuPrefDone(): void {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.layoutId = this.layoutIdForSubformPref;
			this.selectableService.savePreference(this.selectedSideviewmenuPreference).subscribe(
				() => {
					this.editSideviewmenu = false;
				}
			);
		}
	}

	showDeleteSideviewmenuPref(): boolean {
		return this.selectedSideviewmenuPreference !== undefined && !this.editSideviewmenu && !this.selectedSideviewmenuPreference.shared &&
			(
				this.sideviewmenuPreferences.length > 1
				||
				this.sideviewmenuPreferences.length === 1 && this.sideviewmenuPreferences[0].prefId !== undefined
			);
	}

	/**
	 * delete a not shared pref
	 */
	deleteSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.deletePref(this.selectedSideviewmenuPreference, this.sideviewmenuPreferences).subscribe(
				() => {
					delete this.selectedSideviewmenuPreference;
					if (this.sideviewmenuPreferences.length > 0) {
						this.selectedSideviewmenuPreference = this.sideviewmenuPreferences[0];
					}
					if (this.sideviewmenuPreferences.length === 0) {
						this.newSideviewmenuPref(false, true);
					}
					if (this.selectedSideviewmenuPreference) {
						this.sideviewmenuColumns = this.selectedSideviewmenuPreference.content.columns as Selectable[];
					}
				}
			);
		}
	}

	/**
	 * discard user changes of sideviewmenu pref
	 * deletes customized pref
	 * reselect shared pref
	 */
	discardSideviewmenuPref() {
		if (this.selectedSideviewmenuPreference) {
			this.preferencesService.deleteCustomizedPreferenceItem(this.selectedSideviewmenuPreference).subscribe(() => {
				if (this.selectedSideviewmenuPreference && this.selectedSideviewmenuPreference.prefId) {
					this.preferencesService.getPreference(this.selectedSideviewmenuPreference.prefId).subscribe(
						(pref: Preference<SideviewmenuPreferenceContent>) => {
							if (pref) {

								let prefInList = this.sideviewmenuPreferences.filter(p => p.prefId === pref.prefId).shift();
								if (prefInList) {
									prefInList.content = pref.content;
									prefInList.customized = pref.customized;
								}
								this.selectedSideviewmenuPref$.next(pref);

								// TODO select HTML option - this should not be necessary
								if (this.selectedSideviewmenuPreference) {
									let selectedPrefId = this.selectedSideviewmenuPreference.prefId;
									if (selectedPrefId) {
										let index = this.sideviewmenuPreferences.findIndex(p => p.prefId === selectedPrefId) + 1;
										setTimeout(() => {
											this.sideviewmenuSelector.nativeElement.selectedIndex = index;
										});
									}
								}
							}
						});
				}
			});
		}
	}

	resetSorting() {
		if (this.selectedSideviewmenuPreference) {
			this.selectedSideviewmenuPreference.content.columns.forEach(col => {
				delete col.sort;
			});
		}
		this.showResetSortingButton = false;
	}

	newSideviewmenuPref(editMode = true, selectDefaultColumns = false) {

		this.editSideviewmenu = editMode;

		let sideviewmenuPref;
		let selectedEo = this.eoResultService.getSelectedEo();
		if (this.meta.isSubform()) {
			if (selectedEo) {
				sideviewmenuPref = this.sideviewmenuService.emptySubformPreference(this.meta, selectedEo.getEntityClassId(), selectDefaultColumns);
			}
		} else {
			sideviewmenuPref = this.sideviewmenuService.emptySideviewmenuPreference(this.meta, selectDefaultColumns);
		}
		if (sideviewmenuPref) {
			this.sideviewmenuPreferences.push(sideviewmenuPref);
		}
		this.selectSideviewmenu(sideviewmenuPref);

		if (editMode) {
			window.setTimeout(() => {
				// focus text input
				this.document.getElementById('sideviewmenu-name-input').focus();
			});
		}
	}


	selectSearchtemplate(searchtemplatePreference: Preference<SearchtemplatePreferenceContent> | undefined): void {
		if (!searchtemplatePreference) {
			return;
		}

		this.selectedSearchtemplatePreference = searchtemplatePreference;

		if (this.selectedSearchtemplatePreference.prefId) {
			this.preferencesService.selectPreference(this.selectedSearchtemplatePreference, this.layoutIdForSubformPref).subscribe();
		}

		this.selectedSearchtemplatePref$.next(this.selectedSearchtemplatePreference);

		this.searchtemplateColumns = searchtemplatePreference.content.columns as Selectable[];
	}

	newSearchtemplatePref() {

		this.editSearchtemplate = true;

		let searchtemplatePref = this.searchtemplateService.emptySearchtemplatePreference(this.meta);
		searchtemplatePref.selected = true;

		this.searchtemplatePreferences.push(
			searchtemplatePref
		);

		// this.selectedSearchtemplatePreference = searchtemplatePref;

		this.selectSearchtemplate(searchtemplatePref);

		this.focusSearchtemplateNameInput();
	}

	editSearchtemplatePref() {
		this.editSearchtemplate = true;
		this.focusSearchtemplateNameInput();
	}

	editSearchtemplatePrefDone() {
		this.selectableService.savePreference(this.selectedSearchtemplatePreference).subscribe(
			() => {
				this.editSearchtemplate = false;
			}
		);
	}

	showDeleteSearchtemplatePref(): boolean {
		return this.selectedSearchtemplatePreference !== undefined && !this.editSearchtemplate && !this.selectedSearchtemplatePreference.shared &&
			(
				this.searchtemplatePreferences.length > 1
				||
				this.searchtemplatePreferences.length === 1 && this.searchtemplatePreferences[0].prefId !== undefined
			);
	}

	deleteSearchtemplatePref(): void {
		if (this.selectedSearchtemplatePreference) {
			this.deletePref(this.selectedSearchtemplatePreference, this.searchtemplatePreferences).subscribe(
				() => {
					delete this.selectedSearchtemplatePreference;
					if (this.searchtemplatePreferences.length > 0) {
						this.selectedSearchtemplatePreference = this.searchtemplatePreferences[0];
					}
					if (this.searchtemplatePreferences.length === 0) {
						this.selectedSearchtemplatePreference = this.searchtemplateService.emptySearchtemplatePreference(this.meta);
						this.searchtemplatePreferences.push(this.selectedSearchtemplatePreference);
					}
					if (this.selectedSearchtemplatePreference) {
						this.searchtemplateColumns = this.selectedSearchtemplatePreference.content.columns as Selectable[];
					}
				}
			);
		}
	}


	private deletePref(
		prefItem: Preference<AttributeSelectionContent>,
		prefList: Preference<AttributeSelectionContent>[]
	): Observable<void> {
		return new Observable<void>(
			observer => {
				let removeFromModel = () => {
					prefList.splice(prefList.indexOf(prefItem), 1);
					if (prefList.length > 0) {
						prefItem = prefList[0];
					}
				};
				if (prefItem.prefId) {
					this.preferencesService.deletePreferenceItem(prefItem).subscribe(
						() => {
							if (prefItem) {
								removeFromModel();
							}
							observer.next();
							observer.complete();
						}
					);
				} else {
					removeFromModel();
					observer.next();
					observer.complete();
				}
			}
		);
	}

	private focusSearchtemplateNameInput() {
		window.setTimeout(() => {
			// focus text input
			this.document.getElementById('searchtemplate-name-input').focus();
		});
	}

	handleNameChange(preferenceItem: Preference<AttributeSelectionContent>) {
		preferenceItem.content.userdefinedName =
			preferenceItem.name !== undefined && preferenceItem.name.length > 0
			&& preferenceItem.name !== this.selectableService.getDefaultName(preferenceItem);
	}

}


@Directive({
	selector: '[nucViewPreferencesModal]'
})
export class ViewPreferencesModalDirective {
	constructor(confirmTemplate: TemplateRef<any>, state: PreferenceDialogState) {
		state.templateRef = confirmTemplate;
	}
}


@Component({
	selector: 'nuc-view-preferences-config',
	styleUrls: ['./view-preferences-modal-container.component.css'],
	template: `
		<ng-template nucViewPreferencesModal>
			<nuc-view-preferences-modal>
			</nuc-view-preferences-modal>
		</ng-template>
		<a (click)="showViewPreferencesModal()"><span class="fa fa-cog view-preferences-icon"></span></a>
	`
})
export class ViewPreferencesModalContainerComponent {

	@Input()
	meta: EntityMeta;

	@Input()
	hiddenColumns: any[];

	@Input()
	selectedSideviewmenuPref$: BehaviorSubject<Preference<SideviewmenuPreferenceContent> | undefined>;

	@Input()
	selectedSearchtemplatePref$: BehaviorSubject<Preference<SearchtemplatePreferenceContent> | undefined>;

	constructor(
		private modalService: NgbModal,
		private state: PreferenceDialogState
	) {
	}

	showViewPreferencesModal(): Promise<any> {
		this.state.modalRef = this.modalService.open(ViewPreferencesModalComponent, {size: 'lg'});
		this.state.modalRef.componentInstance.selectedSideviewmenuPref$ = this.selectedSideviewmenuPref$;
		this.state.modalRef.componentInstance.selectedSearchtemplatePref$ = this.selectedSearchtemplatePref$;
		this.state.modalRef.componentInstance.meta = this.meta;
		this.state.modalRef.componentInstance.hiddenColumns = this.hiddenColumns;
		return this.state.modalRef.result;
	}
}
