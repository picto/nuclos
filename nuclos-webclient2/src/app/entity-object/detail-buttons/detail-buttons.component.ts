import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService, MandatorData } from '../../authentication/authentication.service';
import { DialogService } from '../../dialog/dialog.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import {
	BrowserRefreshService,
	SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM,
	SelectReferenceInOtherWindowEvent,
	SHOW_CLOSE_ON_SAVE_BUTTON_PARAM,
	SHOW_SELECT_BUTTON_PARAM
} from '../../shared/browser-refresh.service';
import { EntityMeta } from '../shared/bo-view.model';
import { EntityObjectEventService } from '../shared/entity-object-event.service';
import { EntityObjectNavigationService } from '../shared/entity-object-navigation.service';
import { EntityObjectResultService } from '../shared/entity-object-result.service';
import { EntityObject } from '../shared/entity-object.class';
import { EntityObjectService } from '../shared/entity-object.service';
import { MetaService } from '../shared/meta.service';

interface DetailButton {
	id: string;
	labelKey: string;

	buttonCssClass: string;
	iconCssClass: string;

	action(eo: EntityObject | undefined): void;
	isVisible(eo: EntityObject | undefined): boolean;
	isEnabled(eo: EntityObject | undefined): boolean;
	blur?(): void;
}

@Component({
	selector: 'nuc-detail-buttons',
	templateUrl: './detail-buttons.component.html',
	styleUrls: ['./detail-buttons.component.css']
})
export class DetailButtonsComponent implements OnInit, OnChanges {
	@Input()
	canCreateBo: boolean;

	eo: EntityObject | undefined;

	private selectEntryInOtherWindowToken: string;

	saveInProgress = false;
	showReferenceSelectionButton = false;
	showCloseOnSaveButton = false;

	// TODO: Move mandator stuff to own component or module.
	mandators: MandatorData[];
	selectedMandator: MandatorData | undefined;
	mandatorLabel: string | undefined;
	showMandatorSelection = false;

	private isNewAllowedForMandator = false;

	@ViewChild('popover') popover;

	buttons: DetailButton[] = [];

	/**
	 * Remember the previous EO when adding a new one.
	 * The previous EO should be selected again, if adding a new one is cancelled.
	 */
	private previousEO: EntityObject | undefined;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private authenticationService: AuthenticationService,
		private eoService: EntityObjectService,
		private eoEventService: EntityObjectEventService,
		private eoResultService: EntityObjectResultService,
		private eoNavigationService: EntityObjectNavigationService,
		private metaService: MetaService,
		private browserRefreshService: BrowserRefreshService,
		private dialogService: DialogService,
		private i18n: NuclosI18nService,
		private $log: Logger,
	) {
	}

	ngOnInit() {
		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.eo = eo
		);

		// show 'select entry in other window' button if requested
		this.route.queryParams.subscribe((params: Params) => {
			let showReferenceSelectionButton = params[SHOW_SELECT_BUTTON_PARAM];
			if (showReferenceSelectionButton) {
				this.showReferenceSelectionButton = true;
			}
			let selectEntryInOtherWindowToken = params[SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM];
			if (selectEntryInOtherWindowToken) {
				this.selectEntryInOtherWindowToken = selectEntryInOtherWindowToken;
			}
			let showCloseOnSaveButton = params[SHOW_CLOSE_ON_SAVE_BUTTON_PARAM];
			if (showCloseOnSaveButton) {
				this.showCloseOnSaveButton = showCloseOnSaveButton;
			}

		});

		let checkNewAllowedFunction = () => {
			this.checkNewAllowedForMandator().subscribe(isNewAllowed => {
				this.isNewAllowedForMandator = isNewAllowed;
			});
		};

		checkNewAllowedFunction();

		this.route.url.subscribe(() => {
			checkNewAllowedFunction();
		});


		this.initButtons();
	}

	ngOnChanges(_changes: SimpleChanges): void {
		// this.updateVisibility();
	}

	private initButtons() {
		this.buttons = [
			{
				id: 'newEO',
				labelKey: 'webclient.button.new',
				buttonCssClass: 'btn btn-sm btn-success',
				iconCssClass: 'fa fa-fw fa-plus',
				action: () => this.addNew(),
				isVisible: () => this.doShowNewButton(),
				isEnabled: () => true
			},
			{
				id: 'saveEO',
				labelKey: 'webclient.button.save',
				buttonCssClass: 'btn btn-sm btn-primary',
				iconCssClass: 'fa fa-fw fa-floppy-o',
				action: () => this.save(),
				isVisible: eo => !!eo && eo.isDirty(),
				isEnabled: () => this.isSaveButtonEnabled(),
				blur: () => this.hideUnsavedChangesPopover()
			},
			{
				id: 'cancelEO',
				labelKey: 'webclient.button.cancel',
				buttonCssClass: 'btn btn-sm btn-warning',
				iconCssClass: 'fa fa-fw fa-times',
				action: () => this.cancel(),
				isVisible: eo => !!eo && eo.isDirty(),
				isEnabled: () => !this.saveInProgress
			},
			{
				id: 'deleteEO',
				labelKey: 'webclient.button.delete',
				buttonCssClass: 'btn btn-sm btn-danger',
				iconCssClass: 'fa fa-fw fa-trash',
				action: () => this.delete(),
				isVisible: eo => !!eo && eo.canDelete() && !this.isOpenedInPopup(),
				isEnabled: eo => !!eo && eo.canDelete()
			},
		];
	}

	private isSaveButtonEnabled(): boolean {
		return !!this.eo && this.eo.canWrite() && !this.saveInProgress;
	}

	private isOpenedInPopup(): boolean {
		return this.router.url.indexOf('/popup/') !== -1;
	}

	/**
	 * Prepares for adding a new EO.
	 * A dummy EO instance is instantiated and selected.
	 */
	addNew() {
		this.previousEO = this.eo;

		let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
		if (selectedEntityClassId) {
			this.metaService.getEntityMeta(selectedEntityClassId).subscribe((meta: EntityMeta) => {

				// if more then 1 mandators are available ask user to choose mandator
				let mandatorLevelId = meta.getMandatorLevelId();
				if (mandatorLevelId) {
					this.mandators = this.getMandatorsByLevelAndCurrentMandator(mandatorLevelId);
				}

				let nuclosMandatorAttributeMeta = meta.getMandatator();
				if (nuclosMandatorAttributeMeta) {
					this.mandatorLabel = nuclosMandatorAttributeMeta.getName();
				}
				if (this.mandators) {
					if (this.mandators.length > 1 && this.selectedMandator === undefined) {
						this.showMandatorSelection = true;

						// TODO: Hack!
						$('nuc-layout').hide();

						return;
					} else if (this.mandators.length === 1) {
						this.selectedMandator = this.mandators[0];
					}
				}

				this.eoResultService.navigateToNew();
			});
		}

		$('#detailButtonsContainer')['popover']();
	}

	// TODO: Why is mandator logic in a button component?!
	chooseMandatorCancel() {
		this.selectedMandator = undefined;
		this.showMandatorSelection = false;
		if (this.eo) {
			this.eo.setDirty(false);
		}
	}

	// TODO: Why is mandator logic in a button component?!
	selectMandator(mandator: MandatorData) {
		this.selectedMandator = mandator;
		this.showMandatorSelection = false;
		$('nuc-layout').show();

		this.eoService.selectMandator(mandator);
		this.addNew();
	}

	// TODO: Why is mandator logic in a button component?!
	private getMandatorsByLevelAndCurrentMandator(mandatorLevelId: string): MandatorData[] {
		let authentication = this.authenticationService.getAuthentication();
		if (authentication) {
			let mandators = authentication.mandators;
			let currentMandator = authentication.mandator;
			if (mandators) {
				let result = mandators
					.filter(m => m.mandatorLevelId === mandatorLevelId)
					.filter(m => !currentMandator || m.path.indexOf(currentMandator.path) === 0)
				;
				return result;
			}
		}
		return [];
	}


	/**
	 * check if user can create a new entry dependent on selected mandator
	 * TODO: Why is mandator logic in a button component?!
	 */
	private checkNewAllowedForMandator(): Observable<boolean> {
		return new Observable(observer => {
			let authentication = this.authenticationService.getAuthentication();
			let selectedEntityClassId = this.eoResultService.getSelectedEntityClassId();
			if (selectedEntityClassId) {
				this.metaService.getEntityMeta(selectedEntityClassId).subscribe((meta: EntityMeta) => {
					let mandatorLevelId = meta.getMandatorLevelId();

					let isNewAllowed = (
						// mandators not present
						(authentication !== undefined && authentication.mandators && authentication.mandators.length === 0)

						// eo is not mandator dependent
						|| !mandatorLevelId

						// mandators present but no mandator selection available for this bo
						|| !!mandatorLevelId && this.getMandatorsByLevelAndCurrentMandator(mandatorLevelId).length !== 0
					);
					observer.next(isNewAllowed);
					observer.complete();
					});
			}
		});
	}

	doShowNewButton() {
		return this.canCreateBo && !(this.eo && this.eo.isDirty()) && this.isNewAllowedForMandator;
	}

	/**
	 * Saves the current (modified) EO and selects it again.
	 */
	save() {
		if (this.eo) {
			this.saveInProgress = true;
			this.eo.save().subscribe(
				// Select this EO again after saving - triggers updates in other components
				eo => {
					this.saveInProgress = false;
					if (this.selectEntryInOtherWindowToken) {
						this.selectEoInOtherWindow(eo);
					} else {
						eo.select();
						this.selectEoInOtherWindow(eo);
					}
					if (this.showCloseOnSaveButton) {
						window.close();
					}
				},
				() => {
					this.saveInProgress = false;
				}
			);
		}
	}

	/**
	 * Cancels the "new" operation.
	 * Selects the previous EO again.
	 */
	cancel() {
		if (!this.eo) {
			// Should never be reached - "cancel" should not be available when there's no EO
			this.$log.error('Could not cancel editing - no EO');
			return;
		}

		if (this.eo.isNew()) {
			this.eo.setDirty(false);
			this.eoService.delete(this.eo).subscribe(
				() => {
					if (this.previousEO) {
						this.eoNavigationService.navigateToEo(this.previousEO);
					} else {
						this.eoResultService.selectEo(undefined);
					}
				}
			);
		} else {
			this.eoResultService.resetEo(this.eo);
		}

		this.selectedMandator = undefined;
	}

	/**
	 * Deletes the current EO and goes back to the previous EO.
	 */
	delete() {
		this.dialogService.confirm({
				title: this.i18n.getI18n('webclient.button.delete'),
				message: this.i18n.getI18n('webclient.dialog.delete')
			}
		).then(
			() => {
				// ok, delete
				if (this.eo) {
					this.eo.delete().subscribe();
				}
			},
			() => {
				// cancel
			}
		);
	}

	/**
	 * will be called when selecting a referenced attribute in another window
	 */
	selectThisEoInOtherWindow() {
		if (this.eo) {
			this.selectEoInOtherWindow(this.eo);
			window.close();
		}
	}

	private selectEoInOtherWindow(eo: EntityObject) {
		let eoId = eo.getId();
		if (!eoId) {
			this.$log.warn('Could not select EO without ID: %o', eo);
			return;
		}
		// send event to other browser instance
		let selectReferenceEvent = new SelectReferenceInOtherWindowEvent();
		selectReferenceEvent.entityClassId = eo.getEntityClassId();
		selectReferenceEvent.eoId = eoId;
		let name = eo.getData().title;
		if (name) {
			selectReferenceEvent.name = name;
		}
		selectReferenceEvent.token = Number(this.selectEntryInOtherWindowToken);
		this.browserRefreshService.selectEntryInOtherWindow(selectReferenceEvent);
	}

	@Input()
	set triggerUnsavedChangesPopover(date: Date) {
		this.$log.debug('Show popover on %o', this.popover);
		if (date && !this.dialogService.hasOpenDialog()) {
			this.runOrRetry(() => {
				$(this.popover.nativeElement)['popover']('show');
				$('#saveEO').focus();
			}, 3);
		}
	}

	hideUnsavedChangesPopover() {
		this.runOrRetry(
			() => $(this.popover.nativeElement)['popover']('hide'),
			3
		);
	}

	private runOrRetry(fn: Function, times: number, count?: number) {
		try {
			fn();
		} catch (e) {
			this.$log.warn(e);
			if (count === undefined || count < times) {
				let nextCount = count !== undefined ? count + 1 : 1;
				setTimeout(
					() => this.runOrRetry(fn, times, nextCount),
					150
				);
			}
		}
	}

	hasVisibleButtons() {
		return this.buttons.find(button => button.isVisible(this.eo));
	}
}
