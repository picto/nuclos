import { TestBed, inject } from '@angular/core/testing';

import { NuclosCookieService } from './nuclos-cookie.service';

describe('NuclosCookieService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosCookieService]
		});
	});

	it('should ...', inject([NuclosCookieService], (service: NuclosCookieService) => {
		expect(service).toBeTruthy();
	}));
});
