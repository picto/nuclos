import { Injectable } from '@angular/core';
import { CookieOptionsArgs, CookieService } from 'angular2-cookie/core';
import { StringUtils } from '../../shared/string-utils';

@Injectable()
export class NuclosCookieService {

	constructor(
		private cookieService: CookieService
	) {
	}

	get(key: string) {
		return this.cookieService.get(key);
	}

	getObject(key: string) {
		return this.cookieService.getObject(key);
	}

	put(key: string, value: string, args?: CookieOptionsArgs) {
		let options = this.getOptions(args);

		this.cookieService.put(key, value, options);
	}

	putObject(key: string, value: object) {
		let options = this.getOptions();

		this.cookieService.putObject(key, value, options);
	}

	remove(key: string) {
		let options = this.getOptions();

		this.cookieService.remove(key, options);
	}

	private getOptions(args?: CookieOptionsArgs) {
		let options: CookieOptionsArgs = args || {};

		if (!options.path) {
			options.path = this.getPathDirectory();
		}

		return options;
	}

	private getPathDirectory() {
		let path = window.location.pathname;
		let dir = path.substring(0, path.lastIndexOf('/')) + '/';

		dir = StringUtils.regexReplaceAll(dir, '/{2,}', '/');

		return dir;
	}
}
