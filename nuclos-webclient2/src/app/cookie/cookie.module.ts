import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CookieOptions, CookieService } from 'angular2-cookie/core';
import { NuclosCookieService } from './shared/nuclos-cookie.service';

export function cookieOptionsFactory() {
	return new CookieOptions();
}

export function cookieServiceFactory(options: CookieOptions) {
	return new CookieService(options);
}

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [],
	providers: [
		{provide: CookieOptions, useFactory: cookieOptionsFactory},
		{
			provide: CookieService,
			useFactory: cookieServiceFactory,
			deps: [CookieOptions]
		},
		NuclosCookieService
	]
})
export class CookieModule {
}
