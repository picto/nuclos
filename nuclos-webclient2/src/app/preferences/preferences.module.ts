import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { DialogModule } from '../dialog/dialog.module';
import { I18nModule } from '../i18n/i18n.module';
import { PreferencesComponent } from './preferences.component';
import { PreferencesRoutes } from './preferences.routes';
import { PreferencesService } from './preferences.service';
import { ListModule } from '../list/list.module';
import { AgGridModule } from 'ag-grid-angular';
import { SharedRendererComponent } from './cell-renderer/shared-renderer/shared-renderer.component';
import { TypeRendererComponent } from './cell-renderer/type-renderer/type-renderer.component';
import { EditRowRendererComponent } from './cell-renderer/edit-row-renderer/edit-row-renderer.component';
import { PreferencesAdminService } from './preferences-admin.service';
import { SharedFilterComponent } from './filter/shared-filter/shared-filter.component';
import { GridModule } from '../grid/grid.module';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		PrettyJsonModule,

		AgGridModule.withComponents([
			SharedRendererComponent,
			TypeRendererComponent,
			EditRowRendererComponent
		]),

		DialogModule,
		I18nModule,
		ListModule,
		GridModule,

		PreferencesRoutes
	],
	declarations: [
		SharedRendererComponent,
		TypeRendererComponent,
		EditRowRendererComponent,
		PreferencesComponent,
		SharedFilterComponent
	],
	providers: [
		PreferencesAdminService,
		PreferencesService
	],
	entryComponents: [
		SharedFilterComponent
	]
})
export class PreferencesModule {
}
