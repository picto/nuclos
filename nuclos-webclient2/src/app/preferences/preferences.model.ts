import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { BoAttr } from '../entity-object/shared/bo-view.model';
export type SidebarLayoutType = 'table' | 'card';
export type PreferenceTypeName =
	'table'
	| 'subform-table'
	| 'searchtemplate'
	| 'chart'
	| 'menu'
	| 'perspective';

export class PreferenceType {
	static searchtemplate: PreferenceTypeConfig = {name: 'searchtemplate', iconClass: 'fa-search'};
	static table: PreferenceTypeConfig = {name: 'table', iconClass: 'fa-columns'};
	static subformTable: PreferenceTypeConfig = {name: 'subform-table', iconClass: 'fa-table'};
	static perspective: PreferenceTypeConfig = {name: 'perspective', iconClass: 'fa-eye'};
	static default: PreferenceTypeConfig = {name: 'menu', iconClass: 'fa-asterisk'};

	static getIconClass(preferenceTypeName: string) {
		return Object.getOwnPropertyNames(PreferenceType)
			.map(key => {
					return {
						name: PreferenceType[key].name,
						iconClass: PreferenceType[key].iconClass
					};
				}
			)
			.filter(p => p.name === preferenceTypeName).map(p => p.iconClass).shift();
	}
}

export interface IPreferenceFilter {
	type?: Array<PreferenceType>;
	boMetaId?: string;
	selected?: boolean;
	layoutId?: string;
	orLayoutIsNull?: boolean;

	/**
	 * If true, only preferences for sub-BOs of the given BO with the given boMetaId should be returned.
	 */
	returnSubBoPreferences?: boolean;
}


export class Preference<T> {
	type: PreferenceTypeName;
	prefId?: string;
	boMetaId: string;
	layoutId?: string;
	layoutName?: string;
	boName?: string;
	name?: string;
	shared?: boolean;
	selected?: boolean;
	iconClass?: string;
	customized?: boolean;
	content: T;
	dirty?: boolean;

	constructor(
		_type: PreferenceTypeName,
		_boMetaId: string
	) {
		this.type = _type;
		this.boMetaId = _boMetaId;
		this.content = {} as T;
	}
}

export interface SelectedAttribute extends BoAttr {
	selected: boolean;
	resizingInProgress?: boolean;
	searchPopoverOpen?: boolean;
	width?: number;
}

export abstract class PreferenceContent {
}

export abstract class AttributeSelectionContent extends PreferenceContent {
	columns: Array<ColumnAttribute>;

	// true if Preference.name is not default generated from selected columns
	userdefinedName?: boolean;
}

export interface SearchtemplatePreferenceContent extends AttributeSelectionContent {
	columns: Array<SearchtemplateAttribute>;
	disabled: boolean;
	isValid: boolean;
	isNewBoInstanceTemplate?: boolean;
}

export interface SearchtemplateAttribute extends SelectedAttribute {
	operator?: string;
	value?: any;
	formattedValue?: string;
	enableSearch?: boolean;
	isValid?: boolean;
	searchPopoverOpen?: boolean;
	datepickerValue?: NgbDateStruct;
}

export class SideviewmenuPreferenceContent extends AttributeSelectionContent {
	sideviewMenuWidth?: number;
	sidebarLayout?: SidebarLayoutType;
}


export interface ColumnAttribute extends SelectedAttribute {
	sort?: SideviewColumnSort;
	position?: number;
}

export interface SideviewColumnSort {
	direction?: string;
	prio?: number;
	enabled?: boolean;
}


export interface IUserRole {
	name: string;
	userRoleId: string;
	shared: boolean;
}

export class PreferenceTypeConfig {
	name: PreferenceTypeName;
	iconClass: string;
}
