import { Injectable } from '@angular/core';
import { Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';
import { NuclosCache, NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { MetaService } from '../entity-object/shared/meta.service';
import { Logger } from '../log/shared/logger';
import { FqnService } from '../shared/fqn.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import {
	IPreferenceFilter,
	IUserRole,
	Preference,
	PreferenceContent,
	PreferenceType,
	SearchtemplatePreferenceContent,
	SideviewmenuPreferenceContent
} from './preferences.model';
import { BrowserRefreshService } from '../shared/browser-refresh.service';

export const PREFERENCE_NAME_MAX_LENGTH = 255;

@Injectable()
export class PreferencesService {

	private preferencesCache: NuclosCache;

	constructor(
		private nuclosConfig: NuclosConfigService,
		private cacheService: NuclosCacheService,
		private http: NuclosHttpService,
		private metaService: MetaService,
		private fqnService: FqnService,
		private browserRefreshService: BrowserRefreshService,
		private $log: Logger
	) {
		this.preferencesCache = this.cacheService.getCache('preferencesCache');
	}

	getPreference(prefId: string): Observable<Preference<PreferenceContent>> {

		return this.http.get(
			this.nuclosConfig.getRestHost() + '/preferences/' + prefId
		)
			.map((response: Response) => response.json())
			.map(this.handleSideviewmenuPrefs);
	}


	getPreferences(filter: IPreferenceFilter, useCache = false): Observable<Array<Preference<PreferenceContent>>> {

		this.$log.debug('filter: %o', filter);

		let filterString = (filter.type ? filter.type.join(',') : undefined);
		let key = filter.boMetaId + ':' + filterString;
		this.$log.debug('pref key: %o', key);

		let params = new URLSearchParams();
		if (filter.boMetaId) {
			params.append('boMetaId', filter.boMetaId);
		}
		if (filter.layoutId) {
			params.append('layoutId', filter.layoutId);
		}
		if (filter.orLayoutIsNull) {
			params.append('orLayoutIsNull', 'true');
		}
		if (filter.returnSubBoPreferences) {
			params.append('returnSubBo', 'true');
		}
		if (filterString) {
			params.append('type', filterString);
		}

		let observable = this.http.get(
			this.nuclosConfig.getRestHost() + '/preferences',
			{
				search: params
			}
		)
			.map((response: Response) => response.json())
			.map(
				(preferences: Preference<any>[]) => {
					return preferences.map(this.handleSideviewmenuPrefs);
				}
			)
			.map(this.preferenceFormatMapper())
			.do(prefs => Logger.instance.debug('Loaded prefs for filter %o = %o', filter, prefs));

		let cacheKey = key + '-' + filterString;

		return useCache ? this.preferencesCache.get(cacheKey, observable) : observable;
	}

	/**
	 * convert preferences format from webclient1 to webclient2
	 */
	private preferenceFormatMapper() {
		return (preferences: Preference<any>[]) => {
			return preferences.map(
				(pref: Preference<any>) => {
					if (pref.type === 'searchtemplate') {
						let searchtemplatePref = pref as Preference<SearchtemplatePreferenceContent>;
						if (pref.content.attributes && !pref.content.columns) {
							searchtemplatePref.content.columns = pref.content.attributes;
							this.metaService.getBoMeta(pref.boMetaId).subscribe(
								meta => {
									searchtemplatePref.content.columns.forEach(column => {
										if (column.enableSearch) {
											column.selected = true;
											column.isValid = true;
										}
										let attrName = this.fqnService.getShortAttributeName(pref.boMetaId, column.boAttrId);
										let attributeLabel = meta.getAttributeLabel(attrName);
										if (attributeLabel) {
											column.name = attributeLabel;
										}
									});
								}
							);
						}
					}
					return pref;
				}
			);
		};
	}

	/*
	 * in preferences only selected columns are stored (without selected flag)
	 */
	private handleSideviewmenuPrefs(pref: Preference<PreferenceContent>) {
		if (pref.type === 'table' || pref.type === 'subform-table') {
			let sideviewmenuPref = (<Preference<SideviewmenuPreferenceContent>> pref);
			sideviewmenuPref.content.columns.forEach(c => c.selected = true);
			if (sideviewmenuPref && !sideviewmenuPref.content.sideviewMenuWidth) {
				sideviewmenuPref.content.sideviewMenuWidth = 200;
			}
		}
		return pref;
	}

	getPreferenceShareGroups(preferenceItem: Preference<any>) {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share'
		)
			.map((response: Response) => response.json());
	}


	shareOrUnsharePreferenceItem(preferenceItem: Preference<any>, userRole: IUserRole) {

		let url = this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share/' + userRole.userRoleId;

		let restCall;
		if (userRole.shared) {
			restCall = this.http.post(
				url,
				null
			);
		} else {
			restCall = this.http.delete(
				url
			);
		}

		return restCall
			.map((response: Response) => response.json())
			.do(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			});
	}

	/**
	 * removes all shared roles
	 * @param preferenceItem
	 * @return {Promise<T>}
	 */
	unsharePreferenceItem(preferenceItem: Preference<any>): Promise<any> {
		return new Promise(resolve => {
			this.getPreferenceShareGroups(preferenceItem).subscribe(
				data => {
					let promises: Promise<any>[] = [];
					for (let userRole of data.userRoles) {
						if (userRole.shared) {
							let url = this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share/' + userRole.userRoleId;
							let promise = this.http.delete(url).map((response: Response) => response.json()).toPromise();
							promises.push(promise);
						}
					}
					Promise.all(promises).then(() => {
						this.browserRefreshService.emitPreferenceChange(preferenceItem);
						resolve()
					});
				}
			);
		});
	}

	updatePreferenceShare(preferenceItem: Preference<any>): Observable<void> {
		return this.http.put(
			this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/share',
			preferenceItem
		)
			.map((response: Response) => response.json());
	}

	/**
	 * reset customized preferences
	 * @param types
	 * @param eoClassId
	 * @return {Observable<T>}
	 */
	resetCustomizedPreferenceItems(types: PreferenceType[], eoClassId?: string): Observable<boolean> {
		let typesParamString = '?types=' + types.join(',');
		let eoClasssIdParamString = eoClassId ? '&boMetaId=' + eoClassId : '';
		return this.http.get(this.nuclosConfig.getRestHost() + '/preferences/reset' + typesParamString + eoClasssIdParamString)
			.map(
				// no json response when resetting preference
				() => true
			)
			.do(() => {
				this.browserRefreshService.emitPreferencesChange();
			});
	}

	selectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		return this.http.put(
			this.getSelectionURL(preferenceItem, layoutId),
			undefined
		).map((response: Response) => response.json());
	}

	deselectPreference(preferenceItem: Preference<any>, layoutId?: string): Observable<void> {
		return this.http.delete(
			this.getSelectionURL(preferenceItem, layoutId)
		).map((response: Response) => response.json());
	}

	private getSelectionURL(preferenceItem: Preference<any>, layoutId?: string) {
		return this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId + '/select' + (layoutId ? '/' + layoutId : '');
	}

	deleteCustomizedPreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		if (preferenceItem.customized) {
			return this.deletePreferenceItem(preferenceItem);
		}
		return Observable.of(false);
	}

	/**
	 * Saves the given preference item.
	 *
	 * If the item is new (without prefId), it is saved as new preference.
	 * If it is updated, it might be saved as 'customization' of a shared preference.
	 *
	 */
	savePreferenceItem(preferenceItem: Preference<any>): Observable<Preference<any> | undefined> {

		if (preferenceItem.name && preferenceItem.name.length > PREFERENCE_NAME_MAX_LENGTH) {
			preferenceItem.name = preferenceItem.name.substr(0, PREFERENCE_NAME_MAX_LENGTH - 2) + '..';
		}

		if (preferenceItem.prefId) {
			// update existing preference
			return this.http.put(
				this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId,
				preferenceItem
			).map(
				// no json response when updating preference
				() => preferenceItem
			).do(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			});
		} else {
			return this.http.post(
				this.nuclosConfig.getRestHost() + '/preferences',
				preferenceItem
			)
			.map((response: Response) => response.json())
			.do(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			});
		}

	}

	deletePreferenceItem(preferenceItem: Preference<any>): Observable<boolean> {
		return this.http.delete(this.nuclosConfig.getRestHost() + '/preferences/' + preferenceItem.prefId)
			.map(
				// no json response when deleting preference
				() => true
			)
			.do(() => {
				this.browserRefreshService.emitPreferenceChange(preferenceItem);
			});
	}

	/**
	 * Delete multipe preference items.
	 * Shared preferences will be unshared.
	 * @param preferenceItems
	 * @return {Observable<R>}
	 */
	deletePreferenceItems(preferenceItems: Preference<any>[]): Observable<boolean> {
		let prefIds = preferenceItems.map(p => p.prefId);
		return this.http.post(this.nuclosConfig.getRestHost() + '/preferences/delete', prefIds)
			.map(
				// no json response when deleting preference
				() => true
			);
	}

	/**
	 * assign/remove user roles to all preferences depending on userRole.selected flag
	 * not selected user roles will be removed
	 */
	shareUserRoles(preferenceItems: Preference<any>[], userRoles: IUserRole[]): Promise<any> {
		return Promise.all(
			[
				...preferenceItems
					.map(pref => {
						return Promise.all(
							[
								...userRoles
									.map(userRole => {
										return this.shareOrUnsharePreferenceItem(pref, userRole).toPromise();
									})
							]
						)
					})
			]
		);
	}
}
