import { RouterModule, Routes } from '@angular/router';
import { PreferencesComponent } from './preferences.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'preferences',
		redirectTo: 'preferences/list'
	},
	{
		path: 'preferences/:prefId',
		component: PreferencesComponent
	}
];

export const PreferencesRoutes = RouterModule.forChild(ROUTE_CONFIG);
