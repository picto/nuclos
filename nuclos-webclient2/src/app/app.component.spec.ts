/* tslint:disable:no-unused-variable */

import { async, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DialogModule } from './dialog/dialog.module';
import { MenuModule } from './menu/menu.module';

describe('App: NuclosWebclient2', () => {

	beforeEach(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent
			],
			imports: [RouterTestingModule, DialogModule, MenuModule]
		});
	});

	it('should create the app', async(() => {
		let fixture = TestBed.createComponent(AppComponent);
		let app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	// it(`should have as name 'app works!'`, async(() => {
	// 	let fixture = TestBed.createComponent(AppComponent);
	// 	let app = fixture.debugElement.componentInstance;
	// 	expect(app.name).toEqual('app works!');
	// }));

	// it('should render name in a h1 tag', async(() => {
	// 	let fixture = TestBed.createComponent(AppComponent);
	// 	fixture.detectChanges();
	// 	let compiled = fixture.debugElement.nativeElement;
	// 	expect(compiled.querySelector('h1').textContent).toContain('app works!');
	// }));
});
