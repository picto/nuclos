import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TwoSideMultiSelectComponent } from './two-side-multi-select.component';
import { DndModule } from 'ng2-dnd';
import { ListModule } from '../list/list.module';
import { I18nModule } from '../i18n/i18n.module';

@NgModule({
	imports: [
		CommonModule,
		ListModule,
		I18nModule,
		DndModule
	],
	declarations: [
		TwoSideMultiSelectComponent
	],
	exports: [
		TwoSideMultiSelectComponent
	]
})
export class TwoSideMultiSelectModule {
}
