import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Logger } from '../../log/shared/logger';
import { ServerLogLevel } from '../shared/server-log-level';
import { ServerLogService, SqlLoggingMode } from '../shared/server-log.service';

@Component({
	selector: 'nuc-server-log-options',
	templateUrl: './server-log-options.component.html',
	styleUrls: ['./server-log-options.component.css']
})
export class ServerLogOptionsComponent implements OnInit {
	@Input() autoScroll = true;
	@Output() autoScrollChange = new EventEmitter<boolean>();

	@Input() logLevel: string = ServerLogLevel[ServerLogLevel.INFO];
	@Output() logLevelChange = new EventEmitter<string>();

	@Input() sqlLoggingMode;
	@Output() sqlLoggingModeChange = new EventEmitter<any>();

	@Input() textFilter = '';
	@Output() textFilterChange = new EventEmitter<string>();

	logLevels: string[] = Object.keys(ServerLogLevel).filter(key => isNaN(Number(key)));

	sqlLoggingOptions = [
		{name: '', value: SqlLoggingMode.Disabled},
		{name: 'SQL-Logging', value: SqlLoggingMode.SQLLogger},
		{name: 'SQL-Timer-Logging', value: SqlLoggingMode.SQLTimerLogger},
		{name: 'SQL-UpdateInsertDelete-Logging', value: SqlLoggingMode.SQLUpdateLogger}
	];

	constructor(
		private $log: Logger,
		private serverLogService: ServerLogService
	) {
	}

	ngOnInit() {
		this.serverLogService.getSqlLoggingMode().subscribe(sqlLoggingMode => {
			this.sqlLoggingMode = sqlLoggingMode;
			this.$log.debug('sql logging mode = %o', sqlLoggingMode);
		});
	}

	setAutoscroll(autoScroll: boolean) {
		this.autoScroll = autoScroll;
		this.autoScrollChange.emit(autoScroll);
	}

	setLogLevel(logLevel: string) {
		this.logLevel = logLevel;
		this.logLevelChange.emit(logLevel);
	}

	setSqlLoggingMode(sqlLoggingMode) {
		this.sqlLoggingMode = sqlLoggingMode;
		this.sqlLoggingModeChange.emit(sqlLoggingMode);
	}

	setTextFilter(textFilter: string) {
		this.$log.debug(textFilter);
		this.textFilter = textFilter;
		this.textFilterChange.emit(textFilter);
	}

	selectSqlLoggingMode(sqlLoggingMode: SqlLoggingMode) {
		this.serverLogService.setLoggingMode(sqlLoggingMode);
	}
}
