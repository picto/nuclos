import { ServerLogLevel } from './server-log-level';
export class ServerLogMessage {
	counter: number;
	date: Date;
	formattedDate?: string;
	level: ServerLogLevel;
	message: string;
}
