import { AppComponent } from './app.component';
import { AppRoutesModule } from './app.routes';
import { AuthenticationModule } from './authentication/authentication.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserRefreshService } from './shared/browser-refresh.service';
import { BusinesstestModule } from './businesstest/businesstest.module';
import { CacheModule } from './cache/cache.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { DatetimeService } from './shared/datetime.service';
import { DialogModule } from './dialog/dialog.module';
import { EntityObjectModule } from './entity-object/entity-object.module';
import { ErrorModule } from './error/error.module';
import { FormsModule } from '@angular/forms';
import { FqnService } from './shared/fqn.service';
import { I18nModule } from './i18n/i18n.module';
import { LogModule } from './log/log.module';
import { MapEntriesPipe } from './shared/map-entries.pipe';
import { MenuModule } from './menu/menu.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { NuclosConfigService } from './shared/nuclos-config.service';
import { NumberService } from './shared/number.service';
import { PreferencesModule } from './preferences/preferences.module';
import { ServerInfoModule } from './server-info/server-info.module';
import { ValidationModule } from './validation/validation.module';
import { AccountModule } from './account/account.module';
import { DisclaimerModule } from './disclaimer/disclaimer.module';
import { BrowserDetectionService } from './shared/browser-detection.service';
import { HyperlinkService } from './shared/hyperlink.service';
import { CanNeverActivateGuard } from './shared/can-never-activate-guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCATION_INITIALIZED } from '@angular/common';
import { IdFactoryService } from './shared/id-factory.service';
import { DndModule } from 'ng2-dnd';
import { HttpModule } from './http/http.module';
import { WindowTitleService } from './entity-object/shared/window-title.service';

export function configFactory(config: NuclosConfigService, injector: Injector) {
	// fix for https://github.com/angular/angular-cli/issues/5762
	return () => new Promise<any>((resolve: any) => {
		const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
		locationInitialized.then(() => {
			config.load().then(() => resolve(null));
		});
	});
}

@NgModule({
	declarations: [
		AppComponent,
		MapEntriesPipe
	],
	imports: [
		// Angular modules
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,

		NgbModule.forRoot(), // needed for DialogModule

		DndModule.forRoot(),

		// Nuclos modules
		HttpModule,
		AccountModule,
		AuthenticationModule,
		BusinesstestModule,
		DashboardModule,
		DialogModule,
		EntityObjectModule,
		ErrorModule,
		I18nModule,
		LogModule,
		MenuModule,
		PreferencesModule,
		ServerInfoModule,
		CacheModule,
		ValidationModule,
		DisclaimerModule,

		// App Routes (contains Wildcard-Routes and must therefor be defined last)
		AppRoutesModule
	],
	providers: [
		BrowserRefreshService,
		BrowserDetectionService,
		FqnService,
		DatetimeService,
		NumberService,
		NuclosConfigService,
		HyperlinkService,
		{
			provide: APP_INITIALIZER,
			useFactory: configFactory,
			deps: [NuclosConfigService, Injector],
			multi: true
		},
		CanNeverActivateGuard,
		IdFactoryService,
		WindowTitleService,
	],
	exports: [
		AppComponent
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {
}
