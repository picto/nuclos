export enum Action {
	SharePreferences,
	ConfigureCharts,
	ConfigurePerspectives,
	WorkspaceCustomizeEntityAndSubFormColumn,
	PrintSearchResultList
}
