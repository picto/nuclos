import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { NavigationEnd, Router } from '@angular/router';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { NuclosCookieService } from '../cookie/shared/nuclos-cookie.service';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { LinkContainer } from '../shared/link.model';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { NuclosHttpService } from '../shared/nuclos-http.service';
import { SystemParameter } from '../shared/system-parameters';
import { User } from './';
import { Action } from './action.enum';

/**
 * Holds data for the current user session.
 */
export interface AuthenticationData {
	sessionId: string;
	username: string;
	allowedActions: any;
	superUser: boolean;
	initialEntity?: string;
	mandators?: Array<MandatorData>;
	mandator?: MandatorData;
}

export interface MandatorData {
	mandatorLevelId: string;
	mandatorId: string;
	name: string;
	path: string;
	color?: string;
	links: LinkContainer;
}

export class LoginData {
	username: string;
	password: string;
	locale: string;
	autologin: boolean;
}

@Injectable()
export class AuthenticationService {

	static readonly COOKIE_KEY_REDIRECT = 'redirect';

	private currentUser: User | undefined;
	private allowedActions: Map<string, boolean>;

	private readonly ANONYMOUS_USER_NAME = 'anonymous';

	private loggedIn = false;
	private loginSubject: ReplaySubject<boolean>;

	private authentication: AuthenticationData | undefined;

	private mandatorSelection: BehaviorSubject<MandatorData | undefined>;
	private authenticationDataAvailable: BehaviorSubject<boolean> = new BehaviorSubject(false);

	private loginInProgress = false;

	constructor(
		private router: Router,
		private http: NuclosHttpService,
		private cookieService: NuclosCookieService,
		private nuclosConfig: NuclosConfigService,
		private $log: Logger,
		private i18nService: NuclosI18nService,
		private cacheService: NuclosCacheService
	) {
		this.loginSubject = new ReplaySubject<boolean>(1);

		this.router.events.subscribe(
			event => {
				if (event instanceof NavigationEnd) {
					let url = (<NavigationEnd>event).url;
					// Redirect to the login page, if not logged in and the page requires authentication
					if (this.isAuthenticationRequired(url)) {
						this.observeLoginStatus().take(1).subscribe(loggedIn => this.checkLogin(loggedIn, url));
					}
				}
			}
		);

		this.mandatorSelection = new BehaviorSubject(undefined);

		this.observeLoginStatus().subscribe(() => this.cacheService.invalidateAllCaches());

		this.init();
	}

	private checkLogin(loggedIn, url: string) {
		this.$log.debug('Logged in: %o', loggedIn);
		if (!loggedIn) {
			this.redirectToLoginPage();
		} else {
			// Remember the last location after successful navigation
			this.$log.debug('Location changed: %o', url);
			if (this.shouldUrlBeRemembered(url)) {
				this.rememberUrl(url);
			}
		}
	}

	init() {
		this.removeRedirectCookie();
		this.setRedirectCookie(window.location.hash);

		this.autologin();
	}

	login(loginData: LoginData): Observable<AuthenticationData> {
		this.loginInProgress = true;

		return new Observable<AuthenticationData>(observer => {
			return this.http.post(
				this.nuclosConfig.getRestHost(),
				JSON.stringify(loginData)
			).map(
				(res: Response) => res.json()
			).finally(
				() => this.loginInProgress = false
			).subscribe(
				data => {
					this.loginSuccessful(data);
					observer.next(data);
					observer.complete();
				},
				error => {
					this.$log.debug('Login with data %o failed: ', loginData, error);
					observer.error(error);
				}
			);
		});
	}

	onAuthenticationDataAvailable(): BehaviorSubject<boolean> {
		return this.authenticationDataAvailable;
	}

	/**
	 * Performs a login as the anonymous user.
	 *
	 * @returns {Subscription}
	 */
	private loginAsAnonymous(): Observable<AuthenticationData> {
		this.$log.info('Trying anonymous login...');
		let loginData = {
			username: this.ANONYMOUS_USER_NAME,
			password: this.ANONYMOUS_USER_NAME,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: false
		};

		return this.login(loginData);
	}

	getCurrentUser(): User | undefined {
		return this.currentUser;
	}

	/**
	 * Determines if the given Action is allowed for the current user.
	 *
	 * @param action
	 * @returns {any}
	 */
	isActionAllowed(action: Action): boolean {
		if (this.isSuperUser()) {
			return true;
		}

		if (!this.authenticationDataAvailable.getValue()) {
			this.$log.warn('Calling isActionAllowed before authentication data is loaded.');
			return false;
		}

		if (!this.allowedActions) {
			return false;
		}

		let actionString = Action[action];
		return this.allowedActions[actionString];
	}

	/**
	 * Determines if the current user is logged in as "anonymous",
	 * based on the given data or the current username.
	 *
	 * @param data
	 * @returns {boolean}
	 */
	isAnonymous(data?: AuthenticationData) {
		if (!data) {
			return this.getUsername() === this.ANONYMOUS_USER_NAME;
		}

		return data.username === this.ANONYMOUS_USER_NAME;
	};

	/**
	 * Handles login data after a successful login.
	 *
	 * @param data
	 */
	private loginSuccessful(data: AuthenticationData) {
		this.$log.info('LOGIN SUCCESSFUL: %o', data);

		this.authentication = data;

		this.currentUser = new User(data.username);
		this.allowedActions = data.allowedActions;
		this.authenticationDataAvailable.next(true);

		this.redirectToLastLocation(data);
		this.redirectToInitialEntity(data);

		// SAVE COOKIE
		this.cookieService.putObject('authentication', data);

		if (data.mandator) {
			this.mandatorSelection.next(data.mandator);
		}

		if (!data.mandators) {
			this.autoNavigation();
		}

		this.setLoggedIn(true);
	};

	autoNavigation() {
		if (this.isAutoNavigationAllowed()) {
			let redirect = this.getRedirectCookie();
			if (redirect) {
				this.removeRedirectCookie();
				// Prevent redirecting to the same location
				if (!window.location.href.endsWith(redirect)) {
					this.$log.debug('Redirecting to: %o', redirect);
					this.router.navigateByUrl(redirect);
				}
			} else {
				this.navigateHome();
			}
		}
	}

	navigateHome(): void {
		this.router.navigate(['dashboard']);
	}

	getMandatorSelection(): Subject<MandatorData> {
		return this.mandatorSelection;
	}

	getCurrentMandatorId(): string | undefined {
		let mandatorData = this.mandatorSelection.getValue();
		return mandatorData ? mandatorData.mandatorId : undefined;
	}

	selectMandator(mandator): Observable<any> {
		let mandatorSelection = this.http.post(mandator.links.chooseMandator.href, {});
		return mandatorSelection.do(() => this.mandatorSelection.next(mandator));
	}

	private autologin() {
		this.loginInProgress = true;

		let checkIfLoggedIn = this.http.get(
			this.nuclosConfig.getRestHost()
		).map(
			(res: Response) => res.json()
		).do(
			(data: AuthenticationData) => {
				this.loginSuccessful(data);
			},
		).catch(() => Observable.empty());

		let anonymousLogin = this.anonymousLogin();

		checkIfLoggedIn.concat(anonymousLogin)
			.take(1)
			.finally(() => this.loginInProgress = false)
			.subscribe();
	}

	/**
	 * Logs in as anonymous, if the anonymous login is enabled via system parameter.
	 */
	private anonymousLogin() {
		return new Observable(observer => {
			this.$log.debug('Checking anonymous login...');
			this.nuclosConfig.getSystemParameters().subscribe(
				params => {
					this.$log.debug('Got system params: %o', params);
					if (params.is(SystemParameter.ANONYMOUS_USER_ACCESS_ENABLED)) {
						this.loginAsAnonymous()
							.finally(() => observer.complete())
							.subscribe(
								data => this.$log.debug('Logged in as anonymous: %o', data)
							);
					} else {
						observer.complete();
					}
				}
			)
		});
	}

	isLoginInProgress() {
		return this.loginInProgress;
	}

	waitForLogin(): Observable<any> {
		if (this.isLoginInProgress()) {
			return this.observeLoginStatus().filter(loggedIn => loggedIn).take(1);
		}
		return Observable.of(true);
	}

	/**
	 * Determines if the page with the given location requires authentication.
	 * This is not the case for start, login/registration and error pages.
	 *
	 * @param locationPath
	 * @returns {boolean}
	 */
	isAuthenticationRequired(locationPath) {
		let result = locationPath.indexOf('/login') === -1
			&& locationPath.indexOf('/logout') === -1
			&& locationPath.indexOf('/locale/') === -1
			&& locationPath !== ''
			&& locationPath !== '/'
			&& locationPath.indexOf('/error') === -1
			&& locationPath.indexOf('/account/register') === -1
			&& locationPath.indexOf('/account/activate') === -1;

		this.$log.debug('isAuthenticationRequired(%o) = %o', locationPath, result);

		return result;
	}

	/**
	 * Determines if it is okay to automatically navigate away from the current URL.
	 */
	isAutoNavigationAllowed() {
		let location = window.location.href;
		return location.indexOf('/account/register') === -1
			&& location.indexOf('/account/activate') === -1;
	}

	hasSessionId() {
		return this.getSessionId() !== undefined;
	}

	getSessionId(): string | null {
		let authentication: AuthenticationData = <AuthenticationData> this.cookieService.getObject('authentication');
		return authentication ? authentication.sessionId : null;
	}

	hasRedirectCookie() {
		return this.getRedirectCookie() !== undefined;
	}

	setRedirectCookie(path: string) {
		path = path ? path.replace(/^#*/, '') : '';
		this.$log.info('Setting redirect cookie for location: %o', path);
		if (this.isAuthenticationRequired(path)) {
			this.$log.debug('Setting redirect cookie: %o', path);
			this.cookieService.put(AuthenticationService.COOKIE_KEY_REDIRECT, path);
		}
	}

	removeRedirectCookie() {
		this.$log.debug('Removing redirect cookie');
		this.cookieService.remove(AuthenticationService.COOKIE_KEY_REDIRECT);
	}

	getRedirectCookie() {
		let redirect = this.cookieService.get(AuthenticationService.COOKIE_KEY_REDIRECT);
		return redirect ? redirect : undefined;
	}

	invalidateSessionId() {
		let auth: AuthenticationData = <AuthenticationData> this.cookieService.getObject('authentication');
		if (auth) {
			delete auth.sessionId;
			this.cookieService.putObject('authentication', auth);
		}
	}

	/**
	 * Sets the redirect cookie to the "initial entity" of this user, if there is not
	 * already a redirect cookie set.
	 */
	private redirectToInitialEntity(data: AuthenticationData) {
		if (!data.initialEntity || this.hasRedirectCookie()) {
			return;
		}

		this.setRedirectCookie('/view/' + data.initialEntity);
	}

	/**
	 * Sets the redirect cookie for a redirect to the last location that is stored via the "location" cookie,
	 * if the user is not anonymous and does not have a redirect cookie already.
	 *
	 * @returns {boolean}
	 */
	redirectToLastLocation(data: AuthenticationData) {
		if (this.isAnonymous(data) || this.hasRedirectCookie()) {
			return;
		}

		let username = data.username;
		let lastLocation = this.cookieService.get('location.' + username);

		if (lastLocation) {
			this.$log.info('Redirecting to last location: %o', lastLocation);
			this.setRedirectCookie(lastLocation);
			return true;
		}

		return false;
	}

	/**
	 * Sets the redirect cookie for a redirect (after login) to the current location.
	 *
	 * @returns {boolean}
	 */
	redirectToCurrentLocation() {
		let location = window.location.hash;
		if (location && this.isAuthenticationRequired(location)) {
			this.setRedirectCookie(location);
			return true;
		}
		return false;
	}

	/**
	 * Redirects immediately to the login page.
	 *
	 * @returns {boolean}
	 */
	redirectToLoginPage() {
		this.$log.debug('Redirecting to login page...');
		this.router.navigate(['/login']);
	}

	getUsername(): string | undefined {
		let username = this.cookieService.get('authentication')
			? (<AuthenticationData> this.cookieService.getObject('authentication')).username
			: undefined;
		return username;
	}

	getAuthentication(): AuthenticationData | undefined {
		return this.authentication;
	}

	/**
	 * Determines if the current user is a superuser.
	 */
	isSuperUser(): boolean {
		// TODO: Use current auth data, not from cookie
		let authentication = this.getAuthentication();
		return !!(authentication && authentication.superUser);
	}

	/**
	 * Deletes the current session from the server and invalidates the session ID.
	 *
	 * @returns {Observable<Response>}
	 */
	logout(): Observable<any> {
		return new Observable<any>(observer => {
			this.http.delete(this.nuclosConfig.getRestHost()).subscribe(
				data => {
					this.$log.debug('Logout successful', data);
					this.invalidateSessionId();
					this.currentUser = undefined;
					this.authentication = undefined;
					observer.next(data);
					observer.complete();
					this.router.navigateByUrl('/login');
					this.setLoggedIn(false);
				},
				error => {
					this.$log.error('Logout failed', error);
					this.invalidateSessionId();
					this.authentication = undefined;
					try {
						observer.error(error);
					} catch (ex) {
					}
					this.router.navigateByUrl('/login');
					this.setLoggedIn(false);
				}
			);
		});
	}

	/**
	 * Possibly saves the given url in the "last location" cookie.
	 * The user is redirected to this location after the next login.
	 *
	 * @param url
	 */
	rememberUrl(url: string) {
		this.$log.debug('Remembering URL: %o', url);

		let key = 'location.' + this.getUsername();
		this.cookieService.put(key, url, {
			expires: new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 365)
		});
	}

	shouldUrlBeRemembered(url: string) {
		return !this.isAnonymous()
			&& this.getUsername()
			&& this.isAuthenticationRequired(url)
			&& !url.endsWith('/new')
			&& !url.endsWith('index.html');
	}

	isLoggedIn(): boolean {
		return this.loggedIn;
	}

	private setLoggedIn(loggedIn: boolean) {
		this.loggedIn = loggedIn;
		this.loginSubject.next(loggedIn);
	}

	observeLoginStatus(): Observable<boolean> {
		return this.loginSubject;
	}

	resetWorkspace() {
		let link = this.nuclosConfig.getRestHost() + '/meta/resetworkspace';
		this.http.get(link).subscribe(
			() => window.location.reload()
		);
	}
}
