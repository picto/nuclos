import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NOT_ACCEPTABLE } from 'http-status-codes';
import { Observable } from 'rxjs';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { NuclosConfigService } from '../shared/nuclos-config.service';
import { SystemParameter } from '../shared/system-parameters';
import { AuthenticationData, AuthenticationService } from './authentication.service';

@Component({
	selector: 'nuc-authentication',
	templateUrl: './authentication.component.html',
	styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

	@Input()
	username = '';

	@Input()
	password: string;

	@Input()
	autologin: boolean;

	@ViewChild('usernameInput') usernameInput;
	@ViewChild('passwordInput') passwordInput;

	showRegistrationLink = false;

	loginFormEnabled = true;
	showMandatorForm = false;
	showMandatorlessSessionButton = false;
	selectedMandatorId;
	loginData: AuthenticationData;

	/**
	 * Should be set to an i18n key, e.g. 'webclient.nosession.error.wrongpass'.
	 */
	errorMessage: string | undefined;

	constructor(
		private route: ActivatedRoute,
		private authenticationService: AuthenticationService,
		private i18nService: NuclosI18nService,
		private configService: NuclosConfigService
	) {
	}

	ngOnInit() {

		// initialize username input with username from last login
		let lastLoginUsername = this.authenticationService.getUsername();
		if (lastLoginUsername) {
			this.username = lastLoginUsername;
		}

		this.route.params.subscribe(
			params => this.i18nService.setLocaleString(params['locale'])
		);

		this.configService.getSystemParameters()
			.subscribe(params => {
				this.showRegistrationLink = params.is(SystemParameter.USER_REGISTRATION_ENABLED);
			});
		setTimeout(() => {
			if (this.usernameInput) {
				// focus username respectively password input
				if (this.username.length === 0) {
					this.usernameInput.nativeElement.focus();
				} else {
					this.passwordInput.nativeElement.focus();
				}
			}
		});
	}

	login() {
		this.errorMessage = undefined;
		let loginData = {
			username: this.username,
			password: this.password,
			locale: this.i18nService.getCurrentLocale().key,
			autologin: this.autologin
		};

		this.authenticationService.login(loginData).catch(error => {
			if (error.status === NOT_ACCEPTABLE) {
				this.errorMessage = 'webclient.nosession.error.wrongpass';
			} else {
				this.errorMessage = 'webclient.nosession.error.nocode';
			}
			return Observable.throw(error);
		}).subscribe((authenticationData) => {

			// show mandator selection dialog if configured
			this.showMandatorlessSessionButton = authenticationData.superUser;
			if (authenticationData.mandators) {
				this.loginData = authenticationData;
				this.selectedMandatorId = authenticationData.mandators[0].mandatorId;
				if (authenticationData.mandators.length === 1) {
					// only one mandator configured no need for mandator selection
					this.authenticationService.navigateHome();
				} else {
					this.loginFormEnabled = false;
					this.showMandatorForm = true;
				}
			}
		});
	}

	selectMandator() {
		this.authenticationService.onAuthenticationDataAvailable().subscribe(() => {
			if (this.loginData.mandators) {
				let mandator = this.loginData.mandators.filter(m => m.mandatorId === this.selectedMandatorId).shift();
				this.authenticationService.selectMandator(mandator).subscribe(
					() => {
						this.loginData.mandator = mandator;

						this.navigateHome();
					}
				);
			}
		});
	}

	mandatorlessSession() {
		this.navigateHome();
	}

	private navigateHome() {
		$('body').css('cursor', 'progress');
		this.authenticationService.navigateHome();

		if (window.location.href.indexOf('/login') === -1) {
			window.location.reload();
		}
	}

	isBusy() {
		return this.authenticationService.isLoginInProgress();
	}
}
