import { Component, OnInit } from '@angular/core';
import { PerspectiveModel } from '../perspective-model';
import { PerspectiveService } from '../shared/perspective.service';

@Component({
	selector: 'nuc-perspective',
	templateUrl: './perspective.component.html',
	styleUrls: ['./perspective.component.css']
})
export class PerspectiveComponent implements OnInit {
	visible = true;

	constructor(
		private perspectiveService: PerspectiveService
	) {
	}

	ngOnInit() {
	}

	getModel(): PerspectiveModel {
		return this.perspectiveService.getModel();
	}

	openNew() {
		this.perspectiveService.newPerspective();
	}

	isNewAllowed() {
		return this.perspectiveService.isNewAllowed();
	}
}
