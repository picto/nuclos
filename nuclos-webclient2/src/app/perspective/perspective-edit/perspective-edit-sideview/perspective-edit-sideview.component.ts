import { Component } from '@angular/core';
import { AbstractPerspectiveEditComponent } from '../abstract-perspective-edit.component';

@Component({
	selector: 'nuc-perspective-edit-sideview',
	templateUrl: './perspective-edit-sideview.component.html',
	styleUrls: ['./perspective-edit-sideview.component.css']
})
export class PerspectiveEditSideviewComponent extends AbstractPerspectiveEditComponent {

	hasSideviewMenuPrefs() {
		return this.model.getSideviewMenuPrefs(this.perspective).length > 0;
	}

}
