import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Logger } from '../../log/shared/logger';
import { IPerspective } from '../perspective';
import { PerspectiveModel } from '../perspective-model';

@Component({
	selector: 'nuc-perspective-edit',
	templateUrl: './perspective-edit.component.html',
	styleUrls: ['./perspective-edit.component.css']
})
export class PerspectiveEditComponent implements OnInit {
	@Input() perspective: IPerspective;
	@Input() model: PerspectiveModel;
	@Input() isNew: boolean;

	@Output() apply = new EventEmitter<IPerspective>();

	constructor(
		private activeModal: NgbActiveModal,
		private $log: Logger
	) {
	}

	ngOnInit() {
	}

	applyPerspective() {
		this.apply.next(this.perspective);
	}

	savePerspective() {
		this.$log.debug('Save perspective: %o', this.perspective);
		this.activeModal.close(this.perspective);
	}

	cancel() {
		this.activeModal.dismiss();
	}

	isValid(): boolean {
		return this.model.isValidPerspective(this.perspective);
	}
}
