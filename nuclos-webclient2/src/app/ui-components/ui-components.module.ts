import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DatechooserModule } from './datechooser/datechooser.module';

@NgModule({
	imports: [
		CommonModule,
		DatechooserModule
	],
	declarations: []
})
export class UiComponentsModule {
}
