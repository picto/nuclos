import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DatechooserComponent } from './datechooser.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		DatechooserComponent
	],
	exports: [
		DatechooserComponent
	]
})
export class DatechooserModule {
}
