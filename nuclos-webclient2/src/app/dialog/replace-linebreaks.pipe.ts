import { Pipe, PipeTransform } from '@angular/core';

/**
 * replace \n linebreaks with <br> tag
 */
@Pipe({name: 'replaceLinebreaks'})
export class ReplaceLinebreaksPipe implements PipeTransform {
	transform(value: string): string {
		let newValue = value.replace(/\n/g, '<br>');
		return `${newValue}`;
	}
}
