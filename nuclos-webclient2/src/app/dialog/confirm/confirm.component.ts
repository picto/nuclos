import { Component } from '@angular/core';
import { ConfirmOptions } from '../dialog.model';
import { DialogState } from '../dialog.service';

@Component({
	selector: 'nuc-confirm-modal-component',
	templateUrl: './confirm.component.html',
	styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent {

	options: ConfirmOptions;

	constructor(private state: DialogState) {
		this.options = state.options;
	}

	yes() {
		this.state.modalRef.close('confirmed');
	}

	no() {
		this.state.modalRef.dismiss('not confirmed');
	}
}
