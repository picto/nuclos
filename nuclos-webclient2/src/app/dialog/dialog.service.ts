import { Injectable, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmOptions } from './dialog.model';


/**
 * An internal service allowing to access, from the confirm modalRef component, the options and the modalRef reference.
 * It also allows registering the TemplateRef containing the confirm modalRef component.
 *
 * It must be declared in the providers of the NgModule, but is not supposed to be used in application code
 */
@Injectable()
export class DialogState {

	options: ConfirmOptions;

	/**
	 * last opened confirmation modalRef
	 */
	modalRef: NgbModalRef;

	/**
	 * ref containing the alert modal component
	 */
	alertTemplateRef: TemplateRef<any>;

	/**
	 * ref containing the confirmation modal component
	 */
	confirmTemplateRef: TemplateRef<any>;
}

/**
 * TODO: Make sure there is never more than 1 dialog opened at a time.
 * Additional dialogs should be queued.
 */
@Injectable()
export class DialogService {

	constructor(
		private modalService: NgbModal,
		private state: DialogState
	) {
	}

	/**
	 * opens a alert modal
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user clicks ok button
	 */
	alert(options: ConfirmOptions): Promise<any> {
		this.state.options = options;
		this.state.modalRef = this.modalService.open(this.state.alertTemplateRef);
		this.focusConfirmationButton();
		return this.state.modalRef.result;
	}

	/**
	 * opens a display modal (similar to alert modal, but larger)
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user clicks ok button
	 */
	display(options: ConfirmOptions): Promise<any> {
		this.state.options = options;
		let nmo: NgbModalOptions = {size: 'lg'};
		this.state.modalRef = this.modalService.open(this.state.alertTemplateRef, nmo);
		this.focusConfirmationButton();
		return this.state.modalRef.result;
	}

	/**
	 * opens a confirm modal
	 * @param options the options for the modal (title and message)
	 * @returns {Promise<any>} a promise that is fulfilled when the user chooses to confirm, and rejected when
	 * the user chooses not to confirm, or closes the modal
	 */
	confirm(options: ConfirmOptions): Promise<any> {
		this.state.options = options;
		this.state.modalRef = this.modalService.open(this.state.confirmTemplateRef);
		this.focusConfirmationButton();
		return this.state.modalRef.result;
	}

	focusConfirmationButton() {
		$('#button-ok').focus();
	}

	hasOpenDialog() {
		// TODO: Track open modal instances instead of testing for a button
		return $('#button-ok:visible').length > 0;
	}
}

