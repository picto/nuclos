import { Directive, TemplateRef } from '@angular/core';
import { DialogState } from '../dialog.service';

@Directive({
	selector: '[nucAlert]'
})
export class AlertDirective {
	constructor(templateRef: TemplateRef<any>, state: DialogState) {
		state.alertTemplateRef = templateRef;
	}
}
