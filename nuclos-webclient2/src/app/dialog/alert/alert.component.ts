import { Component } from '@angular/core';
import { ConfirmOptions } from '../dialog.model';
import { DialogState } from '../dialog.service';

@Component({
	selector: 'nuc-alert-modal-component',
	templateUrl: './alert.component.html',
	styleUrls: ['./alert.component.css']
})
export class AlertComponent {

	options: ConfirmOptions;

	constructor(private state: DialogState) {
		this.options = state.options;
	}

	close() {
		this.state.modalRef.close('confirmed');
	}

}
