import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { InputRequiredDialogComponent } from '../input-required-dialog/input-required-dialog.component';
import { InputRequired } from './model';

/**
 * Handles "Input Required" which can occur whenever an EO is saved.
 * This service does not save the EO itself or some strange callback magic.
 */
@Injectable()
export class InputRequiredService {

	constructor(private modalService: NgbModal) {
	}

	/**
	 * Handles possible InputRequired "errors".
	 * Returns an Observable that handles the InputRequired specification
	 * or just an Observable containing the given error if no InputRequired was found.
	 *
	 * @param error The error that occurred while sending the data. Could contain an InputRequired specification.
	 * @param data The original request data.
	 * @returns {any}
	 */
	handleError(error: Response | any, data: any): Observable<any> {
		if (error instanceof Response) {
			const json = this.responseToJson(error);
			if (json && json.inputrequired) {
				let inputRequired = <InputRequired>json.inputrequired;
				return new Observable(observer => {
					this.handleInputRequired(inputRequired).subscribe(result => {
						// data might have a "inputrequired" attribute from a previous InputRequired error.
						// The previous result must be merged with the current one and sent again.
						let nextInputRequired = data.inputrequired || inputRequired;
						if (!nextInputRequired.result) {
							nextInputRequired.result = {};
						}
						nextInputRequired.result[inputRequired.specification.key] = result;
						data.inputrequired = nextInputRequired;

						// Triggers the original request to be re-sent (with modified data)
						// if this function was called from "retryWhen"
						observer.next(true);
						observer.complete();
					});
				});
			}
		}

		// If no InputRequired was handled, just re-throw the error
		return Observable.throw(error);
	}

	private responseToJson(response: Response) {
		let result;

		try {
			result = response.json();
		} catch (e) {
		}

		return result;
	}

	/**
	 * Handles a concrete InputRequired object.
	 *
	 * @param inputRequired
	 * @returns {any}
	 */
	private handleInputRequired(inputRequired: InputRequired): Observable<any> {
		if (!inputRequired || !inputRequired.specification) {
			return Observable.of(undefined);
		}

		let ngbModalRef = this.modalService.open(
			InputRequiredDialogComponent
		);

		ngbModalRef.componentInstance.specification = inputRequired.specification;

		return Observable.fromPromise(ngbModalRef.result);
	}
}
