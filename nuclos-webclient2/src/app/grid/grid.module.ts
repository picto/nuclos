import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { GridDirective } from './grid.directive';
import { GridHeaderComponent } from './grid-header/grid-header.component';

@NgModule({
	imports: [
		CommonModule,
		AgGridModule
	],
	declarations: [
		GridHeaderComponent,
		GridDirective
	],
	exports: [
		GridDirective
	],
	entryComponents: [
		GridHeaderComponent,
	]
})
export class GridModule {
}
