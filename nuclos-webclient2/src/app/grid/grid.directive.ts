import { Directive } from '@angular/core';
import { ColDef } from 'ag-grid';
import { AgGridNg2 } from 'ag-grid-angular';
import { GridHeaderComponent } from './grid-header/grid-header.component';

@Directive({
	selector: '[nucGrid]'
})
export class GridDirective {

	defaultColDef: ColDef = {
		headerComponentFramework: GridHeaderComponent,
	};

	constructor(grid: AgGridNg2) {
		if (!grid.defaultColDef) {
			grid.defaultColDef = {};
		}

		grid.defaultColDef.headerComponentFramework = GridHeaderComponent;
	}

}
