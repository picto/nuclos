import { Component, ElementRef, NgZone, OnInit } from '@angular/core';
import { BrowserDetectionService } from './shared/browser-detection.service';
import { WindowTitleService } from './entity-object/shared/window-title.service';

@Component({
	selector: 'nuc-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'Nuclos Webclient v2';

	constructor(
		private elementRef: ElementRef,
		private browserDetectionService: BrowserDetectionService,
		private ngZone: NgZone,

		// TODO: title service is only injected here, because angular does not instantiate un-injected services
		private _titleService: WindowTitleService
	) {
		window['NgZone'] = this.ngZone;
	}

	ngOnInit(): void {
		if (this.browserDetectionService.isSafari()) {
			$(this.elementRef.nativeElement).addClass('browser-safari');
		}
	}
}


