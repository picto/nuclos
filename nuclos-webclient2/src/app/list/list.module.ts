import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ListFilterPipe } from './list-filter.pipe';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		ListFilterPipe
	],
	exports: [
		ListFilterPipe
	]
})
export class ListModule {
}
