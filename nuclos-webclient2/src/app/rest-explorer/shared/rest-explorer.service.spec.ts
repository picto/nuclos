/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { RestExplorerService } from './rest-explorer.service';

xdescribe('Service: RestExplorer', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [RestExplorerService]
		});
	});

	it('should ...', inject([RestExplorerService], (service: RestExplorerService) => {
		expect(service).toBeTruthy();
	}));
});
