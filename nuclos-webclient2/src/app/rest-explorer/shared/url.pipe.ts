import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
	name: 'urlpipe'
})
export class UrlPipe implements PipeTransform {

	constructor(private sanitizer: DomSanitizer) {
		window['openRestExplorerUrl'] = (url) => {
			let restExplorerUrl = '#/restexplorer/preview?url=' + url;
			window.location.href = restExplorerUrl;
		};
	}

	transform(value: any): any {
		if (value == null) {
			return null;
		}
		return this.sanitizer.bypassSecurityTrustHtml(
			value.replace(
				/((http|https):\/\/[\w?=&.\/-;#~%-{}]+(?![\w\s?&.\/;#~%"=-]*>))/g,
				'<a onclick=\'window.openRestExplorerUrl("$1");\'>$1</a>'
			)
		);
	}
}
