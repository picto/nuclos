import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ResizableModule } from 'angular-resizable-element';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { RestExplorerComponent } from '.';
import { ListModule } from '../list/list.module';
import { LogModule } from '../log/log.module';
import { MenuModule } from '../menu/menu.module';
import { RestExplorerDetailComponent } from './rest-explorer-detail';
import { RestExplorerNavigationComponent } from './rest-explorer-navigation/rest-explorer-navigation.component';
import { RestExplorerRoutes } from './rest-explorer.routes';
import { RestExplorerService } from './shared';
import { UrlPipe } from './shared/url.pipe';
import { I18nModule } from '../i18n/i18n.module';

@NgModule({
	imports: [
		CommonModule,
		RouterModule,
		FormsModule,
		ResizableModule,
		RestExplorerRoutes,
		MenuModule,
		ListModule,
		PrettyJsonModule,
		I18nModule,
		LogModule
	],
	declarations: [
		UrlPipe,
		RestExplorerComponent,
		RestExplorerNavigationComponent,
		RestExplorerDetailComponent
	],
	exports: [
		UrlPipe
	],
	providers: [
		RestExplorerService
	]
})
export class RestExplorerModule {
}
