import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IBusinessTestVO } from '../../../../nuclos_typings/nuclos/businesstest';
import { DialogService } from '../../dialog/dialog.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { Logger } from '../../log/shared/logger';
import { BusinesstestLogHandler, BusinesstestService } from '../shared/businesstest.service';

@Component({
	selector: 'nuc-businesstest-advanced',
	templateUrl: './businesstest-advanced.component.html',
	styleUrls: ['./businesstest-advanced.component.css']
})
export class BusinesstestAdvancedComponent implements OnInit {

	progress = 0;
	started = false;
	executionResult = '';
	tests: Array<IBusinessTestVO> = [];
	logHandler: BusinesstestLogHandler;

	constructor(
		private businesstestService: BusinesstestService,
		private $log: Logger,
		private dialogService: DialogService,
		private nuclosI18n: NuclosI18nService
	) {
		this.logHandler = {
			start: () => {
				this.started = true;
				this.progress = 0;
			},
			receiveMessage: message => {
				this.executionResult += message;
				let output = $('#output');
				if (output.length) {
					output.scrollTop(output[0].scrollHeight - output.height());
				}
			},
			receiveProgress: progress => {
				this.progress = progress;
			},
			done: () => {
				this.started = false;
				this.progress = 100;
			},
			fail: () => {
				this.started = false;
			}
		};
	}

	ngOnInit() {
		this.businesstestService.getTests().subscribe(tests => this.tests = tests);
	}

	/**
	 * Sets the "started" state of this component, subscribes to the given observable and resets the state afterwards.
	 * @param observable
	 * @returns {Subscription}
	 */
	private start(observable: Observable<any>): Subscription {
		this.executionResult = '';
		this.started = true;
		return observable.subscribe({
			complete: () => {
				this.started = false;
				this.updateTests();
			},
			error: (error) => {
				this.$log.error(error);
				this.started = false;
			}
		});
	}

	generateAllTests(): Subscription {
		return this.start(this.businesstestService.generateAllTests(this.logHandler));
	};

	runAllTests(): Subscription {
		return this.start(this.businesstestService.runAllTests(this.logHandler));
	};

	private updateTests() {
		this.businesstestService.updateTests().subscribe(tests => this.tests = tests);
	}

	deleteTest(testId: string): Subscription {
		this.$log.debug('Deleting test ' + testId + '...');
		return this.businesstestService.deleteTest(testId).subscribe(
			result => {
				this.$log.debug('Test deleted: %o', result);
				$('#test_' + testId).remove();
			},
			error => {
				this.$log.error(error);
			}
		);
	};

	deleteAllTests(): void {
		this.dialogService.confirm({
			title: this.nuclosI18n.getI18n('webclient.businesstests.delete.all'),
			message: this.nuclosI18n.getI18n('webclient.businesstests.delete.all.confirm')
		}).then(
			() => {
				this.$log.debug('Deleting all tests...');
				this.businesstestService.deleteAllTests().subscribe(
					() => this.updateTests()
				);
			}
		);
	};
}
