import { Injectable } from '@angular/core';
import { Headers, Response, ResponseContentType } from '@angular/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { BusinesstestOverallResult, IBusinessTestVO } from '../../../../nuclos_typings/nuclos/businesstest';
import { Logger } from '../../log/shared/logger';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';

declare var oboe;

export interface BusinesstestLogHandler {
	start: () => void;
	receiveMessage: (message: string) => void;
	receiveProgress: (progress: number) => void;
	done: () => void;
	fail: (error: any) => void;
}

@Injectable()
export class BusinesstestService {

	overallResult: Subject<BusinesstestOverallResult | null>;
	tests: Subject<Array<IBusinessTestVO>>;

	constructor(
		private http: NuclosHttpService,
		private nuclosConfig: NuclosConfigService,
		private $log: Logger
	) {
		this.overallResult = new BehaviorSubject<BusinesstestOverallResult | null>(null);
		this.tests = new BehaviorSubject<Array<IBusinessTestVO>>([]);

		this.updateOverallResult().subscribe();
		this.updateTests().subscribe();
	}

	getOverallResult(): Observable<BusinesstestOverallResult | null> {
		return this.overallResult;
	}

	setOverallResult(overallResult: BusinesstestOverallResult | null) {
		this.overallResult.next(overallResult);
	}

	/**
	 * Fetches the overall result from the server.
	 *
	 * @returns {Observable<BusinesstestOverallResult>}
	 */
	updateOverallResult(): Observable<BusinesstestOverallResult> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/businesstests/status'
		)
			.map((response: Response) => response.json())
			.do(data => this.overallResult.next(data));
	}

	getTests(): Observable<Array<IBusinessTestVO>> {
		return this.tests;
	}

	/**
	 * Fetches a single test with the given ID.
	 */
	getTest(testId: string): Observable<IBusinessTestVO> {
		return this.http.get(
			this.nuclosConfig.getRestHost() + '/businesstests/' + testId,
			{
				responseType: ResponseContentType.Json,
				headers: new Headers(
					{
						accept: 'application/json'
					}
				)
			}
		).map(
			(response: Response) => response.json()
		);
	}

	/**
	 * Deletes the given test on the server.
	 *
	 * @param testId
	 * @returns {Observable<R>}
	 */
	deleteTest(testId: string): Observable<any> {
		return this.http.delete(
			this.nuclosConfig.getRestHost() + '/businesstests/' + testId,
			{withCredentials: true}
		);
	}

	/**
	 * Deletes all tests on the server.
	 *
	 * @returns {Observable<R>}
	 */
	deleteAllTests(): Observable<any> {
		return this.http.delete(
			this.nuclosConfig.getRestHost() + '/businesstests/',
			{withCredentials: true}
		);
	}

	/**
	 * Inserts the given test.
	 */
	createNew(test: IBusinessTestVO): Observable<IBusinessTestVO> {
		this.$log.debug('Saving new test: %o', test);
		return this.http.post(this.nuclosConfig.getRestHost() + '/businesstests/',
			test,
			{
				responseType: ResponseContentType.Json,
				headers: new Headers(
					{
						accept: 'application/json'
					}
				)
			}
		).map(
			(response: Response) => response.json()
		);
	};

	/**
	 * Saves (updates) the given test.
	 *
	 * @param test
	 */
	save(test: IBusinessTestVO): Observable<IBusinessTestVO> {
		return this.http.put(this.nuclosConfig.getRestHost() + '/businesstests/' + test.id.string,
			test,
			{
				responseType: ResponseContentType.Json,
				headers: new Headers(
					{
						accept: 'application/json'
					}
				)
			}
		).map(
			(response: Response) => response.json()
		);
	}


	/**
	 * Fetches all tests anew from the server.
	 *
	 * @returns {Observable<Array<IBusinessTestVO>>}
	 */
	updateTests(): Observable<Array<IBusinessTestVO>> {
		return new Observable<Array<IBusinessTestVO>>(observer => {
			this.http.get(
				this.nuclosConfig.getRestHost() + '/businesstests/',
				{
					responseType: ResponseContentType.Json,
					headers: new Headers(
						{
							accept: 'application/json'
						}
					)
				}
			)
				.map((response: Response) => response.json())
				.subscribe(data => {
					this.tests.next(data);
					observer.next(data);
					observer.complete();
				}, error => {
					this.$log.warn('Unable to get business tests: %o', error);
					observer.error(error);
				});
		});
	}

	/**
	 * Generates all tests.
	 *
	 * @param logHandler
	 * @returns {Observable<any>}
	 */
	generateAllTests(logHandler: BusinesstestLogHandler): Observable<any> {
		return this.runOboe(this.nuclosConfig.getRestHost() + '/businesstests/generate', logHandler);
	}

	/**
	 * Runs all tests and afterwards updates the overall result.
	 *
	 * @param logHandler
	 * @returns {Observable<R>|Observable<[T,T2,T3]>}
	 */
	runAllTests(logHandler: BusinesstestLogHandler): Observable<any> {
		this.setOverallResult(null);

		return Observable.concat(
			this.runOboe(this.nuclosConfig.getRestHost() + '/businesstests/run', logHandler),
			this.updateOverallResult(),
			this.updateTests()
		);
	}

	/**
	 * Runs the test with the given test ID.
	 *
	 * @param logHandler
	 * @returns {Observable<R>|Observable<[T,T2,T3]>}
	 */
	runTest(logHandler: BusinesstestLogHandler, testId: string): Observable<any> {
		return this.runOboe(this.nuclosConfig.getRestHost() + '/businesstests/run/' + testId, logHandler);
	}

	/**
	 * Runs oboe on the given URL and streams the log messages and progress to the given handler.
	 * The returned Observable emits 'true' when finished.
	 *
	 * @param url
	 * @returns {Observable}
	 */
	runOboe(url: string, logHandler: BusinesstestLogHandler): Observable<boolean> {
		return new Observable<boolean>(observer => {
			logHandler.start();
			oboe({
				url: url,
				withCredentials: true
			})
				.node('!.{message}', element => {
						let message = element.message;
						logHandler.receiveMessage(message);
					}
				)
				.node('!.{progress}', element => {
						let progress = Math.round(element.progress * 100);
						logHandler.receiveProgress(progress);
					}
				)
				.done(() => {
					logHandler.done();
					observer.next(true);
					observer.complete();
				})
				.fail((error) => {
					logHandler.fail(error);
					observer.error(error);
				});
		});
	}
}
