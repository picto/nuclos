import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AceEditorComponent, AceEditorDirective } from 'ng2-ace-editor';
import { DialogModule } from '../dialog/dialog.module';
import { I18nModule } from '../i18n/i18n.module';
import { LogModule } from '../log/log.module';
import { BusinesstestAdvancedComponent } from './businesstest-advanced/businesstest-advanced.component';
import { BusinesstestDetailComponent } from './businesstest-detail/businesstest-detail.component';
import { BusinesstestOverviewComponent } from './businesstest-overview/businesstest-overview.component';
import { BusinesstestComponent } from './businesstest.component';
import { BusinesstestRoutesModule } from './businesstest.routes';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { BusinesstestService } from './shared/businesstest.service';

@NgModule({
	imports: [
		FormsModule,
		CommonModule,
		RouterModule,

		LogModule,
		I18nModule,
		DialogModule,

		BusinesstestRoutesModule
	],
	declarations: [
		AceEditorComponent,
		AceEditorDirective,
		BusinesstestComponent,
		BusinesstestOverviewComponent,
		BusinesstestDetailComponent,
		BusinesstestAdvancedComponent,
		ProgressBarComponent
	],
	providers: [
		BusinesstestService
	]
})
export class BusinesstestModule {
}
