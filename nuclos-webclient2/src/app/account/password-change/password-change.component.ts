import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { AuthenticationService } from '../../authentication/authentication.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { HasMessage } from '../../shared/has-message';
import { NuclosAccountService } from '../shared/nuclos-account.service';
import { UserPassword } from './password';

@Component({
	selector: 'nuc-password-change',
	templateUrl: './password-change.component.html',
	styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent extends HasMessage implements OnInit {
	passwordItem: UserPassword;
	showForm;

	constructor(
		private accountService: NuclosAccountService,
		private authenticationService: AuthenticationService,
		i18n: NuclosI18nService
	) {
		super(i18n);
	}

	ngOnInit() {
		this.authenticationService.observeLoginStatus().subscribe(loggedIn => {
			if (!loggedIn) {
				this.showForm = false;
				return;
			}

			let user = this.authenticationService.getCurrentUser();
			if (user) {
				this.passwordItem = {
					userName: user.username
				};
				this.showForm = true;
			}
		});
	}

	changePassword() {
		this.clearMessage();

		let valid = this.validatePassword();
		if (!valid) {
			return;
		}

		this.accountService.changePassword(this.passwordItem).subscribe(
			() => {
				this.setMessage(
					'success',
					'webclient.user.changepassword.changed',
					''
				);
				this.showForm = false;
			},
			error => this.showErrorFromResponse('webclient.account.error', error)
		);
	}

	showErrorFromResponse(titleKey: string, error: Response) {
		if (error.status === 403) {
			this.showError(
				titleKey,
				'webclient.user.changepassword.passwordwrong'
			);
		} else if (error.status === 406) {
			this.showError(
				titleKey,
				'webclient.user.changepassword.passwordnotsuitable'
			);
		} else {
			super.showErrorFromResponse(
				titleKey,
				error
			);
		}
	}

	private validatePassword(): boolean {
		if (!this.passwordItem.newPassword) {
			this.showError(
				'webclient.account.error',
				'webclient.user.changepassword.empty'
			);
			return false;
		}

		if (this.passwordItem.newPassword !== this.passwordItem.confirmNewPassword) {
			this.showError(
				'webclient.account.error',
				'webclient.user.changepassword.nomatch'
			);
			return false;
		}

		return true;
	}
}
