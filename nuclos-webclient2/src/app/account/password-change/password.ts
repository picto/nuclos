export interface UserPassword {
	userName: string;
	oldPassword?: string;
	newPassword?: string;
	confirmNewPassword?: string;
}
