import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { I18nModule } from '../i18n/i18n.module';
import { AccountRoutesModule } from './account.routes';
import { ActivationComponent } from './activation/activation.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { RegistrationComponent } from './registration/registration.component';
import { NuclosAccountService } from './shared/nuclos-account.service';
import { BusyModule } from 'angular2-busy';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,

		I18nModule,
		BusyModule,

		AccountRoutesModule
	],
	declarations: [RegistrationComponent, ActivationComponent, PasswordChangeComponent],
	providers: [
		NuclosAccountService
	]
})
export class AccountModule {
}
