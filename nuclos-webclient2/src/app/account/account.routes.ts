import { RouterModule, Routes } from '@angular/router';
import { ActivationComponent } from './activation/activation.component';
import { PasswordChangeComponent } from './password-change/password-change.component';
import { RegistrationComponent } from './registration/registration.component';

export const ROUTE_CONFIG: Routes = [
	{
		path: 'account/register',
		component: RegistrationComponent
	},
	{
		path: 'account/activate/:username/:activationCode',
		component: ActivationComponent
	},
	{
		path: 'account/changePassword',
		component: PasswordChangeComponent
	},
];

export const AccountRoutesModule = RouterModule.forChild(ROUTE_CONFIG);
