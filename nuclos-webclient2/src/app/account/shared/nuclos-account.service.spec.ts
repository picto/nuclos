import { inject, TestBed } from '@angular/core/testing';
import { NuclosAccountService } from './nuclos-account.service';

describe('NuclosAccountService', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [NuclosAccountService]
		});
	});

	it('should ...', inject([NuclosAccountService], (service: NuclosAccountService) => {
		expect(service).toBeTruthy();
	}));
});
