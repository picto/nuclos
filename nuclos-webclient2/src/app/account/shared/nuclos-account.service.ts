import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { AccountData } from './account-data';
import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable()
export class NuclosAccountService {

	constructor(
		private config: NuclosConfigService,
		private http: NuclosHttpService,
		private authService: AuthenticationService
	) {
	}

	create(account: AccountData): Observable<any> {
		return this.http.post(this.config.getRestHost() + '/admin/account', account);
	}

	activate(username: any, activationCode: any): Observable<any> {
		// Activation is only possible after anonymous login... (NUCLOS-5614)
		return this.authService.waitForLogin().take(1).skip(1)
			.concat(
				this.http.get(
					this.config.getRestHost() + '/admin/account/activate/' + username + '/' + activationCode
				)
			);
	}

	changePassword(passwordItem: any) {
		let url = this.config.getRestHost() + '/user/' + passwordItem.userName + '/password';

		return this.http.put(
			url, passwordItem
		);
	}
}
