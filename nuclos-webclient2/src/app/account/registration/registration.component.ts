import { Component, OnInit } from '@angular/core';
import { DisclaimerService, LegalDisclaimer } from '../../disclaimer/shared/disclaimer.service';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { HasMessage } from '../../shared/has-message';
import { AccountData } from '../shared/account-data';
import { NuclosAccountService } from '../shared/nuclos-account.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'nuc-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})
export class RegistrationComponent extends HasMessage implements OnInit {

	account: AccountData;

	showForm = true;

	private privacyDisclaimer: LegalDisclaimer | undefined;

	private busy: Subscription;

	constructor(
		private accountService: NuclosAccountService,
		private disclaimerService: DisclaimerService,
		i18n: NuclosI18nService
	) {
		super(i18n);

		this.account = {};
	}

	ngOnInit() {
		this.disclaimerService.getPrivacyDisclaimer().subscribe(
			disclaimer => this.privacyDisclaimer = disclaimer
		);
	}

	onSubmit() {
		if (this.hasDisclaimer() && !this.account.privacyconsent) {
			this.setMessage(
				'danger',
				'webclient.account.privacyconsent2',
				'webclient.account.noprivacyconsent'
			);
			return;
		}

		this.busy = this.accountService.create(this.account)
			.subscribe(
				() => {
					this.showSuccessMessage();
					this.showForm = false;
				},
				error => this.showErrorFromResponse('webclient.account.error', error)
			);
	}

	isBusy() {
		return this.busy;
	}

	hasDisclaimer() {
		return this.privacyDisclaimer;
	}

	showDisclaimer() {
		if (this.privacyDisclaimer) {
			this.disclaimerService.showDisclaimer(this.privacyDisclaimer);
		}
	}

	private showSuccessMessage() {
		this.setMessage(
			'success',
			'webclient.account.successful.registered',
			'webclient.account.you.get.an.email.to.activate.your.account'
		);
	}
}
