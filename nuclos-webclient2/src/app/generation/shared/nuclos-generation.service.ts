import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { EntityObject } from '../../entity-object/shared/entity-object.class';
import { InputRequiredService } from '../../input-required/shared/input-required.service';
import { Logger } from '../../log/shared/logger';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { GenerationResultComponent } from '../generation-result/generation-result.component';
import { Generation, GenerationResult } from './generation';
import { NuclosI18nService } from '../../i18n/shared/nuclos-i18n.service';
import { DialogService } from '../../dialog/dialog.service';

@Injectable()
export class NuclosGenerationService {

	constructor(
		private http: NuclosHttpService,
		private inputRequired: InputRequiredService,
		private nuclosI18nService: NuclosI18nService,
		private dialogService: DialogService,
		private modalService: NgbModal,
		private $log: Logger
	) {
	}

	/**
	 * Executes the given generation.
	 * Possibly opens the result in the given target window (or else closes it).
	 *
	 * TODO: Additional window params?
	 * TODO: Window should only be opened after the generation is complete
	 * TODO: Handle errors
	 *
	 * @param generation
	 * @param targetWindow
	 * @returns {Observable<T>}
	 */
	generateEO(
		sourceEO: EntityObject,
		generation: Generation,
		popupparameter?: string
	): Observable<GenerationResult> {

		let postData = {};
		return this.http.post(generation.links.generate.href, postData)
			.map(response => response.json())
			.do((result: GenerationResult) => {
				if (!result.complete) {
					result.bo.businessError = result.businessError;
					localStorage.setItem('' + result.bo.temporaryId, JSON.stringify(result.bo));
				}

				this.showResult(result, popupparameter);

				if (result.refreshSource) {
					sourceEO.reload().subscribe(
						eo => eo.select()
					);
				}
			}).retryWhen(
				// Retry until all InputRequired exceptions are handled
				errors => errors.flatMap(
					error => this.inputRequired
						.handleError(error, postData)
						.catch(e => this.handleError(error))
				)
			);
	}


	private handleError(error: Response | any) {
		let errorMessage = error.json().message;
		if (errorMessage) {
			this.dialogService.alert(
					{
						title: this.nuclosI18nService.getI18n('webclient.error.title'),
						message: errorMessage
					}
			);
		}
		return Observable.throw(errorMessage);
	}

	private showResult(result: GenerationResult, popupparameter?: string) {
		if (result.showGenerated || !result.complete) {
			let url = window.location.href;
			let eoLink = url.substring(0, url.indexOf('#'))
				+ (popupparameter ? '#/popup/' : '#/view/')
				+ result.bo.boMetaId
				+ '/' + (result.bo.boId || result.bo.temporaryId)
				// + "?" + nuclos.detail.EXPAND_SIDEVIEW_PARAM
				// + "&" + nuclos.detail.SHOW_CLOSE_ON_SAVE_BUTTON_PARAM
				+ '&refreshothertabs';

			let newWindow = window.open(eoLink, '_blank', popupparameter);
			if (!newWindow) {
				this.showResultModal(result, eoLink);
			}
		}
	}

	private showResultModal(result: GenerationResult, eoLink: string) {
		let ngbModalRef = this.modalService.open(
			GenerationResultComponent
		);

		ngbModalRef.componentInstance.generationResult = result;
		ngbModalRef.componentInstance.targetURL = eoLink;

		ngbModalRef.result.then(res => {
			this.$log.debug('Result: %o', res);
		});
	}
}
