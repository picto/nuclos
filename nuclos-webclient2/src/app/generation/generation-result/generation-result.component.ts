import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GenerationResult } from '../shared/generation';

@Component({
	selector: 'nuc-generation-result',
	templateUrl: './generation-result.component.html',
	styleUrls: ['./generation-result.component.css']
})
export class GenerationResultComponent implements OnInit {
	@Input() generationResult: GenerationResult;
	@Input() targetURL: string;

	constructor(private activeModal: NgbActiveModal) {
	}

	ngOnInit() {
	}

	close() {
		this.activeModal.dismiss();
	}
}
