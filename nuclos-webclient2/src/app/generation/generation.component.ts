import { Component, Input, OnInit } from '@angular/core';
import { DialogService } from '../dialog/dialog.service';
import { EntityObject } from '../entity-object/shared/entity-object.class';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { Logger } from '../log/shared/logger';
import { Generation } from './shared/generation';
import { NuclosGenerationService } from './shared/nuclos-generation.service';

@Component({
	selector: 'nuc-generation',
	templateUrl: './generation.component.html',
	styleUrls: ['./generation.component.css']
})
export class GenerationComponent implements OnInit {
	@Input() eo: EntityObject;

	constructor(
		private $log: Logger,
		private generationService: NuclosGenerationService,
		private nuclosI18n: NuclosI18nService,
		private dialog: DialogService
	) {
	}

	ngOnInit() {
	}

	generate(generation: Generation) {
		if (!generation) {
			return;
		}

		this.eo.getMeta().subscribe(meta => {
			let title = this.nuclosI18n.getI18n(
				'webclient.dialog.generate.header',
				generation.target
			);

			let message = this.nuclosI18n.getI18n(
				'webclient.dialog.generate.message',
				meta.getEntityName(),
				generation.target
			);

			this.dialog.confirm({
				title: title,
				message: message
			}).then(() => {
				this.$log.debug('Generate %o', generation);
				this.generationService.generateEO(this.eo, generation).subscribe();
			}).catch(() => this.$log.debug('Generation aborted'));
		});
	}
}
