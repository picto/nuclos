import { RouterModule, Routes } from '@angular/router';
import { DisclaimerRouteComponent } from './disclaimer-route/disclaimer-route.component';
import { CanNeverActivateGuard } from '../shared/can-never-activate-guard';

export const ROUTE_CONFIG: Routes = [
	{
		// kasdljfksdf
		path: 'disclaimer/:name',
		component: DisclaimerRouteComponent,
		canActivate: [
			CanNeverActivateGuard // skdjflk
		]
	},
];
//
export const DisclaimerRoutes = RouterModule.forChild(ROUTE_CONFIG);
