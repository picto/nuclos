import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DialogService } from '../../dialog/dialog.service';
import { NuclosConfigService } from '../../shared/nuclos-config.service';
import { NuclosHttpService } from '../../shared/nuclos-http.service';
import { Logger } from '../../log/shared/logger';


export interface LegalDisclaimer {
	name: string;
	text: string;
}

export class LegalDisclaimers {
	disclaimers: LegalDisclaimer[];

	constructor(disclaimers: LegalDisclaimer[]) {
		this.disclaimers = disclaimers;
	}
}

@Injectable()
export class DisclaimerService {

	constructor(
		private configService: NuclosConfigService,
		private http: NuclosHttpService,
		private dialogService: DialogService,
		private $log: Logger
	) {
	}

	getLegalDisclaimers(): Observable<LegalDisclaimers> {
		let link = this.configService.getRestHost() + '/version/legaldisclaimer';

		return this.http.getCachedJSON(
			link,
			json => new LegalDisclaimers(json)
		);
	}

	showDisclaimerByName(disclaimerName: string) {
		this.$log.debug('Trying to show disclaimer %o...', disclaimerName);
		this.findDisclaimerByName(disclaimerName).subscribe(disclaimer => {
			if (disclaimer) {
				this.showDisclaimer(disclaimer);
			} else {
				this.$log.warn('Unknown disclaimer name: %o', disclaimerName);
			}
		});
	}

	private findDisclaimerByName(disclaimerName: string) {
		return this.getLegalDisclaimers().map(disclaimers => {
			return disclaimers.disclaimers.find(disclaimer => disclaimer.name === disclaimerName);
		});
	}

	showDisclaimer(disclaimer: LegalDisclaimer) {
		this.$log.debug('Showing disclaimer %o...', disclaimer);
		this.dialogService.display({
			title: disclaimer.name,
			message: disclaimer.text
		});
	}

	getPrivacyDisclaimer(): Observable<LegalDisclaimer | undefined> {
		return this.getLegalDisclaimers().map(
			disclaimers => disclaimers.disclaimers.find(
				disclaimer => disclaimer.name === 'Datenschutz'
			)
		);
	}
}
