import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { EntityObject } from '../entity-object/shared/entity-object.class';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { NuclosStateService } from './shared/nuclos-state.service';
import { StateInfo } from './shared/state';
import { EntityObjectResultService } from '../entity-object/shared/entity-object-result.service';
import { EntityObjectEventListener } from '../entity-object/shared/entity-object-event-listener';

@Component({
	selector: 'nuc-state',
	templateUrl: './state.component.html',
	styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit, OnChanges {
	@Input() eo: EntityObject;

	currentAndNextStates: StateInfo[] = [];

	private eoListener: EntityObjectEventListener = {
		afterSave: () => this.updateStates(),
		afterReload: () => this.updateStates()
	};

	constructor(
		protected i18n: NuclosI18nService,
		protected stateService: NuclosStateService,
		private eoResultService: EntityObjectResultService
	) {
	}

	ngOnInit() {
	}

	ngOnChanges(changes: SimpleChanges): void {
		let eoChange = changes['eo'];
		if (eoChange) {
			if (eoChange.previousValue) {
				let previousEo: EntityObject = eoChange.previousValue;
				previousEo.removeListener(this.eoListener);
			}
			this.eo.addListener(this.eoListener);
			this.updateStates();
		}
	}

	/**
	 * Sets the given state on the current EO and saves it.
	 *
	 * @param state
	 */
	changeState(stateInfo: StateInfo) {
		this.stateService.confirmStateChange(this.eo, stateInfo).subscribe(() => {
			this.stateService.changeState(this.eo, stateInfo).subscribe(() => {
				this.eoResultService.selectEo(this.eo);
			});
		});
	}

	private updateStates() {
		let states: StateInfo[] = [];
		states.push(...this.eo.getNextStates());
		let currentState = this.eo.getState();
		if (currentState && currentState.nuclosStateId) {
			states.push(
				{
					currentState: true,
					nuclosStateId: currentState.nuclosStateId,
					name: currentState.name,
					number: currentState.number,
					buttonLabel: currentState.name,
					nonstop: false,
					links: currentState.links
				}
			);
		}
		states.sort((a, b) => a.number - b.number);
		this.currentAndNextStates = states;
	}
}
