import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { Operator } from './searchtemplate.model';
import { SearchtemplateService } from './searchtemplate.service';
import { EntityObject } from '../entity-object/shared/entity-object.class';
import { BoAttr, EntityMeta, InputType, LovEntry } from '../entity-object/shared/bo-view.model';
import { MetaService } from '../entity-object/shared/meta.service';
import { FqnService } from '../shared/fqn.service';
import { LovDataService } from '../entity-object/shared/lov-data.service';
import { EntityObjectEventService } from '../entity-object/shared/entity-object-event.service';
import { DatetimeService } from '../shared/datetime.service';
import { Logger } from '../log/shared/logger';
import { SelectableService } from '../entity-object/shared/selectable.service';
import { EntityObjectPreferenceService } from '../entity-object/shared/entity-object-preference.service';
import { Preference, SearchtemplateAttribute, SearchtemplatePreferenceContent } from '../preferences/preferences.model';


export interface OperatorDefinitions {
	[ index: string ]: Operator[];
}

@Component({
	selector: 'nuc-searchtemplate',
	templateUrl: './searchtemplate.component.html',
	styleUrls: ['./searchtemplate.component.css']
})
export class SearchtemplateComponent implements OnInit {

	private eo: EntityObject | undefined;

	operatorDefinitions: OperatorDefinitions;
	textSearch: string;
	datePattern: string;

	lovValues: LovEntry[];

	@Input()
	meta: EntityMeta;

	@Output()
	onSearch: EventEmitter<string> = new EventEmitter<string>();


	/**
	 * provides text input changes for debounced search
	 */
	private textSearchUpdated: Subject<string> = new Subject<string>();

	/**
	 * provides changes of searchtemplate for debounced search / pref save
	 */
	private searchtemplateUpdated: Subject<number> = new Subject<number>();

	constructor(
		private metaService: MetaService,
		private fqnService: FqnService,
		private selectableService: SelectableService,
		private searchtemplateService: SearchtemplateService,
		private lovDataService: LovDataService,
		private eoEventService: EntityObjectEventService,
		private eoPreferenceService: EntityObjectPreferenceService,
		private datetimeService: DatetimeService,
		private $log: Logger
	) {
		this.operatorDefinitions = this.searchtemplateService.getOperatorDefinitions();
		this.datePattern = this.datetimeService.getDatePattern();
		this.searchtemplateService.updateSearchInputText('');
	}

	getOperatorDefinitions(inputType: any) {
		return this.operatorDefinitions[inputType];
	}

	ngOnInit() {

		this.eoEventService.observeSelectedEo().subscribe(
			eo => this.eo = eo
		);


		// debounce search input

		this.textSearchUpdated.asObservable()
			.debounceTime(400)
			.distinctUntilChanged().subscribe(
			(searchInputText) => {

				this.searchtemplateService.updateSearchInputText(searchInputText);

				// notify EntityObjectCompoent to execute search
				this.onSearch.emit(searchInputText);

				this.scrollSidebarToTop();
			}
		);

		this.searchtemplateUpdated.asObservable()
			.debounceTime(400)
			.distinctUntilChanged().subscribe(
			() => {

				// notify EntityObjectCompoent to execute search
				// this.onSearch.emit(this.selectedSearchtemplatePreference);
				this.onSearch.emit();

				this.scrollSidebarToTop();

				// save preference

				// strip out temporary data
				let prefCopy = JSON.parse(JSON.stringify(this.getSelectedSearchtemplatePreference()));
				prefCopy.content.columns.forEach(col => {
					delete col.searchPopoverOpen;
					delete col.inputType;
					delete col.formattedValue;
				});

				this.selectableService.savePreference(prefCopy).subscribe();
			}
		);
	}

	getSelectedSearchtemplatePreference(): Preference<SearchtemplatePreferenceContent> | undefined {
		let result = this.eoPreferenceService.getSelectedSearchtemplatePreference();
		// this.$log.debug('searchtemplate = %o', result && result.name);
		return result;
	}


	doTextSearch(): void {
		this.textSearchUpdated.next(this.textSearch);
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		return this.searchtemplateService.getOperatorDefinition(attribute);
	}

	/**
	 * define which search input component will be used for which attribute
	 */
	getSearchComponent(attribute: SearchtemplateAttribute): string {
		let inputType = this.getInputType(attribute);
		if (inputType === InputType.BOOLEAN) {
			return 'boolean';
		} else if (inputType === InputType.REFERENCE && (!attribute.operator || attribute.operator === '=')) {
			return 'lov';
		} else if (inputType === InputType.DATE) {
			return 'datepicker';
		}
		return 'string';
	}

	getOperatorLabel(attribute: SearchtemplateAttribute): string | undefined {
		if (attribute.inputType !== undefined) {
			let operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
			if (operatorDef) {
				return operatorDef.label;
			}
		}
		return undefined;
	}


	showOperator(attribute: SearchtemplateAttribute, operatorDef: Operator): boolean {
		return !(!attribute.nullable && operatorDef.isUnary)
			&& !(attribute.boAttrName === 'nuclosStateNumber' || attribute.boAttrName === 'nuclosState')
	}


	togglePopover(attribute: SearchtemplateAttribute): void {

		let searchtemplate = this.getSelectedSearchtemplatePreference();
		if (!searchtemplate) {
			return;
		}
		/*
		 toggle this popover state
		 close other popovers
		 */
		searchtemplate.content.columns.forEach(attr => {
			if (attr.boAttrId === attribute.boAttrId) {
				attribute.searchPopoverOpen = !attribute.searchPopoverOpen;

				// select first operator if no operator is chosen (first time opened)
				if (attribute.operator === undefined) {
					attribute.operator = '=';
				}
			} else {
				delete attr.searchPopoverOpen;
			}
		});

		// initialize datepicker
		if (this.getInputType(attribute) === InputType.DATE && attribute.value) {
			attribute.datepickerValue = this.datetimeService.buildNgbDateStruct(attribute.value);
		}

		/*
		 place popover directly under the open button,
		 except popover was called from attribute selection checkbox, then place it right to attribute selection panel
		 */
		let button = $('#button-' + attribute.boAttrId);
		if (button.offset() !== undefined) {
			let popover = $('.searchfilter-attribute-popover');
			popover.css('left', button.offset().left + 'px');
			popover.css('top', (button.offset().top + button.outerHeight()) + 'px');
		}

		// focus text input
		window.setTimeout(() => {
			$('#searchfilter-value').focus();
		});

	}


	inputSearchValue(attribute: SearchtemplateAttribute): void {
		// TODO
		// if (attribute.value !== undefined && attribute.value.id !== undefined) {
		// 	// empty dropdown selection
		// 	attribute.value = undefined;
		// }

		attribute.enableSearch = true;

		if (attribute.datepickerValue) {
			attribute.value = new Date(
				attribute.datepickerValue.year,
				attribute.datepickerValue.month,
				attribute.datepickerValue.day
			).toISOString();
		}

		let operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
		if (operatorDef) {
			if (operatorDef.isUnary) {
				attribute.value = '';
			} else {
				window.setTimeout(() => {
					$('#searchfilter-value').focus();
				});
			}
		}

		if (operatorDef) {
			this.searchtemplateService.formatValue(attribute, operatorDef);
		}

		// clear input if reference was selected before in lov and then a 'like' search was called
		if (attribute.reference && operatorDef && operatorDef.operator === 'like'
			&& attribute && attribute.value && attribute && attribute.value.id) {
			delete attribute.value;
		}

		this.doSearch(true, true);
	}


	doSearch(doSavePreferences: boolean, selectFirstFoundEntry: boolean, validate = true): void {

		this.$log.debug('doSearch(%o, %o, %o)', doSavePreferences, selectFirstFoundEntry, validate);

		// TODO selectFirstFoundEntry ?

		let searchtemplate = this.getSelectedSearchtemplatePreference();
		if (searchtemplate) {

			if (validate) {
				this.validateSearchForm(searchtemplate.content);
			}

			if (doSavePreferences) {
				this.searchtemplateUpdated.next(new Date().getTime());
			}
		}
	}


	private validateSearchForm(searchtemplate: SearchtemplatePreferenceContent): void {
		if (!searchtemplate) {
			this.$log.warn('No searchtemplate given');
			return;
		}

		this.$log.debug('Validating searchtemplate: %o with %o attributes', searchtemplate, searchtemplate.columns.length);

		let searchFormIsValid = true;
		searchtemplate.columns
			.forEach((attribute) => {
				let operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
				if (operatorDef) {
					if (operatorDef.isUnary) {
						attribute.value = '';
					}

					attribute.isValid =
						operatorDef.isUnary
						||
						(
							attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === InputType.NUMBER
							||
							attribute.value !== undefined && operatorDef.operator === '='
							&& attribute.value.id && attribute.inputType === InputType.REFERENCE // equal search reference with dropdown
							||
							attribute.value !== undefined && operatorDef.operator === 'like'
							&& attribute.value.length > 0 && attribute.inputType === InputType.REFERENCE // equal search reference with dropdown
							||
							attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === InputType.DATE
							||
							attribute.value !== undefined && attribute.value.length > 0
						);

					if (!attribute.isValid && attribute.enableSearch) {
						searchFormIsValid = false;
					}
				} else {
					if (attribute.enableSearch) {
						searchFormIsValid = false;
					}
				}
				searchtemplate.isValid = searchFormIsValid;
			});
	}

	isDisabled() {
		return !!this.eo && this.eo.isDirty();
	}

	loadSearchLovDropDownOptions(query, attribute: BoAttr): void {
		let attrName = this.fqnService.getShortAttributeName(this.meta.getBoMetaId(), attribute.boAttrId);
		this.metaService.getEntityMeta(this.meta.getBoMetaId()).subscribe(entityMeta => {
			let attributeMeta = entityMeta.getAttributeMeta(attrName);
			if (attributeMeta) {
				this.lovDataService.loadSearchLovEntries(attributeMeta, query).subscribe(
					data => {
						if (data) {
							this.lovValues = data;
						}
					}
				);
			}
		});
	}

	selectLovOption(attribute: SearchtemplateAttribute, option: LovEntry): void {
		if (option.id !== undefined) {
			attribute.value = option;
			this.inputSearchValue(attribute);
		}
	}

	clearLovSelection(): void {
		$('p-autocomplete input').focus();
	}

	private getInputType(attribute: SearchtemplateAttribute): string {
		let attrName = this.fqnService.getShortAttributeName(this.meta.getBoMetaId(), attribute.boAttrId);
		return this.searchtemplateService.getInputType(<SearchtemplateAttribute>this.meta.getAttribute(attrName));
	}

	/**
	 * scroll sidebar to top
	 */
	private scrollSidebarToTop(): void {
		$('#sidebar-list-container').scrollTop(0);
	}
}
