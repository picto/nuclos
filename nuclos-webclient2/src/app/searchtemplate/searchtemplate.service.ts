import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { OperatorDefinitions } from './searchtemplate.component';
import { Operator } from './searchtemplate.model';
import { NuclosI18nService } from '../i18n/shared/nuclos-i18n.service';
import { DatetimeService } from '../shared/datetime.service';
import { EntityMeta, InputType } from '../entity-object/shared/bo-view.model';
import { Preference, SearchtemplateAttribute, SearchtemplatePreferenceContent } from '../preferences/preferences.model';

@Injectable()
export class SearchtemplateService {

	/**
	 * text search
	 */
	private searchInputText: BehaviorSubject<string>;

	private operatorDefinitions: OperatorDefinitions;

	constructor(
		private nuclosI18n: NuclosI18nService,
		private datetimeService: DatetimeService
	) {
		this.searchInputText = new BehaviorSubject<string>('');

		this.operatorDefinitions = {
			NUMBER: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				},
				{
					operator: '!=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.notequal')
				},
				{
					operator: '>',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gt')
				},
				{
					operator: '<',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lt')
				},
				{
					operator: '>=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.gte')
				},
				{
					operator: '<=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.lte')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			BOOLEAN: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.number.equal')
				},
			],
			STRING: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: 'like',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			],
			REFERENCE: [
				{
					operator: '=',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.equal')
				},
				{
					operator: 'like',
					isUnary: false,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.string.like')
				},
				{
					operator: 'is null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.null')
				},
				{
					operator: 'is not null',
					isUnary: true,
					label: this.nuclosI18n.getI18n('webclient.searchtemplate.operator.notnull')
				}
			]
		};
		this.operatorDefinitions[InputType.DATE] = this.operatorDefinitions[InputType.NUMBER];
	}

	getInputType(attribute: SearchtemplateAttribute): string {
		if (attribute.reference) {
			return InputType.REFERENCE;
		} else if (attribute.type === 'Decimal' || attribute.type === 'Integer') {
			return InputType.NUMBER;
		} else if (attribute.type === 'String') {
			return InputType.STRING;
		} else if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
			return InputType.DATE;
		} else if (attribute.type === 'Boolean') {
			return InputType.BOOLEAN;
		}
		return InputType.STRING;
	}


	/**
	 * create an empty searchtemplate preference
	 * @param metaData
	 * @returns {Preference<SearchtemplatePreferenceContent>}
	 */
	emptySearchtemplatePreference(metaData: EntityMeta): Preference<SearchtemplatePreferenceContent> {

		let searchtemplatePreference: Preference<SearchtemplatePreferenceContent> = {
			type: 'searchtemplate',
			boMetaId: metaData.getBoMetaId(),
			selected: false,
			content: {
				columns: [],
				isValid: false,
				disabled: false
			}
		};


		// load all attributes from meta data
		metaData.getAttributes().forEach(attributeMeta => {

			if (!attributeMeta.isStateIcon()
				&& !attributeMeta.isDeletedFlag()
				&& !attributeMeta.isHidden()) {
				searchtemplatePreference.content.columns.push(
					{
						name: attributeMeta.getName(),
						boAttrId: attributeMeta.getAttributeID(),
						system: attributeMeta.isSystemAttribute(),
						selected: false
					}
				);
			}
		});

		return searchtemplatePreference;
	}


	getOperatorDefinitions(): OperatorDefinitions {
		return this.operatorDefinitions;
	}

	getOperatorDefinition(attribute: SearchtemplateAttribute): Operator | undefined {
		let type = attribute.inputType;
		let operator = attribute.operator;

		if (type === undefined) {
			return;
		}
		return this.operatorDefinitions[type].filter(op => op.operator === operator).shift();
	}


	formatValue(attribute: SearchtemplateAttribute, operatorDef: Operator) {
		attribute.formattedValue = attribute.value;

		if (attribute.inputType === InputType.DATE) {
			attribute.formattedValue = this.datetimeService.formatDate(attribute.value);
		} else if (attribute.inputType === InputType.REFERENCE) {
			attribute.formattedValue = attribute.value && attribute.value.name ? attribute.value.name : attribute.value;
		}

		if ((attribute.inputType === InputType.STRING || attribute.inputType === InputType.REFERENCE) && (operatorDef && !operatorDef.isUnary)) {
			attribute.formattedValue = '\'' + attribute.formattedValue + '\'';
		}
	}


	createFromUrlSearch(meta: EntityMeta, searchString: string): Preference<SearchtemplatePreferenceContent> {
		let searchtemplatePreference = this.emptySearchtemplatePreference(meta);

		let sp = searchString.split(',');
		for (let searchParameter of sp) {
			let paramName = searchParameter.split('=')[0];
			let paramValue = searchParameter.split('=')[1];
			let attrFqn = meta.getBoMetaId() + '_' + paramName;
			if (paramName !== undefined && paramValue !== undefined) {

				let attributes = searchtemplatePreference.content.columns.filter((attr) => {
					return attr.boAttrId === attrFqn;
				});

				if (attributes !== undefined && attributes.length !== 0) {

					let attrMeta = <SearchtemplateAttribute>meta.getAttribute(paramName);

					let attribute = attributes[0];
					if (attribute.inputType === 'number') {
						attribute.value = Number(paramValue);
					} else {
						attribute.value = paramValue;
					}

					// Do a LIKE search if the search value starts or ends with a wildcard charater (* or ?)
					if (/(^[*?])|([*?]$)/.test(attribute.value)) {
						attribute.operator = 'like';
					} else {
						attribute.operator = '=';
					}
					let inputType = this.getInputType(attrMeta);
					let operatorDefsForInputType = this.operatorDefinitions[inputType];
					if (operatorDefsForInputType.length !== 0) {
						let operatorDefs = operatorDefsForInputType.filter(
							elem => elem.operator === attribute.operator);
						let operatorDef;
						if (operatorDefs.length !== 0) {
							operatorDef = operatorDefs[0];
						} else {
							operatorDef = operatorDefsForInputType[0];
						}
						attribute.operator = operatorDef.operator;
						this.formatValue(attribute, operatorDef);
					}

					attribute.selected = true;
					attribute.enableSearch = true;
					attribute.isValid = true;
					attribute.searchPopoverOpen = false;
				} else {
					alert('Wrong attribute names: ' + paramName);
					// TODO use errorhandler
				}
			}
		}
		searchtemplatePreference.content.isValid = true;
		searchtemplatePreference.selected = true;

		return searchtemplatePreference;
	}

	updateSearchInputText(text: string) {
		this.searchInputText.next(text);
	}

	getCurrentSearchInputText(): string {
		return this.searchInputText.getValue();
	}
}
