import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { ClickOutsideModule } from '../click-outside/click-outside.module';
import { SearchtemplateComponent } from './searchtemplate.component';
import { I18nModule } from '../i18n/i18n.module';
import { SearchtemplateService } from './searchtemplate.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		I18nModule,
		AutoCompleteModule,
		NgbModule,
		ClickOutsideModule
	],
	exports: [
		SearchtemplateComponent
	],
	declarations: [
		SearchtemplateComponent
	],
	providers: [
		SearchtemplateService,
	]
})
export class SearchtemplateModule {
}
