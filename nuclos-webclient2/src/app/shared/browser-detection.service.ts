import { Injectable } from '@angular/core';

@Injectable()
export class BrowserDetectionService {

	constructor() {
	}

	isSafari() {
		return /constructor/i.test(window['HTMLElement']) || (function (p) {
				return p.toString() === '[object SafariRemoteNotification]';
			})(!window['safari'] || window['safari'].pushNotification);
	}
}
