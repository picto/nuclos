import { Injectable } from '@angular/core';
import { BoAttr, EntityMeta, EntityMetaData } from '../entity-object/shared/bo-view.model';
import { Logger } from '../log/shared/logger';

export interface FqnFormatter {
	(fqn: string)
}

@Injectable()
export class FqnService {

	private static FQN_REGEXP: RegExp = /\${([^\}]+)}/g;

	static toFqn(entityMeta: EntityMeta, attributeName: string): string {
		return entityMeta.getBoMetaId() + '_' + attributeName;
	}


	static getShortAttributeNameFailsafe(eoMetaId: string, attributeFqn: string): string {
		let result = attributeFqn;

		try {
			result = FqnService.getShortAttributeName(eoMetaId, attributeFqn);
		} catch (e) {
		}

		return result;
	}

	/**
	 * convert attribute FQN to short name (e.g.: example_rest_Order_orderNumber -> orderNumber)
	 * @param eoMetaId
	 * @param attributeFqn
	 * @returns {string}
	 */
	static getShortAttributeName(eoMetaId: string, attributeFqn: string): string {
		let separator = '_';
		if (eoMetaId) {
			let prefix = eoMetaId + separator;
			if (attributeFqn.lastIndexOf(prefix) === 0) {
				return attributeFqn.substring(prefix.length);
			}
		} else {
			let index = attributeFqn.lastIndexOf(separator);
			return attributeFqn.substring(index + 1);
		}

		throw Error('Unable to get short attribute name for ' + eoMetaId + ', ' + attributeFqn);
	}

	constructor() {
	}

	/**
	 * creates an array of FQN's used in inputString, like:
	 * "${com_example_Test_attribute1} ${com_example_Test_attribute2}"
	 * ->
	 * ['com_example_Test_attribute1', 'com_example_Test_attribute1']
	 */
	parseFqns(inputString: string | undefined): string[] {
		let result: string[] = [];

		if (!inputString) {
			return result;
		}

		let match;
		do {
			match = FqnService.FQN_REGEXP.exec(inputString);
			if (match !== undefined && match !== null) {
				let fqn = match[1];
				result.push(fqn);
			}
		} while (match !== undefined && match !== null);
		return result;
	}


	/**
	 * @param inputString containing attribute patterns like '${orderNumber} - ${orderDate}
	 * @param formatter formats each fqn token
	 * @return {string}
	 */
	public formatFqnString(inputString: string | undefined, formatter: FqnFormatter): string {

		if (inputString === undefined) {
			return '';
		}

		let result = inputString;

		let match: RegExpExecArray | null;
		do {
			match = FqnService.FQN_REGEXP.exec(inputString);
			if (match !== undefined && match !== null) {
				let token = match[0];
				let fqn = match[1];
				result = result.replace(token, formatter(fqn));
			}
		} while (match !== undefined && match !== null);
		return result;
	}

	/**
	 * Searches by the given attribute FQN for the corresponding attribute in the given Meta.
	 *
	 * @param meta
	 * @param attributeFqn
	 * @returns {BoAttr}
	 */
	getAttributeByFqn(meta: EntityMeta, attributeFqn: string): BoAttr {
		let attributeName = attributeFqn.replace(meta.getBoMetaId(), '').replace('_', '');
		return meta.getAttribute(attributeName);
	}


	/**
	 * convert attribute FQN to short name (e.g.: example_rest_Order_orderNumber -> orderNumber)
	 * @param eoMetaId
	 * @param attributeFqn
	 * @returns {string}
	 */
	getShortAttributeName(eoMetaId: string, attributeFqn: string): string {
		return FqnService.getShortAttributeName(eoMetaId, attributeFqn);
	}
}
