import { Injectable } from '@angular/core';

@Injectable()
export class HyperlinkService {
	constructor() {
	}

	open(url: string, target?: string, features?: string) {
		if (this.isRelative(url)) {
			target = '_self';
		}

		this.openInWindow(url, target, features);
	}

	openInWindow(url: string, target?: string, features?: string) {
		url = this.validateURL(url);

		window.open(url, target, features);
	}

	validateURL(url: string): string {
		if (!this.hasProtocol(url) && !this.isRelative(url)) {
			url = 'http://' + url;
		}

		return url;
	}

	private hasProtocol(url: string) {
		if (url.indexOf('://') >= 0 || url.startsWith('data:')) {
			return true;
		}

		return false;
	}

	private isRelative(url: string) {
		if (url.startsWith('#')
			|| url.startsWith('/')
			|| url.startsWith('./')
			|| url.startsWith('../')) {
			return true;
		}

		return false;
	}
}
