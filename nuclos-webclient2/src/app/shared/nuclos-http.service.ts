import { Injectable, Injector } from '@angular/core';
import {
	ConnectionBackend,
	Headers,
	Http,
	Request,
	RequestMethod,
	RequestOptions,
	RequestOptionsArgs,
	Response
} from '@angular/http';
import { Router } from '@angular/router';
import { FORBIDDEN, NOT_FOUND, UNAUTHORIZED } from 'http-status-codes';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from '../authentication/authentication.service';
import { NuclosCacheService } from '../cache/shared/nuclos-cache.service';
import { Logger } from '../log/shared/logger';
import { ObservableUtils } from './observable-utils';

@Injectable()
export class NuclosHttpService extends Http {

	private pendingRequests = new Map<number, string>();
	private requestsPendingSince: Date | undefined;
	private lastRequestID = 0;

	constructor(
		protected _backend: ConnectionBackend,
		protected _defaultOptions: RequestOptions,
		protected $log: Logger,
		protected cacheService: NuclosCacheService,
		protected injector: Injector
	) {
		super(_backend, _defaultOptions);
	}

	private setCustomHeaders(
		url: string | Request,
		options?: RequestOptionsArgs,
		additionalHeaders?: Map<string, string>
	): RequestOptionsArgs {

		if (!options) {
			options = new RequestOptions({});
		}

		if (!options.headers) {
			options.headers = new Headers();
		}


		if (typeof url === 'string') {
			options.withCredentials = true;
			this.addHeaders(options.headers, additionalHeaders);
		} else {
			url.withCredentials = true;
			this.addHeaders(url.headers, additionalHeaders);
		}

		return options;
	}

	private addHeaders(headers: Headers, additionalHeaders: Map<string, string> | undefined): void {
		if (additionalHeaders) {
			additionalHeaders.forEach(
				(value, key) => {
					headers.set(key, value);
				}
			);
		}
	}

	request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
		options = this.setCustomHeaders(url, options);

		let method: any = (<Request>url).method;
		if (method !== undefined) {
			method = RequestMethod[method].toUpperCase();
		} else {
			method = options.method;
		}

		let urlString: any = url;
		if (url instanceof Request) {
			urlString = url.url;
		}

		const requestID = this.nextRequestID();

		let request = super.request(url, options);

		let observedRequest = ObservableUtils.onSubscribe(
			request,
			() => this.requestStarted(requestID, urlString)
		).catch(err => {
			this.handleError(err);
			return Observable.throw(err);
		}).finally(
			() => this.requestFinished(requestID, urlString)
		);

		return observedRequest;
	}

	private handleError(err) {
		if (err.status === UNAUTHORIZED) {
			let authenticationService = this.getAuthenticationService();
			if (authenticationService && authenticationService.isAutoNavigationAllowed()) {
				this.setRedirectCookie();
				this.getRouter().navigate(['/login']);
			}
		} else if (err.status === FORBIDDEN) {
			this.getRouter().navigate(['/error', FORBIDDEN]);
		} else if (err.status === NOT_FOUND) {
			this.getRouter().navigate(['/error', NOT_FOUND]);
		}
	}

	private getRouter(): Router {
		return this.injector && this.injector.get(Router);
	}

	nextRequestID() {
		return ++this.lastRequestID;
	}

	requestStarted(requestID: number, url: string) {
		if (!this.requestsPendingSince) {
			this.requestsPendingSince = new Date();
		}

		this.pendingRequests.set(requestID, url);
	}

	requestFinished(requestID: number, _url: string) {
		this.pendingRequests.delete(requestID);

		if (!this.hasPendingRequest()) {
			this.requestsPendingSince = undefined;
		}
	}

	/**
	 * Tries to get the AuthenticationService and set the redirect cookie for the current URL.
	 * The user should be redirected to this URL after login.
	 */
	private setRedirectCookie() {
		let authenticationService = this.getAuthenticationService();
		if (authenticationService) {
			authenticationService.redirectToCurrentLocation();
		}
	}

	private getAuthenticationService() {
		return this.injector && this.injector.get(AuthenticationService);
	}
	/**
	 * Performs a POST request.
	 *
	 * It is assumed we always send JSON data, so the Content-Type header is set to 'application/json'.
	 *
	 * @param url
	 * @param body
	 * @param options
	 * @returns {Observable<Response>}
	 */
	post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
		options = this.setCustomHeaders(
			url,
			options,
			new Map<string, string>().set('Content-Type', 'application/json')
		);
		return super.post(url, body, options);
	}

	/**
	 * Same as {@link Http#get}, but caches the result.
	 *
	 * If an additional mapper is provided, the JSON is mapped again before caching.
	 *
	 * @param url
	 * @param mapper
	 * @param options
	 * @returns {Observable<any>}
	 */
	getCachedJSON(
		url: string,
		mapper?: (json: any) => any,
		options?: RequestOptionsArgs
	): Observable<any> {
		let cache = this.cacheService.getCache('http.GET');

		let observable = this.get(url, options).map(response => response.json());
		if (mapper) {
			observable = observable.map(mapper);
		}

		return cache.get(
			url,
			observable
		);
	}

	hasPendingRequest() {
		return this.pendingRequests.size > 0;
	}

	/**
	 * How long since there were no pending requests (in ms).
	 *
	 * @returns {number}
	 */
	getPendingRequestTime() {
		if (!this.requestsPendingSince) {
			return -1;
		}

		let result = Date.now() - this.requestsPendingSince.getTime();

		return result;
	}
}
