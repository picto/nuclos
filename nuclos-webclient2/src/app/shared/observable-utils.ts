import { Observable } from 'rxjs/Observable';

export class ObservableUtils {

	/**
	 * Executes the given action when the given Observable is subscribed to.
	 *
	 * @param observable
	 * @param action
	 * @returns {Observable<T>|Observable<T2|T>}
	 */
	static onSubscribe<T>(
		observable: Observable<T>,
		action: () => any
	): Observable<T> {
		return Observable.empty()
			.do(
				() => {},
				undefined,
				action
			).concat(observable);
	}
}
