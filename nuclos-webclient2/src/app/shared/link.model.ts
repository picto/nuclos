export class HTPPMethod {
	static GET = 'GET';
	static POST = 'POST';
	static PUT = 'PUT';
	static DELETE = 'DELETE';
}

export interface Link {
	href: string;
	methods?: Array<HTPPMethod>;
}

export interface LinkContainer {
	self: Link;
	defaultLayout?: Link;
	defaultGeneration?: Link;
	printouts?: Link;
	bos?: Link;
	boMeta?: Link;
	layout?: Link;
	insert?: Link;
	stateIcon?: Link;
	lock?: Link;
	detail?: Link;
}

export interface SubEOLinkContainer {
	bos: Link;
	boMeta: Link;
}
