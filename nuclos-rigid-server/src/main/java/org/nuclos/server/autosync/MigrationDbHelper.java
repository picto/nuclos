//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.Mutable;
import org.nuclos.common.SF;
import org.nuclos.common.SimpleDbField;
import org.nuclos.common.SysEntities;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.server.dblayer.DbUtils;
import org.nuclos.server.dblayer.MetaDbHelper;
import org.nuclos.server.dblayer.PersistentDbAccess;
import org.nuclos.server.dblayer.expression.DbCurrentDateTime;
import org.nuclos.server.dblayer.expression.DbIncrement;
import org.nuclos.server.dblayer.query.DbFrom;
import org.nuclos.server.dblayer.query.DbQuery;
import org.nuclos.server.dblayer.query.DbQueryBuilder;
import org.nuclos.server.dblayer.query.DbSelection;
import org.nuclos.server.dblayer.statements.DbDeleteStatement;
import org.nuclos.server.dblayer.statements.DbInsertStatement;
import org.nuclos.server.dblayer.statements.DbMap;
import org.nuclos.server.dblayer.statements.DbUpdateStatement;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable
class MigrationDbHelper implements IMigrationHelper {
	
	private static final Logger LOG = Logger.getLogger(MigrationDbHelper.class);
	
	private final PersistentDbAccess dbAccess;
	private final Set<UID> uidEntities = new HashSet<UID>();
	private final SysEntities sysEntities;

	public MigrationDbHelper(SysEntities sysEntities, PersistentDbAccess dbAccess) {
		super();
		this.dbAccess = dbAccess;
		this.sysEntities = sysEntities;
		initMetaData(sysEntities);
	}
	
	private void initMetaData(SysEntities sysEntities) {
		for (EntityMeta<?> entityMeta : sysEntities._getAllEntities()) {
			if (entityMeta.isUidEntity()) {
				uidEntities.add(entityMeta.getUID());
			}
		}
	}
	
	@Override
	public PersistentDbAccess getDbAccess() {
		return dbAccess;
	}
	
	@Override
	public Collection<RigidEO> getAll(EntityMeta<?> meta) {	
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<Object[]> query = builder.createQuery(Object[].class);
		final DbFrom<?> t = query.from(meta, "t", true);
		final List<FieldMeta<?>> selectionMeta = new ArrayList<FieldMeta<?>>();
		final Mutable<DbSelection<?>> pkcol = new Mutable<DbSelection<?>>();

		query.multiselect(getSelection(meta, t, selectionMeta, pkcol));
		
		final List<RigidEO> result = dbAccess.executeQuery(query, new ResultTransformer(meta, selectionMeta));		
		return result;
	}
	
	@Override
	public Collection<Object> getAllPks(EntityMeta<?> meta) {
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		if (meta.isUidEntity()) {
			final DbQuery<UID> query = builder.createQuery(UID.class);
			final DbFrom<?> t = query.from(meta, "t", true);		
			final FieldMeta<?> pkmeta = meta.getPk().getMetaData(meta.getUID());
			final DbField<UID> pkfield = SimpleDbField.create(pkmeta.getDbColumn(), UID.class);
			query.select(t.baseColumn(pkfield));
			final List<Object> result = dbAccess.executeQuery(query, new Transformer<UID, Object>() {
				@Override
				public Object transform(UID uid) {
					return uid;
				}
			});		
			return result;
		} else {
			final DbQuery<Long> query = builder.createQuery(Long.class);
			final DbFrom<?> t = query.from(meta, "t", true);		
			final FieldMeta<?> pkmeta = meta.getPk().getMetaData(meta.getUID());
			final DbField<Long> pkfield = SimpleDbField.create(pkmeta.getDbColumn(), Long.class);
			query.select(t.baseColumn(pkfield));
			final List<Object> result = dbAccess.executeQuery(query, new Transformer<Long, Object>() {
				@Override
				public Object transform(Long id) {
					return id;
				}
			});		
			return result;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RigidEO getByPrimaryKey(EntityMeta<?> meta, Object pk) {
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<Object[]> query = builder.createQuery(Object[].class);
		final DbFrom<?> t = query.from(meta, "t", true);
		final List<FieldMeta<?>> selectionMeta = new ArrayList<FieldMeta<?>>();
		final Mutable<DbSelection<?>> pkcol = new Mutable<DbSelection<?>>();

		query.multiselect(getSelection(meta, t, selectionMeta, pkcol));
		query.where(builder.equalValue((DbSelection<Object>) pkcol.getValue(), pk));
		
		final RigidEO result = dbAccess.executeQuerySingleResult(query, new ResultTransformer(meta, selectionMeta));		
		return result;
	}
	
	@Override
	public Collection<RigidEO> getBySysField(FieldMeta<?> field, Object value) {
		EntityMeta<?> meta = sysEntities._getByUID(field.getEntity());
		if (meta == null) {
			throw new IllegalArgumentException("Entity " + field.getEntity() + " of field " + field + " does not exist in sysEntities");
		}
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<Object[]> query = builder.createQuery(Object[].class);
		final DbFrom<?> t = query.from(meta, "t", true);
		final List<FieldMeta<?>> selectionMeta = new ArrayList<FieldMeta<?>>();
		final Mutable<DbSelection<?>> pkcol = new Mutable<DbSelection<?>>();
		final Mutable<DbSelection<?>> searchcol = new Mutable<DbSelection<?>>();

		query.multiselect(getSelection(meta, t, selectionMeta, pkcol, field.getUID(), searchcol));
		query.where(builder.equalValue((DbSelection<Object>) searchcol.getValue(), value));
		
		final List<RigidEO> result = dbAccess.executeQuery(query, new ResultTransformer(meta, selectionMeta));		
		return result;
	}
	
	@Override
	public Collection<RigidEO> getByField(EntityMeta<?> meta, FieldMeta<?> field, Object value) {
		return getByField(meta, field.getUID(), value);
	}
	
	@Override
	public Collection<RigidEO> getByField(EntityMeta<?> meta, UID field, Object value) {
		final DbQueryBuilder builder = dbAccess.getQueryBuilder();
		final DbQuery<Object[]> query = builder.createQuery(Object[].class);
		final DbFrom<?> t = query.from(meta, "t", true);
		final List<FieldMeta<?>> selectionMeta = new ArrayList<FieldMeta<?>>();
		final Mutable<DbSelection<?>> pkcol = new Mutable<DbSelection<?>>();
		final Mutable<DbSelection<?>> searchcol = new Mutable<DbSelection<?>>();
		
		query.multiselect(getSelection(meta, t, selectionMeta, pkcol, field, searchcol));
		if (searchcol.getValue() == null) {
			throw new IllegalArgumentException("Field " + field + " in entity " + meta + " does not exist");
		}
		query.where(builder.equalValue((DbSelection<Object>) searchcol.getValue(), value));
		
		final List<RigidEO> result = dbAccess.executeQuery(query, new ResultTransformer(meta, selectionMeta));		
		return result;
	}
	
	private List<DbSelection<?>> getSelection(final EntityMeta<?> meta, final DbFrom<?> t, final List<FieldMeta<?>> selectionMeta, final Mutable<DbSelection<?>> pkcol) {
		return getSelection(meta, t, selectionMeta, pkcol, null, null);
	}
	
	private List<DbSelection<?>> getSelection(
			final EntityMeta<?> meta, 
			final DbFrom<?> t, 
			final List<FieldMeta<?>> selectionMeta, 
			final Mutable<DbSelection<?>> pkcol, 
			final UID searchfield, 
			final Mutable<DbSelection<?>> searchcol) {
		
		final FieldMeta<?> pkmeta = meta.getPk().getMetaData(meta.getUID());
		final DbField<?> pkfield = getDbField(pkmeta.getDbColumn(), pkmeta.getJavaClass(), null);
		final List<DbSelection<?>> select = new ArrayList<DbSelection<?>>();
		select.add(pkcol.setValue(t.baseColumn(pkfield)));
		selectionMeta.add(pkmeta);
		for (FieldMeta<?> fieldMeta : meta.getFields()) {
			if (SF.CREATEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CREATEDAT.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDAT.checkField(fieldMeta.getFieldName())) {
				continue;
			}
			
			final DbField<?> dbfield = getDbField(fieldMeta.getDbColumn(), fieldMeta.getJavaClass(), fieldMeta.getForeignEntity());
			if (dbfield != null) {
				DbSelection<?> dbcol = t.baseColumn(dbfield);
				select.add(dbcol);
				selectionMeta.add(fieldMeta);
				
				if (searchfield != null && searchcol != null) {
					if (searchfield.equals(fieldMeta.getUID())) {
						searchcol.setValue(dbcol);
					}
				}
			}
		}
		return select;
	}
	
	@Override
	public void updateAll(EntityMeta<?> meta, Collection<RigidEO> values) {
		for (RigidEO value : values) {
			update(meta, value);
		}
	}
	
	public void insert(EntityMeta<?> meta, RigidEO value) {
		final DbMap insertMap = new DbMap();
		insertMap.putUnsafe(meta.getPk(), value.getPrimaryKey());
		for (FieldMeta<?> fieldMeta : meta.getFields()) {
			if (fieldMeta.isReadonly()) {
				continue;
			}
			if (SF.CREATEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CREATEDAT.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDAT.checkField(fieldMeta.getFieldName())) {
				continue;
			}
			Object o = null;
			if (fieldMeta.getJavaClass() == UID.class) {
				o = value.getForeignUID((DbField<UID>) fieldMeta);
			} else {
				if (fieldMeta.getForeignEntity() != null) {
					o = value.getForeignID((DbField<Long>) fieldMeta);
				} else {
					o = value.getValue(fieldMeta);
					if (o == null && SF.IMPORTVERSION.checkField(fieldMeta.getFieldName())) {
						o = value.getValue(SF.IMPORTVERSION);
					}
				}
			}
			if (o == null) {
				insertMap.putNull(fieldMeta);
			} else {
				insertMap.putUnsafe(fieldMeta, o);
			}
		}
		insertMap.put(SF.CREATEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		insertMap.put(SF.CREATEDBY, "MIGRATION");
		insertMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		insertMap.put(SF.CHANGEDBY, "MIGRATION");
		insertMap.put(SF.VERSION, 0);
		
		DbInsertStatement<?> insertStmt = new DbInsertStatement(MetaDbHelper.getTableName(meta), insertMap);
		dbAccess.execute(insertStmt);
	}
	
	@Override
	public void update(EntityMeta<?> meta, RigidEO value) {
		final DbMap condition = new DbMap();
		condition.putUnsafe(meta.getPk(), value.getPrimaryKey());
		final DbMap updateMap = new DbMap();
		for (FieldMeta<?> fieldMeta : meta.getFields()) {
			if (fieldMeta.isReadonly()) {
				continue;
			}
			if (SF.CREATEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CREATEDAT.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDBY.checkField(fieldMeta.getFieldName()) ||
					SF.CHANGEDAT.checkField(fieldMeta.getFieldName())) {
				continue;
			}
			Object o = null;
			if (fieldMeta.getJavaClass() == UID.class) {
				o = value.getForeignUID((DbField<UID>) fieldMeta);
			} else {
				if (fieldMeta.getForeignEntity() != null) {
					o = value.getForeignID((DbField<Long>) fieldMeta);
				} else {
					o = value.getValue(fieldMeta);
					if (o == null && SF.IMPORTVERSION.checkField(fieldMeta.getFieldName())) {
						o = value.getValue(SF.IMPORTVERSION);
					}
				}
			}
			if (o == null) {
				updateMap.putNull(fieldMeta);
			} else {
				updateMap.putUnsafe(fieldMeta, o);
			}
		}
		updateMap.put(SF.CHANGEDAT, DbCurrentDateTime.CURRENT_DATETIME);
		updateMap.put(SF.CHANGEDBY, "MIGRATION");
		updateMap.put(SF.VERSION, DbIncrement.INCREMENT);
		
		DbUpdateStatement<?> updateStmt = new DbUpdateStatement(MetaDbHelper.getTableName(meta), updateMap, condition);
		dbAccess.execute(updateStmt);
	}
	
	@Override
	public void delete(EntityMeta<?> meta, RigidEO value) {
		final DbMap condition = new DbMap();
		condition.putUnsafe(meta.getPk(), value.getPrimaryKey());
		DbDeleteStatement<?> deleteStmt = new DbDeleteStatement(MetaDbHelper.getTableName(meta), condition);
		dbAccess.execute(deleteStmt);
	}
	
	private DbField<?> getDbField(String column, Class<?> javaClass, UID foreignEntity) {
		if (foreignEntity != null) {
			if (StringUtils.startsWithIgnoreCase(column, "STRVALUE_") || 
				StringUtils.startsWithIgnoreCase(column, "INTVALUE_") || 
				StringUtils.startsWithIgnoreCase(column, "OBJVALUE_")) {
				final boolean refsToUidEntity = uidEntities.contains(foreignEntity);
				if (refsToUidEntity) {
					return SimpleDbField.create(MetaDbHelper.getDbRefColumn(column, refsToUidEntity), UID.class);
				} else {
					return SimpleDbField.create(MetaDbHelper.getDbRefColumn(column, refsToUidEntity), Long.class);					
				}
			}
		} 
		return SimpleDbField.create(column.toUpperCase(), javaClass);
	}
	
	public class ResultTransformer implements Transformer<Object[], RigidEO> {
		
		private final EntityMeta<?> meta;
		private final List<FieldMeta<?>> selectionMeta;
		
		public ResultTransformer(EntityMeta<?> meta, List<FieldMeta<?>> selectionMeta) {
			this.meta = meta;
			this.selectionMeta = selectionMeta;
		}
		
		@Override
		public RigidEO transform(Object[] dbRow) {
			final Object pk = dbRow[0];
			final RigidEO reo = new RigidEO(meta.getUID(), pk);
			for (int i = 1; i < dbRow.length; i++) {
				final Object oValue = dbRow[i];
				final FieldMeta<?> columnMeta = selectionMeta.get(i);
				if (UID.class.isAssignableFrom(columnMeta.getJavaClass())) {
					reo.setForeignUID((FieldMeta<UID>) columnMeta, (UID) oValue);
				} else {
					if (columnMeta.getForeignEntity() != null) {
						reo.setForeignID((FieldMeta<Long>) columnMeta, (Long) oValue);
					} else {
						reo.setValueUNSAFE((DbField<?>) columnMeta, oValue);
					}
				}
			}
			return reo;
		}
	}
	
	/**
	 * Renames a db table
	 * 
	 * Only supported from helper for db migrations.
	 * Helper for nuclet migrations throws Exception.
	 * 
	 * 
	 * @param currentName
	 * @param newName
	 */
	public void renameTable(String currentName, String newName) {
		DbUtils.renameTable(dbAccess, currentName, newName);
	}
	
}
