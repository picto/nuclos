//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync.migration;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.SysEntities;
import org.nuclos.server.autosync.AutoDbSetupUtils;
import org.nuclos.server.autosync.MigrationContext;

public abstract class AbstractMigration {
	
	@Target({ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	public
	static @interface Migration {
	}
	
	private MigrationContext context;

	protected final MigrationContext getContext() {
		return context;
	}
	
	public final void init(MigrationContext context) {
		if (this.context != null) {
			throw new NuclosFatalException("Context already set");
		}
		this.context = context;
	}

	public abstract void migrate();
	
	/**
	 * 
	 * @return the source meta data
	 */
	public abstract SysEntities getSourceMeta();
	
	/**
	 * 
	 * @return the source static XML schema extension (/resources/db/v??)
	 */
	public abstract String getSourceStatics();
	
	/**
	 * 
	 * @return the source meta data
	 */
	public abstract SysEntities getTargetMeta();
	
	/**
	 * 
	 * @return the target static XML schema extension (/resources/db/v??)
	 */
	public abstract String getTargetStatics();
	
	/**
	 * 
	 * @return necessary until schema version
	 * 			e.g. the 4.00.0022 migration is only necessary if source is 3.x, and NOT 4.00.0016.
	 * 			In this case return "3.99.9999"
	 * 		
	 * 			Default is schema from target meta!
	 */
	public String getNecessaryUntilSchemaVersion() {
		return getTargetMeta()._getSchemaVersion();
	}

	/**
	 *
	 * @return necessary only if prior schema version migrates already
	 * 			e.g. the 4.12.0003 migration is also included in the special customer Nuclos 4.8.30 version (branch 4.8-documentfile)
	 */
	public String[] getIgnoringSourceSchemaVersions() {
		return new String[]{};
	};
	
	public final String getMigrationName() {
		return getLowerClassName(this.getClass());
	}
	
	protected static Logger getLogger(Class<? extends AbstractMigration> migcls) {
		Logger result = (Logger) LogManager.getLogger(migcls);
		Appender appender = AutoDbSetupUtils.createAppender(getLowerClassName(migcls));
		if (appender != null) {
			result.addAppender(appender);
		}
		return result;
	}
	
	private static String getLowerClassName(Class cls) {
		return cls.getSimpleName().toLowerCase();
	}
	
}
