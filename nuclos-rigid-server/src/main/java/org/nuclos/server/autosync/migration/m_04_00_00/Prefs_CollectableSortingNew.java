//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.Serializable;

import org.nuclos.common.UID;

/**
 * Sorting (German: "Sortierung") of a <code>Collectable</code>, consisting of a field name and a direction.
 * Typically, this is an element in a <code>List</code> containing the complete sorting order.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Incorporate a real (foreign) table ref instead of just the entity.
 * </p><p>
 * TODO: Consider including an entity field rather than the mere field name.
 * </p>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
class Prefs_CollectableSortingNew implements Serializable {
	
	private static final long serialVersionUID = 1002927873365829614L;

	private final UID entity;
	
	private final boolean isBaseEntity;
	
	private final UID field;
	
	private final boolean asc;
	

	/**
	 * §precondition sFieldName != null
	 * 
	 * @param field name of the field to sort
	 * @param asc Sort ascending? (false: sort descending)
	 */
	public Prefs_CollectableSortingNew(UID entity, boolean isBaseEntity, UID field, boolean asc) {
		if (entity == null || field == null) throw new NullPointerException();
		this.entity = entity;
		this.isBaseEntity = isBaseEntity;
		this.field = field;
		this.asc = asc;
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return uid of the field to sort.
	 */
	public UID getField() {
		return this.field;
	}

	/**
	 * @return Sort ascending? (false: sort descending)
	 */
	public boolean isAscending() {
		return this.asc;
	}
	
	public UID getEntity() {
		return entity;
	}

	public boolean isBaseEntity() {
		return isBaseEntity;
	}	
	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Prefs_CollectableSortingNew)) return false;
		final Prefs_CollectableSortingNew other = (Prefs_CollectableSortingNew) o;
		return entity.equals(other.entity) && field.equals(other.field);
	}
	
	public int hashCode() {
		int result = 3 * entity.hashCode() + 7;
		result += 11 * field.hashCode();
		return result;
	}

    @Override
    public String toString() {
    	final StringBuilder result = new StringBuilder();
    	result.append("CollectableSorting[");
    	result.append(", field=").append(field);
    	result.append(", isBase=").append(isBaseEntity);
    	result.append(",");
    	if (asc) {
    		result.append("ASC");
    	}
    	else {
    		result.append("DSC");
    	}
    	result.append("]");
    	return result.toString();
    }

}  // class CollectableSorting
