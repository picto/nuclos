package org.nuclos.server.autosync.migration.m_04_00_00;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

@SuppressWarnings("serial")
@XmlRootElement(name="planElement")
class RP_PlanElementNew implements Serializable {
	final public static int ENTRY = 1;
	final public static int MILESTONE = 2;
	final public static int RELATION = 3;
	
	private int type;
	private UID entity;
	private UID primaryField;
	private UID secondaryField;
	
	private UID dateFromField;
	private UID dateUntilField;
	private String timePeriodsString;
	private UID timeFromField;
	private UID timeUntilField;

	private String labelText;
	private String toolTipText;
	
	private String scriptingEntryCellMethod;
	
	private int presentation;
	private int fromPresentation;
	private int toPresentation;
	private boolean newRelationFromController;
	
	private String color;
	private String fromIcon;
	private String toIcon;
	
	private List<RP_PlanElementLocaleNew> planElementLocale;
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RP_PlanElementNew) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return RigidUtils.hashCode(entity) ^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(type) 
				^ RigidUtils.hashCode(primaryField) ^ RigidUtils.hashCode(secondaryField);
	}

	@XmlElement(name="entity")
	public UID getEntity() {
		return entity;
	}

	public void setEntity(UID eEntity) {
		this.entity = eEntity;
	}

	@XmlElement(name="primaryField")
	public UID getPrimaryField() {
		return primaryField;
	}

	public void setPrimaryField(UID primaryField) {
		this.primaryField = primaryField;
	}
	
	@XmlElement(name="secondaryField")
	public UID getSecondaryField() {
		return secondaryField;
	}

	public void setSecondaryField(UID secondaryField) {
		this.secondaryField = secondaryField;
	}

	@XmlElement(name="dateFromField")
	public UID getDateFromField() {
		return dateFromField;
	}

	public void setDateFromField(UID dateFromField) {
		this.dateFromField = dateFromField;
	}

	@XmlElement(name="dateUntilField")
	public UID getDateUntilField() {
		return dateUntilField;
	}

	public void setDateUntilField(UID dateUntilField) {
		this.dateUntilField = dateUntilField;
	}

	@XmlElement(name="timePeriodsString")
	public String getTimePeriodsString() {
		return timePeriodsString;
	}

	public void setTimePeriodsString(String timePeriodsString) {
		this.timePeriodsString = timePeriodsString;
	}

	@XmlElement(name="timeFromField")
	public UID getTimeFromField() {
		return timeFromField;
	}

	public void setTimeFromField(UID timeFromField) {
		this.timeFromField = timeFromField;
	}

	@XmlElement(name="timeUntilField")
	public UID getTimeUntilField() {
		return timeUntilField;
	}

	public void setTimeUntilField(UID timeUntilField) {
		this.timeUntilField = timeUntilField;
	}

	@XmlElement(name="labelText")
	public String getLabelText() {
		return labelText;
	}

	public void setLabelText(String labelText) {
		this.labelText = labelText;
	}

	@XmlElement(name="toolTipText")
	public String getToolTipText() {
		return toolTipText;
	}

	public void setToolTipText(String toolTipText) {
		this.toolTipText = toolTipText;
	}

	@XmlElement(name="scriptingEntryCellMethod")
	public String getScriptingEntryCellMethod() {
		return scriptingEntryCellMethod;
	}

	public void setScriptingEntryCellMethod(String scriptingEntryCellMethod) {
		this.scriptingEntryCellMethod = scriptingEntryCellMethod;
	}

	@XmlElement(name="type")
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@XmlElement(name="presentation")
	public int getPresentation() {
		return presentation;
	}

	public void setPresentation(int presentation) {
		this.presentation = presentation;
	}

	@XmlElement(name="fromPresentation")
	public int getFromPresentation() {
		return fromPresentation;
	}

	public void setFromPresentation(int fromPresentation) {
		this.fromPresentation = fromPresentation;
	}

	@XmlElement(name="toPresentation")
	public int getToPresentation() {
		return toPresentation;
	}

	public void setToPresentation(int toPresentation) {
		this.toPresentation = toPresentation;
	}

	@XmlElement(name="newRelationFromController")
	public boolean isNewRelationFromController() {
		return newRelationFromController;
	}

	public void setNewRelationFromController(boolean newRelationFromController) {
		this.newRelationFromController = newRelationFromController;
	}

	@XmlElement(name="color")
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@XmlElement(name="fromIcon")
	public String getFromIcon() {
		return fromIcon;
	}

	public void setFromIcon(String fromIcon) {
		this.fromIcon = fromIcon;
	}

	@XmlElement(name="toIcon")
	public String getToIcon() {
		return toIcon;
	}

	public void setToIcon(String toIcon) {
		this.toIcon = toIcon;
	}
	
	@XmlElement(name="planElementLocale")
	public List<RP_PlanElementLocaleNew> getPlanElementLocaleVO() {
		return planElementLocale;
	}

	public void setPlanElementLocaleVO(List<RP_PlanElementLocaleNew> planElementLocale) {
		this.planElementLocale = planElementLocale;
	}
}