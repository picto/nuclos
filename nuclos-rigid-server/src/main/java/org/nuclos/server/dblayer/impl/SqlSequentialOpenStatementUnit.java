package org.nuclos.server.dblayer.impl;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.dal.DalCallResult;
import org.nuclos.server.dblayer.DbException;
import org.nuclos.server.dblayer.EBatchType;
import org.nuclos.server.dblayer.IPreparedStringExecutor;
import org.nuclos.server.dblayer.impl.util.PreparedString;
import org.nuclos.server.dblayer.statements.DbMap;

public class SqlSequentialOpenStatementUnit extends SqlSequentialUnit {

	private static final Logger LOG = Logger.getLogger(SqlSequentialOpenStatementUnit.class);
	 
	private DbMap map;
	private PreparedString targetSequence;
	
	public SqlSequentialOpenStatementUnit(List<PreparedString> sequence, PreparedString targetSequence, DbMap map) {
		super(sequence);
		this.map = map;
		this.targetSequence = targetSequence;
	}

	public SqlSequentialOpenStatementUnit(PreparedString sequence,  PreparedString targetSequence,DbMap map) {
		this(RigidUtils.newOneElementArrayList(sequence), targetSequence, map);
	}
	
	@Override
	public DalCallResult process(IPreparedStringExecutor ex, EBatchType type) {
		final boolean debug = LOG.isDebugEnabled();
		final DalCallResult result = new DalCallResult();
		if (debug) LOG.debug("begin process of " + this);
		for (PreparedString ps: getSequence()) {
			try {
				ex.executePreparedStatement(ps, targetSequence, map);
				result.addToNumberOfDbChanges(1);
			} catch (SQLException e) {
				if (!type.equals(EBatchType.FAIL_NEVER_IGNORE_EXCEPTION)) {
					result.addDbException(null, Collections.singletonList(ps.toString()), e);
				}
				else {
					if (debug) LOG.info("Ignored exception: " + e + " while executing " + ps);
				}
				switch (type)  {
					case FAIL_EARLY:
						try {result.throwFirstException();} catch (Exception ex2) {throw (DbException) ex2;}
						break;
					case FAIL_LATE:
					case FAIL_NEVER:
					case FAIL_NEVER_IGNORE_EXCEPTION:
						break;
					default:
						throw new IllegalArgumentException(type.toString());
				}
			}
		}
		if (debug) LOG.debug("end batch process with result: " + result);
		if (type.equals(EBatchType.FAIL_LATE)) {
			try {result.throwFirstException();} catch (Exception ex2) {throw (DbException) ex2;}
		}
		return result;
	}
	
}
