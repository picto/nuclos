package org.nuclos.server.dblayer;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.dataset.datatype.NumberDataType;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;

public class NuclosMsSqlDataTypeFactory extends MsSqlDataTypeFactory {

	@Override
	public DataType createDataType(int sqlType, String sqlTypeName)
			throws DataTypeException {
		DataType dataType = super.createDataType(sqlType, sqlTypeName);
		
		if (dataType instanceof NumberDataType) {
			dataType = new NuclosNumberDataType((NumberDataType)dataType);
		}
		
		return dataType;
	}
}
