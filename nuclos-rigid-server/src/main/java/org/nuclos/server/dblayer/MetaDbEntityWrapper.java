//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.RigidE;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;
import org.nuclos.server.autosync.RigidEO;

public class MetaDbEntityWrapper {
	
	private final static RigidE._Entity ENTITY = new RigidE._Entity() {}; 
	
	private final RigidEO rEntity;
	private final EntityMeta<?> entityMeta;
	
	public MetaDbEntityWrapper() {
		this.rEntity = null;
		this.entityMeta = null;
	}

	public MetaDbEntityWrapper(RigidEO rEntity) {
		this.rEntity = rEntity;
		this.entityMeta = null;
	}

	public MetaDbEntityWrapper(EntityMeta<?> entityMeta) {
		this.rEntity = null;
		this.entityMeta = entityMeta;
	}

	public UID getUID() {
		if (entityMeta != null)
			return entityMeta.getUID();
		return (UID) rEntity.getPrimaryKey();
	}

	public boolean isUidEntity() {
		if (entityMeta != null)
			return entityMeta.isUidEntity();
		return false;
	}
	
	public boolean isDatasourceBased() {
		if (entityMeta != null)
			return entityMeta.isDatasourceBased();
		return false;
	}
	
	public boolean isTableMaster() {
		if (entityMeta != null)
			return entityMeta.isTableMaster();
		return true;
	}
	
	public boolean isMandator() {
		if (entityMeta != null) {
			return entityMeta.isMandator();
		}
		return false;
	}

	public String getEntityName() {
		if (entityMeta != null)
			return entityMeta.getEntityName();
		return (String) rEntity.getValue(ENTITY.entity);
	}

	public String getDbTable() {
		if (entityMeta != null)
			return entityMeta.getDbTable();
		return (String) rEntity.getValue(ENTITY.dbtable);
	}

	public String getVirtualEntity() {
		if (entityMeta != null)
			return entityMeta.getVirtualEntity();
		return StringUtils.stripToNull((String) rEntity.getValue(ENTITY.virtualentity));
	}
	
	public String getDataLangRefPath() {
		if (entityMeta != null) {
			return entityMeta.getDataLangRefPath();
		}
		return null;
	}
	
	public boolean isProxy() {
		if (entityMeta != null)
			return entityMeta.isProxy();
		return Boolean.TRUE.equals(rEntity.getValue(ENTITY.proxy));
	}
	
	public boolean isWriteProxy() {
		if (entityMeta != null)
			return entityMeta.isWriteProxy();
		return Boolean.TRUE.equals(rEntity.getValue(ENTITY.writeproxy));
	}


	public boolean isGeneric() {
		if (entityMeta != null) {
			return entityMeta.isGeneric();
		}
		return Boolean.TRUE.equals(rEntity.getValue(ENTITY.generic));
	}

	public UID[][] getUniqueFieldCombinations() {
		if (entityMeta != null)
			return entityMeta.getUniqueFieldCombinations();
		return null;
	}
	
	public UID[][] getLogicalUniqueFieldCombinations() {
		if (entityMeta != null)
			return entityMeta.getLogicalUniqueFieldCombinations();
		return null;
	}
	
	public UID[][] getIndexFieldCombinations() {
		if (entityMeta != null)
			return entityMeta.getIndexFieldCombinations();
		return null;
	}

	public boolean isLocalized() {
		boolean retVal = false;
		
		if (entityMeta != null) {
			retVal = entityMeta.IsLocalized();
		}
		
		return retVal;
	}
	
	public boolean isDataLanguageEntity() {
		boolean retVal = false;
		
		if (entityMeta != null) {
			retVal = EntityMeta.isEntityLanguageUID(entityMeta.getUID());
		}
		
		return retVal;
	}
	
	public boolean isOwner() {
		if (entityMeta != null) {
			return entityMeta.isOwner();
		}
		return false;
	}
	
	public EntityMeta<?> getEntityMeta() {
		return this.entityMeta;
	}
	
	@Override
	public int hashCode() {
		final UID pk = getUID();
		if (pk != null)
			return pk.getString().hashCode();
		return super.hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that)
			return true;
		if (that instanceof MetaDbEntityWrapper) {
			RigidUtils.equal(((MetaDbEntityWrapper) that).getUID(), getUID());
		}
		return super.equals(that);
	}

	@Override
	public String toString() {
		return String.format("MetaDbEntityWrapper[entity=%s, table=%s, uid=%s", getEntityName(), getDbTable(), getUID());
	}
	
	

}
