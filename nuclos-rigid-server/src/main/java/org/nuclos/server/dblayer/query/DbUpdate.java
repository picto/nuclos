//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.server.dblayer.query;

import org.apache.log4j.Logger;
import org.nuclos.common.DbField;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.dal.vo.SystemFields;
import org.nuclos.server.dblayer.impl.util.PreparedStringBuilder;
import org.nuclos.server.dblayer.statements.DbMap;

public class DbUpdate {
	
	private static final Logger LOG = Logger.getLogger(DbUpdate.class);

	private final DbQueryBuilder builder;
	private EntityMeta<?> from;
	private DbCondition condition;
	private DbMap values;
	private final String tAlias;
	
	DbUpdate(DbQueryBuilder builder, EntityMeta<?> from, DbMap values) {
		this.builder = builder;
		this.from = from;
		this.values = values;
		this.tAlias = SystemFields.BASE_ALIAS;
	}
	
	public DbQueryBuilder getBuilder() {
		return builder;
	}
	
	public EntityMeta<?> getFrom() {
		return from;
	}
	
	public String getAlias() {
		return tAlias;
	}
	
	public <T> DbExpression<T> baseColumn(DbField<T> column) {
		return new DbExpression<T>(builder, column.getJavaClass(), new PreparedStringBuilder(tAlias + "." + column.getDbColumn()));
	}
	
	public DbUpdate where(DbCondition condition) {
		if (this.condition != null) {
			throw new IllegalStateException("where condition already set, use addToWhere or replaceWhere if this is intended");
		}
		this.condition = condition;
		return this;
	}
	
	public DbUpdate replaceWhere(DbCondition condition) {
		this.condition = condition;
		return this;
	}
	
	public DbUpdate addToWhereAsAnd(DbCondition condition) {
		if (this.condition == null) {
			this.condition = condition;
		}
		else {
			this.condition = builder.and(this.condition, condition);
		}
		return this;
	}	
	
	public DbUpdate addToWhereAsOr(DbCondition condition) {
		this.condition = builder.or(this.condition, condition);
		return this;
	}	
	
	public DbCondition getRestriction() {
		return condition;
	}
	
	public DbMap getValues() {
		return values;
	}

	@Override
	public String toString() {
		return "DbDelete [from=" + from + ", condition=" + condition + "]";
	}
}
