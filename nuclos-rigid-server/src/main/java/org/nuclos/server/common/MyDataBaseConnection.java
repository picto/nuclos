package org.nuclos.server.common;

import java.lang.reflect.Field;
import java.sql.Connection;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.AbstractDatabaseConnection;
import org.dbunit.database.DatabaseConnection;

public class MyDataBaseConnection extends DatabaseConnection implements IMyDataBaseConnection {

	public MyDataBaseConnection(Connection connection, String schema)
			throws DatabaseUnitException {
		super(connection, schema);
	}
	
	@Override
	public void resetMeta() {
		try {
			Field f = AbstractDatabaseConnection.class.getDeclaredField("_dataSet");
			f.setAccessible(true);
			f.set(this, null);
		} catch (NoSuchFieldException e) {
			Logger.getLogger(MyDataBaseConnection.class).error(e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Logger.getLogger(MyDataBaseConnection.class).error(e.getMessage(), e);
		}
		
	}
	
	public interface IStreamingFilterProvider {
		
		boolean prepare(String table);
		
		void commit();
		
		void setMyDataBaseConnection(IMyDataBaseConnection conn);
		
	}

}
