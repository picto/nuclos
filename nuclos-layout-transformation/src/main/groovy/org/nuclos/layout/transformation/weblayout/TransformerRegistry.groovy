package org.nuclos.layout.transformation.weblayout

import org.nuclos.schema.layout.layoutml.Button
import org.nuclos.schema.layout.layoutml.CollectableComponent
import org.nuclos.schema.layout.layoutml.Label
import org.nuclos.schema.layout.layoutml.Matrix
import org.nuclos.schema.layout.layoutml.Subform
import org.nuclos.schema.layout.layoutml.Tabbedpane
import org.nuclos.schema.layout.web.WebComponent

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class TransformerRegistry {
	static Map<Class, ElementTransformer> map = new HashMap<>()

	static {
		register(Tabbedpane.class, new TabbedpaneTransformer())
		register(CollectableComponent.class, new CollectableComponentTransformer())
		register(Subform.class, new SubformTransformer())
		register(Button.class, new ButtonTransformer())
		register(Matrix.class, new MatrixTransformer())
		register(Label.class, new LabelTransformer())
	}

	static <T> void register(Class<T> clss, ElementTransformer<T, ? extends WebComponent> transformer) {
		this.map.put(clss, transformer)
	}

	static <T> ElementTransformer<T, ? extends WebComponent> lookup(Class<T> clss) {
		return map.get(clss)
	}
}
