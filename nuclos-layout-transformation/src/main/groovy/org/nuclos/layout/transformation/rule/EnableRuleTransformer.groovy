package org.nuclos.layout.transformation.rule

import org.nuclos.schema.layout.layoutml.Enable
import org.nuclos.schema.layout.rule.RuleActionEnable

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class EnableRuleTransformer extends ActionTransformer<Enable, RuleActionEnable> {

	EnableRuleTransformer() {
		super(Enable.class)
	}

	@Override
	RuleActionEnable transform(final Enable input) {
		RuleActionEnable result = factory.createRuleActionEnable()

		result.invertable = input.invertable == org.nuclos.schema.layout.layoutml.Boolean.YES
		result.targetcomponent = input.targetcomponent

		return result
	}
}
