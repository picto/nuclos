package org.nuclos.layout.transformation

import org.nuclos.common.FieldMeta
import org.nuclos.common.IMetaProvider
import org.nuclos.common.UID
import org.nuclos.schema.layout.layoutml.Layoutml

import groovy.transform.CompileStatic

/**
 * A generic LayoutML transformer.
 *
 * @param <T>
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
abstract class AbstractLayoutmlTransformer<T> {
	protected final Layoutml layoutml
	protected final IMetaProvider metaProvider

	AbstractLayoutmlTransformer(
			final IMetaProvider metaProvider,
			final Layoutml layoutml
	) {
		this.metaProvider = metaProvider
		this.layoutml = layoutml
	}

	abstract T transform()

	/**
	 * Tries to lookup the field name for the given UID string of the form "uid{...}".
	 *
	 * TODO: Maybe this can be done completely automatically by a Jackson serializer.
	 *
	 * @param uidString
	 * @return The field name if found, or the given uidString if not found.
	 */
	String getFieldName(String uidString) {
		UID uid = UID.parseUID(uidString)
		if (uid) {
			FieldMeta<?> fieldMeta = metaProvider.getEntityField(uid)
			if (fieldMeta) {
				return fieldMeta.fieldName
			}
		}
		return uidString
	}
}