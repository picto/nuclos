package org.nuclos.layout.transformation.rule

import org.nuclos.schema.layout.layoutml.RefreshValuelist
import org.nuclos.schema.layout.rule.RuleActionRefreshValuelist

import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
@PackageScope
class RefreshValuelistRuleTransformer extends ActionTransformer<RefreshValuelist, RuleActionRefreshValuelist> {

	RefreshValuelistRuleTransformer() {
		super(RefreshValuelist.class)
	}

	@Override
	RuleActionRefreshValuelist transform(final RefreshValuelist input) {
		RuleActionRefreshValuelist result = factory.createRuleActionRefreshValuelist()

		result.entity = input.entity
		result.targetcomponent = input.targetcomponent
		result.parameterForSourcecomponent = input.parameterForSourcecomponent

		return result
	}
}
