module.exports = function (grunt) {

	// Load Grunt tasks declared in the package.json file
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


	// use server-default.json if no local server.json exists
	if (!grunt.file.exists('src/app/server.json')) {
		grunt.file.copy('src/app/server-default.json', 'src/app/server.json');
	}


	// read REST configuration
	var serverJson = grunt.file.readJSON('src/app/server.json');
	var s = serverJson.server.substring(serverJson.server.indexOf('//') + 2);
	var context = s.split('/')[1];

	var host = 'localhost';
	var port = 80;

	if (context) {
		s = s.substring(0, s.length - context.length - 1);
		host = s.indexOf(':') != -1 ? s.split(':')[0] : s;
		port = s.indexOf(':') != -1 ? s.split(':')[1] : 80;
	}

	var restservice = {
		host: host,
		port: port,
		context: context
	};

	console.log("using REST service", restservice);

	var proxyPort = '7000';

	// Configure Grunt
	grunt.initConfig({

		ts: {
			'default': {
				src: ["src/app/**/*.ts", "!src/app/lib/**/*.ts"],
				tsconfig: "src/tsconfig.json"
			},
			options: {
				compiler: './node_modules/typescript/bin/tsc',
				failOnTypeErrors: true
		    }
		},

		cacheBust: {
			options: {
				encoding: 'utf8',
				algorithm: 'md5',
				length: 16,
				rename: false
			},
			assets: {
				files: [{
					src: ['src/app/index.html']
				}]
			}
		},


		jshint: {
			all: ['Gruntfile.js', 
			      'src/app/services/**/*.js'
			      ],
			      options: {
			    	  jshintrc: '.jshintrc'
			      }
		},

		// grunt-contrib-connect will serve the files of the project
		// on specified port and hostname
		connect: {
			server: {
				//http://www.hierax.org/2014/01/grunt-proxy-setup-for-yeoman.html
				proxies: [{
					context: '/' + restservice.context, // the context of the data service
					host: restservice.host, // wherever the data service is running
					port: restservice.port // the port that the data service is running on
				}],

				options: {
					port: proxyPort,
					hostname: "0.0.0.0",
					// No need for keepalive anymore as watch will keep Grunt running keepalive: true,

					// Livereload needs connect to insert a cJavascript snippet in the pages it serves.
					// This requires using a custom connect middleware
					middleware: function (connect, options) {
						return [
							// Load the middleware provided by the
							// livereload plugin
							// that will take care of inserting the
							// snippet
							// require('grunt-contrib-livereload/lib/utils').livereloadSnippet,
							require('connect-livereload')(), 

							// Serve the project folder
							connect.static(options.base + '/src')];
					}
				}
			},
			coverage: {
				//http://www.hierax.org/2014/01/grunt-proxy-setup-for-yeoman.html
				proxies: [{
					context: '/' + restservice.context, // the context of the data service
					host: restservice.host, // wherever the data service is running
					port: restservice.port // the port that the data service is running on
				}],

				options: {
					port: proxyPort,
					hostname: "0.0.0.0",
					middleware: function (connect, options) {
						return [
							connect.static(options.base + '/instrumented/app')
						];
					}
				}
			}
		},

		// grunt-open will open your browser at the project's URL
		open: {
			all: {
				// Gets the port from the connect configuration
				path: 'http://localhost:<%= connect.server.options.port%>/#/'
			}
		},

		
		watch: {
			
			scripts: {
				files: ['src/app/**/*.html', 'src/app/**/*.css', 'src/app/**/*.js'],
				options: {
					spawn: false,
					livereload: true
				},
			},
			
			ts: {
				files: ["src/app/**/*.ts", "!node_modules/**/*.ts"],
				tasks: ['ts'],
				options: {
					spawn: false,
					livereload: true
				}
			}
		},
		

		clean: {
			coverage: ['instrumented'],
			screenshots: 'test/e2e/output/screenshots/current'
		},

		copy: {
			coverage: {
				files: [{
					expand: true,
					dot: true,
					cwd: 'src/app',
					dest: 'instrumented/app',
					src: [
						'**/*',
					]
				}]
			}
		},

		instrument: {
			files: ['src/app/**/*.js', '!src/app/lib/**/*.js'],
			options: {
				basePath: "instrumented"
			}
		},

		protractor_coverage: {
			options: {
				keepAlive: false,
				noColor: false,
				collectorPort: 3001,
				coverageDir: 'test/e2e/output/report/coverage',
				args: {
					baseUrl: 'http://localhost:<%= connect.server.options.port%>/#/'
				}
			},
			local: {
				options: {
					configFile: 'test/e2e/protractor_conf.js'
				}
			}
		},

		makeReport: {
			src: 'test/e2e/output/report/coverage/*.json',
			options: {
				type: 'lcov',
				dir: 'test/e2e/output/report/coverage',
				print: 'detail'
			}
		}
	});

	// Creates the `server` task
	grunt.registerTask('server', [
		'connect:server',
		'ts', //'connect:livereload',
		// Connect is no longer blocking other tasks, so it makes more sense to open the browser after the server starts
		'open',
		// Starts monitoring the folders and keep Grunt alive
		'watch'
	]);
	

	grunt.registerTask("default", ["server"]);

	grunt.registerTask("dist", ["ts", "cacheBust"]);

	grunt.registerTask('coverage', [
		'clean:coverage',
		'clean:screenshots',
		'copy:coverage',
		'instrument',
		'connect:coverage',
		'protractor_coverage',
		'makeReport'
	]);
};
