/**
 * diffs screenshots from the e2e tests
 * 
 * ImageMagick and GraphicsMagick needed to be installed to execute this script
 * (https://github.com/aheckmann/gm)
 * 
 * compares screenshots inside the latest history directory with the
 * corresponding image (browser and screenshot name) in the previous history
 * directory
 * 
 */

var fs = require('node-fs'),
	path = require('path'),
	gm = require('gm'),
	mkdirp = require('mkdirp'),
	wrench = require('wrench'),
	request = require("sync-request")
;

var log = console.log;

var args = process.argv.slice(2);

if (args.length < 1 && args.length > 3) {
	console.log("command:");
	console.log("node screenshotdiff.js screenshot-base-dir [diff-dir] [previous-diff-dir]");
	console.log("node screenshotdiff.js screenshot-base-dir all");
	return;
}

var screenshotDirPath = args[0];
var screenshotHistoryPath = screenshotDirPath + '/history';


var imageCompareOptions = {
	highlightColor: 'yellow',
	tolerance: 0.00002
};


var fileFilterPng = function(file) {
	if (file.indexOf('.png') != -1) {
		return true;
	}
}

var fileFilterDirectory = function(basepath, file) {
	return 
		file.indexOf('.') !== 0 // ignore hidden directories
		&& 
		fs.statSync(path.join(basepath, file)).isDirectory();
}

var rmDir = function(dirPath) {
	try { 
		var files = fs.readdirSync(dirPath); 
	} catch(e) { 
		log("rmDir error ", e);			
		return; 
	}
	if (files.length > 0) {
		for (var i = 0; i < files.length; i++) {
			var filePath = dirPath + '/' + files[i];
			if (fs.statSync(filePath).isFile())
				fs.unlinkSync(filePath);
			else
				rmDir(filePath);
		}
	}
	fs.rmdirSync(dirPath);
};



var findCorrespondingFile = function(filename, testsuiteName, browser, basedir) {
	if (fs.existsSync(basedir + '/' + browser)) {
		var browsersPath = basedir + '/' + browser;
		
		var testsuites = fs.readdirSync(screenshotLatestPath + '/' + browser);
		for (var testsuite of testsuites) {
			if (testsuite.indexOf('.')===0 || testsuite.indexOf(testsuiteName) === -1)
				continue;
			var testsuitepath = browsersPath+'/'+testsuite;
			if (fs.existsSync(browsersPath + '/' + testsuite)) {
				var screenshots = fs.readdirSync(browsersPath + '/' + testsuite).filter(fileFilterPng);
				for (var screenshot of screenshots) {
					var screenshotname = screenshot.substring(26); // without timestamp
					if (screenshot.indexOf(filename) != -1) {
						return testsuitepath + '/' + screenshot;
					}
				}
			}
		}
	}
};


var writeDiffHtml = function(diffArray) {
	var output = 
		"<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'></script>" +
		"<style>" +
		"body { font-family: sans-serif; }" +
		".row { float: left; width: 100%; }" +
		".row-item { border: 2px solid black; margin: 20px; background-color: grey; float: left; }" +
		".row-item img:nth-child(2) { margin-left: 15px; }" +
		"</style>" +
		"<div style='margin: 20px;'>Click on an image to show the images used for the diff.</div>";

	for (var diff of diffArray) {
		
		if (!diff.isEqual) {
			output += 
				"<div class='row'>" +
				"<div class='row-item' onclick='\$(this).children().show();' >" +
				"<img src='" + diff.diffimage + "' title='" + diff.diffimage + "' />" +
				"<img src='" + diff.second + "' title='vorher: " + diff.second + "' style='display:none;' />" +
				"<img src='" + diff.first + "' title='nachher: " + diff.first + "' style='display:none;'/>" +
				"</div>" +
				"</div>";
		}
	}

	
	fs.writeFileSync(outputBasePath + '/screenshot-differences.html', output); 
}


function doDiff(screenshotLatestPath, previousHistoryDirPath, outputBasePath) {
	
	var browsers = fs.readdirSync(screenshotLatestPath);
	var diffArray = [];
	var nrOfDiffs = 0;
	var compareCounter = 0;
	for (var browser of browsers) {
		if (browser.indexOf('.')===0 || browser.indexOf('.html') !== -1)
			continue;
		var browsersPath = screenshotLatestPath + '/' + browser;
		var testsuites = fs.readdirSync(browsersPath);
		for (var testsuite of testsuites) {
			if (testsuite.indexOf('.')===0)
				continue;
			var testsuitepath = browsersPath + '/' + testsuite;
			var screenshots = fs.readdirSync(browsersPath + '/' + testsuite).filter(fileFilterPng);
			for (var screenshot of screenshots) {
				var screenshotpath = testsuitepath + '/' + screenshot;
				var screenshotpathrelative = '../../history/' + latestHistoryVersionDirName  + '/' + browser  + '/' + testsuite + '/' + screenshot;
				var screenshotname = screenshot.substring(26); // without timestamp
				if (screenshot.indexOf('.png') != -1) {
					var filetodiffwith = findCorrespondingFile(screenshotname, testsuite, browser, previousHistoryDirPath);
					if (filetodiffwith != undefined) {
						var filetodiffwithFilename = filetodiffwith.substring(filetodiffwith.lastIndexOf('/') + 1);
						var filetodiffwithrelative = '../../history/' + previousHistoryVersionDirName  + '/' + browser  + '/' + testsuite + '/' + filetodiffwithFilename;
						nrOfDiffs ++;
						
						(function(browser, testsuite, screenshot, screenshotpath, filetodiffwith, screenshotpathrelative, filetodiffwithrelative) {
						setTimeout(function() {
		
						(function(browser, testsuite, screenshot, screenshotpath, filetodiffwith, screenshotpathrelative, filetodiffwithrelative) {
		
							var outputdir = outputBasePath + '/' + browser + '/' + testsuite;
							var diffoutputpath =  outputdir + '/' + screenshot + '_diff.png'
							mkdirp.sync(outputdir);
							
							imageCompareOptions.file = diffoutputpath;
							
							log("Comparing images: [" + compareCounter++ + "/" + nrOfDiffs + "]", screenshotpath, filetodiffwith, diffoutputpath);
							gm.compare(screenshotpath, filetodiffwith, imageCompareOptions, function (err, isEqual, equality, raw) {
								
								var diffoutputpathrelative =  browser + '/' + testsuite + '/' + screenshot + '_diff.png';
								
								diffArray.push({
									diffimage: diffoutputpathrelative,
									first: screenshotpathrelative,
									second: filetodiffwithrelative,
									isEqual: isEqual
								});
								
								if (diffArray.length + 1 == nrOfDiffs) {
									
									// this will be called when resizing is done
									// this will not be called if no image was found
									// for resizing
									
									writeDiffHtml(diffArray);
									createIndexHtml();
									resizeFiles();
									
								}
								
							});
						
						})(browser, testsuite, screenshot, screenshotpath, filetodiffwith, screenshotpathrelative, filetodiffwithrelative);
						}, 10 * nrOfDiffs + 20);
					})(browser, testsuite, screenshot, screenshotpath, filetodiffwith, screenshotpathrelative, filetodiffwithrelative);
						
					}
				}
			}
		}
	}
}



if (args.length == 1 || args.length == 3) {
	
	var latestHistoryVersionDirName = args[1];
	var previousHistoryVersionDirName = args[2];
	
	
	// if called without arguments use the current and previous dir found in history dir
	if (!latestHistoryVersionDirName && !previousHistoryVersionDirName) {
		latestHistoryVersionDirName = getDirectoriesSortedByDate(screenshotHistoryPath)[0];
		previousHistoryVersionDirName = getDirectoriesSortedByDate(screenshotHistoryPath)[1];
	}
	
	var screenshotLatestPath = screenshotHistoryPath + '/' + latestHistoryVersionDirName;
	var previousHistoryDirPath = screenshotHistoryPath + '/' + previousHistoryVersionDirName;
	var outputBasePath = 		screenshotDirPath + '/diff/' + latestHistoryVersionDirName + '_' + previousHistoryVersionDirName;
	
	
	log("Comparing " + latestHistoryVersionDirName + " with " + previousHistoryVersionDirName);
	
	doDiff(screenshotLatestPath, previousHistoryDirPath, outputBasePath);

} else if (args.length == 2) {
	// build diff for all subfolders - for initial execution

	var historyDirs = getDirectoriesSortedByDate(screenshotHistoryPath);
	for (var d=0;  d < historyDirs.length - 1; d++) {
		
		var latestHistoryVersionDirName = historyDirs[d];
		var previousHistoryVersionDirName = historyDirs[d+1];

		
		var screenshotLatestPath = screenshotHistoryPath + '/' + historyDirs[d];
		var previousHistoryDirPath = screenshotHistoryPath + '/' + previousHistoryVersionDirName;
		var outputBasePath = 		screenshotDirPath + '/diff/' + latestHistoryVersionDirName + '_' + previousHistoryVersionDirName;


		// doDiff(screenshotLatestPath, previousHistoryDirPath, outputBasePath);
		// ^ this will not work because of some async problems, just call it manually - copy/paste
		
		log("node ../nuclos-webclient/test/e2e/screenshotdiff.js", screenshotDirPath, latestHistoryVersionDirName, previousHistoryVersionDirName)	
	}
}







var resizeFilesRecursively = function(dirPath) {
	try { 
		var files = fs.readdirSync(dirPath); 
	} catch(e) { 
		log("resize error", e);			
		return; 
	}
	if (files.length > 0) {
		for (var i = 0; i < files.length; i++) {
			var filePath = dirPath + '/' + files[i];
			if (fs.statSync(filePath).isFile() && filePath.indexOf('.png') !== -1) {
				gm(filePath).resize(400).write(filePath, function (err) {
					if (err) console.log(err);
				});

			} else {
				if (fs.statSync(filePath).isDirectory() && filePath.indexOf('.') !== 0)
					resizeFilesRecursively(filePath);
			}
		}
	}
};


var resizeFiles = function() {
	
	var screenshotDirPathResizedBase = screenshotDirPath +'_resized';
	var screenshotDirPathResized = screenshotDirPathResizedBase + '/diff/' + latestHistoryVersionDirName + '_' + previousHistoryVersionDirName;
	mkdirp.sync(screenshotDirPathResized);
	mkdirp.sync(screenshotDirPathResizedBase + '/history/' + latestHistoryVersionDirName);
	mkdirp.sync(screenshotDirPathResizedBase + '/history/' + previousHistoryVersionDirName);
	
	log("copy generated files to resize dir")
	wrench.copyDirSyncRecursive(screenshotLatestPath,  screenshotDirPathResizedBase + '/history/' + latestHistoryVersionDirName, {forceDelete: true});
	wrench.copyDirSyncRecursive(previousHistoryDirPath,  screenshotDirPathResizedBase + '/history/' + previousHistoryVersionDirName, {forceDelete: true});
	wrench.copyDirSyncRecursive(outputBasePath, screenshotDirPathResized, {forceDelete: true});
	
	console.log("resize")
	resizeFilesRecursively(screenshotDirPathResizedBase + '/history/' + latestHistoryVersionDirName);
	resizeFilesRecursively(screenshotDirPathResizedBase + '/history/' + previousHistoryVersionDirName);
	resizeFilesRecursively(screenshotDirPathResized);
	
	fs.createReadStream(screenshotDirPath + '/index.html').pipe(fs.createWriteStream(screenshotDirPathResizedBase + '/index.html')); // copy file
};


function getDirectoriesSortedByDate(directory) {
	var subdirs = fs.readdirSync(directory);
	
	for (var dir of subdirs) {
		if (dir.indexOf('.') === 0) {
			subdirs.splice(subdirs.indexOf(dir), 1);
		}
	}
	
	subdirs.sort(function(a, b) {
        return fs.statSync(directory + '/' + b).mtime.getTime() - 
               fs.statSync(directory + '/' + a).mtime.getTime();
    });
	
	return subdirs;
}


var getNumberOfDifferences = function(screenshotDifferencesHtmlFilepath) {
	try {
		var data = fs.readFileSync(screenshotDifferencesHtmlFilepath, 'utf8');
		console.log(data);
		var count = (data.match(/\'row-item/g) || []).length;
		return count;
	} catch (error) {
		log('Unable to find file: '+ screenshotDifferencesHtmlFilepath + '. run diff script for all screenshotdirectories.', error);
		return '-';
	}
}


var getGitMessage = function(gitHash) {
	try {
		var jsonResult = JSON.parse(request('GET', 'https://bitbucket.org/api/1.0/repositories/nuclos/nuclos/changesets/' + gitHash).getBody()); 
		return jsonResult;
	} catch (error) {
		log('Unable to get git message for: ' + gitHash, error);
	}
	return '-';
};


var createIndexHtml = function() {
	var historyDirOutput = 
		"<style>" +
			"body { " +
				"font-family: monospace; " +
			"}" +
			"a:link { " +
				"text-decoration: none; " +
			"}" +
			"a:hover { " +
				"text-decoration: underline; " +
			"}" +
			"ul { " +
				"list-style-type: none; " +
			"}" +
		"</style>" +

		"<link href='http://getbootstrap.com/dist/css/bootstrap.min.css' rel='stylesheet'>";
	
	try { 
		var directory = screenshotDirPath + '/history/';
		var historyDirs = getDirectoriesSortedByDate(directory);
		
		historyDirOutput += "<ul>";
		for(var d = 0; d < historyDirs.length; d++) {
			var dir = historyDirs[d]; // dir = git-hash

			var gitMessage = getGitMessage(dir);
			var author = gitMessage.author ? gitMessage.author.trim() : "-";
			var message = gitMessage.author ? gitMessage.message.trim() : "-";
			
			historyDirOutput += 
				"<li>" +
					"<a href='https://bitbucket.org/nuclos/nuclos/commits/" + dir + "' title='open in bitbucket' >" +
							"["+ dir + "] " +
					"</a> " +
					"<a href='history/" + dir + "' title='open screenshots' >" +
						"<span style='color: black;'>"+ gitMessage.timestamp + "</span> &nbsp; " + author + ': '  + message + 
					"</a> " +
					"<br/>" +
				"</li>";
			
			
			if (d + 1 < historyDirs.length) {
				var diffDir = dir + '_' + historyDirs[d + 1];
				var numberOfDiffs = getNumberOfDifferences("screenshots/diff/" + diffDir + "/screenshot-differences.html");
	
				historyDirOutput += 
					"<li>" +
						"<a href='diff/" + diffDir + "/screenshot-differences.html' style='font-size: large;'>" + 
							"<span class='glyphicon glyphicon-resize-vertical' aria-hidden='true'></span>" + 
							"<span title='" + numberOfDiffs + " different screenshots'>" + numberOfDiffs + "</span>"+ 
						"</a>" +
					"</li>"
					;
			}
		}
		historyDirOutput += "</ul>";
	} catch(e) { 
		log("history dir list error", e);			
		return; 
	}
	
	fs.writeFileSync(screenshotDirPath + '/index.html', historyDirOutput); 
};
