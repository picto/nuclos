var gulp = require('gulp');

var url = require('url'); // https://www.npmjs.org/package/url
var proxy = require('proxy-middleware'); // https://www.npmjs.org/package/proxy-middleware
var browserSync = require('browser-sync'); // https://www.npmjs.org/package/browser-sync


var restservice = {
	context: 'nuclos-war',
	port: '8080'
};

var proxyPort = '7000';



var paths =  {
    css: ['./**/*.css', './**/*.js', './**/*.html', '!./node_modules/**/*']
};


// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    var proxyOptions = url.parse('http://localhost:' + restservice.port + '/' + restservice.context + '/rest');
    proxyOptions.route = '/rest';

    browserSync({
        open: true,
        port: proxyPort,
        server: {
            baseDir: "./app",
            middleware: [proxy(proxyOptions)]
        }
    });
});

// Stream the style changes to the page
gulp.task('reload-css', function() {
    gulp.src(paths.css)
        .pipe(browserSync.reload({stream: true}));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(paths.css, ['reload-css']);
});

// Default Task
gulp.task('default', ['browser-sync', 'watch']);
