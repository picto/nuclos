/// <reference path="nuclos.d.ts" />
/// <reference path="angularjs/angular.d.ts" />
/// <reference path="jquery/jquery.d.ts" />
/// <reference path="angularjs/angular-resource.d.ts" />
/// <reference path="angularjs/angular-route.d.ts" />
/// <reference path="angular-translate/angular-translate.d.ts" />
/// <reference path="angularjs/angular-cookies.d.ts" />
/// <reference path="moment/moment-node.d.ts" />
/// <reference path="moment/moment.d.ts" />
/// <reference path="angular-ui-bootstrap/angular-ui-bootstrap.d.ts" />
/// <reference path="ui-grid/ui-grid.d.ts" />
/// <reference path="ace/ace.d.ts" />
/// <reference path="d3/d3.d.ts" />

// TODO: Get rid of global variables!

declare var nuclosApp: nuclos.INuclosApp;

declare var restHost: string;

declare var log: any;
declare var errorhandler: nuclos.Errorhandler;

declare var getShortFieldName: Function;

// FileSaver
declare var saveAs: Function;

declare var setTitle: Function;
declare var sessionHeader: Function;

declare var chunkSize: number;
declare var vlpParams: any;

declare var ace;

declare var logStackTrace: Function;
declare var printJSON: Function;
declare var URI: Function;
declare var defaultMenu: string;
declare var developmentMode;
declare var restservices;
declare var defMaxMenuEntries: number;
declare var nuclosServer;
declare var BoViewModel;

// message service stuff
declare var eventSource;
declare var EventSource;
declare var ruleDirectionHandler;
declare var ruleNotificationHandler;

declare var printStackTrace: Function;
