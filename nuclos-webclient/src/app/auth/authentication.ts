module nuclos.auth {
	export interface IAuthentication {
		allowedActions: any;
		superUser?: boolean;
	}
}