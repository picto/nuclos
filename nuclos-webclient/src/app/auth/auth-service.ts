module nuclos.auth {
	import Mandator = nuclos.model.Mandator;
	export class AuthService {
		authCache = null;

		private mandators: Mandator[];
		private currentMandator: Mandator;

		static instance: AuthService = null;

		constructor(private $rootScope,
					private $resource,
					private $q,
					private $cookieStore,
					private $cookies,
					private $cacheFactory,
					private dialog) {
			this.authCache = this.$cacheFactory('authCache');
			AuthService.instance = this;
		}

		loginAsAnonymous() {
			console.log("LOGIN AS ANONYMOUS");
			var loginData = {
				"username": "anonymous",
				"password": "anonymous",
				"locale": "de"
			}
			return this.authenticate(loginData);
		}

		isAnonymous(data?) {
			if (!data) {
				return this.getUsername() == 'anonymous';
			}

			return data['username'] == 'anonymous';
		};

		loginSuccessful(data) {
			console.log("LOGIN SUCCESSFUL");

			this.currentMandator = data.mandator;
			this.mandators = data.mandators;

			// SAVE COOKIE
			this.$cookieStore.put('authentication', data);
			setTitle(this.$cookieStore);

			var redirect = this.$cookies.get('redirect');
			if (redirect) {
				this.$cookies.remove('redirect');
				window.location.href = redirect;
			} else {
				window.location.href = defaultMenu;
			}
		};

		authenticate(loginData) {
			console.log("LOGIN AS ", loginData.username);
			var deferred = this.$q.defer();

			this.$resource(restHost).save(loginData,
				data => {
					this.mandators = data.mandators;
					deferred.resolve(data);
				},
				error => {
					var textStatus = error.status;
					var jqxhr = error.data;
					console.error(textStatus, jqxhr);
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

		lastLocationRedirect(data) {
			var deferred = this.$q.defer();

			if (!this.isAnonymous(data) && !this.hasRedirectCookie()) {
				this.setRedirectToLastLocation(data.username);
			}
			this.loginSuccessful(data);
			deferred.resolve(data);

			return deferred.promise;
		}

		selectMandator(mandator) {
			var deferred = this.$q.defer();
			this.$resource(mandator.links.chooseMandator.href).save({},
				(data) => {
					this.currentMandator = mandator;
					return deferred.resolve(data)
				},
				(error) => deferred.reject(error)
			);
			return deferred.promise;
		}

		getCurrentMandator(): Mandator {
			return this.currentMandator;
		}

		autologin() {
			var deferred = this.$q.defer();

			this.$resource(restHost).get(null,
				data => {
					if (!this.isAnonymous(data) && !this.hasRedirectCookie()) {
						this.setRedirectToLastLocation(data.username);
					}
					this.loginSuccessful(data);
					deferred.resolve(data);
				},
				error => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

		clearCache() {
			this.authCache.removeAll();
		}

		isAnonymousUserAccessEnabled() {
			console.log("$rootScope.isAnonymousUserAccessEnabled=", this.$rootScope.isAnonymousUserAccessEnabled);
			return this.$rootScope.isAnonymousUserAccessEnabled
		}

		isDevMode() {
			return this.$rootScope.isDevMode;
		}

		isStartOrLoginPage(locationPath) {
			return (locationPath.indexOf('/login') == -1
				&& locationPath.indexOf('/logout') == -1
				&& locationPath != ""
				&& locationPath != "/") == false;
		}

		hasSessionId() {
			return this.getSessionId() !== undefined;
		}

		public getSessionId(): string {
			var authentication = this.$cookieStore.get('authentication');
			return authentication ? authentication.sessionId : undefined;
		}

		hasRedirectCookie() {
			return this.getRedirectCookie() !== undefined;
		}

		setRedirectCookie(locationPath) {
			if (!this.isStartOrLoginPage(locationPath)) {
				this.$cookies.put('redirect', '#' + locationPath);
			}
		}

		removeRedirectCookie() {
			this.$cookies.remove('redirect');
		}

		getRedirectCookie() {
			var redirect = this.$cookies.get('redirect');
			return redirect ? redirect : undefined;
		}

		delRedirectCookie() {
			var redirect = this.$cookies.get('redirect');
			if (redirect) {
				this.$cookies.remove(redirect);
			}
		}

		invalidateSessionId() {
			var auth = this.$cookieStore.get('authentication');
			if (auth) {
				delete auth.sessionId;
				this.$cookieStore.put('authentication', auth);
			}
		}

		solve401(windowLocationHref) {
			// --> MATRX FOR 401
			this.invalidateSessionId();
			if (this.hasSessionId()) { // 401 AND SESSION IS SESSION-TIMEOUT
				// TODO RE-LOGIN WITH CURRENT CREDENCIALs
				this.setRedirectCookie(windowLocationHref);
				window.location.href = '#/login';
			} else {
				this.setRedirectCookie(windowLocationHref);
				window.location.href = '#/login';
			}
		}

		solve403(windowLocationHref) {
			//TODO MATRX FOR 403
		}

		solveAppRun() {
			//TODO MATIX
		}

		/**
		 * Sets the redirect cookie for a redirect to the last location that is stored via the "location" cookie.
		 *
		 * @returns {boolean}
		 */
		setRedirectToLastLocation(username) {
			var lastLocation = this.$cookies.get('location.' + username);
			if (lastLocation) {
				console.log('Redirecting to last location: %o', lastLocation);
				this.setRedirectCookie(lastLocation);
				return true;
			}
			return false;
		}

		resetLastLocation() {
			this.$cookies.remove('location.' + this.getUsername());
		}

		showLegalDisclaimer(name) {
			this.getLegalDisclaimers().then(data => {
				for (var i in data) {
					if (data[i].name == name) {
						this.dialog.alert(null, data[i].text, null, 'halfwidth-modal-window');
						break;
					}
				}
			});
		}

		getLegalDisclaimers() {
			var deferred = this.$q.defer();
			var key = "legaldisclaimers";

			var legalDisclaimersFromCache = this.authCache.get(key);
			if (legalDisclaimersFromCache) {
				deferred.resolve(legalDisclaimersFromCache);
				return deferred.promise;
			}

			this.$resource(restHost + '/version/legaldisclaimer').query(null,
				data => {
					this.authCache.put(key, data);
					deferred.resolve(data);
				},
				error => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

		hasLegalDisclaimer(name, disclaimers) {
			if (disclaimers) {
				for (var i in disclaimers) {
					if (disclaimers[i].name == name) {
						return true;
					}
				}
			}
			return false;
		}

		getUsername() {
			var username = this.$cookieStore.get('authentication') ? this.$cookieStore.get('authentication').username : null;
			return username;
		}


		/**
		 * Determines if the given Action is allowed for the current user.
		 *
		 * @param action
		 */
		isActionAllowed(action: Action): boolean {
			let actionName = Action[action];
			return !!this.getAuthentication().allowedActions[actionName] || this.isSuperUser();
		}

		/**
		 * Returns the current authentication object.
		 *
		 * @returns {IAuthentication}
		 */
		public getAuthentication(): IAuthentication {
			return this.$cookieStore.get('authentication');
		}

		/**
		 * Determines if the current user is a superuser.
		 */
		public isSuperUser(): boolean {
			return this.getAuthentication().superUser;
		}

		public getMandators(): Mandator [] {
			return this.mandators !== undefined ? this.mandators : [];
		}

	}

	nuclosApp.service('authService', AuthService);
}
