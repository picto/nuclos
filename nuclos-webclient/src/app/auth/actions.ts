module nuclos.auth {

	/**
	 * An enum of all webclient-specific actions.
	 */
	export enum Action {
		SharePreferences,
		ConfigureCharts,
		ConfigurePerspectives
	}
}