module nuclos.perspective {

	import PreferenceService = nuclos.preferences.PreferenceService;
	import IPreferenceFilter = nuclos.preferences.IPreferenceFilter;
	import IPreference = nuclos.preferences.IPreference;
	import IModalService = angular.ui.bootstrap.IModalService;
	import IModalServiceInstance = angular.ui.bootstrap.IModalServiceInstance;
	import IScope = angular.IScope;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
	import IPromise = angular.IPromise;
	import Action = nuclos.auth.Action;

	export interface IPerspective {
		name: string;
		boMetaId: string;
		layoutId: string;
		searchTemplatePrefId: string;
		sideviewMenuPrefId: string;

		// TODO: Subform columns pref

		layoutForNew: boolean;

		selected?: boolean;
	}

	export class PerspectiveCtrl {
		static id = 'PerspectiveCtrl';

		visible = false;

		vm = this;

		constructor(private dialog: nuclos.dialog.DialogService,
					private $uibModal: IModalService,
					private perspectiveService: PerspectiveService,
					private $rootScope: ng.IScope,
					private $log: ng.ILogService,
					private authService: nuclos.auth.AuthService,
					private $q: ng.IQService) {
			this.perspectiveService.init().then(
				() => {
					this.visible = this.getModel().perspectives.length > 0 || this.isEditAllowed();
					this.applyPerspective(this.perspectiveService.getSelectedPerspective());
				}
			);
		}

		private getModel(): IPerspectiveModel {
			return this.perspectiveService.getModel();
		}

		public openNew() {
			let newPerspective: IPerspective = {
				name: null,
				boMetaId: this.perspectiveService.getBoMetaId(),
				layoutId: null,
				searchTemplatePrefId: null,
				sideviewMenuPrefId: null,
				layoutForNew: true
			};

			this.openModal(newPerspective).then(
				(perspective: IPerspective) => {
					let perspectivePrefs: IPreference<IPerspective> = {
						boMetaId: this.perspectiveService.getBoMetaId(),
						type: "perspective",
						name: perspective.name,
						content: perspective
					};
					this.perspectiveService.save(perspectivePrefs);
				}
			);
		}

		/**
		 * Opens an edit modal for the given perspective.
		 *
		 * @param perspectivePref
		 */
		public editPerspective(perspectivePref: IPreference<IPerspective>) {
			this.openModal(perspectivePref.content).then(
				(perspective: IPerspective) => {
					this.perspectiveService.save(perspectivePref)
				}
			);
		}

		public deletePerspective(perspectivePref: IPreference<IPerspective>) {
			this.perspectiveService.delete(perspectivePref)
		}

		openModal(perspective: IPerspective): IPromise<IPerspective> {
			this.$log.debug('Editing perspective: %o', perspective);

			let deferred = this.$q.defer();

			this.perspectiveService.fetchAllPreferences().then(
				() => {
					let modal: IModalServiceInstance = this.$uibModal.open({
						templateUrl: 'app/perspective/perspective-edit.html',
						controller: PerspectiveModalCtrl.id + ' as vm',
						size: 'lg',
						windowClass: 'halfwidth-modal-window',
						resolve: {
							// this will be injected into PerspectiveCtrl constructor
							model: this.perspectiveService.getModel(),
							perspective: perspective,
							perspectiveController: this
						}
					});

					this.dialog.registerModalInstance(modal);

					modal.result.then(
						(result: IPerspective) => {
							this.$log.debug('New perspective: %o', result);
							deferred.resolve(perspective);
						},
						error => {
							this.$log.error(error);
							deferred.reject(error);
						}
					);
				},
				error => {
					deferred.reject(error);
				}
			)

			return deferred.promise;
		};


		/**
		 * Toggles the selection state of the given perspective and applies it.
		 *
		 * @param perspective
		 */
		public togglePerspective(perspective: IPerspective): void {
			this.getModel().togglePerspective(perspective);
			this.applyPerspective(perspective);
			this.perspectiveService.saveChanges().then(
				() => this.$log.debug('Saved perspective changes'),
				error => this.$log.error('Could not save perspective changes: %o', error)
			);
		}

		/**
		 * Applies all preferences of the given perspective to the respective components.
		 *
		 * @param perspective
		 */
		public applyPerspective(perspective: IPerspective) {
			if (!perspective) {
				return;
			}

			this.$log.debug('Applying perspective: %o', perspective);

			if (!perspective.selected) {
				// TODO: "Unapply" the perspective?
				return;
			}

			let layoutInfo = this.getModel().getLayoutInfo(perspective.layoutId);
			this.$log.debug('layout: %o -> %o', perspective.layoutId, layoutInfo);
			this.applyLayout(layoutInfo);

			let searchTemplate = this.getModel().getSearchTemplate(perspective.searchTemplatePrefId);
			this.applySearchTemplate(searchTemplate);

			let sideviewMenu = this.getModel().getSideviewMenu(perspective.sideviewMenuPrefId);
			this.applySideviewMenu(sideviewMenu);
		}

		/**
		 * Applys the given layout to the details view.
		 *
		 * @param layout
		 */
		private applyLayout(layout: ILayoutInfo) {
			this.perspectiveService.applyLayout(layout);
		};

		/**
		 * Applies the given search template to the search bar.
		 *
		 * @param searchTemplate
		 */
		public applySearchTemplate(searchTemplate: IPreference<any>) {
			if (!searchTemplate) {
				return;
			}

			this.$log.debug('Applying search template: %o', searchTemplate);

			// Defensive copy
			searchTemplate = angular.copy(searchTemplate);

			// TODO: Ugly
			searchTemplate.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);

			this.$rootScope.$broadcast('searchTemplateChanged', searchTemplate);
		};

		/**
		 * Applies the given sideview-menu prefs.
		 *
		 * @param sideviewMenu
		 */
		public applySideviewMenu(sideviewMenu: IPreference<any>) {
			if (!sideviewMenu) {
				return;
			}

			this.$log.debug('Applying sideview menu: %o', sideviewMenu);

			// Defensive copy
			sideviewMenu = angular.copy(sideviewMenu);

			// TODO: Ugly
			sideviewMenu.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);

			this.$rootScope.$broadcast('sideviewMenuChanged', sideviewMenu);
		};

		public isNewAllowed() {
			return this.authService.isActionAllowed(Action.ConfigurePerspectives);
		}

		public isEditAllowed() {
			return this.authService.isActionAllowed(Action.ConfigurePerspectives);
		}

		public mouseover(perspective: IPerspective) {
			this.$log.debug('Mouseover: %o', perspective);
		}
	}

	export class PerspectiveModalCtrl {
		static id = 'PerspectiveModalCtrl';

		constructor(private model: IPerspectiveModel,
					private perspective: IPerspective,
					private perspectiveController: PerspectiveCtrl,
					private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance,
					private $log: ng.ILogService) {
			this.$log.debug(model)
		}

		closeModal() {
			this.$uibModalInstance.close();
		}

		/**
		 * Returns the result to the parent controller.
		 */
		savePerspective() {
			this.$uibModalInstance.close(this.perspective);
		}

		/**
		 * Applies the perspective to the parent controller.
		 */
		public applyPerspective() {
			// Mark the perspective temporarily as selected
			this.perspective.selected = true;
			this.perspectiveController.applyPerspective(this.perspective);
			this.perspective.selected = false;
		}
	}

	nuclosApp.controller(PerspectiveCtrl.id, PerspectiveCtrl);
	nuclosApp.controller(PerspectiveModalCtrl.id, PerspectiveModalCtrl);
}
