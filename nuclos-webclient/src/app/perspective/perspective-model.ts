/**
 * Common model for perspective controller and service to store information about available perspectives and
 * preferences for the currently selected entity.
 */
module nuclos.perspective {
	import IPreference = nuclos.preferences.IPreference;

	export interface IPerspectiveModel {
		/**
		 * Existing perspectives for this BO.
		 */
		readonly perspectives: Array<IPreference<IPerspective>>;

		/**
		 * The currently toggled perspective, if any.
		 */
		selectedPerspective?: IPerspective;

		/**
		 * Other available prefs for the current BO.
		 * These are used to configure new perspectives.
		 */
		searchTemplatePrefs?: Array<IPreference<any>>;
		sideviewMenuPrefs?: Array<IPreference<any>>;
		layoutInfos?: Array<ILayoutInfo>;

		// TODO: Subform columns pref

		getSearchTemplatePrefs: (perspective: IPerspective) => Array<IPreference<any>>;
		getSideviewMenuPrefs: (perspective: IPerspective) => Array<IPreference<any>>;

		getSearchTemplate(searchTemplatePrefId: string): IPreference<any>;
		getSideviewMenu(prefId: string): IPreference<any>;
		getLayoutInfo(layoutUID: string): ILayoutInfo;

		/**
		 * Selects the given perspective, deselects all other perspectives.
		 *
		 * @param perspective
		 */
		togglePerspective(perspective: IPerspective);

		removePerspective(perspectivePref: IPreference<IPerspective>);

		/**
		 * Adds the given perspective to the model, if it is new.
		 * Updates the perspective in the model, if it is only modified.
		 *
		 * @param perspectivePref
		 */
		addPerspective(perspectivePref: nuclos.preferences.IPreference<IPerspective>): void;

		/**
		 * Determines if there are any changes that need to be persistet.
		 */
		isDirty(): boolean;

		/**
		 * Returns all modified perspectives.
		 */
		getDirtyPerspectives(): Array<IPreference<IPerspective>>;

		/**
		 * Marks this model as not dirty (after changes were persistet).
		 */
		clearDirty();

		/**
		 * Setter method for the perspectives of this model.
		 * If a perspective in the given array has the "selected" flag, it is set as selectedPerspective.
		 *
		 * @param perspectives
		 */
		setPerspectives(perspectives: Array<IPreference<IPerspective>>);

		/**
		 * Sets some default values.
		 * TODO: There should be a Perspective class which does this on its own.
		 *
		 * @param perspective
		 */
		setDefaultValues(perspective: IPerspective);
	}

	export class PerspectiveModel implements IPerspectiveModel {
		perspectives: Array<IPreference<IPerspective>> = [];
		selectedPerspective: IPerspective = null;

		searchTemplatePrefs: Array<IPreference<any>> = [];
		sideviewMenuPrefs: Array<IPreference<any>> = [];
		layoutInfos: Array<ILayoutInfo> = [];

		dirtyPerspectives: Array<IPreference<IPerspective>> = [];

		prefsCache = {};

		constructor(private $log: ng.ILogService) {
		}

		public getSearchTemplate(searchTemplatePrefId: string) {
			if (!searchTemplatePrefId) {
				return null;
			}

			for (let pref of this.searchTemplatePrefs) {
				if (pref.prefId === searchTemplatePrefId) {
					return pref;
				}
			}

			return null;
		}

		getSideviewMenu(prefId: string): nuclos.preferences.IPreference<any> {
			if (!prefId) {
				return null;
			}

			for (let pref of this.sideviewMenuPrefs) {
				if (pref.prefId === prefId) {
					return pref;
				}
			}

			return null;
		}

		public getLayoutInfo(layoutUID: string): nuclos.perspective.ILayoutInfo {
			if (!layoutUID) {
				return null;
			}

			for (let info of this.layoutInfos) {
				if (info.uid.uid == layoutUID) {
					return info;
				}
			}

			return null;
		}

		togglePerspective(perspective: nuclos.perspective.IPerspective) {
			let selected = false;
			for (let p of this.perspectives) {
				if (p.content == perspective) {
					p.content.selected = !p.content.selected;
					this.selectedPerspective = perspective;
					selected = this.selectedPerspective.selected;
					this.dirtyPerspectives.push(p);
				}
				else {
					if (p.content.selected) {
						this.dirtyPerspectives.push(p);
					}
					p.content.selected = false;
				}
			}

			// If the current perspective is not selected anymore, set it to null
			if (!selected) {
				this.selectedPerspective = null;
			}

			this.$log.debug('selected perspective: %o', this.selectedPerspective);
		}

		removePerspective(perspectivePref: IPreference<IPerspective>) {
			let index = this.perspectives.indexOf(perspectivePref);
			if (index >= 0) {
				this.perspectives.splice(index, 1);
			}
			this.$log.debug('Removed perspective %o at index %o from model %o', perspectivePref, index, this);
		}

		addPerspective(perspectivePref: nuclos.preferences.IPreference<nuclos.perspective.IPerspective>): void {
			this.setDefaultValues(perspectivePref.content);

			let existingPref = this.findPerspectivePref(perspectivePref.prefId);
			if (existingPref) {
				let index = this.perspectives.indexOf(existingPref);
				this.perspectives[index] = perspectivePref;
				this.$log.debug('updated perspective pref: %o', perspectivePref);
			} else {
				this.perspectives.push(perspectivePref);
			}
		}

		findPerspectivePref(prefId: string) {
			for (let pref of this.perspectives) {
				if (pref.prefId == prefId) {
					return pref;
				}
			}
			return null;
		}

		isDirty(): boolean {
			return this.getDirtyPerspectives().length > 0;
		}

		getDirtyPerspectives(): Array<nuclos.preferences.IPreference<nuclos.perspective.IPerspective>> {
			return this.dirtyPerspectives;
		}

		clearDirty() {
			this.dirtyPerspectives = [];
		}

		setPerspectives(perspectives: Array<nuclos.preferences.IPreference<nuclos.perspective.IPerspective>>) {
			this.perspectives = perspectives;

			for (let p of perspectives) {
				this.setDefaultValues(p.content);
				if (p.content.selected) {
					this.selectedPerspective = p.content;
					this.$log.debug('selectedPerspective = %o', this.selectedPerspective);
				}
			}
		}

		getSideviewMenuPrefs(perspective: IPerspective): Array<nuclos.preferences.IPreference<any>> {
			return this.sideviewMenuPrefs;
		}

		getSearchTemplatePrefs(perspective: IPerspective): Array<nuclos.preferences.IPreference<any>> {
			return this.searchTemplatePrefs;
		}

		setDefaultValues(perspective: IPerspective) {
			// Layout is always also for new, if not explicitly set to false
			perspective.layoutForNew = perspective.layoutForNew !== false;
		}

		/**
		 * Returns the selected sideview menu pref ID, or null.
		 *
		 * @returns {any}
		 */
		private getSelectedSideviewMenuPrefId(): string {
			if (this.selectedPerspective) {
				return this.selectedPerspective.sideviewMenuPrefId;
			}

			return null;
		}

		/**
		 * Returns the selected searchtemplate pref ID, or null.
		 *
		 * @returns {any}
		 */
		private getSelectedSearchTemplatePrefId(): string {
			if (this.selectedPerspective) {
				return this.selectedPerspective.searchTemplatePrefId;
			}

			return null;
		}
	}
}
