module nuclos.perspective {

	import PreferenceService = nuclos.preferences.PreferenceService;
	import IPreferenceFilter = nuclos.preferences.IPreferenceFilter;
	import IPreference = nuclos.preferences.IPreference;
	import IModalService = angular.ui.bootstrap.IModalService;
	import IPromise = angular.IPromise;
	import Bo = nuclos.model.interfaces.Bo;

	export interface ILayoutInfo {
		description: string;
		uid: UID;
		nucletUID: UID;
		name: string;
		fqn: string;
	}

	export class PerspectiveService {
		model: IPerspectiveModel;
		master: nuclos.model.interfaces.BoViewModel;

		constructor(private metaService: nuclos.core.MetaService,
					private preferences: PreferenceService,
					private $q: ng.IQService,
					private layoutService: nuclos.layout.LayoutService,
					private dataService: nuclos.core.DataService,
					private $log: ng.ILogService) {
			this.model = new PerspectiveModel(this.$log);
		}

		public init(): IPromise<IPerspectiveModel> {
			let deferred = this.$q.defer();

			this.master = this.dataService.getMaster();

			this.layoutService.setPerspectiveModel(this.model);

			this.$q.all(
				[
					this.fetchLayouts(),
					this.fetchAllPreferences(),
					this.fetchPerspectives()
				]
			).then(
				() => {
					deferred.resolve();
				},
				error => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

		public getSelectedBo(): Bo {
			return this.master.selectedBo;
		}

		public getBoMetaId() {
			return this.master.boMetaId;
		}

		public getModel(): IPerspectiveModel {
			return this.model;
		}

		/**
		 * Fetches all available layouts for this BO.
		 */
		public fetchLayouts(): IPromise<ILayoutInfo[]> {
			let deferred = this.$q.defer();

			this.fetchLayoutsAsync(this.master.boMetaId).then(
				(data: ILayoutInfo[]) => {
					this.$log.debug(data);
					this.model.layoutInfos = data;
					deferred.resolve(data);
				},
				error => {
					this.$log.error(error);
					deferred.reject(error);
				}
			);

			return deferred.promise;
		};

		// TODO: Instead of loading complete preferences, load only necessary infos (UID, name)
		public fetchAllPreferences(): IPromise<IPreference<any>[]> {
			let deferred = this.$q.defer();

			this.fetchAllPreferencesAsync(this.master.boMetaId).then(
				(prefs: Array<IPreference<any>>) => {
					this.model.sideviewMenuPrefs = prefs.filter(function (pref: IPreference<any>) {
						let prefType: nuclos.preferences.PreferenceType = 'table';
						return pref.type == prefType;
					});
					this.model.searchTemplatePrefs = prefs.filter(function (pref: IPreference<any>) {
						let prefType: nuclos.preferences.PreferenceType = 'searchtemplate';
						return pref.type == prefType;
					});
					deferred.resolve(prefs);
				},
				(error) => {
					this.$log.error(error);
					deferred.reject(error);
				}
			);

			return deferred.promise;
		};

		/**
		 * Fetches the available perspectives for this BO.
		 */
		private fetchPerspectives(): IPromise<Array<IPreference<IPerspective>>> {
			var deferred = this.$q.defer();

			this.fetchPerspectivesAsync(this.master.boMetaId).then(
				perspectives => {
					this.$log.debug('Perspectives: %o', perspectives);
					this.model.setPerspectives(perspectives);
					deferred.resolve(perspectives);
				},
				error => {
					deferred.reject(error);
				}
			)

			return deferred.promise;
		}

		/**
		 * Retrieves all available layouts for the given BO.
		 *
		 * @param boMetaId
		 * @returns {IPromise<T>}
		 */
		private fetchLayoutsAsync(boMetaId: string): IPromise<Array<ILayoutInfo>> {
			var deferred = this.$q.defer();

			this.metaService.getLayoutsForEntity(boMetaId).then(
				(data: ILayoutInfo[]) => {
					deferred.resolve(data);
				},
				error => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		};

		/**
		 * Retrieves all preferences for the given BO that are relevant for perspectives.
		 *
		 * @param boMetaId
		 * @returns {IPromise<T>}
		 */
		private fetchAllPreferencesAsync(boMetaId: string): IPromise<Array<IPreference<any>>> {
			var deferred = this.$q.defer();

			var filter: IPreferenceFilter = {
				boMetaId: boMetaId,
				type: ['searchtemplate', 'table']
			};

			this.preferences.getPreferences(filter).then(
				(prefs) => {
					this.$log.debug('all prefs: %o', prefs);
					deferred.resolve(prefs);
				},
				(error) => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		};

		/**
		 * Retrieves all available perspectives for the given BO.
		 *
		 * @param boMetaId
		 * @returns {IPromise<T>}
		 */
		public fetchPerspectivesAsync(boMetaId: string): IPromise<Array<IPreference<IPerspective>>> {
			var deferred = this.$q.defer();

			var filter: IPreferenceFilter = {
				boMetaId: boMetaId,
				type: ['perspective']
			};

			this.preferences.getPreferences(filter).then(
				prefs => {
					deferred.resolve(prefs);
				},
				error => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

		/**
		 * Inserts or updates the given perspective preferences.
		 *
		 * @param perspectivePrefs
		 */
		public save(perspectivePref: IPreference<IPerspective>): IPromise<IPreference<IPerspective>> {
			var deferred = this.$q.defer();

			this.model.setDefaultValues(perspectivePref.content);

			if (!this.isValidPerspective(perspectivePref.content)) {
				this.$log.warn('Invalid perspective');
				return;
			}

			perspectivePref.name = perspectivePref.content.name;

			this.preferences.savePreferenceItem(perspectivePref).then(
				savedPerspectivePrefs => {
					this.$log.debug('pref id %o, saved id %o', perspectivePref.prefId, savedPerspectivePrefs.prefId);
					if (!perspectivePref.prefId) {
						perspectivePref.prefId = savedPerspectivePrefs.prefId;
					}
					this.getModel().addPerspective(perspectivePref);
					deferred.resolve(perspectivePref);
				},
				error => {
					this.$log.warn('Could not save perspective: %o', error);
					deferred.reject(error);
				}
			)

			return deferred.promise;
		}

		/**
		 * Deletes the given perspective preferences and removes it from the model.
		 *
		 * @param perspectivePrefs
		 */
		public delete(perspectivePref: IPreference<IPerspective>): IPromise<void> {
			return this.preferences.deletePreferenceItem(perspectivePref).$promise.then(
				data => {
					this.model.removePerspective(perspectivePref);
				},
				error => {
					this.$log.warn('Could not save perspective: %o', error);
				}
			)
		}

		/**
		 * Checks if the given perspective is valid.
		 * I.e. it contains a boMetaId, a name and at least one configured property (layout, sideview menu, search template, ...).
		 *
		 * @param perspective
		 * @returns {boolean}
		 */
		public isValidPerspective(perspective: IPerspective): boolean {
			return !!(
				perspective
				&& perspective.boMetaId
				&& perspective.name
				&& (perspective.layoutId || perspective.searchTemplatePrefId || perspective.sideviewMenuPrefId)
			);
		}

		/**
		 * Changes the currently displayed layout of the given record/entity.
		 * A currently selected perspective may prevent the layout from being switched.
		 *
		 * @param bo Should be the currently displayed record of entity
		 * @param entity
		 * @param layoutURL
		 */
		public applyLayout(layout: ILayoutInfo) {
			if (!layout) {
				return;
			}

			let currentBo = this.getSelectedBo();

			if (!currentBo) {
				this.$log.debug('No BO selected');
				return;
			}

			if (currentBo.boMetaId != this.getBoMetaId()) {
				this.$log.debug('BO meta ID does not match entity meta ID');
				return;
			}

			let layoutURL = this.layoutService.getLayoutURLForFQN(layout.fqn);

			this.$log.debug('Selecting layout: %o', layoutURL);
			this.layoutService.switchLayout(
				currentBo,
				this.master,
				layoutURL,
				true
			);
		};

		/**
		 * Sets the given perspective as the selected perspective.
		 *
		 * @param perspective
		 */
		public selectPerspective(perspective: IPerspective) {
			this.model.selectedPerspective = perspective;
		}

		/**
		 * Returns the selected perspective, or null if no perspective is selected.
		 *
		 * @returns {IPerspective}
		 */
		public getSelectedPerspective(): IPerspective {
			return this.model.selectedPerspective;
		}

		/**
		 * Returns the search template preference for the currently selected perspective, or null.
		 *
		 * @returns {IPerspective}
		 */
		public getSelectedSearchTemplate(): IPreference<any> {
			if (this.getSelectedPerspective() && this.getSelectedPerspective().searchTemplatePrefId) {
				return this.model.getSearchTemplate(this.getSelectedPerspective().searchTemplatePrefId);
			}

			return null;
		}

		/**
		 * Returns the selected sideview menu preference ID, or null if no perspective is selected.
		 *
		 * @returns {string}
		 */
		public getSelectedSideviewMenuPrefId(): string {
			if (!this.model.selectedPerspective) {
				return null;
			}
			return this.model.selectedPerspective.sideviewMenuPrefId;
		}

		/**
		 * Persists all changes of the model.
		 *
		 * @returns {IPromise<T[]>|IPromise<{}>|IPromise<T>}
		 */
		public saveChanges(): IPromise<any> {
			let dirtyPerspectives = this.getModel().getDirtyPerspectives();
			let promises = [];

			for (let p of dirtyPerspectives) {
				let promise = this.preferences.savePreferenceItem(p);
				promises.push(promise);
				promise.then(
					(newPref: IPreference<IPerspective>) => {
						/**
						 * If the pref was customized, it has a new ID now.
						 * In this case, replace the old pref with the new one.
						 */
						if (p.prefId && newPref.prefId && p.prefId != newPref.prefId) {
							this.$log.debug('Replacing pref %o with pref %o', p.prefId, newPref.prefId);
							this.getModel().removePerspective(p);
							this.getModel().addPerspective(newPref);
						}
					}
				)
			}

			return this.$q.all(promises).then(
				() => this.getModel().clearDirty()
			);
		}
	}

	nuclosApp.service("perspectiveService", PerspectiveService);
}
