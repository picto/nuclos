module nuclos.home {
	export class HomeCtrl {
		constructor(
			private $scope,
			private authService: nuclos.auth.AuthService
		) {
			console.log("HOME CTRL");
			console.log("HOME CTRL isDevMode:: " + $scope.isDevMode);
			console.log("HOME CTRL isAnonymo:: " + $scope.isAnonymousUserAccessEnabled);

			if (this.authService.hasSessionId()) {
				//MENU
				window.location.href = '#/menu'
			} else {
				//LOGIN
				window.location.href = '#/login'
			}
		}
	}
}
