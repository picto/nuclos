/**
 */
module nuclos.searchreferences {

    import Component = nuclos.util.Component;

    @Component(nuclosApp, 'menuItem', {
        controllerAs: '$ctrl',
        templateUrl: 'app/header/menu-items.html',
        bindings: {
            entries: '<',
            level: '<'
        }
    })
    class MenuItemView {

        private ctrl = this;
        private level: number;

        constructor() {
            if (!this.level) {
                this.level = 0;
            }
        }
    }

}
