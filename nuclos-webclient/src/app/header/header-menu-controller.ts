module nuclos.header {

    import Mandator = nuclos.model.Mandator;
	interface HeaderRouteParams extends ng.route.IRouteParamsService {
        processmetaid: string;
	}

    export class HeaderMenuCtrl {

        vm = this;

        menu: Object;
        results; // TODO rename
        isTaskList: boolean;
        mMenuLabel: string;
        mMenuIcon: string;
        oMenuPath: string;
        username: string;
        viewLink: string;
        searchlink: string;
        showlivesearch: boolean;
        master: nuclos.model.interfaces.BoViewModel;
        authentication: Object;
        legalDisclaimers;
        maxMenuEntries: number;
		cookiesaccepted: any;
		hasDatenschutz: any;

		mandators: Mandator[];
		currentMandator: Mandator;

        constructor (
            private $http, // TODO remove
            private $cookieStore: ng.cookies.ICookiesService,
            private $cookies,
            private restservice,
            private dataService: nuclos.core.DataService,
            private authService: nuclos.auth.AuthService,
            private $routeParams: HeaderRouteParams,
            private $location: ng.ILocationService,
            private dialog: nuclos.dialog.DialogService,
            private menuService
        ) {

			setTitle($cookieStore);
            this.isTaskList = $location.path() === '/tasks';
            this.mMenuLabel = this.isTaskList ? 'Tasks' : 'Menu';
            this.mMenuIcon = this.isTaskList ? 'glyphicon-bookmark' : 'glyphicon-book';
            this.oMenuPath = this.isTaskList ? '#/menu' : '#/tasks';
            this.username = $cookieStore.get('authentication') ? $cookieStore.get('authentication')['username'] : '';
            var viewType = localStorage.getItem("selectedViewType") == "tableview" ? "tableview" : "sideview";
            this.viewLink = this.isTaskList ? '#/taskview' : '#/' + viewType;

            this.showlivesearch = false;
            restservice.systemparameters().then((data) => {
                this.showlivesearch = data.FULLTEXT_SEARCH_ENABLED;
            });

            this.master = dataService.getMaster();

            this.cookiesaccepted = $cookies.get('cookiesaccepted');

            // saving the links in a cookie leads to problems if the rest host changes
            //
            //var authentication = $cookieStore.get('authentication');
            //var metalink = this.isTaskList ? authentication.links.tasks : authentication.links.menu;
            //this.searchlink = authentication.links.search;

            // var metalink = {href: restHost + (this.isTaskList ? '/meta/tasks' : '/meta/menu')};
            this.searchlink = restHost + '/data/search/{text}';
            this.authentication = $cookieStore.get('authentication');


            // menuService.getMenu(metalink.href).then(
            menuService.getMenuStructure().then(
                (data) => {
                    if ((!data || data.length === 0) && this.isTaskList) {
                        window.location.href = this.oMenuPath;
                        return;
                    }

                    this.menu = data;
                },
                (error) => {
                    if(error && error.status == 401) { // unauthorized
                        $cookieStore.remove('authentication');
                        $location.path('login');
                    }
                }
            );

            this.authService.getLegalDisclaimers().then((data) => {
                this.legalDisclaimers = data.length > 0 ? data : undefined;
                this.hasDatenschutz = authService.hasLegalDisclaimer("Datenschutz", data);
            });

            this.mandators = this.authService.getMandators();
            this.currentMandator = this.authService.getCurrentMandator();
        }

		livesearchchanged(searchtext?) {
            if (searchtext && searchtext.length >= 2) {
            var searchlink = this.searchlink.replace('{text}', searchtext);
				this.$http.get(searchlink).
            success((data) => {
                var newSearchlink = this.searchlink.replace('{text}', searchtext);
                if (newSearchlink === searchlink) {
                    this.results = data;
                } else {
                    this.livesearchchanged();
                }
            }).
            error((jqxhr, textStatus) => {
				errorhandler.show(jqxhr, textStatus, this.$cookieStore);
            });
            } else {
                this.results = [];
            }
        }

        about(ld) {
            this.dialog.alert(null, ld.text, null, 'halfwidth-modal-window');
        }

    	acceptCookies() {
			this.$cookies.put('cookiesaccepted', 'true');
    		this.cookiesaccepted = true;
    	}

    	showPrivacyContent() {
    		this.authService.showLegalDisclaimer('Datenschutz');
    	};

        openDashboard() {
            if (this.$location.url() === '/menu') {
                // dashboard is already shown in menu
                return;
            }
            var $uibModal = angular.element(document.body).injector().get('$uibModal');
            $uibModal.open({
                templateUrl: 'app/dashboard/dashboard-modal.html',
                controller: nuclos.dashboard.DashboardModalCtrl,
                controllerAs: 'vm',
                size: 'lg',
                windowClass: 'fullsize-modal-window',
                resolve: {}
            }).result.then(() => {
                // ok
            });
        }

		public getName(): any {
			if (!this.master || !this.master.meta) {
				return undefined;
			}

			let processName = this.$routeParams.processmetaid;
			if (processName && processName.lastIndexOf(this.master.boMetaId) === 0) {
				processName = processName.substring(this.master.boMetaId.length + 1);
			}

			return this.master.meta.name + (processName ? ' (' + processName + ')' : '');
		}

		selectMandator(mandator: Mandator) {
			this.authService.selectMandator(mandator).then(
				() => {
					this.authService.resetLastLocation();
					window.location.href = '#/menu';
					window.location.reload(true);
				}
			);
		}
	}

	nuclosApp.controller('HeaderMenuController', HeaderMenuCtrl);
}
