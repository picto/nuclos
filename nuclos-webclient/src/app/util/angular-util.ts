module nuclos.util {

    /**
     * typescript decorator to make an Angular ^1.4 component more Angular 2ish for easier upgrade
     * 
     * http://stackoverflow.com/questions/35451652/what-is-best-practice-to-create-an-angularjs-1-5-component-in-typescript?answertab=active#tab-top
     */
    export function Component(moduleOrName: string | ng.IModule, selector: string, options: {
        controllerAs?: string,
        bindings?: Object,
        template?: string,
        templateUrl?: string
    }) {
        return (controller: Function) => {
            var module = typeof moduleOrName === "string" ? angular.module(moduleOrName) : moduleOrName;
            module.component(selector, angular.extend(options, { controller: controller }));
        }
    }    
}