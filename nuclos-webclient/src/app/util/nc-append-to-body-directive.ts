module nuclos.util {
	/**
	 * moves the html element to a div which is a direct child of the "body" element
	 *
	 * this might be used to make an overlaying element like a tooltip div visible
	 * over an element with overflow:hidden/auto
	 *
	 * this won't work with more then 1 element concurrently
	 */
	nuclosApp.directive('ncAppendToBody', function ($compile) {
		return {

			compile: function (elm) {
				var html = elm.html();

				return function (_scope, _element, _attrs) {
					var id = _attrs.id;

					// create a div as child of "body"
					var target = $('#appendToBodyDiv-' + id);
					if (target.length === 0) {
						target = $('<div id="appendToBodyDiv-' + id + '" style="position:absolute;"></div>');
						$('body').append(target);
					}

					var targetRelativeOffsetTop = target.offset().top - target.position().top;
					var top = _element.offset().top - targetRelativeOffsetTop;
					var targetRelativeOffsetLeft = target.offset().left - target.position().left;
					var left = _element.offset().left - targetRelativeOffsetLeft;

					// move html content to new div
					target.html(html);
					$compile(target.contents())(_scope);
					_element.empty();

					// set absolute position
					target.css({
						top: top + 'px',
						left: left + 'px'
					});
				};
			}
		};
	});
}
