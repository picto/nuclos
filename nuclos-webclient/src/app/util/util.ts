// TODO avoid global functions, better move to util-service.js
var showDialog = function ($q, okFunction, title, text, scope) {

	var deferred = $q ? $q.defer() : null;

	scope.dialogText = text;
	scope.showDialog = true;
	var dialogButtons = {};

	dialogButtons[scope.okLabel] = function () {
		okFunction();
		$(this)['dialog']("close");
		if (deferred) {
			deferred.resolve(true);
		}
	};
	dialogButtons[scope.cancelLabel] = function () {
		$(this)['dialog']("close");
		if (deferred) {
			deferred.reject(false);
		}
	};
	$(function () {
		$("#dialog")['dialog']({
			modal: true,
			buttons: dialogButtons,
			title: title,
			open: function () {
				$(".ui-dialog-titlebar-close").hide();
				$('.ui-dialog-buttonpane').find('button:contains("' + scope.cancelLabel + '")').addClass('btn btn-default');
				$('.ui-dialog-buttonpane').find('button:contains("' + scope.okLabel + '")').addClass('btn btn-default');
				$('.ui-dialog-buttonpane').find('button:contains("' + scope.okLabel + '")').attr('id', 'okButton');
			}
		});
	});
};

function scrollToId(id, offset) {
	offset = $(id).offset();
	if (!offset) return false;

	var position = $(id).offset().top;
	if (position >= offset) {
		$('html,body').animate({
			scrollTop: position - 70
		});
		return true;
	}
	return false;
}


/**
 * sort an array by given attribute
 */
var sortArray = function (obj, key) {

	var arrayOfObj = [];

	for (var item in obj) {
		if (obj.hasOwnProperty(item)) {
			arrayOfObj.push(obj[item]);
		}
	}

	arrayOfObj.sort(function (a, b) {
		if (a[key] < b[key]) {
			return -1;
		}
		else if (a[key] > b[key]) {
			return 1;
		}
		return 0;
	});

	return arrayOfObj;
};


function formatNumberForShow(value, precision, scale, localePreferences) {
	if (value == null || typeof value !== 'number') {
		return value;
	}
	if (precision > 0) {
		//DOUBLE

		var hundredDelimiter = localePreferences.getLocale() === "de" ? "," : ".";
		var thousandsSeperator = localePreferences.getLocale() === "de" ? "." : ",";

		value = value.toFixed(precision);
		value = value.split(".")[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + thousandsSeperator) + hundredDelimiter + value.split(".")[1];
		return value;
	} else {
		//INTEGER
		value = value.toString();
	}
	if (localePreferences.getLocale() === "de") {
		value = value.replace('.', '_');
		value = value.replace(',', '.');
		value = value.replace('_', ',');
	} else {
		//USE DEFAULT en
	}
	return value;
}

function parseNumberForRest(value, precision, scale, localePreferences) {
	if (value != null) {

		if (value.length == 0) {
			return null;
		}
		if (localePreferences.getLocale() === "de") {
			value = value.replaceAll('.', '');
			value = value.replace(',', '.');
		} else {
			value = value.replaceAll(',', '');
		}
		value = parseFloat(value);
	}
	return value;
}

function baseUrl() {
	return window.location.href.substring(0, window.location.href.indexOf('#/') + 2);
}