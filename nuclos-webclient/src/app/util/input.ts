module nuclos.util {

	interface IInputAttributes extends angular.IAttributes {
		ncType: any;
		precision: any;
		scale: any;
	}

	interface IInputModelController {
		$parsers: any;
		$formatters: any;
	}

	nuclosApp.directive('numberInput', function (dataService, localePreferences) {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attrs: IInputAttributes, ngModelController: IInputModelController) {
				if (attrs.ncType === 'Decimal' || attrs.ncType === 'Integer' || attrs.ncType === 'Number') {
					element.addClass("nuclos-number");

					ngModelController.$parsers.push(function (value) {
						//convert data from view format to model format
						var precision = attrs.precision;
						var scale = attrs.scale;
						return parseNumberForRest(value, precision, scale, localePreferences);
					});

					ngModelController.$formatters.push(function (value) {
						//convert data from model format to view format
						if (value || value === 0) {
							var precision = attrs.precision;
							var scale = attrs.scale;
							return formatNumberForShow(value, precision, scale, localePreferences)
						}
						return value;
					});

				}

			}
		};
	});

	nuclosApp.directive('dateInput', function () {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attrs, ngModelController: IInputModelController) {

				ngModelController.$parsers.shift(function (value) {
					if (value == null) {
						return null;
					}

					//ISO 8601
					var yyyy = String(value.getFullYear());

					var mm = String((value.getMonth() + 1));

					if (mm.length === 1) {
						mm = "0" + mm;
					}

					var dd = String(value.getDate());
					if (dd.length === 1) {
						dd = "0" + dd;
					}

					return yyyy + "-" + mm + "-" + dd;
				});

				ngModelController.$formatters.push(function (value) {
					// TODO Implement
					return value;
				});

			}
		};
	});


	/**
	 * calls function after pressing the enter key
	 */
	nuclosApp.directive('ngEnter', function () {
		return function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				if (event.which === 13) {
					scope.$apply(function () {
						scope.$eval(attrs.ngEnter, {'event': event});
					});

					event.preventDefault();
				}
			});
		};
	});
}
