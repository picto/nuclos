module nuclos.util {
	/**
	 * makes height of textarea equal to height of the text within it
	 *
	 * $timeout queues an event that will fire after the DOM loads,
	 * which is what's necessary to get the right scrollHeight (otherwise you'll get undefined)
	 */

	nuclosApp.directive('textareaElastic', ['$timeout',
		function ($timeout) {
			return {
				restrict: 'A',
				link: function ($scope, element) {
					$scope.initialHeight = $scope.initialHeight || element[0].style.height;
					var resize = function () {
						element[0].style.height = $scope.initialHeight;
						element[0].style.height = "" + (element[0].scrollHeight + 2) + "px";
					};
					element.on("input change", resize);
					$timeout(resize, 0);
				}
			};
		}
	]);
}
