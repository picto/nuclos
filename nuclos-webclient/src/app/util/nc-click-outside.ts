module nuclos.util {

    export class ClickOutsideService {

		private callbackList: Array<Function>;

        constructor(private $window: ng.IWindowService) {
			
			this.callbackList = [];

			// call callback on click 
			// except clicks below an element with css class 'prevent-popover-close' or datapickers
			this.$window.document.onclick = (mouseEvent) => {
				if (
					$(mouseEvent.target).closest('.prevent-popover-close').length === 0 &&
					$(mouseEvent.target).closest('.uib-datepicker-popup').length === 0
				) {
					this.executeCallbacks();
				}
			};
        }

		public registerCallback(callback) {
			if (this.callbackList.indexOf(callback) == -1) {
				this.callbackList.push(callback);
			}
		}

		public executeCallbacks() {
			for (let callback of this.callbackList) {
				callback();			
			}
			angular.element(document.querySelector('#main')).scope().$apply();
		}
		
	}
	nuclosApp.service("clickOutsideService", ClickOutsideService);


	// nc-click-outside
	nuclosApp.directive('ncClickOutside', ['$location', 'clickOutsideService', function($location, clickOutsideService: ClickOutsideService, $compile) {
		return {
			restrict: "A",
			scope: {
				ncClickOutside: '&ncClickOutside'
			},
			compile : function(elm) {
				var html = elm.html();
			
				return function(scope, element, attrs) {
					if (scope.ncClickOutside) {
						clickOutsideService.registerCallback(scope.ncClickOutside);
					}
				}
			}
		}
	}]);

}
