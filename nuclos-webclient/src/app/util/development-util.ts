function enableLogStackTrace() {
	localStorage.setItem('logStackTrace', 'true');
}

function disableLogStackTrace() {
	localStorage.setItem('logStackTrace', 'false');
}

function logStackTraceEnabled() {
	return localStorage.getItem('logStackTrace') == "true";
}

var logString = "";

var getLogCodeForObject = function (obj) {
	if (typeof obj == 'string' || typeof obj == 'number') {
		return '%s';
	} else {
		return '%o';
	}
};

/**
 * logging
 *
 * example:
 * log('test', 123);
 * log.red('test', 123);
 * log.blue('test', 123, obj1, obj2);
 */
log = function (...args) {
	var colorCode = (
		arguments[arguments.length - 1] &&
		(typeof arguments[arguments.length - 1] == 'string') &&
		arguments[arguments.length - 1].indexOf('color:') === 0
	) ? arguments[arguments.length - 1].substring('color:'.length)
		: null;
	var argsLength = colorCode != null ? arguments.length - 1 : arguments.length;

	logString = 'console.log("';
	if (colorCode) {
		logString += '%c ';
	}
	for (var i = 0; i < argsLength; i++) {
		logString += getLogCodeForObject(arguments[i]) + ' ';
	}
	logString += '"';
	if (colorCode) {
		logString += ', "color: ' + colorCode + ';"';
	}
	for (var i = 0; i < argsLength; i++) {
		if (!colorCode || i < argsLength) {
			logString += ', arguments[' + i + ']';
		}
	}
	logString += ');';
	eval(logString);

	//	printStackTrace();
	if (logStackTraceEnabled()) {
		console.debug(printStackTrace().join('\n\n'));
	}
};

// https://gist.github.com/bobspace/2712980#file-css_colors-js
var CSS_COLOR_NAMES = ["AliceBlue", "AntiqueWhite", "Aqua", "Aquamarine", "Azure", "Beige", "Bisque", "Black", "BlanchedAlmond", "Blue", "BlueViolet", "Brown", "BurlyWood", "CadetBlue", "Chartreuse", "Chocolate", "Coral", "CornflowerBlue", "Cornsilk", "Crimson", "Cyan", "DarkBlue", "DarkCyan", "DarkGoldenRod", "DarkGray", "DarkGrey", "DarkGreen", "DarkKhaki", "DarkMagenta", "DarkOliveGreen", "Darkorange", "DarkOrchid", "DarkRed", "DarkSalmon", "DarkSeaGreen", "DarkSlateBlue", "DarkSlateGray", "DarkSlateGrey", "DarkTurquoise", "DarkViolet", "DeepPink", "DeepSkyBlue", "DimGray", "DimGrey", "DodgerBlue", "FireBrick", "FloralWhite", "ForestGreen", "Fuchsia", "Gainsboro", "GhostWhite", "Gold", "GoldenRod", "Gray", "Grey", "Green", "GreenYellow", "HoneyDew", "HotPink", "IndianRed", "Indigo", "Ivory", "Khaki", "Lavender", "LavenderBlush", "LawnGreen", "LemonChiffon", "LightBlue", "LightCoral", "LightCyan", "LightGoldenRodYellow", "LightGray", "LightGrey", "LightGreen", "LightPink", "LightSalmon", "LightSeaGreen", "LightSkyBlue", "LightSlateGray", "LightSlateGrey", "LightSteelBlue", "LightYellow", "Lime", "LimeGreen", "Linen", "Magenta", "Maroon", "MediumAquaMarine", "MediumBlue", "MediumOrchid", "MediumPurple", "MediumSeaGreen", "MediumSlateBlue", "MediumSpringGreen", "MediumTurquoise", "MediumVioletRed", "MidnightBlue", "MintCream", "MistyRose", "Moccasin", "NavajoWhite", "Navy", "OldLace", "Olive", "OliveDrab", "Orange", "OrangeRed", "Orchid", "PaleGoldenRod", "PaleGreen", "PaleTurquoise", "PaleVioletRed", "PapayaWhip", "PeachPuff", "Peru", "Pink", "Plum", "PowderBlue", "Purple", "Red", "RosyBrown", "RoyalBlue", "SaddleBrown", "Salmon", "SandyBrown", "SeaGreen", "SeaShell", "Sienna", "Silver", "SkyBlue", "SlateBlue", "SlateGray", "SlateGrey", "Snow", "SpringGreen", "SteelBlue", "Tan", "Teal", "Thistle", "Tomato", "Turquoise", "Violet", "Wheat", "White", "WhiteSmoke", "Yellow", "YellowGreen"];


// http://www.heise.de/developer/artikel/Konsole-Currying-2049605.html
var curry = function (fn) {
	return function (colorName) {
		return function () {
			var args = Array.prototype.slice.call(arguments);
			args.push(colorName);
			return fn.apply(null, args);
		};
	};
};

for (var i in CSS_COLOR_NAMES) {
	var color = CSS_COLOR_NAMES[i].toLowerCase();
	eval('log.' + color + ' = curry(log)("color:' + color + '");');
}


logStackTrace = function () {
	var st = printStackTrace();
	for (var s in st) {
		var msg = st[s];
		if (msg.indexOf('stacktrace.js') == -1
			&& msg.indexOf('logStackTrace') == -1
			&& msg.indexOf('rest-call-history') == -1
			&& msg.indexOf('angular.js') == -1
			&& msg.indexOf('angular.min.js') == -1) {
			if (msg.indexOf('/lib/') != -1) {
				log('\t| ' + msg, 'color: rgba(0, 0, 255, 0.4); font-size: 7pt;');
			} else {
				log('\t| ' + msg, 'color: rgba(255, 153, 0, 0.6); font-size: 7pt;');
			}
		}
	}
};


/*
 *  color css class and id selectors for debugging
 *  colorCss();
 *
 */

function addCSSRule(sheet, selector, rules?, index?) {
	if ("insertRule" in sheet) {
		sheet.insertRule(selector + "{" + rules + "}", index);
	} else if ("addRule" in sheet) {
		sheet.addRule(selector, rules, index);
	}
}


var cssSelectors = [
	'#sideviewmenu', '#buttonarea', '#detailstab', '.activetab', '.tab', '#detailblock', '#detailblock-form', '.active',
	'[id^=tableview-]',

	//'.changeState', '#myTab', '#navigatorRow', '.gridStyleContainerRow', // gelöscht
	'.scroll-pane',
	'#menu-table',
	'#sideview-details', // (#svEditor)
	'#sideview-navigation', // (#svList)
	'#buttonarea-states', // (#buttonarea_states)
	'.searchfilter', // (searchFilter)
	'#main', // (#ngView)
	'.tab-content-table',
	'#view-selector', // (.view-selector)
	'#tableview-grid', // (.gridStyleContainer)
	'.sideview-list-entry' // (.sideviewtableentry)
];
var CSS_COLOR_NAMES_DARK = [];
for (var i in CSS_COLOR_NAMES) {
	if (CSS_COLOR_NAMES[i].indexOf('Dark') == 0) {
		CSS_COLOR_NAMES_DARK.push(CSS_COLOR_NAMES[i]);
	}
}

var colorCss = function (hover) {
	for (var i in cssSelectors) {
		addCSSRule(
			document.styleSheets[document.styleSheets.length - 1],
			cssSelectors[i] + (hover ? ":hover" : "") + ":before", 'content:"' + cssSelectors[i] + '";color: ' + CSS_COLOR_NAMES_DARK[i]
		);
		addCSSRule(
			document.styleSheets[document.styleSheets.length - 1],
			cssSelectors[i] + (hover ? ":hover" : ""), ' border: 2px dashed ' + CSS_COLOR_NAMES_DARK[i]
		);
	}
};


var colorIncludesAndDirectives = function () {
	$('ng-include, sideviewmenu, detailblock, validNumber, grid, simpletable, matrix, jsonText, states, fileupload, i18n, svtable, selectpicker').each(function (index, element) {
		var isDirective = false;
		var item;
		if ($(element).attr('src')) {
			item = $(element).attr('src');
		} else {
			isDirective = true;
			item = $(element).get(0).tagName;
		}
		var id = item.replace(/\\./g, '').replace(/\./g, '').replace(/'/g, '').replace(/\s/g, '').replace(/\//g, '');

		$(element).append('<div id="' + id + '"></div>');
		$(element).children().appendTo($('#' + id));

		addCSSRule(document.styleSheets[document.styleSheets.length - 1], '#' + id + ":before", 'content:"' + (isDirective ? '<' + item + ' />' : item ) + '";color: ' + CSS_COLOR_NAMES_DARK[index]);
		addCSSRule(document.styleSheets[document.styleSheets.length - 1], '#' + id, ' border: 2px dotted ' + CSS_COLOR_NAMES_DARK[index]);
	});
};


/**
 * log objects inside templates to console for debugging
 * usage: {{master | log:this}}
 */
nuclosApp.filter('log', function () {
	return function (obj, scope) {
		if (scope.logcounter == undefined) {
			scope.logcounter = 0;
		}
		if (scope.logcounter < 10) {
			log['green']("log filter:", obj);
			scope.logcounter++;
		}
	};
});


/**
 * print out master to console for debugging
 */
var printMaster = function () {
	var dataService = angular.element(document.body).injector().get('dataService');
	var master = dataService.getMaster();

	return printJSON(master);
};

var searchMaster = function (search) {
	var dataService = angular.element(document.body).injector().get('dataService');
	var master = dataService.getMaster();
	searchJSON(master, search);
};

function removePromises(obj, path, level) {
	try {
		for (var k in obj) {
			if (obj[k] !== null) {
				for (var x in obj[k]) {
					if (x && x.indexOf('$') == 0) { // remove functions starting with '$' like $promise, $get, ...
						delete obj[k][x];
					}
				}
				if (typeof obj[k] == "object") {
					if (level < 20) {
						path.push(Array.isArray(obj) ? '[' + k + ']' : k);
						removePromises(obj[k], path, level++);
						path.pop();
					}
				}
			}
		}
	} catch (error) {
		// usually call stack overflow - just stop removing
	}
}


function searchJSON(obj, search) {
	removePromises(obj, [], 0);

	function searchJSONprivate(obj, search, path, level) {
		try {
			for (var k in obj) {
				if (k && k.indexOf && k.indexOf(search) != -1
					|| typeof obj[k] === 'string' && obj[k].indexOf(search) != -1
					|| typeof obj[k] === 'number' && ('' + obj[k]).indexOf(search) != -1
				) {
					var pathString = '';
					var counter = 0;
					for (var p in path) {
						if (counter > 0 && path[p].indexOf('[') == -1) {
							pathString += '.';
						}
						pathString += path[p];
						counter++;
					}
					pathString += '.' + k;
					switch (typeof obj[k]) {
						case 'string':
							log['green']('found ' + (typeof obj[k]) + ': ', pathString, obj);
							break;
						case 'object':
							log['darkgoldenrod']('found ' + (typeof obj[k]) + ': ', pathString, obj);
							break;
						default:
							log('found ' + (typeof obj[k]) + ': ', pathString, obj);
					}
				}

				if (obj[k] !== null) {
					if (typeof obj[k] == "object") {
						path.push(Array.isArray(obj) ? '[' + k + ']' : k);
						searchJSONprivate(obj[k], search, path, level++);
						path.pop();
					}
				}
			}

		} catch (error) {
			// usually call stack overflow - just stop searching
			log['gray']("error while traversing object", error);
		}
	}

	searchJSONprivate(obj, search, [], 0);
}

printJSON = function (object) {
	removePromises(object, [], 0);
	return JSON.stringify(object, null, 2);
};

/* log angular events
 * https://github.com/angular/angular.js/issues/6043
 *
 nuclosApp.config(function($provide) {
 $provide.decorator("$rootScope", function($delegate) {
 var delegateConstructor = $delegate.constructor;
 var origBroadcast = delegateConstructor.prototype.$broadcast;
 var origEmit = delegateConstructor.prototype.$emit;

 delegateConstructor.prototype.$broadcast = function() {
 console.log("$broadcast was called with arguments:", arguments);
 return origBroadcast.apply(this, arguments);
 };
 delegateConstructor.prototype.$emit = function() {
 console.log("$emit was called with arguments:", arguments);
 return origEmit.apply(this, arguments);
 };
 return $delegate;
 });
 });
 */
