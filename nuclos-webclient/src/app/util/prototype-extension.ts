module nuclos.util {
	String.prototype['reverse'] = function () {
		return this.split("").reverse().join("");
	}

	String.prototype['replaceAll'] = function (target, replacement) {
		return this.split(target).join(replacement);
	};

	String.prototype['hashCode'] = function () {
		var hash = 0, i, char, l;
		if (this.length == 0)
			return hash;
		for (i = 0, l = this.length; i < l; i++) {
			char = this.charCodeAt(i);
			hash = ((hash << 5) - hash) + char;
			hash |= 0; // Convert to 32bit integer
		}
		return hash;
	};
}
