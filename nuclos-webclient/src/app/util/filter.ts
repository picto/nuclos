module nuclos.util
{
	nuclosApp.filter('reverse', function() {
		return function(items) {
			if (items == null) {
				return null;
			}
			return items.slice().reverse();
		};
	});
}