module nuclos.util {

	export class I18NService {
		selectedLocale: string = null;
		defaultLocale = {"key": "en", "name": "English"};
		locales = [{"key": "de", "name": "Deutsch"}, this.defaultLocale];

		constructor(private messages,
					private $cookies: ng.cookies.ICookiesService,
					private $log: ng.ILogService) {
		}

		public setLocale(locale) {
			this.selectedLocale = locale;
			this.$cookies.put('locale', locale);
		}

		public getBrowserLocale() {
			var browserlocale = this.$cookies.get('locale');
			if (!browserlocale) browserlocale = navigator.language;
			if (!browserlocale) browserlocale = 'en';
			return browserlocale;
		}

		public checkDefaultLocale() {
			if (this.selectedLocale) return;
			var browserlocale = this.getBrowserLocale();
			for (var i in this.locales) {
				if (this.locales[i].key === browserlocale) {
					this.setLocale(this.locales[i].key);
					return;
				}
			}
			for (var i in this.locales) {
				if (this.locales[i].key.substring(0, 2) === browserlocale.substring(0, 2)) {
					this.setLocale(this.locales[i].key);
					return;
				}
			}
			this.setLocale(this.defaultLocale.key);
		}

		public getI18n(key) {
			this.checkDefaultLocale();
			if (this.messages[this.selectedLocale] == null) {
				return key;
			}
			var value = this.messages[this.selectedLocale][key];
			if (!value) {
				console.warn('Key "' + key + '" not found in locale.js');
				return key;
			}
			return value;
		}
	}


	nuclosApp.directive('i18n', function (nuclosI18N: I18NService, $cookies) {
		return {
			restrict: 'E',
			replace: false,
			scope: {key: '@key'},
			link: function (scope, element, attrs) {
				element[0].appendChild(
					document.createTextNode(nuclosI18N.getI18n(attrs['key']))
				);
			}
		}
	});


	nuclosApp.filter('i18n', function (nuclosI18N: I18NService, $cookies) {
		return function (key) {
			if (key == null) {
				return null;
			}
			return nuclosI18N.getI18n(key);
		};
	});
}

angular.module("nuclos").service("nuclosI18N", nuclos.util.I18NService);
