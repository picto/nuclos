module nuclos.common {

	interface ISearchfilterScope extends angular.IScope {
		master: BoViewModel;
		searchcallback?: any;
		searchByField?: any;
		dialog?: any;
	}

	nuclosApp.directive('searchfilter', function (
		$timeout,
		$uibModal,
		$q,
		$resource,
		$routeParams: ng.route.IRouteParamsService,
		metaService,
		boService,
		errorhandler
	) {
		return {
			restrict: 'E',
			replace: false,
			templateUrl: 'app/common/searchfilter/searchfilter-directive.html',
			scope: {
				searchcallback: '<',
				master: '<'
			},
			link: function (scope: ISearchfilterScope, element, attrs) {

				var filterTextTimeout;
				scope.$watch('master.svQuery', function (val) {
					if (filterTextTimeout) {
						$timeout.cancel(filterTextTimeout);
					}
					filterTextTimeout = $timeout(function () {
						var query = scope.master.svQuery;
						if (!query || query.length < 2) {
							query = "";
						}
						if (query === scope.master.search) {
							return;
						}
						scope.master.search = query + "";

						if (scope.searchcallback) {
							scope.searchcallback();
						}

					}, 650); // delay
				});

				scope.searchByField = function (entity) {

					var $scope = scope;

					// combine metaService response
					$q.all([metaService.getBoMetaData(entity.boMetaId)]).then(function (res) {
						entity.metaData = res[0];

						// open dialog
						var modalInstance = $uibModal.open({
							templateUrl: 'app/common/searchfilter/searchfilterbyfield-dialog.html',

							controller: function ($scope, $uibModalInstance, dialog) {
								$scope.attributes = [];
								$scope.dialog = dialog;

								for (var i = 0; i < entity.metaData.attributes.length; i++) {
									var attr = entity.metaData.attributes[i];

									//First of all Image are not supported yet.
									//This also excludes "nuclosStateIcon" which yields errors
									if (attr.type === 'Image') {
										continue;

									} else if (attr.type === 'Boolean') {
										attr.sinput = 'checkbox'

									} else if (attr.reference) {
										attr.sinput = 'select';
										//attr.ticked = true; //TODO: For Debugging, remove later

									} else {
										attr.sinput = 'input'

									}

									if (!attr.valuelist) {
										attr.valuelist = [];
									}

									$scope.attributes.push(attr);
								}

								$scope.fOpen = function (attr) {
									if (attr.reference && attr.valuelist.length === 0) {
										var select = {reffield: attr.boAttrId};

										boService.loadDropDownOptions(select).then(function (data) {
											attr.valuelist = data;
										});

									}
								}

								$scope.fSearchChange = function (data, attribute) {
									console.log(data);
									console.log(attribute);
								}

								$scope.search = function () {
									$uibModalInstance.close();
								};

								$scope.cancel = function () {
									$uibModalInstance.dismiss('cancel');
								};
							},

							resolve: {
								dialog: function () {

									$scope.dialog = {};
									$scope.dialog.header = 'Feldbezogene Suche für ' + entity.metaData.name;
									$scope.dialog.message = 'Nuclos rules!';

									return $scope.dialog;
								}
							}
						});

						modalInstance.result.then(function (selectedItem) {

							scope.searchcallback();

						}, function () {
							// cancel

						});

						modalInstance.opened.then(function () {
							// focus on primary button
							$('.btn-primary').focus();
						});

					});

				}

			}
		};
	});
}
