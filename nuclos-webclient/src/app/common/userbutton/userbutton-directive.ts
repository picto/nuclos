module nuclos.common {

	interface IUserbuttonScope extends angular.IScope {
		master: BoViewModel;
		isUserButtonDisabled?: any;
		userPushButton?: any;
	}

	nuclosApp.directive('userButton', function ($rootScope, stateService, boService, boGenerationService, inputrequiredService) {
		return {
			scope: {
				button: '<',
				master: '<'
			},
			restrict: 'E',
			templateUrl: 'app/common/userbutton/userbutton.html',
			link: function ($scope: IUserbuttonScope, $element, $attrs) {

				$scope.isUserButtonDisabled = function (button, entity) {
					if (!button || button.disabled) {
						return true;
					}

					if (button["disable-during-edit"] === 'yes' && entity.dirty) {
						return true;
					}

					if (button.targetState) {
						if (!entity.selectedBo || !entity.selectedBo.nextStates) {
							return true;
						}
						for (var i in entity.selectedBo.nextStates) {
							var state = entity.selectedBo.nextStates[i];
							if (state.nuclosStateId === button.targetState) {
								return false;
							}
						}
						return true;
					}

					if (button.generatortoexecute) {
						if (!entity.selectedBo || !entity.selectedBo.generations) {
							return true;
						}
						for (var i in entity.selectedBo.generations) {
							var generation = entity.selectedBo.generations[i];
							if (generation.generationId == button.generatortoexecute) {
								return false;
							}
						}

					}

					return false;
				};

				var doGenerate = function (popupparameter) {
					return boGenerationService.doGenerateBo($scope.master.generations, popupparameter);
				}

				$scope.userPushButton = function (button, entity) {
					if (!entity.selectedBo) {
						return;
					}

					var isChangeStateAction = button.actioncommand == 'ChangeStateButtonAction' && button.targetState;

					if (isChangeStateAction) { // change state action
						var bos = [entity.selectedBo];

						stateService.changeState(bos, button.targetState).then(function (data) {

						}, function (error) {
							errorhandler.show(error.data, error.status, null);
						});

					} else if (button.actioncommand == 'ExecuteRuleButtonAction') { // custom rule action
						entity.selectedBo.receiverId = $rootScope.browserTabId;
						entity.selectedBo.executeCustomRule = button.ruletoexecute;

						boService.saveBo(entity.selectedBo, entity, true);

					} else if (button.actioncommand == 'GeneratorButtonAction') { //generator action

						//TODO: Get link from server
						//TODO: button.generatortoexecute came as "UID" and is sent back this way. Better use full qualified name.

						var link = restHost + '/boGenerations/' + entity.meta.boMetaId + '/' + entity.selectedBo.boId + '/generate/' + button.generatortoexecute;
						$scope.master.generations = {selected: {links: {generate: {href: link}}}};

						inputrequiredService.setSaveCallback(doGenerate);

						doGenerate(button.popupparameter).then(
							function () {
								// generation was successful
							},
							function (generations) {
								// generation was not successful
								inputrequiredService.processSaveDataRecursive(generations, button.popupparameter);
							}
						);
					}
				};
			}
		}
	});
}
