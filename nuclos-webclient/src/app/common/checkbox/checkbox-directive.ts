module nuclos.common {
	nuclosApp.directive('ncCheckbox', function () {
		return {
			scope: {
				checkbox: '<',
				model: '=',
				readonly: '<',
				changecallback: '&'
			},
			restrict: 'E',
			templateUrl: 'app/common/checkbox/checkbox.html',
			link: function ($scope, $element, $attrs) {

			}
		}
	});
}
