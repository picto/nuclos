module nuclos.common {
	nuclosApp.directive('actiontext', function () {
		return {
			scope: {
				field: '<',
				model: '=',
				readonly: '<',
				changecallback: '&',
				actioncallback: '&',
				glyph: '@'
			},
			restrict: 'E',
			templateUrl: 'app/common/actiontext/actiontext.html',
			link: function ($scope, $element, $attrs) {

			}
		}
	});
}
