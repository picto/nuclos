module nuclos.common {

	// TODO: Correct types
	interface IFileuploadScope extends angular.IScope {
		bo?: any;
		field?: any;
		fileUploaded?: any;
		showDocumentOverlayMenu?: any;
		fileSelected?: any;
		changecallback?: any;
		handleFileUpload?: any;
		isImage?: any;
		removeDocument?: any;
		openDocument?: any;
		clickOnDocument?: any;
	}

	nuclosApp.directive('ncFileupload', function ($window, dialog) {
		return {
			restrict: 'E',
			templateUrl: 'app/common/fileupload/fileupload-directive.html',
			replace: false,
			scope: {
				bo: '<',
				field: '<',
				changecallback: '<',
				css: '@',
				isImage: '<',
				filename: '<',
				readonly: '<'
			},
			link: function (scope: IFileuploadScope, element, attrs) {

				var isFileUploaded = function () {
					return scope.bo.attrImages && scope.bo.attrImages.links[scope.field.name] != null
				}

				scope.fileUploaded = isFileUploaded();

				scope.showDocumentOverlayMenu = function () {
					return scope.bo.attributes[scope.field.name] != null || (scope.bo.attrImages && scope.bo.attrImages.links[scope.field.name] != null);
				};

				scope.fileSelected = function (files) {

					var changeCallback = function () {
						scope.changecallback(scope.field.name, null, scope.bo);
						scope.fileUploaded = isFileUploaded();
						scope.$apply();
					};

					handleFileUpload(scope.bo, scope.field.name, files, changeCallback, scope.isImage);
				};


				/**
				 * reset document or image attribute
				 */
				scope.removeDocument = function (bo, field) {
					bo.attributes[field.name] = null;

					// reset background image if type is image
					if (bo.attrImages && bo.attrImages.links[field.name]) {
						bo.attrImages.links[field.name] = undefined;
					}
					scope.fileUploaded = isFileUploaded();

					scope.changecallback(field.name, null, bo);
				};

				scope.openDocument = function (bo, field) {
					//TODO: This is very constructed
					var docAttrId = bo.boMetaId + '_' + field.name;

					//TODO: HATEOAS
					var link = "";
					if (scope.isImage == undefined) {
						link = bo.links.self.href.replace('/bos/', '/boDocuments/') + '/documents/' + docAttrId;
					} else {
						link = bo.links.self.href.replace('/bos/', '/boImages/') + '/images/' + docAttrId + '/' + bo.version;
					}
					$window.open(link);
				};

				scope.clickOnDocument = function (bo, field, isImage) {
					if (!bo.attributes[field.name]) { // document doesn't exists
						return;
					}
					if (!isImage) {
						scope.openDocument(bo, field);
						return;
					}

					var imageLink = bo.attrImages.links[field.name].href;
					dialog.preview(null, imageLink);
				}
			}
		};
	});
}
