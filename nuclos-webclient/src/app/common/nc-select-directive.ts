module nuclos.common {

    import Component = nuclos.util.Component;

    @Component(nuclosApp, 'ncSelect', {
        controllerAs: 'vm',
        templateUrl: 'app/common/nc-select-directive.html',
        bindings: {
            model: '=',
            boId: '=',
            quicksearch: '=',
            loadOptionsCallback: '&',
            valueChangedCallback: '&',
            disabled: '=',
            required: '='
        }
    })
    class NcSelect {
        private model;
        private boId;
        private quicksearch;
        private options;
        private disabled: boolean;
        private required: boolean;
        private loadOptionsCallback: Function;
        private valueChangedCallback: Function;

        constructor(private boService: nuclos.core.BoService, private $timeout: ng.ITimeoutService) {
        }

        valueChanged() {
            this.$timeout(() => {
                this.valueChangedCallback(this.model);
            }, 100);
        }

        loadLovDropDownOptions(quickSearchInput) {

            this.quicksearch = quickSearchInput;

            // prevent flicker when opening dropdown
            var openDropdownHtmlItems = $('.ui-select-container.open ul');
            openDropdownHtmlItems.css('display', 'none');

            this.$timeout(() => {
                this.loadOptionsCallback().then((result) => {

                    var selectedId = result.selectedId
                    var data = result.data;

                    // add an empty entry at first position of the dropdown array if dropdown is not a required field
                    if (this.required !== undefined && !this.required) {
                        data.unshift({ name: '', id: null });
                    }

                    openDropdownHtmlItems.css('display', 'initial');

                    // mark selected dropdown options as active -> update css class
                    if (selectedId) {
                        for (var i in data) {
                            if (data[i].id == selectedId) {
                                this.$timeout(function () {
                                    $('.ui-select-choices-row').removeClass('active');
                                    $($('.ui-select-choices-row')[i]).addClass('active');
                                });
                                break;
                            }
                        }
                    }

                    this.options = data;
                });
            });
        }

    }

}