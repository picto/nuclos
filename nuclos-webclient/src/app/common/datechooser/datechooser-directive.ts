module nuclos.common {
	nuclosApp.directive('datechooser', function ($timeout, localePreferences) {
		return {
			scope: {
				date: '<',
				model: '=',
				readonly: '<',
				changecallback: '&'
			},
			restrict: 'E',
			templateUrl: 'app/common/datechooser/datechooser.html',
			link: function ($scope, $element, $attrs) {

				// TODO: Proper typing for $scope
				$scope['datePickerOptions'] = {
					formatYear: 'yy',
					startingDay: 1
				};

				$scope['dateFormat'] = localePreferences.getDateFormat();

				$scope['isDatePickerOpen'] = {};

				$scope['datePickerOpen'] = function (fieldname) {
					$timeout(function () {
						$scope['isDatePickerOpen'][fieldname] = true;
					});
				};

				$scope['editorChange'] = function () {
					if ($scope['focus']) {
						$scope['changecallback']();
					}
				}
			}
		}
	});
}
