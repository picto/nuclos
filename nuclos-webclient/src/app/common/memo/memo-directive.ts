module nuclos.common {
	nuclosApp.directive('ncMemo', function () {
		return {
			scope: {
				memo: '<',
				readonly: '<',
				model: '=',
				changecallback: '&',
				style: '@'
			},
			restrict: 'E',
			templateUrl: 'app/common/memo/memo.html',
			link: function ($scope, $element, $attrs) {

			}
		}
	});
}
