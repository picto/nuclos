module nuclos.searchtemplate {

	import SideviewService = nuclos.sideview.SideviewService;
	import IPreference = nuclos.preferences.IPreference;
	import SearchtemplatePreferenceContent = nuclos.sideview.SearchtemplatePreferenceContent;
	import SearchTemplate = nuclos.searchtemplate
	import PerspectiveService = nuclos.perspective.PerspectiveService;


	export interface Dictionary<T extends string, U> {
		[ index: string ]: U;
	}


	export interface Operator {
		operator: string;
		isUnary: boolean;
		label: string;
	}


	export interface SearchTemplateAttr extends nuclos.model.interfaces.BoAttr {
		inputType?: string;
		operator?: string;
		value?: any;
		selected?: boolean;
		enableSearch?: boolean;
		isValid?: boolean;
		searchPopoverOpen?: boolean;
	}


	/**
	 * data model for searchtemplates in sideview
	 */
	export class SearchtemplateModel {

		attributePopoverOpen: boolean;

		allAttributes: Array<SearchTemplateAttr>;

		operatorDefinitions: Dictionary<string, Array<Operator>>;
		searchtemplatePreferences: Array<nuclos.preferences.IPreference<SearchtemplatePreferenceContent>>;
		selectedSearchtemplatePreference: nuclos.preferences.IPreference<SearchtemplatePreferenceContent>;

		editSearchtemplate: boolean;
		newBoInstanceTemplates: Array<nuclos.preferences.IPreference<SearchtemplatePreferenceContent>>;

		public setSelectedSearchtemplatePreference(pref: IPreference<SearchtemplatePreferenceContent>) {
			this.selectedSearchtemplatePreference = pref;
		}
	}

	export class SearchtemplateService {

		static searchtemplateModel: SearchtemplateModel;

        constructor(
            private util: nuclos.util.UtilService,
					private preferences: nuclos.preferences.PreferenceService,
					private dataService: nuclos.core.DataService,
					private nuclosI18N: nuclos.util.I18NService,
					private $q: ng.IQService,
			private perspectiveService: PerspectiveService
        ) {

			SearchtemplateService.searchtemplateModel = new SearchtemplateModel();

			SearchtemplateService.searchtemplateModel.operatorDefinitions = {
				number: [
                    { operator: "=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.equal') },
                    { operator: "!=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.notequal') },
                    { operator: ">", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.gt') },
                    { operator: "<", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.lt') },
                    { operator: ">=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.gte') },
                    { operator: "<=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.lte') },
                    { operator: "is null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.null') },
                    { operator: "is not null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.notnull') }
				],
				boolean: [
                    { operator: "=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.number.equal') },
				],
				string: [
                    { operator: "=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.string.equal') },
                    { operator: "like", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.string.like') },
                    { operator: "is null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.null') },
                    { operator: "is not null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.notnull') }
				],
				reference: [
                    { operator: "=", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.string.equal') },
                    { operator: "like", isUnary: false, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.string.like') },
                    { operator: "is null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.null') },
                    { operator: "is not null", isUnary: true, label: this.nuclosI18N.getI18n('webclient.searchtemplate.operator.notnull') }
				]
			};
			SearchtemplateService.searchtemplateModel.operatorDefinitions['date'] = SearchtemplateService.searchtemplateModel.operatorDefinitions['number'];

			SearchtemplateService.searchtemplateModel.searchtemplatePreferences = [];
			let pref: IPreference<SearchtemplatePreferenceContent> = {
				content: {
					allAttributes: [],
					attributes: []
				} as SearchtemplatePreferenceContent,
				name: '',
				type: 'searchtemplate',
				boMetaId: null
			};

			SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(pref);
		}

		public formatValue(attribute, operatorDef) {
			attribute.formattedValue = attribute.value;
			if (attribute.inputType === 'date') {
				attribute.formattedValue = this.util.formatDate(attribute.value);
			} else if (attribute.inputType === 'reference') {
				attribute.formattedValue = attribute.value && attribute.value.name ? attribute.value.name : attribute.value;
			}

			if ((attribute.inputType === 'string' || attribute.inputType === 'reference') && (operatorDef && !operatorDef.isUnary)) {
				attribute.formattedValue = '\'' + attribute.formattedValue + '\'';
			}
		}

		public getOperatorDefinition = (attribute) => {

			var type = attribute.inputType;
			var operator = attribute.operator;

			if (type == undefined) {
				console.warn('Unable to get operatordefinition for:', type, operator);
				return;
			}

			var filteredOperator =
				SearchtemplateService.searchtemplateModel.operatorDefinitions[type].filter(function (op) {
					if (op.operator == operator) {
						return true;
					}
				});
			if (filteredOperator.length == 1) {
				return filteredOperator[0];
			}
		}

		public getDefaultName(preferenceItem) {
			var selectedAttributes = preferenceItem.content.allAttributes.filter(function (elem) {
				return elem.selected;
			});

			return selectedAttributes.map(function (a) {
				return a.name;
			}).join(', ');
		}

		public updateModel() {
			SearchtemplateService.searchtemplateModel.newBoInstanceTemplates = this.getNewBoInstanceTemplatePreferences();
		}

		public getNewBoInstanceTemplatePreferences(): Array<nuclos.preferences.IPreference<nuclos.sideview.SearchtemplatePreferenceContent>> {
			return SearchtemplateService.searchtemplateModel.searchtemplatePreferences.filter(
				function (pref) {
					return pref.content.isNewBoInstanceTemplate;
				}
			);
		}

		public saveSearchtemplatePreference(preferenceItem) {

			var deferred = this.$q.defer<Object>();

			// update preference name with default name if not overridden by user
			if (!preferenceItem.content.userdefinedName) {
				preferenceItem.name = this.getDefaultName(preferenceItem);
			}

			var isNew = !preferenceItem.prefId;

			var selectedAttributes = preferenceItem.content.allAttributes.filter((elem) => {
				return elem.selected;
			});
			preferenceItem.content.attributes = selectedAttributes;

			var preferenceItemCopy = angular.copy(preferenceItem);

			// remove unnecessary data
			delete preferenceItemCopy.content.operatorDefinitions;
			delete preferenceItemCopy.content.allAttributes;

			preferenceItemCopy.content.attributes = [];
			preferenceItem.content.attributes.forEach((attribute) => {
				preferenceItemCopy.content.attributes.push({
					boAttrId: attribute.boAttrId,
					value: attribute.value,
					operator: attribute.operator,
					enableSearch: attribute.enableSearch
				});
			});

			if (preferenceItemCopy.name.length > 50) {
				preferenceItemCopy.name = preferenceItemCopy.name.substring(0, 50) + '...';
			}

			this.preferences.savePreferenceItem(preferenceItemCopy).then(
				(data) => {
					if (data !== undefined && data.prefId !== undefined) {
						preferenceItem.prefId = data.prefId;
					}
					deferred.resolve(preferenceItem);
				},
				(error) => {
					console.error('Unable to save preferences.', error);
					deferred.reject(error);
				}
			);


			if (isNew) {
				SearchtemplateService.searchtemplateModel.searchtemplatePreferences.push(preferenceItem);
			}

			this.updateModel();

			return deferred.promise;
		}


		public updateSelectedFlag() {
			this.doUpdateSelectedFlag(true);
		}

		public resetSelectedFlag() {
			this.doUpdateSelectedFlag(false);
		}


		private setSelectedFlag() {
			SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.selected = true;
			this.saveSearchtemplatePreference(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference);
		}

		private doUpdateSelectedFlag(setSelectedFlagOfSelectedSearchTemplate: boolean) {

			if (!SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference) {
				return;
			}

			// reset selected flag
			var itemsProcessed = 0;

			// list with 0 or 1 selected preferences
			var selectedPrefList = SearchtemplateService.searchtemplateModel.searchtemplatePreferences.filter((pref) => {
				return pref.content.selected
			});

			for (let pref of selectedPrefList) {
				// remove selected flag
				delete pref.content.selected;
				this.saveSearchtemplatePreference(pref).then((updatedPref) => {
					itemsProcessed++;
					if (itemsProcessed == selectedPrefList.length) {
						// set selected flag
						if (setSelectedFlagOfSelectedSearchTemplate) {
							this.setSelectedFlag();
						}
					}
				});
			}
			if (selectedPrefList.length === 0) {
				if (setSelectedFlagOfSelectedSearchTemplate) {
					this.setSelectedFlag();
				}
			}
		}


		public resetSelectedSearchtemplatePreference(processMetaId?: string) {
			var boMetaId = this.dataService.getMaster().boMetaId;
			let searchTemplate: IPreference<nuclos.sideview.SearchtemplatePreferenceContent> = {
				prefId: null,
				type: 'searchtemplate',
				boMetaId: boMetaId,
				content: {
					attributes: []
				} as nuclos.sideview.SearchtemplatePreferenceContent,
				name: ''
			};
			searchTemplate.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);
			if (processMetaId) {
				let processAttribute: SearchTemplateAttr = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes.filter(function (value, index, array) {
					return value.boAttrName == "nuclosProcess";
				})[0];
				processAttribute.operator = "=";
				let processName: string = processMetaId.lastIndexOf(boMetaId) == 0 ? processMetaId.substring(boMetaId.length + 1) : "";
				let value = {"id": processMetaId, "name": processName};
				processAttribute.value = value;
				searchTemplate.content.attributes.push(processAttribute);
				this.setSearchComponent(processAttribute);
				SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.selected = true;
				processAttribute.enableSearch = true;
				processAttribute.selected = true;
				// processAttribute.searchPopoverOpen = true;
				processAttribute.isValid = true;
				this.formatValue(processAttribute, this.getOperatorDefinition(processAttribute));

				let openSearchButton = angular.element(document.querySelector('#opensearch-button'));
				openSearchButton.click();
			}
			SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(searchTemplate);
		}


		/**
		 * define which search input component will be used for which attribute
		 */
		public setSearchComponent = function (attribute) {
			if (attribute.inputType === 'boolean') {
				attribute.searchComponent = 'boolean';
			} else if (attribute.inputType === 'reference' && (!attribute.operator || attribute.operator === '=')) {
				attribute.searchComponent = 'lov';
			} else if (attribute.inputType === 'date') {
				attribute.searchComponent = 'datepicker';
			} else {
				delete attribute.searchComponent;
			}
		}


		public newSearchtemplate(boMetaId) {

			SearchtemplateService.searchtemplateModel.editSearchtemplate = true;

			this.resetSelectedSearchtemplatePreference();
			SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);

			this.updateSelectedFlag();
		}


		public deleteSearchtemplate() {
			this.preferences.deletePreferenceItem(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference);

			// remove from model
			var elementPositionInArray = SearchtemplateService.searchtemplateModel.searchtemplatePreferences.map(function (pref) {
				return pref.prefId;
			}).indexOf(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.prefId);
			SearchtemplateService.searchtemplateModel.searchtemplatePreferences.splice(elementPositionInArray, 1);

			// select first in list
			SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(SearchtemplateService.searchtemplateModel.searchtemplatePreferences[0]);

			// if list is empty create a new search template
			if (!SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference) {
				this.resetSelectedSearchtemplatePreference();
			}

			this.updateModel();
		}

		public getSelectedSearchtemplatePreference(): IPreference<SearchtemplatePreferenceContent> {
			let perspectiveSearchTemplate = this.perspectiveService.getSelectedSearchTemplate();
			if (perspectiveSearchTemplate && perspectiveSearchTemplate.prefId === SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.prefId) {
				return perspectiveSearchTemplate;
			}

			return SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference;
		}
	}
}

angular.module("nuclos").service("searchtemplateService", nuclos.searchtemplate.SearchtemplateService);
