module nuclos.searchtemplate {

	import Component = nuclos.util.Component;
	import SideviewService = nuclos.sideview.SideviewService;
	import IScope = angular.IScope;
	import IPreference = nuclos.preferences.IPreference;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;

	interface SideViewScope extends IScope {
		masterSelectBoHandler: Function;
	}

	export interface ISearchTemplateScope extends ng.IScope {
		operatorDefinitions: any;
		searchtemplate: any;
		master: nuclos.model.interfaces.BoViewModel;

		getOperatorLabel: Function;
		getOperatorDefinition: Function;
		dateFormat: Function;
		inputSearchValue: Function;
		doSearch: Function;
		savePreferences: Function;
		selectSearchAttribute: Function;
		openSearchAttributeList: Function;
		togglePopover: Function;
		resetSearch: Function;
		clickOutsideSearchtemplate: Function;
		loadSearchLovDropDownOptions: Function;
		newSearchtemplate: Function;
	}


	@Component(nuclosApp, 'searchtemplate', {
		controllerAs: 'vm',
		templateUrl: 'app/common/searchtemplate/searchtemplate-directive.html',
	})
	class SearchTemplate {

		private searchtemplateModel;
		private timeout;
		private operatorDefinitions;
		private dateFormat;
		public showSideviewNavigation: boolean = SideviewService.showSideviewNavigation;
		private textSearch;
		private filterTextTimeout;
		private getOperatorDefinition: Object;

		constructor(private $rootScope: ng.IScope,
					private $timeout: ng.ITimeoutService,
					private dataService: nuclos.core.DataService,
					private layoutService,
					private boService: nuclos.core.BoService,
					private localePreferences: nuclos.clientprefs.LocalePreferenceService,
					private sideviewService: nuclos.sideview.SideviewService,
					private searchtemplateService: nuclos.searchtemplate.SearchtemplateService,
					private $q: ng.IQService,
					private $log: ng.ILogService) {
			// close popover when navigating away
			this.$rootScope.$on('$locationChangeStart', (event, next, current) => {
				SearchtemplateService.searchtemplateModel.attributePopoverOpen = false;
			});

			this.$rootScope.$on('searchTemplateChanged', (event, searchTemplate: IPreference<nuclos.sideview.SearchtemplatePreferenceContent>) => {
				this.$log.debug('search template changed: %o', searchTemplate);
				this.searchtemplateModel = searchTemplate;

				// TODO: This does not work, somewhere the content is overwritten...
				this.selectSearchtemplate(searchTemplate, false);
			});

			this.searchtemplateModel = SearchtemplateService.searchtemplateModel;
			this.operatorDefinitions = SearchtemplateService.searchtemplateModel.operatorDefinitions;
			this.getOperatorDefinition = searchtemplateService.getOperatorDefinition;

			this.dateFormat = this.localePreferences.getDateFormat();
		}


		public doTextSearch() {
			if (this.filterTextTimeout) {
				this.$timeout.cancel(this.filterTextTimeout);
			}
			this.filterTextTimeout = this.$timeout(() => {
				let query = this.textSearch;
				if (!query || query.length < 2) {
					query = "";
				}
				this.dataService.getMaster().search = query;
				this.doSearch(false, true);
			}, 650); // delay
		}

		public inputSearchValue(attribute) {

			// wait before executing search so that not every keypress inits a new search
			if (this.timeout) {
				this.$timeout.cancel(this.timeout);
			}
			// start new time out
			this.timeout = this.$timeout(() => {

				attribute.enableSearch = true;

				let operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
				if (operatorDef) {
					if (operatorDef.isUnary) {
						attribute.value = '';
					} else {
						this.$timeout(() => {
							$('#searchfilter-value').focus();
						}, 0);
					}
				}

				this.searchtemplateService.setSearchComponent(attribute);

				this.searchtemplateService.formatValue(attribute, this.searchtemplateService.getOperatorDefinition(attribute));

				// clear input if reference was selected before in lov and then a 'like' search was called
				if (attribute.reference && operatorDef.operator == 'like' && attribute && attribute.value && attribute && attribute.value.id) {
					delete attribute.value;
				}

				this.doSearch(true, true);

			}, 500);
		}


		public getOperatorLabel(attribute) {
			if (attribute.inputType !== undefined) {
				let operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
				if (operatorDef) {
					return operatorDef.label;
				}
			}
			return undefined;
		}

		public savePreferences() {
			if (!SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.boMetaId) {
				SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.boMetaId = this.dataService.getMaster().boMetaId;
			}
			this.searchtemplateService.saveSearchtemplatePreference(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference);
		}

		public doSearch(doSavePreferences, selectFirstFoundEntry, validate = true) {

			// scope.validateSearchForm();

			this.$log.debug('doSearch(%o, %o, %o)', doSavePreferences, selectFirstFoundEntry, validate);

			// if (this.searchtemplateModel.selectedSearchtemplatePreference) {
			let selectedSearchTemplate = this.searchtemplateService.getSelectedSearchtemplatePreference();
			if (selectedSearchTemplate) {

				this.searchtemplateModel.selectedSearchtemplatePreference = selectedSearchTemplate;
				if (!selectedSearchTemplate.content.allAttributes || selectedSearchTemplate.content.allAttributes.length === 0) {
					selectedSearchTemplate.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);
					this.sideviewService.prepareSearchtemplateModel(selectedSearchTemplate.content);
				}

				if (validate) {
					this.sideviewService.validateSearchForm(selectedSearchTemplate.content);
				}

				this.sideviewService.search(this.dataService.getMaster().boMetaId, selectFirstFoundEntry).then((data) => {
					// open first entry in list
					if (data.bos.length > 0) {
						if (selectFirstFoundEntry) {
							this.dataService.getMaster().selectedBo = data.bos[0];
						}

						// TODO
						let sideviewScope: SideViewScope = angular.element($("#main")).scope() as SideViewScope;
						sideviewScope.masterSelectBoHandler(this.dataService.getMaster(), this.dataService.getMaster().selectedBo);
					}

				});

				if (doSavePreferences) {
					this.savePreferences();
				}
			}
		}

		public selectSearchAttribute(attribute/*: BoAttr*/) {
			this.searchtemplateService.setSearchComponent(attribute);
			this.searchtemplateModel.selectedSearchtemplatePreference.content.selected = true;
			this.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes
				.filter(attr => attr.boAttrId === attribute.boAttrId)
				.forEach(attr => attr.selected = attribute.selected);
			attribute.enableSearch = attribute.selected;
			if (!attribute.selected) {
				attribute.searchPopoverOpen = false;
			}
			this.doSearch(true, false);
		}

		public openSearchAttributeList($event?) {

			if ($event) {
				$('#searchfilter-attributelist-popover').css(
					'left',
					$($event.target).closest('button').offset().left - $('#searchfilter-attributelist-popover').width() +
					16 + $($event.target).closest('button').width() + 'px'
				);
			}

			// focus input
			this.$timeout(() => {
				$('#attribute-filter').focus();
			}, 0);

			// close all other popups
			if (this.searchtemplateModel) {
				for (let attr of this.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes) {
					attr.searchPopoverOpen = false;
				}
			}
		}

		public togglePopover(attribute, $event) {

			/*
			 place popover directly under the open button,
			 except popover was called from attribute selection checkbox, then place it right to attribute selection panel

			 open only one popover
			 */

			let button = $('#button-' + attribute.boAttrId);
			if (button.offset() !== undefined) {
				let popover = $('.searchfilter-attribute-popover');
				popover.css('left', button.offset().left + 'px');
				popover.css('top', (button.offset().top + button.outerHeight()) + 'px');
			}
			this.$timeout(() => {
				$('#searchfilter-value').focus();
			}, 0);


			for (let attr of this.searchtemplateModel.selectedSearchtemplatePreference.content.attributes) {
				if (attr.boAttrId == attribute.boAttrId) {
					if (attr.searchPopoverOpen == undefined)
						attr.searchPopoverOpen = false;
					attr.searchPopoverOpen = !attr.searchPopoverOpen;
					if (!attribute.selected) {
						attr.searchPopoverOpen = false;
					}
				} else {
					attr.searchPopoverOpen = false;
				}
			}
		}

		public resetSearch(processMetaId?: string) {
			this.searchtemplateModel.attributePopoverOpen = false;

			this.dataService.getMaster().svQuery = '';

			this.searchtemplateService.resetSelectedSearchtemplatePreference(processMetaId);
			this.searchtemplateService.resetSelectedFlag();

			this.doSearch(false, false);
		};

		public clickOutsideSearchtemplate() {
			if (this.searchtemplateModel) {
				// close all popups
				this.searchtemplateModel.attributePopoverOpen = false;
				for (let a in this.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes) {
					let attr = this.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes[a];
					attr.searchPopoverOpen = false;
				}
			}
		};

		public loadSearchLovDropDownOptions(attribute, search) {

			let deferred = this.$q.defer();

			let master = this.dataService.getMaster();
			this.layoutService.defaultSearchLayout(master).then((layout) => {

				let layoutField = this.layoutService.findField(layout, attribute.boAttrName);
				if (layoutField) {
					let select = {
						reffield: layoutField.uid,
						vlp: layoutField.vlp
					};
					// set 'searchmode' parameter for VLP
					if (select.vlp) {
						select.vlp.params.searchmode = true;
					}
					let selectedId = attribute.value ? attribute.value.id : undefined;
					this.boService.loadDropDownOptions(select, null, search).then((data) => {
						deferred.resolve({selectedId: selectedId, data: data});
					});
				} else {
					this.$log.warn('Attribute', attribute.boAttrName, 'not found in layout.');
					deferred.reject();
				}

			});

			return deferred.promise;
		}

		private closeAllPopups() {
			this.searchtemplateModel.attributePopoverOpen = false;
			if (!this.searchtemplateModel.selectedSearchtemplatePreference) {
				return;
			}
			for (let attr of this.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes) {
				attr.searchPopoverOpen = false;
			}
		}

		/**
		 * Applys the given search template prefs.
		 * If updateSelectedFlag is true, the "selected" flag of the search template is set and saved server-side.
		 *
		 * @param searchtemplatePreference
		 * @param updateSelectedFlag
		 */
		public selectSearchtemplate(searchtemplatePreference: IPreference<nuclos.sideview.SearchtemplatePreferenceContent>,
									updateSelectedFlag: boolean) {
			this.$log.debug('select search template: %o', searchtemplatePreference);

			SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(searchtemplatePreference);

			this.searchtemplateModel = SearchtemplateService.searchtemplateModel;

			if (updateSelectedFlag !== false) {
				this.searchtemplateService.updateSelectedFlag();
			}

			this.doSearch(updateSelectedFlag, false, false);
			this.closeAllPopups();
		}

		public editSearchtemplate() {
			this.$timeout(() => {
				this.searchtemplateModel.editSearchtemplate = true;
				this.$timeout(() => {
					angular.element('#searchtemplate-name-input').focus();
				});
			});
		}

		public saveSearchtemplate() {
			this.$timeout(() => {
				this.searchtemplateModel.editSearchtemplate = false;
				this.updatePreferenceName();
			});
		}

		public newSearchtemplate() {
			this.$timeout(() => {
				this.searchtemplateService.newSearchtemplate(this.dataService.getMaster().boMetaId);
				this.searchtemplateModel.editSearchtemplate = true;
				this.$timeout(() => {
					angular.element('#searchtemplate-name-input').focus();
				});
			});
		}

		public deleteSearchtemplate() {
			this.searchtemplateService.deleteSearchtemplate();

			this.doSearch(false, true);
			SearchtemplateService.searchtemplateModel.attributePopoverOpen = true;
		}

		public updatePreferenceName() {
			this.searchtemplateModel.selectedSearchtemplatePreference.content.userdefinedName =
				this.searchtemplateModel.selectedSearchtemplatePreference.name.length > 0
				&&
				this.searchtemplateService.getDefaultName(this.searchtemplateModel.selectedSearchtemplatePreference)
				!== this.searchtemplateModel.selectedSearchtemplatePreference.name
			;
			this.savePreferences();
		}

		public getProcessMetaId(): string {
			return "";
		}

	}
}

/**
 * add scrollbar to attribute popover
 */
nuclosApp.directive('searchtemplateScrollbar', function ($timeout) {
	return {
		restrict: 'A',
		link: function ($scope, element) {
			$timeout(() => {
				element.css('max-height', $('#detailblock').height() - 110);
			});
		}
	}
});
