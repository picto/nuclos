/// <reference path="../../../typings/tsd.d.ts" />

module nuclos.model.interfaces {

    export interface Map<T> {
        [K: string]: T;
        addToMap: (T);
    }


    export class HTPPMethod {
        static GET: string = "GET";
        static POST: string = "POST";
        static PUT: string = "PUT";
        static DELETE: string = "DELETE";
    }


    export class Flag {
        static insert: string = "insert";
        static update: string = "update";
        static delete: string = "delete";
    }

    export interface Link {
        href: string;
        methods?: Array<HTPPMethod>;
    }


    export interface LinkContainer {
        self?: Link;
        defaultLayout?: Link;
        defaultGeneration?: Link;
        bos?: Link;
		layout?: Link;
		chooseMandator?: Link;
    }

    export interface BoAttr {
        boAttrId: string;
        boAttrName: string;
        calculated: boolean;
        hidden: boolean;
        nullable?: boolean;
        unique?: boolean;
        reference?: boolean;
        system?: boolean;

        name?: any;
        type?: any;
        referencingBoMetaId?: any;

        // TODO refactor
        searchPopoverOpen?: boolean;
        inputType?: string;

        // ...
    }


    export interface Bo {
        _flag?: Flag; // TODO rename
        boId: Number;
        boMetaId: string;
        attributes: Object;
        title?: string; // TODO
        info?: string; // TODO

        // TODO suBos and bos ?
        subBos?: Map<Bo> | any; // TODO
        bos?: Array<Bo>;

        subChartBos?: Map<Bo>;
        //links: Map<Link>;
        //links: Map<Link>;
        links: LinkContainer;
        restriction?: string;
        canWrite?: boolean;
        canDelete?: boolean;
        dropdownoptions: Object;
		lafParameter?: Object;

        submetas?: Map<Meta>;

        inputrequired?: InputRequired;
        executeCustomRule?: any; // TODO

        _layout?: any;
    }


    export interface InputRequired {
        specification: InputRequiredSepecification;
        result: any; // TODO
    }
    export interface InputRequiredSepecification {
        defaultoption: any;
        key: string;
        type: any;
        message: string;
    }

    export interface Meta {
        boMetaId: string;
        name?: string;
        detailBoMetaId?: string;
        processMetaId?: string;
        lafParameter?: any; // Look & Feel parameter
        attributes: Array<BoAttr>;
        //links: Map<Link>;
        links: LinkContainer;

		mandatorLevelId?: string;

        readonly?: boolean;

        titlePattern?: string; // TODO correct place?
        infoPattern?: string; // TODO correct place?

        searchfilter?: string; // TODO correct place?

        // tree
        acceptDraggable?: boolean;

    }

    export interface BoViewModel {

        boMetaId: string; // same as meta.boMetaId
        bos: Array<Bo>; // list of BOs in sideview
        selectedBo: Bo;

        meta: Meta;
        metaLink?: string; // TODO rename

        search?: string; // text search filter for bos
        searchfilter?: string; // search filter id   // TODO rename

        sort?: string; // TODO is this needed anymore

        dirty?: boolean; // true if selectedBo was modified
        all?: boolean; // true if all data is loaded

        total?: number; // total number of bos
        canCreateBo?: boolean; // true if it is allowed to create a BO

        table?: any; // layout

        selectedRows?: Array<any>; // TODO rename/remove - only used in tableview for selecting multiple bos
        sortExpression?: string; // TODO remove - only used in tableview
        svQuery?: string; // TODO remove - only used in tableview

        inValidFormSubmitted?: boolean;

        setSelectedBo?: Function;


        // TODO needed when opening subform data in detail view
        selectedBoIndex?: number;
        subentry?: BoViewModel;

        // tree
        seltab?: any; // TODO rename
        treesearch?: any;
        treesearchQuery?: any;
        treesearching?: any;

		generations?: any;
    }

}



module nuclos.model {
	import Map = nuclos.model.interfaces.Map;

    export class BoImpl implements nuclos.model.interfaces.Bo {
        _flag: nuclos.model.interfaces.Flag;
        boId: Number;
        boMetaId: string;
        attributes: Object;
        subBos: Array<nuclos.model.interfaces.Bo>;
        links: nuclos.model.interfaces.LinkContainer;
		dropdownoptions: Object;

		submetas: Map<nuclos.model.interfaces.Meta>;

        constructor() {

        }
    }
    export class LinkImpl implements nuclos.model.interfaces.Link {
        href: string;
        methods: Array<nuclos.model.interfaces.HTPPMethod>;
    }
    export class MetaImpl implements nuclos.model.interfaces.Meta {
        boMetaId: string;
        attributes: Array<nuclos.model.interfaces.BoAttr>;
        links: nuclos.model.interfaces.Map<nuclos.model.interfaces.Link>;

        addLink = (link: nuclos.model.interfaces.Link) => {
         //   this.links.addToMap(link);
        }
    }


    export class BoViewModelImpl implements nuclos.model.interfaces.BoViewModel {
        boMetaId: string;
        bos: Array<nuclos.model.interfaces.Bo>;
        selectedBo: nuclos.model.interfaces.Bo;
        meta: nuclos.model.interfaces.Meta;
    }

    export class Mandator {
		mandatorId: string;
		mandatorLevelId: string;
		name: string;
		path: string;
		color: string;
		links: nuclos.model.interfaces.LinkContainer;
	}
}