module nuclos.services {
	var loadingIndicatorThreshold = 1000; // wait x milliseconds before showing loading indicator
	var nrOfOpenRestRequests = 0;

	class HttpInterCeptor {
		constructor(private $q,
					private $location,
					private $localStorage,
					private dataService) {
			return {
				request: this.request.bind(this),
				// requestError: this.requestError.bind(this),
				response: this.response.bind(this),
				responseError: this.responseError.bind(this)
			} as HttpInterCeptor;
		}

		request(request) {
			if (request.url.indexOf('/rest/') !== -1) {
				this.restRequestStarted(request);
			}
			return request;
		}

		response(response) {
			if (response.config.url.indexOf('/rest/') !== -1) {
				this.restResponseReceived(response);
			}
			return response;
		}

		responseError(response) {
			console.warn("HTTP error in response.", response);

			if (response.config.url.indexOf('/rest/') !== -1) {
				nrOfOpenRestRequests = 0;
				setLoadingIndicator(false);
			}

			if (response.status === 401) { // Unauthorized
				if (this.$location.path().indexOf('login') === -1) {
					// remove dirty flag - otherwise navigation to login page will not be permitted
					if (this.dataService.getMaster()) {
						delete this.dataService.getMaster().dirty;
					}

					nuclos.auth.AuthService.instance.solve401(this.$location.path());
				}
			}

			return this.$q.reject(response);
		}

		restRequestStarted(request) {
			nrOfOpenRestRequests++;
			window.setTimeout(function () {

				if (nrOfOpenRestRequests > 0) {
					setLoadingIndicator(true);
				}
			}, loadingIndicatorThreshold);

			if (this.$localStorage.logAngularRESTCalls) {
				log.teal('[' + request.method + ']', request.url, request);
				logStackTrace();
			}
		}

		restResponseReceived(response) {
			nrOfOpenRestRequests--;

			if (nrOfOpenRestRequests <= 0) {
				setLoadingIndicator(false);
			}

			if (this.$localStorage.logAngularRESTCalls) {
				log.teal('\t\t[' + response.statusText + ']', response.config.url, response);
				logStackTrace();
			}

			if (this.$localStorage.logAngularRESTCallsContainingResponse) {
				if (JSON.stringify(response.data).indexOf(this.$localStorage.logAngularRESTCallsContainingResponse) != -1) {
					log.teal('found: ' + this.$localStorage.logAngularRESTCallsContainingResponse + '\t\t[' + response.statusText + ']', response.config.url, response);
					logStackTrace();
				}
			}
		};
	}

	/**
	 * intercept HTTP calls for debugging error handling and login handling
	 */
	nuclosApp.factory('httpInterCeptor', ($q, $location, $localStorage, dataService) =>
		new HttpInterCeptor($q, $location, $localStorage, dataService)
	);
	nuclosApp.config(['$httpProvider', function ($httpProvider) {
		$httpProvider.interceptors.push('httpInterCeptor');
	}]);
}