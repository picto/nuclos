module nuclos.services {
	var systemparameters = null;
	var restservices;

	nuclosApp.service('restservice', function ($http,
											   $cookieStore,
											   $cacheFactory,
											   $q,
											   $rootScope,
											   boService,
											   errorhandler: Errorhandler) {

		var rs = {

			//TODO: Do not param boMetaId and isTaskView, but the entity-meta with Hateout-Link
			loadMetaData: function (boMetaId, isTaskView) {
				var deferred = $q.defer();
				var metaLink = restHost + (isTaskView ? '/meta/searchfilter/' : '/boMetas/');

				$http.get(metaLink + boMetaId).success(function (data) {
					deferred.resolve(data);

				}).error(function (error) {
					errorhandler.show(error.responseText, error.status, $cookieStore);
					deferred.reject();

				});

				return deferred.promise;
			},


			developmentMode: function () {
				var deferred = $q.defer();
				rs.systemparameters().then(function (data) {
					deferred.resolve(data['ENVIRONMENT_DEVELOPMENT']);
				});
				return deferred.promise;
			}
			,

			systemparameters: function () {
				var deferred = $q.defer();
				if (systemparameters != null) {
					deferred.resolve(systemparameters);
				} else {
					$http.get(restHost + '/meta/systemparameters')
						.success(function (data) {
							systemparameters = data;
							deferred.resolve(systemparameters);
						})
						.error(function (jqxhr, textStatus) {
							console.warn("Unable to get systemparameters.");
							deferred.reject(false);
						});
				}
				return deferred.promise;
			}
			,
			searchfilters: function (uid) {
				var deferred = $q.defer();
				$http.get(restHost + '/meta/searchfilters/' + uid)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get searchfilters.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,
			sideviewMenuSelector: function () {
				var deferred = $q.defer();
				$http.get(restHost + '/meta/sideviewmenuselector/')
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get sideviewmenu.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,
			dataGet: function (link) {
				var deferred = $q.defer();
				$http.get(link)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get data.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,

			metaSearchfilter: function (uid) {
				var deferred = $q.defer();
				$http.get(restHost + '/meta/searchfilter/' + uid)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get meta searchfilter.");
						deferred.reject("Unable to get meta searchfilter.");
					});
				return deferred.promise;
			}
			,

			metaEntity: function (uid) {
				var deferred = $q.defer();
				$http.get(restHost + '/boMetas/' + uid)
					.success(function (data) {
						data.uid = data.boMetaId;
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get meta entity.");
						deferred.reject("Unable to get meta entity.");
					});
				return deferred.promise;
			}
			,

			dataList: function (link, filter) {
				var deferred = $q.defer();
				$http.get(link, sessionHeader(filter))
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get list.");
						deferred.reject("Unable to get list.");
					});
				return deferred.promise;
			}
			,

			matrix: function (bo, matrixTab) {

				var boMetaId = bo.boMetaId;
				var boId = bo.boId;

				var deferred = $q.defer();

				var matrixDataHref = restHost + "/data/data/matrix";

				$http.post(
					matrixDataHref,
					{
						"boId": "" + boId,
						"type": "MATRIX",
						"entity_matrix": matrixTab.entity_matrix,
						"entity_matrix_value_type": matrixTab.entity_matrix_value_type,
						"entity_x": matrixTab.entity_x,
						"entity_y": matrixTab.entity_y,
						"entity_field_matrix_x_refField": matrixTab.entity_field_matrix_x_refField,
						"entity_matrix_value_field": matrixTab.entity_matrix_value_field,
						"entity_field_matrix_parent": matrixTab.entity_field_matrix_parent,
						"entity_y_parent_field": matrixTab.entity_y_parent_field,
						"entity_x_sorting_fields": matrixTab.entity_x_sorting_fields,
						"entity_y_sorting_fields": matrixTab.entity_y_sorting_fields,
						"entity_x_header": matrixTab.entity_x_header,
						"entity_y_header": matrixTab.entity_y_header,
						"entity_matrix_reference_field": matrixTab.entity_matrix_reference_field,
						"entity_x_vlp_id": matrixTab.entity_x_vlp_id,
						"entity_x_vlp_idfieldname": matrixTab.entity_x_vlp_idfieldname,
						"entity_x_vlp_fieldname": matrixTab.entity_x_vlp_fieldname,
						"entity_x_vlp_reference_param_name": matrixTab.entity_x_vlp_reference_param_name,
						"cell_input_type": matrixTab.cell_input_type,
						"editable": matrixTab.editable,
						"entity": matrixTab.entity
					}
				).success(function (matrixData) {

					var entity_field_matrix_parentShort = matrixTab.entity_field_matrix_parent.substring(matrixTab.entity_field_matrix_parent.lastIndexOf('_') + 1);

					for (var i = 0; i < matrixData.xrefData.length; i++) {
						var yAxisBoId = matrixData.xrefData[i].attributes[entity_field_matrix_parentShort].id;
						// set xref data
						for (var j = 0; j < matrixData.bos.length; j++) {
							var subsubformBoId = matrixData.bos[j].boId;
							if (yAxisBoId == subsubformBoId) {
								if (matrixData.bos[j].subBos[matrixTab.entity_field_matrix_parent] != null) {
									if (matrixData.bos[j].subBos[matrixTab.entity_field_matrix_parent].bos === undefined) {
										matrixData.bos[j].subBos[matrixTab.entity_field_matrix_parent].bos = [];
									}
									matrixData.bos[j].subBos[matrixTab.entity_field_matrix_parent].bos.push(matrixData.xrefData[i]);
								}
							}
						}
					}

					deferred.resolve(matrixData);
				}).error(function (data, status) {
					errorhandler.show(data, status, $cookieStore);
				});

				return deferred.promise;
			}
			,

			evaluateTitleExpression: function (boMetaId, boId, expression) {
				var deferred = $q.defer();

				$http.post(restHost + '/data/evaluatetitleexpression/' + boMetaId + '/' + boId, expression)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get statusinfo.");
						deferred.reject(false);
					});

				return deferred.promise;
			}
			,

			tree: function (boMetaId, boId) {
				var deferred = $q.defer();
				$http.get(restHost + '/data/tree/' + boMetaId + '/' + boId)
					.success(function (data) {
						for (var i in data) {
							data[i].iconUrl = restHost + "/meta/icon/" + data[i].icon;
						}
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get tree.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,

			treesearch: function (boMetaId, boId, search) {
				var deferred = $q.defer();
				$http.get(restHost + '/data/tree/' + boMetaId + '/' + boId + '/' + search)
					.success(function (data) {
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get tree.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,

			subtree: function (nodeid) {
				var deferred = $q.defer();
				$http.get(restHost + '/data/subtree/' + nodeid)
					.success(function (data) {
						for (var i in data) {
							data[i].iconUrl = restHost + "/meta/icon/" + data[i].icon;
						}
						deferred.resolve(data);
					})
					.error(function (jqxhr, textStatus) {
						console.warn("Unable to get subtree.");
						deferred.reject(false);
					});
				return deferred.promise;
			}
			,

			restservicelist: function () {
				var cache = $cacheFactory.get('restservicelist');
				if (cache == null) {
					cache = $cacheFactory('restservicelist');
				}

				var deferred = $q.defer();

				var restserviceList = cache.get();
				if (restserviceList) {
					deferred.resolve(restserviceList);
				} else {
					$http.get(restHost + '/meta/restservices')
						.success(function (data) {
							restserviceList = [];
							if (data.services) {
								for (var j = 0; j < data.services.length; j++) {
									var service = data.services[j];
									service.basePath = service.path;
									if (service.path != null && service.path.indexOf("{") != -1) {
										service.basePath = service.path.substring(0, service.path.indexOf("{"));
										// pathParams
										var tokens = service.path.split(/({.*?})/);
										service.pathParams = [];
										if (tokens != null) {
											for (var t = 0; t < tokens.length; t++) {
												var token = tokens[t];
												if (token && token.indexOf("{") == 0) {
													service.pathParams.push(token.substring(1, token.length - 1));
												}
											}
										}
									}
									restserviceList.push(service);
								}
							}

							restservices = restserviceList;
							deferred.resolve(restserviceList);
						})
						.error(function (jqxhr, textStatus) {
							errorhandler.show(jqxhr, textStatus, $cookieStore);
							console.error("Unable to get restservice info.");
							deferred.reject(false);
						});

					return deferred.promise;
				}
			}

		};

		return rs;
	});

}

/**
 * TODO: Get rid of this hack!
 *
 * @constructor
 */
function BoViewModelHack() {
	this.selectedBoIndex = null;
}

BoViewModelHack.prototype.getSubform = function (boMetaId) {
	//TODO: Avoid any JSON conversions for performance
	return JSON['search'](this, '//subform[boMetaId="' + boMetaId + '"]');
}

BoViewModelHack.prototype.getSelectedBo = function () {
	if (this.bos === undefined) {
		return undefined;
	}
	return this.bos[this.selectedBoIndex];
}

BoViewModelHack.prototype.setSelectedBo = function (bo) {
	this.selectedBo = bo;
	if (!bo) {
		this.selectedBoIndex = undefined;
	} else {
		this.selectedBoIndex = this.bos ? this.bos.indexOf(bo) : undefined;
	}
}

BoViewModelHack.prototype.extendJson = function (entity) {
	if (entity && entity['getSelectedBo'] === undefined) { // only if not done already
		for (var key in BoViewModelHack.prototype) {
			entity[key] = BoViewModelHack.prototype[key];
		}
	}
}