module nuclos.util {

	import Meta = nuclos.model.interfaces.Meta;
	import IPreference = nuclos.preferences.IPreference;
	import ISearchTemplate = nuclos.chart.ISearchTemplate;

	/**
	 * provides utility functions
	 */
	export class UtilService {

		
		constructor(
			private localePreferences: nuclos.clientprefs.LocalePreferenceService,
			private $log: ng.ILogService
        ) {
		}

		public hashCode(obj: Object): number {
			if (obj == null) {
				return null;
			}
			var s = JSON.stringify(obj);
		    var hash = 0;
		    if (s.length == 0) return hash;
		    for (var i = 0; i < s.length; i++) {
		        var character = s.charCodeAt(i);
		        hash = ((hash<<5)-hash)+character;
		        hash = hash & hash; // Convert to 32bit integer
		    }
		    return hash;
		}
		
		public jsonStringifyWithFunctions(objWithFunctions: Object): string {
			if (!objWithFunctions) {
				return undefined;
			}
			return JSON.stringify(objWithFunctions, function(key, val) {
				if (typeof val === 'function') {
					return val + ''; // implicitly `toString` it
				}
				return val;
			}, 2);
		}

		
		public jsonParseWithFunctions(jsonText: string): Object {
			if (!jsonText) {
				return undefined;
			}
			return JSON.parse(jsonText, function (key, value) {
				if (value && (typeof value === 'string') && value.indexOf('function') === 0) {
					// we can only pass a function as string in JSON ==> doing a real function
					try {
						var jsFunc = new Function('return ' + value)();
						return jsFunc;							
					} catch (Error) {
						console.warn("unable to access: ", value);					
						return value;
					}
				}
					
				return value;
			});
		}


		/**
		 * creates an array of FQN's used in inputString, like:
		 * "${com_example_Test_attribute1} ${com_example_Test_attribute2}"
		 * ->
		 * ['com_example_Test_attribute1', 'com_example_Test_attribute1'] 
		 */
		public parseFqns(inputString: string): string[] {
			var result = [];
			var fqnRegexp = /\${([^\}]+)}/g;
			do {
				var match = fqnRegexp.exec(inputString);
				if (match != undefined) {
					var fqn = match[1];
					result.push(fqn);
				}
			} while (match != undefined)
			return result;
		}
		
		public formatDate(date) {
			if (date == undefined || date.length < 10) {
				return '';
			}
			if (this.localePreferences.getLocale()==="de") {
				return moment(date).format("DD.MM.YYYY");
			} else {
				return moment(date).format("MMM DD. YYYY");
			}
		}

		public formatTimestamp(timestamp: Date): string {
			if (timestamp == undefined) {
				return '';
			}
			if (this.localePreferences.getLocale()==="de") {
				return moment(timestamp).format("DD.MM.YYYY HH:mm");
			} else {
				return moment(timestamp).format("MMM DD. YYYY h:mm A");
			}
		}


        public setInputType(attribute) {
            if (attribute.reference) {
                attribute.inputType = 'reference';
            } else if (attribute.type == 'Decimal' || attribute.type == 'Integer') {
                attribute.inputType = 'number';
            } else if (attribute.type == 'String') {
                attribute.inputType = 'string';
            } else if (attribute.type == 'Date' || attribute.type == 'Timestamp') {
                attribute.inputType = 'date';
            } else if (attribute.type == 'Boolean') {
                attribute.inputType = 'boolean';
            }
        }

		/**
		 * Builds the "AND"-concatenated WHERE condition based on the given search object.
		 *
		 * TODO: searchObject type
		 *
		 * @param searchObject
		 * @param meta
		 * @returns {any}
		 */
		public buildWhereCondition(searchObject, meta: Meta): string {
			var result = {
				aliases: {}
			} as any;
			var index = 0;
			var andArray = [];

			// TODO: Get rid of this workaround
			searchObject = this.synchronizeAttributes(searchObject);

			for (var a in searchObject.attributes) {
				var searchElement = searchObject.attributes[a];

				if (!searchElement.operator || searchElement.enableSearch === false) {
					continue;
				}

				var attribute = this.getAttributeByFqn(meta, searchElement.boAttrId);
				this.setInputType(attribute);
				var fqn = searchElement.boAttrId;
				var alias = "col" + index;
				let useTicks = this.needsTicks(attribute, searchElement.operator);
				var andPart = alias + " " + searchElement.operator + " ";

				if (!this.isUnaryOperator(searchElement.operator)) {
					let value = this.formatValue(searchElement, attribute, useTicks);
					if (value === null) {
						this.$log.warn('No value given for non-unary operator "%o" on attribute "%o", ignoring', searchElement.operator, attribute);
						continue;
					}

					andPart += value;
				}

				andArray.push(andPart);
				result.aliases[alias] = {boAttrId: fqn};
				index++;
			}

			result.clause = andArray.join(" AND ");

			return result;
		}

		/**
		 * Determines if the ticks should be used to escape the value for the given attribute in a WHERE-query.
		 *
		 * @param attribute
		 * @param operator
		 * @returns {boolean}
		 */
		private needsTicks(attribute: any, operator: string) {
			if (attribute.system) {
				return true;
			}
			else if (attribute.inputType === 'number') {
				return false;
			}
			else if (attribute.inputType === 'reference' && operator === '=') {
				return false;
			}

			return true;
		}

		/**
		 * Validates and formats the search input value according to the "input type" of the attribute.
		 *
		 * @param searchElement
		 * @param attribute
		 * @param useTicks
		 * @returns {any}
		 */
		private formatValue(
			searchElement: any,
			attribute: any,
			useTicks: boolean
		): string {
			var value = searchElement.value;

			this.setInputType(attribute);

			if (attribute.inputType == 'reference') {
				value = (value && value.id) ? value.id : value;
			}

			if (value !== undefined
				&& value.length === undefined
				&& (
					attribute.inputType == 'number'
					|| attribute.inputType == 'reference'
					|| attribute.inputType == 'date'
				)
				|| value !== undefined && value.length > 0) {

				if (useTicks) {
					if (attribute.inputType == 'date') {
						value = moment(value).format('YYYY-MM-DD');
					}
					if (typeof value !== 'number') {
						value = value.replace(/'/g, ''); // remove '
					}
				}
				if (useTicks) {
					value = "'" + value + "'";
				}
			}
			else {
				value = null;
			}

			return value;
		}

		public getRequestParameter(name) :string {
            if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.href)) {
                return decodeURIComponent(name[1]);
            }
        }

		/**
		 * Searches by the given attribute FQN for the corresponding attribute in the given Meta.
		 *
		 * @param meta
		 * @param attributeFqn
		 * @returns {any}
		 */
		private getAttributeByFqn(meta: Meta, attributeFqn: string) {
			let attributeName = attributeFqn.replace(meta.boMetaId, '').replace('_', '');
			return meta.attributes[attributeName];
		}

		/**
		 * Determines if the given operator is unary.
		 *
		 * TODO: Available operators and their types should be defined somewhere centrally.
		 *
		 * @param operator
		 * @returns {boolean}
		 */
		private isUnaryOperator(operator: string): boolean {
			return operator === 'is null' || operator === 'is not null';
		}

		/**
		 * Copys all attributes from searchObject.allAttributes to searchObject.attributes.
		 * This is just a workaround to keep old code working and should be removed in the future.
		 *
		 * @param searchObject
		 * @returns {any}
		 */
		private synchronizeAttributes(searchObject: any) {
			searchObject = angular.copy(searchObject);

			if (!searchObject.allAttributes) {
				return searchObject;
			}

			for (let allAttribute of searchObject.allAttributes) {
				if (!allAttribute.selected) {
					continue;
				}
				let found: boolean = false;
				for (let attribute of searchObject.attributes) {
					if (attribute.boAttrId === allAttribute.boAttrId) {
						attribute.value = allAttribute.value;
						found = true;
						break;
					}
				}
				if (!found) {
					searchObject.attributes.push(allAttribute);
				}
			}
			return searchObject;
		}
	}

	nuclosApp.service("util", UtilService);

}
