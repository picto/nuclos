module nuclos.services {

	interface ICommand {
		id: number,
		commandType: string
	}

	interface IMessageboxCommand extends ICommand {
		title: string,
		message: string
	}

	interface INavigateCommand extends ICommand {
		additionalNavigation: string,
		path: string,
		newTab: boolean
	}

	/**
	 * Handles org.nuclos.api.command.Commands to the client.
	 */
	export class CommandService {
		constructor(
			private $uibModal: angular.ui.bootstrap.IModalService,
			private $window: ng.IWindowService,
			private $rootScope: ng.IScope,
			private $log: ng.ILogService
		) {
		}

		/**
		 * Executes the given command.
		 *
		 * TODO: Make this more generic to avoid if-else switches and casting.
		 *
		 * @param command
		 */
		handleCommand(command: ICommand) {
			this.$log.debug('Handling command: %o', command);

			if (command.commandType === "MESSAGEBOX") {
				this.messageboxCommand(<IMessageboxCommand>command);
			} else if (command.commandType === "NAVIGATE") {
				this.navigateCommand(<INavigateCommand>command);
			} else if (command.commandType === "CLOSE") {
				this.closeCommand(command);
			}
		}

		/**
		 * Displays a message box.
		 *
		 * @param command
		 */
		public messageboxCommand(command: IMessageboxCommand) {
			this.$uibModal.open({
				templateUrl: 'app/util/error-dialog.html',
				scope: this.$rootScope,
				controller: ($scope, $uibModalInstance) => {

					$scope.dialog = {
						header: command.title,
						messageTitle: command.message
					};

					$scope.ok = () => {
						$uibModalInstance.close();
					};
				}
			});
		}

		/**
		 * Navigates to another browser location.
		 *
		 * @param command
		 */
		public navigateCommand(command: INavigateCommand) {
			var href = location.href.substring(0, location.href.indexOf('/#/'));

			if (command.additionalNavigation != null) {
				if (command.additionalNavigation === "LOGIN") {
					href = href + '/#/login';
				} else if (command.additionalNavigation === "LOGOUT") {
					href = href + '/#/logout';
				}
			} else {
				href = href + '/#/sideview/' + command.path;
			}
			if (command.id != null) {
				href = href + '/' + command.id;
			}
			if (command.newTab) {
				this.$window.open(href);
			} else {
				this.$window.location.href = href;
			}
		}

		/**
		 * Closes the current window.
		 *
		 * @param command
		 */
		public closeCommand(command: ICommand) {
			this.$log.debug('Closing the window');
			this.$window.close();
		}
	}

	nuclosApp.service('commandService', CommandService);
}