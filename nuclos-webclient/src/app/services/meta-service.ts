module nuclos.core {

	import IPromise = angular.IPromise;
	export class MetaService {

		private metaDataCache: ng.ICacheObject;
		
		constructor(
			private $cookieStore: ng.cookies.ICookiesService,
			private $q: ng.IQService,
			private $resource: ng.resource.IResourceService,
			private $cacheFactory,
			private errorhandler
        ) {
            this.metaDataCache = this.$cacheFactory('metaDataCache');
		}

		public clearCache() {
			this.metaDataCache.removeAll();
		}

		private getBoMetaDataImpl(metaLink, bQuery): IPromise<Array<nuclos.model.interfaces.Meta>> {
			var deferred = this.$q.defer();

			var metaDataFromCache = this.metaDataCache.get(metaLink);
			if (metaDataFromCache) {				
				deferred.resolve(metaDataFromCache);
	            return deferred.promise;		
			}
			
			var success = (data) => {
    		    this.metaDataCache.put(metaLink, data);
    		    deferred.resolve(data);
			};
			
			var fail = (error) => {
				this.errorhandler.show(error.data, error.status);
                deferred.reject();
            };
    			
            if (bQuery) {
            	this.$resource(metaLink).query(success, fail);
            } else {
            	this.$resource(metaLink).get(success, fail);
            }
			
            return deferred.promise;		
        }


		private getBoMetaList() {
			var authentication: any = this.$cookieStore.get('authentication');
			return this.getBoMetaDataImpl(authentication.links.boMetas.href, true);
		}

		/**
		 * @param {String} boMetaId
		 * @param {String} [refField] reference to mainform if called for a subform
		 */
		public getBoMetaData(boMetaId, refField?): IPromise<nuclos.model.interfaces.Meta> {
			var link = restHost + '/boMetas/' + boMetaId;
			if (refField) {
				link += '/subBos/' + refField;
			}
			
            return this.getBoMetaDataByLink(link);
		}

		public getBoMetaDataByLink(link): IPromise<nuclos.model.interfaces.Meta> {
			var deferred = this.$q.defer();
			
			this.getBoMetaDataImpl(link, false).then(
				(metaData) => {
					deferred.resolve(metaData);
				},
				(error) => {
					deferred.reject();
				}
			);
			return deferred.promise;
		}

		public getBoLinks(boMetaId): IPromise<any> {
			var deferred = this.$q.defer();
			
			this.getBoMetaList().then((data) => {
				
				var found = false;
				for (var i in data) {
					if (data[i].boMetaId == boMetaId && data[i].links) {
						var links = data[i].links.bos;
						if (links) {
							deferred.resolve(links);
							found = true;
						}
					}
				}
				if (!found) {
					/* TODO: this is a workaround - sometimes entities are not in getBoMetaList(..) */
					console.warn("Unable to find meta data for: ", boMetaId);
					// TODO promise reject handling					
					deferred.resolve();
				}
				
			});
			
            return deferred.promise;	
		}


		public canCreateBo(boMetaId): IPromise<boolean> {
			var deferred = this.$q.defer();
			
			this.getBoLinks(boMetaId).then((data) => {
				
				for (var i in data.methods) {
					if (data.methods[i] === 'POST') {
						return deferred.resolve(true);
					}
				}
				
				return deferred.resolve(false);
				
			});
			
			return deferred.promise;
		}

		/**
		 * Fetches all available layouts for the given entity.
		 *
		 * TODO: Caching?
		 *
		 * @param boMetaId
		 * @returns {IPromise<T>}
		 */
		public getLayoutsForEntity(boMetaId) {
			var deferred = this.$q.defer();

			this.$resource(restHost + '/boMetas/' + boMetaId + '/layouts').query(
				(data) => {
					deferred.resolve(data);
				},
				(error) => {
					deferred.reject(error);
				}
			);

			return deferred.promise;
		}

	}

	nuclosApp.service("metaService", MetaService);

}
