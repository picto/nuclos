module nuclos.dialog {

	export interface IDialogScope extends ng.IScope {
		dialog: {header: string, message: string};
		ok: Function;
		cancel: Function;
        registerModalInstance: Function;
        closeModalInstances: Function;
	}

	
	export class DialogCtrl {
		constructor(
            private header,
            private message,
            private showCancelButton,
            private $uibModalInstance,
            private $timeout
        ) {
		}
            
        ok = () => {
            this.$uibModalInstance.close();
        };
        
        cancel = () => {
            this.$uibModalInstance.dismiss('cancel');
        };
    }
    
	export class DialogService {
		
		$uibModal: any;
		$timeout: ng.ITimeoutService;
	
        static $inject = ["$uibModal", "$timeout"];
	
		constructor($uibModal, $timeout: ng.ITimeoutService) {
			this.$uibModal = $uibModal;
			this.$timeout = $timeout;
		}

        // collect modalInstances to make it possible to close them outside the modal controller		
        modalInstances = [];
        
        registerModalInstance = (modalInstance) => {
            this.modalInstances.push(modalInstance);
        };
        
        closeModalInstances = () => {
            for (var modalInstance of this.modalInstances) {
                modalInstance.close();    
            }
            this.modalInstances = [];
        };
        
		focusOnOk = () => {
            this.$timeout(function() {
            	$('#okButton').focus();
            }, 800);			
		};

		alert = (title: string, text: string, okFunction?: Function, windowClass?: string) => {
			
			// open dialog
			this.$uibModal.open({
				templateUrl: 'app/util/dialog.html',
				windowClass: windowClass,
                controller: 'DialogInstanceCtrl as vm',
				resolve: {
                    header: function() {
                        return title;
                    },
                    message: function() {
                        return text;
                    },
                    showCancelButton: function() {
                        return false;
                    },
				}
			}).result.then(okFunction);
			
			//NUCLOS-4910 Don't focus and scroll disclaimers
			if (windowClass != 'halfwidth-modal-window') {
				this.focusOnOk();				
			}
		};
              
        confirm = (title, text, okFunction: Function, cancelFunction: Function) => {
            
			// open dialog
			this.$uibModal.open({
				templateUrl: 'app/util/dialog.html',
                controller: 'DialogInstanceCtrl as vm',
				resolve: {
                    header: function() {
                        return title;
                    },
                    message: function() {
                        return text;
                    },
                    showCancelButton: function() {
                        return true;
                    }
                    
				}
			}).result.then(okFunction, cancelFunction);
			
			this.focusOnOk();
		};
		
		preview = (title: string, text: string) => {
			
			// open dialog
			this.$uibModal.open({
				templateUrl: 'app/util/preview.html',
				windowClass: 'fullwidth-modal-window',
                controller: 'DialogInstanceCtrl as vm',
				resolve: {
                    header: function() {
                        return title;
                    },
                    message: function() {
                        return text;
                    },
                    showCancelButton: function() {
                        return false;
                    },
				}
			}).result.then();
			
		};
	}
}
nuclosApp.controller('DialogInstanceCtrl', nuclos.dialog.DialogCtrl);
nuclosApp.service("dialog", nuclos.dialog.DialogService);
