module nuclos.core {

	import BoViewModel = model.interfaces.BoViewModel;

	export class DataService {

		constructor(
			private $rootScope: ng.IScope
        ) {
		}

		public getMaster(): BoViewModel {
			var master = null;
			if(this.$rootScope['master'] != null) {
				master = this.$rootScope['master'];
			} else {
				var scope = angular.element($("#main")).scope();
				master = scope != null ? scope['master'] : null;
			}

			if (master) {
				master.setSelectedBo = function(bo) {
					this.selectedBo = bo;
					if (!bo) {
						this.selectedBoIndex = undefined;
					} else {
						this.selectedBoIndex = this.bos ? this.bos.indexOf(bo) : undefined;		
					}
				};

				master.getSubform = function(boMetaId) {
					//TODO: Avoid any JSON conversions for performance
					return JSON['search'](this, '//subform[boMetaId="' + boMetaId + '"]');
				}

			}
			
			return master;
		}

		public setMaster(entity: BoViewModel) {
			this.$rootScope['master'] = entity;
		}

	}
}
angular.module("nuclos").service("dataService", nuclos.core.DataService);