module nuclos.services {
	/**
	 open modal for editing

	 will be called from:
	 - new subrow button
	 - drag&drop from tree to subform
	 - edit existing row in subform
	 - new entry from tableview
	 - edit existing entry from tableview

	 expected data:
	 {
		 entity: {},
		 parentEntity: {},                   // required for subforms - if a new entry is created it will be added to parentEntity.bos
		 row: {},                            // not required, if present the modal will be filled with the data - used for editing an existing row
		 preselectReferenceData: {},         // not required, if present the references found will be preselected with given value - used for drag&drop
		 scope: {},                          // scope below the modal will open
		 callback: {ok: {}, dismiss: {}}     // callbacks which will be called when clicking ok or dismissing the dialog
	 }

	 */
	class ModalService {
		constructor(
			private $q: ng.IQService,
			private $uibModal,
			private $rootScope,
			private $timeout: ng.ITimeoutService,
			private restservice,
			private layoutService: nuclos.layout.LayoutService,
			private boService: nuclos.core.BoService,
			private metaService: nuclos.core.MetaService,
			private dataService: nuclos.core.DataService
		) {
		}

		openEditModal(data) {
			var $scope = data.scope;
			var boMetaId = data.entity.boMetaId;

			var getAutonumberAttribute = () => {
				var attributes = data.entity.meta.attributes;
				for (var a in attributes) {
					var attribute = attributes[a];
					if (attribute.defcomptype == 'Autonummer') {
						return a;
					}
				}
				return;
			};


			var _newSubRow = (entity, parentEntity): ng.IPromise<nuclos.model.interfaces.Bo> => {
				var deferred = this.$q.defer();

				if (!entity.meta.boMetaId) entity.meta.boMetaId = entity.boMetaId;
				this.boService.emptyBO(entity, parentEntity).then(subBo => {

					//TODO: The Meta-Data of subforms have the attributes in "fields", this should be equal to the meta-data of the main entities
					prepareBoValuesAndOptions(entity.meta.attributes, subBo);

					if (!entity.bos) { // no subbos
						entity.bos = [];
					}
					entity.bos.unshift(subBo); // insert at first position
					subBo._flag = 'insert';

					if (data.preselectReferenceData) { // call via drag & drop
						for (var a in data.entity.meta.attributes) {
							var attr = data.entity.meta.attributes[a];
							if (attr.referencingBoMetaId == data.preselectReferenceData.boMetaId) {
								var attrName = getShortFieldName(entity, attr.boAttrId);
								subBo.attributes[attrName] = {
									id: parseInt(data.preselectReferenceData.boId),
									name: data.preselectReferenceData.title
								};
							}
						}
					}

					// if subbo has an autonumber attribute then set it to the next number
					var autonumberAttributeName = getAutonumberAttribute();
					if (autonumberAttributeName) {
						var lastNumber = 0;
						for (var b in entity.bos) {
							var autonumberValue = entity.bos[b].attributes[autonumberAttributeName];
							if (autonumberValue > lastNumber) {
								lastNumber = autonumberValue;
							}
						}
						var nextNumber = lastNumber + 1;
						subBo.attributes[autonumberAttributeName] = nextNumber;
					}

					deferred.resolve(subBo);
				});

				return deferred.promise;
			};

			var _openDialog = () => {
				// save function for tableview
				data.scope.saveBo = (editedBO, $dismiss) => {
					var master = this.dataService.getMaster();

					this.boService.submitBOForInsertUpdate(editedBO, master, editedBO._flag).then(
						data => {
							// ok
							this.$rootScope.$broadcast('reloadtabledata');
							$dismiss();
							this.$timeout(() => {
								this.$rootScope.$broadcast('refreshgrids');
							}, 0);
						}, () => {
							// not ok
						});
				};

				$scope.deleteBo = (editedBO, $dismiss) => {
					this.$rootScope.$broadcast('deletebo', editedBO);
					$dismiss();
				};

				// open dialog
				var modalInstance = this.$uibModal.open({
					templateUrl: 'app/detail/dialog.html',
					size: 'lg',
					windowClass: 'fullsize-modal-window',
					scope: data.scope,
					controller: ($scope, $uibModalInstance, dialog, modelEntity) => {
						$scope.master = modelEntity;
						$scope.dialog = dialog;
						$scope.ok = function () {
							$uibModalInstance.close();
						};

						// focus first input field of opened modal
						this.$timeout(() => {
							$($('#detailblockModal input:not(disabled)').get(0)).focus();
						}, 1000);
					},
					resolve: {
						modelEntity: function () {
							//	$scope.modelEntity = entity;
							//	return $scope.modelEntity;
							return data.entity;
						},
						dialog: function () {
							$scope.dialog = {};
							return $scope.dialog;
						}
					}
				});

				modalInstance.result.then(selectedItem => {
					// ok clicked

					if (data.callback && data.callback.ok) {
						data.callback.ok();
					}

					if (data.parentEntity) {
						data.parentEntity.dirty = true;
					}

					if (!data.parentEntity.selectedBo.boId) { // if main entry is new
						$scope.$broadcast('refreshgrids');
					}

					modalInstance.close();
				}, () => {
					// modal dismissed - esc / cancel clicked

					//TODO Actually the fix for NUCLOS-4199 f) never was completed, so this code is commented out
					//NUCLOS-4199 f)
//					if (data.parentEntity && !data.scope.showCancelButton) {
//						data.parentEntity.dirty = true;
//					}

					//TODO: This makes only sense when a new subrow was added. For the editing of existing subrows, it doesn't work at all.

					// remove recently added unsaved entry from list
					removeLastAddedInsert(data.entity);

					if (data.callback && data.callback.dismiss) {
						data.callback.dismiss();
					}

					modalInstance.close();
				});
			};

			var row = null;

			if (data.parentEntity) { // subform entry in modal

				// subformbuttons (Ok/Delete) inside edit modal should only show up for subforms - not for edit modal from tableview
				$scope.showSubformButtons = true;
				$scope.showCancelButton = true;

				if (data.row) { // existing subform entry
					this.metaService.getBoMetaData(data.entity.boMetaId).then(
						boMeta => { // TODO data.entity.meta werden beim Klick auf den Subformtab überschrieben, hier werden die Daten wieder geladen
							data.entity.meta.links = boMeta.links;
							data.entity.meta.attributes = boMeta.attributes;

							if (data.entity.meta.links || data.entity.meta.links.defaultLayout) {
								BoViewModelHack.prototype.extendJson(data.entity);

								$scope.showCancelButton = false;
								_openDialog();
								this.$timeout(() => {
									this.layoutService.createTableForThisBO(data.entity, data.row).then(
										layoutLoadedActions => {
											data.entity.setSelectedBo(data.row);
											this.$rootScope.$broadcast('rowselected', {
												layoutLoadedActions: layoutLoadedActions,
												bo: data.row
											});
										}, error => {
											console.error(error);
										});
								}, 0);
							}
						});
				} else { // new subform entry
					this.metaService.getBoMetaData(boMetaId).then(metaData => {
						data.entity.meta = metaData;

						_newSubRow(data.entity, data.parentEntity).then(newSubrow => {
							BoViewModelHack.prototype.extendJson(data.entity);
							data.entity.setSelectedBo(newSubrow);

							var openModal = false;
							if (metaData.links.defaultLayout) {
								openModal = true;

								if (data.parentEntity) {
									var subforms = data.parentEntity.getSubform(boMetaId);
									if (subforms) {
										for (var c in subforms) {
											var ignorsublayout = subforms[c].ignoresublayout;
											if (ignorsublayout) {
												openModal = false;
											} else {
												openModal = true;
												break;
											}
										}
									}
								}
							}

							if (openModal) { // if layout exists open modal
								newSubrow._layout = metaData.links.defaultLayout.href; // use initial layout from Entity
								_openDialog();
								this.$timeout(() => {
									this.layoutService.createTableForThisBO(data.entity, newSubrow).then(
										layoutLoadedActions => {
											this.$rootScope.$broadcast('rowselected', {
												layoutLoadedActions: layoutLoadedActions,
												bo: data.row
											});
										});
								}, 0);
							} else {
								if (!data.parentEntity.selectedBo.boId) { // if main entry is new
									this.$rootScope.$broadcast('refreshgrids');
								}
								data.parentEntity.dirty = true;

								if (data.callback && data.callback.ok) {
									this.$timeout(() => {
										data.callback.ok();
									}, 0);
								}
							}
						});
					});
				}
			} else { // edit main entry in modal (from tableview)
				$scope.showTableviewFormButtons = true;
				$scope.showDeleteButton = data.row && data.row.links && data.row.links.self && data.row.links.self.methods.indexOf('DELETE') != -1;

				if (data.row) { // existing entry
					this.restservice.loadMetaData(data.entity.boMetaId).then(metaData => {
						data.entity.meta = metaData;
						data.entity.setSelectedBo(data.row);

						data.row._layout = metaData.links.defaultLayout.href; // TODO use layout from bo (depends on status/process/..)

						_openDialog();
						this.$timeout(() => {
							this.layoutService.createTableForThisBO(data.entity, data.row).then(
								layoutLoadedActions => {
									this.$rootScope.$broadcast('rowselected', {
										layoutLoadedActions: layoutLoadedActions,
										bo: data.row
									});
								});
						}, 0);
					});

				} else { // new entry
					this.restservice.loadMetaData(boMetaId).then(metaData => {
						data.entity.meta = metaData;

						_newSubRow(data.entity, null).then(newSubrow => {
							data.row = newSubrow;

							BoViewModelHack.prototype.extendJson(data.entity);
							data.entity.setSelectedBo(data.row);

							//ISD-297:
							//The emptyBo in bo-service already returned the correct layout-links.
							//Besides, the following code is wrong by itself. it destroys the existing data.row.

							//data.row = {links: {layout: {href: metaData.links.defaultLayout.href}}}; // use initial layout from Entity

							_openDialog();
							this.$timeout(() => {
								this.layoutService.createTableForThisBO(data.entity, data.row).then(
									layoutLoadedActions => {
										data.row.boMetaId = boMetaId;
									});
							}, 0);
						});
					});
				}
			}
		}
	}

	nuclosApp.service('boModalService', ModalService);
}


function removeLastAddedInsert(master) {
	var sublist = master.bos;
	if (sublist !== null && sublist.length >= 1) {
		if (sublist[sublist.length - 1] !== null && sublist[sublist.length - 1]._flag == 'insert') {
			sublist.pop();
		}
	}
}
