/**
 * Handle Client Side Preference
 *
 * TODO: Refactor this to eliminate ugly if-else blocks for every locale in every method!
 *
 */
module nuclos.clientprefs {
	export type LocaleString = 'de' | 'en';

	export class LocalePreferenceService {
		defaultLocale = {"key": "en", "name": "English"};
		locales = [{"key": "de", "name": "Deutsch"}, this.defaultLocale];

		constructor(private $cookies) {
		}

		public getDateFormat = () => {
			if (this.getLocale() === "de") {
				return "dd.MM.yyyy";
			} else {
				return "MM/dd/yyyy";
			}
		};

		public getNumberFormat = () => {
			// TODO
			return "##,00";
		};

		public getLanguage = () => {
			if (this.getLocale() === "de") {
				return "Deutsch";
			} else {
				return "English";
			}
		};

		public getDataLanguage = () => {
			// TODO

		};

		public getLocale = (): LocaleString => {
			return this.$cookies.get("locale");
		};


		public getDecimalSeparator = () => {
			if (this.getLocale() === "de") {
				return ",";
			} else {
				return ".";
			}
		};

		public getThousandsSeparator = () => {
			if (this.getLocale() === "de") {
				return ".";
			} else {
				return ",";
			}
		};

		public setLocale = (locale: LocaleString) => {
			this.$cookies.put('locale', locale);
		};

		public formatDate = date => {
			if (this.getLocale() === "de") {
				return moment(date).format('DD.MM.YYYY');
			} else {
				return moment(date).format('MMM/DD/YYYY');
			}
		}
	}
}

nuclosApp.service('localePreferences', nuclos.clientprefs.LocalePreferenceService);
