module nuclos.services {
	class MessageService {
		eventSourceUrl: string = null;

		constructor(private $rootScope,
					private $window) {

			// Browser Tab Scope
			this.$rootScope.browserTabId = Math.floor(Math.random() * 2147483647);
			this.eventSourceUrl = restHost + '/msg?receiverId=' + this.$rootScope.browserTabId
		}

		//HANDLE DIRECTIONs
		ruleDirectionHandle(event) {
			console.log("EventSource topic.ruleDirection")
			var direction = JSON.parse(event.data);
			console.log("EventSource direction.id=" + direction.id);
			console.log("EventSource direction.path=" + direction.path);
			console.log("EventSource direction.newTab=" + direction.newTab);

			var href = location.href.substring(0, location.href.indexOf('/#/')) + '/#/sideview/' + direction.path + '/' + direction.id;
			if (direction.newTab) {
				this.$window.open(href);
			} else {
				this.$window.location.href = href;
			}
		}

		//HANDLE MESSAGEs
		ruleNotificationHandler(event) {
			console.log("EventSource topic.ruleNotification")
			var message = JSON.parse(event.data);
			console.log("EventSource message.id=" + message.message);
			console.log("EventSource message.path=" + message.title);
			this.$window.alert(message.title + "\n" + message.message);
		}

		//HANDLE onOpen
		onSourceOpen(e) {
			console.log("EventSource open ", e);
		};

		//HANDLE onERROR
		onSourceError(e) {
			console.log("EventSource error ", e);

			if (eventSource) {
				eventSource.close();
			}
			this.unbind();

			setTimeout(() => {
				eventSource = new EventSource(this.eventSourceUrl, {withCredentials: true});
				this.bind();
				console.log("EventSource reopen");
			}, 2000);
		};

		//BIND ALL LISTENERs
		bind() {
			eventSource.addEventListener('topic.ruleDirection', ruleDirectionHandler, false);
			eventSource.addEventListener('topic.ruleNotification', ruleNotificationHandler, false);
			eventSource.addEventListener('error', this.onSourceError, false);
			eventSource.addEventListener('open', this.onSourceOpen, false);
		}

		//UNBIND ALL LISTENERs
		unbind() {
			eventSource.removeEventListener('topic.ruleDirection', ruleDirectionHandler, false);
			eventSource.removeEventListener('topic.ruleDirection', ruleNotificationHandler, false);
			eventSource.removeEventListener('error', this.onSourceError, false);
			eventSource.removeEventListener('open', this.onSourceOpen, false);
		}
	}

	nuclosApp.service('messageService', MessageService);
}
