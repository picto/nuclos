module nuclos.services {
	nuclosApp.service('errorhandler', function (
		$uibModal,
		$cookieStore,
		$rootScope,
		util,
		authService: nuclos.auth.AuthService,
		nuclosI18N: nuclos.util.I18NService
	) {
		var errorhandler = {
			errors: null,
			show: function (data, code) {
				// avoid multiple modals for the same error
				if (!errorhandler.errors) {
					errorhandler.errors = [];
				}
				var errorHashcode = util.hashCode({data: data, code: code});
				if (errorhandler.errors.indexOf(errorHashcode) != -1) {
					return;
				}
				errorhandler.errors.push(errorHashcode);


				if (typeof data === "string" && data.indexOf("<") !== 0 && data.indexOf("{") != -1) { // parse error if it isn't already a JSON object and if it isn't HTML
					data = JSON.parse(data);
				}

				if (code === undefined) {
					console.error(data);
				} else if (code === 401) {
					authService.solve401(window.location.href);

				} else {

					var allowStacktrace = true;
					var showWarningSign = false;
					var messageTitle = nuclosI18N.getI18n("webclient.error.code" + code);
					if (!messageTitle) {
						messageTitle = nuclosI18N.getI18n("webclient.error.unknowncode" + code);
					}

					//SHOP-156 HTTP-ERROR-CODE 412 "Precondition Code Failed" means it has been a Business Exception
					if (code == 412) {
						messageTitle = data.message;
						allowStacktrace = false;
						showWarningSign = true;
					}

					var scope = data ? data.scope : $rootScope;
					var stacktrace = data ? data.stacktrace : null;

					var modalInstance = $uibModal.open({
						templateUrl: 'app/util/error-dialog.html',
						scope: scope,
						controller: function ($scope, $uibModalInstance) {

							$scope.dialog = {
								messageTitle: messageTitle,
								stacktrace: stacktrace,
								showStacktrace: false,
								allowStacktrace: allowStacktrace,
								showWarningSign: showWarningSign
							};

							$scope.ok = function () {
								delete errorhandler.errors;
								$uibModalInstance.close();
							};
						}
					});


					modalInstance.result.then(function (selectedItem) {
						// ok clicked

						if (data && data.callback && data.callback.ok) {
							data.callback.ok();
						}

						if (data && data.parentEntity) {
							data.parentEntity.dirty = true;
						}

						delete errorhandler.errors;
						modalInstance.close();

					}, function () {
						// modal dismissed - esc / cancel clicked

						if (data.callback && data.callback.dismiss) {
							data.callback.dismiss();
						}

						delete errorhandler.errors;
						modalInstance.close();
					});
				}
			}

		};

		return errorhandler;
	});
}