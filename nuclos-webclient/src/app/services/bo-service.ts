module nuclos.core {

	import IPromise = angular.IPromise;
	import Bo = nuclos.model.interfaces.Bo;
	import InputRequired = nuclos.model.interfaces.InputRequired;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
	export class BoService {

		constructor(
			private $http: ng.IHttpService,
			private $cookieStore: ng.cookies.ICookiesService,
			private $q: ng.IQService,
			private $resource: ng.resource.IResourceService,
			private $rootScope: ng.IScope,
			private $log: ng.ILogService,
			private metaService: nuclos.core.MetaService,
			private authService: nuclos.auth.AuthService,
			private boGenerationService,
			private util: nuclos.util.UtilService,
			private errorhandler,
			private localePreferences: nuclos.clientprefs.LocalePreferenceService,
			private inputrequiredService,
			private browserRefreshService: nuclos.detail.BrowserRefreshService,
			private dataService: nuclos.core.DataService,
			private nuclosI18N: nuclos.util.I18NService,
			private valuelistproviderService: nuclos.detail.ValuelistproviderService,
			private commandService: nuclos.services.CommandService
        ) {
		}


		private buildFilter(entity, attributes, offset: number, chunkSize: number) {

			var filter = {
				offset: offset,
				chunksize: chunkSize,
				gettotal: false,
				countTotal: true,
				search: entity.search,
				searchFilter: entity.searchfilter,
				withTitleAndInfo: false,
				attributes: null,
				sort: null,
				searchCondition: null
			};

			// request only given attributes
			if (attributes !== undefined) {
				filter.attributes = 
					attributes
						// use short name instead of fqn to avoid problems with to long query param
						.map(attribute => getShortFieldName({ boMetaId: entity.boMetaId }, attribute))
						.join(',');
			}

			if (entity.sort !== undefined) {
				filter.sort = entity.sort;
			}

			var searchCondition = getSearchCondition(entity.metaData);
			if (searchCondition) {
				filter.searchCondition = searchCondition;
			}
			return filter;
		}

        public loadBOList(entity, offset, attributes?, chunkSize?, searchtemplate?): IPromise<IBoList> {

			this.$log.debug('loadBOList: search attributes: ' + (searchtemplate ? searchtemplate.attributes.length : ' - '));

			var deferred = this.$q.defer();


			delete entity.all;

			var queryObject = {
				where: null
			};

			if (searchtemplate !== undefined) {
                queryObject.where = this.util.buildWhereCondition(searchtemplate, entity.meta);
			}

			var filter = this.buildFilter(entity, attributes, offset, chunkSize);
			var _errorhandler = this.errorhandler;

			//TODO use HATEOAS
			var url = restHost + '/bos/' + entity.boMetaId + '/query';
			this.$resource(
				url,
				filter,
				{ 'bosquery': { method: 'POST' } }
			)['bosquery'](queryObject).$promise.then(

				(data) => {

					if (offset === 0 || !entity.bos) {
						entity.bos = data.bos;
					} else {
						for (var i = 0; i < data.bos.length; i++) {
							entity.bos.push(data.bos[i]);
						}
					}

					for (var i = 0; i < data.bos.length; i++) {
						this.updateTitleAndInfo(data.bos[i], entity.meta);
					}

					entity.all = data.all;
					entity.total = data.total;
					deferred.resolve(data);

				},
				(error) => {
					_errorhandler.show(error.responseText, error.status, this.$cookieStore);
					deferred.reject(error);

				});

            return deferred.promise;
        }

		/**
		 * set boId and refAttrId to export subform data
		 * @boId boId of main bo
		 * @refAttrId reference to main bo
		 */
        public boListExport(boId: number, refAttrId: string, boAttributes, format: string, pageOrientationLandscape: boolean, isColumnScaled: boolean) {

			pageOrientationLandscape = pageOrientationLandscape || false;
			isColumnScaled = isColumnScaled || false;

			var master = this.dataService.getMaster();
			var searchtemplate = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content;

			var deferred = this.$q.defer();


			var queryObject = {
				where: null
			};

			if (searchtemplate !== undefined) {
                queryObject.where = this.util.buildWhereCondition(searchtemplate, master.meta);
			}

			var filter = this.buildFilter(master, boAttributes, 0, null);
			var _errorhandler = this.errorhandler;

			//TODO use HATEOAS
			if (!boId || !refAttrId) {
				// main form data export
				var url = restHost + '/bos/' + master.boMetaId + '/boListExport/' + format + '/' + pageOrientationLandscape + '/' + isColumnScaled;
			this.$resource(
				url,
				filter,
				{ 'bosquery': { method: 'POST' } }
			)['bosquery'](queryObject).$promise.then(

				(data) => {
					deferred.resolve(data);
				},
				(error) => {
						_errorhandler.show(error.data, error.status);
					deferred.reject(error);
					}
				);
			} else {
				// subform data export
				
				var url = restHost + '/bos/' + master.boMetaId + '/' + boId + '/subBos/' + refAttrId + '/export/' + format + '/' + pageOrientationLandscape + '/' + isColumnScaled;
				this.$resource(
					url,
					filter,
					{ 'bosquery': { method: 'GET' } }
				)['bosquery']().$promise.then(
					(data) => {
						deferred.resolve(data);
					},
					(error) => {
						_errorhandler.show(error.data, error.status);
						deferred.reject(error);
					}
				);
			}

            return deferred.promise;
        }


		loadSingleBO(entity, bo, boList): IPromise<Bo> {

			var deferred = this.$q.defer();

			if (!boList) {
				boList = entity.bos;
			}

			this.metaService.getBoMetaData(entity.boMetaId).then((meta) => {
				entity.meta = meta;

				if (!bo.links || !bo.links.self) { // new unsaved entry
					deferred.resolve(null);
				} else {
					this.$resource(bo.links.self.href).get(
						(data) => {
							if (data) {
								bo = data;
							} else {
								bo._flag = 'delete';
							}

							assembleBoInBoList(bo, boList);
							prepareBoValuesAndOptions(entity.meta.attributes, bo);

							deferred.resolve(data);

						},
						(error) => {
							this.errorhandler.show(error.data, error.status);
							deferred.reject();
						});
				}
			},
				(error) => {
					console.error("Unable to get meta data.", error);
				}
			);

			return deferred.promise;
		}


		updateTitleAndInfo(bo: nuclos.model.interfaces.Bo, meta: Meta) {

			var fillPatternString = (bo, attributes, pattern) => {
				var result = pattern;
				attributes.forEach((attr) => {
					var attrShort = getShortFieldName({ boMetaId: meta.boMetaId }, attr);
					var value = bo.attributes[attrShort];
					if (value == undefined) {
						value = '';
					} else {
						if (value.name != undefined) {
							value = value.name;
						} else if (meta.attributes[attrShort].type == 'Boolean') {
							value = value ? this.nuclosI18N.getI18n('common.yes') : this.nuclosI18N.getI18n('common.no');
						} else if (meta.attributes[attrShort].type == 'Date') {
							value = this.util.formatDate(value);
						} else if (meta.attributes[attrShort].type == 'Timestamp') {
							value = this.util.formatTimestamp(value);
						}
					}
					result = result.replace('${' + attr + '}', value);
				});
				return result;
			};

			var titleAttributes = this.util.parseFqns(meta.titlePattern);
			var infoAttributes = this.util.parseFqns(meta.infoPattern);

			var attributes = titleAttributes.concat(infoAttributes);
			// remove duplicates
			attributes = attributes.filter(
				(v, i) => {
					return attributes.indexOf(v) == i;
				}
			);

			bo.title = fillPatternString(bo, attributes, meta.titlePattern);
			bo.info = fillPatternString(bo, attributes, meta.infoPattern);
		}


		/**
		 * @param {object} entity
		 * @param {object} [parentBo] if called for a subform
		 */
		emptyBO(entity: nuclos.model.interfaces.BoViewModel, parentBo?): ng.IPromise<Bo> { 
			var deferred = this.$q.defer();

			this.metaService.getBoMetaData(entity.boMetaId).then((meta) => {
				entity.meta = meta;

				this.boGenerationService.getNewBo(meta).then((bo) => {

					bo.dropdownoptions = {};
					bo._flag = "insert";
					bo._restrictions = {};

					for (var i in meta.attributes) {
						var attribute = meta.attributes[i];
						var fieldname = getShortFieldName(bo, attribute.boAttrId);

						var required = !attribute.nullable || attribute.unique;
						if (required && attribute.type === 'Boolean' && bo.attributes[fieldname] == undefined) {
							bo.attributes[fieldname] = false;
						}

						if (attribute.reference) {
							bo.dropdownoptions[fieldname] = [];

							// set reference to parentBo (new bo in subform)
							if (parentBo && attribute.referencingBoMetaId === parentBo.boMetaId) {
								bo.attributes[fieldname] = { id: parentBo.selectedBo.boId, name: parentBo.selectedBo.title };
							}
						}
					}

					// TODO promise reject handling
					this.metaService.getBoLinks(meta.boMetaId).then((links) => {
						bo.links = links;
						//NUCLOS-4875 Under certain circumstances (dependent of user data) links can be undefined
						if (!bo.links) {
							bo.links = {};
						}

						//NUCLOS-4171 1) bo.links.layout.href will be altered later. Thus angular.copy
						bo.links.layout = angular.copy(meta.links.defaultLayout);
						deferred.resolve(bo);
					});

				});

			});

			return deferred.promise;
		}

		temporaryBO(entity, bo) {
			var deferred = this.$q.defer();

			this.metaService.getBoMetaData(entity.boMetaId).then((meta) => {
				entity.meta = meta;

				assembleBoInBoList(bo, entity.bos);
				prepareBoValuesAndOptions(entity.meta.attributes, bo);

				deferred.resolve(bo);
			});

			return deferred.promise;
		}


        submitBOForInsertUpdate(bo, entity, flagSave) {

			var deferred = this.$q.defer();

            var postData = angular.copy(bo);

            postData.subBos = {
				insert: {},
				update: {},
				delete: {}
            };

			if (typeof bo.subBos == 'object') {

				for (var reffieldId in bo.subBos) {
					if (Array.isArray(bo.subBos[reffieldId].bos)) {
						var subBoArray = bo.subBos[reffieldId].bos;

						subBoArray.map((subBo) => {

							// implementation for sub-sub-form - TODO make recursive for deeper nesting
							//TODO: Avoid at any circumstances such an implementation. Elegent and recursive.
							//Anyway, doesn't work, see NUCLOS-4199
							if (subBo.subBos) {
								subBo.subBos.insert = {};
								subBo.subBos.update = {};
								subBo.subBos.delete = {};
								for (var subsubreffieldId in subBo.subBos) {
									var subSubBoArray = subBo.subBos[subsubreffieldId].bos;
									if (subSubBoArray) {
										subSubBoArray.map((subSubBo) => {
											if (subSubBo._flag === 'insert') {
												if (!subBo.subBos.insert[subsubreffieldId]) {
													subBo.subBos.insert[subsubreffieldId] = [];
												}
												subBo.subBos.insert[subsubreffieldId].push(subSubBo);
											} else if (subSubBo._flag === 'update') {
												if (!subBo.subBos.update[subsubreffieldId]) {
													subBo.subBos.update[subsubreffieldId] = [];
												}
												subBo.subBos.update[subsubreffieldId].push(subSubBo);
											} else if (subSubBo._flag === 'delete') {
												if (!subBo.subBos.delete[subsubreffieldId]) {
													subBo.subBos.delete[subsubreffieldId] = [];
												}
												subBo.subBos.delete[subsubreffieldId].push(subSubBo.boId);
											}
										});
									}
								}
							}

							if (subBo._flag === 'insert') {
								if (!postData.subBos.insert[reffieldId]) {
									postData.subBos.insert[reffieldId] = [];
								}
								postData.subBos.insert[reffieldId].push(subBo);

							} else if (subBo._flag === 'update') {
								if (!postData.subBos.update[reffieldId]) {
									postData.subBos.update[reffieldId] = [];
								}
								postData.subBos.update[reffieldId].push(subBo);

							} else if (subBo._flag === 'delete') {
								if (!postData.subBos.delete[reffieldId]) {
									postData.subBos.delete[reffieldId] = [];
								}
								postData.subBos.delete[reffieldId].push(subBo.boId);

							}
						});
					}
				}
			}
			function removeUnnecessaryData(obj, level) {
				if (!obj || (obj.indexOf && obj.indexOf('$') == 0) || level > 10) {
					return;
				}
				for (var k in obj) {
					if (obj[k] !== null) {
						if (typeof obj[k] == "object") {
							removeUnnecessaryData(obj[k], level++);
						}
					}
				}
			}

			try {
				removeUnnecessaryData(postData, 0);
				delete postData.submetas;
			} catch (error) {
				console.warn('Unable to optimize update data size.', error);
			}

            // change date values to string
			for (var i in postData.attributes) {
				if (postData.attributes[i] instanceof Date) {
					postData.attributes[i] = new Date(moment(postData.attributes[i]).format("YYYY-MM-DD"));
				}
			}
            for (var iudKey in postData.subBos) {
                if (postData.subBos.hasOwnProperty(iudKey)) {
                    var iud = postData.subBos[iudKey]
                    for (var subBoElemKey in iud) {
                        if (iud.hasOwnProperty(subBoElemKey)) {
                            var subBoElem = iud[subBoElemKey]
                            for (var j = 0; j < subBoElem.length; j++) {
                                var subBo = subBoElem[j];
                                for (var a in subBo.attributes) {
                                    if (subBo.attributes.hasOwnProperty(a)) {
                                        var attribute = subBo.attributes[a];
                                        if (attribute instanceof Date) {
                                            postData.subBos[iudKey][subBoElemKey][j].attributes[a] = new Date(moment(attribute).format("YYYY-MM-DD"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


			//TODO: Consistent links handling
			var bInsert = postData._flag === 'insert';
            var saveFunction = bInsert ? this.$http.post : this.$http.put;
            var saveLink = bInsert ? (postData.links.insert && postData.links.insert.href ? postData.links.insert.href
				: postData.links.href) : postData.links.self.href;

            saveFunction(saveLink, postData).
                success((data) => {
					data['_flag'] = flagSave;

					//Please note, that for entities without state-model it will be undefined and we know it.
					var bStateNotChangedServerSide = (postData.attributes.nuclosState && data['attributes'].nuclosState && postData.attributes.nuclosState.id === data['attributes'].nuclosState.id);
					var bStateNotChangedClientSide = true;

					// some data should not be overwritten when saving, so back it up and restore it after receiving REST response
					var backup;
					if (entity.selectedBo) {
						backup = {
							subBosBackup: entity.selectedBo.subBos,
							submetasBackup: entity.selectedBo.submetas,
							inputrequiredResultBackup: entity.selectedBo.inputrequired ? entity.selectedBo.inputrequired.result : null,
							nuclosState: entity.selectedBo.attributes.nuclosState,
							oldState: entity.selectedBo.oldState
						};
					}

					assembleBoInBoList(data, entity.bos);
					prepareBoValuesAndOptions(entity.meta.attributes, data);
					entity.setSelectedBo(data);

					if (backup) {
						bStateNotChangedClientSide = (backup.oldState && data['attributes'].nuclosState && backup.oldState.id === data['attributes'].nuclosState.id);

						for (var i in backup.subBosBackup) {
							delete backup.subBosBackup[i]._flag;
						}

						//FIXME: NUCLOS-5080 Still there is not reason for this kind of backup anymore. Anyway the subforms should
						//be reloaded after save (rules could change the data)
						//entity.selectedBo.subBos = backup.subBosBackup;
						entity.selectedBo.submetas = backup.submetasBackup;

						if (backup.inputrequiredResultBackup) {
							if (!entity.selectedBo.inputrequired) {
								entity.selectedBo.inputrequired = {};
							}
							entity.selectedBo.inputrequired.result = backup.inputrequiredResultBackup;
						}

						if (backup.nuclosState) {
							entity.selectedBo.attributes.nuclosState = backup.nuclosState;
						}
					}

					delete data['_flag'];
					delete entity.dirty;

					deferred.resolve(entity);

					var bStateChangedByServerSide = bStateNotChangedServerSide != true && bStateNotChangedServerSide != undefined;
					var bStateChangedByClientSide = bStateNotChangedClientSide != true && bStateNotChangedClientSide != undefined;

					if (bInsert || bStateChangedByServerSide || bStateChangedByClientSide) {
						// emit state:changed event to update state buttons for initial state
						this.$rootScope.$emit('state:changed');
					}

					// COMMANDS EXECUTION
					var commandList = data['commands'].list;
					var commandCount = commandList.length;
					for (var j = 0; j < commandCount; j++) {
						var command = commandList[j]
						this.commandService.handleCommand(command);
					}

					if (bInsert) {
						entity.total ++;
					}

				}).error((error, status) => {
					if (error && !error.inputrequired) {

						// reset state to original state
						if (entity.selectedBo.oldState != undefined) {
							entity.selectedBo.attributes.nuclosState = entity.selectedBo.oldState;
						}

						this.errorhandler.show(error, status);

						if (error.validationErrors) {
							// required fields missing when trying to change state - mark required fields as required
							for (var i = 0; i < error.validationErrors.length; i++) {
								var validationError = error.validationErrors[i];
								var fieldName = getShortFieldName({ boMetaId: entity.meta.boMetaId }, validationError.field);

								// input field / textarea
								var input = $('#field-' + fieldName);
								input.attr('required', 'required');
								input.addClass('ng-invalid-required');

								// dropdown
								var dropdown = $('#field-' + fieldName);
								dropdown.find('.ui-select-container').attr('required', 'required');
								dropdown.find('.ui-select-container').addClass('ng-invalid-required');
							}
						}
					}
					deferred.reject(error);
				});

            return deferred.promise;
        }

        submitBOForDelete(edited, entity) {
			var deferred = this.$q.defer();

            this.$http.delete(edited.links.self.href)
				.success(() => {
					assembleBoInBoList(edited, entity.bos);
					entity.setSelectedBo(undefined);
					if (entity.total === undefined) {
						entity.total = entity.bos.length;
					}
					entity.total--;
					deferred.resolve();
				})
				.error((jqxhr, textStatus) => {
					this.errorhandler.show(jqxhr, '' + textStatus, this.$cookieStore);
					deferred.reject();
				});

			return deferred.promise;
        }

		loadDropDownOptions(select, boId, quickSearchInput, mandatorId?): ng.IPromise<Array<any>> {

			if (!mandatorId && this.authService.getCurrentMandator()) {
				mandatorId = this.authService.getCurrentMandator().mandatorId;
			}

			var deferred = this.$q.defer();

			//TODO: Get this from HATEOAS
			var link;
			if (select.reffield) {
				link = restHost + '/data/referencelist/' + select.reffield;
			} else {
				link = restHost + '/data/vlpdata';
			}

			var header;

			if (select.vlp) {
				var vlpparams = {
					vlptype: select.vlp.type,
					vlpvalue: select.vlp.value,
					intid: boId,
					mandatorId: mandatorId
				};

				for (var i in select.vlp.params) {
					vlpparams[i] = select.vlp.params[i];
				}

				var params = this.valuelistproviderService.getParameters(select.reffield);
				if (params) {
					for (var param in params) {
						vlpparams[param] = params[param];
					}
				}

				header = sessionHeader(vlpparams);
			} else {
				header = sessionHeader({mandatorId: mandatorId});
			}

			if (quickSearchInput) {
				link += link.indexOf('?') != -1 ? '&' : '?';
				link += 'quickSearchInput=' + quickSearchInput;
			}

			this.$http.get(link, header).success((data) => {
				deferred.resolve(data);

			}).error((jqxhr, textStatus) => {
				this.errorhandler.show(jqxhr, '' + textStatus, this.$cookieStore);
				deferred.reject();
			});

			return deferred.promise;
		}


		createEmptySubBoIfNotThere(bo, reffield, force) {
			if (!bo) {
				return false;
			}

			if (!bo.subBos) {
				bo.subBos = {};
			}

			if (!reffield) {
				return false;
			}

			if (!bo.subBos[reffield]) {
				if (!force) {
					return false;
				}
				bo.subBos[reffield] = { links: {} };
			}

			if (!bo.subBos[reffield].bos) {
				bo.subBos[reffield].bos = [];
			}

			return true;
		}

		prepareAttributeValues(bo, attributes) {
			for (var i in attributes) {
				var attribute = attributes[i];

				if (bo.attributes) {
					var shortname = getShortFieldName(bo, attribute.boAttrId);
					var value = bo.attributes[shortname];

					if (value === '') {
						bo.attributes[shortname] = null;
					}
					if (!value && !attribute.nullable && attribute.type === 'Boolean') {
						bo.attributes[shortname] = false;
					}
					if (attribute.precision && typeof value === 'string') {
						value = parseNumberForRest(value, attribute.precision, attribute.scale, this.localePreferences);
						bo.attributes[shortname] = value;
					}
				}
			}
		}

		getEditingDataForSave(bo, meta, bDelete) {
			var editedBO = angular.copy(bo);

			this.prepareAttributeValues(editedBO, meta.attributes);
			editedBO._flag = bDelete ? 'delete' : (editedBO.boId ? 'update' : 'insert');

			if (editedBO.subBos) {
				for (var i in editedBO.subBos) {
					var subBOList = editedBO.subBos[i].bos;
					for (var j in subBOList) {
						var subBO = subBOList[j];
						if (subBO._flag) {
							//TODO: Had to be switched off. We avoid any conversation within the model
							//metaService.getBoMetaData(subBO.boMetaId).then(function(meta) {
							//boService.prepareAttributeValues(subBO, meta.attributes);
							//});
						}
					}
				}
			}

			return editedBO;
		}

		saveCompleteBo(bo: Bo, entity) {
			var deferred = this.$q.defer();

			var editedBO = this.getEditingDataForSave(bo, entity.meta, false);
			var masterDirtyBackup = entity.dirty;

			//TODO validate
			this.submitBOForInsertUpdate(editedBO, entity, editedBO._flag).then((data) => {
				// ok
				delete bo.executeCustomRule;

				this.$rootScope.$broadcast('refreshgrids');

				// update title and info for sideview menu
				this.updateTitleAndInfo(bo, data['meta']);
				bo.boId = data['selectedBo'].boId;
				deferred.resolve(bo);
			}, (error) => {
				// not ok - maybe inputrequired
				if (error.inputrequired) {
					if (!bo.inputrequired) {
						bo.inputrequired = {} as InputRequired;
					}
					bo.inputrequired.specification = error.inputrequired.specification;

					deferred.reject(bo);
				} else {
					this.errorhandler.show(error.message, undefined);

					entity.dirty = masterDirtyBackup;
					delete bo.executeCustomRule;
					deferred.reject(error);
				}
			});
            return deferred.promise;
		}




		saveBo(bo: Bo, entity: BoViewModel, isValid: boolean, doRefreshOtherBrowserInstances?: boolean) {
			var deferred = this.$q.defer();

            entity.inValidFormSubmitted = !isValid;

			if (!isValid) {
				deferred.reject(bo);
				return deferred.promise;
			}

			if (!bo) {
				bo = entity.selectedBo;
			} else if (!entity.selectedBo) {
				entity.selectedBo = bo;
			}

			// reset user input values before saving entry
			delete bo.inputrequired;

			var doSave = () => {
				return this.saveCompleteBo(bo, entity);
			};

			this.inputrequiredService.setSaveCallback(doSave);
			this.inputrequiredService.setRefreshCallback(() => {
				// reset subforms
				for (var i in bo.subBos) {
					var subBo = bo.subBos[i];
					delete subBo.bos;
				}
				this.$rootScope.$broadcast('refreshgrids');
			});

			doSave().then(
				(bo2) => {
					// save was successful
					deferred.resolve(bo2);
					if (doRefreshOtherBrowserInstances) {
						this.browserRefreshService.refreshOtherBrowserInstances();
					}
				},
				(bo2) => {
					// save was not successful
					deferred.reject(bo2);
					if (bo2.inputrequired) {
						this.inputrequiredService.processSaveDataRecursive(bo2);
					}
				}
			);

            return deferred.promise;
		}
		
		
		subformTabInfo(bo) {
			if (bo.links && bo.links.subforminfo) {
                this.$resource(bo.links.subforminfo.href).get(function(data) {
                    for (var subformFk in data.subforms) {
                        var subform = data.subforms[subformFk];
                        if (bo.subBos[subformFk]) {
                            bo.subBos[subformFk].bosSize = subform.count;
                        }                                
                    }
                }); 
            }
		}

	}
}
angular.module("nuclos").service("boService", nuclos.core.BoService);



getShortFieldName = function(bo, fielduid) {
	if (bo == null || bo.boMetaId == null || fielduid == null || $.type(fielduid) != "string") {
		return null;
	}

	var prefix = bo.boMetaId + '_';
	if (fielduid.lastIndexOf(prefix) === 0) {
		var fn = fielduid.substring(prefix.length);
		
		if (fn == 'nuclosSystemIdentifier') { //NUCLOS-5427
			fn = 'nuclosSystemId';
		} else if (fn == 'nuclosLogicalDeleted') {
			fn = 'nuclosDeleted';
		}
		
		return fn;
	}

	return null;
}


function getSearchCondition(metaData) {
	if (!metaData || !metaData.attributes) {
		return undefined;
	}

    var hasFieldFilter = false;
    var fieldFilter = "CompositeCondition:AND:[";

	for (var i in metaData.attributes) {
		var attribute = metaData.attributes[i];
		if (!attribute.ticked) {
			continue;
		}

		var fieldName = getShortFieldName({ boMetaId: metaData.boMetaId }, attribute.boAttrId);

		if (attribute.search) {
			if (hasFieldFilter) {
				fieldFilter += ",";
			}

			fieldFilter += "LikeCondition:LIKE:";
			fieldFilter += fieldName;
			fieldFilter += ":*" + attribute.search + "*";

			hasFieldFilter = true;
		}

		if (attribute.valuelist && attribute.valuelist.length > 0) {
			var hasInCondition = false;
			var inCondition = "";

			for (var j in attribute.valuelist) {
				var value = attribute.valuelist[j];
				if (value.ticked && value.name) {
					if (hasInCondition) {
						inCondition += ",";
					}

					//NUCLOS-4111 CollectableInCondition works with the String Name of a Reference Field
					//TODO: It would be better if it worked with the IDs
					inCondition += '"' + value.name + '"';
					hasInCondition = true;
				}
			}

			if (hasInCondition) {
				if (hasFieldFilter) {
					fieldFilter += ",";
				}

				fieldFilter += "InCondition:IN:";
				fieldFilter += fieldName;
				fieldFilter += ":[";
				fieldFilter += inCondition;
				fieldFilter += "]";

				hasFieldFilter = true;
			}
		}
	}

    if (hasFieldFilter) {
		return fieldFilter + "]";
    }

    return undefined;
}
