module nuclos.menu {

	export class MenuService {

		private menuCache;

		constructor(
			private $cookieStore,
			private $q: ng.IQService,
			private $cacheFactory,
			private $resource: ng.resource.IResourceService,
			private nuclosI18N: nuclos.util.I18NService,
			private errorhandler: nuclos.Errorhandler
		) {
			this.menuCache = this.$cacheFactory('menuCache');
		}


		public clearCache() {
			this.menuCache.removeAll();
		}



		public getMenu(menuLink: string) {
			var deferred = this.$q.defer();

			var cacheKey = this.nuclosI18N.getBrowserLocale() + '_' + menuLink;

			var menuDataFromCache = this.menuCache.get(cacheKey);
			if (menuDataFromCache) {
				deferred.resolve(menuDataFromCache);
				return deferred.promise;
			}

			this.$resource(menuLink).query(
				(data) => {
					this.menuCache.put(cacheKey, data);
					deferred.resolve(data);
				},
				(error) => {
					errorhandler.show(error.data, error.status, this.$cookieStore);
					deferred.reject();
				}
			);

			return deferred.promise;
		}


		public getMenuStructure() {
			var menuLink = restHost + '/meta/menustructure';
			var deferred = this.$q.defer();

			var cacheKey = this.nuclosI18N.getBrowserLocale() + '_' + menuLink;

			var menuDataFromCache = this.menuCache.get(cacheKey);
			if (menuDataFromCache) {
				deferred.resolve(menuDataFromCache);
				return deferred.promise;
			}

			this.$resource(menuLink).get(
				(data) => {
					this.menuCache.put(cacheKey, data);
					deferred.resolve(data);
				},
				(error) => {
					this.errorhandler.show(error.data, error.status, this.$cookieStore);
					deferred.reject();
				}
			);

			return deferred.promise;
		}

		/**
		 * @param {String} menuName menu name
		 * @param {boolean} expand show menu items below if true, hide otherwise
		 */
		public toggleMenuItem(menuName: string, expand: boolean) {
			this.$resource(restHost + '/meta/togglemenuitem/' + menuName + '/' + expand).query(
				(data) => {
				},
				(error) => {
					this.errorhandler.show(error.data, error.status, this.$cookieStore);
				}
			);
		}

	}

	angular.module("nuclos").service("menuService", MenuService);
}