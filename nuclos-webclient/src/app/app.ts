module nuclos {
	export interface INuclosApp extends angular.IModule {
		isDevMode: () => boolean;
	}

	nuclosApp = angular.module('nuclos', ['ngResource', 'ui.bootstrap', 'ui.tree', 'ngCookies', 'ngRoute', 'ngSanitize', 'hljs', 'flow', 'ngStorage', 'nya.bootstrap.select', 'ngDraggable',
		'ui.grid', 'ui.grid.resizeColumns', 'ui.grid.edit', 'ui.grid.autoResize', 'ui.grid.cellNav', 'ui.grid.selection', 'ui.grid.infiniteScroll', // subform / tableview
		'ui.grid.pinning', // matrix
		'ui.select',
		'ui.grid.saveState',
		'ui.grid.moveColumns',
		'nvd3',
		'ui.calendar',
		'gridstack-angular',
		'ui.ace'
	]) as INuclosApp;

	nuclosApp.config(['$rootScopeProvider', function ($rootScopeProvider) {
		$rootScopeProvider.digestTtl(10);
	}]);

//INIT SERVICE PROMISE HOLDER
	nuclosApp.service('initService', ['$q', function ($q) {
		var d = $q.defer();
		return {
			defer: d,
			promise: d.promise
		};
	}]);

	nuclosApp.config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/', {
			templateUrl: 'app/home/home.html',
			controller: nuclos.home.HomeCtrl
		}).when('/dashboard', {
			templateUrl: 'app/dashboard/dashboard-home.html',
			controller: nuclos.dashboard.DashboardCtrl,
			controllerAs: 'vm'
		}).when('/preferences', {
			templateUrl: 'app/preferences/preferences.html',
			controller: nuclos.preferences.PreferencesCtrl,
			controllerAs: 'vm'
		}).when('/changepassword', {
			templateUrl: 'app/user/password/changepassword.html',
			controller: nuclos.passwords.PasswordCtrl,
			controllerAs: 'vm'
		}).when('/login', {
			templateUrl: 'app/login/login.html',
			controller: nuclos.login.LoginCtrl,
			controllerAs: 'vm'
		}).when('/login/:locale', {
			templateUrl: 'app/login/login.html',
			controller: nuclos.login.LoginCtrl,
			controllerAs: 'vm'
		}).when('/menu', {
			templateUrl: 'app/dashboard/dashboard-home.html',
			controller: nuclos.dashboard.DashboardCtrl,
			controllerAs: 'vm'
		}).when('/tasks', {
			templateUrl: 'app/menu/menu.html',
			controller: nuclos.menu.MenuCtrl
		}).when('/sideview/:uid', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/sideview/:uid/search/:searchString', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/sideview/:uid/:pk', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/sideview/:uid/process/:processmetaid', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/sideview/:uid/process/:processmetaid/:pk', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/taskview/:uid/:searchFilterId', {
			templateUrl: 'app/sideview/sideview.html',
			controller: nuclos.sideview.SideViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/tableview/:uid', {
			templateUrl: 'app/tableview/tableview.html',
			controller: nuclos.tableview.TableViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/tableview/:uid/:pk', {
			templateUrl: 'app/tableview/tableview.html',
			controller: nuclos.tableview.TableViewCtrl,
			reloadOnSearch: false,
			resolve: {
				init: ['initService', function (Init) {
					return Init.promise;
				}]
			}
		}).when('/devutils', {
			templateUrl: 'app/devutils/devutils.html',
			controller: nuclos.dev.DevUtilsCtrl,
			reloadOnSearch: false
		}).when('/devutils/restexplorer', {
			templateUrl: 'app/devutils/restexplorer/restexplorer.html',
			controller: nuclos.dev.rest.RestExplorerCtrl,
			reloadOnSearch: false
		}).when('/devutils/restexplorer/:restserviceIndex', {
			templateUrl: 'app/devutils/restexplorer/restexplorer.html',
			controller: nuclos.dev.rest.RestExplorerCtrl,
			reloadOnSearch: false
		}).when('/devutils/restexplorer/history/:historyIndex', {
			templateUrl: 'app/devutils/restexplorer/restexplorer.html',
			controller: nuclos.dev.rest.RestExplorerCtrl,
			reloadOnSearch: false
		}).when('/devutils/restexplorer/preview/:restservicePath*', {
			templateUrl: 'app/devutils/restexplorer/restexplorer.html',
			controller: nuclos.dev.rest.RestExplorerCtrl,
			reloadOnSearch: false
		}).when('/logout', {
			templateUrl: 'app/login/login.html',
			controller: nuclos.login.LogoutCtrl
		}).when('/account/:action', {
			templateUrl: 'app/account/account.html',
			controller: nuclos.account.AccountCtrl
		}).when('/account/:action/:user/:parameter', {
			templateUrl: 'app/account/account.html',
			controller: nuclos.account.AccountCtrl,
			controllerAs: 'vm'
		}).when('/businesstests', {
			templateUrl: 'app/devutils/businesstest/index.html',
			controller: nuclos.businesstest.BusinessTestCtrl,
			controllerAs: 'ctrl',
			reloadOnSearch: false
		}).when('/businesstests/advanced', {
			templateUrl: 'app/devutils/businesstest/overview-advanced.html',
			controller: nuclos.businesstest.BusinessTestCtrl,
			controllerAs: 'ctrl',
			reloadOnSearch: false
		}).when('/businesstests/:testId', {
			templateUrl: 'app/devutils/businesstest/details.html',
			controller: nuclos.businesstest.BusinessTestDetailsCtrl,
			controllerAs: 'ctrl',
			reloadOnSearch: false
		}).otherwise({templateUrl: 'app/home/home.html', controller: nuclos.home.HomeCtrl});
	}]);


	nuclosApp.config(function ($logProvider) {
		$logProvider.debugEnabled(false); // turn off debug console output
	});

	nuclosApp.config(['$compileProvider', function ($compileProvider) {
		$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|data|chrome-extension):/);
	}]);

	nuclosApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
		flowFactoryProvider.defaults = {
			chunkSize: 104857600, //TODO: 100 MB Chunk Size, Reduce and allow chunks at all
			target: null,
			permanentErrors: [404, 500, 501],
			maxChunkRetries: 1,
			chunkRetryInterval: 5000,
			simultaneousUploads: 4,
			singleFile: true
		};
		flowFactoryProvider.on('catchAll', function (event) {
			console.log('catchAll', arguments);
		});
		// Can be used with different implementations of Flow.js
		// flowFactoryProvider.factory = fustyFlowFactory;
	}]);


	nuclosApp.config(function ($httpProvider) {
		$httpProvider.defaults.withCredentials = true;
	});

	nuclosApp.config(function ($cookiesProvider) {
		var n = new Date();
		$cookiesProvider.defaults.expires = new Date(n.getFullYear() + 1, n.getMonth(), n.getDate());
	});

	nuclosApp.config(function ($logProvider) {
		// TODO: Only enable debug in Dev mode
		$logProvider.debugEnabled(true);
	});

	var developmentMode;
	var globalTimeout;


	nuclosApp.run(function ($route,
							$rootScope,
							$cookies,
							$location,
							$timeout,
							$uibModal,
							$q,
							restservice,
							dataService,
							layoutService,
							messageService,
							authService: nuclos.auth.AuthService,
							initService,
							$window,
							nuclosI18N: nuclos.util.I18NService) {

		var original = $location.path;
		$location.path = function (path, reload) {
			if (reload === false && window.location.href.indexOf(path) === -1) {
				var lastRoute = $route.current;
				var un = $rootScope.$on('$locationChangeSuccess', function () {
					$route.current = lastRoute;
					un();
				});
			}
			return original.apply($location, [path]);
		};

		//AFTER LOAD SYSTEMPARAMETERS, ANNONYMOUS-LOGIN, etc...
		function afterInit() {
			console.log("AFTER INIT");

			// Strip multiple slashes from the URL
			if ($location.absUrl().reverse().match(/\/\/+(?!:)/)) {
				var newUrl = $location.absUrl().reverse().replace(/\/\/+(?!:)/g, '/').reverse();
				console.debug('Redirecting to %o', newUrl);
				$window.location.href = newUrl;
				return;
			}

			initService.defer.resolve(true);

			$rootScope.showMasterDirtyPopover = function () {
				var divToAppendPopover = $('#buttonarea-left');
				if (divToAppendPopover.length !== 0) {
					divToAppendPopover['popover']({
						trigger: 'focus',
						title: nuclosI18N.getI18n('webclient.master.dirty.popover.title'),
						content: nuclosI18N.getI18n('webclient.master.dirty.popover.text'),
						html: 'true',
						placement: 'bottom',
						delay: {
							show: 500,
							hide: 100
						}
					});
					divToAppendPopover['popover']("show");
					var popupWidth = $('.popover').width();
					var popoverAppendToBodyDiv = $('#appendToBodyDiv-popover');
					if (popoverAppendToBodyDiv.length === 0) {
						popoverAppendToBodyDiv = $('<div id="appendToBodyDiv-popover" style="position:absolute; z-index: 1;"></div>');
						$('body').append(popoverAppendToBodyDiv);
					}
					popoverAppendToBodyDiv.prepend($('.popover'));
					$('.popover').css(
						{
							top: divToAppendPopover.offset().top + divToAppendPopover.height(),
							left: divToAppendPopover.offset().left + divToAppendPopover.width() / 2 - popupWidth / 2,
							'white-space': 'nowrap'
						}
					);
					$timeout(function () {
						divToAppendPopover['popover']('hide');
						divToAppendPopover.unbind('focusin');
					}, 4000);
				} else {
					// this shouldn't happen
					alert(nuclosI18N.getI18n('webclient.master.dirty.popover.title') + "\n" +
						nuclosI18N.getI18n('webclient.master.dirty.popover.text'));
				}
			}


			/**
			 * show popover message and prevent navigation if user tries to navigate away from unsaved changes
			 */
			$rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {
				var master = dataService.getMaster();
				if (
					master && master.dirty && master.selectedBo && master.selectedBo.boId // master is dirty
					&& authService.hasSessionId() // user session is valid
				) {
					console.warn("can't change location - master is dirty");
					$rootScope.showMasterDirtyPopover();
					event.preventDefault();
				}
			});


			/**
			 * Remember the last location
			 */
			$rootScope.$on('$locationChangeSuccess', function (event, newUrl, oldUrl) {
				if (!authService.isStartOrLoginPage($location.path()) && !authService.isAnonymous()) {
					if (authService.getUsername()) {
						$cookies.put('location.' + authService.getUsername(), $location.path());
					}
				}
			});

			restservice.developmentMode().then(function (data) {
				developmentMode = data;
				if (developmentMode) {
					// load restservices for rest-call-history
					restservice.restservicelist().then(function (data) {
						// restservices = data;
						$rootScope.restservices = data;
					});
				}
			});

			$rootScope.locationHome = function (path) {
				var master = dataService.getMaster();
				if (master && master.dirty) {
					$rootScope.showMasterDirtyPopover();
					console.warn("can't change location to home - master is dirty");
					return;
				}

				if (path === null) {
					path = "/menu";
				}
				if ($rootScope && !$rootScope.$$phase) {
					$rootScope.$apply(function () {
						$location.path(path, true);
					});

				} else {
					// not ready now, try again later
					$timeout(function () {
						$rootScope.locationHome(path);
					}, 100);
				}
			};
		}


		//INIT STEPs
		var initAuth = $q.defer();
		var initSystemparameters = $q.defer();

		$q.all(initSystemparameters).then(function () {
			console.log("initSystemparameters FINISH", $rootScope.isAnonymousUserAccessEnabled);

			var startPage = authService.isStartOrLoginPage($location.url());
			var anonEnabled = authService.isAnonymousUserAccessEnabled();

			authService.removeRedirectCookie();
			if (!startPage) {
				authService.setRedirectCookie($location.url());
			}

			// Check if we already have a valid Session for the REST Service (via Autologin)
			authService.autologin().then(function (data) {
				console.log("AUTOLOGIN successful");
				//NUCLOS-4922
				if (data.initialEntity) {
					window.location.href = '/#/sideview/' + data.initialEntity;
				}

				initAuth.resolve(true);
			}, function () { // No valid session
				if (startPage) {
					if (anonEnabled) {
						console.log("loginAsAnonymous START");
						initAuth.promise.then(authService.loginAsAnonymous().then(function () {
							initAuth.resolve(true)
						}));
					}
					else {
						$location.path('login');
						initAuth.resolve(true);
					}
				}
				else {
					if (anonEnabled) {
						console.log("LOGIN AS ANONYMOUS FIRST")
						initAuth.promise.then(authService.loginAsAnonymous().then(function () {
							initAuth.resolve(true)
							console.log("LOGIN AS ANONYMOUS FIRST FINISH")
						}));
					}
					else {
						$location.path('login');
						initAuth.resolve(true);
					}
				}
			});
		});


		initSystemparameters.promise.then(restservice.systemparameters().then(function (systemparameters) {
			console.log("initSystemparameters LOAD");

			nuclosApp.isDevMode = () => {
				return systemparameters['ENVIRONMENT_DEVELOPMENT']
			};

			$rootScope.isDevMode = systemparameters['ENVIRONMENT_DEVELOPMENT'];
			$rootScope.isAnonymousUserAccessEnabled = systemparameters['ANONYMOUS_USER_ACCESS_ENABLED'];
			$rootScope.isUserRegistrationEnabled = systemparameters['USER_REGISTRATION_ENABLED'];

			if (systemparameters.WEBCLIENT_CSS !== undefined) {
				// append CSS from systemparameters
				$('head').append('<style type="text/css">' + systemparameters.WEBCLIENT_CSS + '</style>');
			}

			initSystemparameters.resolve(true);
		}));

		var initAll = $q.all(initAuth);
		initAll.then(afterInit);
	});
}