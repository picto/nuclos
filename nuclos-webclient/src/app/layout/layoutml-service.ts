module nuclos.layout {
	export interface LayoutTD {
		text?: string;
		class?: string;
		width?: number;
		height?: number;
		minheight?: number;
		field?: any;
		showCollapsiblePanelButton?: boolean;
		rowspan?: number;
		colspan?: number;
		key?: any;
		style?: string;
	}

	export class LayoutMLService {

		constructor(
			private valuelistproviderService: nuclos.detail.ValuelistproviderService
		) {
			window['getSpecialTypeFromItem'] = this.getSpecialTypeFromItem;
			window['setOrDeleteVlpParam'] = this.setOrDeleteVlpParam;
		}

		transferConstraints(cstr, layout, btabbedpane) {
			var td = {
				style: "",
				colspan: 1,
				rowspan: 1,
				width: 0,
				height: 0
			} as LayoutTD;

			if (cstr) {

				if (cstr.colspan) td.colspan = cstr.colspan;
				if (cstr.rowspan) td.rowspan = cstr.rowspan;
				if (cstr.valign) td.style = td.style + "vertical-align:" + cstr.valign + ";";
				if (cstr.align) td.style = td.style + "text-align:" + cstr.align + ";";

				if (layout) {
					td.width = 0;
					for (var ix = cstr.column; ix < cstr.column + td.colspan; ix++) {
						if (layout.columns[ix] > 0) {
							td.width += layout.columns[ix];
						}
					}

					td.height = 0;
					for (var iy = cstr.row; iy < cstr.row + td.rowspan; iy++) {
						if (layout.rows[iy] > 0) {
							td.height += layout.rows[iy];
						}
					}

				}

				if (td.width > 0) {
					td.style = td.style
						+ "width:" + td.width + "px;"
				} else if (td.colspan == 1 && layout.columns[cstr.column] == -1) {
					td.style = td.style + "width:100%;";
					// width for rows which should use remaining width is calculated and overwritten in layoutService
				} else {
					td.style = td.style + "width:1px;";
				}

				if (td.height > 0) {
					var height = td.height;
					//Firefox needs the extra height in order to render the tabbed-pane in the correct size.
					if (btabbedpane) {
						height += 29;
					}

					td.style = td.style
						+ "height:" + height + "px;"
				} else if (td.rowspan == 1 && layout.rows[cstr.row] == -1) {
					td.style = td.style + "height:100%;";
					// height for rows which should use remaining height is calculated and overwritten in layoutService
				} else {
					td.style = td.style + "height:1px;";
				}

			}

			return td;
		}

		getSizeOfComponent(comp, index, which, rowOrColumn, widthOrHeight) {
			if (!comp.cstr || comp.cstr[rowOrColumn] != index) {
				return 0;
			}

			var size = 0;
			if (comp.type === 'PANEL' && comp.layout) {
				var array = comp.layout[rowOrColumn + 's'];

				for (var i in array) {
					var subsize = array[i] > 0 ? array[i] : 0;
					for (var j in comp.components) {
						var r1 = this.getSizeOfComponent(comp.components[j], i, which, rowOrColumn, widthOrHeight);
						if (r1 > subsize) {
							subsize = r1;
						}
					}
					size += subsize;
				}

			} else if (comp.cstr && comp.cstr.size) {
				if (comp.cstr.size[which]) {
					size += comp.cstr.size[which][widthOrHeight];
				} else if (comp.cstr.size["strict"]) {
					size += comp.cstr.size["strict"][widthOrHeight];
				}
			}
			return size;
		}

		/*TODO: This function is too linear and can be too simple. For most cases it's working, that's why it can
		 *stay this way for the next time. Find a better, but difficult algorithm.
		 */
		replaceMinus23WithConcreteSize(array, comps, rowOrColumn, widthOrHeight) {
			for (var i in array) {
				var a = array[i];
				if (a == -2 || a == -3) {
					var which = a == -2 ? "preferred" : "minimum";
					var size = 0;
					var largestComp = null;
					for (var j in comps) {
						var comp = comps[j];
						var thisSize = this.getSizeOfComponent(comp, i, which, rowOrColumn, widthOrHeight);

						if (thisSize > size) {
							size = thisSize;
							largestComp = comp;
						}

					}
					array[i] = size;

					if (largestComp && largestComp.border) {
						largestComp.borderCorrection = true;
					}
					//console.log(rowOrColumn + " " + i + "("+ a + ") replaces with:" + size);
				}
			}
		}

		replaceMinusOneWithConcreteSize(array, size) {
			var numberFills = 0;
			var hasBiggerZero = false;

			for (var i in array) {
				if (array[i] == -1) {
					numberFills++;

				} else if (array[i] > 0) {
					size -= array[i];
					hasBiggerZero = true;

				}
			}

			if (!hasBiggerZero) {
				//TODO: Implement some action here
			}

			if (numberFills > 0) {
				var concreteSize = size / numberFills;
				if (concreteSize < 1) {
					return;
				}

				for (var i in array) {
					if (array[i] == -1) {
						array[i] = concreteSize;
					}
				}
			}

		}

		refreshVLP(bo, sourceuid, action, attributes) {

			//Attributes have the short keys of the Field UIDs, thus convert
			var sourcefield = getShortFieldName(bo, sourceuid);
			var targetfield = getShortFieldName(bo, action.targetcomponent);
			var value = undefined;

			if (sourcefield && bo.attributes) {
				value = bo.attributes[sourcefield];

				if (targetfield && bo.dropdownoptions) {

					var options = bo.dropdownoptions[targetfield];
					if (bo.attributes[targetfield]) {
						if (!options || options.length != 1 || options[0] != bo.attributes[targetfield]) {
							bo.dropdownoptions[targetfield] = [bo.attributes[targetfield]];
						}
					} else {
						if (options && options.length != 0) {
							bo.dropdownoptions[targetfield] = [];
						}
					}

				}

			}

			if (targetfield) {
				var targetAttribute = attributes[targetfield];
				if (targetAttribute) {
					if (!value) {
						if (targetAttribute.reference) value = 0;
						else if (targetAttribute.type === 'String') value = '';
						else if (targetAttribute.type === 'Number') value = 0;
					}
				}
			}

			this.setOrDeleteVlpParam(value, action);
		}

		setOrDeleteVlpParam(value, action) {
			if (value && value.id !== undefined) {
				value = value.id;
			}

			//TODO: Don't set params, load the target-combo now
			this.valuelistproviderService.setParameter(action, value);
		}

		clearField(bo, action) {
			var targetfield = getShortFieldName(bo, action.targetcomponent);

			if (targetfield && bo.attributes && bo.attributes[targetfield]) {
				if (bo.attributes[targetfield] && bo.attributes[targetfield].id !== undefined) {
					// reference field
					bo.attributes[targetfield] = {id: null, name: null};
				} else {
					bo.attributes[targetfield] = null;
				}

				if (!bo.dropdownoptions) {
					bo.dropdownoptions = {};
				}
				bo.dropdownoptions[targetfield] = [];
			}
		}

		translateSplitPosition(cstr) {
			if (cstr) {
				if (cstr.position === 'left' || cstr.position === 'top') {
					cstr.column = 0;
					cstr.row = 0;

				} else if (cstr.position === 'right') {
					cstr.column = 1;
					cstr.row = 0;

				} else if (cstr.position === 'bottom') {
					cstr.column = 0;
					cstr.row = 1;
				}
			}
		}

		getReplacedSize(cstr, td, which) {
			var size = td[which];
			if (size <= 0) {
				if (cstr.size && cstr.size.preferred) {
					size = cstr.size.preferred[which];
				} else if (cstr.size && cstr.size.minimum) {
					size = cstr.size.minimum[which];
				} else {
					size = 120;
				}
			}
			return size;

		}

		translateSplitPane2Panel(comp, td) {
			var width = this.getReplacedSize(comp.cstr, td, "width");
			var height = this.getReplacedSize(comp.cstr, td, "height");


			var props = comp.properties;
			var bHorizontal = props.orientation === 'horizontal';

			var factor = 0.5;
			if (props.resizeweight) {
				factor = parseFloat(props.resizeweight);

			} else {
				if (comp.cstr && comp.cstr.size) {
					var dimens = comp.cstr.size.preferred;
					if (!dimens) {
						dimens = comp.str.size.strict;
					}
					if (dimens) {
						if (bHorizontal && width > 0) {
							factor = dimens.width / width;
						} else if (height > 0) {
							factor = dimens.height / height;
						}
					}
				}
			}

			if (bHorizontal) {
				comp.layout = {columns: [width * factor, width * (1 - factor)], rows: [height]};
			} else {
				comp.layout = {columns: [width], rows: [height * factor, height * (1 - factor)]};
			}

			for (var i in comp.components) {
				this.translateSplitPosition(comp.components[i].cstr);
			}

			comp.type = 'PANEL';
			comp.title = '';

		}

		getSpecialTypeFromItem(stype, item, paramval) {
			var retval = paramval || [];

			for (var i in item) {

				if (i == stype && item[i] && item[i].type) {
					retval[item.uid] = item[i];
				}

				if (typeof item[i] == "object") {
					retval.concat(this.getSpecialTypeFromItem(stype, item[i], retval));
				}
			}

			return retval;
		}
	}
	nuclosApp.service('layoutMLService', LayoutMLService);
}