/**
 * provides layout data for a given business object
 * layout data can be cached until it is changed via the layout editor
 */
module nuclos.layout {
	import Bo = nuclos.model.interfaces.Bo;
	import IPerspectiveModel = nuclos.perspective.IPerspectiveModel;
	import Flag = nuclos.model.interfaces.Flag;

	export class LayoutService {
		private layoutCache: any;
		private perspectiveModel: IPerspectiveModel;

		constructor(private $http,
					private $rootScope,
					private $timeout: ng.ITimeoutService,
					private $cookieStore,
					private $q,
					private $cacheFactory,
					private $resource,
					private boService,
					private localePreferences: nuclos.clientprefs.LocalePreferenceService,
					private $log: ng.ILogService,
					private layoutMLService: LayoutMLService) {
			this.layoutCache = $cacheFactory('layoutCache');
		}

		public setPerspectiveModel(model: IPerspectiveModel) {
			this.perspectiveModel = model;
		}

		clearCache() {
			this.layoutCache.removeAll();
		}

		getLayoutImpl(layoutLink) {
			var deferred = this.$q.defer();

			if (!layoutLink) {
				this.$log.warn('No layout link provided.');
				deferred.reject();
				return deferred.promise;
			}

			var layoutJSON = this.layoutCache.get(layoutLink);
			if (layoutJSON) {
				deferred.resolve(layoutJSON);
				return deferred.promise;
			}

			this.$resource(layoutLink).get(data => {
				this.layoutCache.put(layoutLink, data);
				deferred.resolve(data);
			}, error => {
				errorhandler.show(error.data, error.status, this.$cookieStore);
				deferred.reject();
			});

			return deferred.promise;
		}

		getLayoutURL(entity: BoViewModel, bo, forcedLayoutURL: string = null) {
			if (forcedLayoutURL) {
				this.$log.debug('Forced layout: %o', forcedLayoutURL);
				return forcedLayoutURL;
			}

			var layoutLink;
			if (bo.links && bo.links.layout) {
				layoutLink = bo.links.layout.href;
			} else if (entity.meta.links.defaultLayout) { // use default layout of entity if bo has no layout link
				layoutLink = entity.meta.links.defaultLayout.href;
			} else {
				alert('No layout defined for "' + entity.boMetaId + '".');
				return;
			}

			layoutLink = this.getPerspectiveOverride(bo, entity, layoutLink);

			return layoutLink;
		}

		getLayout(entity: BoViewModel, bo, forcedLayoutURL: string = null) {
			var layoutLink = this.getLayoutURL(entity, bo, forcedLayoutURL);
			return this.getLayoutImpl(layoutLink);
		}

		tableviewlayout(uid) {
			//TODO: Use HATEOUS
			var layoutLink = restHost + '/meta/tableviewlayout/' + uid;
			return this.getLayoutImpl(layoutLink);
		}

		tableviewlayoutsubform(parentuid, dependenceuid) {
			//TODO: Use HATEOUS
			var layoutLink = restHost + '/meta/tableviewlayout/' + parentuid + '/' + dependenceuid;
			return this.getLayoutImpl(layoutLink);
		};

		defaultSearchLayout(boViewModel) {
			return this.getLayoutImpl(boViewModel.meta.links.defaultSearchLayout.href);
		}

		//TODO: The argument list and so the argument list of the next calls (clearField, refreshVLP..)
		//should be ordered up and grouped.
		executeLayoutMLRules(mlrules, bo, attributes, entity) {
			if (!mlrules || mlrules.length === 0) {
				return true;
			}

			var allExecuted = true;
			for (var i in mlrules) {
				var mlrule = mlrules[i];

				if (mlrule.type === 'value-changed' || mlrule.type === 'lookup') {
					for (var j in mlrule.actions) {
						var action = mlrule.actions[j];

						if (action.type === 'clear') {
							if (entity) this.layoutMLService.clearField(bo, action);
						} else if (action.type === 'refresh-valuelist') {
							this.layoutMLService.refreshVLP(bo, mlrule.sourcecomponent, action, attributes);
						} else if (action.type === 'transfer-lookedup-value') {
							if (entity) this.transferLUV(bo, mlrule.sourcecomponent, action, attributes, entity);
						} else if (action.type === 'check-layout-change') {
							if (entity) this.checkForLayoutChange(bo, mlrule.sourcecomponent, entity);
						} else {
							allExecuted = false;
						}
					}
					continue;
				}
				allExecuted = false;
			}
			return allExecuted;
		}

		transferLUV(bo, sourceuid, action, attributes, entity) {
			if (bo && bo.attributes) {
				var sourcefield = getShortFieldName(bo, sourceuid);
				var targetfield = getShortFieldName(bo, action.targetcomponent);

				if (!bo.dropdownoptions) {
					bo.dropdownoptions = {};
				}
				var id = bo.attributes[sourcefield] ? bo.attributes[sourcefield].id : null;

				if (id) {
					//TODO: Get link from HATEOUS
					var link = restHost + '/data/fieldget/' + action.parameter + '/' + id + '/' + sourceuid + '/' + bo.boId;

					this.$http.get(link).success((data) => {
						var paramfield = getShortFieldName(data, action.parameter);
						var value = data.attributes[paramfield];

						// if attribute was cleared previously setting it to undefined would not save the cleared value
						if (value === undefined) {
							value = null;
						}

						bo.attributes[targetfield] = value;
						bo.dropdownoptions[targetfield] = [value];
						this.triggerNextMLRules(bo, action.targetcomponent, attributes, entity);

					}).error((jqxhr, textStatus) => {
						errorhandler.show(jqxhr, textStatus, this.$cookieStore);
					});
				} else {
					//Source Field was cleared, so clear the target. too.
					bo.attributes[targetfield] = null;
					bo.dropdownoptions[targetfield] = [];
					this.triggerNextMLRules(bo, action.targetcomponent, attributes, entity);
				}
			}
		}

		triggerNextMLRules(bo, targetuid, attributes, entity) {
			if (entity && entity.table && entity.table.trs) {
				for (var i in entity.table.trs) {
					for (var j in entity.table.trs[i].tds) {

						var td = entity.table.trs[i].tds[j];
						if (td.field && td.field.uid === targetuid) {
							this.executeLayoutMLRules(td.field.mlrules, bo, attributes, entity);
						}

					}
				}

			}
		}

		/**
		 * Returns the complete URL for the given layout FQN.
		 *
		 * @param layoutFQN
		 * @returns {string}
		 */
		public getLayoutURLForFQN(layoutFQN: string) {
			// TODO: Use HATEOS or some kind of layout registry to lookup the layout URL.
			return restHost + "/meta/layout/" + layoutFQN;
		}

		/**
		 * Changes the currently displayed layout of the given record/entity.
		 * A currently selected perspective may prevent the layout from being switched.
		 *
		 * If force is true, the layout must not be overriden by the perspective.
		 *
		 * @param bo
		 * @param entity
		 * @param layoutURL
		 * @param force
		 * @param isNew
		 */
		public switchLayout(bo: Bo, entity: BoViewModel, layoutURL: string, force: boolean = false) {
			if (!force) {
				layoutURL = this.getPerspectiveOverride(bo, entity, layoutURL);
			}

			bo.links.layout.href = layoutURL;
			let forcedLayoutURL = force ? layoutURL : null;

			// initially only the subforms of the initially selected layout is loaded
			// when switching layout there might be subforms which are not loaded already
			this.getLayout(entity, bo, forcedLayoutURL).then(layoutJSON => {
				if (!bo.subBos) {
					bo.subBos = {};
				}
				if (bo._flag !== Flag.insert) {
					let subforms = (JSON as any).search(layoutJSON, '//*[type="SUBFORM"]');
					for (let subform of subforms) {
						let reffield = subform.reffield;

						let restriction = bo.subBos[reffield].restriction;
						bo.subBos[reffield] = {
							links: {
								"bos": {
									"href": restHost + "/bos/" + bo.boMetaId + "/" + bo.boId + "/subBos/" + reffield,
									"methods": ["GET"]
								}
							}
						};
						if (restriction) {
							bo.subBos[reffield].restriction = restriction;
						}
					}
				}
			});
			this.$timeout(() => {
				this.$rootScope.$broadcast('refreshgrids'); // refresh active subforms
			});

			this.createTableForThisBO(entity, bo, forcedLayoutURL).then(layoutLoadedActions => {
				this.$rootScope.$broadcast('rowselected', {
					layoutLoadedActions: layoutLoadedActions,
					bo: bo
				});
			});
		}

		/**
		 * Checks if a currently selected perspective overrides the given layout.
		 *
		 * @param bo
		 * @param entity
		 * @param layoutURL
		 * @returns {string}
		 */
		private getPerspectiveOverride(bo: Bo, entity: BoViewModel, layoutURL: string): string {
			let perspectiveLayout = this.getPerspectiveLayoutURL(bo, entity);

			this.$log.debug('getPerspectiveOverride: layout = %o, perspective layout = %o', layoutURL, perspectiveLayout);

			// Perspective layout overrides BO layout
			if (perspectiveLayout != null && perspectiveLayout != layoutURL) {
				layoutURL = perspectiveLayout;
			}

			return layoutURL;
		}

		/**
		 * If a perspective is selected which has a layout defined,
		 * the corresponding URL for this layout is returned.
		 *
		 * @param bo
		 * @param entity
		 * @returns {null}
		 */
		public getPerspectiveLayoutURL(bo: Bo, entity: BoViewModel): string {
			let noPerspectiveSelected = !this.perspectiveModel || !this.perspectiveModel.selectedPerspective;
			if (noPerspectiveSelected) {
				return null;
			}

			let skipPerspectiveForNew = this.perspectiveModel.selectedPerspective.layoutForNew === false && bo._flag === Flag.insert;
			if (skipPerspectiveForNew) {
				this.$log.debug('Skipping perspective layout for new record');
				return null;
			}

			let layout = this.perspectiveModel.getLayoutInfo(this.perspectiveModel.selectedPerspective.layoutId);

			if (!layout) {
				return null;
			}

			return this.getLayoutURLForFQN(layout.fqn);
		}

		checkForLayoutChange(bo, sourceuid, entity, isNew : boolean = false) {
			var sourcefield = getShortFieldName(bo, sourceuid);
			var process = bo.attributes[sourcefield];

			//TODO: Get From HATEOAS
			var link = restHost + '/meta/processlayout/' + process.id + '/' + bo.boMetaId;
			if (bo.boId) {
				link += '/' + bo.boId;
			}

			this.$resource(link).get(data => {
				if (data.links && data.links.layout && data.links.layout.href) {
					if (data.links.layout.href !== bo.links.layout.href) {
						this.switchLayout(bo, entity, data.links.layout.href, false);
					}
				}
				//TODO: NUCLOS-3713 Implement
			}, error => {
				errorhandler.show(error.data, error.status, this.$cookieStore);
			});
		};

		checkLayoutChangeAfterCancel(entity, bo, loadedBo) {
			if (bo.links.layout.href !== loadedBo.links.layout.href) {
				this.createTableForThisBO(entity, loadedBo).then(layoutLoadedActions => {
					this.$rootScope.$broadcast('rowselected', {
						layoutLoadedActions: layoutLoadedActions,
						loadedBo: loadedBo
					});
				});
			}
		};

		createTableForThisBO(entity, bo, forcedLayoutURL: string = null) {
			var deferred = this.$q.defer();

			this.getLayout(entity, bo, forcedLayoutURL).then(layoutJSON => {
				var maxWidth = $('#buttonarea').width();

				//TODO: Avoid JSON.search
				//TODO: Counting "TABBEDPANE" is wrong anyway. It should be the max number "TAB" inside any TABBEDPANE
				var numberOfTabs = (JSON as any).search(layoutJSON, '//*[type="TAB"]').length;
				var numberOfTabRows = Math.ceil(numberOfTabs * 160 / maxWidth);
				var tabHeight = numberOfTabRows * 29; // substract height of tab if used in layout

				var maxHeight;
				if ($('.modal').length > 0) { // detailblock inside modal
					maxHeight = $('.modal #sideview-details').height() - $('.modal #buttonarea').height() - $('.modal #subformButtons').height();
				} else { // no modal open
					maxHeight = $('#detailblock-content').height() - $('#buttonarea').outerHeight() - tabHeight - 46;
				}

				var completeTableDimension = {width: maxWidth, height: maxHeight};

				//Nuclos-4113: Necessary, because layoutJSON is altered in translateLayout2TableRows
				var layoutJSONCopy = angular.copy(layoutJSON);
				var trs = this.translateLayout2TableRows(layoutJSONCopy, entity.meta, bo, completeTableDimension, 0);

				// RESTRICTIONs OVER ALL USED subBOs
				var restrictions_subbo = "";
				for (var subBo in bo.subBos) {
					restrictions_subbo = restrictions_subbo + "," + subBo + "=" + bo.subBos[subBo].restriction;
				}

				if (!entity.table) {
					entity.table = {trs: trs};
				} else {
					entity.table.trs = trs;
				}

				entity.table._currentlayout = (bo.links && bo.links.layout) ? bo.links.layout.href : entity.meta.links.defaultLayout.href;
				entity.table._restrictions = bo.attrRestrictions;
				entity.table._restrictions_subbo = restrictions_subbo;
				entity.table._canWrite = bo.canWrite;

				var allTabs = this.getTabs(entity.table);

				var tabpanesToSelect = this.getTabbedpanes(entity.table);

				var subformsToLoad = this.getSubforms(entity.table, null);

				for (var i in allTabs) {
					var tab = allTabs[i];
					var subformsInTab = this.getSubforms(tab, null);

					for (var j in subformsInTab) {
						var subformInTab = subformsInTab[j];

						for (var k in subformsToLoad) {
							if (subformsToLoad[k].boMetaId == subformInTab.boMetaId) {
								subformsToLoad.splice(k, 1);
							}
						}
					}
				}

				deferred.resolve({subformsToLoad: subformsToLoad, tabpanesToSelect: tabpanesToSelect});
			});
			return deferred.promise;
		}

		translateLayout2TableRows(mainComp, entitymeta, bo, tableDimension, depth) {
			var trs = [];
			var layout = mainComp.layout;
			//No Layout, no components
			if (!layout) {
				return trs;
			}

			//TODO: Difficulty to get the right dimension: The extra space of a (titled) border must be considered for the
			//calculation of the used space. It must be added in the following methods, but finally subtracted from the
			//replaceMinusOneWithConcreteSize function, a level lower.

			this.layoutMLService.replaceMinus23WithConcreteSize(layout.rows, mainComp.components, "row", "height");
			this.layoutMLService.replaceMinus23WithConcreteSize(layout.columns, mainComp.components, "column", "width");

			//TODO: Check if the -1 should really be replaced here or if it is rather better to replace all -1 by 100% (in transferConstraints)
			var tableWidth = tableDimension ? tableDimension.width : null;
			if (tableWidth) {
				this.layoutMLService.replaceMinusOneWithConcreteSize(layout.columns, tableWidth);
			}

			var layoutRowsOrig = angular.copy(layout.rows);

			var tableHeight = tableDimension ? tableDimension.height : null;
			if (tableHeight) {
				this.layoutMLService.replaceMinusOneWithConcreteSize(layout.rows, tableHeight);
			}

			var cells = {};
			var restrictions = bo.attrRestrictions;

			for (var i in mainComp.components) {
				var comp = mainComp.components[i];

				if (restrictions) {
					var srestrict = restrictions[comp.fieldname];

					//SHOP-48: Disable buttons || NUCLOS-5053 remove labels if not readable
					if (!srestrict && (comp.actioncommand || comp.type === 'LABEL')) {
						srestrict = restrictions[comp.name];
					}

					if (srestrict) {
						if (srestrict === 'hidden') {
							continue;
						}

						if (srestrict.indexOf('readonly') > -1) {
							comp.readonly = true;
						}

						if (srestrict.indexOf('disabled') > -1) {
							comp.disabled = true;
						}
					}
				}

				if (!comp.cstr) {
					continue;
				}

				var td = this.layoutMLService.transferConstraints(comp.cstr, layout, comp.type === 'TABBEDPANE');

				if (comp.type === 'SPLITPANE') {
					//TODO: This is a first draft until final implementation: Translate the SplitPane to a Panel, that resembles the
					//SplitPane, but without the split-functionality
					this.layoutMLService.translateSplitPane2Panel(comp, td);
				}

				if (comp.type === 'PANEL') {

					var nextTableDimension = {width: td.width, height: td.height};

					td.text = comp.title;
					if (td.text && td.text.indexOf('Tabbedpane_') == 0) {
						delete td.text;
					}
					td.class = comp.border ? "border " + comp.border : "fieldsetpanel";
					td.minheight = td.height + (comp.borderCorrection ? 23 : 0);
					td.field = {input: "panel"};
					td.field.table = {trs: this.translateLayout2TableRows(comp, entitymeta, bo, nextTableDimension, depth + 1)};

					if (depth == 0) {
						// a panel should be callapsible only if no other components are placed in the same row
						// otherwise it is not guaranteed that whole space is collapsed
						var numberOfComponentsInCurrentRow = mainComp.components.filter(
							item => {
								return item.cstr && item.cstr.row == comp.cstr.row;
							}
						).length;
						if (numberOfComponentsInCurrentRow == 1) {
							td.showCollapsiblePanelButton = true;
						}
					}

				} else if (comp.type === 'TABBEDPANE') {

					var nextTableDimension = {width: td.width, height: td.height};

					td.text = comp.title;
					td.field = {input: "tabbedpane", tabs: []};

					for (var j in comp.components) {
						var tabComp = comp.components[j];

						var tabTitle = tabComp.title;
						var tabTable = {trs: this.translateLayout2TableRows(tabComp, entitymeta, bo, nextTableDimension, depth + 1)};

						if (tabTable.trs.length > 0) {
							var reffield = (JSON as any).search(tabTable, '//subform/reffield')[0];
							var tab = {title: tabTitle, table: tabTable, reffield: reffield};
							td.field.tabs.push(tab);
						}
					}

				} else if (comp.type === 'SUBFORM') {

					if (!this.boService.createEmptySubBoIfNotThere(bo, comp.reffield, bo._flag == 'insert')) {
						continue;
					}

					if (bo.subBos[comp.reffield].restriction === 'readonly') {
						comp.readonly = true;
					}

					var subform = {
						boMetaId: comp.uid,
						name: comp.name,
						columns: comp.columns,
						ignoresublayout: comp.ignoresublayout,
						readonly: comp.readonly,
						newbutton: comp.newenabled !== 'false', reffield: comp.reffield,
						clonebutton: comp.cloneenabled !== 'false',
						deletebutton: comp.deleteenabled !== 'false',
						editenabled: comp.editenabled !== 'false',
						width: td.width,
						height: td.height,
						depth: depth,
						initialreadonly: null
					};
					td.field = {input: "subform", subform: subform};
					if (comp.initialreadonly !== undefined && comp.initialreadonly !== 'false') {
						subform.initialreadonly = true;
					}
				} else if (comp.type === 'MATRIX') {
					if (bo.subBos && bo.subBos[comp.reffield]) {
						td.field = {input: "matrix", matrix: comp};
					} else {
						continue;
					}
				} else if (comp.type === 'LABEL') {
					td.text = comp.text;
					td.field = {input: "label"};
				} else {
					if (comp.actioncommand) { //User Buttons

						td.field = comp;
						//TODO: Server should deliver the complete link
						if (comp.icon) {
							td.field.icon = restHost + '/resources/images/' + comp.icon;
						}
						td.field.input = "button";

					} else {

						if (comp.fieldname && entitymeta.attributes[comp.fieldname]) {

							var attribute = entitymeta.attributes[comp.fieldname];
							td.field = this.createFieldForHtmlTable(entitymeta, attribute, comp, bo);
							td.text = comp.text;
							
						} else {

							var text = comp.text ? comp.text : comp.name;
							var label = {input: comp.type, text: text};

							td.text = text;
							td.field = label;

						}
					}
					if (comp.tabindex !== undefined) {
						td.field.tabindex = comp.tabindex;
					}

				}

				if (comp.invisible) { //NUCLOS-5425 Invisible components triggering rules already did their job in createFieldForHtmlTable().
					continue;
				}

				td.field.width = td.width;
				td.field.height = td.height;

				cells[comp.cstr.column + 'x' + comp.cstr.row] = td;

				if (comp.background) {
					td.field.style = 'background: ' + comp.background;
				}
			}

			if (Object.keys(cells).length === 0) {
				return trs;
			}

			var toskip = {};

			var collapsibleRowSpan = 0;

			for (var y in layout.rows) {

				var ny = parseInt(y);
				//Skip first row if size == 0 anyway
				if (ny == 0 && layout.rows[0] == 0) {
					continue;
				}

				var tr = {
					tds: [],
					depth: depth,
					notempty: null,
					flex: null,
					collapsible: null,
					class: null
				};
				trs.push(tr);

				for (var x in layout.columns) {

					var xy = x + 'x' + y;
					if (toskip[xy]) {
						continue;
					}

					var nx = parseInt(x);

					//Skip first column if size == 0 anyway
					if (nx == 0 && layout.columns[0] == 0) {
						continue;
					}

					var td = cells[xy] as LayoutTD;

					if (!td) {
						//An empty cell.
						var colspan = 1;

						//If this is not the first row, try to group it with other empty cells
						if (trs.length > 1) {
							for (var ix = nx + 1; ; ix++) {
								if (!layout.columns[ix]) break;

								var ixy = ix + 'x' + y;
								if (cells[ixy]) break;
								colspan++;
							}
						}

						td = this.layoutMLService.transferConstraints({row: ny, column: nx, colspan: colspan}, layout, false);

					} else {
						tr.notempty = true;
					}

					for (var iy = ny; iy < ny + td.rowspan; iy++) {
						for (var ix = nx; ix < nx + td.colspan; ix++) {
							toskip[ix + 'x' + iy] = true;
						}
					}
					if (layoutRowsOrig[y] == -1) {
						tr.flex = true;
					}
					if (td.showCollapsiblePanelButton) {
						tr.collapsible = true;
						if (collapsibleRowSpan == 0) {
							collapsibleRowSpan = td.rowspan;
						}
					}

					td.key = depth + '_' + y + '_' + x;
					tr.tds.push(td);

					if (collapsibleRowSpan > 0) {
						tr.collapsible = true;
						collapsibleRowSpan--;
					}
				}

				tr.class = 'tr-' + tr.depth + (tr.flex == true ? ' tr-flex' : '') + (tr.collapsible ? ' tr-collapsible' : '');

			}

			return trs;
		}

		createFieldForHtmlTable(entitymeta, attribute, comp, bo) {
			var field = {
				uid: attribute.boAttrId,
				name: comp.fieldname,
				readonly: attribute.readonly || comp.readonly,
				memo: comp.memo,
				mlrules: comp.mlrules,
				properties: comp.properties,
				type: attribute.type,
				required: null,
				input: null,
				itype: null,
				maxlength: null,
				scale: null,
				precision: null,
				select: null,
				nojump: null
			};

			//TODO: The execution of layoutMLRules for the start of the mask has to be done outside this.
			this.executeLayoutMLRules(comp.mlrules, bo, entitymeta.attributes, undefined);

			field.required = !attribute.nullable;

			field.input = 'input';
			field.itype = 'text';

			if (attribute.type === 'Boolean') {
				field.input = 'checkbox';
			}

			if (attribute.type === 'Date' || attribute.type === 'Timestamp') {
				field.input = 'date';

				var dateFormat = this.localePreferences.getDateFormat();
				if (dateFormat) {
					field.maxlength = dateFormat.length;
					;
				}
			}

			if (attribute.type === 'Number' || attribute.type === 'Decimal') {
				field.scale = attribute.scale;
				field.precision = attribute.precision;
			}

			if (attribute.type === 'String') {
				field.maxlength = attribute.scale;
				if (field.memo) {
					field.input = 'memo';
				}
			}

			if (attribute.type === 'Image') {
				field.input = 'image';
				if (attribute.reference) {
					field.input = 'stateicon';
				}
			}

			else if (attribute.type === 'Document') {
				field.input = 'document';
			}

			else if (attribute.reference || comp.vlp) {
				field.select = {'reffield': attribute.boAttrId, 'vlp': comp.vlp};
				field.input = 'select';
				field.nojump = attribute.nojump;
			}

			else if (attribute.defcomptype == 'Hyperlink') {
				field.input = 'hyperlink';
			}

			else if (attribute.defcomptype == 'Email') {
				field.input = 'email';
			}

			else if (attribute.defcomptype == 'Password') {
				field.itype = 'password';
			}

			return field;
		}

		getTabs(table) {
			var allTabs = [];
			angular.forEach(table.trs, tr => {
				angular.forEach(tr.tds, td => {
					if (td.field) {
						angular.forEach(td.field.tabs, tab => {
							allTabs.push(tab);
						});
					}
				});
			});
			return allTabs;
		}


		getTabbedpanes(table) {
			var tabbedpanes = [];
			angular.forEach(table.trs, tr => {
				angular.forEach(tr.tds, td => {
					if (td.field && td.field.input == 'tabbedpane') {
						tabbedpanes.push(td.field);
					}
				});
			});
			return tabbedpanes;
		}

		getSubforms(table, subforms) {
			var subforms = subforms || [];
			angular.forEach(table.trs, tr => {
				angular.forEach(tr.tds, td => {
					if (td.field) {
						if (td.field.subform) {
							subforms.push(td.field.subform);
						}

						if (td.field.tabs) {
							angular.forEach(td.field.tabs, tab => {
								if (tab.table) {
									var s = this.getSubforms(tab.table, subforms);
									subforms.concat(s);
								}
							});
						}
					}
				});
			});
			return subforms;
		}

		findField(item, fieldname) {
			if (item.fieldname == fieldname) {
				return item;
			}
			if (item.components) {
				for (var i in item.components) {
					var component = item.components[i];
					var foundObject = this.findField(component, fieldname);
					if (foundObject != undefined) {
						return foundObject;
					}
				}
			}
		}
	}
}

nuclosApp.service('layoutService', nuclos.layout.LayoutService);
