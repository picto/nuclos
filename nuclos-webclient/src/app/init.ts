// module nuclos {

$.ajaxSetup({"async": false});

setTitle = (cStore) => {
	document.title = 'Nuclos ' + nuclosVersion;
	if (cStore) {
		if (cStore.get('tabtitle')) {
			document.title = cStore.get('tabtitle');
			cStore.remove('tabtitle');
		} else if (cStore.get('authentication')) {
			document.title += ' - ' + cStore.get('authentication').username;
		}
	}
}

var getServerJsonUrl = function (fileName) {
	var sUrl = document.URL;
	if (sUrl.indexOf('#/') >= 0) {
		sUrl = sUrl.substring(0, sUrl.indexOf('#/'));
	}
	var jsonServerURL = sUrl.replace('index\.html', fileName);
	if (jsonServerURL.indexOf(fileName) < 0) {
		sUrl = sUrl.replace('#', '');
		jsonServerURL = sUrl + fileName;
	}
	return jsonServerURL;
}

// try to load 'server-dev.json' (which is not in git) otherwise try to get 'app/server.json'
var nuclosServer = null;
$.getJSON(getServerJsonUrl('app/server.json'), function (data) {
	nuclosServer = data.server;
}).fail(function (error) {
	//ignore
	console.warn("Unable to load app/server.json", error);
});

if (!nuclosServer) nuclosServer = 'http://<host>:8080/nuclos';
nuclosServer = nuclosServer.replace('<host>', window.location.hostname);
nuclosServer = nuclosServer.replace('<port>', window.location.port);

var chunkSize = 40;
defMaxMenuEntries = 4;
defaultMenu = '#/menu';
var restHost = nuclosServer + '/rest';
var nuclosVersion = '';

$.get(restHost + '/version', function (data) {
	nuclosVersion = data;
	setTitle(null);
});

function setLoadingIndicator(bWait) {
	if (window.navigator.userAgent.indexOf('Firefox') != -1) {
		var loadingImage = $('#loading-image').get(0);
		if (loadingImage) {
			loadingImage['src'] = loadingImage['src'].replace('loading.gif', 'loading_35.gif');
		}
	}

	if (bWait) {
		$('#loading-image').show();
	} else {
		$('#loading-image').hide();
	}
}

/**
 * Sets custom HTTP headers.
 *
 * TODO: Why do we need this?
 *
 * @param _cStore
 * @param params
 * @returns {{}}
 */
sessionHeader = params => {
	let header = {headers: {}};
	if (params) {
		header['params'] = params;
	}
	return header;
}

// TODO replace all handleError(..) calls with errorhandler.show(..)
function handleError(data, code, cStore) {
	var errorhandler = angular.element(document.body).injector().get('errorhandler');
	errorhandler.show(data, code);
}

// detect if device is touch device or not
document.documentElement.className += (("ontouchstart" in document.documentElement) ? ' touch' : ' no-touch');

window.onresize = function () {
	var element = $('#sideview-navigation');

	var offset = $('#sideview-details').offset();
	if (offset && offset.top >= 320) {
		//Logical Resolution of Iphone 5
		element.css('height', '568px');
		return;
	}

	element.css('height', '100%');
};
// }
