module nuclos.preferences {

	import IPromise = angular.IPromise;

	export const PREFERENCE_NAME_MAX_LENGTH: number = 255;

	export type PreferenceType = 'table' | 'searchtemplate' | 'chart' | 'perspective';

	export interface IPreferenceFilter {
		type?: Array<PreferenceType>,
		boMetaId?: string
	}

	export class PreferenceService {

		private preferencesCache: ng.ICacheObject;

		constructor(private $resource: ng.resource.IResourceService,
					private $q: ng.IQService,
					private util: nuclos.util.UtilService,
					private $cacheFactory,
					private $log: ng.ILogService) {
			this.preferencesCache = $cacheFactory('preferencesCache');
		}

		/**
		 * Clears the cache completely.
		 *
		 * TODO: Only update specific when possible, instead of clearing the complete cache every time.
		 */
		public clearCache() {
			this.preferencesCache.removeAll();
			//TODO: Invalidate cache when preferences changed via other client or server.
		}

		public getPreferences(filter: IPreferenceFilter): IPromise<Array<IPreference<IPreferenceContent>>> {
			var deferred = this.$q.defer();

			this.$log.debug('filter: %o', filter);

			var filterString = (filter.type ? filter.type.join(',') : undefined);
			var key = filter.boMetaId + ':' + filterString;
			this.$log.debug('pref key: %o', key);

			var pref = this.preferencesCache.get(key);
			if (pref) {
				deferred.resolve(pref);
			} else {
				pref = this.$resource(restHost + '/preferences').query(
					{
						boMetaId: filter.boMetaId,
						type: filterString
					}
				).$promise.then(
					(prefs) => {
						this.preferencesCache.put(key, prefs);
						deferred.resolve(prefs);
					},
					(error) => {
						deferred.reject(error);
					}
				)
			}

			return deferred.promise;
		}

		public getPreferenceShareGroups(preferenceItem: IPreference<any>) {
			return this.$resource(restHost + '/preferences/' + preferenceItem.prefId + '/share').get();
		}

		public shareOrUnsharePreferenceItem(preferenceItem: IPreference<any>, userRole: IUserRole) {
			this.clearCache();
			if (userRole.shared) {
				return this.$resource(restHost + '/preferences/' + preferenceItem.prefId + '/share/' + userRole.userRoleId).save();
			} else {
				return this.$resource(restHost + '/preferences/' + preferenceItem.prefId + '/share/' + userRole.userRoleId).delete();
			}


		}

		public updatePreferenceShare = (preferenceItem: IPreference<any>) => {
			this.clearCache();
			return this.$resource(restHost + '/preferences/' + preferenceItem.prefId + '/share', null, {'update': {method: 'PUT'}})['update'](preferenceItem);
		}

		public deleteCustomizedPreferenceItem(preferenceItem: IPreference<any>) {
			if (preferenceItem.customized) {
				this.clearCache();
				return this.$resource(restHost + '/preferences/' + preferenceItem.prefId).delete();
			}
		}

		private setDefaultName(preferenceItem: IPreference<any>) {
			if (preferenceItem.name === undefined) {
				preferenceItem.name = preferenceItem.boMetaId.substring(preferenceItem.boMetaId.lastIndexOf('_') + 1);
			}
		}

		/**
		 * Saves the given preference item.
		 *
		 * If the item is new (without prefId), it is saved as new preference.
		 * If it is updated, it might be saved as "customization" of a shared preference.
		 *
		 * @param preferenceItem
		 * @returns {any}
		 */
		public savePreferenceItem(preferenceItem: IPreference<any>): IPromise<IPreference<any>> {
			var deferred = this.$q.defer();

			this.setDefaultName(preferenceItem);
			if (preferenceItem.name.length > PREFERENCE_NAME_MAX_LENGTH) {
				preferenceItem.name = preferenceItem.name.substr(0, PREFERENCE_NAME_MAX_LENGTH - 2) + '..';
			}

			if (preferenceItem.prefId) {
				// update existing preference 
				this.$resource<IPreference<any>>(
					restHost + '/preferences/' + preferenceItem.prefId,
					null,
					{'save': {method: 'PUT'}}
				).save(this.util.jsonStringifyWithFunctions(preferenceItem),
					data => {
						deferred.resolve(preferenceItem);
					},
					error => {
						deferred.reject(error);
					}
				);
			} else {
				// create new preference 
				this.$resource<IPreference<any>>(restHost + '/preferences').save(
					this.util.jsonStringifyWithFunctions(preferenceItem),
					data => {
						deferred.resolve(data);
					},
					error => {
						deferred.reject(error);
					}
				);
			}

			this.clearCache();
			return deferred.promise;
		}

		public deletePreferenceItem(preferenceItem) {
			this.clearCache();
			return this.$resource(restHost + '/preferences/' + preferenceItem.prefId).delete();
		}

		public deleteMyPreferences() {
			this.clearCache();
			return this.$resource(restHost + '/preferences').delete();
		}
	}
}

angular.module("nuclos").service("preferences", nuclos.preferences.PreferenceService);
