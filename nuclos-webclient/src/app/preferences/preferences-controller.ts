module nuclos.preferences {

	export interface IPreferencesCtrl {
	}

	export interface IUserRole {
		name: string;
		userRoleId: string;
		shared: boolean;
	}

	export class PreferencesCtrl {

		vm: IPreferencesCtrl = this;
		preferencesItems: Array<IPreference<any>> = [];
		selectedPreferenceItem: IPreference<any>;
		userRoles = [];

		constructor(private preferences: nuclos.preferences.PreferenceService,
					private $window: ng.IWindowService,
					private $cookieStore: ng.cookies.ICookiesService,
					private dialog: nuclos.dialog.DialogService,
					private errorhandler: Errorhandler,
					private nuclosI18N: nuclos.util.I18NService) {
			this.updatePreferenceItemView();
		}


		handleError = (error) => {
			this.errorhandler.show(error.data, error.status, this.$cookieStore);
		};


		selectPreferenceItem = (preferenceItem: IPreference<any>): void => {
			this.selectedPreferenceItem = preferenceItem;

			this.preferences.getPreferenceShareGroups(preferenceItem).$promise.then(
				data => {
					this.userRoles = data.userRoles;
				},
				error => {
					this.handleError(error);
				}
			);
		};


		shareOrUnshareSelectedPreferenceItem = (userRole: IUserRole): void => {
			this.preferences.shareOrUnsharePreferenceItem(this.selectedPreferenceItem, userRole).$promise.then(
				data => {
					this.updatePreferenceItemView();
				},
				error => {
					this.updatePreferenceItemView();
					this.handleError(error);
				}
			);
		};

		updatePreferenceShare = (preferenceItem: IPreference<any>): void => {
			this.preferences.updatePreferenceShare(preferenceItem).$promise.then(
				data => {
					this.updatePreferenceItemView();
				},
				error => {
					this.handleError(error);
				}
			);
		};


		deleteCustomizedPreferenceItem = (preferenceItem: IPreference<any>): void => {
			var self = this;
			this.dialog.confirm(
				this.nuclosI18N.getI18n("webclient.button.delete"),
				this.nuclosI18N.getI18n("webclient.dialog.delete"),
				function () {
					self.preferences.deleteCustomizedPreferenceItem(preferenceItem).$promise.then(
						data => {
							delete self.selectedPreferenceItem;
							self.updatePreferenceItemView();
						},
						error => {
							delete self.selectedPreferenceItem;
							self.handleError(error);
						}
					);
				}, function () {
				});
		};


		deletePreferenceItem = (preferenceItem: IPreference<any>): void => {
			this.dialog.confirm(
				this.nuclosI18N.getI18n("webclient.button.delete"),
				this.nuclosI18N.getI18n("webclient.dialog.delete"),
				() => {
					// ok
					this.preferences.deletePreferenceItem(preferenceItem).$promise.then(
						data => {
							delete this.selectedPreferenceItem;
							this.updatePreferenceItemView();
						},
						error => {
							delete this.selectedPreferenceItem;
							this.handleError(error);
						}
					);
				},
				() => {
					// not ok
				}
			);
		};

		/**
		 * Fetches all preferences and updates the view.
		 *
		 * TODO: Don't do this when only a single pref needs to be updated (e.g. after (un-)sharing).
		 */
		private updatePreferenceItemView = (): void => {
			this.preferences.getPreferences({}).then(
				preferences => {
					preferences.map((item: IPreference<any>) => {
						if (item.boMetaId) {
							item.boName = item.boMetaId.substring(item.boMetaId.lastIndexOf('_') + 1);
						}
						if (this.selectedPreferenceItem && item.prefId == this.selectedPreferenceItem.prefId) {
							this.selectedPreferenceItem = item;
						}
					});
					this.preferencesItems = preferences;
				},
				error => {
					this.handleError(error);
				}
			);
		};

	}
}
