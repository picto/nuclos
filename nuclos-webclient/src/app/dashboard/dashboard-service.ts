module nuclos.dashboard {
    
    import IWidget = nuclos.dashboard.widget.IWidget;
    
    export interface IDashboardService {
        widgets: Array<IWidget>;
        registerWidget: (widget: IWidget) => void;
    }
    
    
    export class DashboardService implements IDashboardService {
        
        widgets: Array<IWidget> = [];
        
        constructor () {
        }
        
        registerWidget = (widget: IWidget) : void => {
            for (let w of this.widgets) {
                if (w.id == widget.id) {
                    return;
                }
            }
            this.widgets.push(widget);
        };
        
        unregisterAllWidgets = () : void => {
        	this.widgets = [];
        }
        
    }
    
    var factory = ($q) => {
        return new DashboardService();
    };

    nuclosApp.factory("dashboardService", factory);
}