
module nuclos.dashboard {

    import IWidget = nuclos.dashboard.widget.IWidget;

    const LOCALSTORAGE_PREFIX = 'dashboard-widget-';

    export interface IDashboardCtrl {
    }

    export class DashboardCtrl {

        vm: IDashboardCtrl = this;
        dashboardItems: Array<IWidget> = [];

        constructor (
            protected dashboardTaskWidgetService: nuclos.dashboard.widget.IDashboardWidgetTaskService,
            protected dashboardMenuWidgetService: nuclos.dashboard.widget.IDashboardMenuWidgetService,
            protected dashboardService: nuclos.dashboard.IDashboardService,
            protected $timeout: ng.ITimeoutService
        ) {

            if (this.dashboardService.widgets.length == 0) {

                dashboardTaskWidgetService.registerWidgets()
                    .then(
                        dashboardMenuWidgetService.registerWidgets
                    )
                    .then(
                        () => {
                            for (let widget of this.dashboardService.widgets) {
                                this.loadWidgetConfig(widget);
                            }
                            this.dashboardItems = this.dashboardService.widgets;
                        }
                    )
                ;

            } else {
                for (let widget of this.dashboardService.widgets) {
                    this.loadWidgetConfig(widget);
                }
                this.dashboardItems = this.dashboardService.widgets;
            }
        }



        options = {
            cell_height: 200,
            vertical_margin: 10
        };

        onDragStop = function(event, ui, widget) {
            this.storeWidgetsConfig();
        };
        onResizeStop = function(event, ui) {
            this.storeWidgetsConfig();
        };


        loadWidgetConfig = (widget: IWidget): void => {
            var widgetConfigString = localStorage.getItem(LOCALSTORAGE_PREFIX + widget.id);
            if (widgetConfigString !== null) {
                widget.config = JSON.parse(widgetConfigString);
            }
        };

        storeWidgetsConfig = (): void => {
            this.$timeout(() => {
                for (let widget of this.dashboardItems) {
                    this.storeWidgetConfig(widget);
                }
            }, 200);
        };

        storeWidgetConfig = (widget: IWidget): void => {
            if (widget.id === undefined) {
                console.warn("Unable to save widget.", widget);
            }
            localStorage.setItem(LOCALSTORAGE_PREFIX + widget.id, JSON.stringify(widget.config));
        };


        getWidgetConfig = (widget: IWidget): Object => {
            var widgetConfigString = localStorage.getItem(LOCALSTORAGE_PREFIX + widget.id);
            var widgetConfig;
            if (widgetConfigString === null) {
                widgetConfig = {
                    hidden: false
                };
            } else {
                widgetConfig = JSON.parse(widgetConfigString);
            }
            return widgetConfig;
        };

        toggleWidget = (widget: IWidget, hidden: boolean) : void => {
            widget.config.hidden = hidden;
            this.storeWidgetConfig(widget);
        };
    }

    export class DashboardModalCtrl extends DashboardCtrl {

        constructor (
            protected dashboardTaskWidgetService: nuclos.dashboard.widget.IDashboardWidgetTaskService,
            protected dashboardMenuWidgetService: nuclos.dashboard.widget.IDashboardMenuWidgetService,
            protected dashboardService: nuclos.dashboard.IDashboardService,
            protected $timeout: ng.ITimeoutService,
            protected dialog: nuclos.dialog.DialogService,
            private $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance
        ) {
            super(dashboardTaskWidgetService, dashboardMenuWidgetService, dashboardService, $timeout);

            this.dialog.registerModalInstance(this.$uibModalInstance);
        }

        closeModal = () : void => {
            this.$uibModalInstance.close();
        };

    }
}
