module nuclos.dashboard.widget {
    
    export interface ITaskCtrl {
        tasks: Array<Object>;
    }
    
    export class TaskCtrl {

        vm: ITaskCtrl = this;
        tasks: Array<Object>;
        widgetConfig: IWidget;
        
        constructor (
            private dashboardTaskWidgetService: nuclos.dashboard.widget.IDashboardWidgetTaskService,
            private dashboardService: nuclos.dashboard.IDashboardService,
            private $location: ng.ILocationService,
            private dialog: nuclos.dialog.DialogService
        ) {
        }

        setWidgetConfig = (widget: IWidget) => {
            this.widgetConfig = widget;

			this.dashboardTaskWidgetService.tasks(this.widgetConfig.content.entry as TaskEntry).then(
                (tasks : Array<TaskEntry>) => {
                    this.vm.tasks = tasks;
                }
            );
        };
        
        closeModal = () : void => {
            this.dialog.closeModalInstances();
        };
        
    }
    
    nuclosApp.controller("TaskController", TaskCtrl);
}
