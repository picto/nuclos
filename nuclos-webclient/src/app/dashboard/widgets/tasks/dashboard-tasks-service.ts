module nuclos.dashboard.widget {

	import IResourceArray = angular.resource.IResourceArray;
	import IResource = angular.resource.IResource;

	export interface IDashboardWidgetTaskService extends IWidgetService {
        tasks: (entry: TaskEntry) => ng.IPromise<any>;
    }

    interface BoLinks {
        bos: nuclos.model.interfaces.Link;
    }

	interface TaskList {
		taskMetaId: string;
		boMetaId: string;
		name: string
	}

	interface TaskListMenuEntry extends IResource<TaskListMenuEntry> {
		entries: Array<TaskList>;
	}

    export interface TaskEntry {
        boMetaId: string;
        name: string;
        taskMetaId: string;
        bos: Array<nuclos.model.interfaces.Bo>;
        links?: BoLinks;
    }
    
    export class TaskService implements IDashboardWidgetTaskService {
     
        constructor (
            private dashboardService: nuclos.dashboard.IDashboardService,
            private $cookieStore: ng.cookies.ICookiesService,
            private $resource: ng.resource.IResourceService,
            private $q: ng.IQService,
			private nuclosI18N: nuclos.util.I18NService
            ) {
        }
        
        registerWidgets = () => {
			var deferred = this.$q.defer();

            this.taskLists().$promise.then(
	            taskLists => {
                    var count = 0;
                    for (let taskListMenuEntry of taskLists) {
                        for (let taskList of taskListMenuEntry.entries) {

							// NUCLOS-5422 - Dynamische Aufgabenlisten auch im Webclient und iOS-Client anbieten
							// TODO wird derzeit noch nicht unterstützt
							if (taskList.taskMetaId.length === 0) {
								continue;
							}

							var widget: IWidget = {
                                id: 'task-' + taskList.taskMetaId,
                                title: this.nuclosI18N.getI18n('webclient.tasks') + ' - ' + taskList.name,
                                template: 'app/dashboard/widgets/tasks/tasks.html',
                                url: '#/taskview/' + taskList.boMetaId + '/' + taskList.taskMetaId,
                                content: {
                                    entry: taskList
                                },
                                config: {
                                    gridstack: {
                                        x: '' + (count++ * 2 + 3),
                                        y: '0',
                                        width: '2',
                                        height: '3'
                                    },
                                    hidden: false
                                }
                            }
                            this.dashboardService.registerWidget(widget);
                        }
                    }
                    deferred.resolve();
                }
            );

            return deferred.promise;
        };

		taskLists = (): IResourceArray<TaskListMenuEntry> => {
			return this.$resource(restHost + '/meta/tasks').query() as IResourceArray<TaskListMenuEntry>;
		};

        tasks = (entry: TaskEntry) : ng.IPromise<any> => {
			var deferred = this.$q.defer();
            var result : Array<Object> = [];

            var url = entry.links.bos.href + '/query?searchFilter=' + entry.taskMetaId;
            (function (entry, $resource) {
                $resource(url).save(
                    {}, 
                    (data) => {
                        var resultEntry: TaskEntry = {
                            boMetaId: entry.boMetaId,
                            name: entry.name,
                            taskMetaId: entry.taskMetaId,
                            bos: []
                        };
                        resultEntry.bos = data.bos;
                        result.push(resultEntry);
                    }, 
                    (error) => {
                        console.error("Unable to get search filter", error);
                    }
                );
            }(entry, this.$resource));
            deferred.resolve(result);                        
            return deferred.promise;
        };
        
    }
    
    var factory = (dashboardService, $cookieStore, $resource, $q, nuclosI18N: nuclos.util.I18NService) => {
        return new TaskService(dashboardService, $cookieStore, $resource, $q, nuclosI18N);
    };

    nuclosApp.factory("dashboardTaskWidgetService", factory);
}