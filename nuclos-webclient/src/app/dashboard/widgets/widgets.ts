module nuclos.dashboard.widget {

    interface IWidgetConfigGridstack {
        // needs to be string not number because of used gridstack library
        x: string;
        y: string;
        width: string;
        height: string;
    }
    interface IWidgetConfig {
        hidden: boolean;
        gridstack: IWidgetConfigGridstack;
    }
    export interface IWidget {
        id: string;
        title: string;
        template: string;
        url?: string;
        
        // configuration like widget position
        config: IWidgetConfig; 
        
        // widget specific content
        content?: IWidgetContent;
    }

	export interface IWidgetContent {
		entry: Object;
	}

    export interface IWidgetService {
        registerWidgets: () => ng.IPromise<Object>;
    }
    
}
