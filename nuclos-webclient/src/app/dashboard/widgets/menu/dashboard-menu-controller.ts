module nuclos.dashboard.widget {
    
    export interface IDashboardMenuCtrl {
        vm: IDashboardMenuCtrl;
        menuItems: Array<Object>;
        toggleMenuItem: Function;
    }
    
    export class DashboardMenuCtrl implements IDashboardMenuCtrl {

        vm: IDashboardMenuCtrl = this;
        menuItems: Array<Object>;
        viewLink: string;
        
        constructor (
            private $location: ng.ILocationService,
            private dialog: nuclos.dialog.DialogService,
            private menuService: nuclos.menu.MenuService
        ) {
            this.init();
        }

        private init() {
            var viewType = localStorage.getItem("selectedViewType") == "tableview" ? "tableview" : "sideview";
            this.viewLink = '#/' + viewType;

            this.menuService.getMenu(restHost + '/meta/menu').then(
                (data: Object[]) => {
                    this.menuItems = data;
                },
                (error) => {
                }
            );
        }

        toggleMenuItem = (menu) => {
            menu.hidden = !menu.hidden;
            this.menuService.toggleMenuItem(menu.path, !menu.hidden);
        };

        closeModal = () : void => {
            this.dialog.closeModalInstances();
        };

    }
    
    nuclosApp.controller("DashboardMenuController", DashboardMenuCtrl);
}
