module nuclos.dashboard.widget {
    
    export interface IDashboardMenuWidgetService extends IWidgetService {
    }

    export class DashboardMenuWidgetService implements IDashboardMenuWidgetService {
     
        constructor (
            private dashboardService: nuclos.dashboard.IDashboardService,
            private $cookieStore: ng.cookies.ICookiesService,
            private $resource: ng.resource.IResourceService,
            private $q: ng.IQService,
			private nuclosI18N: nuclos.util.I18NService
            ) {
        }
        
        registerWidgets = () : ng.IPromise<Object> => {
			var deferred = this.$q.defer<Object>();
            var widget: IWidget = {
                id: 'menu',
                title: this.nuclosI18N.getI18n('webclient.menu'),
                template: 'app/dashboard/widgets/menu/menu.html',
                config: {
                    gridstack: {
                        x: '0',
                        y: '0',
                        width: '3',
                        height: '3'
                    },
                    hidden: false
                }
            };
            this.dashboardService.registerWidget(widget);
            deferred.resolve(this);
            return deferred.promise;
        };  
            
    }

    
    var factory = (dashboardService, $cookieStore, $resource, $q, nuclosI18N: nuclos.util.I18NService) => {
        return new DashboardMenuWidgetService(dashboardService, $cookieStore, $resource, $q, nuclosI18N);
    };

    nuclosApp.factory("dashboardMenuWidgetService", factory);
}