var openChartDetails; // this function needs to be global so it can be accessed by nvd3 click callback 

var nvd3Scope;
module nuclos.chart {

	import Bo = nuclos.model.interfaces.Bo;
	import Map = nuclos.model.interfaces.Map;

	export interface IChartCtrlScope extends ng.IScope {
		chart: any;
		chartData: any;
		chartDataString: string;
		chartOptionsString: string;
		chartOptionsStringValid: boolean;
		chartPreferences: Array<any>;
		selectedChartPreferenceItem: any;
		availableChartTypes: Array<any>;
		showoptionpanel: boolean;
		errorMessage?: string;
		nvd3: any;
		searchTemplate?: Array<ISearchTemplateClient>;
		dateFormat: string;
		primaryChartMetaData: any;
        authentication: Object;
		
		closeModal();
		newChart();
		redrawChart();
		selectChartPreference(any);
		selectedChartPreferenceItemIndex();
		updateDropdown(searchTemplateItemIndex, searchText);
		buildChartData(IChartCtrlScope);
		setShowoptionpanel(boolean);
		saveChart();
		exportChartDataAsCsv();
		deleteChart();
		search();
	}
	
	export class ChartCtrl {
		constructor(
				private $scope: IChartCtrlScope,
				private $uibModalInstance,
				private bo: nuclos.model.interfaces.Bo, 
				private $resource: ng.resource.IResourceService, 
				private preferences: nuclos.preferences.PreferenceService,
				private localePreferences: nuclos.clientprefs.LocalePreferenceService, 
				private util: nuclos.util.UtilService,
				private $timeout: ng.ITimeoutService,
				private $filter: ng.IFilterService,
				private dialog: nuclos.dialog.DialogService,
				private $cookieStore: ng.cookies.ICookiesService,
				private $q: ng.IQService,
				private metaService: nuclos.core.MetaService,
				private errorhandler,
				private nuclosI18N: nuclos.util.I18NService
			) {
			
            this.dialog.registerModalInstance(this.$uibModalInstance);
			
            $scope.authentication = $cookieStore.get('authentication');
            
			$scope.nvd3 = {
				api: undefined
			};
			
			nvd3Scope = $scope.nvd3;
			// dateformat for datepicker in searchfilter
			$scope.dateFormat = localePreferences.getDateFormat();

			var chartLoaded = false;

			var allMandatoryFieldsFilled = false;

			// load preferences from REST service
			this.preferences.getPreferences({
				boMetaId: this.bo.boMetaId,
				type: ['chart']
			}).then(
				function(chartPreferences) {
					
					$scope.chartPreferences = $filter('orderBy')(chartPreferences, 'content.name.de');
					
					var index = 0;
					$scope.chartPreferences.forEach(chartPreferenceItem => {
						chartPreferenceItem.content.chartIndex = index;
						index ++;
					});
					
					
					if ($scope.chartPreferences.length > 0) {
						// $scope.selectedChartPreferenceItem = $scope.chartPreferences[0];
						$scope.selectChartPreference($scope.chartPreferences[0]);
					} else {
						chartLoaded = true;
					}
				},
				function(error) {
					console.error('Unable to get preferences.', error);
				}
			);
			
			
			
			// options and data can be edited as JSON in textarea 
			$scope.$watch('chartData', function(json) {
				$scope.chartDataString = json as string;
			}, true);
			$scope.$watch('chartDataString', function(json) {
				if (json) {
					if (typeof(json) === 'object') {
						$scope.chartData = json;
					} else {
						$scope.chartData = JSON.parse(json + '');
					}
				}
			}, true);

			$scope.$watch('selectedChartPreferenceItem.content', (json, oldvalue) => {
				$scope.chartOptionsString = util.jsonStringifyWithFunctions(json);
				if (json && chartLoaded && oldvalue && chartLoaded) {
					$scope.selectedChartPreferenceItem.isDirty = true;
				}
			}, true);
			$scope.$watch('chartOptionsString', (jsonNew, jsonOld) => {
				if (jsonNew) {
					if (!$scope.selectedChartPreferenceItem) {
						$scope.selectedChartPreferenceItem = {};
					}
					try {
						$scope.selectedChartPreferenceItem.content = util.jsonParseWithFunctions(jsonNew as string);
						$scope.chartOptionsStringValid = true;
					} catch(error) {
						$scope.chartOptionsStringValid = false;
						
					}
					if (jsonNew && jsonOld && jsonNew != jsonOld && chartLoaded) {
						$scope.selectedChartPreferenceItem.isDirty = true;
						if (allMandatoryFieldsFilled) {
							this.buildChartData($scope.selectedChartPreferenceItem.content, $scope);
						}
					}
				}
			}, true);
			
			this.$scope.redrawChart = function() {
				
				chartLoaded = false;

				try {
					$scope.nvd3.api.update();
					//$scope.selectedChartPreferenceItem.content.chart.update();
					
					console.info('Chart updated.');
					chartLoaded = true;
					
				} catch (error) {
					console.warn('Unable to update chart.', $scope, error);
					
					// invoke chart rendering by changing content
					if ($scope.selectedChartPreferenceItem && $scope.selectedChartPreferenceItem.content && $scope.selectedChartPreferenceItem.content.chart) {

						$scope.selectedChartPreferenceItem.content.___dummy = {};
						$timeout(function() {
							delete $scope.selectedChartPreferenceItem.___dummy;
							$timeout(function() {
								chartLoaded = true;
							}, 100);
						}, 0);
						
					} else {
						chartLoaded = true;
					}

				}
			}

			// displayed chart type icons from: http://www.iconarchive.com/show/oxygen-icons-by-oxygen-icons.org.html
			this.$scope.availableChartTypes = [
				'multiBarChart',
				//'discreteBarChart',
				//'historicalBarChart',
				'multiBarHorizontalChart',
				'lineChart',
				'stackedAreaChart',
				'scatterChart',
				'pieChart',
				'linePlusBarChart'
			];

			
			this.$scope.closeModal = function () {
				if ($scope.nvd3.api) {
					$scope.nvd3.api.clearElement();
				}
				$uibModalInstance.dismiss('cancel');
			};

			this.$scope.setShowoptionpanel = function (show: boolean) {
				$scope.showoptionpanel = show;
				$timeout(function() {
					$scope.redrawChart();
				}, 0);
			};


			$scope.updateDropdown = function(searchTemplateItemIndex, searchText) {
				var item = $scope.searchTemplate[searchTemplateItemIndex]; 
				item.dropdownValuesFiltered = item.dropdownValues.filter(
					function(d) {
						if (d.name && d.name.toLowerCase) {
							return d.name.toLowerCase().indexOf(searchText.toLowerCase()) != -1;
						}
						return true;
					}
				);
			};
			
			
			
			var preparePrint = function() {
				// clone chart DOM element and add it below "body" to make correct printing possible
				$('#chart-print-area').remove();
				var svgWidth = $('svg').width();
				var svgHeight = $('svg').height();
				$('body').prepend("<div id='chart-print-area'></div>");
				$('body').addClass('print-chart');
				if ($('svg').length != 0) {
					$('svg').clone().prependTo("#chart-print-area");
					$('#chart-print-area svg').get(0).setAttribute('viewBox', '0, 0, ' + svgWidth + ', ' + svgHeight);
				}
			};

			// remove cloned chart DOM element which is only used for printing
			$scope.$on("$destroy", function handler() {
				$('body').removeClass('print-chart');
				$('#chart-print-area').remove();
			});

			// Ugly!
			var _loadData = this.loadData;
			var _buildChartData = this.buildChartData;

			// select chart by clicking on tab
			this.$scope.selectChartPreference = function(chartPreferenceItem) {
				
				chartLoaded = false;
				
				nvd3Scope = $scope.nvd3;
				
				if (nvd3Scope.api) {
					$scope.chartData = [];
					nvd3Scope.api.clearElement();
				}


				// prepare search filter
				if ($scope.selectedChartPreferenceItem != chartPreferenceItem) {
					if (chartPreferenceItem.content.chart.searchTemplate) {
						$scope.searchTemplate = angular.copy(chartPreferenceItem.content.chart.searchTemplate);
						$scope.searchTemplate.forEach(item => {
							var fieldName = getShortFieldName({boMetaId: chartPreferenceItem.content.chart.primaryChart.boMetaId}, item.boAttrId);
							
							metaService.getBoMetaData(chartPreferenceItem.content.chart.primaryChart.boMetaId).then(
								function(metaData) {
									
									$scope.primaryChartMetaData = metaData;
									
									var fieldMeta = metaData.attributes[fieldName];
									
									if (item.dropdown) {
										
										item.inputType = 'dropdown'; 
										item.url = restHost + '/bos/' + bo.boMetaId + '/' +bo.boId + '/subBos/' + chartPreferenceItem.content.chart.primaryChart.refBoAttrId + '/valuelist/' + fieldMeta.boAttrId;
										$resource(item.url).query(function(data) {
											item.dropdownValues = data;
											item.dropdownValuesFiltered = data;
										});
		
									} else {
										if (fieldMeta) {
											if (fieldMeta.type == 'Timestamp' || fieldMeta.type == 'Date') {
												item.inputType = 'datepicker'; 
											} else {
												item.inputType = 'string'; 
											}
										}
									}

								}, 
								function(error) {
									errorhandler.show(error.message); 
								}
							);
							
						});
					} else {
						delete $scope.searchTemplate;
					}
				}

				$scope.selectedChartPreferenceItem = undefined;
				
				if ($scope.searchTemplate) {
					// build something like: 'CompositeCondition:AND:[Comparison:EQUAL:categoryName:Hardware,Comparison:GTE:orderMonth:2015.01]';
					var searchFilter = 'CompositeCondition:AND:[';
					var addSeparator:boolean = false;
					for (var i in $scope.searchTemplate) {
						var searchTemplateItem = $scope.searchTemplate[i];
						if (searchTemplateItem.value) {
							var value = searchTemplateItem.value.name ? searchTemplateItem.value.name : searchTemplateItem.value;
							if (value) {
								// TODO use FQN
								var fieldName = getShortFieldName({boMetaId: chartPreferenceItem.content.chart.primaryChart.boMetaId}, searchTemplateItem.boAttrId);
								if (searchTemplateItem.inputType == 'datepicker') {
									value = moment(value).format('YYYY-MM-DD');
								}
								if (addSeparator) {
									searchFilter += ',';
								}
								searchFilter += 'Comparison:' + searchTemplateItem.comparisonOperator + ':' + fieldName + ':' + value;
								addSeparator = true;
							}
							
						}
						
						if (!searchTemplateItem.value && searchTemplateItem.mandatory) {
							// mandatory field is not set - don't load data
							$scope.selectedChartPreferenceItem = chartPreferenceItem;
							allMandatoryFieldsFilled = false;
							return;
						}
						
					}
					allMandatoryFieldsFilled = true;
					searchFilter += ']';
				}
				
				_loadData(bo, chartPreferenceItem.content, searchFilter).$promise.then(
					function(data) {
						if (!bo.subChartBos) {
							bo.subChartBos = {} as Map<Bo>;
						}
						bo.subChartBos[chartPreferenceItem.content.chart.primaryChart.refBoAttrId] = data;
						_buildChartData(chartPreferenceItem.content, $scope);
						$scope.selectedChartPreferenceItem = chartPreferenceItem;
						$timeout(function() { // timeout to make sure $watch was already called
							chartLoaded = true;
							preparePrint();
						}, 1000);
			
					}, 
					function(error) {
						console.log(error);
						$scope.selectedChartPreferenceItem = chartPreferenceItem;
						errorhandler.show(error.data, error.status); 
						chartLoaded = true;
					}
				);

			};
			

			this.$scope.search = () => {
				this.$scope.selectChartPreference($scope.selectedChartPreferenceItem);
				
				// trigger chart update (needed for strange behaviour with linePlusBarChart - TODO)
				$timeout(function() {
					$scope.chartOptionsString = $scope.chartOptionsString + ' ';
				}, 1000);
			};

			this.$scope.newChart = () => {
				var nextChartIndex = 0;
				if ($scope.chartPreferences.length > 0) {
					nextChartIndex = $scope.chartPreferences[$scope.chartPreferences.length - 1].chartIndex + 1
				}
				var newChart = {
					content: {
						"name": {
								"de": "Neues Diagramm"
							},
						"chart": {
							"type": this.$scope.availableChartTypes[0],
							"margin": {
								"left": 100,
								"top": 25,
								"bottom": 40,
								"right": 40
							},
							"primaryChart": {
								"boMetaId": "",
								"refBoAttrId": "",
								"categoryBoAttrId": "",
								"categoryLabel": {
									"de": "X"
								},
								"scaleLabel": {
									"de": "Y"
								},
								"series": [
									{
										"boAttrId": "",
										"label": {
											"de": "Wert"
										}
									}
								]
							}
						}
					},
					boMetaId: bo.boMetaId,
					type: 'chart',
					isDirty: true,
					chartIndex: nextChartIndex
				};
				$scope.selectedChartPreferenceItem = newChart;
				$scope.chartPreferences.push(newChart);
				$scope.showoptionpanel = true;
				this.buildChartData($scope.selectedChartPreferenceItem.content, $scope);
				$scope.redrawChart();
			};



			this.$scope.deleteChart = () => {
				
				dialog.confirm(
					this.nuclosI18N.getI18n("webclient.button.delete"),
					this.nuclosI18N.getI18n("webclient.dialog.delete"),
					function() {
						// ok
						
						var afterPreferenceDeletion = function() {
							$scope.chartPreferences.splice($scope.chartPreferences.indexOf($scope.selectedChartPreferenceItem), 1);
							if ($scope.chartPreferences.length > 0) {
								$scope.selectedChartPreferenceItem = $scope.chartPreferences[0];
								this.Data($scope.selectedChartPreferenceItem.content, $scope);
								$scope.redrawChart();
							} else {
								delete $scope.selectedChartPreferenceItem;
							}
						};
						
						if ($scope.selectedChartPreferenceItem.prefId) {
							preferences.deletePreferenceItem($scope.selectedChartPreferenceItem).$promise.then(
								function(data) {
									afterPreferenceDeletion();
								},
								function(error) {
									console.error('Unable to delete preferences.', error);
									errorhandler.show(error.message); 
								}
							);
						} else {
							afterPreferenceDeletion();
						}
						
					},
					function() {
						// cancel
					}
				);
			};

			this.$scope.saveChart = () => {

				var existingChart = this.$scope.selectedChartPreferenceItem.prefId != undefined;

				this.preferences.savePreferenceItem(this.$scope.selectedChartPreferenceItem).then(
					data => {
						if (!existingChart) {
							$scope.chartPreferences[$scope.chartPreferences.indexOf($scope.selectedChartPreferenceItem)] = data;
							$scope.selectedChartPreferenceItem = data;
						}
						delete $scope.selectedChartPreferenceItem.isDirty;
					},
					error => {
						console.error('Unable to save preferences.', error);
						errorhandler.show(error.message); 
					}
				);
			
			};

			this.$scope.exportChartDataAsCsv = () => {
				
				var preferenceContent = $scope.selectedChartPreferenceItem.content;
				
				var columnSeparator = ';';
				var lineSeparator = '\n';
				var fileName = preferenceContent.name.de + '.csv';
				
				
				metaService.getBoMetaData(preferenceContent.chart.primaryChart.boMetaId).then(function(metaData) {
				
					var csvExport = [];
					
					var columnAttributes = [];
					columnAttributes.push(getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, preferenceContent.chart.primaryChart.categoryBoAttrId));
					for(var i=0; i<preferenceContent.chart.primaryChart.series.length; i++) {
						var serie = preferenceContent.chart.primaryChart.series[i];
						columnAttributes.push(getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, serie.boAttrId));
					}
				
	
					var csvHeader = [];
					
					for (var k=0; k<columnAttributes.length; k++) {
						var columnAttribute = columnAttributes[k];
						if (metaData.attributes[columnAttribute] && metaData.attributes[columnAttribute].name) {
							csvHeader.push(metaData.attributes[columnAttribute].name);
						} else {
							csvHeader.push(columnAttribute);
						}
					}
									
									
					csvExport.push(csvHeader.join(columnSeparator) + lineSeparator);
					
					for (var j=0; j < bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos.length; j++) {
						var subBo = bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos[j];
						
						var csvLine = [];
						for (var k=0; k<columnAttributes.length; k++) {
							var columnAttribute = columnAttributes[k];
							var value = '' + subBo.attributes[columnAttribute];
							value = value.replace('.', localePreferences.getDecimalSeparator());
							csvLine.push(value);
						}
	
						csvExport.push(csvLine.join(columnSeparator) + lineSeparator);
					}	
					var blob = new Blob(csvExport, {type: "text/plain;charset=utf-8"});
					saveAs(blob, fileName);
			
				});
			
			};
	
			$scope.selectedChartPreferenceItemIndex = function () {
				if (!$scope.selectedChartPreferenceItem) {
					return -1;
				}
				return $scope.chartPreferences.indexOf($scope.selectedChartPreferenceItem);
			};

		}

		loadData = (bo, preferenceContent, searchFilter) => {
			// TODO links aus REST verwenden
			// var subdataLink = selectedBo.subBos[scope.reffield].links.bos.href;

			var sort = preferenceContent.chart.primaryChart.sort;
			var sortBy;
			var sortDirection = 'asc'
			if (sort != undefined && sort.sortByBoAttrId != undefined) {
				sortBy = sort.sortByBoAttrId;
			} else {
				// sort by category
				sortBy = preferenceContent.chart.primaryChart.categoryBoAttrId;
			}
			if (sort != undefined && sort.sortDirection != undefined) {
				sortDirection = sort.sortDirection;
			}

			var subdataLink: string = restHost + '/bos/' + bo.boMetaId + '/' + bo.boId + '/subBos/' + preferenceContent.chart.primaryChart.refBoAttrId
				+ '?fields=tableview&sort=' + sortBy + '%20' + sortDirection;

			if (searchFilter) {
				subdataLink += '&searchCondition=' + encodeURIComponent(searchFilter);
			}

			return this.$resource(subdataLink).get();
		}

		buildChartData = (preferenceContent, $scope: IChartCtrlScope) => {

			var _bo = this.bo;
		this.metaService.getBoMetaData(_bo.boMetaId, preferenceContent.chart.primaryChart.refBoAttrId).then(
			function(subformMetaData) {

				if (
					!preferenceContent.chart.primaryChart.refBoAttrId ||
					!preferenceContent.chart.primaryChart.boMetaId ||
					!preferenceContent.chart.primaryChart.categoryBoAttrId ||
					!_bo.subChartBos
				) {
					console.error('Invalid chart config.');
					if(_bo.subChartBos) {
						$scope.errorMessage = this.nuclosI18N.getI18n('webclient.charts.warning.invalidChartConfig');
					}
					return;
				}


				if (!preferenceContent.chart.dispatch) {
					preferenceContent.chart.dispatch = {};
				}


				/**
				 crosshair
				 http://wiki.nuclos.de/display/Entwicklung/Konfiguration#Konfiguration-4Quadranten/Fadenkreuz

				 if crosshair is configured in chart.primaryChart.crosshair
				 the crosshair attribute data of the first dataset will be added to the chart data
				 the addCrosshair function will be called after the chart has been rendered
				 */
				var crosshairData = undefined;
				var subChartBos = _bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos;
				if (subChartBos.length > 0) {
					var crosshairConfig = preferenceContent.chart.primaryChart.crosshair;
					if (crosshairConfig) {
						var categoryBoAttrIdShort = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, crosshairConfig.categoryBoAttrId);
						var seriesBoAttrIdShort = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, crosshairConfig.seriesBoAttrId);

						var crosshaircategory = subChartBos[0].attributes[categoryBoAttrIdShort];
						var crosshairseries = subChartBos[0].attributes[seriesBoAttrIdShort];

						if (crosshaircategory && crosshairseries) {
							// crosshair data will be added to chart data
							crosshairData = {
								category: crosshaircategory,
								series: crosshairseries,
								color: crosshairConfig.color
							};

							// crosshair will be rendered after chart has been rendered
							if (!preferenceContent.chart.dispatch.renderEnd) {
								preferenceContent.chart.dispatch.renderEnd = function () { this.addCrosshair(); };
							}
						}
					}
				}


				// open entry in new tab by click on element
				if (preferenceContent.chart.type == 'multiBarChart' ||
					preferenceContent.chart.type == 'pieChart' ||
					preferenceContent.chart.type == 'lineChart' ||
					preferenceContent.chart.type == 'scatterChart') {

					openChartDetails = function(e) {
						$('.nvtooltip').hide();
						if ($scope.primaryChartMetaData) {
							var bos = _bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos;
							// scatterChart: pointIndex
							// barChart, pieChart: index
							var index = e.pointIndex != undefined ? e.pointIndex : e.index;
							var href = baseUrl() + 'sideview/' + $scope.primaryChartMetaData.detailBoMetaId + '/' + bos[index].boId;
							window.open(href, '_blank');
						}
					};

					if (preferenceContent.chart.type == 'multiBarChart' && (!preferenceContent.chart.multibar || !preferenceContent.chart.multibar.dispatch)) {
						preferenceContent.chart.multibar = {
							dispatch: {
								elementClick: function (e) {openChartDetails(e);}
							}
						};
					} else if (preferenceContent.chart.type == 'pieChart' && (!preferenceContent.chart.pie || !preferenceContent.chart.pie.dispatch)) {
						preferenceContent.chart.pie = {
							dispatch: {
								elementClick: function (e) {openChartDetails(e);}
							}
						};
					} else if (preferenceContent.chart.type == 'scatterChart' && (!preferenceContent.chart.scatter || !preferenceContent.chart.scatter.dispatch)) {
						preferenceContent.chart.scatter = {
							dispatch: {
								elementClick: function (e) {openChartDetails(e);}
							}
						};
					} else if (preferenceContent.chart.type == 'lineChart' && (!preferenceContent.chart.lines || !preferenceContent.chart.lines.dispatch)) {
						preferenceContent.chart.lines = {
							dispatch: {
								elementClick: function (e) {openChartDetails(e);}
							}
						};
					}
				}

				var scaleLabel = preferenceContent.chart.primaryChart.scaleLabel ? ' ' + preferenceContent.chart.primaryChart.scaleLabel.de.label : '';

				var categoryAttributeName = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, preferenceContent.chart.primaryChart.categoryBoAttrId);
				var categoryIsDateType = subformMetaData.attributes[categoryAttributeName].type == 'Timestamp' || subformMetaData.attributes[categoryAttributeName].type == 'Date';

				if (!preferenceContent.chart.x) {
					if (categoryIsDateType) {
						preferenceContent.chart.x = function(dataSet) { return new Date(dataSet.categoryValue); };
					} else {
						preferenceContent.chart.x = function(dataSet) { return dataSet.categoryValue; };
					}
				}

				var chartData = [];
				for(var i=0; i<preferenceContent.chart.primaryChart.series.length; i++) {
					var serie = preferenceContent.chart.primaryChart.series[i];

					var seriesAttributeName = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, serie.boAttrId);
					var seriesIsDateType = subformMetaData.attributes[seriesAttributeName].type == 'Timestamp' || subformMetaData.attributes[seriesAttributeName].type == 'Date';

					var tooltipBoAttrName = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, serie.tooltipBoAttrId);

					if (!preferenceContent.chart.y) {
						if (seriesIsDateType) {
							preferenceContent.chart.y = function(dataSet) { return new Date(dataSet.seriesValue); };
						} else {
							preferenceContent.chart.y = function(dataSet) { return dataSet.seriesValue; };
						}
					}

					var values = [];

					delete $scope.errorMessage;

					for (var j=0; j < _bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos.length; j++) {
						var subBo = _bo.subChartBos[preferenceContent.chart.primaryChart.refBoAttrId].bos[j];

						// var categoryAttributeName = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, preferenceContent.chart.primaryChart.categoryBoAttrId);
						var categoryValue = subBo.attributes[categoryAttributeName];
						if (categoryIsDateType) {
							categoryValue = new Date(categoryValue);
						}

						// var seriesAttributeName = getShortFieldName({boMetaId: preferenceContent.chart.primaryChart.boMetaId}, serie.boAttrId);
						var seriesValue = subBo.attributes[seriesAttributeName];
						if (seriesIsDateType) {
							seriesValue = new Date(seriesValue);
						}


						var tooltip = subBo.attributes[tooltipBoAttrName];
						if (tooltip != undefined) {
							if (!preferenceContent.chart.tooltip) {
								preferenceContent.chart.tooltip = {};
							}
							if (!preferenceContent.chart.tooltip.contentGenerator) {
								preferenceContent.chart.tooltip.contentGenerator = "function(p) { return customizedTooltipContentGenerator(p); }";
							}
						}

						values.push(
							{
								categoryValue: categoryValue,
								seriesValue: seriesValue,
								tooltip: tooltip
							}
						);
					}

					chartData.push( {
						// TODO use localized string: key: this.localizedString(serie.label),
						key: serie.label.de,
						values: values,
						color: serie.color,
						crosshair: crosshairData,
						bar: serie.bar
					});
				}

				$scope.chartData = chartData;
				try {
					if (nvd3Scope.api && nvd3Scope.api.getScope() && nvd3Scope.api.getScope().chart) {
						if (nvd3Scope.api.getScope().options.chart.synchronizeLineBarChartYAxes) {
							this.synchronizeLineBarChartYAxes();
						}
						nvd3Scope.api.update();
					}
				} catch(error) {
					console.warn("Unable to update chart.", error);
				}

				// Y-axis unit label
				if (!preferenceContent.chart.yAxis) {
					preferenceContent.chart.yAxis = {};
				}
				if (!preferenceContent.chart.yAxis.tickFormat) {
					preferenceContent.chart.yAxis.tickFormat = new Function('value', 'return value +"' +  scaleLabel + '";');
				}

			},
			function(error) {
				console.error("Unable to get meta data.");
			}
		);

	};

		// TODO: move
		localizedString = (localeString: LocaleString) => {
			var locale: string = this.localePreferences.getLocale();
			if (locale && locale.indexOf('de') == 0 || !localeString.en) {
				return localeString.de;
			}			
			return localeString.en;
		};
	}

}

angular.module("nuclos").controller("chartController", nuclos.chart.ChartCtrl);
