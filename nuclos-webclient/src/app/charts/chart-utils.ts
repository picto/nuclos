/* global nvd3Scope */

import Crosshair = nuclos.chart.Crosshair;
module nuclos.chart {
	export class Crosshair {
		circlegroup = undefined;
		offset = {
			x: 0,
			y: 0
		};
		x1 = undefined;
		y1 = undefined;

		startMove(event, moveType) {
			this.x1 = event.clientX;
			this.y1 = event.clientY;
			document.documentElement.setAttribute("onmousemove", "crosshair.moveIt(event)")

			if (moveType == 'single') {
				this.circlegroup = event.target;
			}
			else {
				this.circlegroup = event.target.parentNode;
			}
		}

		moveIt(event) {
			$('.crosshair-line').hide();
			$('.crosshair-text').hide();
			var translation = this.circlegroup.getAttributeNS(null, "transform").slice(10, -1).split(' ');
			var sx = parseInt(translation[0]);
			var sy = parseInt(translation[1]);

			this.offset.x = sx + event.clientX - this.x1;
			this.offset.y = sy + event.clientY - this.y1;

			this.circlegroup.setAttributeNS(null, "transform", "translate(" + (sx + event.clientX - this.x1) + " " + (sy + event.clientY - this.y1) + ")");
			this.x1 = event.clientX;
			this.y1 = event.clientY;
		}

		endMove() {
			document.documentElement.setAttributeNS(null, "onmousemove", null)
			nvd3Scope.api.refresh();
			$('.crosshair-line').show();
			$('.crosshair-text').show();
		}


		getCrosshairConfig() {
			var data = d3.select('.modal-dialog').selectAll('svg .nv-groups').data();
			if (data == undefined || data[0] == undefined || data[0][0] == undefined || data[0][0].crosshair == undefined) {
				return;
			}
			return data[0][0].crosshair;
		};

		addLine(x1, y1, x2, y2, color) {
			d3.select('.modal-dialog').selectAll('.nv-series-0')
				.append('path')
				.attr({
					d: "M" + x1 + "," + y1 + "L" + x2 + "," + y2,
					stroke: color,
					transform: "translate(0 0)",
					"stroke-width": 2,
					class: 'crosshair-line'
				});
		};


		addCircle(x, y) {
			var canvas = $('.nv-point-paths').length != 0 ? d3.select('.modal-dialog').select('.nv-point-paths') : d3.select('.modal-dialog').select('.nv-groups');
			var g = canvas
				.append('g')
				.attr({
					opacity: 0.0,
					style: "cursor:pointer;",
					onmouseover: "$(this).attr('opacity', 0.8);",
					onmouseout: "$(this).attr('opacity', 0.0);",
					transform: "translate(" + this.offset.x + " " + this.offset.y + ")",
					onmousedown: "crosshair.startMove(event, 'group');",
					onmouseup: "crosshair.endMove()"
				});
			g.append('circle')
				.attr({
					r: 10,
					cx: x,
					cy: y,
					stroke: "lightblue",
					fill: "lightblue",
					"stroke-width": 3,
					class: 'crosshair-circle'
				});
			g.append('circle')
				.attr({
					r: 1,
					cx: x,
					cy: y,
					stroke: "green",
					"stroke-width": 1
				});
			g.append('text')
				.attr({
					x: x + 20,
					y: y - 10,
					class: 'crosshair-text',
					// stroke: "black",
				}).text(function (d) {
				var chart = nvd3Scope.api.getScope().chart;
				var xScale = d3.scale.linear().domain(chart.xAxis.scale().domain()).range(chart.xAxis.range());
				var yScale = d3.scale.linear().domain(chart.yAxis.scale().domain()).range(chart.yAxis.range());

				var x = xScale.invert(xScale(this.getCrosshairConfig().category) + this.offset.x);
				var y = yScale.invert(yScale(this.getCrosshairConfig().series) + this.offset.y);

				return d3.round(x) + ' / ' + d3.round(y);
			});
		};
	}
}

var crosshair = new nuclos.chart.Crosshair();

var addCrosshair = function () {
	if ($('.modal-dialog .crosshair-circle').length > 0) {
		// crosshair was already added
		return;
	}

	var crosshairConfig = crosshair.getCrosshairConfig()
	if (crosshairConfig == undefined) {
		return;
	}

	var x = crosshairConfig.category;
	var y = crosshairConfig.series;

	var color = crosshairConfig.color ? crosshairConfig.color : 'black';

	if ($('.crosshair-line').length > 0) {
		d3.select('.modal-dialog').selectAll(".crosshair-line").remove();
	}

	var chartDim = d3.select('.modal-dialog').selectAll('svg .nv-groups').node()['getBBox']();

	var chart = nvd3Scope.api.getScope().chart;

	var xScale = d3.scale.linear().domain(chart.xAxis.scale().domain()).range(chart.xAxis.range());
	var yScale = d3.scale.linear().domain(chart.yAxis.scale().domain()).range(chart.yAxis.range());

	y = yScale(y);
	x = xScale(x);

	crosshair.addLine(0, y + crosshair.offset.y, chartDim.width, y + crosshair.offset.y, color);
	crosshair.addLine(x + crosshair.offset.x, 0, x + crosshair.offset.x, chartDim.height, color);

	crosshair.addCircle(x, y);
};


// for additional info in tooltip
var customizedTooltipContentGenerator = function (d) {
	if (d === null) {
		return '';
	}

	var headerFormatter = function (value) {
		return value;
	};
	var keyFormatter = function (value) {
		return value;
	};
	var valueFormatter = function (value) {
		return value;
	};
	var headerEnabled = true;

	var table = d3.select(document.createElement("table"));
	if (headerEnabled) {
		var theadEnter = table.selectAll("thead")
			.data([d])
			.enter().append("thead");

		theadEnter.append("tr")
			.append("td")
			.attr("colspan", 3)
			.append("strong")
			.classed("x-value", true)
			.html(headerFormatter(d.value));
	}

	var tbodyEnter = table.selectAll("tbody")
		.data([d])
		.enter().append("tbody");

	var trowEnter = tbodyEnter.selectAll("tr")
		.data(function (p) {
			return p.series
		})
		.enter()
		.append("tr")
		.classed("highlight", function (p: any) {
			return p.highlight
		});

	trowEnter.append("td")
		.classed("legend-color-guide", true)
		.append("div")
		.style("background-color", function (p: any) {
			return p.color
		});

	trowEnter.append("td")
		.classed("key", true)
		.html(function (p: any, i) {
			return keyFormatter(p.key)
		});

	trowEnter.append("td")
		.classed("value", true)
		.html(function (p: any, i) {
			return valueFormatter(p.value)
		});

	// add additional tooltip info
	var tooltipValue;
	if (d.data != undefined && d.data.tooltip != undefined) {
		tooltipValue = d.data.tooltip;
	} else if (d.seriesIndex != undefined && d.pointIndex != undefined) {
		tooltipValue = d.series[d.seriesIndex].values[d.pointIndex].tooltip;
	}
	if (tooltipValue != undefined) {
		trowEnter.append("td")
			.classed("value", true)
			.html(tooltipValue);
	}

	trowEnter.selectAll("td").each(function (p) {
		if (p.highlight) {
			var opacityScale = d3.scale.linear().domain([0, 1]).range(["#fff", p.color]);
			var opacity = 0.6;
			d3.select(this)
				.style("border-bottom-color", opacityScale(opacity))
				.style("border-top-color", opacityScale(opacity))
			;
		}
	});

	var html = table.node()['outerHTML'];
	if (d.footer !== undefined)
		html += "<div class='footer'>" + d.footer + "</div>";
	return html;

};

var synchronizeLineBarChartYAxes = function () {
	var series = nvd3Scope.api.getScope().data;
	var minMaxValues = [];
	for (var index = 0; index < series.length; index++) {
		var seriesItem: any = series[index];
		var minMax = d3.extent(seriesItem.values, function (d: any) {
			return d.seriesValue;
		});
		minMaxValues.push({
			min: minMax[0],
			max: minMax[1]
		});
	}
	var min = d3.min(minMaxValues, function (d) {
		return d.min;
	});
	var max = d3.max(minMaxValues, function (d) {
		return d.max;
	});

	var options = nvd3Scope.api.getScope().options;
	if (!options.chart.lines) {
		options.chart.lines = {};
	}
	if (!options.chart.bars) {
		options.chart.bars = {};
	}

	options.chart.lines.forceY = [min, max];
	options.chart.bars.forceY = [min, max];

	console.log('Updating y-axis of chart. min:', min, 'max:', max);
	nvd3Scope.api.updateWithOptions(options);
};

