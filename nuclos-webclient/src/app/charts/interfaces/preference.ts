module nuclos.preferences {

	export interface IPreference<T> {
		prefId?: string;
		type: PreferenceType;
		boMetaId: string;
		boName?: string;
		name?: string;
		shared?: boolean;
		customized?: boolean;
		content: T;
	}

	export interface IPreferenceContent {

	}
}