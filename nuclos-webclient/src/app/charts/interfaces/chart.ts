module nuclos.chart {

	export interface LocaleString {
		de: string;
		en: string;
	}
	
	export class ChartType {
		static COLUMN: string = 'COLUMN';
		static BAR: string = 'BAR';
		static LINE: string = 'LINE';
		static PIE: string = 'PIE';
		static SCATTER: string = 'SCATTER';
		static SPEEDOMETER: string = 'SPEEDOMETER';
	}

	export interface IChartISerie {
		attrId: string;
		label?: LocaleString;
		color: string;
	}

	export interface IChart {
		chartType: ChartType;
		useCombinedScaleWithPrimary?: boolean;
		boMetaId: string;
		refField: string;						// ok????
		categoryAttrId: string;
		categoryLabel: LocaleString;
		scaleLabel?: LocaleString;
		series: Array<IChartISerie>;
	}	

	export interface IChartConfiguration {
		chartLabel: LocaleString;
		primaryChart: IChart;
		secondaryChart?: IChart;
	}
	
	
	export interface ISearchTemplate {
        label: string;
        value: any;
		boAttrId: string;
        comparisonOperator: string;
        mandatory: boolean;
		dropdown: boolean;
	}

	export interface ISearchTemplateClient extends ISearchTemplate {
		inputType?: string;
		url?: string;
		dropdownValues? : Array<any>;
		dropdownValuesFiltered? : Array<any>;
	}

}