/// <reference path="./sideview-model.ts" />

module nuclos.sideview {

	import IPreferenceFilter = nuclos.preferences.IPreferenceFilter;
	import Meta = nuclos.sideview.Meta;
	import IPreference = nuclos.preferences.IPreference;
	import IPromise = angular.IPromise;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
	import SearchtemplateModel = nuclos.searchtemplate.SearchtemplateModel;
	import SearchTemplateAttr = nuclos.searchtemplate.SearchTemplateAttr;
	import PerspectiveService = nuclos.perspective.PerspectiveService;
	import SideviewmenuService = nuclos.sideview.SideviewmenuService;
	import IPerspective = nuclos.perspective.IPerspective;


	export interface SearchtemplatePreferenceContent {
		allAttributes: Array<SearchTemplateAttr>;
		attributes: Array<SearchTemplateAttr>;
		disabled: boolean;
		isValid: boolean;
		selected: boolean;
		isNewBoInstanceTemplate?: boolean;
	}

	/**
	 * bundled preferences loaded from REST
	 */
	export interface SideviewPreferencesModel {
		sideviewmenuPreferences: any;
		searchtemplatePreferences: any;
	}

	export class SideviewService {

		// TODO this should be removed - right upper expand/collapse button functionality should be replaced by lower left blue arrow-buttons in sideviewmenu
		static showSideviewNavigation: boolean = true;

		constructor(private util: nuclos.util.UtilService,
					private $q: ng.IQService,
					private preferences: nuclos.preferences.PreferenceService,
					private searchtemplateService: nuclos.searchtemplate.SearchtemplateService,
					private boService: nuclos.core.BoService,
					private metaService: nuclos.core.MetaService,
					private dataService: nuclos.core.DataService,
					private sideviewmenuService: nuclos.sideview.SideviewmenuService,
					private $log: ng.ILogService,
					private perspectiveService: PerspectiveService) {
		}


		/**
		 * preferences contains only data of selected attributes + values
		 * metadata contains all attributes (SideviewService.columnLayout)
		 * the searchtempate-directive needs both ot them combined (SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference)
		 */
		private initSearchtemplate(searchtemplatePreference) {

			searchtemplatePreference.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);

			for (let attribute of searchtemplatePreference.content.attributes) {
				for (let allAttributesItem of searchtemplatePreference.content.allAttributes) {
					if (allAttributesItem.boAttrId === attribute.boAttrId) {
						allAttributesItem.value = attribute.value;
						allAttributesItem.operator = attribute.operator;
						allAttributesItem.enableSearch = attribute.enableSearch;
						allAttributesItem.selected = true;

						attribute.boAttrName = allAttributesItem.boAttrName;
						attribute.inputType = allAttributesItem.inputType;
						this.searchtemplateService.setSearchComponent(attribute);
						this.searchtemplateService.formatValue(attribute, this.searchtemplateService.getOperatorDefinition(allAttributesItem));

						attribute.name = this.dataService.getMaster().meta.attributes[allAttributesItem.boAttrName].name;
						attribute.selected = true;

						this.searchtemplateService.setSearchComponent(allAttributesItem);

						if ((allAttributesItem.type === 'Date' || allAttributesItem.type === 'Timestamp') && allAttributesItem.value) {
							allAttributesItem.value = new Date(allAttributesItem.value);
						}
					}
				}
			}

			this.validateSearchForm(searchtemplatePreference.content);
		}


		/**
		 * only selected attributes where saved into DB
		 * so when loading a preference the missing attributes need to be added
		 */
		private addMissingColumnsFromMetadata(preference: IPreference<SideviewmenuPreferenceContent //| SearchtemplatePreferenceContent
			>, metaData: model.interfaces.Meta): void {

			let allColumns: Array<SideviewColumn> = [];

			// add columns which are not already in saved pref from meta data
			for (var a in metaData.attributes) {
				var attribute = metaData.attributes[a];
				var selectedColumList = preference.content.columns.filter((column) => {
					return column.boAttrId === attribute.boAttrId;
				});

				if (selectedColumList.length > 0) {
					// attribute is already saved in preferences - add it
					allColumns.push(selectedColumList[0] as SideviewColumn);
				} else {
					// add attribute from metadata
					if (attribute.boAttrName != 'nuclosStateIcon' && attribute.boAttrName != 'nuclosDeleted' && !attribute.hidden) {
						(<SideviewColumn>attribute).sort = {};
						allColumns.push(attribute);
					}
				}
			}

			preference.content.columns = angular.copy(allColumns);
		}

		/**
		 * load sideviewmenu and searchtemplate prefrences
		 */
		public loadPreferences(boMetaId: string): ng.IPromise<SideviewPreferencesModel> {

			var deferred = this.$q.defer<Object>();

			SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences = [];
			this.sideviewmenuService.emptySideviewmenuPreferences(boMetaId);

			this.metaService.getBoMetaData(boMetaId).then((metaData) => {

				SearchtemplateService.searchtemplateModel.allAttributes = [];
				for (var a in metaData.attributes) {
					var attribute = metaData.attributes[a];
					this.util.setInputType(attribute);
					this.searchtemplateService.setSearchComponent(attribute);

					attribute.searchPopoverOpen = false;

					if (attribute.boAttrName != undefined) {
						if (attribute.boAttrName != 'nuclosStateIcon' && attribute.boAttrName != 'nuclosDeleted' && attribute.inputType != undefined && !attribute.hidden) {
							SearchtemplateService.searchtemplateModel.allAttributes.push(attribute);
						}
					} else {
						// TODO: occurs sometimes - fix
						this.$log.warn('attribute.boAttrName is undefined', attribute);
					}
				}

				var filter: IPreferenceFilter = {
					type: ['table', 'searchtemplate', 'perspective'],
					boMetaId: boMetaId
				};

				this.preferences.getPreferences(filter).then(
					(data) => {


						var perspectiveMenuPreferences = data.filter((pref) => {
							return pref.type === 'perspective';
						}) as Array<IPreference<IPerspective>>;
						if (perspectiveMenuPreferences.length > 0) {
							this.perspectiveService.getModel().setPerspectives(perspectiveMenuPreferences);
						}


						var sideviewMenuPreferences = data.filter((pref) => {
							return pref.type === 'table';
						}) as Array<IPreference<SideviewmenuPreferenceContent>>;

						if (sideviewMenuPreferences.length > 0) {

							SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences = sideviewMenuPreferences;

							if (!this.perspectiveService.getSelectedPerspective()) {
								this.$log.debug('Selecting sideview preferences...');
								SideviewmenuService.sideviewmenuModel.setSelectedSideviewmenuPreference(
									SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.filter((pref) => {
										return pref.content.selected
									})[0]
								);
							} else {
								this.$log.debug('Skipping sideview preferences (perspective is selected)');
								let sideviewPref = SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.filter(
									pref => {
										let prefId = this.perspectiveService.getSelectedSideviewMenuPrefId();
										return pref.prefId === prefId;
									}
								)[0];
								SideviewmenuService.sideviewmenuModel.setSelectedSideviewmenuPreference(sideviewPref);
							}

							// if none is selected select the first
							if (!SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference) {
								SideviewmenuService.sideviewmenuModel.setSelectedSideviewmenuPreference(sideviewMenuPreferences[0]);
							}


							for (let pref of sideviewMenuPreferences) {
								this.addMissingColumnsFromMetadata(pref, metaData);
							}

							var columnLayoutFromPrefs = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference['content'];

							for (let column of SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns) {
								var columnNameFQN = column.boAttrId;
								var columnName = getShortFieldName(this.dataService.getMaster(), columnNameFQN);
								column.nameShort = columnName;
							}

						}

						var searchtemplatePreferences = data.filter(function (pref) {
							return pref.type == 'searchtemplate';
						}) as Array<nuclos.preferences.IPreference<SearchtemplatePreferenceContent>>;

						SearchtemplateService.searchtemplateModel.searchtemplatePreferences = searchtemplatePreferences;

						for (let searchtemplatePreference of searchtemplatePreferences) {
							this.initSearchtemplate(searchtemplatePreference);
						}

						if (searchtemplatePreferences.length > 0) {
							SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(
								SearchtemplateService.searchtemplateModel.searchtemplatePreferences.filter(function (pref) {
									return pref.content.selected
								})[0]
							);
						} else {
							SearchtemplateService.searchtemplateModel.setSelectedSearchtemplatePreference(
								{
									prefId: null,
									type: 'searchtemplate',
									boMetaId: boMetaId,
									content: {
										attributes: []
									} as nuclos.sideview.SearchtemplatePreferenceContent,
									name: ''
								}
							);
						}

						deferred.resolve(
							{
								sideviewmenuPreferences: SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference,
								searchtemplatePreferences: SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference
							}
						);

					},
					(error) => {
						this.$log.error('Unable to get preferences.', error);
						deferred.reject();
					}
				);
			});

			return deferred.promise;
		}

		public validateSearchForm(searchtemplate) {
			if (!searchtemplate) {
				this.$log.warn('No searchtemplate given');
				return;
			}

			this.$log.debug('Validating searchtemplate: %o with %o attributes', searchtemplate, searchtemplate.attributes.length);

			// TODO: "allAttributes" should not be part of the searchtemplate.
			if (!searchtemplate.allAttributes) {
				this.$log.warn('Searchtemplate incomplete, cannot validate. Assuming it is valid.');
				searchtemplate.isValid = true;
				return;
			}

			let searchFormIsValid = true;
			searchtemplate.attributes
				.forEach((attribute, index) => {
					var operatorDef = this.searchtemplateService.getOperatorDefinition(attribute);
					if (operatorDef) {
						if (operatorDef.isUnary) {
							attribute.value = '';
						}

						attribute.isValid =
							operatorDef.isUnary
							||
							(
								attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === 'number'
								||
								attribute.value !== undefined && operatorDef.operator === "=" && attribute.value.id && attribute.inputType === 'reference' // equal search reference with dropdown
								||
								attribute.value !== undefined && operatorDef.operator === "like" && attribute.value.length > 0 && attribute.inputType === 'reference' // equal search reference with dropdown
								||
								attribute.value !== undefined && attribute.value.length === undefined && attribute.inputType === 'date'
								||
								attribute.value !== undefined && attribute.value.length > 0
							);

						if (!attribute.isValid && attribute.enableSearch) {
							searchFormIsValid = false;
						}
					} else {
						if (attribute.enableSearch) {
							searchFormIsValid = false;
						}
					}
					searchtemplate.isValid = searchFormIsValid;
				});
		}

		public search(boMetaId: string, selectFirstFoundEntry: boolean, processMetaId?: string): IPromise<IBoList> {
			this.$log.debug('SideviewService.search(%o)...', SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference);

			var deferred = this.$q.defer<IBoList>();

			// column sorting
			var sortAttributesOrderedByPrio = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns
				.filter(function (item) {
					return item.sort !== undefined && item.sort.prio !== undefined;
				})
				.sort(function (a, b) {
					if (a.sort && b.sort && a.sort.prio < b.sort.prio)
						return -1;
					else if (a.sort && b.sort && a.sort.prio > b.sort.prio)
						return 1;
					else
						return 0;
				});

			var sortStringArray = [];
			sortAttributesOrderedByPrio.forEach(function (column, index) {
				// reasign prio to fix gaps in number range
				column.sort.prio = index + 1;
				sortStringArray.push(column.boAttrId + ' ' + column.sort.direction);

			});

			this.dataService.getMaster().sort = sortStringArray.join(',');


			this.metaService.getBoMetaData(boMetaId).then((meta) => {

				var currentChunksize = this.dataService.getMaster().bos ? this.dataService.getMaster().bos.length : chunkSize;
				if (currentChunksize < chunkSize) {
					currentChunksize = chunkSize;
				}


				let searchtemplate = this.searchtemplateService.getSelectedSearchtemplatePreference();
				let searchtemplateContent = searchtemplate ? searchtemplate.content : undefined;

				// if (processMetaId) {
				//     searchtemplate = null;
				// }

				this.boService.loadBOList(this.dataService.getMaster(), 0, this.getAttributes(meta,
					SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content), currentChunksize, searchtemplateContent).then(
					(data) => {
						this.dataService.getMaster().bos = data['bos'];
						deferred.resolve(data);
					},
					(error) => {
						/*
						 TODO
						 this.dialog.confirm(
						 'Fehlerhafte Suchanfrage',
						 'Sollen die Benutzereinstellungen für ' + this.dataService.getMaster().meta.name + ' gelöscht werden?',
						 () => {
						 this.preferences.deletePreferenceItem(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference).
						 $promise.then(function() {
						 window.location.reload();
						 });
						 },
						 null
						 );
						 */
						deferred.reject(error);
					}
				);
			});
			return deferred.promise;
		}


		/**
		 * used attributes for info and title in sideview menu
		 * plus selected attributes in sideview menu
		 */
		public getAttributes(meta, sideviewmenuPreferences) {

			var titleAttributes = this.util.parseFqns(meta.titlePattern);
			var infoAttributes = this.util.parseFqns(meta.infoPattern);
			var attributes = titleAttributes.concat(infoAttributes);

			var cols = sideviewmenuPreferences.columns
				.filter(function (elem) {
					return elem.selected;
				})
				.map(function (elem) {
					return elem.boAttrId;
				});
			attributes = attributes.concat(cols);

			// remove duplicates
			attributes = attributes.filter(
				function (v, i) {
					return attributes.indexOf(v) == i;
				}
			);
			return attributes.filter(function (attr) {
				return attr !== undefined;
			});
		}


		public prepareSearchtemplateModel(searchtemplateContent: SearchtemplatePreferenceContent) {
			searchtemplateContent.allAttributes = angular.copy(searchtemplateContent.allAttributes);
			searchtemplateContent.attributes.forEach((selectedAttribute) => {
				searchtemplateContent.allAttributes
					.filter(attribute => attribute.boAttrId === selectedAttribute.boAttrId)
					.forEach(attribute => {
						attribute.selected = true;
						attribute.enableSearch = selectedAttribute.enableSearch;
						attribute.operator = selectedAttribute.operator;
						attribute.value = selectedAttribute.value;
					});
			});
			this.validateSearchForm(searchtemplateContent);
		}
	}
}

nuclosApp.service("sideviewService", nuclos.sideview.SideviewService);
