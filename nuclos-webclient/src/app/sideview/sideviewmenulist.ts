module nuclos.sideview {

    import Component = nuclos.util.Component;
	import IPreference = nuclos.preferences.IPreference;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;


    @Component(nuclosApp, 'sideviewmenuList', {
        controllerAs: 'vm',
        templateUrl: 'app/sideview/sideviewmenulist.html',
        bindings: {
            updateStatusbarCallback: '&'
        }
    })
    class SideviewMenuList {

		private entity: nuclos.model.interfaces.BoViewModel;

		private loadingInitialized: boolean;

		private sideviewmenuModel: nuclos.sideview.SideviewmenuModel;

		// column resizing/dragging
		private dragInProgress: boolean = false;
		private resizeColumnInProgress: boolean = false;
		private currentDraggingThIndex;  // count starting from info column
		private currentHoverColumnIndex; // count starting from info column
		private currentResizingColumn;


		// sideview width handling
		private sideviewMenuWidth: number = 200;
		private initialWidth: number;
		private isCollapsed = false;
		private showCollapseButton = true;
		private showInitialWidthLeftButton = false;
		private showInitialWidthRightButton = false;

        private updateStatusbarCallback: Function;

        private sideviewmenuScope = angular.element($('#sideview-navigation')).scope();


        constructor(
			private $cookieStore: ng.cookies.ICookiesService,
			private $timeout: ng.ITimeoutService,
			private $rootScope: ng.IRootScopeService,
			private $window: ng.IWindowService,
			private $q: ng.IQService,
			private $uibModal,
			private restservice,
			private dataService: nuclos.core.DataService,
			private boService: nuclos.core.BoService,
			private metaService: nuclos.core.MetaService,
			private util: nuclos.util.UtilService,
			private localePreferences: nuclos.clientprefs.LocalePreferenceService,
			private sideviewService: SideviewService,
			private sideviewmenuService: nuclos.sideview.SideviewmenuService,
			private errorhandler,
			private $log: ng.ILogService
        ) {

			this.entity = dataService.getMaster();

			this.sideviewmenuModel = SideviewmenuService.sideviewmenuModel;
			this.sideviewmenuModel.onlyInfoColumnSelected = this.sideviewmenuService.onlyInfoColumnSelected();



			this.sideviewmenuScope.$on('sideview-initialized', () => {

				this.sideviewmenuModel = SideviewmenuService.sideviewmenuModel;

				// add scrollbar to attribute popover
				$('#sideview-attributelist-popover .panel-content').css(
					'max-height',
					$('.sideview-list').height() - $('#resizable-columns th').outerHeight() - $('searchtemplate').position().top
				);

				this.initialWidth = $('#sideview-navigation').width();

				// use available width for infocolumn
				if (this.sideviewmenuModel.onlyInfoColumnSelected) {
					$('.cell-content').css('width', '100%');
				}

				this.handleSideviewmenuResize();

			});


			var getSideviewListEntryHeight =
				(
					() => {
						var height;
						return () => {

							if (!height) {
								height = $('.sideview-list-entry').outerHeight();
							}
							return height;
						}
					}
				)();


			// load more data dynamically while scrolling
			var nrOfItemsToPrelaod = 100;
			var sideviewListDiv = $('#sideview-list-div');
			sideviewListDiv.unbind('scroll');
			sideviewListDiv.bind(
				'scroll',
				() => {
					var scrollTop = sideviewListDiv.scrollTop();
					var scrollIndex = scrollTop / getSideviewListEntryHeight();
					var entity = dataService.getMaster();
					if (entity.bos.length < scrollIndex + nrOfItemsToPrelaod) {
						if (!entity.all) {
							var loaded = entity.bos ? entity.bos.length : 0;
							if (!this.loadingInitialized) {
								this.loadingInitialized = true;
								this.loadBoList(loaded).then(() => {
									this.loadingInitialized = false;
								});
							}
						}
					}
				}
			);


			this.sideviewmenuScope.$on('draggable:move', (data, evt) => {
                if (evt.data.position !== undefined) {
					// drag column
					var colIndex = $($(document.elementFromPoint(evt.x, evt.y)).closest('th')).index();
					if (colIndex !== -1) {

						this.currentHoverColumnIndex = colIndex - 1;
						this.dragInProgress = true;
						this.resetDropCssClasses();
						if (this.currentHoverColumnIndex === this.currentDraggingThIndex) {
							return;
						}
						var direction = this.currentHoverColumnIndex < this.currentDraggingThIndex ? 'left' : 'right';
						this.addColumnClass(this.currentHoverColumnIndex, 'drag-column-' + direction);
					}
				}
			});

			this.sideviewmenuScope.$on('draggable:start', (data, evt) => {
				if (evt.data.position !== undefined) {
					this.startColumnDragging(evt);
				}
			});

			this.sideviewmenuScope.$on('draggable:end', (data, evt) => {
				if (evt.data.position !== undefined) {
					this.stopColumnDragging();
				}
			});

			this.$rootScope.$on('sideviewMenuChanged', (event, sideviewMenu: IPreference<nuclos.sideview.SideviewmenuPreferenceContent>) => {
				this.$log.debug('sideview menu changed: %o', sideviewMenu);
				this.selectSideviewmenu(sideviewMenu, false);
				this.metaService.getBoMetaData(this.dataService.getMaster().boMetaId).then((meta) => {
					this.sideviewmenuService.prepareSideviewmenuModel(sideviewMenu.content, meta);
				});
			});
        }


		public masterSelectBoHandler(entity, nextData) {
			this.$rootScope['masterSelectBoHandler'](entity, nextData);
		}


		private loadBoList(offset) {
			let deferred = this.$q.defer();
			this.metaService.getBoMetaData(this.dataService.getMaster().boMetaId).then((meta) => {
				this.boService.loadBOList(
				this.dataService.getMaster(),
				offset,
				this.sideviewService.getAttributes(meta, SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content),
				chunkSize,
				SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content
				).then(
					(data) => {
						data.bos.forEach((bo) => {
							this.boService.updateTitleAndInfo(bo, meta);
						});
						deferred.resolve(data);
					}
				);
			});
			return deferred.promise;
		}


		private storeColumnLayout() {

			if (SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.length == 0) {
				SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.push(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference);
			}

			this.sideviewmenuService.storeColumnLayout().then(
				(sideviewMenuPreferences) => {
				}
			);
		}

		// reorder columns by drag and drop
		private maxColumnPosition() {
			var columnsWithPosition = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns.filter(function(col) {return col.position !== undefined;});
			if (columnsWithPosition.length === 0) {
				return -1;
			}
			return columnsWithPosition.reduce(function(prev, current) {
				return (prev.position > current.position) ? prev : current;
			}).position;
		}

		private fillColumnPositions() {
			var max = this.maxColumnPosition();
			var selectedColumnsWithoutPosition = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns.filter(function(col) {return col.selected === true && col.position === undefined;});
			for (var i in selectedColumnsWithoutPosition) {
				var col = selectedColumnsWithoutPosition[i];
				col.position = max + Number(i) + 1;
			}
		}

		private reorderColumns(source, target) {
			var sourcePosition = source.position;
			var targetPosition = target.position;
			var selectedColumnsSorted = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns
				.filter(function(col) {return col.selected === true;})
				.sort(function(a, b) {return a.position - b.position;});

			if (targetPosition < sourcePosition) {
				for (var i=targetPosition; i<sourcePosition; i++) {
					selectedColumnsSorted[i].position = selectedColumnsSorted[i].position + 1;
					// SideviewService.columnLayout.columns[i].position = SideviewService.columnLayout.columns[i].position + 1;
				}
			} else {
				for (var i=sourcePosition + 1; i<=targetPosition; i++) {
					selectedColumnsSorted[i].position = selectedColumnsSorted[i].position - 1;
					// SideviewService.columnLayout.columns[i].position = SideviewService.columnLayout.columns[i].position - 1;
				}
			}
			source.position = targetPosition;

			this.storeColumnLayout();
		}





		private resetDropCssClasses() {
			$('.drag-column-left').removeClass('drag-column-left');
			$('.drag-column-right').removeClass('drag-column-right');
		}

		private startColumnDragging(event) {
			this.currentDraggingThIndex = $($(document.elementFromPoint(event.x, event.y)).closest('th')).index() - 1;
		}

		private stopColumnDragging() {
			this.resetDropCssClasses();
		}

		private addColumnClass(columnPosition, cssClass) {
			$('.sideview-list-resizable tr .dragable-column:nth-child(' + (columnPosition + 2 /* ignore first column + nth-child starts counting from 1 */ ) + ').dragable-column')
				.addClass(cssClass);
		}


		public columnResizeMousedown(column, $event) {
			if ($($event.target).is('th')) { // click on TH (resizer) not SPAN (column title)

				this.resizeColumnInProgress = true;

				// add resizing class to table
				$($event.target).closest('table').addClass('resizing');
				column.startX = $event.pageX;
				column.htmlElement = $event.target;
				this.currentResizingColumn = column;
				if (column.position !== undefined) {
					this.addColumnClass(column.position, 'resizing');
				}

				column.startWidth = $($event.target).width();
			}
		}

		public columnResizeMousemove($event) {
			if (this.currentResizingColumn !== undefined && this.currentResizingColumn.position !== undefined) {
				var htmlElement = this.currentResizingColumn.htmlElement;
				var columnWidthBeforeResize = $(htmlElement).width();
				var newWidth = this.currentResizingColumn.startWidth+($event.pageX - this.currentResizingColumn.startX);

				// resize header
				$(htmlElement).width(newWidth);

				// resize cell content
				var thIndex = this.currentResizingColumn.position + 1;
				$('.sideview-list-resizable').find('td:nth-child(' + (thIndex + 1) + ') .cell-content').width(newWidth - 10);

				this.handleSideviewmenuResize();
			}
		}


		private getColumnIndex(tdIndex) {
			return tdIndex - (SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.infoColumn.selected ? 1 : 0);
		}

		public sideviewmenuMouseup($event) {
			this.$timeout(() => {
				this.dragInProgress = false;
				this.resizeColumnInProgress = false;
			});

			// handle column drop
			if (this.dragInProgress) {
				var selectedColumns = this.sideviewmenuModel.selectedSideviewmenuPreference.content.columns
					.filter(function(column) { return column.selected; })
					.sort(function(c1, c2) { return c1.position - c2.position; });

				var sourceColumn = selectedColumns[this.getColumnIndex(this.currentDraggingThIndex)];
				var target = selectedColumns[this.getColumnIndex(this.currentHoverColumnIndex)];
				if (target) {
					this.fillColumnPositions();
					this.reorderColumns(sourceColumn, target);
				}
				this.currentDraggingThIndex = undefined;
			}

			// handle column resize
			if (this.currentResizingColumn !== undefined) {

				var columnName = $(this.currentResizingColumn.htmlElement).attr('column-name');

				if (columnName === 'infotitle') {
					this.sideviewmenuModel.selectedSideviewmenuPreference.content.infoColumn.width =  $(this.currentResizingColumn.htmlElement).width();
				} else {
					this.sideviewmenuModel.selectedSideviewmenuPreference.content.columns.forEach((item) => {
						if (item.boAttrId === columnName) {
							item.width = $(this.currentResizingColumn.htmlElement).width();
						}
					});
				}
				this.currentResizingColumn = undefined;

				this.storeColumnLayout();

				// remove resizing class
				$("#sideview-navigation").find(".resizing").removeClass("resizing");
			}
		}


		public handleSideviewmenuResize() {

        	// make last column to use available space
			let table = $('#resizable-columns');
			let tr = $('#resizable-columns tr');
			table.width($('.sideview-list').width());
			let reiszeTh = $($('#resizable-columns th')[$('#resizable-columns th').length - 2]);
			reiszeTh.width(reiszeTh.width() + (table.width() - tr.width()));
		}

		/*
		TODO
		scope.adjustSideviewTableWidth = function () {

			// calculate table width
			var tableWidth =
				scope.sideview.sideviewMenuPreferences.infoColumn.width +
				scope.sideview.sideviewMenuPreferences.columns
				.filter(function (column) {
					return column.selected;
				})
				.reduce(function (a, b) {
					return {width: a.width + (b.width != undefined ? b.width : 0)};
				}).width;

			$("table.sideview-list-resizable").width(tableWidth + 200);
		};
		scope.setOptimalColumnWidth = function() {
			for (var columnNr=0; columnNr < $("table.sideview-list-resizable th").length; columnNr++) {
				var boAttrId = $($("table.sideview-list-resizable th")[columnNr]).attr('column-name');
				if (boAttrId === undefined) {
					continue;
				}
				var startTableWidth = $("table.sideview-list-resizable").width();
				var newWidth = $("table.sideview-list-resizable td:nth-child(" + columnNr + ") span").width();
				if (newWidth !== undefined) {
					newWidth += 80;
					var col = scope.sideview.sideviewMenuPreferences.columns.filter(function(c) {
						return c.boAttrId === boAttrId;
					})[0];
					if (col) {
						col.width = newWidth;
					}
					var oldWidth = $("table.sideview-list-resizable th:nth-child(" + columnNr + ")").width();
					$("table.sideview-list-resizable").width(startTableWidth + newWidth - oldWidth);
					$("table.sideview-list-resizable th:nth-child(" + columnNr + ")").width(newWidth);
				}
			}
			// scope.adjustSideviewTableWidth(); // TODO
		};
		*/

		public displayValue(value) {
			if (value === undefined || value === null) {
				return;
			}

			var result = value;
			if (value.name !== undefined) {
				result = value.name;
			} else if (value instanceof Date || (value.split && value.split('-').length > 2 && moment(value).isValid())) {
				result = this.localePreferences.formatDate(value);
			} else if (typeof(value) === 'boolean') {
				result = '<i class="fa fa' + (value ? '-check' : '') + '-square-o"></i>';
			}
			return result;
		}

		public sort(column) {
			if (this.dragInProgress || this.resizeColumnInProgress) {
				return;
			}

			var sortAttributes = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns
				.filter(function(item) {
					return item.sort && item.sort.prio !== undefined;
				});

			var highestPrio = 0;
			if (sortAttributes.length !== 0) {
				highestPrio = sortAttributes.reduce(function(prev, current) {
					return (prev.sort.prio > current.sort.prio) ? prev : current;
				}).sort.prio;
			}

			if (!column.sort) {
				column.sort = {};
			}
			if (column.sort.direction === 'asc') {
				column.sort.direction = 'desc';
				if (column.sort.prio === undefined) {
					column.sort.prio = highestPrio + 1;
				}
			} else if (column.sort.direction === 'desc') {
				delete column.sort.direction;
				delete column.sort.prio;
			} else {
				column.sort.direction = 'asc';
				if (column.sort.prio === undefined) {
					column.sort.prio = highestPrio + 1;
				}
			}

			this.updateSideviewTable();

			this.storeColumnLayout();
		}


		public selectColumnAttribute(attribute) {
			if (attribute) {
                if (attribute.selected) {
                    attribute.nameShort = getShortFieldName(this.dataService.getMaster(), attribute.boAttrId);
                    this.fillColumnPositions();
                } else {
                    delete attribute.position;
                }
			}
			this.updateSideviewTable();
			this.maximizeInfoColumn();
			this.storeColumnLayout();
		}

		/**
		 * update sideview list view
		 */
		private updateSideviewTable() {

			this.sideviewService.search(this.dataService.getMaster().boMetaId, false).then((data) => {
			});

			// init width of new added columns
			this.sideviewmenuModel.selectedSideviewmenuPreference.content.columns.forEach((item) => {
				if (item.width === undefined) {
					// set column width to width of column header
					item.width = $('[column-name="' + item.boAttrId + '"]').width();
				}
				if (item.width > 500) {
					item.width = 100;
				}
			});
			$('.sideview-list-resizable [column-name="infotitle"]').width(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.infoColumn.width);

			this.updateStatusbarCallback();
		}

		private maximizeInfoColumn() {
			this.$timeout(() => {
				if (this.sideviewmenuService.onlyInfoColumnSelected()) {
					// make info column use complete available width
					$('.sideview-list-resizable [column-name="infotitle"]').width('100%');
				} else {
					$('.sideview-list-resizable').width($('#sideview-list-div').width());
				}
			});
		}


		private openSideviewmenuPopup() {
			SideviewmenuService.sideviewmenuModel.attributeselectionpopupOpen = true;
		}

		/**
		 * Applys the given sideviewmenu prefs.
		 * If updateSelectedFlag is true, the "selected" flag of the preference is set and saved server-side.
		 */
		public selectSideviewmenu(sideviewmenuPreference: IPreference<nuclos.sideview.SideviewmenuPreferenceContent>, updateSelectedFlag: boolean) {

			SideviewmenuService.sideviewmenuModel.setSelectedSideviewmenuPreference(sideviewmenuPreference);

			if (updateSelectedFlag !== false) {
				this.sideviewmenuService.updateSelectedFlag();
			}

			this.updateSideviewTable();
        }

        public editSideviewmenuPref() {
            this.$timeout(() => {
                this.sideviewmenuModel.editSideviewmenu = true;
				this.openSideviewmenuPopup();
                this.$timeout(() => {
                    angular.element('#sideviewmenu-name-input').focus();
                });
            });
        }

        public saveSideviewmenuPref() {
            this.$timeout(() => {
                this.sideviewmenuModel.editSideviewmenu = false;
                this.updatePreferenceName();
            });
        }

        public newSideviewmenuPref() {
            this.$timeout(() => {
                this.sideviewmenuService.newSideviewmenuPref(this.dataService.getMaster().boMetaId);

                this.sideviewmenuModel.editSideviewmenu = true;
				this.openSideviewmenuPopup();
                this.$timeout(() => {
                    angular.element('#sideviewmenu-name-input').focus();
                });
            });
        }

        public deleteSideviewmenuPref() {
            this.sideviewmenuService.deleteSelectedSideviewmenuPref();

			this.updateSideviewTable();
			this.maximizeInfoColumn();
        }

        public updatePreferenceName() {
            this.sideviewmenuModel.selectedSideviewmenuPreference.content.userdefinedName =
                this.sideviewmenuModel.selectedSideviewmenuPreference.name.length > 0
                &&
                this.sideviewmenuService.getDefaultName(this.sideviewmenuModel.selectedSideviewmenuPreference) != this.sideviewmenuModel.selectedSideviewmenuPreference.name
            ;
            this.savePreferences();
        }


		public savePreferences() {
            this.sideviewmenuService.saveSideviewmenuPreference(this.sideviewmenuModel.selectedSideviewmenuPreference);
        }




		private shadeColor1(color, percent) {  //Darken (-percent) or Blend (+percent) Color
			var num = parseInt(color.slice(1),16), amt = Math.round(2.55 * percent), R = (num >> 16) + amt, G = (num >> 8 & 0x00FF) + amt, B = (num & 0x0000FF) + amt;
			return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (G<255?G<1?0:G:255)*0x100 + (B<255?B<1?0:B:255)).toString(16).slice(1);
		}

		public tdClass(data, entity) {
            if (data._flag === 'update') return "tdDirtyClass";
            if (data._flag === 'insert') return "tdNewClass";
            if (data._flag === 'delete') return "tdDelClass";

            var isSelected = entity && entity.selectedBo && entity.selectedBo.boId === data.boId;
            if (isSelected) {

                if (data.rowcolor && (!data.xrowcolor || data.xrowcolor === data.rowcolor)) {
                    data.xrowcolor = this.shadeColor1(data.rowcolor, -12);
                }
                return "tdSelectClass";

            }

            if (data.rowcolor && (!data.xrowcolor || data.xrowcolor !== data.rowcolor)) {
                data.xrowcolor = data.rowcolor;
            }

            return "";
		}

    }

}
