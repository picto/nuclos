module nuclos.sideviewtest {

	import Component = nuclos.util.Component;
	import SideviewService = nuclos.sideview.SideviewService;
	import SideviewmenuService = nuclos.sideview.SideviewmenuService;

	interface DetailRouteParams extends ng.route.IRouteParamsService {
		pk: string;
	}

	@Component(nuclosApp, "sideviewmenu", {
		controllerAs: "vm",
		templateUrl: "app/sideview/sideviewmenu.html"
	})
	class SideviewMenu {

		private authentication = this.$cookieStore.get('authentication');

		private sideviewmenuScope = angular.element($('#sideview-navigation')).scope();

		private entity: nuclos.model.interfaces.BoViewModel;
		private sideviewMenuWidth: number = 200;
		private showSideviewnavigation: boolean;


		// sideview width handling
		private initialWidth: number;
		private showExpandButton: boolean = false;
		private showExpandFullButton: boolean = false;


		// switch sideview menu 
		private sideviewMenuPreferencesMenuSelector;
		private sideviewMenuPreferencesMenuSelectorCurrent;

		// tree
		private master;
		private treedata;
		private treesearchresult;
		private treeOptions;


		constructor(
			private $cookieStore: ng.cookies.ICookiesService,
			private $timeout: ng.ITimeoutService,
			private $rootScope: ng.IRootScopeService,
			private $window: ng.IWindowService,
			private $q: ng.IQService,
			private $uibModal,
			private restservice,
			private dataService: nuclos.core.DataService,
			private boService: nuclos.core.BoService,
			private metaService: nuclos.core.MetaService,
			private util: nuclos.util.UtilService,
			private localePreferences: nuclos.clientprefs.LocalePreferenceService,
			private sideviewService: SideviewService,
			private sideviewmenuService: nuclos.sideview.SideviewmenuService,
			private errorhandler
		) {

			this.master = this.dataService.getMaster();

			// store sideview menu width in preferences (to make it available across browser instances) 
			// and local storage (to avoid flickering and response faster)                 
			
			var width = sideviewmenuService.getSideviewmenuWidthLocal(this.dataService.getMaster().boMetaId);
			if (width) {
				this.sideviewMenuWidth = Number(width);
			}

			if (!this.sideviewMenuWidth) {
				this.sideviewMenuWidth = 200;
			}


			// double click on resize handle to set sideview menu to the best width 
			$('#sideview-navigation .ui-resizable-handle').dblclick(() => {
				var listEntryWidths = $( ".sideview-list-entry" ).map(function(index, entry) {return $(entry).width();});
				var maxListEntryWidth = Math.max.apply(null, listEntryWidths);
				var width = maxListEntryWidth + 40 < $(window).width() ? maxListEntryWidth + 40 : $(window).width() - 20; 
				this.sideviewMenuWidth = width;
				this.handleSideviewmenuResize();
			});



			this.showSideviewnavigation = SideviewService.showSideviewNavigation;

			this.entity = dataService.getMaster();

			var elem: JQuery  = $('#sideview-navigation');
			elem['resizable']();
			
			

			this.addKeyListener();

			// menu buttons
			this.restservice.sideviewMenuSelector().then( (data) => {
				this.sideviewMenuPreferencesMenuSelector = data;
				this.sideviewMenuPreferencesMenuSelectorCurrent = null;
			});


			//tree
			var filterTextTimeout;
			this.sideviewmenuScope.$watch('master.treesearchQuery', (val) => {
				if (filterTextTimeout) {
					$timeout.cancel(filterTextTimeout);
				}
				filterTextTimeout = $timeout( () => {
					var master = this.dataService.getMaster();
					var query = master.treesearchQuery;
					if (!query || query.length < 2) {
						query = "";
					}
					if (query === master.treesearch) {
						return;
					}
					master.treesearch = query + "";
					this.doTreesearch();
				}, 650);
			});

			
			this.treeOptions = {
				dragStart: function(event) {
				console.log('Start dragging', event);
				},
				dragStop: function(event) {
						// This will be never called :(
				//console.log('Stop dragging', event);
				},
				dropped: function(event) {
				console.log('dropped', event, document.elementFromPoint(event.pos.offsetX,event.pos.offsetY));
				}
			};	  	        
		        
			// TODO move: sideviewmenu-directive.js might not be the best place for updating view after state change
			// update next states and layout after state has been changed
			$rootScope.$on('state:changed', () => {
				// scope.masterSelectBoHandler(dataService.getMaster(), dataService.getMaster().selectedBo);
				this.$rootScope['masterSelectBoHandler'](dataService.getMaster(), dataService.getMaster().selectedBo);
			});
			
			


			this.sideviewmenuScope.$on('draggable:move', (data, evt) => {
				if (evt.data.boMetaId !== undefined) {
					// drag tree element
					this.draggableMoveTree(evt.data.boMetaId);
				}
			});



			
			this.sideviewmenuScope.$on('draggable:end', function(e) {
				$('#sideview-list-div').css('overflow','auto');
				$('#detailblock, #detailblock-form').css('z-index','0');

				if (dataService.getMaster().seltab) {
					delete dataService.getMaster().seltab.acceptDraggable;
				}
			});


			this.sideviewmenuScope.$on('sideview-initialized', () => {
				$('#sideview-navigation').width(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.sideviewMenuWidth);
				this.updateSideviewmenuStatusbar();
			});


		}


		public masterSelectBoHandler(entity, nextData) {
			this.$rootScope['masterSelectBoHandler'](entity, nextData);
		}



		private loadBoList(offset) {
			var deferred = this.$q.defer();
			this.metaService.getBoMetaData(this.dataService.getMaster().boMetaId).then((meta) => {
				this.boService.loadBOList(
					this.dataService.getMaster(), 
					offset, 
					this.sideviewService.getAttributes(meta, SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content)
				).then(
					(data) => {
						data.bos.forEach((bo) => {
							this.boService.updateTitleAndInfo(bo, meta);
						});					
						deferred.resolve(data);
					}
				);
			});
			return deferred.promise;
		}
		

		// key up/down handling
		private key(e) {
			var dataService = angular.element(document.body).injector().get('dataService');
			var $rootScope = angular.element(document.body).injector().get('$rootScope');
			
			if (document.activeElement.tagName != 'BODY') { // no sideview key navigation if something else as body is focused
				return;
			}
			
			if ($('.modal').length != 0) { // no sideview key navigation if modal is open 
				return;
			}
			
			var isUp = e.keyCode === 38;
			var isDown = e.keyCode === 40;
			var entity = dataService.getMaster();
			if ((isUp || isDown) && entity.selectedBo && !entity.dirty) {
				var bos = entity.bos as Array<nuclos.model.interfaces.Bo>;
				for ( var i:number=0; i<bos.length; i++) {
					if (entity.selectedBo.boId === bos[i].boId) {
						var nextData = null;
						if (isDown && i < bos.length - 1) {
							nextData = bos[++i];
						} else if (isUp && i > 0) {
							nextData = bos[--i];
						}
						if (nextData) {
							// this.masterSelectBoHandler(entity, nextData);
							$rootScope['masterSelectBoHandler'](entity, nextData);
						}
						break;
					}
				}
			}
		}
	
		private addKeyListener() {
			this.$window.removeEventListener('keydown', this.key);
			this.$window.addEventListener('keydown', this.key);
		}



		
		
		// expand / collapse sideview
		private isSideviewExpanded() {
			var tableWidth = $('.sideview-list-resizable tr').width();
			var sideviewMenuWidth = $('#sideview-navigation').width();
			return tableWidth < sideviewMenuWidth;
		}

		private getNrOfColumns(): number {
			return angular.element('.sideview-list tbody th').length
				-1 // ignore hidden column
				;
		}

		/**
		 * number of visible columns starting from status column
		 */
		private getNrOfVisibleColumns(): number {
			let navWidth = $('#sideview-navigation').width();
			let itemIndex = 0;
			let widthSum = 0;
			let headerElements = angular.element('.sideview-list tbody th').toArray();
			for (let headerElement of headerElements) {
				widthSum += $(headerElement).width();
				if (widthSum > navWidth) {
					return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
				}
				itemIndex++;
			}
			return itemIndex > this.getNrOfColumns() ? this.getNrOfColumns() : itemIndex;
		}

		/**
		 * width of columns starting from status column to column with given columnIndex 
		 */
		private getWidth(columnIndex: number): number {
			let widthSum = 0;
			let headerElements = angular.element('.sideview-list tbody th').toArray().splice(0, columnIndex);

			for (let headerElement of headerElements) {
				widthSum += $(headerElement).width();
			}
			return widthSum;
		}


		public updateSideviewmenuCallback() {
			$('#sideview-navigation').width(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.sideviewMenuWidth);
			this.updateSideviewmenuStatusbar();
		}


		private updateSideviewmenuStatusbar() {
			this.showExpandButton = this.getNrOfVisibleColumns() < this.getNrOfColumns();
			this.showExpandFullButton = this.showExpandButton && this.getNrOfVisibleColumns() > 1; 
		}


		private showNumberOfColumns(numberOfColumns: number) {
			if (numberOfColumns > this.getNrOfColumns()) {
				numberOfColumns = this.getNrOfColumns();
			}
			let newWidth = $($('.sideview-list tbody th')[numberOfColumns]).offset().left - $($('.sideview-list tbody')).offset().left + 10;

			// Don't make the sideview wider than the document
			let documentWidth = $(document).width();
			newWidth = Math.min(documentWidth, newWidth);

			$('#sideview-navigation').width(newWidth);
			this.updateSideviewmenuWidth(newWidth);
		}

		private scrollLeft() {
			$('#sideview-list-div').animate({
				scrollLeft: 0
			}, 500);
		}

		public expandSideview() {
			this.scrollLeft();
			this.showNumberOfColumns(this.getNrOfVisibleColumns() + 1);
		}

		public collapseSideview() {
			this.scrollLeft();
			this.showNumberOfColumns(this.getNrOfVisibleColumns() - 1);
		}

		public expandSideviewFull() {
			this.scrollLeft();
			this.showNumberOfColumns(this.getNrOfColumns());
		}

		public collapseSideviewFull() {
			this.scrollLeft();
			this.showNumberOfColumns(1);
		}


		private storeColumnLayout() {
			this.sideviewmenuService.storeColumnLayout().then(
				() => {
				},
				error => {
					this.errorhandler.show(error.data, error.status);
				}
			);
		}

		public handleSideviewmenuResize() {
			// make last column to use available space
			let table = $('#resizable-columns');
			let tr = $('#resizable-columns tr');
			table.width($('.sideview-list').width());
			let reiszeTh = $($('#resizable-columns th')[$('#resizable-columns th').length - 2]);
			reiszeTh.width(reiszeTh.width() + (table.width() - tr.width()));

			this.updateSideviewmenuWidth($('#sideview-navigation').width());
		}

		private updateSideviewmenuWidth(width) {
			SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.sideviewMenuWidth = width;

			this.updateSideviewmenuStatusbar();

			this.storeColumnLayout();
			
			// let the last column use available width
			var availableWidth = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.sideviewMenuWidth - $('.sideview-list-resizable tr').width();
			if (availableWidth > 0) {
				var selectedColumns = SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns
					.filter(function(column) { return column.selected; })
					.sort(function(c1, c2) { return c1.position - c2.position; });
				
				var lastColumn = selectedColumns[selectedColumns.length - 1]; 
				if (lastColumn) {
					lastColumn.tempWidth = lastColumn.width + availableWidth - 11;
				}
			}			
		}


		public exportBoList() {
			
			// open dialog
			var modalInstance = this.$uibModal.open({
				templateUrl: 'app/sideview/list-export-dialog.html',
				controllerAs: 'vm',
				controller: function($scope, $uibModalInstance, $window, boService, sideviewService, dataService, metaService) {
					
					var vm = this;
					vm.close = function() {
						$uibModalInstance.close();
					};

					vm.executeListExport = () => {

						metaService.getBoMetaData(dataService.getMaster().boMetaId).then( (meta) => {

							boService.boListExport(
								undefined,
								undefined,
								sideviewService.getAttributes(meta, SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content),
								vm.format,
								vm.pageOrientation === 'landscape',
								vm.isColumnScaled
							).then((data)=> {
								vm.exportLink = data.links.export.href;
								$window.location.href = data.links.export.href;
							});
						});

					};
					
				}
			});
		}


		// switch sideview menu 

		public selectSideviewMenuMaster() {
			
			delete this.treedata;

			this.sideviewMenuPreferencesMenuSelectorCurrent = null;
		
			this.metaService.getBoMetaDataByLink(this.entity.metaLink).then((metaData) => {
				this.entity.meta = metaData;
				this.entity.boMetaId = metaData.boMetaId;
				this.entity.searchfilter = metaData.searchfilter;
				this.boService.loadBOList(this.entity, 0);
			});
		}

		public selectSideviewMenu(index) {
			this.sideviewMenuPreferencesMenuSelectorCurrent = this.sideviewMenuPreferencesMenuSelector[index];

			delete this.treedata;

			// load sideview list data
			var metaLink;
			if (this.sideviewMenuPreferencesMenuSelectorCurrent.type === 'searchfilter') {
			
				metaLink = this.sideviewMenuPreferencesMenuSelectorCurrent.links.searchfilter.href;
				
				this.metaService.getBoMetaDataByLink(metaLink).then((metaData) => {
					this.entity.meta = metaData;
					this.entity.boMetaId = metaData.boMetaId;
					this.entity.searchfilter = metaData.searchfilter;
					this.boService.loadBOList(this.entity, 0);
				});
				
			} else if (this.sideviewMenuPreferencesMenuSelectorCurrent.type === 'explorer') {
				this.restservice.tree(this.sideviewMenuPreferencesMenuSelectorCurrent.boMetaId, this.sideviewMenuPreferencesMenuSelectorCurrent.boId).then( (data) => {
					this.treedata = data;
					
					// collapse all
					// hack !!
					this.$timeout(() => {
						var uiTreeScope = angular.element($('[ui-tree]')[0]).scope();
						uiTreeScope['collapseAll']();
					}, 0);
					
					this.prefetchSubnodes(data);

				});
			}
		}




		// tree

		private doTreesearch() {
			/// scope.restHost = restHost; // TODO sollte nicht nötig sein
			var master = this.dataService.getMaster()
			if (master.treesearch==undefined || master.treesearch.length<2 ) {
				master.treesearching=false;
			} else {
				master.treesearching=true;

				this.restservice.treesearch(
					this.sideviewMenuPreferencesMenuSelectorCurrent.boMetaId, 
					this.sideviewMenuPreferencesMenuSelectorCurrent.boId, '*'+master.treesearch+'*'
				).then( 
					(tree) =>{
						this.treesearchresult = tree;
					}
				);
			}
		}
		
		public loadNodeData(nodescope) {
			var nodeId = nodescope.$parent.$modelValue.node_id;

			if (nodescope.$parent.$modelValue.items === null) {
				this.restservice.subtree(nodeId).then( (data) => {
					nodescope.$parent.$modelValue.items = data;
					nodescope.$parent.$modelValue.hasSubnodes = data.length > 0;
					this.prefetchSubnodes(data);
					//nodescope.$parent.expand();
				});
			} else {
				this.prefetchSubnodes(nodescope.$parent.$modelValue.items);
			}
			
		}
		
		private prefetchSubnodes(data) {   
			// prefetch subnodes
			for (var i in data) {
				if (data[i].items === null) {
					this.restservice.subtree(data[i].node_id).then((subnodedata) => {
						data[i].items = subnodedata;
						data[i].hasSubnodes = subnodedata.length > 0;
					});
				}
			}
		}

		public selectTreeNode(nodescope) {
			
			var boMetaId = nodescope.$parent.$modelValue.boMetaId;
			var boId = nodescope.$parent.$modelValue.boId;
			
			this.restservice.loadMetaData(boMetaId, false).then((metaData) => {
				var href = location.href.substring(0, location.href.indexOf('/#/')) + '/#/sideview/' + metaData.boMetaId + '/' + boId;
				this.$window.open(href);
			});
		}

		// quick-fix: change z-index/overflow when dragging to show draggable item outside an overflow:hidden div
		// another solution would be to clone the draggable element while dragging
		private draggableMoveTree(draggableBoMetaId) {
			
			var acceptDraggable = null;
			var submetas = this.dataService.getMaster().selectedBo.submetas;

			var subBos = this.dataService.getMaster().selectedBo.subBos;
			for (var targetBoMetaId in subBos) {

				var targetBoMetaId_FORSUBMETA = targetBoMetaId.substring(0, targetBoMetaId.lastIndexOf('_')); // TODO: move submeta inside subBos - remove this stuff
				
				var boMeta = submetas[targetBoMetaId_FORSUBMETA];
				if (boMeta) {
					if ($('.drag-enter').length != 0) {
						
						var hasReference = false;
						var fields = boMeta.attributes;
						for (var i in fields) {
							var field = fields[i];
							if (field.referencingBoMetaId && field.referencingBoMetaId === draggableBoMetaId) {
								hasReference = true;
							}
						}
						
						// TODO acceptDraggable wird im falschen Element gesetzt - in 'subform' erwartet wird es in 'tab' - deshalb wird hier die CSS-Klasse via jQuery gesetzt
						
						boMeta.acceptDraggable = hasReference;
						acceptDraggable = hasReference;
						$('nc-ui-grid[bometaid="' + targetBoMetaId_FORSUBMETA + '"] .ui-grid-container').addClass(hasReference ? 'accept-draggable' : 'reject-draggable');
					}
				}					
			}
			
			if (acceptDraggable === null) {
				//delete bo.acceptDraggable;
				$('*').removeClass('accept-draggable');
				$('*').removeClass('reject-draggable');
			}

			this.sideviewmenuScope.$apply();	    		
			
			$('#sideview-list-div').css('overflow','visible');
			
			// TODO: führt zu Fehlern, falls aktiviert:
			//$('#detailblock, #detailblock-form').zIndex(-1);
		}

	}
}
