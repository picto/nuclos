/// <reference path="./sideview-model.ts" />

module nuclos.sideview {

	import IPreferenceFilter = nuclos.preferences.IPreferenceFilter;
	import Meta = nuclos.sideview.Meta;
	import IPreference = nuclos.preferences.IPreference;
	import IPromise = angular.IPromise;

    export interface SideviewmenuPreferenceContent {
        userdefinedName? : boolean;
        columns: Array<SideviewColumn>;
        infoColumn: SideviewColumn;
        sideviewMenuWidth?: number;
        selected: boolean;
    }

    export interface SideviewColumn {
        boAttrId?: string;
        name?: string;
        nameShort?: string;
        selected?: boolean;
        width?: number;
        tempWidth?: number;
        position?: number;
        sort?: SideviewColumnSort;
        system?: boolean;
    }

    export interface SideviewColumnSort {
        direction?: string;
        prio?: number;
        enabled?: boolean;
    }


    /**
     * data model for left columns in sideview
     */
	export class SideviewmenuModel {
		sideviewmenuPreferences: Array<nuclos.preferences.IPreference<SideviewmenuPreferenceContent>>;
		selectedSideviewmenuPreference: nuclos.preferences.IPreference<SideviewmenuPreferenceContent>;

		editSideviewmenu: boolean; // if true text input is shown otherwise dropdown is shown
        attributeselectionpopupOpen: boolean;

        onlyInfoColumnSelected: boolean;

		public setSelectedSideviewmenuPreference(pref: IPreference<SideviewmenuPreferenceContent>) {
			this.selectedSideviewmenuPreference = pref;
		}
	}


    export class SideviewmenuService {

        static sideviewmenuModel: SideviewmenuModel;

        constructor(
            private $q: ng.IQService,
            private preferences: nuclos.preferences.PreferenceService,
            private metaService: nuclos.core.MetaService,
            private dataService: nuclos.core.DataService,
			private $log: ng.ILogService
        ) {

            SideviewmenuService.sideviewmenuModel = new nuclos.sideview.SideviewmenuModel();

        }


		public prepareSideviewmenuModel(sideviewmenuPreference: SideviewmenuPreferenceContent, metaData: model.interfaces.Meta) {

			// load all columns from meta data
			for (let attrKey of Object.keys(metaData.attributes)) {
				let attribute = metaData.attributes[attrKey];

				if (attribute.boAttrName !== undefined) {
					if (attribute.boAttrName !== 'nuclosStateIcon' && attribute.boAttrName !== 'nuclosDeleted' && !attribute.hidden) {
						let attributeMeta = metaData.attributes[attribute.boAttrName];
						if (sideviewmenuPreference.columns.filter(col => col.boAttrId === attribute.boAttrId).length === 0) {
							sideviewmenuPreference.columns.push(
								{
									name: attribute.name,
									boAttrId: attribute.boAttrId,
									system: attribute.system,
									sort: {
										enabled: attributeMeta && !attributeMeta.calculated
									}
								}
							);
						}

					}
				} else {
					// TODO: occurs sometimes - fix
					this.$log.warn('attribute.boAttrName is undefined', attribute);
				}
			}
		}

        public emptySideviewmenuPreferences(boMetaId: string) {

            SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference = {
                type: 'table',
                boMetaId: boMetaId,
                content: {
                    infoColumn: {
                        width: 100,
                        selected: true
                    },
                    columns: [],
                    selected: false
                }
            };    

            this.metaService.getBoMetaData(boMetaId).then((metaData) => {
            	this.prepareSideviewmenuModel(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content, metaData);
            });
        }        


        private setSideviewmenuWidth(boMetaId: string, width: number) {
            if (boMetaId !== undefined && width !== undefined) {
                var key = 'sideviewmenu-width-' + boMetaId;
                localStorage.setItem(key, '' + width);
            }
        }

        public getSideviewmenuWidthLocal(boMetaId: string) {
            var key = 'sideviewmenu-width-' + boMetaId;
            return localStorage.getItem(key);
        }


        public storeColumnLayout(): IPromise<IPreference<SideviewmenuPreferenceContent>> {

            var boMetaId = nuclos.sideview.SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.boMetaId;

            var deferred = this.$q.defer<Object>();

            var prefCopy = angular.copy(nuclos.sideview.SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference);


            prefCopy.content.columns = 
                prefCopy.content.columns
                    // save only selected columns
                    .filter(item => item.selected)
                    // sort by position
                    .sort((a, b) => a.position - b.position);

            // fix positions if they are not in correct order
            prefCopy.content.columns
                .filter(function(col) { return col.position !== undefined; })
                .forEach(function(col, index) { col.position = index; })            

            this.setSideviewmenuWidth(boMetaId, nuclos.sideview.SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.sideviewMenuWidth);

            if (!SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.userdefinedName) {
                SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.name = this.getDefaultName(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference);
            }

			this.preferences.savePreferenceItem(prefCopy as IPreference<SideviewmenuPreferenceContent>)
				.then(
					data => {
						if (data !== undefined && data.prefId !== undefined) {
							nuclos.sideview.SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.prefId = data.prefId;
							SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.prefId = data.prefId;
							deferred.resolve(data);
						}
					},
					error => {
						this.$log.error('Unable to save preferences.', error);
						deferred.reject(error);
					}
				);
            return deferred.promise;
        }

        public onlyInfoColumnSelected(): boolean {
            if (!SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.infoColumn || !SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.infoColumn.selected) {
                return false;
            }            
            for (let column of SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.columns) {
                if (column.selected) {
                    return false;
                }
            }
            return true;
        }

        public newSideviewmenuPref(boMetaId) {
            SideviewmenuService.sideviewmenuModel.editSideviewmenu = true;
            this.emptySideviewmenuPreferences(boMetaId);
            this.updateSelectedFlag();
        }


        public getDefaultName(preferenceItem: IPreference<SideviewmenuPreferenceContent>) {
            
            var selectedAttributes = preferenceItem.content.columns.filter(function(elem) {
                return elem.selected;
            });
            
            return selectedAttributes.map(function(a) {
                return a.name;
            }).join(', ');
        }



        public saveSideviewmenuPreference(preferenceItem: IPreference<SideviewmenuPreferenceContent>) {

            var deferred = this.$q.defer<Object>();

            // update preference name with default name if not overridden by user
            if (!preferenceItem.content.userdefinedName) {
                preferenceItem.name = this.getDefaultName(preferenceItem);
            }

            var isNew = !preferenceItem.prefId;

            var preferenceItemCopy = angular.copy(preferenceItem);
            
            var selectedColumns = preferenceItemCopy.content.columns.filter((elem) => {
                return elem.selected;
            });
            preferenceItemCopy.content.columns = selectedColumns;

            if (preferenceItemCopy.name.length > 50) {
                preferenceItemCopy.name = preferenceItemCopy.name.substring(0, 50) + '...';
            }

            this.preferences.savePreferenceItem(preferenceItemCopy).then(
                (data) => {
                    if (data !== undefined && data.prefId !== undefined) {
                        preferenceItem.prefId = data.prefId;
                    }
                    deferred.resolve(preferenceItem);
                },
                (error) => {
                    console.error('Unable to save preferences.', error);
                    deferred.reject(error);
                }
            );


            if (isNew) {
                SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.push(preferenceItem);
            }

            return deferred.promise;
        }


        public updateSelectedFlag() {
            this.doUpdateSelectedFlag(true);
        }

        public resetSelectedFlag() {
            this.doUpdateSelectedFlag(false);
        }


        private setSelectedFlag() {
            SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.content.selected = true;
            this.saveSideviewmenuPreference(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference);
        }

        private doUpdateSelectedFlag(setSelectedFlagOfSelectedPreference: boolean) {

            if (!SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference) {
                return;
            }

            // reset selected flag
            var itemsProcessed = 0;

            // list with 0 or 1 selected preferences
            var selectedPrefList = SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.filter((pref) => {return pref.content.selected});
                
            for (let pref of selectedPrefList) {
                // remove selected flag
                delete pref.content.selected;
                this.saveSideviewmenuPreference(pref).then((updatedPref) => {
                    itemsProcessed ++;
                    if (itemsProcessed == selectedPrefList.length) {
                        // set selected flag
                        if (setSelectedFlagOfSelectedPreference) {
                            this.setSelectedFlag();
                        }
                    }
                });
            }
            if (selectedPrefList.length === 0) {
                if (setSelectedFlagOfSelectedPreference) {
                    this.setSelectedFlag();
                }
            }
        }


        public deleteSelectedSideviewmenuPref() {
            this.preferences.deletePreferenceItem(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference);

            // remove from model
            var elementPositionInArray = SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.map(function(pref) {return pref.prefId; }).indexOf(SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference.prefId);
            SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences.splice(elementPositionInArray,1);

            // select first in list
            SideviewmenuService.sideviewmenuModel.setSelectedSideviewmenuPreference(SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences[0]);

            // if list is empty create a new sideviewmenu pref
            if (!SideviewmenuService.sideviewmenuModel.selectedSideviewmenuPreference) {
                SideviewmenuService.sideviewmenuModel.sideviewmenuPreferences = [];
                this.emptySideviewmenuPreferences(this.dataService.getMaster().boMetaId);
            }
        }
    
    }
}

nuclosApp.service("sideviewmenuService", nuclos.sideview.SideviewmenuService);
