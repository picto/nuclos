module nuclos.sideview {

	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
    import SearchTemplateAttr = nuclos.searchtemplate.SearchTemplateAttr;

	interface ISideviewRouteParams extends ng.route.IRouteParamsService {
		uid: string;
		searchFilterId: string;
		pk: any;
		searchString: string;
		processmetaid: string;
	}

    export class SideViewCtrl {

        private master: BoViewModel;
	    processmetaid: string;

        constructor(
            private $scope,
            private $rootScope, //: ng.IRootScopeService,
            private $routeParams: ISideviewRouteParams,
            private $location, //: ng.ILocationService,
            private $cookieStore: ng.cookies.ICookiesService,
            private $cookies,
            private $timeout: ng.ITimeoutService,
            private $q: ng.IQService,
            private boService: nuclos.core.BoService,
            private metaService: nuclos.core.MetaService,
            private dataService: nuclos.core.DataService,
            private layoutService,
            private sideviewService: nuclos.sideview.SideviewService,
            private searchtemplateService: SearchtemplateService,
            private browserRefreshService: nuclos.detail.BrowserRefreshService,
            private errorhandler,
            private authService: nuclos.auth.AuthService
        ) {

            localStorage.setItem("selectedViewType", "sideview");

            this.$scope.isTaskView = this.$location.path().indexOf("/taskview") === 0;
            this.$scope.mMenuLabel = this.$scope.isTaskView ? 'Tasks' : 'Menu';
            this.$scope.mMenuPath = this.$scope.isTaskView ? '#/tasks' : '#/menu';
            this.$scope.showSearchList = true;
            this.$scope.bMobileView = false;

            let boMetaId : string = this.$routeParams.uid;
            var searchFilterId = this.$routeParams.searchFilterId;

            //TODO: Deliver this by HATEOAS
            var metaLink = restHost + (this.$scope.isTaskView ? '/meta/searchfilter/' : '/boMetas/') + boMetaId;

            // var master = {} as nuclos.model.interfaces.BoViewModel;
            this.master = {} as nuclos.model.interfaces.BoViewModel;
            this.master.boMetaId = boMetaId;
            this.master.metaLink = metaLink;
            this.master.svQuery = "";
            this.master.search = "";
            this.master.searchfilter = searchFilterId;
            this.master.canCreateBo = null;
            this.master.meta = null;

            this.metaService.canCreateBo(this.master.boMetaId).then((data) => {
                this.master.canCreateBo = data;
            });

            this.metaService.getBoMetaData(this.master.boMetaId).then((metaData) => {
                this.master.meta = metaData;
                dataService.setMaster(this.master);
            });

            this.dataService.setMaster(this.master);

            this.$scope.$on('$locationChangeStart', (event, next, current) => {
                //TODO: A logic condition to prevent location change

                //NUCLOS-4910 5.
                var n = next.indexOf('/#/disclaimer/');
                if (n > 0) {
                    var what = next.substring(n + 14);
                    this.authService.showLegalDisclaimer(what);
                    event.preventDefault();
                }
            });


            this.$rootScope['masterSelectBoHandler'] = (entity, row) => {

                var deferred = $q.defer();

                if (dataService.getMaster().dirty) {
                    this.$rootScope.showMasterDirtyPopover();
                    deferred.reject();
                    return deferred.promise;
                }

                var wasTheSameDataSet = entity.selectedBo && entity.selectedBo.boId == row.boId;

                // set master to selected entity
                this.dataService.setMaster(entity);

                this.loadSingleBO(entity, row).then(
                    (bo) => {

                        if (bo) {
                            this.$location.path('/sideview/' + entity.meta.boMetaId + '/' + bo.boId, false);
                            entity.setSelectedBo(bo);
                            //NUCLOS-4439: Display configured title of the selected bo in the tab of the browser.
                            this.$cookieStore.put("tabtitle", bo.title);
                            deferred.resolve(bo);
                        }

                        setTitle($cookieStore);

                        this.$scope.bMobileView = scrollToId('#sideview-details', 320);

                        this.layoutService.createTableForThisBO(entity, bo).then((layoutLoadedActions) => {
                            this.$rootScope.$broadcast('rowselected', {layoutLoadedActions: layoutLoadedActions, bo: bo});

                            // subform tab info
                            this.boService.subformTabInfo(entity.selectedBo);

                            //NUCLOS-4984. See nc-ui-grid-directive.ts
                            //NUCLOS-5067. Reactivate for the same data set (forced refresh)
                            if (wasTheSameDataSet) {
                                this.$rootScope.$broadcast('refreshgrids'); // refresh active subforms
                            }

                        });
                    },
                    (error) => {
                        this.errorhandler.show(error.data, error.status);
                        deferred.reject();
                    }
                );

                return deferred.promise;;
            };

            $scope.showSideviewnavigation = nuclos.sideview.SideviewService.showSideviewNavigation;
            $scope.setShowSideviewnavigation = (show) => {
                this.$scope.showSideviewnavigation = show;
                nuclos.sideview.SideviewService.showSideviewNavigation = show;

                //Nuclos-4113: A way to smart resize the layout.

                var entity = this.dataService.getMaster();
                var bo = entity.selectedBo;

                if (entity.table) {
                    delete entity.table._currentlayout;
                }

                if (bo !== undefined) { // there is no selectedBo if a new tab/popup is opened in expanded view
                    setTimeout(() => {
                        this.layoutService.createTableForThisBO(entity, entity.selectedBo).then((layoutLoadedActions) => {
                            this.$rootScope.$broadcast('rowselected', {layoutLoadedActions: layoutLoadedActions, bo: bo});
                        });
                    }, 50);
                }

                //This following line is disabled, because the detail-block is supposed to use the complete width of the browser
                //$('#detailblock').css('display', show ? 'block' : 'table');
            };

            /*
            * add an event listener which reloads data after a bo was created or modified on another browser tab
            */
            browserRefreshService.addRefreshEventListener(() => {
                this.$scope.masterSelectBoHandler(dataService.getMaster(), dataService.getMaster().selectedBo).then(() => {
                    this.$rootScope.$broadcast('refreshgrids'); // refresh active subforms
                });
            });


            // if the page is called with request parameter expand=true (e.g. for popups), the sideview list on the left will be hidden
            if (this.$location.url().indexOf(nuclos.detail.EXPAND_SIDEVIEW_PARAM) !== -1) {
                this.$scope.setShowSideviewnavigation(false);
            }

            if (this.$location.url().indexOf('refreshothertabs') !== -1) {
                this.browserRefreshService.refreshOtherBrowserInstances();

                // remove from URL
                this.$location.url(this.$location.url().replaceAll('refreshothertabs', ''))
            }

            // load preferences for sideview columns / searchtemplates
            this.sideviewService.loadPreferences(this.master.boMetaId).then((data) => {

				SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference = searchtemplateService.getSelectedSearchtemplatePreference();
				if (SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference) {
					SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes =
						angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);
					this.sideviewService.prepareSearchtemplateModel(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content);
					SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.disabled = true;
				}

				let processChanged = false;

                this.updateSearchAttributesFromUrlSearch();

                var pk = this.$routeParams.pk;
                if (this.processmetaid != this.$routeParams.processmetaid) {
                    processChanged = true
		            this.processmetaid = this.$routeParams.processmetaid;
                }
                var selectFirstFoundEntry = true;

                //NOAINT-613: Open this temporaryBo as new object in order to save.
                var jsonTemporaryBo = localStorage.getItem(pk);
                if (jsonTemporaryBo) {
                    selectFirstFoundEntry = false;
                    var temporaryBo = JSON.parse(jsonTemporaryBo);
                    //TODO: Check if it is necessry. temporaryBo.links.insert.href is set.
                    temporaryBo._flag = 'insert';
                    localStorage.removeItem(temporaryBo.boMetaId);

                    //Open temporaryBO
                    this.boService.temporaryBO(dataService.getMaster(), temporaryBo).then((bo) => {
                        this.dataService.getMaster().setSelectedBo(bo);
                        this.layoutService.createTableForThisBO(this.dataService.getMaster(), bo).then((layoutLoadedActions) => {
                            this.$rootScope.$broadcast('rowselected', { layoutLoadedActions: layoutLoadedActions, bo: bo });
                        });
                    });

                    if (temporaryBo.businessError) {
                        this.errorhandler.show({ message: temporaryBo.businessError }, 412);
                        this.dataService.getMaster().dirty = true;
                    }
                } else if (pk != undefined && this.processmetaid == undefined) {

                    // select bo from URL

                    selectFirstFoundEntry = false;

                    if (
                        pk !== 'new'
                        && pk !== 'noid' // prevent reloading
                    ) {
                        let bo = {} as nuclos.model.interfaces.Bo;

                        bo.boMetaId = this.$routeParams.uid
                        bo.boId = pk;
                        bo.attributes = [];
                        bo.links = {};


                        //TODO: Ugly. And get the links from Server!
                        bo.links = { self: { href: restHost + "/bos/" + this.$routeParams.uid + "/" + pk } };

                        let bos = [bo] as Array<nuclos.model.interfaces.Bo>;
                        this.dataService.getMaster().bos = bos;

                        this.$scope.masterSelectBoHandler(this.dataService.getMaster(), bo);
                    }

                    // scroll to selected bo
                    $timeout(() => {
                        if ($('#smi_' + pk).length != 0) {
                            $('#sideview-list-div').animate({
                                scrollTop: $('#smi_' + pk).offset().top
                            });
                        }
                    }, 0);
                } else if (this.processmetaid != undefined) {

					if (pk == 'new' || pk == 'noid') {
						selectFirstFoundEntry = false;
					}

                    if (processChanged) {
						// let processAttribute : SearchTemplateAttr = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes.filter(function(value, index, array) {
	                    //     return value.boAttrName == "nuclosProcess";
	                    // })[0];
	                    // processAttribute.operator = "=";
						// let processName : string = this.processmetaid.lastIndexOf(boMetaId) == 0 ? this.processmetaid.substring(boMetaId.length + 1) : "";
						// let value = {"id" : this.processmetaid, "name" : processName};
	                    // processAttribute.value = value;
                        // SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.attributes.length = 0;
                        // SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.attributes.push(processAttribute);
						this.searchtemplateService.resetSelectedSearchtemplatePreference(this.processmetaid);
                    }

										// scroll to selected bo
                    $timeout(() => {
                        if ($('#smi_' + pk).length != 0) {
                            $('#sideview-list-div').animate({
                                scrollTop: $('#smi_' + pk).offset().top
                            });
                        }
                    }, 0);
                }

                // execute search
                sideviewService.search(boMetaId, selectFirstFoundEntry, this.processmetaid).then(
                    (data) => {

                        // open first entry in list
                        if (data.bos.length > 0 && selectFirstFoundEntry) {
                            this.dataService.getMaster().selectedBo = data.bos[0];
                            this.$scope.masterSelectBoHandler(dataService.getMaster(), dataService.getMaster().selectedBo);
                        }

						if (pk == 'new') {
							let newButton = angular.element(document.querySelector('#dataEditorNew'));
							var ctrl = newButton.controller('detailButtons');
							ctrl.newBoInstance(SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference);
						}

                        this.$scope.$broadcast('sideview-initialized');
                    },
                    (error) => {
                        this.errorhandler.show(error.message);
                    }
                )

            });

        }


        private updateSearchAttributesFromUrlSearch () {

            var searchString = this.$routeParams.searchString;
            if (searchString) {

                this.searchtemplateService.resetSelectedSearchtemplatePreference();

                var sp = searchString.split(',');
                for (var s=0; s<sp.length; s++) {
                    var searchParameter = sp[s];
                    var paramName = searchParameter.split('=')[0];
                    var paramValue = searchParameter.split('=')[1];
                    if (paramName != undefined && paramValue != undefined) {

                        // var attribute = this.master.meta.attributes[paramName]
                        var attribute = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes[paramName];
                        var attributes = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes.filter((attr) => {
                            return attr.boAttrName == paramName;
                        });

                        if (attributes != undefined && attributes.length != 0) {
                            attribute = attributes[0];
                            if (attribute.inputType=='number') {
                                attribute.value = new Number(paramValue);
                            } else {
                                attribute.value = paramValue;
                            }

                            // Do a LIKE search if the search value starts or ends with a wildcard charater (* or ?)
                            if (/(^[*?])|([*?]$)/.test(attribute.value)) {
                                attribute.operator = 'like';
                            } else {
                                attribute.operator = '=';
                            }

                            var operatorDefsForInputType = SearchtemplateService.searchtemplateModel.operatorDefinitions[attribute.inputType];
                            if (operatorDefsForInputType.length != 0) {
                                var operatorDefs = operatorDefsForInputType.filter(function(elem) {return elem.operator == attribute.operator;});
                                var operatorDef;
                                if (operatorDefs.length != 0) {
                                    operatorDef = operatorDefs[0];
                                } else {
                                    operatorDef = operatorDefsForInputType[0];
                                }
                                attribute.operator = operatorDef.operator;
                                this.searchtemplateService.formatValue(attribute, operatorDef);
                            }

                            attribute.selected = true;
                            attribute.enableSearch = true;
                            attribute.isValid = true;
                            attribute.searchPopoverOpen = false;
                        } else {
                            alert('Wrong attribute names.');
                            // TODO use errorhandler
                        }
                    }
                }
            }
        }

        private loadSingleBO(entity, row): ng.IPromise<nuclos.model.interfaces.Bo> {
            var deferred = this.$q.defer();
            this.boService.loadSingleBO(entity, row, entity.bos).then((bo) => {
                this.metaService.getBoMetaData(this.dataService.getMaster().boMetaId).then( (meta) => {
                    this.boService.updateTitleAndInfo(bo, meta);
                });
                deferred.resolve(bo);
            });
            return deferred.promise;
        };

    }
}




/**
 * makes an HTML element resizable (query-ui)
 * for initialization on-init callback is called
 * after resize on-resize is called
 */
nuclosApp.directive('resizable', function () {
	interface ILocalScope {
		callback: Function,
		onInitCallback: Function
	}
    return {
        restrict: 'A',
        scope: {
            callback: '&onResize',
            onInitCallback: '&onInit'
        },
        link: function postLink(scope: ILocalScope, elem, attrs) {
            if (scope.onInitCallback) {
                scope.onInitCallback();
            }
            elem.on('resizestop', function (event, ui) {
                if (scope.callback) {
                    scope.callback(elem);
                }
            });
        }
    };
});
