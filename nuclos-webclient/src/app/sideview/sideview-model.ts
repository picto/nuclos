module nuclos.sideview {

    import IPreference = nuclos.preferences.IPreference;
    import Dictionary = nuclos.searchtemplate.Dictionary;
    import Operator = nuclos.searchtemplate.Operator;
	export interface Meta {
        allAttributes: Array<nuclos.model.interfaces.BoAttr>;
        operatorDefinitions: Dictionary<string, Array<Operator>>;
    }
}