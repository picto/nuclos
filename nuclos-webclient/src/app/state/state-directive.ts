module nuclos.state {

	interface IStateScope extends angular.IScope {
		imageResourceURL: Function;
		changeState: Function;
		bos: any;
		dialog: any;
		stateChangeLabel: any;
	}

	nuclosApp.directive('states', function ($uibModal, $q, stateService, errorhandler) {
		return {
			restrict: 'E',
			replace: false,
			templateUrl: 'app/state/states.html',
			scope: {
				bos: '<',
				showCurrentState: '<',
				availableStates: '<',
				currentState: '<'
			},
			link: function (scope: IStateScope, element, attrs) {

				scope.imageResourceURL = function (uid) {
					return nuclosServer + "/rest/data/resource/" + uid;
				};

				var addOrRepaceCssClass = function (className, definition) {
					// remove css class definition
					$('#' + className + '-class').remove();
					// add css class definition
					$("<style type='text/css' id='" + className + "-class'> ." + className + "{ " + definition + "} </style>").appendTo("head");
				}

				var disableButtons = function () {
					var disabledButtons = [];

					var btns = $(element).find('.btn-default');
					for (var c in btns) {
						var list = btns[c].classList;
						if (list && list.contains('btn-default') && !list.contains('disabled')) {
							list.add('disabled');
							disabledButtons.push(btns[c]);
						}
					}

					return disabledButtons;
				}

				var enableButtons = function (btns) {
					for (var c in btns) {
						var list = btns[c].classList;
						if (list && list.contains('btn-default') && list.contains('disabled')) {
							list.remove('disabled');
						}
					}
				}

				scope.changeState = function (newState) {

					//Disable all enabled buttons
					var disabledButtons = disableButtons();

					// multiple bos can be selected in tableview
					var firstBo = scope.bos[0];

					var currentStateId = firstBo.attributes.nuclosState.id;
					var newStateId = newState.nuclosStateId;

					var changeStateFunction = function () {

						stateService.changeState(scope.bos, newStateId).then(function (data) {
							enableButtons(disabledButtons);

						}, function (error) {
							enableButtons(disabledButtons);
							errorhandler.show(error.data, error.status);

						});
					}

					if (newState.nonstop) {
						changeStateFunction();
						return;
					}

					var $scope = scope;

					// combine statusinfo rest service response for current and new state
					$q.all([stateService.statusinfo(currentStateId), stateService.statusinfo(newStateId)]).then(function (res) {

						var currentStateInfo = res[0];
						var newStateInfo = res[1];

						// open dialog
						var modalInstance = $uibModal.open({
							templateUrl: 'app/state/change-state-dialog.html',
							controller: function ($scope, $uibModalInstance, dialog) {
								$scope.dialog = dialog;
								$scope.ok = function () {
									$uibModalInstance.close();
								};
								$scope.cancel = function () {
									$uibModalInstance.dismiss('cancel');
								};
							},
							resolve: {
								dialog: function () {
									if (currentStateInfo.buttonIcon != null) {
										currentStateInfo.buttonIconResourceURL = $scope.imageResourceURL(currentStateInfo.buttonIcon);
									}
									if (newStateInfo.buttonIcon != null) {
										newStateInfo.buttonIconResourceURL = $scope.imageResourceURL(newStateInfo.buttonIcon);
									}

									$scope.dialog = {};
									$scope.dialog.header = $scope.stateChangeLabel;
									$scope.dialog.message = newStateInfo.description;
									$scope.dialog.currentStateInfo = currentStateInfo;
									$scope.dialog.newStateInfo = newStateInfo;

									var gradientCss;
									if (window['Modernizr'].smil) {
										// not IE
										gradientCss =
											"background: linear-gradient(to right, " + $scope.dialog.currentStateInfo.color + ", " + $scope.dialog.newStateInfo.color + ");";
									} else {
										// IE
										gradientCss =
											"background: -webkit-gradient(linear, left top, left bottom, from(" + $scope.dialog.currentStateInfo.color + "), to(" + $scope.dialog.newStateInfo.color + "));" +
											"background-image: linear-gradient(to right, " + $scope.dialog.currentStateInfo.color + " 0%, " + $scope.dialog.newStateInfo.color + " 100%);";
									}
									addOrRepaceCssClass('change-state-gradient', gradientCss);

									return $scope.dialog;
								}
							}
						});

						modalInstance.result.then(function (selectedItem) {
							// ok
							changeStateFunction();

						}, function () {
							// cancel
							enableButtons(disabledButtons);

						});

						modalInstance.opened.then(function () {
							// focus on primary button
							$('.btn-primary').focus();
						});

					});

				};

			}
		}
	});
}



