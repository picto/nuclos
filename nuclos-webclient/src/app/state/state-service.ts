module nuclos.state {
	/**
	 * provides state information for a given business object
	 * state data can be cached until it is changed via the State-Model-Editor
	 */
	class StateService {
		constructor(
			private $resource: ng.resource.IResourceService,
			private $q: ng.IQService,
			private $rootScope, //: ng.IScope,
			private $http: ng.IHttpService,
			private dataService: nuclos.core.DataService,
			private boService: nuclos.core.BoService,
			private browserRefreshService: nuclos.detail.BrowserRefreshService
		) {
		}

		statusinfo(uid) {
			var deferred = this.$q.defer();
			this.$http.get(restHost + '/data/statusinfo/' + uid)
				.success(data => {
					deferred.resolve(data);
				})
				.error((jqxhr, textStatus) => {
					console.warn("Unable to get statusinfo.");
					deferred.reject(false);
				});
			return deferred.promise;
		}

		changeState(bos, newStateId) {
			var deferred = this.$q.defer();
			var boscount = bos.length;
			var result = {
				suceeded: [],
				errors: []
			}

			//TODO: Remove this dirty (Backup) Stuff. A bo should be saved without this dirty flag, too.
			var entity = this.dataService.getMaster();
			var dirtyBackup = entity.dirty;

			for (var i in bos) {
				var bo = bos[i];
				bo.receiverId = this.$rootScope.browserTabId;

				this.$resource(bo.links.self.href).get(data => {
					var newState;
					for (var j in data.nextStates) {
						var state = data.nextStates[j];
						if (state.nuclosStateId === newStateId) {
							newState = state;
							break;
						}
					}

					bo.oldState = bo.attributes.nuclosState;

					bo.attributes.nuclosState = {
						id: newState.nuclosStateId,
						name: newState.name
					};

					entity.dirty = true;

					this.boService.saveBo(bo, entity, true).then(bo2 => {
						result.suceeded.push(bo2);
						if (--boscount == 0) {
							if (result.errors.length === 0) {
								deferred.resolve(result);
							} else {
								entity.dirty = dirtyBackup;
								deferred.reject(result);
							}

							this.browserRefreshService.refreshOtherBrowserInstances();
						}

					}, error => {
						result.errors.push(error);
						if (--boscount == 0) {
							entity.dirty = dirtyBackup;
							deferred.reject(result);
						}

					});

				}, error => {
					result.errors.push(error);
					if (--boscount == 0) {
						entity.dirty = dirtyBackup;
						deferred.reject(result);
					}

				});
			}

			return deferred.promise;
		}
	}

	nuclosApp.service('stateService', StateService);
}
