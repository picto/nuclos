module nuclos.generation {
	class GenerationService {
		boGenerationCache;

		constructor(private $q,
					private $cacheFactory,
					private $resource,
					private errorhandler: nuclos.Errorhandler) {
			//TODO: Invalidation if metaData has been altered in server
			this.boGenerationCache = $cacheFactory('boGenerationCache');
		}

		clearCache() {
			this.boGenerationCache.removeAll();
		}

		getNewBoImpl(generationLink) {
			var deferred = this.$q.defer();

			var boGenerationFromCache = this.boGenerationCache.get(generationLink);
			if (boGenerationFromCache) {
				deferred.resolve(angular.copy(boGenerationFromCache));
				return deferred.promise;
			}

			this.$resource(generationLink).get(
				newBo => {
					this.boGenerationCache.put(generationLink, newBo);
					deferred.resolve(angular.copy(newBo));
				},
				error => {
					this.errorhandler.show(error.data, error.status);
					deferred.reject();
				}
			);

			return deferred.promise;
		}

		getNewBo(boMeta) {
			var newBo = this.getNewBoImpl(boMeta.links.defaultGeneration.href);
			return newBo;
		}

		doGenerateBo(generations, popupparameter) {

			var deferred = this.$q.defer();

			var generation = generations.selected;

			//NUCLOS-4448
			if (!generation) {
				generation = generations.oldSelected;
			}

			this.$resource(generation.links.generate.href).save(generations, data => {

				var refreshSource = data.refreshSource;

				if (data.complete) {
					if (data.showGenerated) {
						//TODO: Find a generic way to get this link, similar to
						//var newBoLink = data.bo.links.self.href;
						var newBoLink = "#/sideview/" + data.bo.boMetaId + "/" + data.bo.boId
							+ "?" + nuclos.detail.EXPAND_SIDEVIEW_PARAM
							+ "&" + nuclos.detail.SHOW_CLOSE_ON_SAVE_BUTTON_PARAM
							+ "&refreshothertabs";
						window.open(newBoLink, '', popupparameter);
					}

				} else {
					//TODO: Somehow there should be an "InputRequiredException" or similar
					//In data.bo there is an incomplete bo that needs one or more fields to be filled out.
					//Then it should be able to be saved.

					//TODO: Open the incomplete data.bo in a new tab, the controller for the boMetaId and
					//a "new bo" but not really new but the bo that has been returned.

					data.bo.businessError = data.businessError;

					localStorage.setItem(data.bo.temporaryId, JSON.stringify(data.bo));
					var newBoLink =
						"#/sideview/" + data.bo.boMetaId + "/" + data.bo.temporaryId
						+ "?" + nuclos.detail.SHOW_CLOSE_ON_SAVE_BUTTON_PARAM;
					window.open(newBoLink, '', popupparameter);

				}
				deferred.resolve(generations);


			}, error => {

				// not ok - maybe inputrequired
				if (error.data.inputrequired) {
					if (!generations.inputrequired) {
						generations.inputrequired = {};
					}
					generations.inputrequired.specification = error.data.inputrequired.specification;

					deferred.reject(generations);
				} else {
					this.errorhandler.show(error.data, error.status);
				}

			});

			return deferred.promise;
		}
	}

	nuclosApp.service('boGenerationService', GenerationService);
}
