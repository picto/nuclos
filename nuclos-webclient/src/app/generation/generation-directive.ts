module nuclos.generation {

	interface IGenerationScope {
		imageResourceURL: Function;
		generate: Function;
		master: any;
	}

	nuclosApp.directive('generations', function (
		$cookieStore,
		metaService,
		inputrequiredService,
		boGenerationService,
		dialog,
		nuclosI18N: nuclos.util.I18NService
	) {
		return {
			restrict: 'E',
			replace: false,
			templateUrl: 'app/generation/generations.html',
			scope: {
				master: '<',
				availableGenerations: '<'
			},
			link: function (scope: IGenerationScope, element, attrs) {

				scope.imageResourceURL = function (uid) {
					return nuclosServer + "/rest/data/resource/" + uid;
				};

				scope.generate = function (generations) {

					scope.master.generations = generations;

					inputrequiredService.setSaveCallback(doGenerate);
					inputrequiredService.setRefreshCallback(function () {
						//Resetting combo-box
						scope.master.generations.selected = undefined;
					});

					var doGenerate = function () {
						boGenerationService.doGenerateBo(scope.master.generations).then(
							function () {
								// generation was successful
							},
							function (generations) {
								// generation was not successful
								inputrequiredService.processSaveDataRecursive(generations);
							}
						);

						//NUCLOS-4448 Keep track of the selection in case an input-requiredment exception will be thrown.
						generations.oldSelected = generations.selected;

						//NOAINT-621 Resetting combo-box also when generation has been confirmed
						generations.selected = undefined;
					};

					if (generations.selected && generations.selected.nonstop) {
						doGenerate();
						return;
					}

					// multiple bos can be selected in tableview
					var firstBo = scope.master.selectedBo ? scope.master.selectedBo : scope.master.selectedRows[0];
					var targetBoName = generations.selected.target;

					var $scope = scope;
					metaService.getBoMetaData(firstBo.boMetaId).then(function (metaData) {
						var sourceBoName = metaData.name;

						// open dialog
						var headerTemplate = nuclosI18N.getI18n("webclient.dialog.generate.header");
						var header = headerTemplate.replace('{0}', targetBoName);

						var messageTemplate = nuclosI18N.getI18n("webclient.dialog.generate.message");
						var message = messageTemplate.replace('{0}', sourceBoName).replace('{1}', targetBoName);

						dialog.confirm(header, message,
							function () {
								// ok
								doGenerate();

							}, function () {
								// cancel

								//Keeping track of the selection before
								generations.oldSelected = generations.selected;
								//Resetting combo-box
								generations.selected = undefined;
							}
						);

					});
				};

			}
		};
	});
}
