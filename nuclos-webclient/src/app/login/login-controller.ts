module nuclos.login {

	interface ILocaleParams extends ng.route.IRouteParamsService {
		locale: string;
	}

	export class LoginCtrl {

		private username;
		private password;
		private showMandatorlessSessionButton;
		private locales;
		private autologin;
		private registration;
		private cookiesaccepted;
		private navoffset;
		private legalDisclaimers;
		private hasDatenschutz;
		private restservices;

		constructor(private $routeParams: ILocaleParams,
					private $cookieStore: ng.cookies.ICookiesService,
					private $cookies,
					private $rootScope,
					private $location,
					private restservice,
					private $timeout,
					private metaService,
					private layoutService,
					private menuService,
					private authService: nuclos.auth.AuthService,
					private dialog,
					private preferences,
					private dashboardService,
					private nuclosI18N: nuclos.util.I18NService,
					private errorhandler: nuclos.Errorhandler
		) {

			this.username = this.$cookieStore.get('authentication') ? this.$cookieStore.get('authentication')['username'] : undefined;
			this.locales = this.nuclosI18N.locales;
			this.autologin = false;
			this.registration = this.$rootScope.isUserRegistrationEnabled;
			this.cookiesaccepted = this.$cookies.get('cookiesaccepted');
			this.navoffset = this.cookiesaccepted ? 0 : 32;

			setTitle(null);
			console.log("username" + this.username);
			if (this.authService.hasSessionId() && this.username != "anonymous") {
				window.location.href = defaultMenu;
				return;
			}

			if (this.$routeParams.locale && this.$routeParams.locale.length <= 5) {
				this.nuclosI18N.setLocale(this.$routeParams.locale);
			}

			// focus username input
			this.$timeout(() => {
				if ($('#username').val() == 0) {
					$('#username').focus();
				} else {
					$('input[type="password"]').focus();
				}
			}, 100);

			this.authService.clearCache(),
				this.metaService.clearCache();
			this.layoutService.clearCache();
			this.menuService.clearCache();
			this.preferences.clearCache();
			this.dashboardService.unregisterAllWidgets();


			this.authService.getLegalDisclaimers().then((data) => {
				this.legalDisclaimers = data.length > 0 ? data : undefined;
				this.hasDatenschutz = this.authService.hasLegalDisclaimer("Datenschutz", data);
			});

		}

		acceptCookies() {
			this.$cookies.put('cookiesaccepted', 'true');
			this.cookiesaccepted = true;
		};

		showPrivacyContent = () => {
			this.authService.showLegalDisclaimer('Datenschutz');
		};


		private loginData;
		private showLoginForm = true;
		private showMandatorForm = false;
		private selectedMandatorId;

		authenticate() {

			this.authService.invalidateSessionId();

			this.username = this.username;
			var loginData = {
				"username": this.username,
				"password": this.password,
				"locale": this.nuclosI18N.selectedLocale,
				"autologin": this.autologin
			};

			this.authService.authenticate(loginData).then(
				(data) => {
					// login successful

					this.loginData = data;

					delete this.loginData.mandator;

					this.showMandatorlessSessionButton = data.superUser;

					if (data.mandators) {
						this.showLoginForm = false;
						this.showMandatorForm = true;
						this.selectedMandatorId = data.mandators[0].mandatorId;
					} else {
						this.handleRedirect();
					}
				}
				,
				(error) => {
					//SHOP-184 - Wrong try must invalidate any existing session id
					this.authService.invalidateSessionId();

					var textStatus = error.status;
					var jqxhr = error.data;

					this.password = '';
					if (textStatus == "406") {

						var title = this.nuclosI18N.getI18n("webclient.nosession.login.title");
						var text = this.nuclosI18N.getI18n("webclient.nosession.error.wrongpass");

						this.dialog.alert(title, text);

					} else {
						this.errorhandler.show(jqxhr, textStatus, this.$cookieStore);
					}
				}
			);
		}


		selectMandator() {

			let mandator = this.loginData.mandators.filter( mandator => mandator.mandatorId === this.selectedMandatorId).shift();
			if (mandator) {
				this.authService.selectMandator(mandator).then(
					() => {
						this.loginData.mandator = mandator;
						this.handleRedirect();
					},
					(error) => {
						this.errorhandler.show(error.data, error.status, this.$cookieStore);
					}
				);
			} else {
				this.handleRedirect();
			}
		}

		mandatorlessSession() {
			this.handleRedirect();
		}

		handleRedirect() {
			this.authService.lastLocationRedirect(this.loginData).then(
				() => {

					setTitle(this.$cookieStore);

					//NUCLOS-4922
					if (this.loginData.initialEntity) {
						window.location.href = '/#/sideview/' + this.loginData.initialEntity;
					}

					this.restservice.developmentMode().then(data => {
						developmentMode = data;

						if (developmentMode) {
							// load this.restservices for rest-call-history
							this.restservice.restservicelist().then(data => {
								this.restservices = data;
								this.$rootScope.restservices = data;
							});
						}
					});

				}
			);
		}

		switchLocale(locale) {
			this.nuclosI18N.setLocale(locale.key);
			this.locales = this.nuclosI18N.locales;
			this.$location.path('login/' + locale.key);
		}

		about(ld) {
			this.dialog.alert(null, ld.text, null, 'halfwidth-modal-window');
		}


	}

	export class LogoutCtrl {
		constructor(private $http,
					private $cookieStore,
					private $location,
					private authService) {

			this.$http.delete(restHost).success(() => {
				this.logout();
			}).error(() => {
				this.logout();
			});
		}

		logout() {
			this.authService.invalidateSessionId();
			this.$location.path('login');
		}

	}
}
