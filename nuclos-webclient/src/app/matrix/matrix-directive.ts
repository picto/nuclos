module nuclos.matrix {

	// TODO: Correct typing
	import INuclosGridOptions = nuclos.grid.INuclosGridOptions;
	import INuclosColumnDefOf = nuclos.grid.INuclosColumnDefOf;
	interface IMatrixScope extends nuclos.grid.INcUiGridScope {
		config: any;
		loadDataInProgress: any;
		bo: any;
		isLoaded: any;
		data: any;
		changeState: any;
		getValue: any;
	}

	nuclosApp.directive('matrix', function (restservice, dataService, boService) {
		return {
			restrict: 'E',
			replace: false,
			templateUrl: 'app/matrix/matrix.html',
			scope: {
				bo: "=",
				height: "@",
				width: "@",
				config: "=",
				data: "=",
				tabindex: "="
			},
			compile: function compile(tElement, tAttributes) {
				return {
					pre: function preLink(scope: IMatrixScope, element, attributes) {

						scope.gridOptions = {
							virtualizationThreshold: Infinity // disable virtualization to make dropdowns work correct while scrolling
						} as INuclosGridOptions<any>;

						scope.gridOptions.columnDefs = [];

						scope.gridOptions.data = [];

						var master = dataService.getMaster();

						var matrixConfig = scope.config;

						var matrixValueType;

						var entity_field_matrix_parentShort = getShortFieldName({boMetaId: matrixConfig.entity_matrix}, matrixConfig.entity_field_matrix_parent);
						var entity_field_matrix_x_refFieldShort = getShortFieldName({boMetaId: matrixConfig.entity_matrix}, matrixConfig.entity_field_matrix_x_refField);
						var entity_matrix_value_fieldShort = getShortFieldName({boMetaId: matrixConfig.entity_matrix}, matrixConfig.entity_matrix_value_field);
						var entity_matrix_reference_fieldShort = getShortFieldName({boMetaId: matrixConfig.entity_matrix}, matrixConfig.entity_matrix_reference_field);

						scope.loadDataInProgress = false;
						var init = function () {
							scope.loadDataInProgress = true;

							matrixConfig.entity = scope.bo.boMetaId;
							restservice.matrix(scope.bo, matrixConfig).then(function (matrixData) { // ok
								scope.isLoaded = true;
								scope.data = matrixData;

								if (!scope.bo.subBos[matrixConfig.entity_y_parent_field]) {
									scope.bo.subBos[matrixConfig.entity_y_parent_field] = {bos: []};
								}
								scope.bo.subBos[matrixConfig.entity_y_parent_field].bos = matrixData.bos;

								matrixValueType = matrixConfig.cell_input_type;

								// TODO change matrix-directive to not relad after save, this implies that created xref entries have to be updated in the model (new created id of bo)
								//scope.bo._reloadTab = matrixData.entity_matrix_reference_field != null && matrixData.entity_matrix_reference_field.length != 0;
								scope.bo._reloadTab = true;

								var cellTemplate =
									"<div> " +
									"	<input " +
									"		ng-model='grid.appScope.data.entityMatrixBoDict[col.field + \"_\" + row.entity.boId].value' " +
									"		ng-change='grid.appScope.changeState(col.field, row.entity.boId, grid.appScope.data.entityMatrixBoDict[col.field + \"_\" + row.entity.boId].value)' " +
									/*
									 "		type='number' " +
									 "		class='number' " +
									 */
									"	></input> " +
									"</div>";
								switch (matrixValueType) {
									case 'checkicon':
										cellTemplate =
											"<div> " +
											"	<div ng-click='grid.appScope.changeState(col.field, row.entity.boId)' class='matrixCell matrixCellCheckIcon'> " +
											"		<span class='glyphicon glyphicon-plus-sign' ng-show='grid.appScope.getValue(col.field, row.entity.boId) == 1'></span> " +
											"		<span class='glyphicon glyphicon-minus-sign' ng-show='grid.appScope.getValue(col.field, row.entity.boId) == 2'></span> " +
											"	</div> " +
											"</div>";
										break;
									case 'combobox':
										cellTemplate =
											"<select " +
											"ng-model=\"grid.appScope.data.entityMatrixBoDict[col.field + '_' + row.entity.boId].value.id\" " +
											"ng-change=\"grid.appScope.changeState(col.field, row.entity.boId, grid.appScope.data.entityMatrixBoDict[col.field + '_' + row.entity.boId].value.id)\"> " +
											"<option value=''></option> " +
											"<option ng-repeat='matrixValue in grid.appScope.data.matrixValues' " +
											"	ng-selected=\"{{matrixValue.id == grid.appScope.data.entityMatrixBoDict[col.field + '_' + row.entity.boId].value.id}}\" " +
											"	value='{{matrixValue.id}}'>{{matrixValue.name}}</option> " +
											"</select>";

										break;
								}

								scope.gridOptions.columnDefs = [];
								scope.gridOptions.columnDefs.push({
									field: 'firstColumn',
									pinnedLeft: true,
									enablePinning: true,
									width: 200,
									headerCellTemplate: '<div></div>'
								} as INuclosColumnDefOf<any>);
								scope.gridOptions.data = [];
								for (var c = 0; c < matrixData.xAxisBoList.length; c++) {
									var column = matrixData.xAxisBoList[c];

									var headerCellTemplate = '<div>' + (column.evaluatedHeaderTitle != null ? column.evaluatedHeaderTitle : column._title) + '</div>';
									scope.gridOptions.columnDefs.push({
										field: column.boId,
										name: column.boId,
										width: 150,
										enablePinning: false,
										headerCellTemplate: headerCellTemplate,
										cellTemplate: cellTemplate
									} as INuclosColumnDefOf<any>);
								}
								for (var r = 0; r < matrixData.yAxisBoList.length; r++) {
									var row = matrixData.yAxisBoList[r];

									var tableRow = {
										firstColumn: row.evaluatedHeaderTitle != null ? row.evaluatedHeaderTitle : row._title,
										boId: row.boId
									};

									for (var c = 0; c < matrixData.xAxisBoList.length; c++) {
										var column = matrixData.xAxisBoList[c];
										tableRow[column.boId] = row.boId + "/" + column.boId;
									}
									(<Array<any>> scope.gridOptions.data).push(tableRow);
								}
								scope.loadDataInProgress = false;

							}, function () { // not ok
								console.error("Unable to get matrix data.", arguments);
							});
						};

						init();


						scope.$on('refreshgrids', function () {
							init();
						});

						scope.$on('$locationChangeStart', function () {
							init();
						});


						scope.gridOptions.onRegisterApi = function (gridApi) {
							scope.gridApi = gridApi;

							(<IMatrixScope>scope.gridApi.grid.appScope).getValue = function (xAxisBoId, yAxisBoId) {
								var cell = (<IMatrixScope>scope.gridApi.grid.appScope).data.entityMatrixBoDict[xAxisBoId + "_" + yAxisBoId];
								return cell !== undefined ? cell.value : null;
							};


							/**
							 * changes the state of the given cell
							 * if no value is set (no dropdown), the existing value will be incremented
							 */
							(<IMatrixScope>scope.gridApi.grid.appScope).changeState = function (columnPK, rowPK, value) {

								if (scope.loadDataInProgress) {
									return;
								}

								columnPK = parseInt(columnPK);

								master.dirty = true;

								scope.bo.evictMatrixCache = true;

								var key = columnPK + "_" + rowPK;
								var matrixCell = scope.data.entityMatrixBoDict[key];
								if (matrixCell != null && matrixCell.attributes != null) { // existing matrix cell - increment or set matrix cell value
									if (matrixValueType == 'combobox' || matrixValueType == 'number') {
										// matrixCell.value is set in select directive (matrix.html)
									} else if (matrixValueType == 'checkicon') {
										matrixCell.value++;
										if (matrixConfig.entity_matrix_number_state != null && matrixCell.value >= matrixConfig.entity_matrix_number_state) {
											matrixCell.value = 0;
										}
									}

									// update model
									for (var i = 0; i < scope.data.bos.length; i++) {
										var row = scope.data.bos[i];
										if (row.boId == rowPK) {
											for (var j = 0; j < row.subBos[matrixConfig.entity_field_matrix_parent].bos.length; j++) {
												var column = row.subBos[matrixConfig.entity_field_matrix_parent].bos[j];
												if (column.attributes[entity_field_matrix_x_refFieldShort].id == columnPK) {
													if (matrixValueType == 'combobox') {
														column.attributes[entity_matrix_reference_fieldShort] = {
															id: parseInt(value),
															name: "dummy1"
														};
													} else if (matrixValueType == 'number' || matrixValueType == 'checkicon') {
														column.attributes[entity_matrix_value_fieldShort] = matrixCell.value;
													}

													if (
														matrixValueType == 'number' && value == null
														||
														(matrixValueType != 'number' && (matrixCell.value == 0 || (value != null && value.length == 0)))

													) { // delete empty cells
														if (column.boId == null) { // column hasn't been saved
															row.subBos[matrixConfig.entity_field_matrix_parent].bos.splice(j, 1); // remove from array
															delete scope.data.entityMatrixBoDict[key];
														} else {
															row._flag = 'update';
															column._flag = "delete"; // remove from DB
														}
													} else {
														if (column._flag == "delete") { // check value was checked when matrix was loaded, then unchecked and now checked again
															delete column._flag;
														} else {
															row._flag = "update";
															column._flag = "update";
														}
													}

												}
											}
										}
									}

								} else { // create new matrix cell value

									for (var i = 0; i < scope.data.bos.length; i++) {
										var row = scope.data.bos[i];
										if (row.boId == rowPK) {

											var newCellValue;
											if (matrixValueType === 'checkicon') {
												newCellValue = 1;
											} else {
												newCellValue = value;
											}

											if (!newCellValue || newCellValue === 0) { // don't save new empty cells
												continue;
											}

											var currentRow = row;
											boService.emptyBO({boMetaId: matrixConfig.entity_y}).then(function (newMatrixCell) {

												newMatrixCell.attributes[entity_field_matrix_x_refFieldShort] = {
													"id": parseInt(columnPK),
													name: "dummy2"
												};
												newMatrixCell.attributes[entity_field_matrix_parentShort] = {
													"id": parseInt(rowPK),
													name: "dummy3"
												};

												if (matrixValueType === 'combobox') {
													newMatrixCell.attributes[entity_matrix_reference_fieldShort] = {
														id: parseInt(newCellValue),
														name: "dummy4"
													};
												}
												newMatrixCell.attributes[entity_matrix_value_fieldShort] = newCellValue;

												newMatrixCell.boMetaId = matrixConfig.entity_matrix;
												newMatrixCell.value = newCellValue;

												currentRow._flag = "update";
												newMatrixCell._flag = "insert";

												if (value == null) {
													scope.data.entityMatrixBoDict[key] = newMatrixCell;
												}

												if (!currentRow.subBos[matrixConfig.entity_field_matrix_parent].bos) {
													currentRow.subBos[matrixConfig.entity_field_matrix_parent].bos = [];
												}

												currentRow.subBos[matrixConfig.entity_field_matrix_parent].bos.push(newMatrixCell);

											});

										}
									}
								}
							};
						};
					}
				};
			}
		};
	});
}
