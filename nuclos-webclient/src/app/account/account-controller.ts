module nuclos.account {

	export interface IAccountCtrl {
		username: string
		firstname: string
		lastname: string
		email: string
		email2: string
		password: string
		privacyconsent: boolean
		vm: IAccountCtrl
	}

	interface IAccountCtrlScope extends ng.IScope {
		buttonClick: any;
		hasDatenschutz: any;
		showPrivacyConsent: any;
	}

	export class AccountCtrl implements IAccountCtrl {
		username
		firstname
		lastname
		email
		email2
		password
		privacyconsent

		vm: IAccountCtrl = this;

		constructor(private $resource: ng.resource.IResourceService,
					private $window: ng.IWindowService,
					private $routeParams: angular.route.IRouteParamsService,
					private $scope: IAccountCtrlScope,
					private dialog: nuclos.dialog.DialogService,
					private authService: nuclos.auth.AuthService,
					private errorhandler: Errorhandler,
					private nuclosI18N: nuclos.util.I18NService) {

			this.vm = this;

			let action = this.$routeParams['action'];
			let user = $routeParams['user'];
			let parameter = $routeParams['parameter'];

			authService.getLegalDisclaimers().then((data) => {
				this.$scope.hasDatenschutz = this.authService.hasLegalDisclaimer("Datenschutz", data);
			});

			if (action == "activate" && user != null && parameter != null) {
				this.$resource(restHost + "/admin/account/activate/" + user + "/" + parameter).get(
					(data) => {
						let title = this.nuclosI18N.getI18n('webclient.account.successful.enabled');
						let text = this.nuclosI18N.getI18n('webclient.account.enabling.was.successful.you.can.sign');
						this.dialog.alert(title, text, () => {
							this.$window.location.href = window.location.pathname + '#/login'
						});
					},
					(error) => {
						let textStatus = error.status;
						if (textStatus == "400") {
							let title = this.nuclosI18N.getI18n('webclient.account.error');
							let text = this.nuclosI18N.getI18n(error.data);
							this.dialog.alert(title, text, null, null);
						} else {
							this.errorhandler.show(error.data, textStatus);
						}
					}
				);
			}

			this.$scope.buttonClick = () => {
				if (this.$scope.hasDatenschutz && !this.privacyconsent) {
					let title = this.nuclosI18N.getI18n('webclient.account.error');
					this.dialog.alert(title, this.nuclosI18N.getI18n('webclient.account.noprivacyconsent'));
					return;
				}

				if (this.email != this.email2) {
					let title = this.nuclosI18N.getI18n('webclient.account.error');
					this.dialog.alert(title, this.nuclosI18N.getI18n('webclient.account.email.nomatch'));
					return;
				}

				let regData = {
					"name": this.username,
					"newPassword": this.password,
					"firstname": this.firstname,
					"lastname": this.lastname,
					"email": this.email,
					"privacyconsent": this.privacyconsent
				};

				let createAccount = () => {
					this.$resource(restHost + "/admin/account").save(regData,
						(data) => {
							let title = this.nuclosI18N.getI18n('webclient.account.successful.registered');
							let text = this.nuclosI18N.getI18n('webclient.account.you.get.an.email.to.activate.your.account');
							this.dialog.alert(title, text, () => {
								this.$window.location.href = window.location.pathname + '#/menu'
							});
						},
						(error) => {
							let textStatus = error.status;

							//SHOP-184 - Catch 401, too, no automatic redirect to login.
							if (textStatus == "400" || textStatus == "401") {
								let title = this.nuclosI18N.getI18n('webclient.account.error');
								let text = this.nuclosI18N.getI18n(error.data);
								this.dialog.alert(title, text, null);

							} else {
								this.errorhandler.show(error.data, textStatus);
							}
						}
					);
				}

				if (!authService.hasSessionId()) {
					//SHOP-184 - Without Session Id try anonymous login first
					authService.loginAsAnonymous().then((login) => {
						createAccount();
					}, (error) => {
						this.errorhandler.show(error.data, error.status);
					});
				} else {
					createAccount();
				}

			};

			this.$scope.showPrivacyConsent = () => {
				this.authService.showLegalDisclaimer('Datenschutz');
			};

		}
	}
}