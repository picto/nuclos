module nuclos.passwords {

	export class PasswordService {
		
		constructor(
			private $resource: ng.resource.IResourceService,
			private util
		) {
		}
		       
		changePassword = (passwordItem: IPassword) => {
			return this.$resource(
					restHost + '/user/' + passwordItem.userName + '/password', 
					null, 
					{'update': { method: 'PUT' }}
				)['update'](this.util.jsonStringifyWithFunctions(passwordItem));

		};

	}
}

angular.module("nuclos").service("passwords", nuclos.passwords.PasswordService);
