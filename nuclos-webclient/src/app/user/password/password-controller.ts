module nuclos.passwords {

    export interface IPasswordCtrl {
    }

    export interface IPassword {
		userName: string;
    	oldPassword: string;
    	newPassword: string;
		confirmNewPassword: string;
    }
    
    export class PasswordCtrl {

        vm: IPasswordCtrl = this;
	    passwordItem: IPassword;
    	msg: string;
    	color: string;

        constructor(
            private passwords: nuclos.passwords.PasswordService,
            private $window: ng.IWindowService,
            private $cookieStore,
            private dialog,
            private errorhandler: Errorhandler
        ) {
        	this.clear();
        }
        
        clear = () => {
			var user = this.$cookieStore.get('authentication').username;
			this.passwordItem = {
				userName: user,
				oldPassword: null,
				newPassword: null,
				confirmNewPassword: null
			};
        }

        handleError = (error) => {
            this.errorhandler.show(error.data, error.status, this.$cookieStore);
        };

        changePassword = (passwordItem: IPassword): void => {
           	if (!passwordItem.newPassword) {
        		this.msg = 'webclient.user.changepassword.empty'
        		this.color = 'red';
        		return;
        	}
           	
         	if (passwordItem.newPassword !== passwordItem.confirmNewPassword) {
        		this.msg = 'webclient.user.changepassword.nomatch';
        		this.color = 'red';
        		return;
        	}
        	
        	this.msg = null;
        	
            this.passwords.changePassword(passwordItem).$promise.then(
                data => {
                	this.msg = 'webclient.user.changepassword.changed';
                	this.color = 'green';
                	this.clear();
                },
                error => {
                	this.color = 'red';                		
                	if (error.status == 403) {
                       	this.msg = 'webclient.user.changepassword.passwordwrong';
                	} else if (error.status == 406) {
                       	this.msg = 'webclient.user.changepassword.passwordnotsuitable';
                	} else {
                       	this.msg = 'webclient.account.error';
                        this.handleError(error);                		
                	}
                }
            );
        };

    }
}
