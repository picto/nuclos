import Component = nuclos.util.Component;
import BoViewModel = nuclos.model.interfaces.BoViewModel;
import Meta = nuclos.model.interfaces.Meta;

@Component(nuclosApp, "ncUiGridReadonly", {
    bindings: {
        bometaid: "@",
        onEditClick: "&",
        reffield: "@",
        subform: "=",
        master: "="
    },
    controllerAs: "vm",
    templateUrl: "app/grid/nc-ui-grid-directive-readonly.html"
})
class NcUiGridReadonly {

    private bometaid: string;

	private master: BoViewModel;
    private reffield: string;
    private data: Array<nuclos.model.interfaces.Bo>;
    private layout;

    private showEditRowInOverlay: boolean;
    private showEditRowInNewTabOrPopup: boolean;
    private showEditRowInPopup: boolean;

    private onEditClick: Function;


	// TODO: Proper types for the following fields:
	private boMeta: Meta;
	private subform: {
		readonly: any,
		reffield: any,
		ignoresublayout: any
	};
	private subformReadonly: any;

    constructor(
        private $timeout: ng.ITimeoutService,
    	private $resource: ng.resource.IResourceService,
        private $rootScope,
        private layoutService,
        private metaService: nuclos.core.MetaService,
        private localePreferences: nuclos.clientprefs.LocalePreferenceService,
        private util: nuclos.util.UtilService,
        private ncGridUtils: NcGridUtils,
        private errorhandler,
        private $element: ng.IAugmentedJQuery
    ) {

        /*
        TODO
        there is a problem with directives which are used inside static-layout.html's switch block
        the directives are not initialized once but also sometimes (randomly) if a new bo entry is selected and detailblock is rendered again
        
        the grid needs to be initialized from constructor (page load) + on refreshgrids (select another bo entry from subform-controller) 
        which leads at the moment to unnecessary REST calls unless the initialization will be called only once
        */

        this.initTable();

        $element.scope().$on('refreshgrids', () => {
            this.initTable();
        });
        
        $element.scope().$on('$locationChangeSuccess', (angularEvent, newUrl) => {
            if (newUrl.indexOf('/logout') == -1) { // don't load data if logout is clicked
        	    this.loadData();
            }            
		});
    }

    public enterEditMode() {
        this.onEditClick();
    }

    public editRow(bo) {
        let master = this.master;

        let boList = master.selectedBo.subBos[this.reffield].bos;
        let meta = this.boMeta;

        this.ncGridUtils.editRow(
            bo,
            master,
            boList,
            meta,
            true,
            this.reffield
        );
    }

    public editRowInNewTab(bo) {
        this.ncGridUtils.editRowInNewTab(bo, this.boMeta);
    }

    public editRowInPopupUrl(bo) {
        this.ncGridUtils.editRowInPopupUrl(bo);
    }
    
    public editRowInPopup(bo, $event) {
        this.ncGridUtils.editRowInPopup(bo, this.boMeta, $event);
    }
    
    private fixedHeader() {
        let container = this.$element.find(".ui-grid-container");
        let thead = this.$element.find("thead");
        let tbody = this.$element.find("tbody");

        // set correct table body height to make header fixed
        tbody.height(container.height() - thead.height());

        // fill available table width (if no horizontal scrolling is needed)
        let table = this.$element.find("table");
        if (table.width() < container.width()) {
            table.css("width", "100%");

            // chrome fix for placing edit button to the correct place after resizing table
            let lastTh = $("nc-ui-grid-readonly .ui-grid-container th:last-child");
            lastTh.css("width", "50%");
            this.$timeout(() => {
                lastTh.css("width", "100%");
            });
        }
    }
    
    private isSubformReadonly() {
    	this.subformReadonly = this.subform.readonly 
    		|| this.master.selectedBo.subBos === undefined
    		|| this.master.selectedBo.subBos[this.subform.reffield].restriction === "readonly"
    	return this.subformReadonly;
    }

	private loadData() {
		let master = this.master;

		if (master
			&& master.selectedBo
			&& master.selectedBo.subBos
			&& master.selectedBo.subBos[this.reffield].links.bos) { // no subBos if entry is new

            let subdataLink: string =
                master.selectedBo.subBos[this.reffield].links.bos.href + "?fields=tableview&sort=:sortExpression";

        	this.isSubformReadonly();
        
            this.$resource(subdataLink, {sortExpression: this.ncGridUtils.getSortExpression(this.layout.columns)}).get(
                (data) => {
                    data.bos.forEach(bo => {

                        if (bo.attributes["nuclosState"]) {
                            bo.attributes['nuclosStateIcon'] = "<img src='" + restHost + '/resources/stateIcons/' + bo.attributes["nuclosState"].id + "'>";
                        }

                        for (let attributeName in bo.attributes) {

                            if (bo.attributes.hasOwnProperty(attributeName)) {
                                let attributeMeta = this.boMeta.attributes[attributeName];
                                if (!attributeMeta) {
                                    continue;
                                }

                                let attribute = bo.attributes[attributeName];

                                if (attribute.name) { // reference
                                    bo.attributes[attributeName] = attribute.name;
                                } else if (attributeMeta.type === "Decimal" || attributeMeta.type === "Integer") {
                                	bo.attributes[attributeName] = formatNumberForShow(
                                        bo.attributes[attributeName],
                                        attributeMeta.precision,
                                        attributeMeta.scale,
                                        this.localePreferences
                                    );
                                } else if (attributeMeta.type === "Date") {
                                    bo.attributes[attributeName] = this.util.formatDate(attribute);
                                } else if (attributeMeta.type === "Timestamp") {
                                    bo.attributes[attributeName] = this.util.formatTimestamp(attribute);
                                } else if (attributeMeta.type === "Boolean") {
                                    bo.attributes[attributeName] = attribute ? "\u2713" : "";
                                }

                                // handle line breaks
                                if (attribute && attribute.indexOf && attribute.indexOf('\n') !== -1) {
									bo.attributes[attributeName] = attribute.replace(/\n/g, '<br>');
								}
                            }
                        }
                    });

                    this.data = data;

                    this.fixedHeader();

                },
                (error) => {
                    this.errorhandler.show(error.data, error.status);
                }
            );
        }
    }

    private initTable() {

        let master = this.master;
        let parentUID = master.boMetaId;

        this.layoutService.tableviewlayoutsubform(parentUID, this.bometaid).then((layout) => {
            this.layout = layout;
            
            this.metaService.getBoMetaData(parentUID, this.reffield).then((boMeta) => {
                
                this.boMeta = boMeta;

                if (master.selectedBo) {
                    if (!master.selectedBo.submetas) {
                        master.selectedBo.submetas = {} as nuclos.model.interfaces.Map<Meta>;
                    }
                    master.selectedBo.submetas[this.bometaid] = boMeta;
                }

                for (let column of layout.columns) {
                    let attributeMeta = boMeta.attributes[column.fieldName];
                    if (attributeMeta && (attributeMeta.type === "Decimal" || attributeMeta.type === "Integer")) {
                        column.isNumber = true;
                    }
                }

                let hasLayout = boMeta.links.defaultLayout !== undefined;
                let hasDetailBoMetaId = boMeta.detailBoMetaId !== undefined;

                let canShowRowInDetails = (hasLayout && !this.subform.ignoresublayout) || hasDetailBoMetaId;

                this.showEditRowInOverlay = !this.isSubformReadonly() && master.selectedBo.canWrite && canShowRowInDetails;
                this.showEditRowInNewTabOrPopup = canShowRowInDetails;

                this.loadData();

                this.showEditRowInPopup = boMeta.lafParameter && boMeta.lafParameter.nuclos_LAF_Webclient_Popup;
            });
        });

    }
}
