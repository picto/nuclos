module nuclos.grid {
	/**
	 * @ngdoc service
	 * @name nuclos.gridLayoutService
	 */
	class GridLayoutService {
		columnLayoutChanges = {};

		constructor(private $resource,
					private $q,
					private layoutService: nuclos.layout.LayoutService,
					private errorhandler: nuclos.Errorhandler) {
		}

		/**
		 * @ngdoc method
		 *
		 * @name storeGridLayout
		 * @methodOf nuclos.gridLayoutService
		 *
		 * @description stores grid column order and column width on the server
		 *
		 * @param {string} boMetaId boMetaId
		 * @param {string} [subformBoMetaId] subformBoMetaId
		 * @param {array} columns array containing column order and column width
		 */
		storeGridLayout(boMetaId, subformBoMetaId, columns) {

			this.columnLayoutChanges[boMetaId] = columns;
			this.$resource(
				restHost + '/meta/storetableviewlayout/' + boMetaId + (subformBoMetaId ? '/' + subformBoMetaId : ''),
				null,
				{
					'update': {method: 'PUT'}
				}
			).update(
				{columns: columns},
				function (data) {
				},
				function (error) {
					console.error('Unable to store column layout.', error);
				}
			);
		}


		/**
		 * @ngdoc method
		 *
		 * @name resetGridLayout
		 * @methodOf nuclos.gridLayoutService
		 *
		 * @description resets grid column order and column width on the server
		 *
		 * @param {string} boMetaId boMetaId
		 * @param {string} [subformBoMetaId] subformBoMetaId
		 */
		resetGridLayout(boMetaId, subformBoMetaId) {

			var deferred = this.$q.defer();

			this.$resource(
				restHost + '/meta/resettableviewlayout/' + boMetaId + (subformBoMetaId ? '/' + subformBoMetaId : '')
			).get(
				function (data) {
					deferred.resolve();
				},
				function (error) {
					console.error('Unable to reset column layout.', error);
					deferred.reject();
				}
			);

			return deferred.promise;
		}

		/**
		 * @ngdoc method
		 *
		 * @name resetWorkspace
		 * @methodOf nuclos.gridLayoutService
		 *
		 * @description resets the users workspace including column settings
		 */
		resetWorkspace() {

			var deferred = this.$q.defer();

			this.$resource(restHost + '/meta/resetworkspace').query(
				function (data) {
					// reload table layout into model
					this.layoutService.clearCache();
					deferred.resolve();
				},
				function (error) {
					console.error('Unable to reset workspace.', error);
					this.errorhandler.show(error.data, error.status);
					deferred.reject();
				}
			);

			return deferred.promise;
		}
	}

	nuclosApp.service('gridLayoutService', GridLayoutService);
}
