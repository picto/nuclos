module nuclos.grid {
	nuclosApp.directive('ncUiGridAutonumber', function ($timeout, $rootScope, dataService) {
		return {
			require: 'ncUiGrid',
			scope: false,
			link: function (scope, element, attrs, ncUiGridCtrl: NcUiGridController) {

				var autonumberColumnSortingDirection = function (grid) {
					for (var c in grid.columns) {
						var column = grid.columns[c];
						if (column.colDef.isAutonumber && column.sort.direction) {
							return column.sort.direction;
						}
					}
				};

				var showAutonumberNavigationButtons = function (grid) {
					return grid.rows.length > 0 && autonumberColumnSortingDirection(grid) !== undefined;
				};

				var gridContext = ncUiGridCtrl.gridContext;


				ncUiGridCtrl.addGridApiRegisteredListener(function (gridApi) {

					// check if autonumber buttons should be shown after grid was rendered
					gridApi.core.on.rowsRendered(scope, function () {
						gridContext.showAutonumberNavigationButtons = showAutonumberNavigationButtons(gridApi.grid);
					});

					// check if autonumber buttons should be shown after column sort order was changed
					gridApi.core.on.sortChanged(scope, function (grid, sortColumns) {
						gridContext.showAutonumberNavigationButtons = showAutonumberNavigationButtons(grid);
					});
				});


				//http://jsfiddle.net/9hs1ed89/
				var subformMoveRows = function (gridApi, reffield, direction) {

					gridApi.grid.rows.move = function (from, to) {
						this.splice(to, 0, this.splice(from, 1)[0]);
					};

					var selectedRows = gridApi.selection.getSelectedRows();
					var rows = gridApi.grid.rows;

					if (direction == 'down') {
						selectedRows = selectedRows.reverse();
						rows = rows.reverse();
					}


					for (var s in selectedRows) {
						var selectedRow = selectedRows[s];
						for (var r in rows) {
							var row = rows[r];
							if (row.entity && selectedRow.boId == row.entity.boId) {
								if (parseInt(r) > 0) {
									gridApi.grid.rows.move(r, parseInt(r) - 1);
								}
							}
						}
					}

					gridApi.core.refresh();

					if (direction == 'down') {
						selectedRows = selectedRows.reverse();
						rows = rows.reverse();
					}


					var getAutonumberAttribute = function () {
						var attributes = ncUiGridCtrl.boMeta.attributes;
						for (var a in attributes) {
							var attribute = attributes[a];
							if (attribute.defcomptype == 'Autonummer') {
								return a;
							}
						}
						return;
					};


					// update autonumbers
					var master = dataService.getMaster();
					master.dirty = true;
					master.selectedBo._flag = 'update';
					var autonumberAttribute = getAutonumberAttribute();

					for (var r in rows) {
						var row = rows[r];
						if (row.entity) {
							var valueBefore = row.entity.attributes[autonumberAttribute];
							row.entity.attributes[autonumberAttribute] = parseInt(r) + 1;
							if (row.entity.attributes[autonumberAttribute] != valueBefore) {
								row.entity._flag = 'update';
							}
						}
					}

					// close open dropdown cell templates in subform
					$rootScope.$broadcast('cellNav', {});

					gridApi.core.refresh();
				};


				gridContext.subformRowsUp = function (gridApi, reffield) {
					subformMoveRows(gridApi, reffield, autonumberColumnSortingDirection(gridApi.grid) == 'asc' ? 'up' : 'down');
				};

				gridContext.subformRowsDown = function (gridApi, reffield) {
					subformMoveRows(gridApi, reffield, autonumberColumnSortingDirection(gridApi.grid) == 'asc' ? 'down' : 'up');
				};
			}
		};
	});
}