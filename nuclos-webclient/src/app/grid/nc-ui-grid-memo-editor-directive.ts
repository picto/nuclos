module nuclos.grid {

	interface IGridMemoScope extends angular.IScope {
		clickOutsideGridMemo: Function;
	}

	/**
	 * open dropdown on edit start
	 */
	nuclosApp.directive('ncUiGridMemo', function ($filter, $timeout, $rootScope, uiGridEditConstants) {
		return {
			priority: -100, // run after default uiGridEditor directive
			require: '?ngModel',
			link: function (scope: IGridMemoScope, element, attrs, ngModel) {
				scope.$on('uiGridEventBeginCellEdit', function () {
					$timeout(function () {
						$('.nuclosmemo').focus();
					}, 0);
				});

				scope.clickOutsideGridMemo = function () {

					// clear memo overlays
					$rootScope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT);
					$('.memo-overlay').remove();
					$('#appendToBodyDiv').empty();

					//NUCLOS-5024
					scope.clickOutsideGridMemo = function () {
					};
				}
			}
		};
	});
}
