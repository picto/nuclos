module nuclos.grid {

	interface IGridDropdownScope extends angular.IScope {
		clickOutsideGridDropdown: Function;
	}

	/**
	 * open dropdown on edit start
	 */
	nuclosApp.directive('ncUiGridDropdown', function ($filter, $timeout, $rootScope, uiGridEditConstants) {
		return {
			priority: -100, // run after default uiGridEditor directive
			require: '?ngModel',
			link: function (scope: IGridDropdownScope, element, attrs, ngModel) {
				scope.$on('uiGridEventBeginCellEdit', function () {
					$timeout(function () {
						$(element).find('.caret').click();
					}, 0);
				});

				scope.clickOutsideGridDropdown = function () {

					// close open LOV's
					$rootScope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT);
					$('#appendToBodyDiv-lov-overlay').remove();

					//NUCLOS-5024
					scope.clickOutsideGridDropdown = function () {
					};
				}
			}
		};
	});
}
