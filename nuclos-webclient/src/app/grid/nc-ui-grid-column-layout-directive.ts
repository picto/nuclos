module nuclos.grid {

nuclosApp.directive('ncUiGridColumnLayout', function(
	$timeout,
	$cookieStore,
	$window,
	$rootScope,
	gridLayoutService,
	nuclosI18N: nuclos.util.I18NService
) {

    return {
    	require: 'ncUiGrid',
    	scope: false,
		link: function (
			scope,
			element,
			attrs,
			ncUiGridCtrl: nuclos.grid.NcUiGridController
		) {


        	ncUiGridCtrl.addGridApiRegisteredListener(function(gridApi) {
        		
        		
				// save workspace
        		
			    var getFieldName = function(columnName) {
			    	for (var c=0; c<ncUiGridCtrl.gridOptions.columnDefs.length; c++) {
			    		var col = ncUiGridCtrl.gridOptions.columnDefs[c];
			    		if (col.name === columnName) {
			    			return col.fieldName;
			    		}
			    	}
			    };

			    var storeGridLayout = function() {
			    	var uiGridColumns = gridApi.saveState.save().columns;
			    	var columns = [];

			    	for (var c=0; c<uiGridColumns.length; c++) {
			    		if (uiGridColumns.hasOwnProperty(c)) {
			    			var col = uiGridColumns[c];
							if (col.visible) {
				    			columns.push({
				    				name: col.name,
				    				fieldName: getFieldName(col.name),
				    				width: col.width,
				    				sort: col.sort // col.sort.direction / col.sort.priority
				    			});
							}
			    		}
			    	}
			    	
			    	gridLayoutService.storeGridLayout(
		    			ncUiGridCtrl.boMeta.boMetaId,
		    			ncUiGridCtrl.subformBoMeta ? ncUiGridCtrl.subformBoMeta.boMetaId : null, 
			    		columns
			    	);
			    };
        		
				// store column layout when columns are resized or moved or sort order changes
				gridApi.colResizable.on.columnSizeChanged(scope, storeGridLayout);
				gridApi.colMovable.on.columnPositionChanged(scope, storeGridLayout);
				
				gridApi.core.on.columnVisibilityChanged(scope, function(column) {
					// column is now hidden
					storeGridLayout();
				});
				
				gridApi.core.on.sortChanged(scope, function(grid, sortColumns) {
					storeGridLayout();
				});


			    
				
				// reset workspace
        		
				if (!ncUiGridCtrl.gridOptions.gridMenuCustomItems) {
					ncUiGridCtrl.gridOptions.gridMenuCustomItems = [];
				}
				ncUiGridCtrl.gridOptions.gridMenuCustomItems.push(
					{
                        title: nuclosI18N.getI18n('webclient.grid.menu.resetcolumns.grid'),
                        action: function ($event) {
                        	gridLayoutService.resetGridLayout(ncUiGridCtrl.boMeta.boMetaId, ncUiGridCtrl.subformBoMeta ? ncUiGridCtrl.subformBoMeta.boMetaId : null).then(function() {
                        		// refresh active grids
                        		$rootScope.$broadcast('refreshgrids');
                        	});
                        },
                        order: 210
                    }
				);
				ncUiGridCtrl.gridOptions.gridMenuCustomItems.push(
					{
                        title: nuclosI18N.getI18n('webclient.grid.menu.resetcolumns.workspace'),
                        action: function ($event) {
                        	gridLayoutService.resetWorkspace().then(function() {
                        		// refresh active grids
                        		$rootScope.$broadcast('refreshgrids');
                        	});
                        },
                        order: 210
                    }
				);
				
				
	    	});
        	
    	}
    };
    
});
}
