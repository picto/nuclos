module nuclos.grid {

	export class NcUiGridController {
		gridContext;
		boMeta;
		subformBoMeta;
		gridOptions;
		gridApi;

		/*
		 * sub directives can register a function which will be called after the gridApi was registered
		 */
		gridApiRegisteredListeners = [];

		public setGridContext(gridContext) {
			this.gridContext = gridContext;
		};

		public setBoMeta(boMeta) {
			this.boMeta = boMeta;
		};

		public setSubformBoMeta(subformBoMeta) {
			this.subformBoMeta = subformBoMeta;
		};

		public setGridOptions(gridOptions) {
			this.gridOptions = gridOptions;
		};


		public addGridApiRegisteredListener(gridApiRegisteredListener) {
			this.gridApiRegisteredListeners.push(gridApiRegisteredListener);
		};

		/**
		 * this function needs to be called from the grid directive after the gridApi was registered
		 */
		public gridApiRegistered(gridApi) {
			this.gridApi = gridApi;
			for (var i = 0; i < this.gridApiRegisteredListeners.length; i++) {
				this.gridApiRegisteredListeners[i](gridApi);
			}
		};
	}

	nuclosApp.controller('NcUiGridController', NcUiGridController);
}
