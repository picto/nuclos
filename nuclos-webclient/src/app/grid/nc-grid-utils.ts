
interface IReffieldScope extends ng.IScope {
	reffield: any;
}

class NcGridUtils {

    constructor(
        private $cookieStore: ng.cookies.ICookiesService,
        private $rootScope: IReffieldScope,
        private $window: ng.IWindowService,
        private boModalService,
        private boService,
		private authService: nuclos.auth.AuthService
    ) {
    }

    public getSortExpression (columns) : string {
        let sortExpressionArray: Array<string> = [];
        for (let column of columns) {
            if (column.sort) {
                sortExpressionArray.push(column.uid + " " + column.sort);
            }
        }
        return sortExpressionArray.join(", ");
    }


    public editRowInNewTab(bo, boMeta) {
        // NUCLOS-4435: Details of dynamik BO
        let href =
            baseUrl() + "sideview/" +
            (boMeta.detailBoMetaId !== undefined ? boMeta.detailBoMetaId : bo.boMetaId) +
            "/" +
            bo.boId + 
            "?" + nuclos.detail.EXPAND_SIDEVIEW_PARAM + 
            "&" + nuclos.detail.SHOW_CLOSE_ON_SAVE_BUTTON_PARAM;

        this.$window.open(href, "_blank");
    }


    public editRowInPopupUrl(bo) {
        return bo ? 
            baseUrl() + "sideview/" + 
            bo.boMetaId + "/" + bo.boId + 
            "?" + nuclos.detail.EXPAND_SIDEVIEW_PARAM +  
            "&" + nuclos.detail.SHOW_CLOSE_ON_SAVE_BUTTON_PARAM
            : null;
    }

    public editRowInPopup(bo, subBoMeta, $event) {
        $event.preventDefault(); // don't open href link when clicking on open in popup icon
        let href = this.editRowInPopupUrl(bo);
        let popupParameter = subBoMeta.lafParameter.nuclos_LAF_Webclient_Popup;
        let popup = window.open(href, "", popupParameter);
        popup.blur();
    }

    public editRow(
            bo,
            master,
            boList,
            meta: Meta,
            isSubform,
            reffield
        ) {
        let boMetaId = bo.boMetaId;
        let isMainform = !isSubform;

        let entity = {
            boMetaId: boMetaId,
            bos: boList,
            meta: meta
        };
        
        var newScope = this.$rootScope.$new();
        newScope['reffield'] = reffield;

        //TODO: This is a very unfine hack, because row.boId should not be "1", but for now it is supposed, that if 1 it is actually null.
        //In this case it not possible to reload the complete bo, the exisiting information must be enough.

        if (!bo.boId || bo.boId === 1) {
            this.boModalService.openEditModal({
                row: bo,
                entity: entity,
                parentEntity: master,
                scope: newScope
            });
            return;
        }

        let openModal = () => {
            if (bo._document) {
                // open documents in sublist directly (without dialog)
                let url = bo._document + "?sessionid=" + this.authService.getSessionId();
                this.$window.open(url, "_blank");
            } else {
	            let callback: {dismiss: () => void};
                if (isMainform) {
                    callback.dismiss = function() {
                        this.$rootScope.$broadcast("reloadtabledata");
                    }
                }

                this.boModalService.openEditModal({
                    row: bo,
                    entity: entity,
                    parentEntity: isSubform ? master : undefined,
                    scope: newScope,
                    callback: callback
                });
            }
        };

        if (bo.loadedComplete) {
            openModal();
        } else {
            let boBackup = bo;
            this.boService.loadSingleBO(entity, bo, boList).then(function(data) {
                bo = data ? data : boBackup; // row is not loaded if it is a new unsaved row - in this case use existing row
                bo.loadedComplete = true;
                openModal();
            });
        }
    }

}

nuclosApp.factory("ncGridUtils", ($cookieStore, $rootScope, $window, boModalService, boService, authService) => {
    return new NcGridUtils($cookieStore, $rootScope, $window, boModalService, boService, authService);
});
