module nuclos.grid {

	import IGridOptionsOf = uiGrid.IGridOptionsOf;
	import IColumnDefOf = uiGrid.IColumnDefOf;

	export interface INuclosColumnDefOf<TEntity> extends IColumnDefOf<TEntity> {
		enableCellSelection?: any
	}

	export interface INuclosGridOptions<TEntity> extends IGridOptionsOf<TEntity> {
		useExternalSorting?: any;
		gridMenuShowHideColumns?: any,
		columnDefs?: Array<INuclosColumnDefOf<TEntity>>;
	}

    export interface INcUiGridScope extends ng.IScope {
        bometaid: any;
        master: any;
        bodata: any;
        subform: any;
        reffield: any;
        newbutton: any;
    	clonebutton: any;
    	deletebutton: any;
    	editenabled: any;
        width: any;
        height: any;
        gridContext: any;
        enableCellNav: boolean;
        showDeleteButton: boolean;
        showCloneButton: boolean;
        isSubformReadonly: boolean;
        entity: nuclos.model.interfaces.BoViewModel;
        gridApi: uiGrid.IGridApi;
        gridOptions: INuclosGridOptions<any>;
        boMeta: nuclos.model.interfaces.Bo;
		layout: Object;
		authentication: any;

		newSubboInGrid(subform: any);
		deleteSelectedBosInGrid(gridApi: uiGrid.IGridApi, subform: any);
		cloneSelectedBosInGrid(gridApi: uiGrid.IGridApi, subform: any);
		editRow(bo: nuclos.model.interfaces.Bo);
		editRowInNewTab(bo: nuclos.model.interfaces.Bo);
	}
}

/**
 * @ngdoc directive
 * @name global.directive:ncUiGrid
 * @scope
 * @restrict E
 *
 * @description
 * directive for displaying a grid using the ui-grid component
 *
 * @param {object} master the master model
 * @param {object=} subform subform object
 * @param {string=} reffield referencing field if used for subforms
 * @param {string} bometaid  boMetaId of the bos which are displayed in the grid
 * @param {boolean=} newbutton displays the new-button if set to true
 * @param {object} bodata data containing the list of bos
 * @param {string=} width width of the grid
 * @param {string=} height height of the grid
 */
nuclosApp.directive('ncUiGrid', function(
	$cookieStore: ng.cookies.ICookiesService,
	$rootScope: ng.IScope,
	$resource: ng.resource.IResourceService,
	$timeout: ng.ITimeoutService,
	$window: ng.IWindowService,
	$location: ng.ILocationService,
	$q: ng.IQService,
	$uibModal,
	dataService: nuclos.core.DataService,
	layoutService,
	uiGridConstants,
	uiGridEditConstants,
	boModalService,
	metaService,
	boService: nuclos.core.BoService,
	localePreferences: nuclos.clientprefs.LocalePreferenceService,
	dialog: nuclos.dialog.DialogService,
	ncGridUtils: NcGridUtils,
	errorhandler,
	nuclosI18N: nuclos.util.I18NService
		) {
    return {
    	scope: {
    		bometaid: "@",
			master: "=",
			bodata: "=",
			subform: "=",
			reffield: "@",
			newbutton: "@",
			clonebutton: "@",
			deletebutton: "@",
			editenabled: "@",
			width: "@",
			height: "@"
		},
        restrict: "E",
		templateUrl: 'app/grid/nc-ui-grid-directive.html',
		controller: "NcUiGridController as ncUiGridCtrl",
        compile: function () {
            return {
                pre: function (scope: nuclos.grid.INcUiGridScope, element, attributes, ncUiGridCtrl: any) {
					let _isSubformReadonlyFunc = {
						initialized: false,
						func: function() { return false }
					};
					var _enableCellEditForColumnDef = (columnDef: any) => { return true; };

					let master: nuclos.model.interfaces.BoViewModel = scope.master;

                	// use this scope object for communication with sub directives
					scope.gridContext = {};
					ncUiGridCtrl.setGridContext(scope.gridContext);

					let isSubform: boolean = scope.subform !== undefined;
					let isMainform: boolean = !isSubform;

					scope.enableCellNav = !isMainform; // ui-grid-cellnav will not work with row selection
					scope.showDeleteButton = !isMainform && scope.deletebutton === 'true';
					scope.showCloneButton = !isMainform && scope.clonebutton === 'true';

					scope.authentication = $cookieStore.get('authentication');

					var _isSubformReadonly = false;
					if (isSubform) {
						_isSubformReadonly =
							scope.subform.readonly
							|| master.selectedBo.subBos && master.selectedBo.subBos[scope.subform.reffield].restriction === 'readonly'
							|| !master.selectedBo.canWrite;
					}

					scope.isSubformReadonly = _isSubformReadonly;

                	boService.createEmptySubBoIfNotThere(master.selectedBo, scope.reffield, true);

                	let subbo: nuclos.model.interfaces.Bo =
						master.selectedBo && master.selectedBo.subBos ? master.selectedBo.subBos[scope.reffield] : undefined;


					let updateGridRowColors = function () {
						$timeout(function () {
							$('.ui-grid-row.grid-row-dirty').removeClass('grid-row-dirty');
							$('.ui-grid-row.grid-row-deleted').removeClass('grid-row-deleted');
							$('.ui-grid-row.grid-row-new').removeClass('grid-row-new');

							// add dirty/ class to to complete row, so empty columns on the right will also be shown in correct color
							$('.ui-grid-render-container-body .ui-grid-cell.grid-row-dirty:first-child').parent().parent().addClass('grid-row-dirty');
							$('.ui-grid-render-container-body .ui-grid-cell.grid-row-new:first-child').parent().parent().addClass('grid-row-new');
							$('.ui-grid-render-container-body .ui-grid-cell.grid-row-deleted:first-child').parent().parent().addClass('grid-row-deleted');
						}, 0);
					};

					let chunkSize = 40;
					
					let loadSubformData = function(subbos, offset, sortExpression) {
						if (!subbos || !subbos.links.bos) {
							return;
						}
						
						if (sortExpression === undefined) {
							sortExpression = subbos['lastSortExpression'];
						}
						
						if (offset === undefined) {
							offset = subbos.bos ? subbos.bos.length : 0;
						}
						
						let subdataLink: string = subbos.links.bos.href + "?fields=tableview&offset=" + offset 
							+ "&chunkSize=" + chunkSize + "&sort=:sortExpression";

						$resource(subdataLink, {sortExpression: sortExpression}).get(
							function (data) {
								subbos.bos = offset > 0 ? subbos.bos.concat(data.bos) : data.bos;
								subbos['bosLoaded'] = data.all ? 'all' : 'partly';
								scope.gridOptions.data = subbos.bos;
								
								scope.gridApi.infiniteScroll.dataLoaded(false, !data.all);									
								subbos['lastSortExpression'] = sortExpression;
							},
							function (error) {
								errorhandler.show(error.data, error.status);
							}
						);
					}
					
					let subformLoadMoreData = function() {
						if (master.selectedBo && master.selectedBo.subBos) {
							loadSubformData(master.selectedBo.subBos[scope.reffield], undefined, undefined);
							
						}
					}

					/**
					 * initialize grid
					 * @param {boolean} [forceLoadData] if true load subform data from restservice
					 */
					let initTable = (subform, boMeta, layout, forceLoadData) => {
						let selectedBo: nuclos.model.interfaces.Bo = master.selectedBo;
						let boMetaId = boMeta.boMetaId;

						// column definition for ui-grid
						if (subbo) {
							subbo.links.defaultLayout = boMeta.links.defaultLayout; // TODO is this the correct place to put ?
						}
						scope.boMeta = boMeta;
						if (selectedBo) {
							if (!selectedBo.submetas) {
								selectedBo.submetas = {} as nuclos.model.interfaces.Map<Meta>;
							}
							selectedBo.submetas[boMetaId] = boMeta;
						}

						//NOAINT-617 Those names come from the main layout and are to be used.
						let mpSubformColNames = {};
						if (subform && subform.columns) {
							for (let column of subform.columns) {
								if (column.fieldname && column.name) {
									mpSubformColNames[column.fieldname] = column.name;
								}
							}
						}

						scope.gridApi.grid.appScope['fields'] = {};

						let defaultCellTemplate = '<div title="{{COL_FIELD CUSTOM_FILTERS}}"'
							+ ' class="ui-grid-cell-contents default-cell-template" ng-click="grid.appScope.focusCell($event, col);"'
							+ ' ng-style="{\'background-color\': row.entity.rowcolor}">{{COL_FIELD CUSTOM_FILTERS}}</div>';

						let gridOptions = [];
						for (var column of layout.columns) {

							// don't show reference in subform to main form
							if (subform && column.uid === subform.reffield) {
								continue;
							}

							//NUCLOS-4228. column.fieldName as it is available, too, can be wrong. Getting the fieldName from the uid is safer.
							let fieldName = getShortFieldName(subform ? subform : scope.master, column.uid);
							var attributeMeta = boMeta.attributes[fieldName];

							//NUCLOS-4209. For nuclosSystemIdentifier it's vice versa. column.fieldName has the correct fieldname "nuclosSystemid"
							//TODO: Why is this the case?
							//TODO: Clear all the confusion concerning the inconsistency between column.fieldname and columnd.uid

							if (!attributeMeta) {
								attributeMeta = boMeta.attributes[column.fieldName];
							}

							if (!attributeMeta) {
								console.warn("No meta data found for '" + column.uid + "'", boMeta.attributes);
								continue;
							}

							let displayName = column.displayName;
							//NOAINT-617 Those names come from the main layout and are to be used.
							if (mpSubformColNames[fieldName]) {
								displayName = mpSubformColNames[fieldName];
							}
							
							let isReference = attributeMeta.reference;
							let isNuclosProcess = attributeMeta.boAttrName === "nuclosProcess";
							let columnDef = {
								field: "attributes." + fieldName + (isReference || isNuclosProcess ? ".name" : ""),
								fieldName: fieldName,
								name: column.displayName,
								displayName: displayName,
								width: column.width,
								enableCellEdit: !scope.isSubformReadonly,
								enableCellEditOnFocus: true,
								cellTemplate: defaultCellTemplate,
								editableCellTemplate: 'app/grid/celltemplates/default-edit.html',
								readonly: null,
								cellEditableCondition: null,
								sort: null,
								cellFilter: null,
								cellClass: null,
								editType: null
							};

							//Function: Is Column Edit Enabled
							let _isSubFormCellEditEnabled = function(fieldName) {
								var isNuclosProcess = fieldName === "nuclosProcess";
								var isNuclosState = fieldName === "nuclosState";
								var enabledCellEdit = true;

								// disable cell edit for column if disabled in layout
								if (scope.isSubformReadonly || scope.editenabled !== 'true' || isNuclosState || isNuclosProcess) { // the whole subform is readonly
									enabledCellEdit = false;

								} else {
									for (let subcol of scope.subform.columns) {
										if (subcol.fieldname === fieldName) {
											enabledCellEdit = !subcol.readonly; // this column is readonly
											break;
										}
									}

								}

								return enabledCellEdit;
							}

							var _isSubFormCellEditEnabled2 = _isSubFormCellEditEnabled;

							_enableCellEditForColumnDef = function (columnDef) {
								var isCellEditEnabled = _isSubFormCellEditEnabled2(columnDef.fieldName);

								if (columnDef.enableCellEdit) {
									columnDef.cellEditableCondition = isCellEditEnabled;
								} else {
									columnDef.readonly = !isCellEditEnabled;
								}

								columnDef.cellClass = function(grid, row, col, rowRenderIndex, colRenderIndex) {
									var cellReadWrite = columnDef.enableCellEdit && columnDef.cellEditableCondition

									return rowRenderIndex % 2 === 0 ?
										(cellReadWrite ? 'readwritecelleven' : 'readonlycelleven') :
										(cellReadWrite ? 'readwritecellodd' : 'readonlycellodd');
								}

								//Not necessary here
								//scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.ALL);

								return columnDef.cellEditableCondition;
							}

							if (isSubform) {
								_enableCellEditForColumnDef(columnDef);
							}

							let isImageOrDocument = attributeMeta.type === 'Image' || attributeMeta.type === 'Document';

							//Image and Document are not editable cells and Autonummer is not editable at all
							if (isImageOrDocument || attributeMeta.defcomptype === 'Autonummer') {
								columnDef.readonly = !columnDef.cellEditableCondition;
								columnDef.enableCellEdit = false;
							}

							//ISD-318: Memo
							if (attributeMeta.scale === 4000) {
								columnDef.editableCellTemplate = 'app/grid/celltemplates/memo.html';
							}

							if (attributeMeta.precision) {
								columnDef.editableCellTemplate = 'app/grid/celltemplates/number.html';
							}

							// sorting
							if (column.sort) {
								columnDef.sort = {
									direction: column.sort,
									priority: column.sortOrder + 1
								};
							}

							scope.gridApi.grid.appScope['fields'][columnDef.field] = {
								uid: column.uid,
								name: fieldName,
								precision: attributeMeta.precision,
								scale: attributeMeta.scale,
								nojump: attributeMeta.nojump
							};

							if (attributeMeta.type === 'Decimal' || attributeMeta.type === 'Integer') {
								columnDef.cellTemplate = 'app/grid/celltemplates/number.html';
							}

							else if (attributeMeta.type === 'Boolean') {
								columnDef.editableCellTemplate = 'app/grid/celltemplates/checkbox-edit.html';
								columnDef.cellTemplate = 'app/grid/celltemplates/checkbox-view.html';
							}

							else if (attributeMeta.type === 'Date') {
								columnDef.cellFilter = 'date:"' + localePreferences.getDateFormat() + '"';
								scope.gridApi.grid.appScope['dateFormat'] = localePreferences.getDateFormat();
								columnDef.editableCellTemplate = 'app/grid/celltemplates/date.html';
							}

							else if (attributeMeta.type === 'Image' && attributeMeta.referencingBoMetaId === 'org_nuclos_businessentity_nuclosstate') {
								columnDef.cellTemplate = 'app/grid/celltemplates/nuclos-state-icon.html';
								scope.gridApi.grid.appScope['stateIconUrl'] = function (state) {
									if (!state) {
										return '';
									}
									return restHost + '/resources/stateIcons/' + state;
								}
							}

							else if (attributeMeta.type === 'Document' || attributeMeta.type === 'Image') {
								columnDef.cellClass = 'document';

								let ctname = attributeMeta.type === 'Image' ? 'image' : 'document';
								columnDef.cellTemplate = 'app/grid/celltemplates/' + ctname + '.html';

								scope.gridApi.grid.appScope['editorChangeDocument'] = function (fieldname, master, bo) {

									if (!master) {
										master = dataService.getMaster();
									}
									master.dirty = true;

									if (master.selectedBo && master.selectedBo._flag !== 'insert') {
										master.selectedBo._flag = 'update';
									}

									if (bo && bo._flag !== 'insert') {
										bo._flag = 'update';
									}

								};

							}

							else if (isReference) {

								// display LOV for references

								columnDef.editType = 'dropdown';
								columnDef.editableCellTemplate = 'app/grid/celltemplates/dropdown.html';

								// open reference from overlay
								function getReferenceUrl(refFieldId) {
									let subMeta = selectedBo.submetas[subform.boMetaId];
									let fieldName = getShortFieldName(subMeta, refFieldId);
									let referencingBoMetaId = subMeta.attributes[fieldName].referencingBoMetaId;
									return location.href.substring(0, location.href.indexOf('/#/')) + '/#/sideview/' + referencingBoMetaId + '/';
								}

								scope.gridApi.grid.appScope['openNewReference'] = function (refFieldId) {
									$window.open(getReferenceUrl(refFieldId) + 'new');
								};

								scope.gridApi.grid.appScope['getPopupParameter'] = function (col) {
									var appScope = scope.gridApi.grid.appScope;
									var refField = appScope['fields'][col.field];
									var refFieldId = refField.uid;
									var columnName = refField.name;
									var subform = appScope['subform'];
									var column = subform.columns.filter(function (column) {
										return column.fieldname == columnName;
									})[0];
									if (column && column.properties && column.properties.popupparameter) {
										return column.properties.popupparameter;
									}
								};

								scope.gridApi.grid.appScope['openReference'] = function (row, col) {
									var appScope = scope.gridApi.grid.appScope;
									var refFieldId = appScope['fields'][col.field].uid;
									var boId = row.entity.attributes[appScope['fields'][col.field].name].id;
									if (boId) {
										var popupParameter = scope.gridApi.grid.appScope['getPopupParameter'](col);
										if (popupParameter) {
											$window.open(getReferenceUrl(refFieldId) + boId, "", popupParameter);
										} else {
											$window.open(getReferenceUrl(refFieldId) + boId);
										}
									}
								};


								scope.gridApi.grid.appScope['loadLovDropDownOptionsInCell'] = (subBo, attrId, quickSearchInput, selectedSubBoAttribute) => {

									let deferred = $q.defer();

									// prevent flicker when opening dropdown
									let openDropdownHtmlItems = $('.ui-select-container.open ul');
									openDropdownHtmlItems.css('display', 'none');

									// see also detail-directive.js
									let select = {
										reffield: attrId,
										vlp: null
									};

									// TODO: Hack - use LayoutMLService instead of global function
									let vlps = window['getSpecialTypeFromItem']('vlp', scope.master.table);

									// VLP
									if (vlps[attrId]) {
										select.vlp = vlps[attrId];
										
										//FIXME: This is the bugfix for NUCLOS-4817 x), it is disabled, because it yields to an error
										//in the integration-test LockTest._04checkLockedUnlockedToggle()

										//delete select.reffield; //NUCLOS-4817 x)
									}

									//NUCLOS-4210
									//The fields of the subform-row have to be checked for mlrules which affect this attrId
									//TODO: Refactor, find a more general solution

									for (let col of subform.columns) {
										if (col.mlrules && col.mlrules.length > 0) {

											for (let mlrule of col.mlrules) {
												if (mlrule.type !== 'value-changed') {
													continue;
												}

												for (let action of mlrule.actions) {
													if (action.type === 'refresh-valuelist' && action.targetcomponent === attrId) {

														let sourcename = getShortFieldName(subBo, mlrule.sourcecomponent);
														let value = subBo.attributes[sourcename];

														if (value instanceof Date) {

															//Server needs format "yyyy-MM-dd". The conversation to another class and format in fields.js is responsible
															let getFormattedDate = function (date) {
																let year = date.getFullYear();
																let month = (1 + date.getMonth()).toString();
																month = month.length > 1 ? month : '0' + month;
																let day = date.getDate().toString();
																day = day.length > 1 ? day : '0' + day;
																return year + '-' + month + '-' + day;
															};

															value = getFormattedDate(value);

														}

														if (value && value.id !== undefined) {
															value = value.id;
														}

														//TODO: Don't set params, load the target-combo now
														var valuelistproviderService = angular.element(document.body).injector().get('valuelistproviderService');
														valuelistproviderService.setParameter(action, value);														
													}

												}
											}
										}
									}

									//NUCLOS-4210 END

									let mandatorId = subBo.attributes['nuclosMandator'] ? subBo.attributes['nuclosMandator'].id : undefined;
									if (!mandatorId) {
										mandatorId = selectedBo.attributes['nuclosMandator'] ? selectedBo.attributes['nuclosMandator'].id : undefined;
									}
									boService.loadDropDownOptions(select, selectedBo.boId, quickSearchInput, mandatorId).then(function (data: Array<any>) {

										// add an empty entry at first position of the dropdown array if dropdown is not a required field
										if (!attributeMeta.required && !quickSearchInput) {
											data.unshift({name: '', id: null});
										}

										deferred.resolve({
											selectedId: selectedSubBoAttribute ? selectedSubBoAttribute.id : undefined,
											data: data
										});
									});

									return deferred.promise;
								};
							}

							scope.gridApi.grid.appScope['focusCell'] = function ($event, col) {
/* not needed anymore - TODO remove from celltemplates
								$timeout(function () {
									$($event.target).closest('.ui-grid-cell').dblclick();
								});
*/
							};

							scope.gridApi.grid.appScope['initDatepicker'] = function (date) {
								// set date picker value initially, otherwise the datepicker component will leave it empty initially
								$timeout(function () {
									if (date) {
										$('[uib-datepicker-popup]').val(moment(new Date(date)).format(localePreferences.getDateFormat().toUpperCase()));
									}
								});
							};

							scope.gridApi.grid.appScope['focusDateInput'] = function ($event: FocusEvent) {
								
								scope.gridApi.grid.appScope['opened'] = true;
								
								// refocus date input after datepicker popover was opened								
								$timeout(() => {

									if (!$($event.target).is(":focus") ) {
										$($event.target).focus();
										$($event.target).select();
									}
								}, 300);
							};

							//TODO: Why was this subBo called "entity"?? See also in dropdown.html the calls with "row.entity"
							scope.gridApi.grid.appScope['editorChange'] = function (subBo, attrId, value, isReference) {

								// disable sorting after changing data to prevent rows from jumping while editing
								scope.gridOptions.enableSorting = false;

								if (value === undefined && isReference) {
									return;
								}
								let master = dataService.getMaster();
								master.dirty = true;

								if (master.selectedBo._flag !== 'insert') {
									master.selectedBo._flag = 'update';
								}

								if (subBo._flag !== 'insert') {
									subBo._flag = 'update';
								}

								//NUCLOS-4214
								//TODO: Check: By this execution of the layoutML Rules the above code for NUCLOS-4210 should be
								// obsolete (doubled) once the subform is loaded. It is still needed for pre-initiatlization of the vlp-params though.

								for (let col of subform.columns) {
									if (col.uid === attrId) {
										layoutService.executeLayoutMLRules(col.mlrules, subBo, boMeta.attributes, master);
									}
								}

								$timeout(function () {
									// NUCLOS-5097 causes unnecessary scrolling after selecting LOV - seems not to be necessary anymore
									// if (isReference) {
									//	// stop editing - close LOV
									// 	scope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT);
									// }

									// display whole row in dirty style
									$('.ui-grid-row .grid-row-dirty').parent().parent().addClass('grid-row-dirty');
								});

								// TODO: execute LayoutML rules - see also editorChange() in detail-directive.js
							};

							// append column index to name to make it unique in case that multiple columns with same name exists
							columnDef.name += gridOptions.length;

							// TODO call END_CELL_EDIT after LOV is closed
							// at the moment there is no callback for the uis:close event (https://github.com/angular-ui/ui-select/issues/439)
							gridOptions.push(columnDef);
						}

						// set column definition only if needed
						if (scope.gridOptions.columnDefs.length === 0) {
							scope.gridOptions.columnDefs = gridOptions;
						}

						let hasLayout = scope.boMeta.links.defaultLayout !== undefined;
						let hasDetailBoMetaId = scope.boMeta['detailBoMetaId'] !== undefined;
						let ignoresublayout = subform && subform.ignoresublayout;

						let canShowRowInDetails = (hasLayout && !ignoresublayout) || hasDetailBoMetaId;

						//Function: Is Subform Readonly
						_isSubformReadonlyFunc.func = function () {
							if (isSubform) {
								return scope.subform.readonly
									|| master.selectedBo.subBos && master.selectedBo.subBos[scope.subform.reffield].restriction === 'readonly'
									|| !master.selectedBo.canWrite;
							}
							return false;
						};
						_isSubformReadonlyFunc.initialized = true;

						_isSubformReadonly = _isSubformReadonlyFunc.func();

						scope.gridApi.grid.appScope['showEditRowInOverlay'] = (isSubform && !_isSubformReadonly && canShowRowInDetails) || isMainform; // same save context as mainform
						scope.gridApi.grid.appScope['showEditRowInNewTabOrPopup'] = (isSubform && canShowRowInDetails) || isMainform; // new save context

						// show row edit buttons
						if (scope.gridApi.grid.appScope['showEditRowInOverlay'] || scope.gridApi.grid.appScope['showEditRowInNewTabOrPopup']) {
							scope.gridApi.grid.appScope['editRow'] = scope.editRow;
							scope.gridApi.grid.appScope['editRowInNewTab'] = scope.editRowInNewTab;

							// the "Open in new tab" button can be configured to open a popup via "Look & Feel - parameter"
							scope.gridApi.grid.appScope['showEditRowInPopup'] = scope.boMeta.lafParameter && scope.boMeta.lafParameter['nuclos_LAF_Webclient_Popup'];

							// url for href to make "browser > right mouse click > open link in new tab" possible
							scope.gridApi.grid.appScope['editRowInPopupUrl'] = function (bo) {
								ncGridUtils.editRowInPopupUrl(bo);
							};

							scope.gridApi.grid.appScope['editRowInPopup'] = function (bo, $event) {
								ncGridUtils.editRowInPopup(bo, scope.boMeta, $event);
							};

							scope.gridOptions.columnDefs.push(
								{
									name: 'edit',
									displayName: '',
									cellTemplate: 'app/grid/celltemplates/edit-row.html',
									width: 46, // 33 would be good if no scrollbar is visible
									pinnedRight: true,
									enableCellEdit: false,
									enableCellSelection: false,
									enableColumnMenu: false
								}
							);
						}
						
						scope.$on('infiniteScrollMoreDataLoaded', function (event, data) {
							scope.gridApi.infiniteScroll.dataLoaded(false, !data.allLoaded);
							// append loaded data
							// scope.gridOptions.data = scope.gridOptions.data.concat(data.bos);
							scope.gridOptions.data = dataService.getMaster().bos;
						});


						let handleRowSelection = function () {
							if (isMainform) {
								// refresh status buttons
								dataService.getMaster().selectedRows = scope.gridApi.selection.getSelectedRows();
								scope.$emit('refreshstatusbuttons', scope.gridApi.selection.getSelectedRows());
							}
						};

						scope.gridApi.selection.on.rowSelectionChangedBatch(scope, function () { // select/unselect all via header
							handleRowSelection();
						});

						scope.gridApi.selection.on.rowSelectionChanged(scope, function (row) { // selection of single row
							handleRowSelection();
						});


						// load data
						if (isSubform) {
							if (selectedBo && selectedBo.subBos && selectedBo.subBos[scope.reffield]) {
								var subbos = selectedBo.subBos[scope.reffield];
								scope.gridOptions.data = subbos.bos;
								
								if (!subbos['bosLoaded'] || forceLoadData) {
									loadSubformData(subbos, 0, ncGridUtils.getSortExpression(layout['columns']));
								}
								
							} else {
								console.log("No model for subform:" + scope.reffield);
								scope.gridOptions.data = [];
							}
														
						} else {
							//Tableview
							scope.gridOptions.data = scope.bodata.bos;
						}


						let sortingAlgorithm = function (column) {
							return function (a, b) {
								if (!scope.gridOptions.enableSorting) {
									return 0;
								}

								let direction = column.sort.direction;
								if (null === a) {
									return direction === 'desc' ? 1 : -1;
								}
								if (null === b) {
									return direction === 'desc' ? -1 : 1;
								}
								if (a === b) {
									return 0;
								}
								return (a < b) ? -1 : 1;
							};
						};

						for (let optionColDef of scope.gridOptions.columnDefs) {
							if (!optionColDef.sortingAlgorithm) {
								optionColDef.sortingAlgorithm = sortingAlgorithm(optionColDef);
							}
						}

						// update readonly permissions - depends on bo status
						for (var i in scope.gridOptions.columnDefs) {
							_enableCellEditForColumnDef(scope.gridOptions.columnDefs[i]);
							scope.gridOptions.columnDefs[i]['enableCellEdit'] = true;
						}
					};

					scope.gridOptions = {
						rowHeight: 21,
						multiSelect: true,
						enableRowSelection: isMainform,
						enableFullRowSelection: true,
						enableRowHeaderSelection: isSubform, // show/hide checkbox column
						useExternalSorting: true, // serverside sorting in tableview, for subforms with paging, too

						// open modal for subform entry
						rowTemplate: 'app/grid/rowtemplate.html',

						// grid menu
						enableGridMenu: true,
						exporterMenuPdf: false,
						exporterMenuCsv: false,
						enablePinning: false,
						gridMenuShowHideColumns: false,

    				    enableColumnMenus: false,

						onRegisterApi: function (gridApi: uiGrid.IGridApi) {

							ncUiGridCtrl.setGridOptions(scope.gridOptions);
							ncUiGridCtrl.gridApiRegistered(gridApi);
							scope.gridApi = gridApi;

							// close focused dropdowns when navigating to another cell
							if (gridApi.cellNav) {
								gridApi.cellNav.on.navigate(scope, function () {
									scope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT);
									$('.subform-overlay').remove();
									$('.memo-overlay').remove();
								});
							}

							// load more data if scrolled to end
							gridApi.infiniteScroll.on.needLoadMoreData(scope, function () {
								if (isSubform) {
									subformLoadMoreData();
								} else {
									//Tableview. Obsolete
									scope.$emit("infiniteScrollneedLoadMoreData");								
								}
							});
								
							gridApi.core.on.rowsRendered(scope, function () {
								updateGridRowColors();

								// NUCLOS-5097 causes unnecessary scrolling after initial loading of page - seems not to be necessary anymore
								// scope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT);

								$('.subform-overlay').remove();
								$('.memo-overlay').remove();
								// clear subform dropdown lov-overlays or memo overlays
								$('#appendToBodyDiv').empty();
							});

						  
							gridApi.core.on.sortChanged(scope, function (grid, sortColumns) {
								//TODO: Looks like this is only user for internal sorting, which is obsolete since paging
								scope.gridOptions.enableSorting = true;

								// update sorting
								for (var column of grid.columns) {
									if (column.field !== 'selectionRowHeaderCol' && column.field !== 'edit') {

										let optionColDef = grid['options'].columnDefs.filter(function (elem) {
											if (elem.field === column.field) {
												return true;
											}
										})[0];

										if (!optionColDef) {
											continue;
										}

										optionColDef.sort = column.sort;
									}
								}

								let sortArray = [];
								for (let column of sortColumns) {
									let columnName = scope.gridApi.grid.appScope['fields'][column.field].uid;
									let sortOrder = sortColumns[0].sort.direction;
									sortArray.push(columnName + ' ' + sortOrder)
								}
								
								var sortExpression = sortArray.join(',');
								
								if (isMainform) {
									// serverside sorting						
									dataService.getMaster().sortExpression = sortExpression;
									$rootScope.$broadcast('reloadtabledata');

								} else {
									//Subform serverside sorting
									var subbos = master.selectedBo.subBos[scope.reffield];
									
									//NUCLOS-5047 6) If there is edited data, dont's change sorting.
									for (let subbo of subbos['bos']) {
										if (subbo._flag) {
											return;
										}
									}
									
									loadSubformData(subbos, 0, sortExpression);
								}
								
								// scroll to first row
								scope.gridApi.core.scrollTo(scope.gridOptions.data[0], column);
							});

						},
						columnDefs: [],
						data: []
					};

					/**
					 * initialize grid
					 * @param {boolean} [forceLoadData] if true load subform data from restservice
					 */
					let initGrid = function(forceLoadData) {
						let boMetaId = scope.bometaid;
						scope.entity = dataService.getMaster();

						ncUiGridCtrl.setBoMeta(master.meta);

						let parentUID = master.boMetaId;
						if (isMainform) { // tableview
							layoutService.tableviewlayout(boMetaId).then(function (layout) {
								scope.layout = layout;
								metaService.getBoMetaData(master.boMetaId).then(function (boMeta) {
									initTable(scope.subform, boMeta, layout, forceLoadData);
								});
							});
						} else { // subform
							layoutService.tableviewlayoutsubform(parentUID, boMetaId).then(function(layout) {
						    	scope.layout = layout;
								metaService.getBoMetaData(master.boMetaId, scope.subform.reffield).then(function (boMeta) {
									initTable(scope.subform, boMeta, layout, forceLoadData);
									ncUiGridCtrl.setSubformBoMeta(scope.subform);
								});
							});
						}
					};

					// wait until data is loaded in tableview
					let removeDataWatcher = scope.$watch('bodata.bos', (newValue: string) => {
						if (newValue && newValue.length > 0) {
							scope.gridOptions.data = scope.bodata.bos;
							removeDataWatcher(); // remove watcher
						}
					});

					//NUCLOS-4984 - The following watch 'bometaid' and listener on broadcast 'refreshgrids' can
					//create a running condition because they are called simultaneously when changing to selectedBo.
					//The emitting broadcast in sideview.controller.js line 115 switchedd off.

					// wait until model is loaded, this watch is triggered when tab is changed
					let removeWatcher = scope.$watch('bometaid', (newValue: string) => {
						if (newValue.length > 0) {
							// wait until api is registered
							$timeout(() => {
								initGrid(false);
							});
							removeWatcher(); // remove watcher
						}
					});

					scope.$on('refreshgrids', function () {
						boService.subformTabInfo(scope.master.selectedBo);
						initGrid(true);

						// this leads to unwanted scrolling in tall layouts 
						// scope.$broadcast(uiGridEditConstants.events.END_CELL_EDIT); // close edit cell templates
					});

					scope.$on('$locationChangeSuccess', (angularEvent, newUrl) => {
						if (newUrl.indexOf('/logout') == -1) { // don't load data if logout is clicked
							initGrid(false);
						}            
					});

					if (isSubform) {
						scope.$watch('master.selectedBo.canWrite', (newValue: string, oldValue: string) => {
							if (!_isSubformReadonlyFunc.initialized) {
								return;
							}
							scope.isSubformReadonly = _isSubformReadonlyFunc.func();
							for (var i in scope.gridOptions.columnDefs) {
								_enableCellEditForColumnDef(scope.gridOptions.columnDefs[i]);
							}
						});
					}

    				/**
    				 * open modal for new subbo
    				 */
    				scope.newSubboInGrid = function(subform) {

                        scope.gridOptions.enableSorting = false;

    	            	let selectedBo = scope.entity.selectedBo;

    	            	boService.createEmptySubBoIfNotThere(selectedBo, subform.reffield, true);

    	            	let subEntity = {
	    					boMetaId: subform.boMetaId,
	                    	bos: selectedBo.subBos[subform.reffield].bos,
	                    	meta: selectedBo.submetas[subform.boMetaId],
    	            	};

    	            	boModalService.openEditModal({
    	            		entity: subEntity,
    	            		parentEntity: scope.entity,
    	            		scope: scope,
	    	            	callback: {
	    	            		ok: function() {
	    	            			updateGridRowColors();
	    	            		}
	    	            	}
    	            	});

                        // focus first field of new row
                        $timeout(function() {
                            scope.gridApi.cellNav.scrollToFocus(scope.gridOptions.data[0], scope.gridOptions.columnDefs[0]);
                        }, 100);

    	            };

    	            /**
    	             * subform entries will be markes as deleted
    	             * mainform entries will be deleted immediatelly
    	             */
    	            let deleteSelectedBos = function(gridApi, subform) {
						let selectedGridRows = gridApi.selection.getSelectedGridRows();
						let bos = subform ? dataService.getMaster().selectedBo.subBos[subform.reffield].bos : dataService.getMaster().bos;
                        for (let selectedGridRow of selectedGridRows) {
							let bo = selectedGridRow.entity;
							if (bo._flag && bo._flag === 'insert') { // remove new unsaved entries
								let arrayIndex = bos.indexOf(bo);
								bos.splice(arrayIndex, 1);
							} else { // mark entry as deleted
								bo._flag = 'delete';
							}

	    					if (isMainform) {
	    						// delete entries
    							boService.submitBOForDelete(bo, dataService.getMaster()).then(function (data) {
	    							// ok
	    							$rootScope.$broadcast('reloadtabledata');
	    						}, function () {
	    							// not ok
	    						});
	    					} else {
	    						dataService.getMaster().dirty = true;
	    					}
						}

						updateGridRowColors();
    	            };

    				scope['exportSubform'] = function(subform, reffield) {
						// open dialog
						var modalInstance = $uibModal.open({
							templateUrl: 'app/sideview/list-export-dialog.html',
							controllerAs: 'vm',
							controller: function ($scope: ng.IScope, $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance, boService: nuclos.core.BoService) {

								var vm = this;
								vm.close = function () {
									$uibModalInstance.close();
								};

								vm.executeListExport = function () {
									boService.boListExport(
										dataService.getMaster().selectedBo.boId as number,
										reffield,
										scope.gridOptions.columnDefs
											.filter(function(column) { return column['fieldName'] !== undefined; })
											.map(function(column) { return subform.boMetaId + '_' + column['fieldName']; }),
										vm.format,
										vm.pageOrientation === 'landscape',
										vm.isColumnScaled
									).then(function(data) {
										vm.exportLink = data['links'].export.href;
										$window.location.href = data['links'].export.href;
									});
								};

							}
						});

    				};

    				scope.deleteSelectedBosInGrid = function(gridApi, subform) {
    					if (isMainform) {
    						dialog.confirm(
    							nuclosI18N.getI18n("webclient.button.delete"),
								nuclosI18N.getI18n("webclient.dialog.delete"),
								function() {
									deleteSelectedBos(gridApi, null);
								},
								function() {
									// TODO dismiss callback???
								}
							);
						} else {
							deleteSelectedBos(gridApi, subform);
						}
					};

					let cloneSelectedBos = function (gridApi, subform) {

						let getAutonumberMaxValue = function (subbos, fieldname) {
							let max = 0;
							for (let subbo of subbos) {
								let value = subbo.attributes[fieldname];
								max = value > max ? value : max;
							}
							return max;
						};

						let submeta = master.selectedBo.submetas[subform.boMetaId];
						let selectedGridRows = gridApi.selection.getSelectedGridRows();
						let bos = subform ? dataService.getMaster().selectedBo.subBos[subform.reffield].bos : dataService.getMaster().bos;
						for (let selectedGridRow of selectedGridRows) {
							let bo = selectedGridRow.entity;

							let boCopy = angular.copy(bo);
							boCopy._flag = 'insert';
							bos.unshift(boCopy); // insert at first position

							// increment autonumbers
							for (let attribute of (submeta.attributes as Array<any>)) {
								if (attribute.defcomptype && attribute.defcomptype === 'Autonummer') {
									let max = getAutonumberMaxValue(bos, attribute.boAttrName);
									boCopy.attributes[attribute.boAttrName] = max + 1;
								}
							}

							dataService.getMaster().dirty = true;
						}

						updateGridRowColors();
					};

					scope.cloneSelectedBosInGrid = function(gridApi, subform) {
                        scope.gridOptions.enableSorting = false;
                        if (!isMainform) {
                            cloneSelectedBos(gridApi, subform);
                        }
					};

					scope.editRowInNewTab = function(bo) {
						let boMetaId = bo.boMetaId;
						let meta = master.selectedBo.submetas[boMetaId];
						ncGridUtils.editRowInNewTab(bo, meta);
					};

					scope.editRow = function(bo) {
						let boMetaId = bo.boMetaId;
						let boList;
						let meta: Meta;
						if (isSubform) {
							boList = master.selectedBo.subBos[scope.reffield].bos;
							meta = master.selectedBo.submetas[boMetaId];
						} else {
							master.selectedBo = bo;
							boList = master.bos;
							meta = master.meta;
						}

						ncGridUtils.editRow(
							bo,
							master,
							boList,
							meta,
							isSubform,
							scope.reffield
						);

					};

		        }
		    };
        }
    };
});
