module nuclos.grid {
	/**
	 * @ngdoc directive
	 * @name global.directive:ncUiGridDateEditor
	 * @scope
	 * @restrict A
	 *
	 * @description
	 * directive for handling dates in ui-grid
	 * supports cell navigation
	 * derived from uiGridEditor directive
	 *
	 *
	 */
	nuclosApp.directive(
		'ncUiGridDateEditor',
		[
			'uiGridConstants',
			'uiGridEditConstants',
			function (uiGridConstants, uiGridEditConstants) {
				return {
					// scope: true,
					require: ['?^uiGrid',
						'?^uiGridRenderContainer'],
					compile: function () {
						return {
							pre: function ($scope, $elm, $attrs) {
								$scope.datePickerOptionsGrid = {
									formatYear: 'yy',
									startingDay: 1,
								};
							},
							post: function ($scope, $elm, $attrs, controllers) {
								var uiGridCtrl, renderContainerCtrl;
								if (controllers[0]) {
									uiGridCtrl = controllers[0];
								}
								if (controllers[1]) {
									renderContainerCtrl = controllers[1];
								}

								// set focus at start of edit
								$scope.$on(
									uiGridEditConstants.events.BEGIN_CELL_EDIT,
									function () {
										$elm[0].focus();
										$elm[0].select();
									}
								);

								$scope.stopEdit = function (evt) {
									if ($scope.inputForm && !$scope.inputForm.$valid) {
										evt.stopPropagation();
										$scope.$emit(uiGridEditConstants.events.CANCEL_CELL_EDIT);
									} else {
										$scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
									}
								};

								$elm.on(
									'keydown',
									function (evt) {
										switch (evt.keyCode) {
											case uiGridConstants.keymap.ESC:
												evt.stopPropagation();
												$scope.$emit(uiGridEditConstants.events.CANCEL_CELL_EDIT);
												break;
											case uiGridConstants.keymap.SPACE:
												$scope.opened = true;
										}

										evt.uiGridTargetRenderContainerId = renderContainerCtrl.containerId;
										if (uiGridCtrl.cellNav.handleKeyDown(evt) !== null) {
											$scope.stopEdit(evt);
										}

										return true;
									}
								);

							}
						};
					}
				};
			}
		]
	);
}
