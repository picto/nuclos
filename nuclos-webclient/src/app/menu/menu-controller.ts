module nuclos.menu {

	export class MenuCtrl {
		constructor($scope,
					$http,
					$location,
					$cookieStore,
					$resource,
					restservice,
					menuService,
					dataService,
					dialog,
					authService: nuclos.auth.AuthService) {
			setTitle($cookieStore);
			$scope.isTaskList = $location.path() === '/tasks';
			$scope.mMenuLabel = $scope.isTaskList ? 'Tasks' : 'Menu';
			$scope.mMenuIcon = $scope.isTaskList ? 'glyphicon-bookmark' : 'glyphicon-book';
			$scope.oMenuPath = $scope.isTaskList ? '#/menu' : '#/tasks';
			$scope.username = $cookieStore.get('authentication') ? $cookieStore.get('authentication').username : '';
			var viewType = localStorage.getItem("selectedViewType") == "tableview" ? "tableview" : "sideview";
			$scope.viewLink = $scope.isTaskList ? '#/taskview' : '#/' + viewType;

			$scope.showlivesearch = false;
			restservice.systemparameters().then(function (data) {
				$scope.showlivesearch = data.FULLTEXT_SEARCH_ENABLED;
			});

			$scope.master = dataService.getMaster();

			// saving the links in a cookie leads to problems if the rest host changes
			//
			//var authentication = $cookieStore.get('authentication');
			//var metalink = $scope.isTaskList ? authentication.links.tasks : authentication.links.menu;
			//$scope.searchlink = authentication.links.search;

			var metalink = {href: restHost + ($scope.isTaskList ? '/meta/tasks' : '/meta/menu')};
			$scope.searchlink = restHost + '/data/search/{text}';
			$scope.authentication = $cookieStore.get('authentication');

			$scope.livesearchchanged = function (searchtext) {
				if (searchtext && searchtext.length >= 2) {
					var searchlink = $scope.searchlink.replace('{text}', searchtext);
					$http.get(searchlink).success(function (data) {
						var newSearchlink = $scope.searchlink.replace('{text}', searchtext);
						if (newSearchlink === searchlink) {
							$scope.results = data;
						} else {
							$scope.livesearchchanged();
						}
					}).error(function (jqxhr, textStatus) {
						errorhandler.show(jqxhr, textStatus, $cookieStore);
					});
				} else {
					$scope.results = [];
				}
			};

			$scope.expandMenu = function () {
				$scope.maxMenuEntries += 2;
			};

			$scope.collapseMenu = function () {
				$scope.maxMenuEntries = defMaxMenuEntries;
			};

			$scope.toggleMenuItem = function (menu) {
				menuService.toggleMenuItem(menu.path, menu.hidden);
				menu.hidden = !menu.hidden;
			};

			authService.getLegalDisclaimers().then(function (data) {
				$scope.legalDisclaimers = data.length > 0 ? data : undefined;
			});

			menuService.getMenu(metalink.href).then(function (data) {

					if ($scope.isTaskList) {
						if (!data) {
							window.location.href = $scope.oMenuPath;
							return;
						}

						// NUCLOS-5422 - Dynamische Aufgabenlisten auch im Webclient und iOS-Client anbieten
						// TODO wird derzeit noch nicht unterstützt

						for (let menu of data) {
							menu.entries = menu.entries.filter(menuEntry => menuEntry.taskMetaId.length > 0);
						}
						data = data.filter(menu => menu.entries.length > 0);


						if (data.length === 0) {
							window.location.href = $scope.oMenuPath;
							return;
						}
					}

					$scope.menues = data;

					$scope.entities = [];
					for (var i in data) {
						for (var j in data[i].entries) {
							if ($scope.isTaskList) {
								var task_meta = data[i].entries[j];
								task_meta.searchFilterId = task_meta.taskMetaId;
								$scope.entities.push(task_meta);
							} else {
								$scope.entities.push(data[i].entries[j]);
							}
						}
					}
				},
				function (error) {
					if (error && error.status == 401) { // unauthorized
						$cookieStore.remove('authentication');
						$location.path('login');
					}
				}
			);


			$scope.openDashboard = function () {
				if ($location.url() === '/menu') {
					// dashboard is already shown in menu
					return;
				}
				var $uibModal = angular.element(document.body).injector().get('$uibModal');
				$uibModal.open({
					templateUrl: 'app/dashboard/dashboard-modal.html',
					controller: nuclos.dashboard.DashboardModalCtrl,
					controllerAs: 'vm',
					size: 'lg',
					windowClass: 'fullsize-modal-window',
					resolve: {}
				}).result.then(function () {
					// ok
				});
			};

		}
	}

	nuclosApp.controller('MenuCtrl', MenuCtrl);
}
