module nuclos.dev.rest {
	var restServiceResponse; // for debugging

	export class RestExplorerCtrl {
		restservices = null;

		constructor(private $scope,
					private $http,
					private $location,
					private $cookieStore,
					private $timeout,
					private $window,
					private $route,
					private $localStorage,
					private restservice,
					private restcallHistory,
					private restUtils: nuclos.dev.rest.RestCallUtil,
					private authService: nuclos.auth.AuthService,
					private errorhandler: Errorhandler) {

			$scope.restserviceFilter = localStorage.getItem('restserviceFilter') ? localStorage.getItem('restserviceFilter') : '';
			$scope.$watch('restserviceFilter', function (newValue, oldValue) {
				localStorage.setItem('restserviceFilter', newValue);
			}, true);

			var self = this;

			restservice.restservicelist().then(function (data) {
				this.restservices = data;

				$scope.selectRestServiceClickHandler = function (index) {
					$scope.prepareRestServiceExecution(this.restservices[index]);
					$location.path("devutils/restexplorer/" + index, false);
				}

				$scope.selectRestServiceHistoryClickHandler = function (index) {
					$scope.selectRestServiceFromHistory($scope.storage.restcallHistory.length - 1 - index);
					$location.path("devutils/restexplorer/history/" + index, false);
				}


				$scope.executeRestCallsByPathParams = function () {
					var restserviceIndex = $route.current.pathParams !== null ? $route.current.pathParams.restserviceIndex : null;
					if (restserviceIndex != null) {
						// $scope.prepareRestServiceExecution(restservices[restserviceIndex]); <-- this doesn't work
						$timeout(function () {
							if (this.restservices != null) {
								angular.element($("#main")).scope()['prepareRestServiceExecution'](this.restservices[restserviceIndex]);
							} else {
								$timeout(function () {
									angular.element($("#main")).scope()['prepareRestServiceExecution'](this.restservices[restserviceIndex]);
								}, 1000);
							}
						}, 0);
					}

					if ($route.current.pathParams.restservicePath != null) {
						var restCall = restUtils.getRestCall($route.current.pathParams.restservicePath, "GET", null, null, this.restservices);
						// $scope.prepareRestServiceExecution(restCall); <-- this doesn't work
						$timeout(function () {

							// call prepareRestServiceExecution to make $scope.selectedRestService available
							angular.element($("#main")).scope()['prepareRestServiceExecution'](restCall);

							// parse query string
							var queryParameters = URI(location.href.substring(location.href.indexOf('http', 1))).query(true);
							if (JSON.stringify(queryParameters) !== "{}") {
								$scope.selectedRestService.requestParams = queryParameters;

								// kann man vielleicht noch optimieren:
								// call prepareRestServiceExecution again with parsed query parameters
								angular.element($("#main")).scope()['prepareRestServiceExecution'](restCall);
							}

						}, 0);
					}

					if ($route.current.pathParams.historyIndex != null && this.restservices != null) {
						// $scope.selectRestServiceFromHistory($route.current.pathParams.historyIndex); <-- this doesn't work
						$timeout(function () {
							angular.element($("#main")).scope()['selectRestServiceFromHistory']($route.current.pathParams.historyIndex);
						}, 0);
					}
				}

				$scope.executeRestCallsByPathParams();

				var lastRestserviceIndex = $route.current.pathParams !== null ? $route.current.pathParams.restservice : null;
				var locationChangeWatcher = $scope.$on('$locationChangeSuccess', function (event) {

					var restserviceIndex = $route.current.pathParams !== null ? $route.current.pathParams.restservice : null;
					if (restserviceIndex == null) {
						locationChangeWatcher(); // remove locationChangeWatcher
					}
					$scope.executeRestCallsByPathParams();

					$timeout(function () {
						$('.btn-group').removeClass('open');
					}, 500);
				});


				var sessionID = self.authService.getSessionId();
				if (!sessionID) {
					var authentication = self.$cookieStore.get('authentication');
					if (authentication) {
						delete authentication['sessionId'];
						self.$cookieStore.put('authentication', authentication);
					}

					window.location.href = '#/';
					return;
				}
				setTitle($cookieStore);
				$scope.sessionID = sessionID;
				$scope.isTaskList = $location.path() === '/tasks';
				$scope.mMenuLabel = 'Menu';
				$scope.mMenuIcon = 'glyphicon-book';
				$scope.oMenuPath = '#/devutils/restexplorer';
				$scope.viewLink = '#/sideview';

				$scope.selectedRestService = null;

				$scope.pathParams = [];

				$scope.executeAutomatically = true;

				$scope.storage = $localStorage;

				$scope.accordion = [{open: true}, {open: false}];

				$scope.clearRestCallHistory = function () {
					restcallHistory.clear();
					$scope.accordion[1].open = !$scope.accordion[1].open; // reopen accordion after clear click
				}


				function getBasePath(path) {
					if (path == null)
						return null;
					if (path.indexOf('{') == -1)
						return path;
					return path.substring(0, path.indexOf('{'));
				}

				function equalsRestserviceCall(a, b) {
					if (a == null && b == null)
						return true;
					if (a == null && b != null || a != null && b == null)
						return false;
					var basepathA = getBasePath(a.path);
					var basepathB = getBasePath(b.path);
					var pathParamsALength = a.pathParams != null ? a.pathParams.length : 0;
					var pathParamsBLength = b.pathParams != null ? b.pathParams.length : 0;
					if (
						basepathA == basepathB
						&& a.method.toLowerCase() == b.method.toLowerCase()
						&& pathParamsALength == pathParamsBLength
					)
						return true;
				}

				$scope.selectRestServiceFromHistory = function (restserviceCallIndex) {
					var restserviceCall = $scope.storage.restcallHistory[restserviceCallIndex];
					var path = restserviceCall.path;
					for (var i = 0; i < this.restservices.length; i++) {
						var restservice = this.restservices[i];

						if (equalsRestserviceCall(restserviceCall, restservice)) { // matched
							var localStorageKey = 'pathParams' + restservice.path.hashCode() + '_' + restservice.method; // TODO refactor
							if (restserviceCall.pathParams != null) {
								localStorage.setItem(localStorageKey, JSON.stringify(restserviceCall.pathParams));
							}

							localStorageKey = 'postData' + restservice.path.hashCode() + '_' + restservice.method; // TODO refactor
							if (restserviceCall.postData != null) {
								localStorage.setItem(localStorageKey, JSON.stringify(restserviceCall.postData));
							}

							localStorageKey = 'requestParams' + restservice.path.hashCode() + '_' + restservice.method; // TODO refactor
							if (restserviceCall.requestParams != null) {
								localStorage.setItem(localStorageKey, JSON.stringify(restserviceCall.requestParams));
							}

							var restserviceObject = restservice;
							restserviceObject.requestParams = restserviceCall.requestParams;
							$scope.prepareRestServiceExecution(restserviceObject);
							break;
						}
					}

				}


				$scope.prepareRestServiceExecution = function (restServiceObject) {
					$scope.restServiceResponse = '';

					window.document.title = restServiceObject.path;

					$scope.selectedRestService = restServiceObject;
					if (restServiceObject.examplePostData != null) {
						$scope.selectedRestService.postData = restServiceObject.examplePostData;
					}
					$scope.restServiceDescription = $scope.selectedRestService.description;

					// get parameters / post data from local storage
					var localStorageKey = 'pathParams' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
					if (restServiceObject.pathParams != null && restServiceObject.pathParams.length != 0 && restServiceObject.pathParams[0].name != null) { // komplettes pathParams Objekt wird übergeben, nicht nur String Array
						$scope.pathParams = restServiceObject.pathParams;
					} else {
						if (localStorage.getItem(localStorageKey) != null) {
							$scope.pathParams = JSON.parse(localStorage.getItem(localStorageKey));
						} else {
							$scope.pathParams = [];
						}
						localStorageKey = 'postData' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
						if (localStorage.getItem(localStorageKey) != null) {
							$scope.selectedRestService.postData = localStorage.getItem(localStorageKey);
						}


						localStorageKey = 'requestParams' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
						if (localStorage.getItem(localStorageKey) != null) {
							var reqParams = localStorage.getItem(localStorageKey);
							$scope.selectedRestService.requestParams = JSON.parse(reqParams);
						}

					}

					updatePathInutFields($scope.selectedRestService, $scope.pathParams);

					if ($scope.pathParams == null
						|| $scope.pathParams.length == 0 || $scope.pathParams[0].value == ""
					) {
						// extract pathParams from rest service
						var tokens = $scope.selectedRestService.path.match(/{.+?}/g);
						if (tokens) {
							$scope.pathParams = [];
							for (var i = 0; i < tokens.length; ++i) {
								var token = tokens[i].substring(1).substring(0, tokens[i].length - 2);
								var tokenExists = false;
								for (var j in $scope.pathParams) {
									if ($scope.pathParams[j].name == token) {
										tokenExists = true;
										break;
									}
								}
								if (!tokenExists) {
									var elem = {name: token, value: ""};
									$scope.pathParams.push(elem);
								}
							}
						}
					}


					//		$("body,html").animate({scrollTop: 0});

					if ($scope.executeAutomatically) {
						var doExecute = true;
						for (var j in $scope.pathParams) {
							var param = $scope.pathParams[j];
							if (param.value == null || param.value == '') {
								doExecute = false;
								break;
							}
						}
						if ($scope.selectedRestService.method != 'GET') {
							doExecute = false;
						}
						if ($scope.selectedRestService.returnType == 'Response') {
							doExecute = false;
						}
						if (doExecute) {
							$scope.executeRestService();
						}
					}

					updateHeight();
				}

				function updatePathInutFields(selectedRestService, pathParams) {
					if (selectedRestService != null && pathParams != null) {

						selectedRestService.translatedPath = selectedRestService.path;
						selectedRestService.translatedPathUI = '<div>' + selectedRestService.path + '</div>';
						for (var i in pathParams) {
							var param = pathParams[i];
							selectedRestService.translatedPath = selectedRestService.translatedPath.replace('{' + param.name + '}', param.value);
						}

						$scope.pathElements = [];
						var s = "/rest" + selectedRestService.path;

						$scope.pathElements = self.getPathElementsFromPath(s, pathParams);

						// adjust the path input fields length
						$timeout(function () {
							$('.restexplorer .path-input').each(function (index) {
								var element = $(this);
								var inputW = (element.val().length) * 7;
								if (inputW < 12)
									inputW = 12;
								if (inputW > 300)
									inputW = 300;
								element.css('width', inputW + 'px');
							});
						}, 0);

					}
				}

				// update translated path, when parameter changes
				$scope.$watch('pathParams', function (newValue, oldValue) {
					updatePathInutFields($scope.selectedRestService, $scope.pathParams);
				}, true);

				function saveInputFieldsToLocalStorage() {
					if ($scope.selectedRestService != null) {
						if ($scope.pathParams != null && $scope.pathParams.length != 0) {
							var localStorageKey = 'pathParams' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
							localStorage.setItem(localStorageKey, JSON.stringify($scope.pathParams));
						}
						if ($scope.selectedRestService.postData != null) {
							var localStorageKey = 'postData' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
							localStorage.setItem(localStorageKey, $scope.selectedRestService.postData);
						}
						if ($scope.selectedRestService.requestParams != undefined) {
							var localStorageKey = 'requestParams' + $scope.selectedRestService.path.hashCode() + '_' + $scope.selectedRestService.method;
							log.orange("localStorageKey", localStorageKey)
							localStorage.setItem(localStorageKey, JSON.stringify($scope.selectedRestService.requestParams));
						}

					}
				}

				$scope.executeRestServiceInNewTab = function () {
					$window.open(restHost + $scope.selectedRestService.translatedPath + '?sessionid=' + sessionID);
				}

				$scope.executeRestService = function () {
					if ($scope.selectedRestService.method == "POST" || $scope.selectedRestService.method == "PUT") {

						var endpoint = restHost + $scope.selectedRestService.translatedPath;
						if ($scope.selectedRestService.postData == null) {
							alert("No post data!");
							return;
						}
						var postData = $scope.selectedRestService.postData;

						var config = sessionHeader();
						config.headers.RESTCallLogging = 'off'; // don't log REST-Calls if they are created by REST-Explorer

						var httpCall = $scope.selectedRestService.method == "POST" ? $http.post : $http.put;

						httpCall(endpoint, postData, config)
							.success(
								function (data) {
									displayRestResponse(data);
								}
							).error(function (data, status) {
							console.error(data)
							console.error(status)

							$scope.restServiceResponse = status
							errorhandler.show(data, status, $cookieStore);
						});

					} else {
						var configuration = sessionHeader();
						configuration.headers.RESTCallLogging = 'off'; // don't log REST-Calls if they are created by REST-Explorer
						configuration.url = restHost + $scope.selectedRestService.translatedPath;
						configuration.method = $scope.selectedRestService.method;
						configuration.params = $scope.selectedRestService.requestParams;
						$http(configuration)
							.success(function (data) {
								displayRestResponse(data);
							}).error(function (jqxhr, textStatus) {
							errorhandler.show(jqxhr, textStatus, $cookieStore);
						});

					}
				}


				var updateHeight = function () {
					/*
					 // scrollable list
					 $('.restservicelist').height($(window).height() - $('.restservicelist').position().top - 10);
					 $('.restServiceResponse').height($(window).height() - $('.restservicelist').position().top - $('#selectedRestService').height() - 30);
					 $(window).resize(function() {
					 if($('.restservicelist').length != 0) {
					 $('.restservicelist').height($(window).height() - $('.restservicelist').position().top - 10);
					 $('.restServiceResponse').height($(window).height() - $('.restservicelist').position().top - $('#selectedRestService').height() - 30);
					 }
					 });
					 */
				}

				var displayRestResponse = function (data) {
					restServiceResponse = data;
					saveInputFieldsToLocalStorage();
					if (data.length != 0) {
						$scope.restServiceResponse = JSON.stringify(data, null, 4);
						$scope.restServiceResponse = $scope.restServiceResponse.replace(/"_image": "(.*)"/g, "\"_image\": ◉");
					} else {
						$scope.restServiceResponse = "no data";
					}
					$timeout(function () {
						self.addLinksToRestResponse();
					}, 1500);
				}

				updateHeight();


			});

		}

		selectRestServicePreviewClickHandler(url) {
			var restservice = angular.element(document.body).injector().get('restservice');
			restservice.restservicelist().then(restservices => {
				var restCall = this.restUtils.getRestCall(url, "GET", null, null, restservices);

				angular.element($("#main")).scope()['prepareRestServiceExecution'](restCall);
				var $location = angular.element(document.body).injector().get('$location');
				$location.path("devutils/restexplorer/preview/" + url, false);
			});
		}

		addLinksToRestResponse() {

			// add a link to each text element with http://...
			$.each($('.hljs-string'), (index, elem) => {
				if ($(elem).text().indexOf('http://') != -1) {

					$(elem).css('border-bottom', '1px solid red');
					$(elem).css('border-bottom', '1px solid red');

					$(elem).click(() => {
						var url = $(elem).text().replace(/\"/g, '');
						var restCall = this.restUtils.getRestCall(url, "GET", null, null, restservices);
						if (restCall != null) {
							this.selectRestServicePreviewClickHandler(url);
						} else {
							alert("Unable to find matching restservice.");
						}
					});
				}
			});
		}

		getPathElementsFromPath(s, pathParams) {
			var tokens = s.match(/{.+?}/g);
			var tokenArray = [];
			if (tokens) {
				for (var i = 0; i < tokens.length; ++i) {
					var token = tokens[i].substring(1).substring(0, tokens[i].length - 2);
					var index = s.indexOf(tokens[i]);
					var staticPartOfPath = s.substring(0, index);
					tokenArray.push(staticPartOfPath);
					s = s.substring(index + tokens[i].length);

					var val;
					for (var j in pathParams) {
						var param = pathParams[j];
						param.placeholder = "{" + param.name + "}";
						if (param.name == token) {
							val = param;
							break;
						}
					}
					if (val != null && val.length != 0) {
						tokenArray.push(val);
					}
				}
				if (s.length != 0)
					tokenArray.push(s);
			} else {
				tokenArray.push(s);
			}
			return tokenArray;
		}
	}


}