module nuclos.dev.rest {

	var maxItemsInHistory = 50;

	nuclosApp.factory('restcallHistory', function ($cookieStore,
												   $cacheFactory,
												   $q,
												   $localStorage,
												   restUtils: nuclos.dev.rest.RestCallUtil) {
		return {
			push: function (restcallEntry) {
				if ($localStorage.restcallHistory == null)
					$localStorage.restcallHistory = [];

				// limit number of history entries
				if ($localStorage.restcallHistory.length > maxItemsInHistory) {
					$localStorage.restcallHistory = $localStorage.restcallHistory.slice(1);
				}
				var restcallHistoryLocalStorage = localStorage.getItem('ngStorage-restcallHistory');
				if (restcallHistoryLocalStorage && restcallHistoryLocalStorage.length > 10000) { // textlength
					$localStorage.restcallHistory = $localStorage.restcallHistory.slice($localStorage.restcallHistory.length - maxItemsInHistory / 2, $localStorage.restcallHistory.length);
				}


				if (restcallEntry.pathParams != null) {
					for (var i = 0; i < restcallEntry.pathParams.length; i++) {
						restcallEntry.pathParams[i].placeholder = '{' + restcallEntry.pathParams[i].name + '}';
					}
				}

				try {

					$localStorage.restcallHistory.push(restcallEntry);
				} catch (exception) {
					console.log("Unable to save REST Call History.");
					console.error(exception);
					$localStorage.restcallHistory.clear();
				}

				//colorize entries by date
				for (var i = $localStorage.restcallHistory.length - 10 > 0 ? $localStorage.restcallHistory.length - 10 : 0; i < $localStorage.restcallHistory.length; i++) {
					if ($localStorage.restcallHistory[i].date != null) {
						$localStorage.restcallHistory[i].color = restUtils.color(new Date($localStorage.restcallHistory[i].date));
					}
				}

			},

			list: function () {
				return $localStorage.restcallHistory;
			},

			clear: function () {
				$localStorage.restcallHistory = [];
			}

		}
	});

	export class RestCallUtil {
		colors = ['#AA0B08', '#AA6600', '#5000AA', '#09B737', '#0A7BC1'];
		lastColor = 0;
		lastDate = new Date();

		public color(d) {
			var time = d.getTime() - this.lastDate.getTime();
			var color = this.lastColor;
			if (time > 2000) {
				color++;
				if (color >= this.colors.length) {
					color = 0;
				}
			}
			this.lastDate = d;
			this.lastColor = color;
			return this.colors[color];
		}

		/*
		 *
		 */
		public getPathRegex(path) {
			return path
					.replaceAll('{boMetaId}', '[a-zA-Z_]*')
					.replaceAll('{boAttrId}', '[a-zA-Z_]*')
					.replaceAll('{boId}', '[0-9]*')
					.replaceAll('{pk}', '[0-9]*')
					.replaceAll('{stateId}', '[0-9]*')
					.replace(new RegExp('{[a-zA-Z0-9_]*}'), '[a-zA-Z0-9_]*')
				+ '$'
				;
		};

		public getRestCall(url, method, postData, requestParams, restservices) {

			if (method == null) {
				method = 'GET';
			}

			var path = url.substring(restHost.length);
			if (restservices != null) {
				var matchedCallEntries = [];
				for (var i in restservices) {
					var service = restservices[i];
					if (
						path.indexOf(service.basePath) != -1 // same path without parameters
						&& path.split('/').length == service.path.split('/').length // same number of parameters
						&& new RegExp(this.getPathRegex(service.path)).test(path) // match regex
						&& service.method.toLocaleLowerCase() == method.toLocaleLowerCase() // same HTTP method
					) { // matched
						var paramString = path.substring(service.basePath.length);
						var pathParams = [];
						var paramStringToken = paramString.split("\/");
						var paramIndex = 0;
						for (var p in paramStringToken) {
							// check if this token is a parameter or something like dependence in /rest/bo/{boMetaId}/{boId}/dependence/{reffield}
							if (this.getNonParamPathTokens(service.path).indexOf(paramStringToken[p]) == -1 && service.pathParams != null) {
								var param = {
									name: service.pathParams[paramIndex],
									value: paramStringToken[p]
								};
								pathParams.push(param);
								paramIndex++;
							}
						}

						if (requestParams === null) {
							requestParams = undefined;
						}

						var callEntry = new RestCall();
						callEntry.path = service.path;
						callEntry.date = new Date();
						callEntry.pathParams = pathParams;
						callEntry.postData = postData;
						callEntry.method = method;
						callEntry.requestParams = requestParams;

						matchedCallEntries.push(callEntry);
					}
				}
				if (matchedCallEntries.length == 1) {
					return matchedCallEntries[0];
				} else {
					var subPath = path;
					while (subPath.length > 0) {
						subPath = subPath.substring(0, subPath.lastIndexOf('/'));
						for (var i in matchedCallEntries) {
							var matchedCallEntry = matchedCallEntries[i];
							if (matchedCallEntry.path.indexOf(subPath) == 0) {
								return matchedCallEntry;
							}
						}
					}

					console.warn("Found %i REST calls.", matchedCallEntries.length);
					console.warn(matchedCallEntries, url);
				}
			}
		}

		public getNonParamPathTokens(path) {
			var result = [];
			var pathTokens = path.split('/')
			for (var p in pathTokens) {
				var pathToken = pathTokens[p];
				if (pathToken.length != 0 && pathToken.indexOf('{') != 0) {
					result.push(pathToken);
				}
			}
			return result;
		}
	}

	nuclosApp.service('restUtils', RestCallUtil);


// TODO log GET/POST/PUT/DELETE/...
	nuclosApp.config(function ($provide) {
		$provide.decorator('$resource', function ($delegate, $localStorage) {

			return function decoratedResource() {
				// Log Angular REST calls if set in devutils page
				if ($localStorage.logAngularRESTCalls) {
					if (arguments[0] != null && arguments[0].indexOf("/rest") != -1) {
						log.teal('[$resource]', arguments);
						logStackTrace();
					}
				}
				return $delegate.apply(this, arguments);
			};

		});
	});


	var RestCall = function () {
		var path, date, pathParams, postData, method;
	};


	/*
	 * delegate for saving http call info to local history
	 * https://www.exratione.com/2013/08/angularjs-wrapping-http-for-fun-and-profit
	 */
	nuclosApp.config(function ($provide) {
		$provide.decorator('$http', ['$delegate', '$localStorage', 'restcallHistory',
			function ($delegate, $localStorage, restcallHistory) {

				var $http = $delegate;

				var wrapper = function () {
					return $http.apply($http, arguments);
				};

				Object.keys($http).filter(function (key) {
					return (typeof $http[key] === 'function');
				}).forEach(function (key) {
					wrapper[key] = function () {

						if (!arguments[0]) {
							console.error("REST call with undefined url.");
							logStackTrace();
						}

						// add timestamp to prevent IE Browser caching of rest calls
						if (arguments[0] && arguments[0].indexOf("/rest/") != -1 && arguments[1]) {
							if (arguments[arguments.length - 1].params) {
								arguments[arguments.length - 1].params.d = new Date().getTime();
							} else {
								arguments[arguments.length - 1].params = {d: new Date().getTime()};
							}
						}

						// Log Angular REST calls if set in devutils page
						if ($localStorage.logAngularRESTCalls) {
							if (arguments[0] && arguments[0].indexOf("/rest") != -1) {
								log.teal('[' + key + ']', arguments);
								logStackTrace();
							}
						}


						if ($localStorage.logAngularRESTCallsContainingResponse) {
							// log angular HTTP response
							var response = $http[key].apply($http, arguments);
							if (arguments[0] && arguments[0].indexOf("/rest") != -1) {
								var request = arguments[0];

								var requestPayload = null;
								if (arguments[1] && !arguments[1].headers && !arguments[1].cache) {
									requestPayload = arguments[1];
								}

								response.then(function (responseData) {
									if (printJSON(responseData).indexOf($localStorage.logAngularRESTCallsContainingResponse) != -1) {
										log.cyan(' [' + key + '] (search for "' + $localStorage.logAngularRESTCallsContainingResponse + '")', request, 'payload:', requestPayload, 'response:', responseData);
										logStackTrace();
									}
								});
							}
							return response;
						}

						return $http[key].apply($http, arguments);

					};
				});
				return wrapper;
			}]);
	});
}
