module nuclos.dev.rest {
	nuclosApp.directive('jsonText', function () {
		return {
			restrict: 'A',
			require: 'ngModel',
			link: function (scope, element, attr, ngModel) {
				function into(input) {
					try {
						return JSON.parse(input);
					} catch (exception) {
						// unable to parse
					}
					return "";
				}

				function out(data) {
					return JSON.stringify(data);
				}

				ngModel['$parsers'].push(into);
				ngModel['$formatters'].push(out);
			}
		};
	});
}