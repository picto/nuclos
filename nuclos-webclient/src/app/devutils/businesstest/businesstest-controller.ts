var oboe;
module nuclos.businesstest {

	import IHttpPromise = angular.IHttpPromise;
	import IDeferred = angular.IDeferred;
	import IPromise = angular.IPromise;
	export interface IBusinessTestVO {
		state: any;
		duration: number;
		description: string;
		source: string;
		entity: any;
		nuclet: any;
		startdate: any;
		result: string;
		stacktrace: string;
		warningCount: number;
		errorCount: number;
		enddate: any;
		masterDataVO: any;
		name: string;
		primaryKey: any;
		id: any;
		version: number;
		createdBy: string;
		createdAt: number;
		changedAt: number;
		changedBy: string;
		removed: boolean;
		warningLine: number;
		errorLine: number;
	}

	export interface BusinessTestOverallResult {
		state: string;
		testsTotal: number;
		testsRed: number;
		testsYellow: number;
		testsGreen: number;
		warningCount: number;
		errorCount: number;
		start: any;
		end: any;
		duration: number;
	}

	export interface IBusinessTestCtrl {
	}

	export class BusinessTestCtrl {

		ctrl: IBusinessTestCtrl = this;
		tests: Array<IBusinessTestVO> = [];
		overallResult: BusinessTestOverallResult;
		executionResult: string = '';
		started: boolean = false;
		progress: number = 0;

		constructor(private $http: ng.IHttpService,
					private $resource: ng.resource.IResourceService,
					private $timeout: ng.ITimeoutService,
					private dialog: nuclos.dialog.DialogService,
					private $q: ng.IQService,
					private nuclosI18N: nuclos.util.I18NService,
					private $log: ng.ILogService) {
			this.fetchTests();
		};

		public fetchTests(callback?: Function) {
			this.$resource(restHost + '/businesstests/status').get(data => {
				this.overallResult = data;
				this.$log.debug(data);
				this.$resource(restHost + '/businesstests/').query(data => {
					this.tests = data;
					if (callback) {
						callback();
					}
				}, error => {
					this.$log.warn("Unable to get business tests: %o", error);
				});
			}, error => {
				this.$log.warn("Unable to get business test overall result: %o", error);
			});
		};

		public runAllTests(): IPromise<any> {
			let deferred = this.$q.defer();

			this.overallResult = null;
			this.executionResult = '';
			this.started = true;
			oboe({
				url: restHost + '/businesstests/run',
				withCredentials: true
			})
				.node('!.{message}', element => {
						var f = () => {
							this.$log.debug(element);
							this.executionResult += element.message;
							var output = $('#output');
							if (output.length)
								output.scrollTop(output[0].scrollHeight - output.height());
						};
						this.$timeout(f);
					}
				)
				.node('!.{progress}', element => {
						var f = () => {
							this.progress = Math.round(element.progress * 100);
							this.$log.debug('Progress: %o', this.progress);
						};
						this.$timeout(f);
					}
				)
				.done(data => {
					this.fetchTests(
						() => {
							this.started = false;
							this.progress = 0;
							deferred.resolve();
						}
					);
				})
				.fail(function (error) {
					this.$log.error('Running tests failed: %o', error);
					var f = () => {
						this.started = false;
						deferred.reject(error);
					};
					this.$timeout(f);
				});

			return deferred.promise;
		};

		public generateAllTests(): IPromise<any> {
			let deferred = this.$q.defer();

			this.executionResult = '';
			this.started = true;
			oboe({
				url: restHost + '/businesstests/generate',
				withCredentials: true
			})
				.node('!.{message}', element => {
						var f = () => {
							this.$log.debug(element);
							this.executionResult += element.message;
							var output = $('#output');
							if (output.length)
								output.scrollTop(output[0].scrollHeight - output.height());
						};
						this.$timeout(f);
					}
				)
				.node('!.{progress}', element => {
						var f = () => {
							this.progress = Math.round(element.progress * 100);
							this.$log.debug('Progress: %o', this.progress);
						};
						this.$timeout(f);
					}
				)
				.done(data => {
					this.fetchTests(
						() => {
							this.started = false;
							this.progress = 0;
							deferred.resolve();
						}
					);
				})
				.fail(function (error) {
					var f = () => {
						this.started = false;
						deferred.reject(error);
					};
					this.$timeout(f);
				});

			return deferred.promise;
		};

		public deleteTest(testId: string): IHttpPromise<any> {
			this.$log.debug('Deleting test ' + testId + '...');
			return this.$http.delete(restHost + '/businesstests/' + testId).success(() => {
				this.$log.debug('Test deleted');
				$('#test_' + testId).remove();
			}).error(error => {
				this.$log.debug('Could not delete test: ', error);
			});
		};

		public deleteAllTests(): void {
			this.dialog.confirm(
				this.nuclosI18N.getI18n("webclient.businesstests.delete.all"),
				this.nuclosI18N.getI18n("webclient.businesstests.delete.all.confirm"),
				() => {
					this.$log.debug('Deleting all tests...');
					var promises = [];
					this.tests.forEach(test => {
						this.$log.debug('Deleting Test: ' + test);
						promises.push(
							this.deleteTest(test.id.string)
						);
					});
					this.$q.all(promises).then(
						() => {
							this.fetchTests();
						}
					);
				},
				() => {
				}
			);
		};

		public generateAndRun(): void {
			this.generateAllTests().then(
				() => {
					this.runAllTests();
				}
			)
		}
	}
}
