"use strict";

var oboe;
module nuclos.businesstest {

	import IBusinessTestVO = nuclos.businesstest.IBusinessTestVO;
	import Editor = AceAjax.Editor;
	import IEditSession = AceAjax.IEditSession;
	import Range = AceAjax.Range;

	interface IBusinessTestDetailsRouteParams extends ng.route.IRouteParamsService {
		testId: string;
	}

	export class BusinessTestDetailsCtrl {

		ctrl: IBusinessTestCtrl = this;
		testId: string;
		test: any;
		new: boolean = false;
		started: boolean = false;
		log: string = '';

		editor: Editor // ACE editor
		marker: any; // ACE highlight marker
		session: IEditSession;

		constructor(private $http: ng.IHttpService,
					private $routeParams: IBusinessTestDetailsRouteParams,
					private $location: ng.ILocationService,
					private $scope,
					private $timeout,
					private $log: ng.ILogService) {
			this.testId = this.$routeParams.testId;
			this.new = this.testId == 'new';

			$scope.aceLoaded = (_editor: Editor): any => {
				this.$log.debug('Ace loaded %o', _editor);
				this.editor = _editor;

				// Editor part
				this.session = _editor.getSession();
				// var _renderer = _editor.renderer;

				this.session.setUseSoftTabs(false);

				_editor.setShowInvisibles(true);
				_editor.setFontSize('14');
				_editor.setWrapBehavioursEnabled(true);

				// Options
				this.session.setUndoManager(new ace.UndoManager());

				// Events
				// _editor.on("changeSession", () => {
				// 	console.log('changeSession');
				// });
				this.session.on("change", () => {
					this.clearHighlights();
				});
			};

			if (!this.new) {
				this.refresh(
					() => {
						this.log = this.test.log;
					}
				);
			}
		};

		public save() {
			if (this.new) {
				return this.createNew();
			}
			return this.$http.put(restHost + '/businesstests/' + this.testId, this.test)
				.success(data => {
					this.$log.debug('Successfully saved test');
					this.test = data;
				})
				.error((jqxhr, textStatus) => {
					this.$log.warn('Unable to save test.');
				});
		};

		public createNew() {
			return this.$http.post(restHost + '/businesstests/', this.test)
				.success(
					(data: IBusinessTestVO) => {
						this.$log.debug('Successfully created test');
						this.$log.debug('Data: %o', data);
						this.$location.path('/businesstests/' + data.id.uid);
					}
				)
				.error((jqxhr, textStatus) => {
					this.$log.warn('Unable to create test.');
				});
		};

		/**
		 * Runs the current test and continually prints the test logs.
		 */
		public run() {
			this.started = true;
			this.save().success(
				() => {
					this.log = '';
					oboe({
						url: restHost + '/businesstests/run/' + this.testId,
						withCredentials: true
					})
						.node('!.{message}', element => {
								var f = () => {
									this.log += element.message;
									var output = $('#testLog');
									if (output.length)
										output.scrollTop(output[0].scrollHeight - output.height());
								};
								this.$timeout(f);
							}
						)
						.done(data => {
							this.refresh();
							this.started = false;
						})
						.fail(error => {
							this.$log.debug('Running tests failed: %o', error);
							var f = () => {
								this.started = false;
							};
							this.$timeout(f);
						});
				}
			).error(
				(data, textStatus) => {
					this.log = 'Unable to save test: ' + data.message;
					this.started = false;
				}
			);
		};

		private refresh(callback?: Function) {
			this.$http.get(restHost + '/businesstests/' + this.testId)
				.success(data => {
					this.test = data;
					this.$log.debug('Business test: %o', this.test);
					if (callback) {
						callback();
					}
					this.highlight();
				})
				.error((jqxhr, textStatus) => {
					this.$log.warn("Unable to get log debug info.");
				});
		};

		public delete() {
			this.$http.delete(restHost + '/businesstests/' + this.testId)
				.success(data => {
					this.$log.debug('Test deleted');
					this.$location.path('/businesstests')
				})
				.error((jqxhr, textStatus) => {
					this.$log.warn('Unable to delete test.');
				});
		};

		/**
		 * Clears all error/warning markers in the editor.
		 */
		private clearHighlights() {
			this.$log.debug('clearHighlights()');
			let markers = this.editor.getSession().getMarkers(false);
			this.$log.debug('Markers: %o', markers);
			for (var i in markers) {
				let marker = markers[i];
				this.$log.debug('Removing marker %o', marker);
				this.session.removeMarker(marker.id);
			}
		};

		/**
		 * Highlights erroneous lines in the editor.
		 */
		private highlight() {
			this.clearHighlights();
			if (this.test.errorLine) {
				this.highlightLine(this.test.errorLine - 1, 'ace_error');
			}
			else if (this.test.warningLine) {
				this.highlightLine(this.test.warningLine - 1, 'ace_warning');
			}
		}

		private highlightLine(line: number, clazz: string) {
			this.$log.debug('Highlighting line %o with class %o', line, clazz);

			var Range = ace.require("ace/range").Range;
			let range: Range = new Range(
				line,
				0,
				line,
				1000
			);
			this.$timeout(
				() => {
					this.session.addMarker(range, clazz, "fullLine", false);
					this.editor.scrollToLine(line, true, true, () => {});
				}
			)
		}
	}
}
