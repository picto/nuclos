module nuclos.dev {
	export class DevUtilsCtrl {

		constructor($scope,
					$q,
					$http,
					$cookieStore,
					$localStorage,
					restservice,
					preferences,
					dialog,
					private nuclosI18N: nuclos.util.I18NService) {


			// enable / disable SQL Logging
			$http.get(restHost + '/maintenance/logging/SQLLogger/level')
				.success(function (data) {
					$scope.logSQL = data.level == 'DEBUG';
				})
				.error(function (jqxhr, textStatus) {
					console.warn("Unable to get log info.");
				});


			$scope.$watch('logSQL', function (newValue, oldValue) {
				if (newValue != oldValue) {
					var postData = {level: $scope.logSQL ? 'DEBUG' : 'INFO'};
					$http.put(restHost + '/maintenance/logging/SQLLogger/level', postData);
				}
			}, true);

			$scope.logAngularRESTCalls = $localStorage.logAngularRESTCalls;
			$scope.$watch('logAngularRESTCalls', function (newValue, oldValue) {
				if (newValue != oldValue) {
					$localStorage.logAngularRESTCalls = newValue;
				}
			}, true);


			$scope.logAngularRESTCallsContainingResponse = $localStorage.logAngularRESTCallsContainingResponse;
			$scope.$watch('logAngularRESTCallsContainingResponse', function (newValue, oldValue) {
				if (newValue != oldValue) {
					$localStorage.logAngularRESTCallsContainingResponse = newValue;
				}
			}, true);


			// set SQL Debug Logging
			$http.get(restHost + '/maintenance/logging/debugSQL')
				.success(function (data) {
					$scope.debugSQL = data.debugSQL;
					$scope.minExecTime = data.minExecTime;
				})
				.error(function (jqxhr, textStatus) {
					console.warn("Unable to get log debug info.");
				});

			$scope.$watch('debugSQL', function (newValue, oldValue) {
				if (newValue != oldValue) {
					var postData = {debugSQL: $scope.debugSQL, minExecTime: $scope.minExecTime};
					$http.put(restHost + '/maintenance/logging/debugSQL', postData);
				}
			}, false);

			$scope.$watch('minExecTime', function (newValue, oldValue) {
				if (newValue != oldValue) {
					var postData = {debugSQL: $scope.debugSQL, minExecTime: $scope.minExecTime};
					$http.put(restHost + '/maintenance/logging/debugSQL', postData);
				}
			}, false);

			$scope.deleteMyPreferences = function () {

				dialog.confirm(
					nuclosI18N.getI18n("webclient.button.delete"),
					nuclosI18N.getI18n("webclient.dialog.delete"),
					function () {
						// ok
						preferences.deleteMyPreferences();

					},
					function () {
						// cancel
					}
				);

			};
		}
	}
}
