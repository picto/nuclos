module nuclos.tableview {

	import BoViewModelImpl = nuclos.model.BoViewModelImpl;

	interface ITableViewRouteParams extends ng.route.IRouteParamsService {
		uid: string;
	}

	export class TableViewCtrl {
		counter = 0;

		constructor(private $scope,
					private $http,
					private $routeParams: ITableViewRouteParams,
					private $location,
					private $cookieStore,
					private $timeout,
					private $uibModal,
					private $q,
					private $rootScope,
					private restservice,
					private dataService,
					private boModalService,
					private boService,
					private metaService,
					private inputrequiredService,
					private dialog,
					private errorhandler,
					private nuclosI18N: nuclos.util.I18NService) {

			var master = {
				uid: this.$routeParams.uid, // TODO remove
				boMetaId: this.$routeParams.uid,
				list: restHost + '/boMetas/' + this.$routeParams.uid,
				meta: undefined,
				dirty: undefined,
				canCreateBo: undefined,
				selectedBo: undefined
			};

			dataService.setMaster(master);

			metaService.getBoMetaData(master.boMetaId).then(function (metaData) {
				master.meta = metaData;
				dataService.setMaster(master);

				localStorage.setItem("selectedViewType", "tableview");

				$scope.tableviewControlObject = {}; // holds isolated scope to call functions inside directive

				$scope.bodata = {};


				$scope.doShowNewButton = function () {
					return master && !master.dirty && master.canCreateBo;
				};


				$scope.doShowDelButton = function () {
					var selectedRows = dataService.getMaster().selectedRows;
					if (selectedRows) {
						for (var r = 0; r < selectedRows.length; r++) {
							var row = selectedRows[r];
							if (row.links && row.links.self.methods.indexOf('DELETE') == -1 || !row.canDelete) {
								return false;
							}
						}
					}
					return master && !master.dirty && selectedRows && selectedRows.length > 0;
				};


				$scope.loadMasterData = function (offset, clearTableHeader, preloadSmallChunk) {
					var deferred = $q.defer();

					if (!clearTableHeader) {
						clearTableHeader = true;
					}
					if (!preloadSmallChunk) {
						preloadSmallChunk = true;
					}

					chunkSize = preloadSmallChunk ? 40 : 80;
					//		chunkSize = 50;

					if (offset == 0) {
						if ($scope.tableviewControlObject.clearTable) {
							$scope.tableviewControlObject.clearTable();
						}

						if ($scope.tableviewControlObject.clearTableHeader && clearTableHeader) {
							$scope.tableviewControlObject.clearTableHeader();
						}

						// reset change state buttons
						if ($scope.availableStates) {
							$scope.availableStates = [];
						}
					}

					//Difficult to understand: Why is this doubled and not the correct service (boService.loadBOList) used?
					//TODO: Remove this code and use boService.loadBOList!
					var entity = dataService.getMaster();
					if (!entity) return;
					delete entity.all;
					delete entity.total;

					var selectedSearchFilter = entity.searchfilter ? entity.searchfilter.pk : entity.searchfilter;
					var sortExpression = entity.sortExpression ? entity.sortExpression : "";
					var filter = {
						offset: offset,
						chunksize: chunkSize,
						gettotal: true,
						fields: "all",
						search: entity.search,
						searchFilter: selectedSearchFilter,
						sort: sortExpression,
						searchCondition: undefined
					};

					var searchCondition = getSearchCondition(entity.metaData);
					if (searchCondition) {
						filter.searchCondition = searchCondition;
					}

					$scope.lastSearchFilter = filter;

					// TODO use HATEOAS
					restservice.dataList(restHost + '/bos/' + entity.boMetaId, filter).then(function (data) {
						if ($scope.lastSearchFilter === filter) {
							$scope.lastSearchFilter = undefined;
							var save = entity.bos;
							entity.bos = data.bos;
							if (offset > 0) {
								save.reverse();
								for (var i = 0; i < save.length; i++) {
									entity.bos.unshift(save[i]);
								}
							}
							entity.all = data.all;
							entity.total = data.total;

							dataService.setMaster(entity);

							if ($scope.tableviewControlObject.updateTable) {
								$scope.tableviewControlObject.updateTable(entity.boMetaId, offset, dataService.getMaster().bos);
							}

							if (preloadSmallChunk && offset === 0 && data.total > chunkSize) {
								// call this method again to load more data
								$scope.loadMasterData(chunkSize, true, false);
							}

						}

						$scope.bodata.bos = data.bos;

						var loaded = entity.bos ? entity.bos.length : 0;
						var newLoadedBos = data.bos.slice(loaded);
						var allLoaded = data.bos.length == data.total;
						// notify ui-grid
						$scope.$broadcast('infiniteScrollMoreDataLoaded', {bos: newLoadedBos, allLoaded: allLoaded})

						deferred.resolve(data);

					});
					return deferred.promise;
				};


				// search
				$scope.tableviewSearchCallback = function () {
					$scope.loadMasterData(0).then(function (data) {
						$rootScope.$broadcast('refreshgrids');
					});
				};

				// initial load of data

				$scope.isTaskView = $location.path().indexOf("/taskview") === 0;
				$scope.mMenuLabel = $scope.isTaskView ? 'Tasks' : 'Menu';
				$scope.mMenuPath = $scope.isTaskView ? '#/tasks' : '#/menu';

				metaService.canCreateBo(master.boMetaId).then(function (data) {
					master.canCreateBo = data;
				});

				dataService.setMaster(master);

				var restServiceFunction = $scope.isTaskView ? restservice.metaSearchfilter : restservice.metaEntity;
				restServiceFunction(this.$routeParams.uid).then(function (data) {

						data.meta = master.meta;

						dataService.setMaster(data);

						// load search filters
						restservice.searchfilters(dataService.getMaster().boMetaId).then(function (searchfilters) {
							for (var i = 0; i < searchfilters.length; i++) {
								var filter = searchfilters[i];
								if (filter.default) {
									filter.isSelected = true;
									break;
								}
							}
							for (var i = 0; i < searchfilters.length; i++) {
								var filter = searchfilters[i];
								filter.imageResourceURL = nuclosServer + "/rest/data/resource/" + filter.icon;
							}
							$scope.searchfilters = searchfilters;
						});

						// TODO loadMasterData(0) is also called from searchfilter-directive
						$scope.loadMasterData(0).then(function (data) {
							if (this.$routeParams.pk == 'new') {

								var dismissCallback = function () {
									$location.path('/tableview/' + dataService.getMaster().boMetaId, false);
								};

								boModalService.openEditModal(
									{
										entity: dataService.getMaster(),
										scope: $scope.$new(),
										callback: {dismiss: dismissCallback}
									}
								);

							}

						});

					},
					function (message) {
						errorhandler.show(message, '', $cookieStore);
					});


				/*
				 * load more data if scrolled to end
				 */
				var blockNeedMoreDataCalls = false;
				$scope.$on('infiniteScrollneedLoadMoreData', function () {

					if (blockNeedMoreDataCalls) {
						return;
					}
					blockNeedMoreDataCalls = true;
					var entity = dataService.getMaster();
					if (!entity.all) {
						var loaded = entity.bos ? entity.bos.length : 0;
						$scope.loadMasterData(loaded, false, false).then(function (data) {
							blockNeedMoreDataCalls = false;
							var allLoaded = data.bos.length == data.total;
							$scope.$broadcast('infiniteScrollMoreDataLoaded', {bos: data, allLoaded: allLoaded})
						});
					}

				});


				$scope.selectFilter = function (filter) {
					for (var i = 0; i < $scope.searchfilters.length; i++) {
						$scope.searchfilters[i].isSelected = false;
					}
					filter.isSelected = true;
					dataService.getMaster().searchfilter = filter;
					$scope.loadMasterData(0).then(function (data) {
						$rootScope.$broadcast('refreshgrids');
					});
				};

				$scope.imageResourceURL = function (uid) {
					return nuclosServer + "/rest/data/resource/" + uid;
				};


				// called from bo modal service
				$scope.$on('reloadtabledata', function (event) {
					$scope.loadMasterData(0).then(function (data) {
						$rootScope.$broadcast('refreshgrids');
					});
				});

				$scope.save = function (entity) {

					// reset user input values before saving entry
					delete entity.inputrequired;

					inputrequiredService.setSaveCallback(doSave);
					inputrequiredService.setRefreshCallback(function () {
					});
					doSave(entity).then(
						function () {
							// save was successful
						},
						inputrequiredService.processSaveDataRecursive
					);
				};


				function doSave(entity) {

					var deferred = $q.defer();

					BoViewModelHack.prototype.extendJson(master);

					master.selectedBo = entity;

					boService.submitBOForInsertUpdate(entity, master, entity._flag).then(function (data) {
						// ok
						$scope.loadMasterData(0).then(function (data) {
							$rootScope.$broadcast('refreshgrids');
						});
						deferred.resolve(data.selectedBo);
					}, function (error) {
						// not ok
						entity.inputrequired = error.inputrequired;
						deferred.reject(entity);
					});

					return deferred.promise;
				}

				$scope.saveCurrentRow = function (editedBO, $dismiss) {
					var master = dataService.getMaster();

					//TODO validate
					boService.submitBOForInsertUpdate(editedBO, master, editedBO._flag).then(function (data) {
						// ok

						$dismiss();
						$scope.loadMasterData(0).then(function (data) {
							$rootScope.$broadcast('refreshgrids');
						});
					}, function () {
						// not ok
					});
				};

				// called from table view
				$scope.deleteSelectedRows = function () {
					$scope.deleteRows(dataService.getMaster().selectedRows);
				};


				// called from detail modal view
				$scope.deleteCurrentRow = function ($dismiss) {
					$scope.deleteRows([dataService.getMaster().selectedBo]);
					$dismiss();
				};

				$scope.$on('deletebo', function (event, bo) {
					$scope.deleteRows([bo]);
				});

				$scope.deleteRows = function (rows) {
					if (rows && rows.length !== 0) {

						var title = this.nuclosI18N.getI18n("webclient.button.delete");
						var text = this.nuclosI18N.getI18n("webclient.dialog.delete.multiple");

						// open delete dialog
						dialog.confirm(
							title,
							text,
							function () {
								// ok

								var deletePromises = [];

								for (var i = 0; i < rows.length; i++) {
									var boToDelete = rows[i];
									deletePromises.push(boService.submitBOForDelete(boToDelete, dataService.getMaster()))
								}

								$q.all(deletePromises).then(function (res) {
									// ok
									$scope.loadMasterData(0).then(function (data) {
										$rootScope.$broadcast('refreshgrids');
									});
								}, function (error) {
									// not ok
									errorhandler.show(error.data, error.status);
								});
							},
							function () {
								// cancel
							});

					}
				};

				$scope.$on('state:changed', function () {
					showStateChangeButtons(null);
				});


				$scope.$on('refreshstatusbuttons', function (event, selectedBos) {
					showStateChangeButtons(selectedBos);
				});


				function showStateChangeButtons(selectedBos) {
					var
						currentNextStates,
						selectedLink,
						missingWritePermission = false,
						differentSelectedStates = false;

					for (var s = 0; s < selectedBos.length; s++) {
						var selectedRowData = selectedBos[s];

						if (!selectedRowData.canWrite) {
							missingWritePermission = true;
							break;
						}

						if (currentNextStates == undefined) {
							currentNextStates = selectedRowData.nextStates;
						}
						if (currentNextStates == undefined) {
							return;
						}
						var currentNextStateNumbers = currentNextStates.map(function (d) {
							return d.number;
						});
						var nextStateNumbers = selectedRowData.nextStates.map(function (d) {
							return d.number;
						});
						if (JSON.stringify(currentNextStateNumbers) != JSON.stringify(nextStateNumbers)) {
							differentSelectedStates = true;
							break;
						}
						selectedLink = selectedRowData.links.self.href;
					}

					// state buttons are only shown for selected bos in the same state
					if (!differentSelectedStates && !missingWritePermission && selectedLink) {
						restservice.dataGet(selectedLink).then(function (data) {
							$scope.availableStates = data.nextStates;
						});
					} else {
						$scope.availableStates = [];
					}

					$scope.currentStateOfSelectedRows = selectedRowData ? selectedRowData.attributes.nuclosState.id : undefined;
				}


				var selectRow = function (boMeta, boInstanceId) {
					var row = {
						"_uid": dataService.getMaster().boMetaId, "boId": boInstanceId,
						"bo_href": restHost + "/boMetas/" + dataService.getMaster().boMetaId + "/bos/" + boInstanceId
					};
					boService.loadSingleBO(dataService.getMaster(), row);
					return row;
				};


				var selectedRowsIterator = function () {

					var index = 0, selectedRows = [], length = 0;
					return {
						build: function () {
							index = 0;
							selectedRows = jQuery("#grid-" + dataService.getMaster().boMetaId)['jqGrid']('getGridParam', 'selarrrow');
							length = selectedRows ? selectedRows.length : 0;
							return this;
						},
						length: function () {
							return length;
						},
						next: function () {
							if (!this.hasNext()) {
								return null;
							}

							var rowNumber = selectedRows[index];
							var rowData = dataService.getMaster().bos[rowNumber - 1];

							index++;
							return rowData;
						},
						hasNext: function () {
							return index < length;
						},
						rewind: function () {
							index = 0;
							return selectedRows[index];
						},
						current: function () {
							return selectedRows[index];
						}
					};

				}();


				var saveState = function (postData) {
					$http.put(postData.bo_href, postData).success(function (data) {
						$scope.loadMasterData(0).then(function (data) {
							$rootScope.$broadcast('refreshgrids');
						});
					}).error(function (data, status) {
						$scope.loadMasterData(0).then(function (data) {
							$rootScope.$broadcast('refreshgrids');
						});
						errorhandler.show(data, status, $cookieStore);
					});
				};

			});
		}
	}
}