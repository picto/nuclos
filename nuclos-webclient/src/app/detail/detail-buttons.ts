module nuclos.detail {

    import Component = nuclos.util.Component;
    import IPreference = nuclos.preferences.IPreference;
    import SearchTemplateAttr = nuclos.searchtemplate.SearchTemplateAttr;
    import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
	import Mandator = nuclos.model.Mandator;

    export const SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM = 'selectEntryInOtherWindowToken';
    export const SHOW_CLOSE_ON_SAVE_BUTTON_PARAM = 'showCloseOnSave';
    export const SHOW_SELECT_BUTTON_PARAM = 'showSelectButton';
    export const EXPAND_SIDEVIEW_PARAM = 'expand=true';

    export const SHOW_CLOSE_ON_SAVE_BUTTON_LOCAL_STORAGE_KEY_NAME = 'showCloseOnSave';

    export function closeWindowOnSave() {
        return localStorage.getItem(SHOW_CLOSE_ON_SAVE_BUTTON_LOCAL_STORAGE_KEY_NAME) === 'true';
    }

	interface DetailRouteParams extends ng.route.IRouteParamsService {
		pk: string;
        processmetaid: string;
	}


    @Component(nuclosApp, 'detailButtons', {
        controllerAs: 'vm',
        templateUrl: 'app/detail/detail-buttons.html',
        bindings: {
            saveCallback: '&',
            saveAndRefreshOtherBrowserInstancesCallback: '&',
            cancelCallback: '&',
            deleteCallback: '&',
            openPrintoutsCallback: '&',
            openChartsCallback: '&',
            unlockCallback: '&'
        }
    })
    class DetailButtons {

        private master: BoViewModel;
        private showSelectButton: boolean;
        private showCloseOnSaveButton: boolean;
        private saveCallback: Function;

        private saveAndRefreshOtherBrowserInstancesCallback: Function;

        private cancelCallback: Function;
        private deleteCallback: Function;
        private openPrintoutsCallback: Function;
        private openChartsCallback: Function;
        private unlockCallback: Function;

		private authentication;

        private closeWindowOnSave: boolean;

		private showChartButton: boolean;

		public showMandatorSelection = false;
		public mandators: Mandator[] = [];
		public selectedMandator: Mandator = undefined;
		private selectedSearchtemplatePreferenceForNew: IPreference<nuclos.sideview.SearchtemplatePreferenceContent>;


		constructor(
			private $cookieStore: ng.cookies.ICookieStoreService,
            private $location: ng.ILocationService,
            private $window: ng.IWindowService,
            private $timeout: ng.ITimeoutService,
			private $rootScope: ng.IRootScopeService,
			private $routeParams: DetailRouteParams,
            private dataService: nuclos.core.DataService,
            private metaService: nuclos.core.MetaService,
            private authService: nuclos.auth.AuthService,
			private preferences: nuclos.preferences.PreferenceService,
            private browserRefreshService: BrowserRefreshService,
            private util: nuclos.util.UtilService,
			private layoutService : nuclos.layout.LayoutService,
			private boService: nuclos.core.BoService,
            private $log: ng.ILogService
        ) {
            this.master = dataService.getMaster();

            if ($location.url().indexOf(SHOW_CLOSE_ON_SAVE_BUTTON_PARAM) != -1) {
                this.showCloseOnSaveButton = true;
            }

            if ($location.url().indexOf(SHOW_SELECT_BUTTON_PARAM) != -1) {
                this.showSelectButton = true;
            }

			this.authentication = this.$cookieStore.get('authentication');

            this.closeWindowOnSave = closeWindowOnSave();

            // show chart button if there is a chart configured for this bo
            this.showChartButton = false;
            this.preferences.getPreferences({
                boMetaId: this.master.boMetaId,
                type: ['chart']
            }).then(
                (chartPreferences) => {
                    if (chartPreferences.length > 0) {
                        this.showChartButton = true;
                    }
                },
                (error) => {
                    this.showChartButton = false;
                }
            );

			/*if (this.$routeParams.pk == 'new' && $('.modal').length == 0) {
				if (!this.master.bos) {
					this.master.bos = [];
				}
				this.newBoInstance();
			}*/

        }


        public toggleCloseWindowOnSave(){
            this.closeWindowOnSave = !this.closeWindowOnSave;
            this.updateCloseWindowOnSave();
        }

        public updateCloseWindowOnSave(){
            localStorage.setItem(SHOW_CLOSE_ON_SAVE_BUTTON_LOCAL_STORAGE_KEY_NAME, '' +this.closeWindowOnSave);
        }

        private selectEntryInOtherWindow(bo) {
            // send message
            var message = new nuclos.detail.SelectEntryInOtherWindowMessage();
            message.boMetaId = bo.boMetaId;
            message.boId = bo.boId;
            message.name = bo.title;
            message.token = Number(this.util.getRequestParameter(SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM));
            this.browserRefreshService.selectEntryInOtherWindow(message);
        }

        public save() {

            // don't refresh other browser instances if this window is opened from another window
            // otherwise the refresh will interfere with the reference update of the origin window
            var saveFunction = this.showCloseOnSaveButton ? this.saveCallback : this.saveAndRefreshOtherBrowserInstancesCallback;

            var promise = saveFunction();
            if (promise != null) {
                // create new bo
                promise.then((bo) => {
                    this.selectEntryInOtherWindow(bo);

                    // close window if it was called from another window and user has 'close window on save' option checked
                    if (this.showCloseOnSaveButton && closeWindowOnSave()) {
                        this.$window.close();
                    }
                });
            }
        }

		public selectMandator(selectedMandatorId) {
			this.selectedMandator = this.mandators.filter(mandator => mandator.mandatorId === selectedMandatorId).shift();
			this.showMandatorSelection = false;
			$('#sideview-details-content').show();

			this.newBoInstance(this.selectedSearchtemplatePreferenceForNew);
		}

		private getMandatorsByLevelAndCurrentMandator(mandatorLevelId: string): Mandator[] {
			let mandators = this.authService.getMandators();
			let currentMandator = this.authService.getCurrentMandator();
			return mandators
				.filter(m => m.mandatorLevelId === mandatorLevelId)
				.filter(m => !currentMandator || m.path.indexOf(currentMandator.path) === 0)
				;
		}

		public newBoInstance(searchtemplatePreference? : IPreference<nuclos.sideview.SearchtemplatePreferenceContent>) {

			// if more then 1 mandators are available ask user to choose mandator
			this.mandators = this.getMandatorsByLevelAndCurrentMandator(this.master.meta.mandatorLevelId);

			if (this.mandators.length > 1 && this.selectedMandator === undefined) {
				this.showMandatorSelection = true;
				$('#sideview-details-content').hide();
				this.selectedSearchtemplatePreferenceForNew = searchtemplatePreference;
				return;
			} else if (this.mandators.length == 1) {
				this.selectedMandator = this.mandators[0];
			}

			this.dataService.getMaster().inValidFormSubmitted = false;
			this.newRow();

			// focus first input field
			$($('#sideview-details-content input:not(disabled)').get(0)).focus();

            let searchtemplate;
            if (!searchtemplatePreference) {
                searchtemplate = {
    				prefId: null,
    				type: 'searchtemplate',
    				boMetaId: this.master.boMetaId,
    				content: {
    					attributes: []
    				} as nuclos.sideview.SearchtemplatePreferenceContent,
    				name: ''
    			};
    			searchtemplate.content.allAttributes = angular.copy(SearchtemplateService.searchtemplateModel.allAttributes);
            } else {
                searchtemplate = searchtemplatePreference;
            }
            if (this.$routeParams.processmetaid) {
                let processAttribute : SearchTemplateAttr = SearchtemplateService.searchtemplateModel.selectedSearchtemplatePreference.content.allAttributes.filter(function(value, index, array) {
                    return value.boAttrName == "nuclosProcess";
                })[0];
                processAttribute.operator = "=";
                let processName : string = this.$routeParams.processmetaid.lastIndexOf(this.master.boMetaId) == 0 ? this.$routeParams.processmetaid.substring(this.master.boMetaId.length + 1) : "";
                let value = {"id" : this.$routeParams.processmetaid, "name" : processName};
                processAttribute.value = value;
                searchtemplate.content.attributes.push(processAttribute);
            }

			// fill new data record with '=' search attributes
			this.$timeout(() => {
				let processFieldUid : string;
				searchtemplate.content.attributes.forEach((attr) => {
					var shortAttrName = getShortFieldName(this.dataService.getMaster().selectedBo, attr.boAttrId)
					if (attr.operator == '=') {
						this.dataService.getMaster().selectedBo.attributes[shortAttrName] = attr.value;
					}
					if (shortAttrName == 'nuclosProcess') {
						processFieldUid = attr.boAttrId;
					}
				});
				if (processFieldUid) {
					let master : BoViewModel = this.dataService.getMaster();
					this.layoutService.defaultSearchLayout(master).then((layout) => {
						let sourcefield = this.layoutService.findField(layout, getShortFieldName(master, processFieldUid));
						if (sourcefield) {
							this.layoutService.checkForLayoutChange(master.selectedBo, sourcefield.uid, master, true);
						}
					})
				}
			});
		}

		private newRow() {
			var entity = this.dataService.getMaster();

			this.boService.emptyBO(entity).then((bo) => {
				this.layoutService.createTableForThisBO(entity, bo).then((layoutLoadedActions) => {
					this.$rootScope.$broadcast('rowselected', { layoutLoadedActions: layoutLoadedActions, bo: bo });

					if (this.selectedMandator) {
						this.master.selectedBo.attributes['nuclosMandator'] = {
							id: this.selectedMandator.mandatorId,
							name: this.selectedMandator.name
						};
						delete this.selectedMandator;
					}

					// clear subforms
					if (this.dataService.getMaster().selectedBo.subBos) {
						for (let subBo of this.dataService.getMaster().selectedBo.subBos) {
							subBo.bos = [];
						}
					}
					this.$rootScope.$broadcast('refreshgrids'); // refresh active subforms
				});

				entity.bos.unshift(bo); // add bo to the first position of entity.bos
				entity.setSelectedBo(bo);
				entity.dirty = true;
			});

		}

		public dismiss() {
			angular.element($('.modal')).scope()['$dismiss']();
		}

		public cancel() {
			delete this.selectedMandator;
			if (this.cancelCallback) {
				this.cancelCallback();
			}
		}

		public doShowDelButton() {
			return this.master && !this.master.dirty && this.master.selectedBo && this.master.selectedBo.canDelete && this.master.selectedBo.links
				&& (this.master.selectedBo.links.self && this.master.selectedBo.links.self.methods.indexOf('DELETE') != -1);
		}

		/**
		 * TODO refactor move to a service - this should also be evaluated from Menu -> new Item
		 */
		public doShowNewButton() {
			return this.master && !this.master.dirty && this.master.canCreateBo
				&& (
					// mandators not present
					this.authService.getMandators().length === 0

					// bo is not mendator dependent
					|| !this.master.meta.mandatorLevelId

					// mandators present but no mandator selection available for this bo
					|| !!this.master.meta.mandatorLevelId && this.getMandatorsByLevelAndCurrentMandator(this.master.meta.mandatorLevelId).length !== 0
				);
		}

		public doShowNewSubrowButton() {
			return this.master && this.master.meta && !this.master.meta.readonly;
		}


        public selectThisEntryInOtherWindow() {
            this.selectEntryInOtherWindow(this.master.selectedBo);
            this.$window.close();
        }

        public openPrintouts() {
            this.openPrintoutsCallback();
        }
        public openCharts() {
            this.openChartsCallback();
        }
    }

}
