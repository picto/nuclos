/**
 * 
 */
nuclosApp.directive('rootLayout', function($compile) {
    
	return {
		scope: {
			master: '<'
		},
		template: '<div class="staticlayout-placeholder"></div>',
		link: function(scope, element, attrs) {
			
			var directive2Compile = '<div static-layout table="master.table" master="master"></div>';
			
			//Once we do things for ourselves (like building dynamic html) more sophisticated stuff must be done,
			//like creating and destroying child-scopes.
	       	var childScope;
			
			var createChildScope = function() {
				childScope = scope.$new();
				var compiledDirective = $compile(directive2Compile);
				var directiveElement = compiledDirective(childScope);
				
				// if detail is opened via modal there is more than one placeholder 
				$($('.staticlayout-placeholder')[0]).append(directiveElement);				
			}
			
			createChildScope();
			var hit = 0;
			
			scope.$watch('master.table.trs', function() {				
				if (++hit > 1) {
        			childScope.$destroy();
        			$($('.staticlayout-placeholder')[0]).empty();
        			createChildScope();	        			
				}
        	});
  
	    }
	}
});
