/**
 * 
 */
nuclosApp.directive('staticLayout', function(
		$http: ng.IHttpService, 
		$templateCache: ng.ITemplateCacheService, 
		$compile: ng.ICompileService, 
		$q: ng.IQService, 
		boService: nuclos.core.BoService, 
		layoutService, 
		$timeout: ng.ITimeoutService
	) {

	interface IStaticLayoutScope extends ng.IScope {
		master: any,
		table: any,
		fields: any,
		hiddenpanels: any,

		togglePanel: Function,
		openLink: Function,
		writeEmail: Function,
		openReference: Function,
		loadLovDropDownOptions: Function,
		editorChange: Function
	}

	// TODO: Where is JSON.search defined? Another interface should not be needed here.
	interface IStaticLayoutJSON extends JSON {
		search: Function;
	}

	return {
		scope: {
			master: '<',
			table: '<'
		},
		link: function(scope: IStaticLayoutScope, element, attrs) {
		
			var templateFile = "app/detail/static-layout.html";
	    	
		    $http.get(templateFile, {cache: $templateCache}).success(function(template: string) {
		    	compile(template);
		    });
	    	
	    	var compile = function(template: string) {
		    	var trs = scope.table.trs;
		        var types = getInputTypesOfNextLevel(trs);
		        
	        	var htmltable = assembleHtmlTable(trs, template, types);
	        	
		    	var contents = element.html(htmltable).contents();
		        $compile(contents)(scope);
	    	};
	    	
			var getFieldForName = function(fieldname) {
				var fields = (JSON as IStaticLayoutJSON).search(scope.table.trs, '//field[name="' + fieldname + '"]');
				return fields.length > 0 ? fields[0] : undefined;
			}
				
	        var getTypesOutOfTemplate = function(template, types) {
	        	var ret = {};
	        	var lines = template.split("\n");
	        	var currenttype = undefined;
	        	
	        	for (var i = 0; i < lines.length; i++) {
	        		var line = lines[i];
	        		
	           		if (line.indexOf("<!--") > -1) {
	    		        continue;
	           		}
	    		
	        		var n = line.indexOf("ng-switch-when='");
	        		if (n > -1) {
	        			var m = line.indexOf("'", n + 16);
	        			currenttype = line.substring(n + 16, m);	        			
        				line = line.replace("ng-switch-when='" + currenttype + "'", "");
	        			
	        		} else if (line.indexOf("ng-switch-default") > -1) {
	        			currenttype = 'default';
        				line = line.replace("ng-switch-default", "");
	        			
	        		} else if (line.indexOf("</ng-switch>") > -1) {
	        			currenttype = undefined;        			
	        			
	        		}
	        		
	        		if (currenttype && types[currenttype]) {
	        			if (!ret[currenttype]) {
	        				ret[currenttype] = '';
	        			}
	        			ret[currenttype] += line + "\n";
	        		}
	        		
	        	}
	        	
	        	return ret;
	        }
	        
	        var getInputTypesOfNextLevel = function(trs) {
	        	var types = {};
	        	
	        	for (var i in trs) {
	        		for (var j in trs[i].tds) {
	        			var td = trs[i].tds[j];
	        			if (td.field && td.field.input) {
	        				types[td.field.input] = true;
	        			}       			
	        		}
	        	}
	        	
	        	return types;
	        }
	        
	        var rowAndColSpan = function(td) {
	        	return (td.colspan > 1 ? " colspan=\"" + td.colspan + "\"" : "")
	        		 + (td.rowspan > 1 ? " rowspan=\"" + td.rowspan + "\"" : "");
	        }
	        
	        var getPlaceHolderDiv = function(td, i, j) {
	        	if (i > 0 && j > 0) {
	        		return '';
	        	}
	        	var div = '<div style="';
	        	if (i == 0) {
	        		div += 'width:' + td.width + 'px;';
	        	}
	        	if (j == 0) {
	        		div += 'height:' + td.height + 'px;'
	        	}     	
	        	return div + '"></div>\n';
	        }
	        	        
	        var assembleHtmlTable = function(trs, template, types) {
	        	var pTypes = angular.copy(types);
	        	pTypes['default'] = true;
	        	
	        	var htmltypes = getTypesOutOfTemplate(template, pTypes); 
	        	//ARAS-133: The non-tab tables doesn't need any width or class
	        	var html = '<table cellpadding="1" border="0">\n\n';
	        	
	        	//NUCLOS-5041: This may only be needed for tab-content-table, a table in a tab.
	        	//html = '<table width="100%" cellpadding="1" border="0" class="tab-content-table">\n\n';
	            
	            for (var i in trs) {
	            	var tr = trs[i];
	                html += "<tr id=\"row-" + i + "\" class=\"" + tr.class + "\">\n";
	            	
	            	if (!tr.notempty) {
	                	for (var j in tr.tds) {
	                		var td = tr.tds[j];
	                        html += "<td style=\"" + td.style + "\"" + rowAndColSpan(td) + ">\n";
                            html += getPlaceHolderDiv(td, i, j);
	                		html += '</td>\n';
	                	}
	            		          		
	            	} else {
	                	for (var j in tr.tds) {
	                		var td = tr.tds[j];
	                        html += "<td class=\"cell td-" + td.key + "\" style=\"" + td.style + "\"" + rowAndColSpan(td) + ">\n";
	                        
	                		if (!tr.flex && !tr.collapsible && (!td.field || !td.field.input)) {
	                            html += getPlaceHolderDiv(td, i, j);
	                            html += "</td>\n";
	                            continue;
	                		}
	                		
	                		if (td.field && td.field.input) {
	                			var innerform = false;
	                			var hadd = htmltypes[td.field.input];
	                			
	                			if (!hadd) {
	                				hadd = htmltypes['default'].replace("ng-bind-html=\"td.text\">", ">" + td.text);
	    	                		
	                			} else {
		            			//Next line with the "innerForM" stuff are obsolete when the last three lines of static-layout.html are not used
		        				//FIXME:  Check the the request, re-implement and create a test
//	    	                		html += '<div'
//	    	                			+ ' class="tab-content-innerform-div"'
//	    	                			+ ' ng-class="{true: \'error\'}[master.inValidFormSubmitted && innerForm.$invalid]"'
//	    	                			+ '>\n';
//	                					+ '<ng-form name="innerForm" >\n';
//	    	                		innerform = true;			
	                			}
	                			
	                			hadd = hadd.replace("[td.field.name]", "." + td.field.name);
	                			hadd = hadd.replace("[td.field.name]", "." + td.field.name);
	                			hadd = hadd.replace("{{::td.field.name}}", td.field.name);
	                			hadd = hadd.replace("::td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");
	                			hadd = hadd.replace("td.field.name", "'" + td.field.name + "'");

								
								if (td.field.properties && td.field.properties.popupparameter) {
									hadd = hadd.replaceAll("td.field.properties.popupparameter", "'" + td.field.properties.popupparameter + "'");
								}
	                			
	                			
	                			hadd = hadd.replace("{{td.field.style}}", td.field.style);
	                			hadd = hadd.replace("ng-attr-style=", "style=");
	                			
	                			hadd = hadd.replace("{{::td.style}}", td.style ? td.style : "");
	                			hadd = hadd.replace("{{::td.width}}", td.width ? td.width : "");
	                			hadd = hadd.replace("{{::td.height}}", td.height ? td.height : "");
	                			hadd = hadd.replace("{{::td.field.type}}", td.field.type ? td.field.type : "");
	                			hadd = hadd.replace("{{::td.field.itype}}", td.field.itype ? td.field.itype : "");
	                			hadd = hadd.replace("{{::td.field.tabindex}}", td.field.tabindex ? td.field.tabindex : "");
	                			hadd = hadd.replace("{{::td.field.precision}}", td.field.precision ? td.field.precision : "");
	                			hadd = hadd.replace("{{::td.field.scale}}", td.field.scale ? td.field.scale : "");
	            				hadd = hadd.replace("{{::td.field.maxlength}}", td.field.maxlength ? td.field.maxlength : "");
	            				hadd = hadd.replace("{{::td.text}}", td.text ? td.text : "");
	            				hadd = hadd.replace("{{::td.class}}", td.class ? td.class : "");
	            				hadd = hadd.replace("{{::td.key}}", td.key);
	            				
	                			hadd = hadd.replace("ng-required='::td.field.required'", td.field.required ? "required" : "");
	                			hadd = hadd.replace("required='::td.field.required'", td.field.required ? "required='true'" : "required='false'");
	                			hadd = hadd.replace("ng-disabled='::td.field.readonly || !master.selectedBo.canWrite'", 
	                					td.field.readonly ? "disabled" : "ng-disabled='!master.selectedBo.canWrite'");
	                			hadd = hadd.replace("::td.field.readonly || !master.selectedBo.canWrite", 
	                					td.field.readonly ? "true" : "!master.selectedBo.canWrite");
	                			hadd = hadd.replace("::!td.field.readonly && master.selectedBo.canWrite", 
	                					td.field.readonly ? "false" : "master.selectedBo.canWrite");
	                			hadd = hadd.replace("td.field.readonly || !master.selectedBo.canWrite", 
	                					td.field.readonly ? "true" : "!master.selectedBo.canWrite");
	                			
	                			hadd = hadd.replace("::!td.field.nojump", td.field.nojump ? "false" : "true");
	                			hadd = hadd.replace("::td.field.readonly", td.field.readonly ? "true" : "false");
	                			hadd = hadd.replace("::!td.field.readonly", td.field.readonly ? "false" : "true");
	                			hadd = hadd.replace("::!td.field.readonly", td.field.readonly ? "false" : "true");
	                			
                				if (!scope.fields) {
                					scope.fields = {};
                				}
                				var key = "f" + td.key;

	                			if (td.field.input == 'panel') {
	                				
	                				var typespanel = getInputTypesOfNextLevel(td.field.table.trs);
	                				var panel = assembleHtmlTable(td.field.table.trs, template, typespanel);
	                				
	                				var showColl = td.showCollapsiblePanelButton;
	                				hadd = hadd.replace('td.key', "'" + td.key + "'");
	                				hadd = hadd.replace('td.key', "'" + td.key + "'");
	                				hadd = hadd.replace('td.key', "'" + td.key + "'");
	                				hadd = hadd.replace('ng-if="::td.showCollapsiblePanelButton"', showColl ? 'ng-if="true"' : 'ng-if="false"');
	                				hadd = hadd.replace('ng-if="::td.text"', td.text ? 'ng-if="true"' : 'ng-if="false"');
	                				hadd = hadd.replace('static-layout table="td.field.table" master="master">', '>' + panel);	                				
	                			}
	                			else if (td.field.input == 'tabbedpane' && td.field.tabs) {
	                				
	                				if (td.text) {
		                				key += td.text.replace(/ /g,"_");	                					
	                				}
	                				scope.fields[key] = {tabs : td.field.tabs};
	                				hadd = hadd.replace("td.field.tabs", "fields." + key + ".tabs");            					
	            					hadd = hadd.replace("ng-if=\"::td.field.tabs\"", "");
	                			}
	                			else if (td.field.input == 'subform') {
	                				
	                				key += td.field.subform.reffield;
	                				scope.fields[key] = {subform : td.field.subform};
	            					hadd = hadd.replace("td.field.subform", "fields." + key + ".subform");
	                			}
	                			else if (td.field.input == 'matrix') {
	                				
	                				scope.fields[key] = {matrix : td.field.matrix};
	            					hadd = hadd.replace("td.field.matrix", "fields." + key + ".matrix");
	            					hadd = hadd.replace("height: {{::td.height > 0 ? td.height + 'px' : '100%'}}", td.height ? td.height + 'px' : '100%');
	            					hadd = hadd.replace("{{::td.height-10}}", td.height - 10);
	                			}
	                			else if (td.field.input == 'date' || td.field.input == 'checkbox' || td.field.input == 'button' 
	                				|| td.field.input == 'memo' || td.field.input == 'email' || td.field.input == 'hyperlink'
	                				|| td.field.input == 'image' || td.field.input == 'document') {
	                				
	                				if (td.field.name) {
		                				key += td.field.name.replace(/ /g,"_").replace(/[^\x00-\x7F]/g, "");
	                				}
	                				if (td.field.input == 'button') { //ARAS-141. TODO: Generate a general hashkey on first hand
	                					key += ((td.field.generatortoexecute ? td.field.generatortoexecute : '') 
	                							+ (td.field.ruletoexecute ? td.field.ruletoexecute : '') 
	                							+ (td.field.targetState ? td.field.targetState : '')).replace(/\./g,"_");
	                				}
	                				
	                				scope.fields[key] = {field : td.field};
	            					hadd = hadd.replace("td.field", "fields." + key + ".field");
	                			}
	                			else if (td.field.input == 'select') {
	                				
	                				key += td.field.name;
	                				scope.fields[key] = {field : td.field};
	                				hadd = hadd.replace("\"::td.field\"", "\"fields." + key + ".field\"");
	                				hadd = hadd.replace("\"::td.field\"", "\"fields." + key + ".field\"");
	                			}
	                		
	                			html += hadd + '\n';
	                			
	                			if (innerform) {
	    	                		html += '</ng-form>\n';
	    	                		html += '</div>\n';	                				
	                			}
	                		}
	                		
	                		html += '</td>\n';            		
	                	}
	            		
	            	}
	            	   	
	            	//TODO: Still needed?
	            	//<!-- dummy row if no tabs are shown to prevent vertical stretching -->
	           	 	//<tr ng-if="::master.table" style="height:0px;"><td></td></tr>
	            	
	            	html += '</tr>\n';
	            }
	            
	          	html += '</table>';
	            return html;
	        }
	        
			scope.openLink = function (url) {
				url = (url.indexOf('://') == -1) ? 'http://' + url : url;
				window.open(url);
			};
	
			scope.writeEmail = function (address) {
				var mailUrl = "mailto:" + address;
				document.location.href = mailUrl;
			};
			
			scope.openReference = function (fieldname, entity, createNewEntry) {
				var field = getFieldForName(fieldname);
				if (!field || field.nojump) {
					return;
				}
				
				var attributeMeta = entity.meta.attributes[field.name];
		    	var referenceBo = createNewEntry ? {id: 'new'} : entity.selectedBo.attributes[getShortFieldName(entity.selectedBo, attributeMeta.boAttrId)];
		    	if (referenceBo && referenceBo.id) {
			    	var href = location.href.substring(0, location.href.indexOf('/#/')) + '/#/sideview/' + attributeMeta.referencingBoMetaId + '/' + referenceBo.id + '?expand=true';
					if (field.properties && field.properties.popupparameter) {
						window.open(href, '', field.properties.popupparameter);
					} else {
						window.open(href);
					}
		    	}
			};
	
			scope.loadLovDropDownOptions = function(fieldname, bo, quickSearchInput, selectedId) {
				var deferred = $q.defer();
				var field = getFieldForName(fieldname);
				if (!field) {
					deferred.reject();
					return deferred.promise;
				}

				let mandatorId = bo.attributes.nuclosMandator ? bo.attributes.nuclosMandator.id : undefined;
				boService.loadDropDownOptions(field.select, bo.boId, quickSearchInput, mandatorId).then(function(data) {
					deferred.resolve({selectedId: selectedId, data: data});
				});
				
	            return deferred.promise;
			};
	
			scope.editorChange = function (fieldname, entity, bo) {
				if (!entity) {
					entity = scope.master;
				}
				
			    entity.dirty = true;
			    
			    //TODO NUCLOS-3171: Change Layout when changing action
			    //Evaluate two possibilities: Either check here and change the layout
			    //or create a new "layoutml" rule within the server for this action at all.
			    
			    if (!bo) {
			    	bo = entity.selectedBo;
			    }
			    
			    if (bo) {
			    	
				    if (bo._flag !== 'insert') {
				    	bo._flag = 'update';
				    }
				    
				    var field = fieldname ? getFieldForName(fieldname) : undefined;
				    
				    if (field && field.mlrules && bo) {
				    	layoutService.executeLayoutMLRules(field.mlrules, bo, entity.meta.attributes, entity);
				    }
			    }
			    
			};
			
	        scope.hiddenpanels = {};            
	        scope.togglePanel = function (key, depth) {
	            
	            $('#detailblock-form').css('overflow-y', 'hidden'); // prevent to show scrollbar - will be reset after collapsing
	            
	            var isCollapsed = scope.hiddenpanels[key] !== undefined; 
	            if (isCollapsed) {
	                delete scope.hiddenpanels[key];
	            } else {
	                scope.hiddenpanels[key] = true;
	            }
	
	            var panel = $('#panel-' + key);
	            panel.toggleClass('collapsed', !isCollapsed)
	            panel.find('.resize-panel-button').toggleClass('glyphicon-resize-small', isCollapsed)
	            panel.find('.resize-panel-button').toggleClass('glyphicon-resize-full', !isCollapsed)
				
				// this is needed for IE to shrink td's when collapsing 
				panel.closest('tr').children('td').each(function(index, item) {
					var td = $(item);
					if (isCollapsed) {
						td.css('height', td.attr('height-backup'));
						td.css('min-height', td.attr('min-height-backup'));
					} else {
						td.attr('height-backup', td.css('height'));
						td.attr('min-height-backup', td.css('min-height'));
						td.css('height', '20px');
						td.css('min-height', '20px');
					}			
				});
	
	            // fit other flexible components height to new available height
	            $timeout(function() {
	
	                var flexibleItems = $('.tr-flex.tr-' + depth);
	                var nrOfFlexibleItems = flexibleItems.length;
	                
	                var collapsedPanelsHeight = 0;
	                $('.fieldset-content.ng-hide').each(function(index, item) {
	                    collapsedPanelsHeight += $(item).height() - $('.resize-panel-button').height();
	                } );
	                
	                var flexHeightPerItem = collapsedPanelsHeight / nrOfFlexibleItems;
	
					var resizeItems = function(tr, depth) {
						var innerItems = $(tr).find('[tr-flex-init-height][tr-depth=' + depth + ']');
						
						var nrOfInnerItems = innerItems.length;
						if (nrOfInnerItems == 0 && depth < 3) {
							// search for more flexible items inside - depth=3 should be enough
							resizeItems(tr, depth + 1);
						}
						
						var innerFlexHeightPerItem = flexHeightPerItem / nrOfInnerItems;
						innerItems.each(function(i2, div) {
							var initialHeight = Number($(div).attr('tr-flex-init-height'));
							$(div).height(initialHeight + innerFlexHeightPerItem);
						});
					};
	                flexibleItems.each(function(i1, tr) {
	                    resizeItems(tr, depth);
	                });
	                
	                $('#detailblock-form').css('overflow-y', 'auto');
	
	            });
	        };
	        
	    }
	}
});
