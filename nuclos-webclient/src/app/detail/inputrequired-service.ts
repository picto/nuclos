module nuclos.detail {

	import Bo = nuclos.model.interfaces.Bo;

	/**
	 * in case a nuclos rule requires user input (InputRequiredException)
	 * this service will call itself until no further input is required
	 */
	class InputRequiredService {
		refreshCallback = null;
		saveCallback = null;

		constructor(
			private $uibModal,
			private errorhandler: nuclos.Errorhandler) {
		}

		setRefreshCallback(refreshCallback) {
			this.refreshCallback = refreshCallback;
		}

		setSaveCallback(saveCallback) {
			this.saveCallback = saveCallback;
		}

		processSaveDataRecursive(bo: Bo, popupparameter: string) {

			// in case there is further input required (http://wiki.nuclos.de/display/Konfiguration/FAQ+Regelcode),
			// data.inputrequired.specification is returned from the REST service

			var inputrequired = bo.inputrequired;

			if (inputrequired && inputrequired.specification) {

				// open dialog
				var modalInstance = this.$uibModal.open({
					templateUrl: 'app/detail/inputrequired-dialog.html',
					controller: ($scope, $uibModalInstance) => {

						var defaultoption = inputrequired.specification.defaultoption;

						$scope.userinput = {
							inputValue: defaultoption
						};

						$scope.inputrequiredType = inputrequired.specification.type;
						$scope.inputrequired = inputrequired;

						$scope.dialog = {
							message: inputrequired.specification.message
						};

						$scope.userResponse = (response) => {
							$uibModalInstance.userResponse = response;
							$uibModalInstance.close();
						};

						$scope.ok = () => {
							$uibModalInstance.close();
						};
						$scope.cancel = () => {
							$uibModalInstance.dismiss('cancel');
						};

						$scope.close = () => {
							$uibModalInstance.close();
						};

					}
				});
				modalInstance.result.then(() => {
					// ok clicked
					if (!inputrequired.result) {
						inputrequired.result = {};
					}
					inputrequired.result[inputrequired.specification.key] = modalInstance.userResponse;

					// save (or generate) again
					// if save: doSave(bo) is called, if generate: doGenerate(popupparameter) is called
					var callBackArgument = popupparameter ? popupparameter : bo;

					this.saveCallback(callBackArgument).then(
						() => {
							// everything ok
							if (this.refreshCallback) {
								this.refreshCallback();
							}
						},
						(error) => {
							if (error.inputrequired) {
								// further input required
								this.processSaveDataRecursive(error, null);
							}
						}
					);

				}, () => {
					// modal dismissed - esc / cancel clicked
				});

			} else { // no further user input required
				this.refreshCallback();
			}

		}
	}

	nuclosApp.service('inputrequiredService', InputRequiredService);
}
