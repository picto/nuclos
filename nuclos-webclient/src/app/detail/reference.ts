/**
 * search for reference / add new reference item in new tab
 *
 * to select / add a reference entry open the sideview in a new tab
 * when the searched entry is selected / new entry is saved, the window is closed
 * and the selection is send via localStorage events to the origin tab
 */
module nuclos.detail {

	import Component = nuclos.util.Component;

	function openInNewTab(boMetaId: string,
							boId: string|number,
							token: number,
							popupparameter: any,
							showSaveButton: boolean,
							showSelectButton: boolean,
							expandSideview: boolean) {
		let href =
				'#/sideview/' + boMetaId
				+ (boId ? '/' + boId : '')
				+ '?' + SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM + '=' + token
				+ (showSaveButton ? '&' + SHOW_CLOSE_ON_SAVE_BUTTON_PARAM : '')
				+ (showSelectButton ? '&' + SHOW_SELECT_BUTTON_PARAM : '')
				+ (expandSideview ? '&' + EXPAND_SIDEVIEW_PARAM : '')
			;

		if (popupparameter) {
			window.open(href, '', popupparameter);
		} else {
			window.open(href);
		}
	}


	function openBoInNewTab(boMetaId: string,
							boId: string|number,
							boInstance,
							boAttributeName,
							showSaveButton: boolean,
							showSelectButton: boolean,
							expandSideview: boolean,
							markMainBoAsDirty: boolean,
							popupParameter,
							browserRefreshService: nuclos.detail.BrowserRefreshService,
							metaService: nuclos.core.MetaService,
							dataService: nuclos.core.DataService) {
		let token = browserRefreshService.addReferenceSelectionEventListener((searchEvent: SelectEntryInOtherWindowMessage) => {
			// update reference in main window
			boInstance.attributes[boAttributeName] = {
				id: searchEvent.boId,
				name: searchEvent.name,
				_flag: 'update'
			};
			if (markMainBoAsDirty) {
				boInstance._flag = 'update';
				dataService.getMaster().dirty = true;
			}
		});

		// open new tab to edit/select reference
		metaService.getBoMetaData(boMetaId).then(meta => {
			let attributeMeta = meta.attributes[boAttributeName];
			openInNewTab(attributeMeta.referencingBoMetaId, boId, token, popupParameter, showSaveButton, showSelectButton, expandSideview);
		});
	}


	export interface IBoAttributeProperties {
		popupparameter: string;
	}

	export interface IBoAttribute {
		name: string;
		properties: IBoAttributeProperties;
		boAttribute: Object;
		value: Object;
	}


	@Component(nuclosApp, 'searchReferenceTarget', {
		controllerAs: '$ctrl',
		template: `
            <a class="search-reference search-entries-in-other-window" 
            ng-if="!td.field.readonly" 
            ng-click="$ctrl.searchReference()" 
            title="{{::\'webclient.button.search\' | i18n}}">
                <span class="glyphicon glyphicon-search"></span>
            </a>
            `,
		bindings: {
			boMetaId: '<',
			boInstance: '<',
			boAttributeName: '<',
			valueChangedCallback: '&',
			popupParameter: '<'
		}
	})
	class SearchReferenceTarget {

		private boMetaId: string;
		private boInstance: nuclos.model.BoImpl;
		private boAttributeName: string;
		private popupParameter;


		constructor(private dataService: nuclos.core.DataService,
					private metaService: nuclos.core.MetaService,
					private browserRefreshService: BrowserRefreshService) {
		}


		searchReference() {

			if (!this.boMetaId) {
				this.boMetaId = this.dataService.getMaster().boMetaId;
			}
			openBoInNewTab(
				this.boMetaId,
				undefined,
				this.boInstance,
				this.boAttributeName,
				false,
				true,
				false,
				true,
				this.popupParameter,
				this.browserRefreshService,
				this.metaService,
				this.dataService
			);
		}
	}


	@Component(nuclosApp, 'searchTemplateReferenceTarget', {
		controllerAs: '$ctrl',
		template: `
            <a class="search-template-reference search-entries-in-other-window" 
                ng-if="!td.field.readonly" 
                ng-click="$ctrl.searchReference()" 
                title="{{::\'webclient.button.search\' | i18n}}">
                <span class="glyphicon glyphicon-search"></span>
            </a>
            `,
		bindings: {
			boMetaId: '<',
			boAttribute: '<',
			boAttributeName: '<',
			valueChangedCallback: '&'
		}
	})
	class SearchTemplateReferenceTarget {

		private boMetaId: string;
		private boAttribute: IBoAttribute;
		private boAttributeName: string;
		private valueChangedCallback: Function;


		constructor(private dataService: nuclos.core.DataService,
					private metaService: nuclos.core.MetaService,
					private browserRefreshService: BrowserRefreshService) {
		}

		/**
		 * search for popupparameter in layout
		 */
		private findPopupParameter() {
			return this.findPopupParameterRecursive(this.dataService.getMaster().table, this.boAttributeName);
		}

		private findPopupParameterRecursive(jsonObject: Object, attributeShortName: string) {
			let r;
			for (let key in jsonObject) {
				let node: any = jsonObject[key];
				if (
					key === 'field'
					&& node.name && node.name === attributeShortName
					&& node.properties && node.properties.popupparameter
				) {
					return node.properties.popupparameter;
				}
				if (typeof node === 'object') {
					if ((r = this.findPopupParameterRecursive(node, attributeShortName)) !== null) {
						return r;
					}
				}
			}
			return null;
		}

		searchReference() {
			if (!this.boMetaId) {
				this.boMetaId = this.dataService.getMaster().boMetaId;
			}

			let token = this.browserRefreshService.addReferenceSelectionEventListener((searchEvent: SelectEntryInOtherWindowMessage) => {
				// update reference in main window
				this.boAttribute.value = {
					id: searchEvent.boId,
					name: searchEvent.name
				};

				if (this.valueChangedCallback) {
					this.valueChangedCallback();
				}

			});

			// open new tab to select reference
			this.metaService.getBoMetaData(this.boMetaId).then(meta => {
				let attributeMeta = meta.attributes[this.boAttributeName];
				openInNewTab(attributeMeta.referencingBoMetaId, undefined, token, this.findPopupParameter(), false, true, false);
			});
		}
	}


	@Component(nuclosApp, 'editReferenceTarget', {
		controllerAs: '$ctrl',
		template: `
            <a class="open-reference" ng-click="$ctrl.editReference()" title="{{::\'webclient.button.open\' | i18n}}">
                <span class="glyphicon glyphicon-new-window"></span>
            </a>
            `,
		bindings: {
			boMetaId: '<',
			boId: '<',
			boInstance: '<',
			boAttributeName: '<',
			popupParameter: '<'
		}
	})
	class EditReferenceTarget {

		private boMetaId: string;
		private boId: any;
		private boInstance: nuclos.model.BoImpl;
		private boAttributeName: string;
		private popupParameter;

		constructor(private dataService: nuclos.core.DataService,
					private metaService: nuclos.core.MetaService,
					private browserRefreshService: BrowserRefreshService) {
		}

		editReference() {
			openBoInNewTab(
				this.boMetaId,
				this.boId,
				this.boInstance,
				this.boAttributeName,
				true,
				false,
				true,
				false,
				this.popupParameter,
				this.browserRefreshService,
				this.metaService,
				this.dataService
			);
		}
	}

	@Component(nuclosApp, 'addReferenceTarget', {
		controllerAs: '$ctrl',
		template: `
            <a class="add-reference" 
				ng-if="::!td.field.readonly" 
				ng-click="$ctrl.addReference()" 
				title="{{::'webclient.button.new' | i18n}}">
                <span class="glyphicon glyphicon-plus"></span>
            </a>
            `,
		bindings: {
			boMetaId: '<',
			boInstance: '<',
			boAttributeName: '<',
			popupParameter: '<'
		}
	})
	class AddReferenceTarget {

		private boMetaId: string;
		private boInstance: nuclos.model.BoImpl;
		private boAttributeName: string;
		private popupParameter;

		constructor(private dataService: nuclos.core.DataService,
					private metaService: nuclos.core.MetaService,
					private browserRefreshService: BrowserRefreshService) {
		}

		public addReference() {
			openBoInNewTab(
				this.boMetaId,
				'new',
				this.boInstance,
				this.boAttributeName,
				true,
				false,
				true,
				true,
				this.popupParameter,
				this.browserRefreshService,
				this.metaService,
				this.dataService
			);
		}
	}


	@Component(nuclosApp, 'searchReferenceSelector', {
		controllerAs: '$ctrl',
		template: `
            <a id="select-entry-in-other-window" class="btn btn-success" ng-click="$ctrl.selectThisEntryInOtherWindow()">
                <span title="{{::\'webclient.button.selectEntryInOtherWindow\' | i18n}}" class="glyphicon glyphicon-ok"></span>
            </a>
            `,
		bindings: {
			showSaveandcloseButton: '=',
			showSelectButton: '=',
			onSelect: '&' // will be called after selecting entry
		}
	})
	class SearchReferenceSelector {

		private master;
		private showSaveandcloseButton: boolean;
		private showSelectButton: boolean;
		private onSelect: Function;

		constructor($location: ng.ILocationService,
					private $window: ng.IWindowService,
					private dataService: nuclos.core.DataService,
					private util: nuclos.util.UtilService,
					private browserRefreshService: BrowserRefreshService) {
			this.master = this.dataService.getMaster();

			if ($location.url().indexOf(SHOW_CLOSE_ON_SAVE_BUTTON_PARAM) != -1) {
				this.showSaveandcloseButton = true;
			}

			if ($location.url().indexOf(SHOW_SELECT_BUTTON_PARAM) != -1) {
				this.showSelectButton = true;
			}
		}

		selectThisEntryInOtherWindow() {
			let selectEntryAndClose = (bo) => {
				// send message
				let message = new nuclos.detail.SelectEntryInOtherWindowMessage();
				message.boMetaId = bo.boMetaId;
				message.boId = bo.boId;
				message.name = bo.title;
				message.token = Number(this.util.getRequestParameter(SELECT_ENTRY_IN_OTHER_WINDOW_TOKEN_PARAM));
				this.browserRefreshService.selectEntryInOtherWindow(message);

				this.$window.close();
			};
			let promise = this.onSelect();
			if (promise != null) {
				// create new bo
				promise.then((bo) => {
					selectEntryAndClose(bo);
				});
			} else {
				// select existing bo
				selectEntryAndClose(this.master.selectedBo);
			}
		}

	}

}
