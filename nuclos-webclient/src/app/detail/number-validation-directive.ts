module nuclos.detail {

	interface INumberValidationCtrl {
		$parsers: any;
		$setValidity: any;
		$formatters: any;
		$modelValue: any;
	}

	/*
	 * validate precision and scale for double values
	 */
	nuclosApp.directive('validNumber', function (dataService, localePreferences) {
		return {
			require: "ngModel",
			link: function (scope, elm, attrs, ctrl: INumberValidationCtrl) {

				var fieldUid = attrs['id'];
				var master = dataService.getMaster();
				var attributes = master.meta.attributes;
				if (attributes[fieldUid] == null) {
					return;
				}

				var type = attributes[fieldUid].type;
				var scale = attributes[fieldUid].scale;
				var precision = attributes[fieldUid].precision;

				if (type == "Decimal") {
					elm.css('text-align', 'right');

					ctrl.$parsers.unshift(function (viewValue) {
						if (viewValue != null) {

							if (viewValue.length == 0) {
								return null;
							}
							if (localePreferences.getLocale() === "de") {
								viewValue = viewValue.replace('.', '');
								viewValue = viewValue.replace(',', '.');
							} else {
								viewValue = viewValue.replace(',', '');
							}

							if (viewValue.indexOf('.') != -1) {
								var currentScale = viewValue.split('.')[0].length;
								var currentPrecision = viewValue.split('.')[1].length;
								if (currentPrecision > precision || currentScale > scale) {
									ctrl.$setValidity('validNumber', false);
								} else {
									ctrl.$setValidity('validNumber', true);
								}
							}
						}
						viewValue = parseFloat(viewValue);
						return viewValue;
					})

					ctrl.$formatters.push(function (value) {
						return formatNumberForShow(value, precision, scale, localePreferences);
					});

					elm.bind('blur', function () {
						var show = formatNumberForShow(ctrl.$modelValue, precision, scale, localePreferences);
						elm.val(show);
					});

				}

				if (type == "Integer") {
					elm.css('text-align', 'right');
				}
				//TODO implement parse and formatters for every nuclos meta type for input field, e.g. password, integer, time, ...
			}
		};
	});
}
