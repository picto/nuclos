module nuclos.detail {
	nuclosApp.directive('subform', function () {
		return {
			scope: {
				subform: '<',
				master: '<'
			},
			restrict: 'E',
			templateUrl: 'app/detail/subform.html',
			link: function ($scope, $element, $attrs) {

			}
		}
	});
}