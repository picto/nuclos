module nuclos.detail {
	nuclosApp.directive('tabbedPane', [function () {
		return {
			scope: {
				master: '<',
				tabs: '<'
			},
			restrict: 'EA',
			templateUrl: 'app/detail/tabbedpane.html',
			link: function ($scope, $element, $attrs) {

				$scope.clickOnTab = function (tab, tabs) {

					for (var i = 0; i < tabs.length; i++) {
						tabs[i].isActive = false;
					}

					tab.isActive = true;

				};
			}
		}
	}]);
}
