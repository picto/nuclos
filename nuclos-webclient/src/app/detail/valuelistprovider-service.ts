module nuclos.detail {
	
    export class VlpParameter {
        parameter: string;
        targetcomponent: string;
        type: string;
    }

    export class ValuelistproviderService {

        public vlpParams;

        constructor(private dataService: nuclos.core.DataService) {
            this.vlpParams = {};
        }

        private getKey(ref: string): string {
            return ref + '_' + this.dataService.getMaster().boMetaId;
        }

        public setParameter(vlpParam: VlpParameter, value: string) {

            let key = this.getKey(vlpParam.targetcomponent);

			if (!this.vlpParams[key]) {
				this.vlpParams[key] = {};
			}

			if (value) {
				this.vlpParams[key][vlpParam.parameter] = value;
			} else {
				delete this.vlpParams[key][vlpParam.parameter];
			}
        }

        public getParameters(reffield: string) {
            let key = this.getKey(reffield);
            return this.vlpParams[key];
        }

    }

    nuclosApp.service("valuelistproviderService", ValuelistproviderService);
}

