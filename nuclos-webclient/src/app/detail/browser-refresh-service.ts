module nuclos.detail {

    export class SelectEntryInOtherWindowMessage {
        boMetaId: string;
        boId: string;
        name: string;
        token: number;
    }

    /**
     * communication between browser instances (tabs/popups/windows):
     * - if data is saved in one browser instance all other browser instances will be notified to refresh and display updated refrences data
     * - references can be searched and selected in another browser instance (initiated from main form or from subform)
     * - references can be created (and then selected in initiating browser instance) in another browser instance (initiated from main form or from subform)
     * 
     * communication is realized with localStoarge events
     */
    export class BrowserRefreshService {

        static REFRESHBROWSERWINDOW_LOCALSTORAGEKEY = 'refreshbrowserwindow';
        static SELECTENTRYINOTHERWINDOW_LOCALSTORAGEKEY = 'selectEntryInOtherWindow';

        /**
         * this function is called on the initiating browser instance
         */
        addRefreshEventListener(callbackFunction: Function) {
            window.addEventListener('storage', (ev) => {
                if (ev.key != BrowserRefreshService.REFRESHBROWSERWINDOW_LOCALSTORAGEKEY) {
                    // ignore other keys
                    return; 
                }
                if (ev.newValue !== null) {
                    console.debug("refresh window data");

                    callbackFunction(); 
                }
            });
        }

        /**
         * notify other browser-tabs and popups to relaod data via storage events
         */
        refreshOtherBrowserInstances() {
            localStorage.setItem(BrowserRefreshService.REFRESHBROWSERWINDOW_LOCALSTORAGEKEY, 'refresh');
            localStorage.removeItem(BrowserRefreshService.REFRESHBROWSERWINDOW_LOCALSTORAGEKEY);
        }     




        /**
         * this function is called on the main window to select a reference in another browserinstance
         */
        addReferenceSelectionEventListener = (callbackFunction: Function /* SelectEntryInOtherWindowMessage*/) : number => {

            var token = new Date().getTime();

            // add an eventListener to get selection from other tab
            // var eventListener = function(ev) {
            var eventListener = (ev) => {
                if (ev.key != BrowserRefreshService.SELECTENTRYINOTHERWINDOW_LOCALSTORAGEKEY) {
                    // ignore other keys
                    return;
                }
                if (ev.newValue !== null) {

                    var searchEvent: SelectEntryInOtherWindowMessage = JSON.parse(ev.newValue);

                    console.debug("local storage event recieved:", searchEvent)

                    if (searchEvent.token != token) {
                        return;
                    }

                    // update selected refrence in main window
                    callbackFunction(searchEvent);

                    window.removeEventListener('storage', eventListener);

                    // this shouldn't be necessary - in angular components (>ng 1.5) there is no scope anymore
                    // but for whatever reason we need to apply changes here to the scope                        
                    var $rootScope = angular.element(document.querySelector('#main')).scope();
                    $rootScope.$apply();
                }
            };
            window.addEventListener('storage', eventListener);
            return token;
        };

        /**
         * this function is called in the second window
         * fires the event listener in the main window
         */
        selectEntryInOtherWindow = (message: SelectEntryInOtherWindowMessage) : void => {
            // send message
            console.debug("send local storage event:", message);
            localStorage.setItem(
                BrowserRefreshService.SELECTENTRYINOTHERWINDOW_LOCALSTORAGEKEY,
                JSON.stringify(message)
            );
            localStorage.removeItem(BrowserRefreshService.SELECTENTRYINOTHERWINDOW_LOCALSTORAGEKEY);
        };
        
    }
    
    var factory = ($q) => {
        return new BrowserRefreshService();
    };

    nuclosApp.factory("browserRefreshService", factory);
}