function prepareBoValuesAndOptions(attributes, bo) {
	if (!bo) {
		return;
	}

	if (!bo.attributes) {
		bo.attributes = {};
	}

	if (!bo.dropdownoptions) {
		bo.dropdownoptions = {};
	}

	bo.showGenerationDropdown = false;

	if (bo.generations) {
		for (var i in bo.generations) {
			if (!bo.generations[i].internal) {
				bo.showGenerationDropdown = true;
				break;
			}
		}
	}

	for (var i in attributes) {
		var attribute = attributes[i];

		var value = bo.attributes[i];

		if (attribute.reference) {

		} else if ((attribute.type === 'Date' || attribute.type === 'Timestamp') && value) {
			//TODO: Why this? This is dangerous and causes side-effects and errors. It transforms data directly from server in a another format and class
			//and this cannot be sent back directly (see nc-ui-grid-directive.ts line 490ff)
			//Even worse: It happens only for newBos and NOT for existing BOs. This is completely wrong and has unpredictable effects.
			bo.attributes[i] = moment(value).toDate();
		} else if (attribute.type === 'Decimal' && value) {
			bo.attributes[i] = parseFloat(value);

		}
	}

}

function assembleBoInBoList(bo, boList) {
	if (!bo || !boList) {
		return;
	}

	var returnBO = undefined;

	for (var i in boList) {
		var row = boList[i];

		if (!row.boId) {

			if (bo._flag === 'insert') {
				boList[i] = bo;
				delete bo._flag;
			}

		} else if (row.boId == bo.boId) {

			if (bo._flag === 'delete') {
				boList.splice(i, 1);

			} else {
				boList[i] = bo;
				returnBO = bo;
			}

		}

	}

	return returnBO;
}

function handleFileUpload(bo, fieldname, files, changeCallback, bImage) {
	if (!bo || !fieldname || !files || files.length === 0) {
		return;
	}

	//TODO: Handle big sized files that arrive in several chunks.
	for (var i in files[0].chunks) {
		var chunk = files[0].chunks[i];
		var fileObj = chunk.fileObj;
		var func = (fileObj.file.slice ? 'slice' :
			(fileObj.file.mozSlice ? 'mozSlice' :
				(fileObj.file.webkitSlice ? 'webkitSlice' :
					'slice')));

		var bytes = fileObj.file[func](chunk.startByte, chunk.endByte);

		var reader = new window['FileReader']();
		reader.readAsDataURL(bytes);
		reader.onloadend = function () {
			var base64data = reader.result;

			if (bImage) {
				if (!bo.attrImages) {
					bo.attrImages = {links: {}};
				}

				bo.attrImages.links[fieldname] = {href: base64data};
			}

			// remove prefix
			var byteString = base64data.substring(base64data.indexOf('base64,') + 'base64,'.length);

			bo.attributes[fieldname] = {
				data: byteString,
				name: files[0].name
			};

			if (changeCallback) {
				changeCallback();
			}
		};
	}
}