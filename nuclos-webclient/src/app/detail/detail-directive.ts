module nuclos.detail {

	import Component = nuclos.util.Component;
	import SearchtemplateService = nuclos.searchtemplate.SearchtemplateService;
	import Mandator = nuclos.model.Mandator;

	@Component(nuclosApp, "detailblock", {
		bindings: {
			master: "=",
			boId: "@",
			boMetaId: "@",
			reffield: "@?",
			subformboid: "@?"
		},
		controllerAs: "vm",
		templateUrl: "app/detail/detailblock.html"
	})
	class Detail {

		private master: nuclos.model.interfaces.BoViewModel;
		private authentication;
		private searchtemplateModel: nuclos.searchtemplate.SearchtemplateModel;
		private tableviewControlObject: any = {}; // holds isolated scope to call functions inside directive
		private printoutModalOpened: boolean = false;
		private dataEditor;

		private showSubformButtons: boolean; // true if detail modal was opened from subform entry
		private reffield: string;
		private subformboid: string;


		constructor(
			private $http: ng.IHttpService,
			private $cookieStore: ng.cookies.ICookieStoreService,
			private $timeout: ng.ITimeoutService,
			private $uibModal,
			private $q: ng.IQService,

			private $resource: ng.resource.IResourceService,
			private $rootScope: ng.IRootScopeService,
			private layoutService,
			private authService: nuclos.auth.AuthService,
			private dialog: nuclos.dialog.DialogService,
			private dataService: nuclos.core.DataService,
			private boModalService,
			private boService: nuclos.core.BoService,
			private localePreferences: nuclos.clientprefs.LocalePreferenceService,
			private browserRefreshService: BrowserRefreshService,
			private errorhandler,
			private nuclosI18N: nuclos.util.I18NService
		) {

			this.authentication = this.$cookieStore.get('authentication');
			this.master = this.getMaster();

			if (this.subformboid !== undefined) {
				this.showSubformButtons = true;
			}

			// update model if selected bo changed - e.g. when opening a node from the tree
			this.$rootScope.$watch('master.boMetaId', (newValue, oldValue) => {
				if (oldValue != newValue) {
					this.master = this.getMaster();
				}
			});


			var destroyRowselectedListener = this.$rootScope.$on('rowselected', (event, data) => {
				// open selected tab in each tabpane
				for (let tabs of data.layoutLoadedActions.tabpanesToSelect) {
					if (tabs.tabs.length != 0) {
                        var activeTabIndex = tabs.tabs.map((tab) => {
							return tab.isActive;
						}).indexOf(true);
                        if (activeTabIndex < 0) {
                            tabs.tabs[0].isActive = true;
                        }
					}
				}
			});

			this.$rootScope.$on('$destroy', () => { // directive is removed
				destroyRowselectedListener();
			});

			this.searchtemplateModel = SearchtemplateService.searchtemplateModel;

		}


		private getMaster(): nuclos.model.interfaces.BoViewModel {
			if (this.master) {
				return this.master;
			}

			//TODO: Re-factor and bring straight logic into it
			var master = this.dataService.getMaster();
			if (master.subentry) { // a row was selected, detail-form is shown in modal
				master = master.subentry;
			}

			return master;
		};

		public deleteSubrow() {
			this.getMaster().dirty = true;
			this.deleteUpdateOrInsertSubrow(true);
		};



		public subformOkButton() {
			// validate fields
			if (this.dataEditor.$error.required && this.dataEditor.$error.required.length > 0) {
				this.dialog.alert(
					this.nuclosI18N.getI18n('webclient.dialog.requiredfields.notfilled.message'),
					'',
					function () {
						// ok clicked
					}
				);
			} else {
				this.deleteUpdateOrInsertSubrow(false);
			}
		}


		private deleteUpdateOrInsertSubrow(doDelete) {

			var modalOkFunction = () => {
				// TODO: bo-modal-service.js still makes use of direct scope access
				var detailblockModalScope = angular.element($('#detailblockModal')).scope();
				detailblockModalScope['ok']();
			};

			// boViewModel of main view
			var master = this.dataService.getMaster();

			// boViewModel of modal view
			var entity = this.getMaster();

			var subBos = master.selectedBo.subBos[this.reffield].bos;

			// update master
			var rowIndex = this.master.selectedBoIndex;

			if (rowIndex != -1) {

				if (master.selectedBo._flag != "insert") {
					master.selectedBo._flag = "update";
				}

				subBos[rowIndex].attributes = this.master.selectedBo.attributes;

				if (doDelete) {
					master.selectedBo.subBos[this.reffield].bos[rowIndex]._flag = 'delete';
				} else {
					if (this.master.selectedBo.boId != null) {
						subBos[rowIndex]._flag = 'update';
					} else {
						subBos[rowIndex]._flagSave = 'insert';
					}
				}

				// this is from the old "get title and info for edited entry in simple list view"
				if (entity.selectedBo.boId) {
					entity.selectedBo.boMetaId = entity.boMetaId;
					modalOkFunction();
				}
			}

			delete master.subentry;

			// update subform tableview
			if (this.tableviewControlObject.updateTable) {
				this.tableviewControlObject.updateTable(entity.boMetaId, 0, subBos);
			} else {
				// workaround because tableviewControlObject is not available in modal
				this.$rootScope.$emit('detail:datachanged',
					{
						boMetaId: entity.boMetaId,
						offset: 0,
						bos: subBos
					}
				);
			}

			// close modal
			modalOkFunction();

		}

		public doUserPushButton(postData) {

			var deferred = this.$q.defer();

			var editedBO = this.boService.getEditingDataForSave(this.getMaster().selectedBo, this.getMaster().meta, false);
			this.boService.submitBOForInsertUpdate(postData, this.getMaster(), editedBO._flag).then(
				(data) => {
					if (postData._reloadTab) {
						this.layoutService.createTableForThisBO(this.getMaster(), data);
						delete postData._reloadTab;
					}

					//TODO: layoutService.createTableForThisBO is a promise, shouldn't the broadcast we sent after it came back?
					this.$rootScope.$broadcast('refreshgrids'); // refresh active subforms
					this.clearDataOfInactiveSubforms(); // clear cata of inactive subforms - so it will be reloaded on next subform click

					deferred.resolve(this.getMaster().selectedBo);
				},
				(error) => {
					// not ok

					postData.inputrequired = error.inputrequired;
					deferred.reject(postData);

				}
			);
			return deferred.promise;
		}


		private clearDataOfInactiveSubforms() {
			var inactiveTabs = this.layoutService.getTabs(this.getMaster().table).filter(
				(item) => {
					return !item.isActive;
				}
			);
			for (let inactiveTab of inactiveTabs) {
				var subforms = this.layoutService.getSubforms(inactiveTab.table);
				for (let subform of subforms) {
					delete this.getMaster().selectedBo.subBos[subform.reffield].bos;
				}
			}
		}

		private loadLovDropDownOptionsHelper(field, boId, quickSearchInput, selectedId, mandatorId) {

			var deferred = this.$q.defer();

			// prevent flicker when opening dropdown
			var openDropdownHtmlItems = $('.ui-select-container.open ul');
			openDropdownHtmlItems.css('display', 'none');

			this.boService.loadDropDownOptions(field.select, boId, quickSearchInput, mandatorId).then((data) => {

				// add an empty entry at first position of the dropdown array if dropdown is not a required field
				if (field.required !== undefined && !field.required) {
					data.unshift({ name: '', id: null });
				}

				openDropdownHtmlItems.css('display', 'initial');

				// mark selected dropdown options as active -> update css class
				if (selectedId) {
					for (var i in data) {
						if (data[i].id == selectedId) {
							this.$timeout(() => {
								$('.ui-select-choices-row').removeClass('active');
								$($('.ui-select-choices-row')[i]).addClass('active');
							});
							break;
						}
					}
				}
				deferred.resolve(data);
			});
			return deferred.promise;
		}


		public save(entity, isValid, refreshOtherBrowserInstances) {
			if (entity !== this.getMaster()) {
				return;
			}
			return this.boService.saveBo(entity.selectedBo, entity, isValid, refreshOtherBrowserInstances);
		}



		private openPrintoutModal(printoutData, printoutsLink) {

			if ($('#printouts-modal').length > 0) {
				// modal alread opened
				return;
			}

			this.printoutModalOpened = true;

			// open dialog
			var modalInstance = this.$uibModal.open({
				templateUrl: 'app/detail/printout-dialog.html',
				controller: ($scope, $uibModalInstance) => {

					$scope.printouts = printoutData;
					$scope.dateFormat = this.localePreferences.getDateFormat();

					$scope.close = () => {
						$uibModalInstance.close();
					};

					// TODO create printoutController and move function to it
					$scope.loadLovDropDownOptionsPrintout = (field, bo, quickSearchInput, selectedId) => {
						let mandatorId = (bo.attributes && bo.attributes.nuclosMandator) ? bo.attributes.nuclosMandator.id : undefined;
						this.loadLovDropDownOptionsHelper(field, bo.id, quickSearchInput, selectedId, mandatorId).then((data) => {
							bo.dropdownoptions = data;
						});
					};


					/**
					 * execute button will be enabled when at least 1 printout is selected
						* and all required fields are filled
						*/
					$scope.executeButtonEnabled = function (printoutsData) {
						var nrOfSelectedPrintouts = 0;
						var requiredFieldsFilled = true;
						for (let item of printoutsData) {
							if (item.outputFormats) {
								for (var j = item.outputFormats.length - 1; j >= 0; j--) {
									var outputFormat = item.outputFormats[j];
									if (outputFormat.selected) {
										nrOfSelectedPrintouts++;
										if (outputFormat.parameters) {
											for (let param of outputFormat.parameters) {
												if (!param.nullable && !param.value) {
													requiredFieldsFilled = false;
												}
											}
										}
									}
								}
							}
						}
						return nrOfSelectedPrintouts != 0 && requiredFieldsFilled;
					};


					$scope.executePrintout = (printoutsData) => {

						// submit selected outputFormat to the server
						var postData = angular.copy(printoutsData);
						for (let item of postData) {
							for (var j = item.outputFormats.length - 1; j >= 0; j--) {
								var outputFormat = item.outputFormats[j];

								// TODO: ui-select component in printout-dialog.html stores the value in parameter.value.name.name
								// maybe there is a more elegant way to configure where to store the model value
								if (outputFormat.parameters) {
									for (let parameter of outputFormat.parameters) {
										if (parameter.value && parameter.value.name && parameter.value.name.name) {
											parameter.value = parameter.value.name.name;
										}
									}
								}

								if (!outputFormat.selected) {
									item.outputFormats.splice(j, 1);
								}
							}

						}


						this.$http.post(printoutsLink, postData).
							success((data: any) => {
								$scope.printoutLinksResult = data;

								// merge links from result into existing object
								for (let printout of data) {
									for (let outputFormat of printout.outputFormats) {
										for (let _printout of $scope.printouts) {
											if (_printout.printoutId == printout.printoutId) {
												for (let _outputFormat of _printout.outputFormats) {
													if (_outputFormat.outputFormatId == outputFormat.outputFormatId) {
														_outputFormat.links = outputFormat.links;
														_outputFormat.fileName = outputFormat.fileName;
													}
												}
											}
										}
									}
								}
							})
							.error((data, status) => {
								this.errorhandler(data, status);
							});

						$scope.printoutExecuted = true;
					};

				}
			});


			modalInstance.result.then(function (selectedItem) {
				// ok clicked

				modalInstance.close();
				this.printoutModalOpened = false;

			}, function () {
				// modal dismissed - esc / cancel clicked

				modalInstance.close();
				this.printoutModalOpened = false;
			});

		}




		/**
		 * open printout dialog (if necessary)
		 */
		public openPrintouts(printoutsLink) {
			this.$resource(printoutsLink).query((data) => {
				this.openPrintoutModal(data, printoutsLink);
			});
		}

		/**
		 * Unlock the record
		 */
		public unlock(lockLink) {
			var master = this.getMaster();
			var selectedBo = master.selectedBo;
			this.$http.delete(lockLink).success(() => {
				// TODO
				var sideviewScope: any = angular.element($("#main")).scope();
				sideviewScope.masterSelectBoHandler(master, master.selectedBo).then(() => {
				});

			}).error((data, status) => {
				this.errorhandler(data, status);
			});
		}

		public openCharts() {

			var selectedBo = this.master.selectedBo;
			this.$uibModal.open({
				templateUrl: 'app/charts/chart.html',
				controller: nuclos.chart.ChartCtrl,
				size: 'lg',
				windowClass: 'fullsize-modal-window',
				resolve: {

					// this will be injected into ChartCtrl constructor
					bo: () => {
						// return this.master.selectedBo;
						return selectedBo;
						// },
						// chartConfiguration: function () {
						// 	return  chartConfiguration;
					}
				}
			}).result.then(function () {
				// ok
			});

		}

		public cancel() {
			var master = this.getMaster();
			delete master.dirty;
			var selectedBo = master.selectedBo;

			if (selectedBo && selectedBo._flag === 'insert') {
				master.setSelectedBo(undefined);
			}

			if (master.bos) {
				for (var i=0; i<master.bos.length; i++) {
					var bo = master.bos[i];
					if (bo._flag === 'insert') {
						master.bos.splice(i, 1);
					}
				}
			}

			if (selectedBo && selectedBo._flag) {
				if (selectedBo._flag === 'insert') {
					master.setSelectedBo(undefined);
					this.$rootScope.$broadcast('refreshgrids');
				} else {
					this.boService.loadSingleBO(master, selectedBo, master.bos).then((loadedBo) => {
						this.layoutService.checkLayoutChangeAfterCancel(master, selectedBo, loadedBo);
						master.setSelectedBo(loadedBo);
						this.$rootScope.$broadcast('refreshgrids');
					});
				}
				delete selectedBo._flag;
			}
		}

		public delete() {
			var entity = this.getMaster();
			this.dialog.confirm(
				this.nuclosI18N.getI18n("webclient.button.delete"),
				this.nuclosI18N.getI18n("webclient.dialog.delete"),
				() => {
					// ok
					var editedBO = this.boService.getEditingDataForSave(entity.selectedBo, entity.meta, true);

					this.boService.submitBOForDelete(editedBO, entity).then((data) => {
						// ok
						this.browserRefreshService.refreshOtherBrowserInstances();
					},
						() => {
							// not ok
						});
				},
				() => {
					// cancel
				}
			);
		}


		/**
		 * drag&drop from tree to subform
		 */
		private onDropComplete(data, evt) {

			var grid = $(document.elementFromPoint(evt.x, evt.y)).closest('nc-ui-grid');
			var acceptDraggable = grid.find('.ui-grid-container').hasClass('accept-draggable');
			if (acceptDraggable) {
				var targetBoMetaId = grid.attr('bometaid');
				var reffield = grid.attr('reffield');

				var entity = this.dataService.getMaster();
				var selectedBo = entity.selectedBo;

				var subBosRestriction = selectedBo.subBos[reffield].restriction;
				if (subBosRestriction != undefined && subBosRestriction == "readonly") {
					//READONLY, NO ACTION
				} else {
					var subEntity = {
						boMetaId: targetBoMetaId,
						bos: selectedBo.subBos[reffield].bos,
						meta: selectedBo.submetas[targetBoMetaId],
					};

					var newScope = this.$rootScope.$new();
					newScope['reffield'] = reffield;

					this.boModalService.openEditModal(
						{
							entity: subEntity,
							parentEntity: entity,
							preselectReferenceData: data,
							scope: newScope
						}
					);

					entity.dirty = true;
				}

			}

			$('*').removeClass('accept-draggable');
			$('*').removeClass('reject-draggable');

		}

		public getMandatorColor(mandatorId: string) {
			let mandators = this.authService.getMandators()
			return mandators
				.filter(m => m.mandatorId === mandatorId)
				.map(m => m.color)
				.shift();
		}

	}
}
