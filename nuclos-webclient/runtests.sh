
# start selenium server if not already running
#if [ `ps -ef|grep [w]ebdriver-manager|wc -l` -eq 0 ]
#then
#  nohup ./node_modules/protractor/bin/webdriver-manager start &
#fi

# run the tests
./node_modules/protractor/bin/protractor test/e2e/protractor_conf.js

# copy screenshots into unique directory
rev=`git rev-parse --short HEAD`
mkdir -p test/e2e/output/screenshots/history/$rev/screenshots
cp -R  test/e2e/output/screenshots/current/* test/e2e/output/screenshots/history/$rev/
