#!/bin/bash

echo "Waiting until the Nuclos server is ready..."

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

tail -f "$DIR/target/cargo/logs/container.log" | while read LOGLINE
do
   [[ "${LOGLINE}" == *"Spring main context setSpringReady: 3"* ]] && echo "Nuclos server ready" && pkill -P $$ tail
done
