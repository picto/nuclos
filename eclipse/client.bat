rem echo off

setlocal

set "APP_DIR=C:\nuclos\nuclos40\webapp\app"
set "SERVER=http://localhost:8080/nuclos-war"
set "JAVA_OPTIONS=-ea -ms256m -mx768m -Dserver=%SERVER% -XX:PermSize=128M -XX:MaxPermSize=256M -XX:+UseThreadPriorities"

rem Ensure that any user defined CLASSPATH variables are not used on startup,
rem but allow them to be specified in setenv.bat, in rare case when it is needed.
set CLASSPATH=

rem Get standard environment variables
if not exist "setenv.bat" goto setenvDone
call "setenv.bat"
:setenvDone

rem Get standard Java environment variables
if exist "%~dp0\setclasspath.bat" goto okSetclasspath
echo Cannot find "setclasspath.bat"
echo This file is needed to run this program
goto end
:okSetclasspath
call "%~dp0\setclasspath.bat"
rem dir
if errorlevel 1 goto end

cd "%APP_DIR%"

rem Add jnlp file (if not done before)
if exist "jnlp-1.0.jar" goto jnlpDone
copy "%HOMEPATH%\.m2\repository\de\novabit\jnlp\1.0\jnlp-1.0.jar" "."
:jnlpDone

rem unpack pack200 stuff
rem http://stackoverflow.com/questions/3215501/batch-remove-file-extension
rem http://www.tomshardware.co.uk/forum/230090-45-windows-batch-file-output-program-variable
for %%a in (*.pack.gz) do (
rem %JAVA_HOME%\bin\unpack200 %t
rem set "TST=%%a"
rem echo %%s %%~ns %_tst%
rem echo %%a %TST% 
)

rem exec java
rem call 
rem http://stackoverflow.com/questions/524081/bat-file-to-create-java-classpath
setLocal EnableDelayedExpansion
set CLASSPATH="
for %%a in (*.jar) do (
   set CLASSPATH=!CLASSPATH!;%%a
)
set CLASSPATH=!CLASSPATH!"
 
%_RUNJAVA% %JAVA_OPTIONS% -cp "!CLASSPATH!" org.nuclos.client.main.Main
:end
