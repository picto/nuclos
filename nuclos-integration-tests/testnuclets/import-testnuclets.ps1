#POWERSHELL
$NUCLOS_URL="http://127.0.0.1:8080/nuclos-war"
$NUCLOS_USER="nuclos"
$NUCLOS_PASSWORD=""

[System.Threading.Thread]::CurrentThread.CurrentUICulture = [System.Globalization.CultureInfo]::GetCultureInfo('en-US')
[System.Threading.Thread]::CurrentThread.CurrentCulture = [System.Globalization.CultureInfo]::GetCultureInfo('en-US')

echo "Importing test-nuclets..."

$body = '{"username":"'+$NUCLOS_USER+'", "password":"'+$NUCLOS_PASSWORD+'"}'

$result=Invoke-RestMethod -Method Post -Uri "$NUCLOS_URL/rest" -Body $body -ContentType 'application/json' -SessionVariable myWebSession
$cookies = $myWebSession.Cookies.GetCookies("$NUCLOS_URL/rest") 
foreach ($cookie in $cookies) { 
     Write-Host "$($cookie.name) = $($cookie.value)" 
}

$version=Invoke-RestMethod -Method Get -Uri "$NUCLOS_URL/rest/version" -ContentType 'application/json' -WebSession $myWebSession
echo NUCLOS-VERSION: $version

If (Test-Path $env:temp\nucletimport.nuclet.zip){ 
	Remove-Item $env:temp\nucletimport.nuclet.zip 
}
If (Test-Path $env:temp\nucletimport.nuclet){ 
	Remove-Item $env:temp\nucletimport.nuclet 
}
$nucletdir = $PSScriptRoot+"\restexample\*"
Compress-Archive -CompressionLevel NoCompression -Update -Path $nucletdir -DestinationPath $env:temp\nucletimport.nuclet.zip
Rename-Item $env:temp\nucletimport.nuclet.zip nucletimport.nuclet

Invoke-RestMethod -WebSession $myWebSession -Uri "$NUCLOS_URL/rest/maintenance/nucletimport" -Method Post -InFile $env:temp\nucletimport.nuclet -ContentType 'application/octet-stream' 
Echo "finish"
