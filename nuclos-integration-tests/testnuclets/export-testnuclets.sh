#!/bin/bash
# exports configured nuclet from DB into filesystem - existing nuclet files will be overwritten


export NUCLOS_URL=${1:-'http://127.0.0.1:8080/nuclos-war'}
export NUCLOS_USER=nuclos
export NUCLOS_PASSWORD=

export NUCLET_NAME=""


#export nucletnames=("Rest%20example" "matrixtest" "Test%20Utils" "TestTabindex" "Test%20Rules" "Test%20Other")
#export nucletdirs=("restexample" "matrixtest" "testutils" "tabindex" "testrules" "testother")

export nucletnames=("Rest%20example")
export nucletdirs=("restexample")



# directory where this script is stored
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
sessionid=`curl $NUCLOS_URL/rest -X POST -H "Accept:application/json" -H "Content-Type: application/json" -d '{"username":"'$NUCLOS_USER'", "password":"'$NUCLOS_PASSWORD'"}' | awk -v FS="\"" '{ print $4  }'`
echo "sessionId=$sessionid"

for i in "${!nucletnames[@]}"
do
   echo "key  : $i"
   echo "value: ${nucletnames[$i]}"
   echo "value: ${nucletdirs[$i]}"

   export NUCLET_DIR="${nucletdirs[$i]}"
   export tmp_nuclet=$DIR/$NUCLET_DIR.nuclet
   export NUCLET_NAME="${nucletnames[$i]}"

   rm -Rf $tmp_nuclet

   echo "curl --cookie "JSESSIONID=$sessionid" "$NUCLOS_URL/rest/maintenance/nucletexport/$NUCLET_NAME?sessionid=$sessionid" > $tmp_nuclet"
   curl --cookie "JSESSIONID=$sessionid" "$NUCLOS_URL/rest/maintenance/nucletexport/$NUCLET_NAME?sessionid=$sessionid" > $tmp_nuclet

   echo Nuclet exported to $tmp_nuclet

   rm -Rf $DIR/$NUCLET_DIR && mkdir $DIR/$NUCLET_DIR && cd $DIR/$NUCLET_DIR && unzip -o $tmp_nuclet
done


rm $DIR/*.nuclet

