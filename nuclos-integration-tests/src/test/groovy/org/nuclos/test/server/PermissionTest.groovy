package org.nuclos.test.server

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper

import groovy.transform.CompileStatic

/**
 * @author Oliver Brausch <oliver.brausch@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PermissionTest extends AbstractNuclosTest {
	static RESTClient suclient = new RESTClient('nuclos', '')
	static RESTClient client
	static Long auftrag1Id;
	static Long auftrag2Id;
	static Long rechnung1Id;
	static Long rechnung2Id;
	static EntityObject<Long> auftrag

	@Test
	void _00_createTestUser() {
		RESTHelper.createUser('testpt', 'testpt', ['Example user', 'Example readonly'], nuclosSession)
		client = new RESTClient('testpt', 'testpt')
	}

	@Test
	void _02_login() {
		client.login()
		assert client.sessionId
	}

	@Test
	void _05_insertEoReadOnlyEntity() {
		// Entity Rechnung that is only readonly for user test

		EntityObject<Long> rechnung = new EntityObject(TestEntities.EXAMPLE_REST_RECHNUNG)
		rechnung.setAttribute('name', 'Rechnung 2')
		assert !rechnung.id

		client.save(rechnung)

		assert !rechnung.id // It has no id, because it couldn't be created
	}

	@Test
	void _06_insertEoReadNoIntitalStateTransition() {
		// Entity Auftrag that where the first state transition is not allowed for user test

		EntityObject<Long> auftrag = new EntityObject(TestEntities.EXAMPLE_REST_AUFTRAG)

		auftrag.setAttribute('name', 'Auftrag 3')
		auftrag.setAttribute('bemerkung', 'Bemerkung 3')

		assert !auftrag.id

		client.save(auftrag)

		assert !auftrag.id // It has no id, because it couldn't be created
	}

	@Test
	void _10_createTestData() {
		Map rechnung = [name: 'Rechnung 1'] as Map<String, Serializable>
		rechnung1Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_RECHNUNG.fqn, rechnung, nuclosSession)['boId']

		assert rechnung1Id > 0

		rechnung = [name: 'Rechnung 2'] as Map<String, Serializable>
		rechnung2Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_RECHNUNG.fqn, rechnung, nuclosSession)['boId']

		assert rechnung2Id > 0

		Map auftrag = [name: 'Auftrag 1', bemerkung: 'Bemerkung', nogroup: 'NoGroup1', rechnung: [id: rechnung1Id], anzahl: 5] as Map<String, Serializable>
		auftrag1Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_AUFTRAG.fqn, auftrag, nuclosSession)['boId']

		assert auftrag1Id > 0

		auftrag = [name: 'Auftrag 2', nogroup: 'NoGroup2'] as Map<String, Serializable>
		auftrag2Id = (Long) RESTHelper.createBo(TestEntities.EXAMPLE_REST_AUFTRAG.fqn, auftrag, nuclosSession)['boId']

		assert auftrag2Id > 0


	}

	@Test
	void _12_readEoNoPermission() {
		List<EntityObject<Long>> lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION, null)
		assert lst == null
	}

	@Test
	void _14_readEoNoDelete() {
		List<EntityObject<Long>> lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst
		assert lst.size() == 2

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag2Id)

		assert auftrag && auftrag.id == auftrag2Id
		assert auftrag.getAttribute('name') == 'Auftrag 2'

		auftrag.delete()

		// Should not be able to delete, so nothing changed
		lst = client.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst
		assert lst.size() == 2
	}

	@Test
	void _16_readEo() {
		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag && auftrag.id == auftrag1Id

		assert auftrag.getAttribute('name') == 'Auftrag 1'
		assert !auftrag.getAttribute('bemerkung') // No right to read
		assert !auftrag.getAttribute('nogroup') // No right to read
		assert auftrag.getAttribute('rechnung')['name'] == 'Rechnung 1'
		assert auftrag.getAttribute('anzahl') == 5
	}

	@Test
	void _18_updateEo() {
		auftrag.setAttribute('name', 'New Auftrag')
		auftrag.setAttribute('bemerkung', 'New Bemerkung')
		auftrag.setAttribute('nogrup', 'New Group')
		auftrag.setAttribute('anzahl', 13)
		auftrag.setAttribute('rechnung', [id: rechnung2Id])
		auftrag.save()

		EntityObject<Long> savedAuftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert savedAuftrag.getAttribute('name') == 'Auftrag 1' // Didn't change, no right to write
		assert savedAuftrag.getAttribute('rechnung')['name'] == 'Rechnung 1' // Didn't change, no right to write
		assert savedAuftrag.getAttribute('anzahl') == 13
		assert !savedAuftrag.getAttribute('bemerkung') // No right to read
		assert !savedAuftrag.getAttribute('nogroup') // No right to read
	}

	@Test
	void _20_addDependents() {
		List<EntityObject<?>> dependents = auftrag.getDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		3.times { i ->
			dependents << new EntityObject(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION).with {
				it.setAttribute('name', 'Position ' + i)
				it.setAttribute('bemerkung', 'Bemerkung ' + i)
				it.setAttribute('nummer', i)
				it.setAttribute('neu', true)
				return it
			}
		}

		auftrag.save()

		List<EntityObject<Long>> savedDependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert savedDependents*.getAttribute('name').sort() == ['Position 0', 'Position 1', 'Position 2']
		assert savedDependents*.getAttribute('bemerkung').sort() == ['Bemerkung 0', 'Bemerkung 1', 'Bemerkung 2']
		assert savedDependents*.getAttribute('nummer').sort() == [0, 1, 2]
		savedDependents*.getAttribute('neu').sort() == [false, false, false] //Not all to write into it, but to read it
	}

	@Test
	void _22_updateDependents() {
		List<EntityObject<?>> dependents = auftrag.getDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		EntityObject<?> dep1 = dependents.find {
			it.getAttribute('name') == 'Position 0'
		}

		dep1.setAttribute('name', 'Position 007')
		dep1.setAttribute('nummer', '7')
		dep1.setAttribute('neu', true)

		auftrag.save()

		List<EntityObject<Long>> savedDependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert savedDependents.size() == 3

		dep1 = savedDependents.find {
			it.getAttribute('name') == 'Position 007'
		}

		assert dep1
		assert dep1.getAttribute('nummer') == 7
		assert dep1.getAttribute('neu') == false // No right to change
	}

	@Test
	void _24_deleteDependents() {
		List<EntityObject<?>> dependents = auftrag.getDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents.size() == 3

		dependents.find {
			it.getAttribute('name') == 'Position 1'
		}.flagDeleted()

		auftrag.save()

		List<EntityObject<Long>> savedDependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert savedDependents.size() == 3
		// Nothing was deleted, because there is no right to delete in this state for these roles

		assert savedDependents*.getAttribute('nummer').sort() == [1, 2, 7]
	}

	@Test
	void _26_changeStateTo_20() {
		auftrag.changeState(20)

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Bearbeitung'
	}

	@Test
	void _30_readEoState20() {
		assert auftrag.getAttribute('name') == 'Auftrag 1'
		assert auftrag.getAttribute('bemerkung') == 'Bemerkung'
		assert !auftrag.getAttribute('nogroup') // No right to read
		assert auftrag.getAttribute('rechnung')['name'] == 'Rechnung 1'
		assert auftrag.getAttribute('anzahl') == 13
	}

	@Test
	void _32_updateEoState20() {
		auftrag.setAttribute('name', 'New Auftrag')
		auftrag.setAttribute('bemerkung', 'New Bemerkung')
		auftrag.setAttribute('nogrup', 'New Group')
		auftrag.setAttribute('anzahl', 17)
		auftrag.setAttribute('rechnung', [id: rechnung2Id])
		auftrag.save()

		EntityObject<Long> savedAuftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert savedAuftrag.getAttribute('name') == 'New Auftrag' // Changed, right to write
		assert savedAuftrag.getAttribute('rechnung')['name'] == 'Rechnung 2' // Changed, right to write
		assert savedAuftrag.getAttribute('anzahl') == 13 // Didn't change, no rights to write
		assert savedAuftrag.getAttribute('bemerkung') == 'New Bemerkung' // Changed, right to write
		assert !savedAuftrag.getAttribute('nogroup') // No right to read
	}

	@Test
	void _35_checkDependents20() {
		List<EntityObject<?>> dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents == null // No rights to subform/dependents at all
	}

	@Test
	void _38_changeStateTo_50() {
		auftrag.changeState(50)

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Fertig'
	}

	@Test
	void _40_readEoState50() {
		assert auftrag.getAttribute('name') == 'New Auftrag'
		assert auftrag.getAttribute('bemerkung') == 'New Bemerkung'
		assert !auftrag.getAttribute('nogroup') // No right to read
		assert auftrag.getAttribute('rechnung')['name'] == 'Rechnung 2'
		assert auftrag.getAttribute('anzahl') == 13
	}

	@Test
	void _42_addDependents50() {

		List<EntityObject<?>> dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		// Nothing could be changed in State 20
		assert dependents.size() == 3
		assert dependents*.getAttribute('name').sort() == ['Position 007', 'Position 1', 'Position 2']
		assert dependents*.getAttribute('nummer').sort() == [null, null, null] //No permission to read

		2.times { i ->
			dependents << new EntityObject(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION).with {
				it.setAttribute('name', 'PositionX ' + i)
				it.setAttribute('bemerkung', 'BemerkungX ' + i)
				it.setAttribute('nummer', i)
				it.setAttribute('neu', true)
				return it
			}
		}

		// Note, as this is now allowed there will be no automatic state transition to 60
		auftrag.save()


		List<EntityObject<Long>> savedDependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		// There was no right to create dependents, thus nothing has been changed
		assert savedDependents.size() == 3
		assert savedDependents*.getAttribute('name').sort() == ['Position 007', 'Position 1', 'Position 2']
		assert savedDependents*.getAttribute('nummer').sort() == [null, null, null] //No permission to read
	}

	@Test
	void _44_updateAnDeleteDependents50() {
		List<EntityObject<?>> dependents = auftrag.getDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		EntityObject<?> dep1 = dependents.find {
			it.getAttribute('name') == 'Position 007'
		}

		assert dep1
		assert dep1.getAttribute('name') == 'Position 007'
		assert dep1.getAttribute('bemerkung') == 'Bemerkung 0'
		assert !dep1.getAttribute('nummer') // No rights to read this
		assert dep1.getAttribute('neu') == false

		dep1.setAttribute('name', 'Position XYZ')
		dep1.setAttribute('nummer', '23')
		dep1.setAttribute('neu', true)

		EntityObject<?> dep2 = dependents.find {
			it.getAttribute('name') == 'Position 2'
		}

		assert dep2
		dep2.flagDeleted()

		//After this save the state will be 60 (automatic state transition)
		auftrag.save()

	}

	@Test
	void _46_checkDependents60() {
		List<EntityObject<Long>> savedDependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert savedDependents.size() == 2
		assert savedDependents*.getAttribute('nummer').sort() == [1, 7]

		EntityObject<?> dep1 = savedDependents.find {
			it.getAttribute('nummer') == 7
		}

		assert dep1
		assert dep1.getAttribute('name') == 'Position 007'
		assert dep1.getAttribute('bemerkung') == 'Bemerkung 0'
		assert dep1.getAttribute('nummer') == 7
		assert dep1.getAttribute('neu') == true // This time there was the right to change

	}

	@Test
	void _48_testStatusUebergenge() {
		auftrag.changeState(50);

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Änderung' // Didn't change, because there is no transition

		auftrag.changeState(75);

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Änderung' // Didn't change, because no permission

		auftrag.changeState(90);

		auftrag = client.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)

		assert auftrag.getAttribute('nuclosState')['name'] == 'Abbruch' // This should work

		List<EntityObject<?>> dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents == null // No rights in state 90 for the dependents at all

	}

	static RESTClient clientctrl

	@Test
	void _50_createControllingUserAndLogin() {
		RESTHelper.createUser('testct', 'testct', ['Example controlling', 'Example readonly'], nuclosSession)
		clientctrl = new RESTClient('testct', 'testct')
		clientctrl.login()
		assert clientctrl.sessionId
	}

	@Test
	void _55_testPermissionsState90() {
		auftrag = clientctrl.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag1Id)
		assert auftrag && auftrag.id == auftrag1Id
		assert auftrag.getAttribute('nuclosState')['name'] == 'Abbruch'
		assert !auftrag.getAttribute('name')

		List<EntityObject<?>> dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents
		assert dependents.size() == 2

		EntityObject<?> dep1 = dependents.find {
			it.getAttribute('name') == 'Position 007'
		}

		assert dep1
		assert dep1.getAttribute('name') == 'Position 007'
		assert dep1.getAttribute('bemerkung') == 'Bemerkung 0'
		assert !dep1.getAttribute('nummer') // No permission
		assert dep1.getAttribute('neu') == true

		dep1.setAttribute('name', 'PosXYZ')
		dep1.setAttribute('neu', false)

		auftrag.save()

		dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents
		assert dependents.size() == 2

		dep1 = dependents.find {
			it.getAttribute('name') == 'Position 007'
		}

		assert dep1
		assert dep1.getAttribute('name') == 'Position 007' // No change, because no permission
		assert dep1.getAttribute('neu') == false // This has been changed

		// Try to save
		dep1.flagDeleted()
		auftrag.save()

		dependents = auftrag.loadDependents(TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION,
				'auftrag'
		)

		assert dependents
		assert dependents.size() == 2
		assert dependents*.getAttribute('name').sort() == ['Position 007', 'Position 1']

	}

	@Test
	void _58_readEoWithDelete() {
		List<EntityObject<Long>> lst = clientctrl.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst
		assert lst.size() == 2

		auftrag = clientctrl.getEntityObject(TestEntities.EXAMPLE_REST_AUFTRAG, auftrag2Id)

		assert auftrag && auftrag.id == auftrag2Id
		assert !auftrag.getAttribute('name') // No permissions

		auftrag.delete()

		//Should be able to delete
		lst = clientctrl.getEntityObjects(TestEntities.EXAMPLE_REST_AUFTRAG, null)
		assert lst
		assert lst.size() == 1
		assert lst.get(0).id == auftrag1Id
	}
}