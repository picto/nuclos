package org.nuclos.test.webclient.search

import java.text.SimpleDateFormat

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SearchTest extends AbstractWebclient2Test {

	static List<NuclosWebElement> listEntries() {
		$$('.sideview-list-entry')
	}

	static Integer statusbarCount() {
		def countString = $('#sideview-statusbar-count').text
		countString != null && countString.length() > 0 ? Integer.parseInt(countString) : null
	}

	static String customerFQN = TestEntities.EXAMPLE_REST_CUSTOMER.fqn
	static String orderFQN = TestEntities.EXAMPLE_REST_ORDER.fqn

	static final Map customer100 =
			[
				boMetaId  : customerFQN,
				attributes: [customerNumber: 100, name: 'Customer 100', discount: 50, birthday: new SimpleDateFormat('yyyy-MM-dd').parse('1999-01-01')]
			]

	static final Map  customer101 =
			[
					boMetaId  : customerFQN,
					attributes: [customerNumber: 101, name: 'Customer 101', discount: 70, birthday: new SimpleDateFormat('yyyy-MM-dd').parse('2000-01-01')]
			]

	static final Map  customer102 =
			[
					boMetaId  : customerFQN,
					attributes: [customerNumber: 102, name: 'Customer 102 ÄÖÜ', discount: 30, birthday: new SimpleDateFormat('yyyy-MM-dd').parse('2000-01-01')]
			]


	@Test
	void _01setup() {
		assert $('#logout')

		RESTHelper.createBo(customer100, nuclosSession)
		RESTHelper.createBo(customer101, nuclosSession)
	}


		@Test
	void _02findCustomerViaUrl() {
		def equalSignEncoded = '%3D'
		getUrlHash("/view/$TestEntities.EXAMPLE_REST_CUSTOMER.fqn/search/customerNumber${equalSignEncoded}100")

		assert EntityObjectComponent.forDetail().getAttribute('customerNumber') == '100'

		/*
		// name like %10
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10")
		assert Sidebar.listEntryCount == 0

		// name like %10%
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10*")
		assert Sidebar.listEntryCount == 2

		// name like %10_
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*10%3F")
		assert Sidebar.listEntryCount == 2

		// name like %1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*1")
		assert Sidebar.listEntryCount == 1

		// name like ___________1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}%3F%3F%3F%3F%3F%3F%3F%3F%3F%3F%3F1")
		assert Sidebar.listEntryCount == 1

		// name like %_1_1
		getUrlHash("#/view/example_rest_Customer/search/name${equalSignEncoded}*%3F1%3F1")
		assert Sidebar.listEntryCount == 1
		*/
	}



	@Test
	void _03findCustomerViaStandardSearch() {

		refresh()
		assert $$('.sideview-list-entry').size() == 2

		Searchtemplate.search('Customer 101')
		screenshot('searched-customer101')

		assert $$('.sideview-list-entry').size() == 1
		assert listEntries().get(0).text.contains('Customer 101')
	}



	@Test
	void _04findCustomerViaSearchTemplate() {

		getUrlHash('/view/' + TestEntities.EXAMPLE_REST_CUSTOMER.fqn)

		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name: 'discount', operator: '>', value: '60'))
		screenshot('searchtemplate-searched-customer-gt-60')
		assert $$('.sideview-list-entry').size() == 1
		assert listEntries().get(0).text.contains('Customer 101')
	}

	@Test
	void _05findCustomerViaSearchTemplate() {

		Searchtemplate.removeAllSearchTemplateItems()
		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name:'discount', operator: '<', value: '60'))
		screenshot('searchtemplate-searched-customer-lt-60')
		assert listEntries().size() == 1
		assert statusbarCount() == 1
		assert listEntries().get(0).text.contains('Customer 100')
	}

	@Test
	void _06findCustomerViaSearchTemplate() {

	/*
		TODO problem with selenium sendkeys in datepicker
		Searchtemplate.removeAllSearchTemplateItems()
		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name:'birthday', operator: '<', value:
				// TODO formatValue(new SimpleDateFormat('yyyy-MM-dd').parse('1999-05-01'))
				'1999-05-01'
		))
		screenshot('searchtemplate-searched-customer-lt-1999-05')
		assert $$('.sideview-list-entry').size() == 1
		assert listEntries().get(0).text.contains('Customer 100')

		Searchtemplate.removeAllSearchTemplateItems()
		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name:'birthday', operator: '>', value:
				// TODO formatValue(new SimpleDateFormat('yyyy-MM-dd').parse('1999-05-01'))
				'1999-05-01'
		))
		screenshot('searchtemplate-searched-customer-gt-1999-05')
		assert $$('.sideview-list-entry').size() == 1
		assert listEntries().get(0).text.contains('Customer 101')


		$('.enable-search').click()
		waitForAngularRequestsToFinish()
		assert $$('.sideview-list-entry').size() == 2
		*/

		Searchtemplate.removeAllSearchTemplateItems()
		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name:'name', operator: '=', value: '-'))
		Searchtemplate.addSearchTemplateItem(customerFQN, new Searchtemplate.SearchTemplateItem(name:'active', value: 'true'))
		assert $$('.attribute-button.activated').findAll{ it.displayed }.size() == 2
	}

	@Test
	void _07findCustomerViaTextSearch() {
		Searchtemplate.removeAllSearchTemplateItems()
		assert listEntries().size() == 2
		assert statusbarCount() == 2

		Searchtemplate.search('Customer 101')
		assert listEntries().size() == 1
		assert statusbarCount() == 1
	}

	@Test
	void _08findCustomerWithUmlautViaTextSearch() {

		RESTHelper.createBo(customer102, nuclosSession)

		Searchtemplate.removeAllSearchTemplateItems()
		assert listEntries().size() == 3
		assert statusbarCount() == 3
		Searchtemplate.search('ÄÖÜ')
		assert listEntries().size() == 1
		assert statusbarCount() == 1
	}

}















//
//
//@Category(IntegrationTest.class)
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
//class SearchTest{
//	static def customerFQN = 'example_rest_Customer'
//	static def orderFQN = 'example_rest_Order'
//	static def articleFQN = 'example_rest_Article'
//	static def customer100 = [customerNumber: 100, name: 'Customer 100', discount: 50, birthday: new SimpleDateFormat('yyyy-MM-dd').parse('1999-01-01')]
//	static def customer101 = [customerNumber: 101, name: 'Customer 101', discount: 70, birthday: new SimpleDateFormat('yyyy-MM-dd').parse('2000-01-01')]
//
//	@Test
//	void _01setup() {
//		assert $('#logout')
//		Sideview.createBo(customerFQN, customer100);
//		Sideview.createBo(customerFQN, customer101);
//	}
//


//
//	@Test
//	void _05findReferenceViaSearchTemplate() {
//
//		TestDataHelper.insertTestData() // TODO use same data for tests above
//
//		Sideview.openSideview(articleFQN)
//
//		Searchtemplate.removeAllSearchTemplateItems()
//
//		assert Sideview.resultListSize() == 4
//
//		Searchtemplate.addSearchTemplateItem(articleFQN, new Searchtemplate.SearchTemplateItem(name:'category', operator: '=', value: 'Hardware'))
//
//		screenshot('searchtemplate-find-reference')
//
//		assert Sideview.resultListSize() == 2
//
//	}
//
//
//	@Test
//	void _06findReferenceWithVLPViaSearchTemplate() {
//
//		Sideview.openSideview(orderFQN)
//
//		Searchtemplate.removeAllSearchTemplateItems()
//
//		assert Sideview.resultListSize() == 6
//
//		waitForAngularRequestsToFinish()
//		sleep(1000)
//
//		// "Test Customer inactive" is only availabe in LOV for searching not when editing - see 'searchmode' parameter in VLP
//		Searchtemplate.addSearchTemplateItem(orderFQN, new Searchtemplate.SearchTemplateItem(name:'customer', operator: '=', value: 'Test-Customer inactive'))
//
//		screenshot('searchtemplate-find-vlp-reference1')
//
//		assert Sideview.resultListSize() == 0
//
//		waitForAngularRequestsToFinish()
//		sleep(1000)
//
//		Searchtemplate.addSearchTemplateItem(orderFQN, new Searchtemplate.SearchTemplateItem(name:'customer', operator: '=', value: 'Test-Customer'))
//
//		screenshot('searchtemplate-find-vlp-reference2')
//
//		assert Sideview.resultListSize() == 6
//
//	}
//
//	@Test
//	void _07newSearchtemplate() {
//
//		Sideview.openSideview(orderFQN)
//
//		Searchtemplate.removeAllSearchTemplateItems()
//
//		Searchtemplate.addSearchTemplate('Ordernumber > 10020150')
//
//		assert Sideview.resultListSize() == 6
//
//		waitForAngularRequestsToFinish()
//		sleep(1000)
//
//		Searchtemplate.addSearchTemplateItem(orderFQN,
//				new Searchtemplate.SearchTemplateItem(
//						name:'orderNumber',
//						operator: '>',
//						value: '10020150'
//				)
//		)
//
//		screenshot('searchtemplate-new')
//
//		assert Sideview.resultListSize() == 2
//
//		Searchtemplate.addSearchTemplate('Note  = B')
//		Searchtemplate.addSearchTemplateItem(
//				orderFQN,
//				new Searchtemplate.SearchTemplateItem(
//						name:'note',
//						operator: '=',
//						value: 'B'
//				)
//		)
//		assert Sideview.resultListSize() == 4
//
//
//
//		// switch back to previous created search template
//		Searchtemplate.selectSearchTemplate('Ordernumber > 10020150')
//		assert Sideview.resultListSize() == 2
//
//
//		// check if searchtemplate selection is persistent
//		Sideview.refresh()
//		assert Sideview.resultListSize() == 2
//	}
//
//
//	@Test
//	void _08deleteSearchtemplate() {
//		Searchtemplate.deleteSearchTemplate('Note = B')
//		Searchtemplate.deleteSearchTemplate('Ordernumber > 10020150')
//		assert Sideview.resultListSize() == 6
//	}
//
//}
