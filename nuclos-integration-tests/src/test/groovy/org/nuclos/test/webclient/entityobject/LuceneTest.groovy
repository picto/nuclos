package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.Dialog
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.FileComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.StateComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LuceneTest extends AbstractWebclient2Test {
	static String catName = 'Essen'
	static String catReName = 'Lebensmittel'
	static String cat2Name = 'Werkzeug'

	String categoryBoMetaId = TestEntities.EXAMPLE_REST_CATEGORY.fqn
	String wordBoMetaId = TestEntities.EXAMPLE_REST_WORD.fqn

	static Map cat1 = [name: catName]
	static Map cat2 = [name: cat2Name]

	static Map word1 = [text: 'Fleisch', times: '1', agent: 'test', kategorie: cat1]
	static Map word2 = [text: 'Kartoffel', times: '3', agent: 'test', kategorie: cat1]
	static Map word3 = [text: catName, times: '7', agent: 'test', kategorie: cat1]

	static Map word4 = [text: 'Hammer', times: '4', agent: 'nuclos', kategorie: cat2]
	static Map word5 = [text: 'Meisel', times: '10', agent: 'nuclos', kategorie: cat2]

	static Map word6 = [text: 'Zange', times: '-1', agent: 'test', kategorie: cat2Name]

	static Map auftrag = [name: '1']

	@Test
	void _0setup() {
		nuclosSession.managementConsole('enableIndexer')
	}

	@Test
	void _2createCategories() {
		cat1['id'] = RESTHelper.createBo([
			boMetaId  : categoryBoMetaId,
			attributes: cat1
		], nuclosSession).boId
		cat2['id'] = RESTHelper.createBo([
			boMetaId  : categoryBoMetaId,
			attributes: cat2
		], nuclosSession).boId
	}

	@Test
	void _3createNewEntries() {
		auftrag['id'] = RESTHelper.createBo([
	        boMetaId: TestEntities.EXAMPLE_REST_AUFTRAG.fqn,
			attributes: auftrag
		], nuclosSession).boId

		RESTHelper.createBo([
			boMetaId  : wordBoMetaId,
			attributes: word1
		], nuclosSession)

		RESTHelper.createBo([
			boMetaId  : wordBoMetaId,
			attributes: word2
		], nuclosSession)

		RESTHelper.createBo([
			boMetaId  : wordBoMetaId,
			attributes: word3
		], nuclosSession)

	}

	@Test
	void _4luceneTest() {
		screenshot('beforeLuceneTest')
		luceneTest(catName, 4) //Note: The three words + the category itself.
	}

	@Test
	void _5renameCategory() {

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CATEGORY)

		Searchtemplate.search(catName)

		Sidebar.selectEntry(0)

		screenshot('afterCategorySearch')

		def foundEntries = $$('.sideview-list-entry')
		assert foundEntries.size() == 1

		$('.sideview-list-entry').click()

		waitForAngularRequestsToFinish()

		def inputs = $$('nuc-layout input[type="text"]')

		inputs*.clear()
		waitForAngularRequestsToFinish()
		inputs*.sendKeys(catReName)

		screenshot('afterChangeName')

		eo.save()

		screenshot('afterSaveEntry')
	}

	@Test
	void _6luceneTestReference() {

		screenshot('beforeLuceneTestReference')

		luceneTest(catName, 1) //Note: catName is still the originalName of word3

		luceneTest(catReName, 4) //Note: The three words + the category itself.

	}

	@Test
	void _7luceneTestRecordGrant() {
		assert logout();
		assert login('nuclos', '')

		RESTHelper.createBo([
				boMetaId  : wordBoMetaId,
				attributes: word4
		], nuclosSession)
		
		RESTHelper.createBo([
				boMetaId  : wordBoMetaId,
				attributes: word5
		], nuclosSession)
		
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORD)

		screenshot('after5WordInsert')

		assert Sidebar.listEntryCount == 5

		// > Funktioniert leider nicht mehr! Firefox schließt das Popup direkt nach der Eingabe von 'Werkzeug' aus unbekannten Gründen.
		// > Andere Tests haben damit keine Probleme, woran könnte es also liegen?
		// > Die Menge der Einträge stimmt, wenn man es manuell testet. Selenium Problem?
		// Das Problem tritt auf dem Jenkins auf, lokal nicht
		// luceneTest(cat2Name, 3)

		assert logout()
		assert login('test', 'test')

		//word4 and word5 must not appear for user test, but still the category for which there are not record grants
		luceneTest(cat2Name, 1)

	}

	@Test
	void _8luceneTestRollback() {

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word6)

		screenshot('errorFinalRule')

		assumeError("Times muss größer oder gleich 1 sein!")

		luceneTest(word6.text as String, 0) //A error was thrown so it must not be indexed (rollbacked)


		def inputs = $$('nuc-layout input[type="text"]')

		assert inputs.size() >= 2

		inputs[1].clear()
		inputs[1].sendKeys("37") //Correct input

		screenshot('afterInput37')

		eo.save()

		screenshot('afterSave')

		luceneTest(word6.text as String, 1) //Now it must be indexed

	}

	@Test
	void _9luceneTestDocument() {
		luceneTest('hello', 0) // No hello until now

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_AUFTRAG)

		File nuclosFile = new File(FileComponentTest.getResource('nuclos.docx').toURI())

		FileComponent fileComponent = eo.getFileComponent('dokument')
		fileComponent.setFile(nuclosFile)

		screenshot('afterSettingDocument')

		eo.save()

		screenshot('afterSave')

		luceneTest('hello', 1) // Expect to find hello within the file

		StateComponent.changeStateByNumeral(20)

		screenshot ('after ChangeState')

		luceneTest('world', 1) // Still one time "hello world"

		eo = EntityObjectComponent.forDetail()

		eo.getTab('Dokumente').click()

		Subform subform = eo.getSubform('org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject');

		subform.newRow()

		screenshot('after-new-row')

		Subform.Row row = subform.getRow(0)

		fileComponent = row.getFileComponent('documentfile')
		fileComponent.setFile(nuclosFile)

		eo.save()

		screenshot('after saving document into subform')

		assert subform.getRowCount() == 1 // This is also a test for NUCLOS-6265

		row = subform.getRow(0)
		Object value = row.getValue('documentfile')

		assert value == nuclosFile.getName()

		// FIXME NUCLOS-6260 (2)
		// luceneTest('world', 2) // Second one appeared

	}


	static List<WebElement> luceneSearch(String s) {

		WebElement fullTextSearch = $('#full-text-search')
		assert fullTextSearch != null

		WebElement input = fullTextSearch.findElement(By.className('full-text-search-input'))
		assert input != null

		input.clear()
		input.sendKeys(s)

		screenshot('afterLuceneSearch')

		return fullTextSearch.findElements(By.className('full-text-search-result-item'))
	}

	static void luceneTest(String s, int expected) {
		waitForAngularRequestsToFinish()

		try {
			waitFor(10) {
				luceneSearch(s).size() == expected
			}
		} catch (Exception e) {
			def size = luceneSearch(s).size()
			screenshot('beforeExceptionInLuceneTest')
			assert size == expected
		}

		screenshot('afterLuceneSearchAndWait')
	}

	static void assumeError(String s) {
		screenshot("errorassumed")

		String error = getErrorMessage()

		assert !error.empty
		assert error.indexOf(s) != -1

		Dialog.close()
	}

	static String getErrorMessage() {
		return $('.modal-body')?.text
	}
}

