package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class AutonumberTest extends AbstractWebclient2Test {

	@Test
	void _05_autonumberInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBER)
		eo.addNew()
		eo.setAttribute('name', 'Autonumber test')

		Subform subform = eo.getSubform(TestEntities.NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM.fqn + '_parent')

		createEntries:
		{
			5.times {
				Subform.Row row = subform.newRow()
				assert row.getValue('autonumber', Integer.class) == it + 1
			}

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
		}

		deleteNewEntry:
		{
			subform.getRow(2).setSelected(true)
			subform.deleteSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
		}

		save:
		{
			eo.save()
			assert subform.getColumnValues('autonumber', Integer.class) == [4, 3, 2, 1]
		}

		deleteSavedEntry:
		{
			subform.getRow(0).setSelected(true)
			subform.getRow(2).setSelected(true)
			subform.deleteSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [2, 1, null, null]

			eo.save()
			assert subform.getColumnValues('autonumber', Integer.class) == [2, 1]
		}

		addAndClone:
		{
			subform.newRow()

			assert subform.getColumnValues('autonumber', Integer.class) == [3, 2, 1]
			assert subform.getRow(0).dirty
			assert !subform.getRow(1).dirty
			assert !subform.getRow(2).dirty

			subform.getRow(1).setSelected(true)
			subform.getRow(2).setSelected(true)

			subform.cloneSelectedRows()

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getRow(0).dirty
			assert subform.getRow(1).dirty
			assert subform.getRow(2).dirty
			assert !subform.getRow(3).dirty
			assert !subform.getRow(4).dirty

			eo.save()
		}

		editManually:
		{
			subform.getRow(2).enterValue('autonumber', '8')

			assert subform.getColumnValues('autonumber', Integer.class) == [5, 4, 3, 2, 1]
			assert subform.getRow(0).dirty
			assert subform.getRow(1).dirty
			assert subform.getRow(2).dirty
			assert !subform.getRow(3).dirty
			assert !subform.getRow(4).dirty
		}
	}
}
