package org.nuclos.test.webclient

import javax.mail.MessagingException
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

import org.apache.commons.mail.util.MimeMessageParser
import org.jsoup.Jsoup
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.common.PortAllocator
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.pageobjects.ActivationComponent
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.RegistrationComponent

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RegistrationTest extends AbstractWebclient2Test {
	static ServerSetup serverSetup
	static GreenMail greenMail
	static GreenMailUser mailUser

	static Map<String, String> oldSystemParameters

	static String registrationEmail = 'testregistration@nuclos.de'

	static String activationUrl

	@BeforeClass
	static void setup() throws IOException, URISyntaxException, MessagingException {
		AbstractWebclient2Test.setup()

		oldSystemParameters = nuclosSession.simpleSystemParameters

		setupMailServer:
		{
			// Copy the default GreenMail setups and make some adjustments
			final int port = PortAllocator.allocate()
			serverSetup = new ServerSetup(port, ServerSetup.SMTP.bindAddress, ServerSetup.SMTP.protocol)
			serverSetup.serverStartupTimeout = 10000
			greenMail = new GreenMail(serverSetup)

			//Start all email servers using non-default ports.
			greenMail.start()
			mailUser = greenMail.setUser("test-user@nuclos.de", "test", "test")
		}

		nuclosSession.deleteUserByEmail(registrationEmail)
		RESTHelper.createUser('anonymous', 'anonymous', ['Example user'], nuclosSession)

		Map<String, String> params = [
				'SMTP Server'                   : greenMail.smtp.bindTo,
				'SMTP Port'                     : greenMail.smtp.port as String,
				'SMTP Username'                 : mailUser.email,
				'SMTP Password'                 : mailUser.password,
				'SMTP Authentication'           : 'Y',
				'SMTP Sender'                   : 'registration@nuclos.de',

				// Anonymous login and self registration
				'ANONYMOUS_USER_ACCESS_ENABLED' : 'Y',
				'ROLE_FOR_SELF_REGISTERED_USERS': 'Example user',
				'ACTIVATION_EMAIL_SUBJECT'      : 'Nuclos account registration',
				'ACTIVATION_EMAIL_MESSAGE'      : activationMessage,
				'EMAIL_SIGNATURE'               : '<b>TODO: signature</b>',
				'INITIAL_ENTRY'                 : 'anonymous:' + TestEntities.EXAMPLE_REST_ORDER.fqn
		]

		nuclosSession.patchSystemParameters(params)
	}

	static String getActivationMessage() {
		"""Hello {0} {1},
<br/><br/>
to activate your account <a href="$nuclosWebclient2BaseURL#/account/activate/{2}/{3}">click here</a>.
<br/><br/>
		Bye
"""
	}

	@AfterClass
	static void teardown() {
		greenMail.stop()

		nuclosSession.setSystemParameters(oldSystemParameters)

		AbstractWebclient2Test.teardown()
	}

	@Test
	void _00_testAnonymousLogin() {
		logout()
		refresh()

		// Should be logged in as anonymous and redirected to the "initial entry" Order
		assert currentUrl.contains('example_rest_Order')
	}

	@Test
	void _05_register() {
		getUrlHash('/login')

		AuthenticationComponent.clickSelfRegistration()

		Closure submitAndAssertError = {
			RegistrationComponent.submit()
			assert RegistrationComponent.errorMessage
		}

		RegistrationComponent.username = 'testregistration'
		submitAndAssertError()

		RegistrationComponent.password = 'test'
		submitAndAssertError()

		RegistrationComponent.firstname = 'Firstname'
		submitAndAssertError()

		RegistrationComponent.lastname = 'Lastname'
		submitAndAssertError()

		RegistrationComponent.email = registrationEmail
		submitAndAssertError()

		RegistrationComponent.email2 = registrationEmail
		submitAndAssertError()

		RegistrationComponent.togglePrivacyConsent()

		RegistrationComponent.submit()
		assert RegistrationComponent.successMessage
	}

	@Test
	void _10_loginBeforeActivation() {
		getUrlHash('/login')

		loginUnsafe('testregistration', 'test')

		// TODO: Check message content
		assert AuthenticationComponent.errorMessage
	}

	@Test
	void _15_activationEmail() {
		greenMail.waitForIncomingEmail(1)
		MimeMessage activationMail = greenMail.getReceivedMessages().first()

		assert activationMail.from.first() == new InternetAddress('registration@nuclos.de')

		MimeMessageParser parser = new MimeMessageParser(activationMail)
		parser.parse()
		String htmlContent = parser.getHtmlContent()
		String plainContent = parser.getPlainContent()

		assert !plainContent
		assert htmlContent

		activationUrl = Jsoup.parse(htmlContent).select('a[href]').first().attr('href')
		assert activationUrl
	}

	@Test
	void _20_activateViaLink() {
		getUrl(activationUrl + '_wrong')
		assert ActivationComponent.errorMessage

		getUrl(activationUrl)
		assert ActivationComponent.successMessage
	}

	@Test
	void _25_loginAfterActivation() {
		getUrlHash('/login')
		assert login('testregistration', 'test')
	}
}
