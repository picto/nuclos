package org.nuclos.test.webclient.entityobject

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.DetailButtonsComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DisclaimerTest extends AbstractWebclient2Test {

	@BeforeClass
	static void setup() {
		setup(true, false)
	}

	@Test()
	void _05_testCookieWarning() {
		WebElement we = $('#showprivacycontent')
		assert we != null

		we.click()

		assertMessageModelAndConfirm('Datenschutz', null)

		we = $('#acceptcookies')
		assert we != null

		we.click()

		assert $('#showprivacycontent') == null
		assert $('#acceptcookies') == null

		assert logout()
		EntityObjectComponent.refresh()

		assert $('#showprivacycontent') == null

	}

	@Test()
	void _10_testLegalDisclaimer() {

		NuclosWebElement we = $('#legal-disclaimers')

		assert we != null
		we.click()

		NuclosWebElement di = $(we, '.dropdown-item')

		assert di != null
		String text = di.getText()

		assert text.trim() == 'Datenschutz'
		di.click()

		assertMessageModelAndConfirm('Datenschutz', null)

		assert login('test', 'test')

		assert $('#legal-disclaimers') != null
		assert $('#showprivacycontent') == null
	}

	@Test()
	void _15_testDisclaimerRoute() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.addNew()
		assert eo.dirty

		getUrlHash('/disclaimer/Datenschutz')

		assertMessageModelAndConfirm('Datenschutz', null)

		assert currentUrl.contains('/view/' + TestEntities.EXAMPLE_REST_ORDER)
		assert !DetailButtonsComponent.popoverTitle
		assert !DetailButtonsComponent.popoverText

		eo.cancel()
	}

	@Test()
	void _20_testDisclaimerRouteViaHyperlinkButton() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()
		eo.setAttribute('text', 'Disclaimer Button Test')
		eo.setAttribute('hyperlink', '#/disclaimer/Datenschutz')
		eo.save()

		eo.clickButton('Hyperlink button')

		assertMessageModelAndConfirm('Datenschutz', null)

		assert currentUrl.contains('/view/nuclet_test_other_TestLayoutComponents')
		assert !DetailButtonsComponent.popoverTitle
		assert !DetailButtonsComponent.popoverText
	}
}
