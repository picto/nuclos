package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.GenerationComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class GenerationTest extends AbstractWebclient2Test {

	Map<String, ?> eoData = [
			name              : 'test',
			exceptionbeiinsert: false
	]
	Map<String, ?> eoData2 = [
			name              : 'test2',
			exceptionbeiinsert: false
	]

	@Test
	void _00_createEO() {
		EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData)
	}

	@Test()
	void _05_generateEO() {
		screenshot('before-calling-generator')
		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')
		screenshot('after-calling-generator')

		assert generatorCount == 2

		GenerationComponent.openResult()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR)
		eo.refresh()
		assert !eo.dirty
		assert Sidebar.listEntryCount == 2

		screenshot('generate-1-window-2')

		driver.close()
		driver.switchTo().window(oldWindow)

		screenshot('generate-1-window-1')
	}

	@Test()
	void _10_generateEOWithException() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('with-exception-before-update')
		eo.setAttribute('exceptionbeiinsert', true)
		eo.save()

		screenshot('with-exception-before-calling-generator-2')
		int generatorCount = GenerationComponent.generateObjectAndConfirm('Test Objektgenerator')

		assert generatorCount == 2

		GenerationComponent.openResult()
		screenshot('with-exception-after-calling-generator-2')

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert eo.dirty
		assert eo.alertText == 'Test Exception'

		eo.clickButtonOk()

		eo.setAttribute('exceptionbeiinsert', false)
		eo.save()

		eo.refresh()

		assert Sidebar.listEntryCount == 3
		assert !eo.dirty

		screenshot('with-exception-generate-1-window-2-after-save')

		driver.close()
		driver.switchTo().window(oldWindow)

		screenshot('with-exception-generate-2-window-1')
	}

	@Test()
	void _15_generateEOViaButton() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR, eoData2)

		eo.checkButton('Intern generieren', true, true)
		eo.clickButton('Objekt generieren')

		GenerationComponent.openResult()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		assert !eo.dirty

		driver.close()
		driver.switchTo().window(oldWindow)
	}

	@Test()
	void _20_generateDependent() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform('nuclet_test_other_TestObjektgeneratorDependent_testobjektgen')
		assert subform.rowCount == 0

		eo.clickButton('Dependent generieren')

		assert subform.rowCount == 1
	}
}
