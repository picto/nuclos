package org.nuclos.test.webclient.preference

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTClient
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MenuComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.preference.PreferenceItem
import org.nuclos.test.webclient.pageobjects.preference.PreferenceType
import org.nuclos.test.webclient.pageobjects.preference.Preferences
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration

import groovy.transform.CompileStatic

/**
 * create, share, customize webclient preferences
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class PreferencesTest extends AbstractWebclient2Test {

	String[] prefColumns1 = ['example_rest_Order_orderNumber', 'example_rest_Order_customer', 'example_rest_Order_note']
	String[] prefColumns2 = ['example_rest_Order_note']

	Map order =
			[
					boMetaId  : 'example_rest_Order',
					attributes: [
							'orderNumber': '555'
					]
			]

	@Test
	void _00createTestUser() {
		RESTHelper.createUser('preferencetest', 'preferencetest', ['Example user'], nuclosSession)
	}

	@Test
	void _01createTestData() {
		RESTClient client = new RESTClient('preferencetest', 'preferencetest')
		RESTHelper.createBo(order, client)
	}


	@Test
	void _02createPref() {
		logout()
		login('preferencetest', 'preferencetest', false)
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		SideviewConfiguration.newSideviewConfiguration('Sideviewconfig1')
		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.saveSideviewConfiguration()
		SideviewConfiguration.close()
	}

	@Test
	void _03sharePref() {

		assert SideviewConfiguration.getSelectedColumnNames().equals(prefColumns1)

		Preferences.open()

		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		assert !pref.shared
		pref = getFirstPreferenceItem()
		Preferences.shareItem(PreferenceType.TABLE, pref.name, 'Example user')
		assert getFirstPreferenceItem().shared

		assertSideviewConfigurationWithOtherUser(prefColumns1)

	}

	private void assertSideviewConfigurationWithOtherUser(String[] selectedColumns) {
		logout()
		login('test', 'test', false) // user of "Example user" group

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.selectSideviewConfiguration('Sideviewconfig1')

		assert SideviewConfiguration.getSelectedColumnNames().equals(selectedColumns)

		logout()
		login('preferencetest', 'preferencetest', false)
	}

	@Test
	void _04customizePref() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal('Sideviewconfig1')
		SideviewConfiguration.getSelectedColumnNames().each {
			SideviewConfiguration.deselectColumn(it)
		}
		prefColumns2.each {
			SideviewConfiguration.selectColumn(it.toString())
		}

		SideviewConfiguration.close()


	}

	@Test
	void _05publishChanges() {
		Preferences.open()
		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()

		assert pref.customized
		pref.publishChanges()

		assert !getFirstPreferenceItem().customized

		assertSideviewConfigurationWithOtherUser(prefColumns2)
	}

	@Test
	void _06discardCustomizedPrefInSideview() {

		// customize again
		customizePref()

		assert Sidebar.selectedColumns().findAll() { !it.equals('status') }.sort().equals(prefColumns1.sort())

		// discard changes
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal('Sideviewconfig1')
		SideviewConfiguration.discardChanges()
		assert !$$('#sideviewmenu-selector')[0].getAttribute('selectedIndex').equals('-1')
		SideviewConfiguration.clickButtonOk()

		assert Sidebar.selectedColumns().findAll() { !it.equals('status') }.sort().equals(prefColumns2.sort())
	}

	@Test
	void _06discardCustomizedPrefInManagePrefView() {

		// customize again
		customizePref()

		// discard changes
		Preferences.open()
		Preferences.filter('name', 'Sideviewconfig1')
		def pref = getFirstPreferenceItem()
		pref.select()
		pref = getFirstPreferenceItem()
		assert pref.customized
		pref.discardChanges()

		assertSideviewConfigurationWithOtherUser(prefColumns2)
	}

	@Test
	void _07discardCustomizedPrefInUserMenu() {

		// customize again
		customizePref()

		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		$('#button-reset-allprefs').click()

		assertSideviewConfigurationWithOtherUser(prefColumns2)

		// customize again
		customizePref()

		// discard changes
		MenuComponent.toggleUserMenu()
		MenuComponent.clickOpenPreferencesResetModal()
		$('#button-reset-prefs-for-current-entity-class').click()

		assertSideviewConfigurationWithOtherUser(prefColumns2)
	}

	@Test
	void _08discardUnsharedPref() {
		// TODO
	}

	private void customizePref() {
		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		SideviewConfiguration.openColumnConfigurationPanel()
		SideviewConfiguration.selectSideviewConfigurationInModal('Sideviewconfig1')
		SideviewConfiguration.getSelectedColumnNames().each {
			SideviewConfiguration.deselectColumn(it)
		}
		prefColumns1.each {
			SideviewConfiguration.selectColumn(it.toString())
		}
		SideviewConfiguration.close()
	}

	private PreferenceItem getFirstPreferenceItem() {
		Preferences.getPreferenceItems()[0]
	}

}