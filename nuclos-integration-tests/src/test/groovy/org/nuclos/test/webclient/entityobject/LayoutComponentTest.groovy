package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.Datepicker
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LayoutComponentTest extends AbstractWebclient2Test {

	@Test
	void _05_comboboxOnTextfield() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)
		eo.addNew()

		assert !eo.getAttribute('text', WebTextfield)

		ListOfValues lov = eo.getLOV('text')
		assert !lov.textValue
		lov.open()
		List<String> choices = lov.choices
		assert choices.containsAll(['nuclos', 'test'])
		assert !choices.contains('')

		changeValueViaTextfield:
		{
			String nonexistentuser = 'nonexistentuser'
			lov.sendKeys(nonexistentuser)

			assert eo.getAttribute('text', WebTextfield) == nonexistentuser
			choices = lov.choices
			assert choices.containsAll(['nuclos', 'test'])
			assert lov.textValue == nonexistentuser
		}

		changeValueViaLov: {
			lov.selectEntry('test')
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}

		checkValuesAfterSave: {
			eo.save()
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}

		checkValuesAfterRest: {
			eo.setAttribute('text', 'asdf')
			eo.cancel()
			assert eo.getAttribute('text', WebTextfield) == 'test'
			assert lov.textValue == 'test'
		}
	}

	@Test
	void _10_selectDateByDatepickerDropdown() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Date date = new Date(117, 0, 1)   // 2017-01-01

		eo.setAttribute('date', date)

		Datepicker datepicker = eo.getDatepicker('date')

		datepicker.open()
		assert datepicker.getMonth() == 'Jan'
		assert datepicker.getYear() == 2017

		changeDate:
		{
			datepicker.clickDayOfCurrentMonth(31)

			Date expectedDate = new Date(117, 0, 31)    // 2017-01-31
			assert eo.getAttribute('date', null, Date.class) == expectedDate
		}
	}
}
