package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class HyperlinkTest extends AbstractWebclient2Test {

	private final String hyperlink = 'hyperlink'
	private final String disabledHyperlink = 'disabledhyperlink'

	private final String dataURL = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAABGdBTUEAALGPC/' +
			'xhBQAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9YGARc5KB0XV+IAAAAddEVYdENvbW1lbnQAQ3JlYX' +
			'RlZCB3aXRoIFRoZSBHSU1Q72QlbgAAAF1JREFUGNO9zL0NglAAxPEfdLTs4BZM4DIO4C7OwQg2JoQ9LE1exd' +
			'lYvBBeZ7jqch9//q1uH4TLzw4d6+ErXMMcXuHWxId3KOETnnXXV6MJpcq2MLaI97CER3N0vr4MkhoXe0rZig' +
			'AAAABJRU5ErkJggg=='

	@Test
	void _00_setup() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTHYPERLINK)
		eo.addNew()
		eo.save()
	}

	@Test
	void _05_testEmptyLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.getAttribute(hyperlink)
		assert !eo.getAttribute(disabledHyperlink)

		NuclosWebElement hyperlink = eo.getAttributeElement(hyperlink)
		hyperlink.doubleClick()
		assert getDriver().getWindowHandles().size() == 1, 'Clicking an empty link should not open a new window'
	}

	@Test
	void _10_testEditLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute(hyperlink, dataURL)
		eo.getAttributeElement(hyperlink).doubleClick()

		checkNewWindowAndCloseIt()
	}

	private void checkNewWindowAndCloseIt() {
		Set<String> handles = getDriver().getWindowHandles()

		assert handles.size() == 2
		switchToOtherWindow()
		assert getDriver().currentUrl == dataURL
		switchToOtherWindow()
		closeOtherWindows()
	}

	@Test
	void _15_testDisabledLink() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.save()
		assert eo.getText(disabledHyperlink).startsWith('data:')

		NuclosWebElement link = eo.getAttributeElement(disabledHyperlink)
		assert link.tagName == 'a', 'A disabled hyperlink component should just be displayed as a standard HTML link'
		link.click()

		checkNewWindowAndCloseIt()
	}

	@Test
	void _20_testLinkButton() {
		EntityObjectComponent.forDetail().clickButton('Hyperlink Button')

		checkNewWindowAndCloseIt()
	}
}
