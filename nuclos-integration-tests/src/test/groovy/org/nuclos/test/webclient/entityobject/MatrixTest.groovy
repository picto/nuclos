package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class MatrixTest extends AbstractWebclient2Test {
	static List<Coordinate> testCoordinates = []

	static {
		(1..2).each { row ->
			(1..2).each { col ->
				testCoordinates << new Coordinate(row as Integer, col as Integer)
			}
		}
	}

	// TODO: Old
	NuclosWebElement matrixCell(int row, int column) {
		$('.ag-body-container .ag-row:nth-child(' + row + ') .ag-cell:nth-child(' + column + ')')
	}

	// TODO: Old
	boolean isMatrixChecked(int row, int column) {
		WebElement cell = matrixCell(row, column);
		return $(cell, '.fa-check-square-o') != null;
	}

	@Test
	void _00_setupMatrix() {
		// create new "Matrix" entry
		def matrix = RESTHelper.createBo(
				[
						boMetaId  : 'nuclet_test_matrix_Matrix',
						attributes: [name: 'Test-Matrix']
				],
				nuclosSession
		)

		// create x-axis entries
		for (int i = 1; i < 4; i++) {
			RESTHelper.createBo(
					[
							boMetaId  : 'nuclet_test_matrix_XAchse',
							attributes: [
									name: 'x-axis-value-' + i,
							]
					],
					nuclosSession
			)
		}

		// create new y-axis entries and asign to matrix
		for (int i = 0; i < 30; i++) {
			RESTHelper.createBo(
					[
							boMetaId  : 'nuclet_test_matrix_YAchse',
							attributes: [
									name  : 'y-axis-value-' + i,
									matrix:
											[id: matrix.boId, name: '']
							]
					],
					nuclosSession
			)

		}
	}

	@Test
	void _05_selectCheckicons() {
		logout()
		login('nuclos', '')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_MATRIX_MATRIX)

		assert !eo.dirty

		Sidebar.selectEntry(0)

		assert !isMatrixChecked(1, 1)

		// click all matrix cells
		testCoordinates.each {
			matrixCell(it.row, it.col).click()
			assert isMatrixChecked(it.row, it.col)
		}
		assert eo.dirty
		screenshot('matrix-cells-clicked')

		eo.save()
		assert !eo.dirty

		// Still all should be checked
		testCoordinates.each {
			assert isMatrixChecked(it.row, it.col)
		}

		// Uncheck all
		testCoordinates.each {
			matrixCell(it.row, it.col).click()
			assert !isMatrixChecked(it.row, it.col)
		}
		assert eo.dirty
		screenshot('matrix-cells-clicked-again')

		eo.save()
		assert !eo.dirty

		// Still all should be unchecked
		testCoordinates.each {
			assert !isMatrixChecked(it.row, it.col)
		}
	}

	@Test
	void _10_changeMatrixTextValues() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		screenshot('select-process-text')
		eo.selectProcess('Matrix-Text-Aktion')
		screenshot('process-text-selected')

		testCoordinates.each {
			matrixCell(it.row, it.col).click()
			NuclosWebElement input = matrixCell(it.row, it.col).$('input')

			assert input != null
			input.sendKeys("${it.row}, ${it.col}")
		}

		eo.save()
		assert !eo.dirty

		//TODO: Test saved data

		screenshot('process-text-input')
	}

	@Test
	void _11_changeMatrixTextValues_NUCLOS_6199() {
		// TEST NUCLOS-6199
		EntityObjectComponent.refresh()
		NuclosWebElement cell = matrixCell(3, 1)
		cell.click()
		NuclosWebElement input = cell.$('input')

		assert input != null
		input.sendKeys('3, 1')
		matrixCell(3, 2).click()
		assert matrixCell(3, 1).text == '3, 1'
		screenshot('process-text-input')
	}

	static class Coordinate {
		int row
		int col

		Coordinate(final int row, final int col) {
			this.row = row
			this.col = col
		}
	}
}
