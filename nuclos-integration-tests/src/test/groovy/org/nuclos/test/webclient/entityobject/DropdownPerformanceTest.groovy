package org.nuclos.test.webclient.entityobject

import org.apache.commons.io.IOUtils
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.EntityObject
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class DropdownPerformanceTest extends AbstractWebclient2Test {

	private List<String> choicesForZwa = ['Zwar', 'abgezwackt', 'vierundzwanzig']
	private List<String> choicesForKzert = ['Konzert']
	private String kzert = 'k*zert'

	@Test
	void _00_setup() {
		InputStream stream = getClass().getResourceAsStream('exampletext.txt')
		String exampleText = IOUtils.toString(stream, 'UTF-8')

		EntityObject word = new EntityObject(TestEntities.EXAMPLE_REST_WORD)

		word.setAttribute('text', 'jweoriwejori')
		word.setAttribute('times', "30000")
		word.setAttribute('exampletext', exampleText)

		nuclosSession.save(word)
		word.executeCustomRule('example.rest.RegisterWords')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTDROPDOWNS)

		eo.addNew()
	}

	@Test
	void _05_testLov() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('lov')

		testInputZ(lov)
		testAddWa(lov)
		testInputKzert(lov)
	}

	@Test
	void _10_testLovWithVlp() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('lovwithvlp')

		testInputZ(lov)
		testAddWa(lov)
		testInputKzert(lov)
	}

	@Test
	void _15_testCombobox() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('combobox')

		testInputZ(lov)
		testAddWa(lov)
		testInputKzert(lov)
	}

	@Test
	void _20_testComboboxWithVlp() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues lov = eo.getLOV('comboboxwithvlp')

		testInputZ(lov)
		testAddWa(lov)
		testInputKzert(lov)
	}

	private void testInputZ(ListOfValues lov) {
		lov.sendKeys('z')
		assert lov.open
		assert lov.choices.size() == 100
	}

	private void testAddWa(ListOfValues lov) {
		lov.sendKeys('wa')
		assert lov.choices == choicesForZwa
	}

	private void testInputKzert(ListOfValues lov) {
		lov.inputElement.clear()
		lov.sendKeys(kzert)
		assert lov.choices == choicesForKzert
	}
}
