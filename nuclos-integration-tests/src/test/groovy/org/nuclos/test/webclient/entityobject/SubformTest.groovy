package org.nuclos.test.webclient.entityobject

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.EntityObjectModal
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.util.RequestCounts
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class SubformTest extends AbstractWebclient2Test {

	@BeforeClass
	static void setup() {
		startProxy()
		setup(true, true)
		RESTHelper.createUser('readonly', 'readonly', ['Example readonly'], nuclosSession)
	}

	@Test
	void _00_createEOs() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)
		eo.addNew()
		screenshot('createnewentry-b')

		def testEntryName = '42'

		eo.setAttribute('customerNumber', testEntryName)
		eo.setAttribute('name', testEntryName)

		screenshot('createnewentry-c')
		eo.save()
		screenshot('createnewentry-d')
	}

	@Test
	void _10_insertSubformEntryInModal() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.getTab('Orders').click()

		Subform subform = eo.getSubform(orderSubBoMetaId)
		subform.newRow()

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		parentRefFieldShouldBeDisabled:
		{
			assert eoModal.getAttribute('customer') == '42'
			assert eoModal.isAttributeReadonly('customer')
		}

		eoModal.setAttribute('orderNumber', 111)
		eoModal.clickButtonOk()

		eo.save()

		assert subform.rowCount == 1

		Subform.Row row = subform.getRow(0)
		assert row.getValue('orderNumber') == '111'
	}

	@Test
	void _12_editSubformEntryInModal() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(orderSubBoMetaId)
		RequestCounts requests = countRequests {
			subform.getRow(0).editInModal()
		}
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_REQUEST) == 0
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_SUBFORM_REQUEST) == 1

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		parentRefFieldShouldBeDisabled:
		{
			assert eoModal.getAttribute('customer') == '42'
			assert eoModal.isAttributeReadonly('customer')
		}

		assert eoModal.getAttribute('orderNumber') == '111'
		eoModal.setAttribute('orderNumber', 222)

		eoModal.clickButtonOk()

		eo.save()

		assert subform.rowCount == 1

		Subform.Row row = subform.getRow(0)
		assert row.getValue('orderNumber') == '222'
	}

	@Test
	void _15_testDynamicButtonActivation() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTSUBFORMBUTTONS)
		eo.addNew()

		Subform subform = eo.getSubform('nuclet_test_other_TestSubformButtonsDependent_reference')
		checkEmptySubform:
		{
			assert subform.newEnabled
			assert !subform.deleteEnabled
			assert !subform.cloneEnabled
		}

		Subform.Row row = subform.newRow()
		assert !row.selected

		checkSubformWithoutSelection:
		{
			assert subform.newEnabled
			assert !subform.deleteEnabled
			assert !subform.cloneEnabled
		}

		row.selected = true
		assert row.selected

		checkSubformWithSelection:
		{
			assert subform.newEnabled
			assert subform.deleteEnabled
			assert subform.cloneEnabled
		}

		eo.cancel()
	}

	String auftragsPosition = TestEntities.EXAMPLE_REST_AUFTRAGSPOSITION.fqn
	String tabAuftragsPosition = 'tab_' + auftragsPosition + '_auftrag'
	String auftragNo1stTransition = TestEntities.EXAMPLE_REST_AUFTRAG.fqn
	String customerBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMER.fqn

	Map customer = [customerNumber: 123, name: 'Customer 123']
	String subBoMetaId = TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer'
	String orderSubBoMetaId = TestEntities.EXAMPLE_REST_ORDER.fqn + '_customer'

	@Test
	void _30_createBos() {
		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_CUSTOMER, customer)
	}

	@Test
	void _32_testEditLinksForIgnoredSublayout() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		def data = [city: 'Test', zipCode: '123', street: 'Test']
		subformRow.enterValues(data)

		eo.save()
		assert !subformRow.canEditInModal()
		assert subformRow.canEditInWindow()

		subformRow.enterValue('city', 'Test 2')
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.cancel()
		assert !subformRow.canEditInModal()
		assert subformRow.canEditInWindow()

		subformRow.setSelected(true)
		subform.deleteSelectedRows()
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
	}

	@Test
	void _34_testEditLinksForEnabledSublayout() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		eo.selectTab('Orders')

		Subform subform = eo.getSubform(orderSubBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		EntityObjectModal eoModal = EntityObjectComponent.forModal()
		eoModal.setAttribute('orderNumber', '123')
		eoModal.clickButtonOk()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		subformRow.enterValue('orderNumber', '1234')
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.cancel()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		subformRow.editInModal()
		eoModal = EntityObjectComponent.forModal()
		eoModal.setAttribute('orderNumber', '1234')
		eoModal.clickButtonOk()
		assert subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
		eo.getAttribute('orderNumber') == '1234'

		subformRow.setSelected(true)
		subform.deleteSelectedRows()
		assert !subformRow.canEditInModal()
		assert !subformRow.canEditInWindow()

		eo.save()
	}

	@Test
	void _35_createEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.selectTab('Address')

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.newRow()
		assert subformRow.new

		// Expect validation error for empty subform row
		eo.save()

		// FIXME: Create proper error dialog
//		assert EntityObject.getErrorMessages().size() == 1
//		assert EntityObject.getErrorStacktraces().first().getAttribute('innerHTML').contains('common.exception.novabitvalidationexception')
		eo.clickButtonOk()

		assert EntityObjectComponent.getErrorMessages().size() == 0

		// Enter valid data
		def data = [city: 'Sauerlach', zipCode: '82054', street: 'Mühlweg 2']
		subformRow.enterValues(data)

		assert subformRow.getValue('city') == data.city
		assert subformRow.getValue('zipCode') == data.zipCode
		assert subformRow.getValue('street') == data.street

		eo.save()

		assert subformRow.getValue('city') == data.city
		assert subformRow.getValue('zipCode') == data.zipCode
		assert subformRow.getValue('street') == data.street

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_2', zipCode: '82054_2', street: 'Mühlweg 2_A']
		subformRow.enterValues(data)

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_3', zipCode: '82054_3', street: 'Mühlweg 2_B']
		subformRow.enterValues(data)

		subformRow = subform.newRow()
		data = [city: 'Sauerlach_4', zipCode: '82054_4', street: 'Mühlweg 2_C']
		subformRow.enterValues(data)

		assert subform.getRowCount() == 4

		// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (4)'

		assert subformRow.new
		eo.save()

		assert !subformRow.new
		assert !subformRow.dirty
		assert !subformRow.deleted

		assert subform.getRowCount() == 4

		// TODO: Test if grid cells are always editable with a single click

		// TODO: Test new BO entry with subform data and only 1 save

		// TODO: Test dependent subforms

		// TODO: Test validation of subform fields
	}

	/**
	 * The "edit in new window" link should not be visible,
	 * if the user has no rights on the target entity.
	 */
	@Test
	void _37_editInWindowWithoutRights() {
		logout()
		login('readonly', 'readonly')

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_CUSTOMERADDRESS.fqn + '_customer')
		Subform.Row row = subform.getRow(0)

		assert !row.canEditInWindow()

		logout()
		login('test', 'test')
	}

	@Test
	void _40_updateEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.getRow(1)
		def data = [city: '', zipCode: '', street: '']

		// TODO: Uncomment the following code block when NUCLOS-4220 is fixed
		/*
subformRow.enterValues(data);


// Expect validation error for empty subform row
EntityObject.save();
expect(Sideview.getErrorMessages().count()).toBe(1);
expect(Sideview.getErrorStacktraces().first().getInnerHtml()).toContain('common.exception.novabitvalidationexception');
Sideview.clickOkButton();
expect(Sideview.getErrorMessages().count()).toBe(0);
*/
		data = [city: 'Sauerlach_5', zipCode: '82054_5', street: 'Mühlweg 2_5']

		assert !subformRow.dirty
		assert !eo.dirty
		subformRow.enterValues(data)
		assert subformRow.dirty
		assert eo.dirty

		eo.save()
		// Neu Selektieren damit die Maske g'scheit ausschaut (NUCLOS-5479)
		Sidebar.selectEntry(0)
		subform = eo.getSubform(subBoMetaId)

		for (int i = 0; i < subform.rowCount; i++) {
			if (subform.getRow(i).getValue('city') == data.city) {
				Log.debug 'Matching row: ' + i
				subformRow = subform.getRow(i)

				assert subformRow.getValue('city') == data.city
				assert subformRow.getValue('zipCode') == data.zipCode
				assert subformRow.getValue('street') == data.street
			}
		}
	}

	@Test
	void _42_checkInputIsNotLost() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		Subform.Row subformRow = subform.getRow(0)

		assert !subformRow.dirty
		assert !eo.dirty
		String oldStreet = subformRow.getValue('street')
		String newStreet = "$oldStreet 2"

		enterValueWithoutLeavingEditor:
		{
			NuclosWebElement cell = subformRow.getFieldElement('street')
			cell.sendKeys(Keys.ENTER)
			cell.$('input').sendKeys("$oldStreet 2")
		}

		assert eo.dirty

		// TODO: Row should be dirty now, too
//		assert subformRow.dirty

		eo.save()

		assert subformRow.getValue('street') == newStreet

		subformRow.enterValue('street', oldStreet)
		eo.save()
	}

	@Test
	void _45_testCloning() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(subBoMetaId)
		subform.selectAllRows()
		assert subform.rowCount == 4

		assert !eo.dirty
		subform.cloneSelectedRows()
		assert subform.getRow(0).new
		assert eo.dirty

		eo.save()

		// uncheck "select all rows" checkbox
		subform.unselectAllRows()
		assert subform.rowCount == 8

		// FIXME
//		assert EntityObject.getTab('tab_' + subBoMetaId + '_customer').getText() == 'Address (8)'

	}

	@Test
	void _52_testDirtyState() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getTab('Orders').click()

		Subform orderSubform = eo.getSubform(orderSubBoMetaId)
		Subform.Row row = orderSubform.newRow()

		// Close the edit modal
		EntityObjectComponent.clickButtonOk()

		row.enterValues([orderNumber: '123'])

		eo.save()

		assert orderSubform.rowCount == 1

		// Reference field
		assertDirtyStateAndReset {
			orderSubform.getRow(0).enterValue('customerAddress', 'Sauerlach, 82054, Mühlweg 2')
		}

		// Date chooser
		assertDirtyStateAndReset {
			orderSubform.getRow(0).enterValue('sendDate', '2017-12-31')
		}
	}

	private static void assertDirtyStateAndReset(Closure c) {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert !eo.dirty
		c()
		assert eo.dirty
		eo.cancel()
	}

	@Test
	void _55_deleteEntry() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.getTab('Address').click()
		Subform subform = eo.getSubform(subBoMetaId)

		int rowCount = subform.rowCount

		subform.getRow(1).selected = true
		subform.getRow(3).selected = true

		screenshot('before-deletion')
		assert !eo.dirty
		assert !subform.getRow(1).deleted
		subform.deleteSelectedRows()
		assert subform.getRow(1).deleted
		assert eo.dirty
		screenshot('after-deletion')

		assert subform.rowCount == rowCount
		assert subform.deletedRowCount == 2

		eo.save()

		assert subform.getRowCount() == rowCount - 2

		subform.selectAllRows()
		for (int i = 0; i < subform.rowCount; i++) {
			assert subform.getRow(i).selected
		}

		subform.deleteSelectedRows()

		assert subform.deletedRowCount == subform.rowCount

		eo.save()

		assert subform.rowCount == 0
	}

	@Test
	void _60_testSubsubform() {
		// TODO
	}


	def goDocument = "org_nuclos_businessentity_nuclosgeneralsearchdocument"
	def tabGoDocument = "tab_" + goDocument + "_genericObject"
	def tabNotiz = "tab_Notiz"

	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _75_testSubformPaging() {
//		assert logout()
//		assert login('nuclos', '')
//
//		def anzahl = 250
//
//		Sideview.createBo(auftragNo1stTransition, [name: 'Auftrag 3', anzahl: anzahl])
//		WebElement tab = Sideview.getTab(tabAuftragsPosition)
//
//		assert tab != null
//		assert tab.getText() == "Position (0)"
//
//		Subform subform = EntityObject.getSubform(auftragsPosition)
//
//		assert subform != null
//		assert subform.rowCount == 0
//
//		Sideview.clickButton('Regel 1') //Start Rule that adds 'anzahl' Words to the subform
//
//		screenshot('afterClickRegelButton1')
//
//		List<WebElement> lstRows = $$('.ui-grid-row')
//
//		assert lstRows.size() >= 30 && lstRows.size() <= 50 // TODO: lstRows.size() should be exactly 40?
//		assert tab.getText() == "Position (40/" + anzahl + ")"
//
//		assert Sideview.getTab(tabGoDocument).getText() == "Dokumente (0)"
//		assert Sideview.getTab(tabNotiz).getText() == "Notiz"
//
//		// scroll down subform list //TODO: Doesn't Work yet
//		new Actions(driver).moveToElement($$('.ui-grid-row')[30]).build().perform();
//
//		lstRows = $$('.ui-grid-row')
//
//		//TODO: Doesn't work because of scrolling didn't work
//		//assert lstRows.size() >= 60 && lstRows.size() <= 80
	}

	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _80_testSortingWithPaging() {
//		// TODO change sort direction doesn't work in phantomjs
//		if (AbstractWebclientTest.browser != AbstractWebclientTest.Browser.PHANTOMJS) {
//
//			Subform subform = EntityObject.getSubform(auftragsPosition)
//
//			Map<String, Integer> mp = countStartingCharacters(subform, 10)
//
//			//Unsorted, neither lot of 'A' or 'z'
//
//			Integer countA = mp.get('a')
//			assert countA == null || countA < 2
//
//			Integer countz = mp.get('z')
//			assert countz == null || countz < 2
//
//			subform.sort(1)
//
//			screenshot("SortAsc")
//
//			mp = countStartingCharacters(subform, 10)
//
//			//Sorted Asc, lot of 'a' but no 'z'
//
//			countA = mp.get('a')
//			assert countA >= 5
//
//			countz = mp.get('z')
//			assert countz == null
//
//			subform.sort(1)
//
//			screenshot("SortDesc")
//
//			mp = countStartingCharacters(subform, 10)
//
//			//Sorted Desc, lot of 'z' but no 'a'
//
//			countA = mp.get('a')
//			assert countA == null
//
//			countz = mp.get('z')
//			assert countz >= 5
//
//			// new entry - sort should apply first after saving
//
//			subform.newRow()
//			subform.getRow(0).enterValues([name: 'Nuclos'])
//
//			assert subform.getRow(0).getValue('name') == 'Nuclos'
//
//			subform.sort(1) //Delete Sorting
//			subform.sort(1) //SortingAsc
//
//			//Must still be at top
//			assert subform.getRow(0).getValue('name') == 'Nuclos'
//
//			mp = countStartingCharacters(subform, 10)
//			//The 'z' also as sorting did not change while editing
//			countz = mp.get('z')
//			assert countz >= 5
//
//			// Sortierung wieder auf den Ausgangszustand setzen, da sonst ein weiterer Testlauf scheitert
//			// TODO Reset über Rest-Methode
//			subform.sort(1) //SortingDesc
//			subform.sort(1) //Delete Sorting
//		}
	}


	@Test
	@Ignore('TODO: Paging is not implemented in Webclient2 yet')
	void _85_testDynamicLoading() {
//		def anzahl = 250
//		Subform subform = EntityObject.getSubform(auftragsPosition)
//
//		WebElement tab = Sideview.getTab(tabAuftragsPosition)
//
//		// scroll down subform list
//		subform.scrollToRow(30)
//		sleep(1000)
//		waitForAngularRequestsToFinish()
//
//		List<WebElement> lstRows = $$('.ui-grid-row')
//
//		assert lstRows.size() > 40
	}


	@Test
	void _90tabKeyTest() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')


		Subform.Row firstRow = subform.newRow()
		Subform.Row secondRow = subform.newRow()

		NuclosWebElement priceWe = firstRow.getFieldElement('price')

		priceWe.sendKeys(Keys.ENTER)
		NuclosWebElement priceWeInput = priceWe.$('input')
		priceWeInput.sendKeys('333')
		priceWeInput.sendKeys(Keys.TAB)

		NuclosWebElement quantityWeInput = firstRow.getFieldElement('quantity').$('input')
		quantityWeInput.sendKeys('222')
		quantityWeInput.sendKeys(Keys.TAB)

		def price = firstRow.getValue('price')
		assert price == '333.00' || price == '333,00'
		def quantity = firstRow.getValue('quantity')
		assert quantity == '222.00' || quantity == '222,00'

		eo.cancel()
	}

	@Test
	void _95_openDynamicSubformEntity() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER)
		assert eo.getAttribute('name') == 'Test 2'

		eo.selectTab('Dynamic Proxy')
		Subform subform = eo.getSubform('nuclet_test_other_DynamicProxyContainerDYN_INTIDTUDGENERICOBJECT')

		subform.getRow(0).editInWindow()

		checkOtherWindow:
		{
			switchToOtherWindow()

			EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
			assert eo2.id == eo.id
			assert currentUrl.contains('/' + TestEntities.NUCLET_TEST_OTHER_TESTPROXYCONTAINER.fqn + '/')

			switchToOtherWindow()
		}
	}


	static Map<String, Integer> countStartingCharacters(Subform subform, Integer nrows) {
		Map<String, Integer> ret = new HashMap<String, Integer>();

		for (int i = 0; i < nrows; i++) {
			String c = subform.getRow(i).getValue('name', String.class).substring(0, 1).toLowerCase()
			if (!ret.containsKey(c)) {
				ret.put(c, 0)
			}

			ret.put(c, ret.get(c) + 1)
		}

		return ret
	}
}
