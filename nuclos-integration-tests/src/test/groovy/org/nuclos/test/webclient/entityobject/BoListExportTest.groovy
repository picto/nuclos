package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

/**
 * TODO deaktiviert, da auf dem Test-Server das Verzeichnis data/compiled-reports noch nicht erstellt wird
 * see AbstractUnpacker: DIR_NAME_COMPILED_REPORTS
 *
 * TODO: @CompileStatic
 */
@Ignore
class BoListExportTest extends AbstractWebclientTest {
	
	static String testEntryName = null
	static String sessionId
	
	void checkExportResponse(String url, String sessionId, String containsText) {

		
		HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Cookie","JSESSIONID=" + sessionId);
		connection.connect();
		
		InputStreamReader isr = new InputStreamReader((InputStream) connection.getContent());
		BufferedReader buff = new BufferedReader(isr);
		String line;
		String text;
		while (true) {
		  line = buff.readLine();
		  text += line;
			if (line == null) {
				break
			}
		}
		
		assert connection.getResponseCode() == 200 // HTTP status ok
		
		assert text.size() > 0
		
		if (containsText != null) {
			assert text.indexOf(containsText) != -1
		}
	}
	
	@Test
	void _00_setup() {
		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSessionId)
		assert $('#logout')
	}
	
	@Test
	void _01_checkSecurity() {
		
		Sideview.openSideview('example_rest_Article')
		Sideview.openFirstBoMenuEntry()

		// user 'test' has not the required permission
		assert $('#e2e-test-export-link') == null
	}

	@Test
	void _02_openPrintDialog() {

		logout()
		login('nuclos')
		sessionId = RESTHelper.login('nuclos', '')
		
		Sideview.openSideview('example_rest_Article')
		Sideview.openFirstBoMenuEntry()
		
		// open export dialog
		$('#export-bo-list').click()
		waitForAngularRequestsToFinish()
	}
	
	
	private void testExecuteDownload(String outputType) {
		testExecuteDownload(outputType, null)
	}
	private void testExecuteDownload(String outputType, String containsText) {
		// select outputType
		$('#list-export-modal input[value="' + outputType + '"]').click()
		
		// execute export
		$('#list-export-modal #execute-listexport-button').click()
		waitForAngularRequestsToFinish()
		
		String exportHref = $('#e2e-test-export-link').getAttribute("href")
		
		checkExportResponse(exportHref, sessionId, containsText)
		screenshot('export-' + outputType + '-done')
	}
	
	
	@Test
	void _03_executePrintoutPDF() {
		testExecuteDownload('pdf')
	}
	
	@Test
	void _04_executePrintoutCSV() {
		testExecuteDownload('csv', '"Mouse"')
	}
	
	@Test
	void _05_executePrintoutXLSX() {
		testExecuteDownload('xlsx')
	}
	
	@Test
	void _06_executePrintoutXLS() {
		testExecuteDownload('xls')
	}
	
	@Test
	void _07_prepareSubformExport() {
		closeModal()
		Sideview.openSideview('example_rest_Order')
		$('#subform-export-button').click()
	}
	
	@Test
	void _08_executeSubformPrintoutPDF() {
		testExecuteDownload('pdf')
	}

	@Test
	void _09_executeSubformPrintoutCSV() {
		testExecuteDownload('csv', '1004 Mouse";"5400";"180";""')
	}
	
	@Test
	void _10_executeSubformPrintoutXLSX() {
		testExecuteDownload('xlsx')
	}
	
	@Test
	void _11_executeSubformPrintoutXLS() {
		testExecuteDownload('xls')
	}
	
	
	
	/*
	TODO: this does not work at the moment
	
	@Test
	void _12_prepareSubformExport2() {
		closeModal()
		Sideview.openSideview('example_rest_Customer')
		
		// select second entry
		$$('.sideview-list-resizable .sideview-list-entry')[1].click()
		
		Sideview.getTab('tab_Orders').click()
		
		$('#subform-export-button').click()
	}

	@Test
	void _13_executeSubformPrintoutXLS() {
		testExecuteDownload('xls')
	}
	*/

}
