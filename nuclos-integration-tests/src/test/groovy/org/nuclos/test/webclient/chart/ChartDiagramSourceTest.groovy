package org.nuclos.test.webclient.chart

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclientTest
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent

import groovy.transform.CompileStatic

/**
 * Charts mit Daten aus Diagramm Datenquelle
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
@Ignore('TODO: Adjust for Webclient2!')
class ChartDiagramSourceTest extends AbstractWebclientTest {

	String chartOptionsString =
	"""
{
  "name": {
      "de": "Umsätze"
  }, 
  "chart": {


    "type": "multiBarChart",
    "margin": {
      "left": 100,
      "top": 25,
      "bottom": 40,
      "right": 40
    },
    "primaryChart": {
      "boMetaId": "example_rest_CategorySalesCustomerCRT",
      "refBoAttrId": "example_rest_CategorySalesCustomerCRT_INTIDTUDGENERICOBJECT",
      "categoryBoAttrId": "example_rest_CategorySalesCustomerCRT_orderMonth",
      "categoryLabel": {
        "de": "X"
      },
      "scaleLabel": {
        "de": "Euro"
      },
      "series": [
        {
          "boAttrId": "example_rest_CategorySalesCustomerCRT_sales",
          "label": {
            "de": "Wert"
          }
        }
      ]
    },
    "yAxis": {
      "tickFormat": "function (value) {return d3.format('.2s')(value) +' Euro';}"
    },
    "transitionDuration": 250,
    "searchTemplate": [
      {
        "label": "Artikelkategorie",
        "boAttrId": "example_rest_CategorySalesCustomerCRT_categoryName",
        "comparisonOperator": "EQUAL",
        "mandatory": false,
        "dropdown": true
      },
      {
        "label": "Monat von",
        "boAttrId": "example_rest_CategorySalesCustomerCRT_orderMonth",
        "comparisonOperator": "GTE",
        "mandatory": false
      },
      {
        "label": "bis",
        "boAttrId": "example_rest_CategorySalesCustomerCRT_orderMonth",
        "comparisonOperator": "LTE",
        "mandatory": false
      }
    ],
    "x": "function (dataSet) { return dataSet.categoryValue; }",
    "y": "function (dataSet) { return dataSet.seriesValue; }"
  },
  "chartIndex": 0
}
	""";

	@Test
	void _01setupBos() {

		assert $('#logout')
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _02configureChart() {

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_CUSTOMER)

		// select 2nd entry
		$$('.sideview-list-resizable .sideview-list-entry')[1].click()

		$('#morefunctions button').click()
		$('#open-charts-button').click()
		waitForAngularRequestsToFinish()

		assert $$('#chart-selection .chart-selection-item').size() == 0

		$('#new-chart').click()
		waitForAngularRequestsToFinish()

		$('#chart-options-tab-extended').click()
		$('#chartOptionsString').clear()

		// $('#chartOptionsString').sendKeys(chartOptionsString) // doesn't work
		executeScript("arguments[0].value = arguments[1];", $('#chartOptionsString'), chartOptionsString);

		$('#chartOptionsString').sendKeys(" ") // trigger angular 2 way-data-binding


		$('#save-chart-button').click();

		closeModal()

		// open chart modal again
		$('#morefunctions button').click()
		$('#open-charts-button').click()
		waitForAngularRequestsToFinish()

		// number of bars
		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 12 }

		screenshot('first-chart')

		assert $$('#chart-selection .chart-selection-item').size() == 1

		assert $$('.modal-dialog svg .nv-multiBarWithLegend').size() == 1
	}

	@Test
	void _03filterChartData() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.setAttribute('searchfilter-0', 'Hardware')
		$('#chart-searchfilter #ok-button').click()
		waitForAngularRequestsToFinish()

		// Actual bar count should be 6, but the nvd3 plugin produces additional elements
		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 12 }

		screenshot('chart-filter-1')


		eo.setAttribute('searchfilter-1', '2014.06')
		$('#chart-searchfilter #ok-button').click()
		waitForAngularRequestsToFinish()

		// Actual bar count should be 4, but the nvd3 plugin produces additional elements
		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 8 }

		screenshot('chart-filter-2')


		eo.setAttribute('searchfilter-2', '2015.04')
		$('#chart-searchfilter #ok-button').click()
		waitForAngularRequestsToFinish()

		// Actual bar count should be 2, but the nvd3 plugin produces additional elements
		waitFor(3) { $$('.modal-dialog nvd3 .nv-bar').size() == 4 }

		screenshot('chart-filter-3')

	}

}
