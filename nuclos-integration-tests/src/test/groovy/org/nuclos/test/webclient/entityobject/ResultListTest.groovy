package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.Searchtemplate

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ResultListTest extends AbstractWebclient2Test {
	Map wordStart = [text: 'Words', times: 64, agent: 'test']

	@Test
	void _0setup() {
		nuclosSession.managementConsole('enableIndexer')
	}

	@Test
	void _2openMenuEntry() {

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORD)

		assert $('#logout')
	}

	@Test
	void _3createNewEntries() {

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, wordStart)

		assert eo.listEntryCount == 1

		String text = eo.checkField('countletters', true, false).getAttribute('value')

		assert text != null
		assert Integer.parseInt(text.replaceAll(",", "").replaceAll("\\.", "")) > 0

		eo.clickButton("Start")

		waitForAngularRequestsToFinish()

		EntityObjectComponent.refresh()

		screenshot('afterCollectingWords')

		/*
		 *  listEntryCount could be 40 or 64
		 *  sideview loads 40 entries and then triggers next chunk to load
		 */
		assert eo.listEntryCount >= 40
	}

	@Test
	void _4textSearch() {
		Searchtemplate.search('Medizin')

		screenshot('afterSearch1')

		EntityObjectComponent eo = EntityObjectComponent.forDetail()
		waitFor(5,
				{ eo.listEntryCount == 1 }
		)

		String text = eo.checkField('countletters', true, false).getAttribute('value')

		assert text != null
		assert Integer.parseInt(text.replaceAll(",", "").replaceAll("\\.", "")) > 0

		Searchtemplate.search('her')

		screenshot('afterSearch2')

		waitFor(5,
				{ eo.listEntryCount == 4 }
		)

	}
	
	//TODO Test _6luceneTest() (lucene Not Implemented)
	@Ignore
	@Test
	void _6luceneTest() {
		// lucene works async...
		// sleep(1000)
		luceneTest("doktor", 2)
	}
}
