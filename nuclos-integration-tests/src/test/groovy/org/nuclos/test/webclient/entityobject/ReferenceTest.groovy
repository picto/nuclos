package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.schema.layout.web.WebListofvalues
import org.nuclos.schema.layout.web.WebTextfield
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.ListOfValues
import org.nuclos.test.webclient.pageobjects.Searchtemplate
import org.nuclos.test.webclient.pageobjects.Sidebar
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.nuclos.test.webclient.pageobjects.subform.SubformReferenceCell
import org.openqa.selenium.Keys

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ReferenceTest extends AbstractWebclient2Test {

	private allArticleChoices = ['1001', '1002', '1003', '1004']

	@Test
	void _00_createEO() {
		TestDataHelper.insertTestData(nuclosSession)

		def customer4 =
				[
						boMetaId  : TestEntities.EXAMPLE_REST_CUSTOMER.fqn,
						attributes: [
								name          : 'Test-Customer 4',
								customerNumber: 2003,
								discount      : 22.5,
								active        : true
						]
				]
		RESTHelper.createBo(customer4, nuclosSession)
	}

	@Test
	void _05_openCreatedEO() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		String customerValue = eo.getAttribute('customer')
		assert "Test-Customer".equals(customerValue)
	}

	@Test
	void _10_testReopenCombobox() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		String entry1 = 'Test 1, 12345, Test 1'
		String entry2 = 'Test 2, 54321, Test 2'

		assert eo.getAttribute('customer') == 'Test-Customer'
		assert !eo.getText('customerAddress')?.trim()

		ListOfValues lov = eo.getLOV('customerAddress')
		assert !lov.open
		assert !lov.textValue

		lov.open()
		assert !lov.textValue
		assert lov.choices == ['', entry1, entry2]
		lov.close()

		lov.selectEntry(entry2)
		assert eo.getAttribute('customerAddress') == entry2

		lov.open()
		assert lov.choices == ['', entry1, entry2]
		lov.close()

		eo.save()
		lov = eo.getLOV('customerAddress')
		assert !lov.open

		lov.open()
		assert lov.choices == ['', entry1, entry2]
		lov.close()
	}

	@Test
	void _12_testReopenComboboxInSubform() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('category')
		ListOfValues lov = row.getLOV('category')
		assert lov.open
		assert !lov.textValue
		assert lov.choices.size() > 1

		2.times {
			lov.clickDropdownButton()
			assert !lov.open

			lov.clickDropdownButton()
			assert lov.open
			assert lov.choices.size() > 1
		}

		lov.closeViaEscape()
	}

	/**
	 * Tests if the selected text is still visible in the input field of the LOV,
	 * when the LOV is open.
	 */
	@Test
	void _13_testTextValueWithOpenPanel() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('category')
		ListOfValues lov = row.getLOV('category')
		assert lov.open
		assert !lov.textValue
		assert lov.choices.size() > 1

		lov.selectEntry('Hardware')

		row.clickCell('category')
		lov = row.getLOV('category')
		assert lov.open
		assert lov.textValue == 'Hardware'

		2.times {
			lov.clickDropdownButton()
			assert !lov.open
			assert lov.textValue == 'Hardware'

			lov.clickDropdownButton()
			assert lov.open
			assert lov.textValue == 'Hardware'
		}

		eo.cancel()
	}

	/**
	 * Tests if the highlighting via up/down keys works.
	 */
	@Test
	void _14_testSubformLovArrowKeys() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		assert row.getValue('article') == '1004 Mouse'

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open

		// FIXME: String representation is not consistent between input field and dropdown
		assert lov.choices == allArticleChoices
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1004'

		// Down-Arrow should not do anything, if the last entry is already highlighted
		lov.sendKeys(Keys.ARROW_DOWN)
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1004'

		lov.sendKeys(Keys.ARROW_UP)
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1003'

		lov.sendKeys(Keys.ARROW_UP)
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1002'

		lov.sendKeys(Keys.ARROW_UP)
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1001'

		// Up-Arrow should not do anything, if the first entry is already highlighted
		lov.sendKeys(Keys.ARROW_UP)
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1001'

		lov.sendKeys(Keys.TAB)
		assert !lov.open

		assert row.getValue('article') == '1001'

		row.clickCell('article')
		lov = row.getLOV('article')

		assert lov.selectedEntryFromDropdown == '1001'
		assert lov.highlightedEntry == '1001'

		eo.cancel()

		row.clickCell('article')
		lov = row.getLOV('article')

		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1004'

		lov.sendKeys(Keys.ESCAPE)
		assert !eo.dirty
	}

	@Test
	void _15_testAutocompletionBehavior() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Subform subform = eo.getSubform(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn + '_order')
		Subform.Row row = subform.getRow(0)

		row.clickCell('article')
		ListOfValues lov = row.getLOV('article')
		assert lov.open

		// FIXME: String representation is not consistent between input field and dropdown
		assert lov.choices == ['1001', '1002', '1003', '1004']
		assert lov.selectedEntryFromDropdown == '1004'
		assert lov.highlightedEntry == '1004'

		testAutocompletionBetweenKeyPresses: {
			lov.sendKeys(Keys.chord(Keys.CONTROL, 'a'))
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert lov.selectedEntryFromDropdown == '1004'
			assert lov.highlightedEntry == '1004'

			lov.sendKeys('1')
			waitForAngularRequestsToFinish()
			assert eo.dirty
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			lov.sendKeys('1')
			waitForAngularRequestsToFinish()
			assert lov.choices.empty
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			lov.sendKeys('*')
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			lov.sendKeys('1')
			waitForAngularRequestsToFinish()
			assert lov.choices == ['1001']
			assert !lov.selectedEntryFromDropdown
			assert lov.highlightedEntry == '1001'
		}

		lov.sendKeys(Keys.ENTER)
		waitForAngularRequestsToFinish()
		assert row.getValue('article') == '1001'

		row.clickCell('article')
		lov = row.getLOV('article')
		assert lov.open

		testAutocompletionForBackspaces: {
			assert lov.selectedEntryFromDropdown == '1001'
			assert lov.highlightedEntry == '1001'

			// FIXME: There is an extra space at the end: "1001 "
			lov.sendKeys(Keys.BACK_SPACE)
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()

			// "100"
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "10"
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "1"
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// ""
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry
			assert lov.inputElement.text == ''

			// "0"
			lov.sendKeys('0')
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry

			// "01"
			lov.sendKeys('1')
			waitForAngularRequestsToFinish()
			assert lov.choices == ['1001']
			assert !lov.selectedEntryFromDropdown
			assert lov.highlightedEntry == '1001'

			// ""
			lov.sendKeys(Keys.BACK_SPACE)
			sleep(50)
			lov.sendKeys(Keys.BACK_SPACE)
			waitForAngularRequestsToFinish()
			assert lov.choices == allArticleChoices
			assert !lov.selectedEntryFromDropdown
			assert !lov.highlightedEntry
			assert lov.inputElement.text == ''
		}

		eo.cancel()
	}

	@Test
	void _18_searchReferenceInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.searchReference()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		Searchtemplate.search('Test-Customer 2')
		Sidebar.selectEntryByText('Test-Customer 2')

		// select first entry in other window
		eo.selectInOtherWindow()

		driver.switchTo().window(oldWindow)

		eo.save()

		assert eo.getAttribute('customer') == 'Test-Customer 2'
	}

	@Test
	void _20_editReferenceInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		Sidebar.selectEntry(2)
		eo.setAttribute('note', 'note changed')

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.openReference()

		waitForAngularRequestsToFinish()

		editCustomerInOtherWindow:
		{
			assert driver.getWindowHandles().size() == 2

			String oldWindow = driver.windowHandle
			String newWindow = switchToOtherWindow()

			assert oldWindow != newWindow

			assert driver.currentUrl.contains('Customer')

			EntityObjectComponent eo2 = EntityObjectComponent.forDetail()
			assert eo2.getAttribute('name') == 'Test-Customer'
			eo2.setAttribute('name', 'Test-Customer changed')

			// save and close window
			eo2.save()

			driver.switchTo().window(oldWindow)
		}

		assert eo.getAttribute('note').equals('note changed')
		assert eo.getAttribute('customer') == 'Test-Customer changed'

		eo.cancel()
	}


	@Test
	void _25_createReferencedEntryInMainForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		customerLov.addReference()


		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		// create new customer entry
		eo.setAttribute('customerNumber', '2004')
		eo.setAttribute('name', 'Test-Customer 3')
		eo.save()

		driver.switchTo().window(oldWindow)

		waitForAngularRequestsToFinish()
		eo.save()

		// new customer is now selected
		assert eo.getAttribute('customer') == "Test-Customer 3"
	}


	@Test
	void _30_searchReferenceInSubForm() {

		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		screenshot('subform-category-selected')

		// click search in other window icon
		subformReferenceCell.searchReference()

		waitForAngularRequestsToFinish()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		screenshot('select-entry-for-subform')

		Searchtemplate.search('Software')

		// select first entry in other window
		EntityObjectComponent.forDetail().selectInOtherWindow()

		driver.switchTo().window(oldWindow)

		screenshot('is-entry-selected-for-subform')

		waitFor {
			subformReferenceCell.subformReference.text == 'Software'
		}
	}


	@Test
	void _35_addReferenceInSubForm() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.open(TestEntities.EXAMPLE_REST_ORDER)
		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		screenshot('subform-category-selected')

		// click add in other window icon
		subformReferenceCell.addReference()

		waitForAngularRequestsToFinish()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		screenshot('add-entry-for-subform')

		// create new category entry
		eo.setAttribute('name', 'Test-Category')
		eo.save()

		driver.switchTo().window(oldWindow)

		screenshot('is-entry-added-for-subform')

		waitFor {
			subformReferenceCell.subformReference.text == 'Test-Category'
		}
	}

	@Test
	void _40_openReferenceViaLovOverlay() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		eo.refresh()
		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'article')

		screenshot('subform-category-selected')

		// click add in other window icon
		subformReferenceCell.openReference()

		waitForAngularRequestsToFinish()

		assert driver.getWindowHandles().size() == 2

		String oldWindow = driver.windowHandle
		String newWindow = switchToOtherWindow()

		assert oldWindow != newWindow

		screenshot('open-ref-entry-from-subform')

		assert driver.currentUrl.indexOf("Article") != -1
	}

	@Test
	void _45_testOverlayVisibility() {

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		ListOfValues customerLov = ListOfValues.findByAttribute('customer')
		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert customerLov.isOpenVisible()


		eo.setAttribute('customer', '')

		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert !customerLov.isOpenVisible()


		eo.cancel()

		assert customerLov.isSearchVisible()
		assert customerLov.isAddVisible()
		assert customerLov.isOpenVisible()
	}

	@Test
	void _50_testOverlayVisibilitySubform() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		SubformReferenceCell subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		assert subformReferenceCell.isSearchVisible()
		assert subformReferenceCell.isAddVisible()
		assert !subformReferenceCell.isOpenVisible()


		subformReferenceCell.setValue('Hardware')

		subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')

		assert subformReferenceCell.isSearchVisible()
		assert subformReferenceCell.isAddVisible()
		assert subformReferenceCell.isOpenVisible()


		eo.cancel()

		subformReferenceCell = new SubformReferenceCell(TestEntities.EXAMPLE_REST_ORDERPOSITION.fqn, 'category')
		assert subformReferenceCell.isSearchVisible()
		assert subformReferenceCell.isAddVisible()
		assert !subformReferenceCell.isOpenVisible()
	}

	@Test
	void _55_testReferenceInTextfield() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS)

		eo.addNew()

		assert !eo.getText('reference', WebListofvalues)?.trim()
		assert !eo.getText('reference', WebTextfield)?.trim()

		eo.setAttribute('reference', 'nuclos')

		assert eo.getAttribute('reference', WebListofvalues) == 'nuclos'
		assert eo.getAttribute('reference', WebTextfield) == 'nuclos'

		eo.setAttribute('text', 'Test')
		eo.save()
		assert !eo.dirty

		assert eo.getAttribute('reference', WebListofvalues) == 'nuclos'
		assert eo.getAttribute('reference', WebTextfield) == 'nuclos'

	}

	@Test
	/* NUCLOS-5935 */
	void _60_testLovClickoutside() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		eo.addNew()

		eo.setAttribute('customer', 'Test-Customer 2')
		eo.setAttribute('note', 'Note')

		// assert that customer is still set
		assert eo.getAttribute('customer') == 'Test-Customer 2'

		eo.setAttribute('orderNumber', 3748293)
		eo.save()
	}

	@Test
	void _65_testDropdownEntrySelection() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		assert eo.getAttribute('customer') == 'Test-Customer 2'

		List<String> allCustomers = ['Test-Customer changed', 'Test-Customer 2', 'Test-Customer 3', 'Test-Customer 4']

		ListOfValues lov = eo.getLOV('customer')
		lov.open()
		assert lov.choices.containsAll(allCustomers)
		lov.close()

		// FIXME: Should not be dirty here
//		assert !eo.dirty

		testSelectionOfSingleResult:
		{
			lov.search('test*4')
			assert lov.choices == ['Test-Customer 4']

			lov.blur()
			assert lov.textValue == 'Test-Customer 4'
			assert eo.dirty

			eo.save()
			assert eo.getAttribute('customer') == 'Test-Customer 4'
		}

		testDeselectionForNoResult:
		{
			lov.search('nonexistentcustomer')
			assert lov.choices.empty
			assert eo.dirty
			lov.blur()
			assert !lov.textValue
			assert eo.dirty

			eo.save()
			assert !eo.getAttribute('customer')
		}

		eo.setAttribute('customer', 'Test-Customer 4')
		eo.save()

		testDeselectionForMultipleResults:
		{
			lov.search('Test')
			assert lov.choices.containsAll(allCustomers)
			lov.blur()
			assert !lov.textValue
			assert eo.dirty

			eo.save()
			assert !eo.getAttribute('customer')
		}

		testNotDirtyAfterSelectingSameEntry:
		{
			eo.setAttribute('customer', 'Test-Customer 4')
			eo.save()
			assert !eo.dirty

			lov.search('test*4')
			assert lov.choices == ['Test-Customer 4']

			lov.blur()
			assert lov.textValue == 'Test-Customer 4'
			// FIXME: Should not be dirty here
//			assert !eo.dirty
		}
	}
}
