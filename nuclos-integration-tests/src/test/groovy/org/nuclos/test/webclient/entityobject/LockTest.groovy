package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.subform.Subform
import org.openqa.selenium.WebElement

import groovy.transform.CompileStatic

/**
 * search references in new window
 */
@Ignore
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LockTest extends AbstractWebclient2Test {

	@Test
	void _00createTestUser() {
		RESTHelper.createUser('locktest', 'locktest', ['Example user'], nuclosSession)
	}

	@Test
	void _01setupBos() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _02lock() {

		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		eo.clickButton('Lock')

		assert $('#lock-info')
	}

	@Test
	void _03checkLock() {

		logout()
		login('nuclos', '')
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)

		assert $('#lock-info')

		eo.checkField('orderNumber', true, false)

		String orderNumber = eo.getAttribute('orderNumber')
		assert orderNumber != null && !orderNumber.empty

		boolean exception = false
		try {
			eo.setAttribute('orderNumber', '99999')
		} catch (Exception e) {
			exception = true
		}
		assert exception

		// no change
		assert orderNumber == eo.getAttribute('orderNumber')

		// no delete button
		assert eo.getDeleteButton() == null

		// no state change
		assert !eo.hasNextStateButton()

	}

	@Test
	void _04checkLockedUnlockedToggle() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		checkForEnabled(eo, false)

		$$('.sideview-list-entry')[1].click()

		checkForEnabled(eo, true)

		$$('.sideview-list-entry')[0].click()

		checkForEnabled(eo, false)

	}

	@Test
	void _05checkUnLock() {
		EntityObjectComponent eo = EntityObjectComponent.forDetail()

		waitForAngularRequestsToFinish()

		WebElement we = $('#unlock-button')
		assert we != null
		assert we.isEnabled()

		we.click()

		waitForAngularRequestsToFinish()

		assert !$('#lock-info')

		// delete button
		assert eo.getDeleteButton() != null

		// state change
		assert eo.hasNextStateButton()

		def newOrderNumber = '999'
		eo.setAttribute('orderNumber', newOrderNumber)

		// change
		assert newOrderNumber == eo.getAttribute('orderNumber')

		eo.save()

		assert newOrderNumber == eo.getAttribute('orderNumber')

	}

	String goDocumentBoMetaId = 'org_nuclos_businessentity_nuclosgeneralsearchdocument_genericObject'
	String tabGoDocument = "Documents"

	String position = 'example_rest_OrderPosition_order';
	String tabPosition = "Position"

	void checkForEnabled(EntityObjectComponent eo, boolean enabled) {

		assert eo.getNewButton() != null

		//Delete and Next State Button
		if (enabled) {
			assert eo.getDeleteButton() != null
			assert eo.hasNextStateButton()

		} else {
			assert eo.getDeleteButton() == null
			assert !eo.hasNextStateButton()

		}

		eo.checkField('nuclosProcess', true, enabled)
		eo.checkField('customer', true, enabled)

		eo.checkField('orderDate', true, enabled)
		eo.checkField('discount', true, enabled)

		eo.checkField('confirmation', true, enabled) // Dokument
		eo.checkField('note', true, enabled)
		eo.checkField('approvedcreditlimit', true, enabled)

		assert eo.hasTab(tabPosition)
		assert eo.hasTab(tabGoDocument)

		Subform subform = eo.getSubform(position)
		assert subform != null

		assert subform.newVisible == enabled
		assert subform.cloneVisible == enabled
		assert subform.deleteVisible == enabled

		Subform.Row row = subform.getRow(0)

		assert !row.isDirty()

		String newArticle = '1001'
		def oldArticle = row.getValue('article')
		try {
			row.enterValue('article', newArticle)

			assert enabled

		} catch (Exception) {
			assert !enabled
		}


		//FIXME The row shouldn't be a new one
		row = subform.getRow(0)

		def oldQuantity = row.getValue('quantity')
		row.enterValue('quantity', '99')

		//FIXME The row shouldn't be a new one
		row = subform.getRow(0)

		screenshot('changedQuantity')

		if (enabled) {
			//cancel and save button
			assert eo.getCancelButton() != null
			assert eo.getSaveButton() != null

			// FIXME: Dirty does not work since fix of NUCLOS-5617
			// assert row.isDirty()
			assert row.getValue('article') == newArticle

			String val = row.getValue('quantity')
			//FIXME Should be formatted
			assert val == '99,00' || val == '99.00' || val == '99'

			eo.getTab(tabGoDocument).click()
			screenshot('changedToDocumentTab')
			assert eo.getSubform(goDocumentBoMetaId) != null

			eo.getTab(tabPosition).click()
			screenshot('changedBackToPositionTab')

			subform = eo.getSubform(position);
			assert subform.newVisible == enabled

			row = subform.getRow(0)

			screenshot('gotFirstRowFromPositionTab')

			// FIXME: Dirty does not work since fix of NUCLOS-5617
			// assert row.isDirty()
			assert row.getValue('article') == newArticle

			val = row.getValue('quantity')
			//FIXME Should be formatted
			assert val == '99,00' || val == '99.00' || val == '99'

			eo.cancel()
		}

		row = subform.getRow(0)

		assert !row.isDirty()

		assert row.getValue('article') == oldArticle
		assert row.getValue('quantity') == oldQuantity

	}

}
