package org.nuclos.test.webclient.chart

import org.junit.FixMethodOrder
import org.junit.Ignore
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclientTest

import groovy.json.JsonOutput

/**
 * testing charts by using subform data
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Ignore('TODO: Adjust for Webclient2!')
// TODO: @CompileStatic
class ChartTest extends AbstractWebclientTest {
	def companyBoMetaId = 'nuclet_test_charts_Company'

	def company =
		[
			boMetaId: 'nuclet_test_charts_Company',
			attributes: [
				'name': 'Company 123'
			],
			'subBos': [
				insert: [
				nuclet_test_charts_FinancialFigures_company: []
				]
			]
		]
		
		
	def deepcopy(orig) {
		def bos = new ByteArrayOutputStream()
		def oos = new ObjectOutputStream(bos)
		oos.writeObject(orig); oos.flush()
		def bin = new ByteArrayInputStream(bos.toByteArray())
		def ois = new ObjectInputStream(bin)
		return ois.readObject()
   }

	@Test
	void _01createTestData() {
		def sessionId = RESTHelper.login('nuclos', '')
		
		def subBo = [
			_flag: 'insert',
			boId: null,
			boMetaId: 'nuclet_test_charts_FinancialFigures',
			attributes: [
				umsatzjahr: '2006',
				gewinn: 5000,
				umsatz: 10000
			]
		]
		
		def 
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2006', umsatz: '10000', gewinn:  '5000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2007', umsatz: '12000', gewinn:  '7000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2008', umsatz: '15000', gewinn:  '7000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2009', umsatz: '17000', gewinn:  '9000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2010', umsatz: '18000', gewinn: '10000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2011', umsatz: '20000', gewinn: '11000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2012', umsatz: '21000', gewinn: '10000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2013', umsatz: '18000', gewinn: '16000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2014', umsatz: '22000', gewinn: '17000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		subBoTemp = deepcopy(subBo); subBoTemp.attributes = [umsatzjahr: '2015', umsatz: '24000', gewinn: '15000']; company.subBos.insert.nuclet_test_charts_FinancialFigures_company.add(subBoTemp)
		
		RESTHelper.createBo(company, sessionId)
		
		println "company: " + JsonOutput.toJson(company)
	}
	
	
	String chartOptionsString = 
	"""
	{
	  "name": {
	    "de": "1. Umsatzanalyse",
	    "en": "sales analysis"
	  }, 

	  "chart": {
	    "type": "multiBarChart",
	    "width": null,
	    "height": null,
	    "margin": {
	      "left": 100,
	      "top": 25,
	      "bottom": 40,
	      "right": 0
	    },
	    "primaryChart": {
	      "boMetaId": "nuclet_test_charts_FinancialFigures",
	      "refBoAttrId": "nuclet_test_charts_FinancialFigures_company",
	      "categoryBoAttrId": "nuclet_test_charts_FinancialFigures_umsatzjahr",
	      "categoryLabel": {
	        "de": "Jahr"
	      },
	      "scaleLabel": {
	        "de": "T Euro"
	      },
	      "series": [
	        {
	          "boAttrId": "nuclet_test_charts_FinancialFigures_umsatz",
	          "label": {
	            "de": "Umsatz"
	          }
	        },
	        {
	          "boAttrId": "nuclet_test_charts_FinancialFigures_gewinn",
	          "label": {
	            "de": "Gewinn"
	          }
	        }
	      ]
	    },
	    "yAxis": {
	      "tickFormat": "function (value) {return value +' T Euro';}"
	    },
	    "transitionDuration": 250,

	    "x": "function (dataSet) { return dataSet.categoryValue; }",
	    "y": "function (dataSet) { return dataSet.seriesValue; }"
	  },
	  "chartIndex": 0
	}

	"""
	
	
	@Test
	void _02createChart() {
		
		Sideview.openSideview(companyBoMetaId)
		Sideview.openFirstBoMenuEntry()
		
		$('#morefunctions button').click()
		$('#open-charts-button').click()
		
		assert $$('#chart-selection .chart-selection-item').size() == 0
		
		$('#new-chart').click()
		waitForAngularRequestsToFinish()

		$('#chart-options-tab-extended').click()
		$('#chartOptionsString').clear()

		// $('#chartOptionsString').sendKeys(chartOptionsString) // doesn't work
		getDriver().executeScript("arguments[0].value = arguments[1];", $('#chartOptionsString'), chartOptionsString);

		$('#chartOptionsString').sendKeys(" ") // trigger angular 2 way-data-binding
		
		
		$('#save-chart-button').click()
		
		closeModal()
		
		// open chart modal again
		$('#morefunctions button').click()
		$('#open-charts-button').click()
		
		waitForAngularRequestsToFinish()
		
		screenshot('first-chart')
		
		assert $$('#chart-selection .chart-selection-item').size() == 1
		
		assert $$('.modal-dialog svg .nv-multiBarWithLegend').size() == 1
		
	}

	@Test
	void _03createChart() {
		
		$('#new-chart').click()
		$('#chart-options-tab-extended').click()
		$('#chartOptionsString').clear()

		getDriver().executeScript("arguments[0].value = arguments[1];", $('#chartOptionsString'), chartOptionsString);
		$('#chartOptionsString').sendKeys(" ") // trigger angular 2 way-data-binding

		
		$('#chart-options-tab-main').click()
		
		$('#chart-label').clear()
		$('#chart-label').sendKeys("2. Umsatzkuchen")
		
		$('.optionpanel-charttype #pieChart').click()
		
		
		$('#save-chart-button').click();
		
		closeModal()
		
		// open chart modal again
		$('#morefunctions button').click()
		$('#open-charts-button').click()
		
		$('#chart-selection li:nth-child(2)').click()
		
		waitForAngularRequestsToFinish()

		screenshot('second-chart')
		
		assert $$('#chart-selection .chart-selection-item').size() == 2
		
		assert $$('.modal-dialog svg .nv-pieChart').size() == 1
		
	}

}

