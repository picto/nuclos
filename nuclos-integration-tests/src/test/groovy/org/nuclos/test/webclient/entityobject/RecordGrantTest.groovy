package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.MessageModal

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class RecordGrantTest extends AbstractWebclient2Test {
	String wordBoMetaId = TestEntities.EXAMPLE_REST_WORD.fqn
	Map<String, ?> word1 = [text: 'Haus', times: 3, agent: 'test']
	Map<String, ?> word2 = [text: 'Pferd', times: 2, agent: 'nuclos']
	Map<String, ?> word3 = [text: 'Pferd', times: 2, agent: 'test']
	Map<String, ?> word4 = [text: 'Esel', times: 2, agent: 'test']

	@Test
	void _05_openMenuEntry() {

		EntityObjectComponent.open(TestEntities.EXAMPLE_REST_WORD)

		assert $('#logout')
	}

	@Test
	void _10_testRecordGrantDenied() {

		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word1)

		assert eo.listEntryCount == 1

		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word2)

		MessageModal modal = getMessageModal()
		assert modal != null
		// TODO: Check modal contents
		modal.confirm()

		//After refresh only one data-set should be visible
		EntityObjectComponent.refresh()

		assert eo.listEntryCount == 1
		eo.cancel()

		logout()
	}

	@Test
	void _15_testUniquenessViolation() {
		login('nuclos')
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word2)

		assert eo.listEntryCount == 2 // superuser = no record grants restriction

		logout()
		login('test', 'test')

		assert eo.listEntryCount == 1

		EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word3)

		MessageModal modal = getMessageModal()
		assert modal != null
		assert modal.message.contains("Eindeutigkeitsverletzung") || modal.message.contains("Uniqueness violation")

		modal.confirm()

		eo.cancel()

		assert eo.listEntryCount == 1
	}

	@Test
	void _20_testCreationSuccessful() {
		EntityObjectComponent eo = EntityObjectComponent.createEO(TestEntities.EXAMPLE_REST_WORD, word4)

		assert eo.listEntryCount == 2
	}

	/**
	 * See NUCLOS-6041
	 */
	@Test
	void _25_testReadNotAllowed() {
		Map word = [
				boMetaId  : wordBoMetaId,
				attributes: [
						text : 'Test read',
						times: 1,
						agent: 'nuclos'
				]
		]
		Map bo = RESTHelper.createBo(word, nuclosSession)

		// Load the full URL to completely refresh the page
		String url = nuclosWebclient2BaseURL + '#/view/' + wordBoMetaId + '/' + bo['boId']
		getUrl(url)

		// Assert that only the error page is visible and no EO auto-selection happened
		waitForAngularRequestsToFinish()
		assertAccessDenied()
	}
}
