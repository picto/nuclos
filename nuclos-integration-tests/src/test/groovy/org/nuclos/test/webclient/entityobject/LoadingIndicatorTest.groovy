package org.nuclos.test.webclient.entityobject

import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.pageobjects.LayoutComponent
import org.nuclos.test.webclient.pageobjects.LoadingIndicatorComponent

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class LoadingIndicatorTest extends AbstractWebclient2Test {

	@Test()
	void _00_testNotLoadingInitially() {
		assert !LoadingIndicatorComponent.loading
	}

	@Test
	void _05_testLoading() {
		EntityObjectComponent eo = EntityObjectComponent.open(TestEntities.NUCLET_TEST_OTHER_TESTLOADINGINDICATOR)
		eo.addNew()
		eo.setAttribute('name', 'Test')
		eo.save()

		assert !LoadingIndicatorComponent.loading
		waitFor(1) {
			!LayoutComponent.busy
		}

		// Do not wait for Angular to finish here, because we must check the loading indicator in the meantime
		eo.getButton('Sleep 5 seconds').clickWithoutWait()

		assert !LoadingIndicatorComponent.loading, 'Loading indicator should not be spinning immediately'

		sleep(1500)
		assert LayoutComponent.busy
		assert LoadingIndicatorComponent.loading, 'Loading indicator should start spinning after 1 second'

		// Loading indicator should stop spinning after the rule execution is finished (5 seconds)
		waitFor(10) {
			!LoadingIndicatorComponent.loading && !LayoutComponent.busy
		}
	}
}
