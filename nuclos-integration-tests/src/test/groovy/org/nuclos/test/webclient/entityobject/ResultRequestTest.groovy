package org.nuclos.test.webclient.entityobject

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.IntegrationTest
import org.nuclos.test.TestEntities
import org.nuclos.test.rest.TestDataHelper
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.pageobjects.EntityObjectComponent
import org.nuclos.test.webclient.util.RequestCounts

import groovy.transform.CompileStatic

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
class ResultRequestTest extends AbstractWebclient2Test {

	@BeforeClass
	static void setup() {
		startProxy()
		setup(true, true)
	}

	@Test
	void _00_setup() {
		TestDataHelper.insertTestData(nuclosSession)
	}

	@Test
	void _10_testOpeningEntityClass() {
		RequestCounts requests = countRequests {
			EntityObjectComponent.open(TestEntities.EXAMPLE_REST_ORDER)
		}

		// There should be exactly 1 request for the result list
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_LIST_REQUEST) == 1

		// There should be exactly 1 request when the first result is auto-selected
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_REQUEST) == 1
	}
}
