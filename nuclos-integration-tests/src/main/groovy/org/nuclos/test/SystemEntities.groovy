package org.nuclos.test

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum SystemEntities implements EntityClass<String> {
	PARAMETER,
	USER

	@Override
	String getFqn() {
		return 'org_nuclos_businessentity_nuclos' + name().toLowerCase()
	}
}
