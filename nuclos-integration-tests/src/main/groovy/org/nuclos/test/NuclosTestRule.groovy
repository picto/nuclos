package org.nuclos.test

import java.text.DecimalFormat

import org.junit.rules.ExternalResource
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.nuclos.test.log.Log

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class NuclosTestRule implements TestRule {
	private final TestCasePrinter printer = new TestCasePrinter();

	private String beforeContent = null
	private String afterContent = null
	private long timeStart
	private long timeEnd

	@Override
	Statement apply(final Statement statement, final Description description) {
		beforeContent = "[TEST START] $description.methodName" // description.getClassName() to get class name
		afterContent = "[TEST ENDED] "
		return printer.apply(statement, description)
	}

	private class TestCasePrinter extends ExternalResource {
		@Override
		protected void before() throws Throwable {
			timeStart = System.currentTimeMillis()
			Log.info(beforeContent)
		};


		@Override
		protected void after() {
			try {
				timeEnd = System.currentTimeMillis();
				double seconds = (timeEnd - timeStart) / 1000.0;
				Log.info(afterContent + "Time elapsed: " + new DecimalFormat("0.000").format(seconds) + " sec")
			} catch (IOException ioe) { /* ignore */
			}
		}
	}
}
