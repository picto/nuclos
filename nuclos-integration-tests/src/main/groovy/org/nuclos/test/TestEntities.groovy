package org.nuclos.test

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
enum TestEntities implements EntityClass<Long> {
	EXAMPLE_REST_CUSTOMER('example_rest_Customer'),
	EXAMPLE_REST_CUSTOMERADDRESS('example_rest_CustomerAddress'),
	EXAMPLE_REST_ORDER('example_rest_Order'),
	EXAMPLE_REST_ORDERPOSITION('example_rest_OrderPosition'),
	EXAMPLE_REST_ARTICLE('example_rest_Article'),
	EXAMPLE_REST_CATEGORY('example_rest_Category'),

	EXAMPLE_REST_WORD('example_rest_Word'),
	EXAMPLE_REST_WRITEPROXYDATA('example_rest_WriteProxyData'),
	EXAMPLE_REST_WRITEPROXYTEST('example_rest_WriteProxyTest'),

	EXAMPLE_REST_AUFTRAG('example_rest_Auftrag'),
	EXAMPLE_REST_AUFTRAGSPOSITION('example_rest_Auftragsposition'),
	EXAMPLE_REST_RECHNUNG('example_rest_Rechnung'),

	EXAMPLE_REST_WORLD('example_rest_World'),
	EXAMPLE_REST_COUNTRY('example_rest_Country'),
	EXAMPLE_REST_CITY('example_rest_City'),

	NUCLET_TEST_OTHER_TESTPROXYCONTAINER('nuclet_test_other_TestProxyContainer'),
	NUCLET_TEST_OTHER_TESTVALIDATION('nuclet_test_other_TestValidation'),
	NUCLET_TEST_OTHER_TESTSUBFORMBUTTONS('nuclet_test_other_TestSubformButtons'),
	NUCLET_TEST_OTHER_TESTDROPDOWNS('nuclet_test_other_TestDropdowns'),
	NUCLET_TEST_OTHER_TESTLAYOUTCOMPONENTS('nuclet_test_other_TestLayoutComponents'),
	NUCLET_TEST_OTHER_TESTSUBFORMIMAGES('nuclet_test_other_TestSubformImages'),
	NUCLET_TEST_OTHER_TESTLAYOUTRULES('nuclet_test_other_TestLayoutRules'),
	NUCLET_TEST_OTHER_TESTLOADINGINDICATOR('nuclet_test_other_TestLoadingIndicator'),
	NUCLET_TEST_OTHER_TESTSTATECHANGE('nuclet_test_other_TestStateChange'),
	NUCLET_TEST_OTHER_TESTHYPERLINK('nuclet_test_other_TestHyperlink'),
	NUCLET_TEST_OTHER_TESTOBJEKTGENERATOR('nuclet_test_other_TestObjektgenerator'),
	NUCLET_TEST_OTHER_LAGERBUCHUNG('nuclet_test_other_Lagerbuchung'),
	NUCLET_TEST_OTHER_TESTAUTONUMBER('nuclet_test_other_TestAutonumber'),
	NUCLET_TEST_OTHER_TESTAUTONUMBERSUBFORM('nuclet_test_other_TestAutonumberSubform'),
	NUCLET_TEST_OTHER_SQLINJECTIONTEST('nuclet_test_other_SQLInjectionTest'),
	NUCLET_TEST_OTHER_SQLSNIPPET('nuclet_test_other_SQLSnippet'),

	NUCLET_TEST_RULES_TESTAPI('nuclet_test_rules_TestAPI'),
	NUCLET_TEST_RULES_TESTMULTIREF('nuclet_test_rules_TestMultiref'),
	NUCLET_TEST_RULES_TESTMULTIREFDEPENDENT('nuclet_test_rules_TestMultirefDependent'),
	NUCLET_TEST_RULES_TESTRULES('nuclet_test_rules_TestRules'),

	NUCLET_TEST_TABINDEX_TESTTABINDEX('nuclet_test_tabindex_TestTabindex'),

	NUCLET_TEST_MATRIX_MATRIX('nuclet_test_matrix_Matrix'),


	final String fqn

	private TestEntities(String fqn) {
		this.fqn = fqn
	}

	String toString() {
		fqn
	}
}
