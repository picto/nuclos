package org.nuclos.test.log

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class CallTrace {

	synchronized static int depth = 0

	/**
	 * TODO: This is not thread-safe.
	 */
	static <T> T trace(String name, Closure<T> c) {
		int currentDepth = ++depth

		long millis = System.currentTimeMillis()

		log 'BEGIN', name, currentDepth

		def result = null
		try {
			result = c()
		} catch (Exception ex) {
			// Ignore
		}

		long duration = System.currentTimeMillis() - millis
		log 'END', "$name ($duration ms)", currentDepth

		--depth

		return result
	}

	static log(String status, String message, int depth) {
		String indent = '\t' * depth
		Log.info "[$status]$indent$message"
	}
}
