package org.nuclos.test

import java.text.SimpleDateFormat

import org.nuclos.api.businessobject.Flag
import org.nuclos.test.rest.RESTClient

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonSerialize

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class EntityObject<PK> {
	PK boId

	int version = 0
	String executeCustomRule

	Map<String, Object> attributes = new HashMap<>()

	@JsonIgnore
	Flag flag = Flag.INSERT

	@JsonIgnore
	final EntityClass<PK> entityClass

	@JsonProperty('subBos')
	@JsonSerialize(using = EntityObjectDependentsSerializer)
	Map<String, List<EntityObject<?>>> dependents = new HashMap<>()

	/**
	 * The RESTClient by which this EO was (re-)loaded.
	 */
	@JsonIgnore
	RESTClient client

	EntityObject(EntityClass<PK> entityClass) {
		this.entityClass = entityClass
	}

	PK getId() {
		return boId
	}

	String toJson() {
		final ObjectMapper result = new ObjectMapper()
		result.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
		result.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true)
		result.writeValueAsString(this)
	}

	void updateFromJson(Map json) {
		boId = json['boId'] as PK
		attributes = json['attributes'] as Map
		version = json['version'] as Integer
	}

	@JsonIgnore
	boolean isNew() {
		return !id
	}

	@JsonIgnore
	boolean isUpdate() {
		return id && flag == Flag.UPDATE
	}

	@JsonIgnore
	boolean isDelete() {
		return id && flag == Flag.DELETE
	}

	Object getAttribute(String attributeName) {
		this.attributes.get(attributeName)
	}

	void setAttribute(String attributeName, Object value) {
		if (value instanceof Date) {
			value = new SimpleDateFormat('yyyy-MM-dd').format(value)
		}

		this.attributes.put(attributeName, value)

		flag = this.new ? Flag.INSERT : Flag.UPDATE
	}

	String getBoMetaId() {
		entityClass.fqn
	}

	@JsonIgnore
	public <SubPK> List<EntityObject<SubPK>> getDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		if (!this.dependents.containsKey(referenceAttributeFqn)) {
			this.dependents.put(referenceAttributeFqn, [])
		}

		return this.dependents.get(referenceAttributeFqn) as List<EntityObject<SubPK>>
	}

	public <SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityClass<SubPK> subEntityClass,
			String referenceAttributeName
	) {
		String referenceAttributeFqn = subEntityClass.fqn + '_' + referenceAttributeName

		List<EntityObject<SubPK>> dependents = client.loadDependents(
				this,
				subEntityClass,
				referenceAttributeFqn
		)

		this.dependents.put(referenceAttributeFqn, dependents as List<EntityObject<?>>)

		return dependents
	}

	void save() {
		checkClient()
		client.save(this)
	}

	private void checkClient() {
		if (!client) {
			throw new IllegalStateException('This EO is not associated with any REST client.')
		}
	}

	void delete() {
		checkClient()
		client.delete(this)
	}

	void flagDeleted() {
		this.flag = Flag.DELETE
	}

	void executeCustomRule(String ruleName) {
		checkClient()
		client.executeCustomRule(this, ruleName)
	}

	void changeState(int statusNumeral) {
		checkClient()
		client.changeState(this, statusNumeral)
	}
}
