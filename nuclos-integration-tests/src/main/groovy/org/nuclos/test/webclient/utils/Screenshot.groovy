package org.nuclos.test.webclient.utils

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot

public class Screenshot {
	static void take(String caller, String name) {
		// don't call waitForAngularRequestsToFinish if called from waitForAngularRequestsToFinish - prevent StackOverflow
		if (!Utils.isCalledFrom('waitForAngularRequestsToFinish')) {
			AbstractWebclientTest.waitForAngularRequestsToFinish()
		}

		File scrFile = ((TakesScreenshot) AbstractWebclientTest.getDriver()).getScreenshotAs(OutputType.FILE)
		String fileName = Utils.formatDate(new Date()) + " - ${name}.png"

		String targetPath = 'target/screenshots/current/'
		if (System.getProperty('basedir')) {
			targetPath = System.getProperty('basedir') + '/' + targetPath
		}

		File targetBrowserDir = new File(targetPath, AbstractWebclientTest.browser.toString().toLowerCase())
		File targetDir = new File(targetBrowserDir, caller)
		Utils.mkdirP(targetDir)
		File targetFile = new File(targetDir, fileName)

		scrFile.withDataInputStream { targetFile << it }
	}
}