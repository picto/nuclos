package org.nuclos.test.webclient.util

import org.nuclos.test.webclient.utils.Utils
import org.nuclos.test.webclient.AbstractWebclient2Test
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot

import groovy.transform.CompileStatic

@CompileStatic
class Screenshot {
	static void take(String caller, String name) {
		// don't call waitForAngularRequestsToFinish if called from waitForAngularRequestsToFinish - prevent StackOverflow
		if (!Utils.isCalledFrom('waitForAngularRequestsToFinish')) {
			AbstractWebclient2Test.waitForAngularRequestsToFinish()
		}

		File scrFile = ((TakesScreenshot) AbstractWebclient2Test.getDriver()).getScreenshotAs(OutputType.FILE)
		String fileName = Utils.formatDate(new Date()) + " - ${name}.png"

		String targetPath = 'target/screenshots/current/'
		if (System.getProperty('basedir')) {
			targetPath = System.getProperty('basedir') + '/' + targetPath
		}

		File targetBrowserDir = new File(targetPath, AbstractWebclient2Test.browser.toString().toLowerCase())
		File targetDir = new File(targetBrowserDir, caller)
		Utils.mkdirP(targetDir)
		File targetFile = new File(targetDir, fileName)

		scrFile.withDataInputStream { targetFile << it }
	}
}