package org.nuclos.test.webclient.pageobjects.viewconfiguration

import static org.nuclos.test.webclient.AbstractWebclient2Test.$
import static org.nuclos.test.webclient.AbstractWebclient2Test.$$
import static org.nuclos.test.webclient.AbstractWebclient2Test.waitFor

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SearchtemplateConfiguration extends ViewConfigurationModal {
	static void selectSearchAttribute(String attributeFqn) {
		findUnselectedSearchAttribute(attributeFqn).click()
		selectMarkedSearchAttributes()
	}

	static NuclosWebElement findSelectedSearchAttribute(String attributeFqn) {
		$('#sideviewmenu-columns-config .select-box-container-selected [attr-fqn="' + attributeFqn + '"]')
	}

	static NuclosWebElement findUnselectedSearchAttribute(String attributeFqn) {
		$('#searchtemplate-columns-config [attr-fqn="' + attributeFqn + '"]')
	}

	static void selectMarkedSearchAttributes() {
		$('#searchtemplate-columns-config #btn-selected-marked').click()
	}

	static def newSearchtemplate(final String name) {
		openSearchtemplateConfigurationPanel()
		$('#new-searchtemplate').click()
		waitFor {
			NuclosWebElement input = $('#searchtemplate-name-input')
			if (input) {
				input.sendKeys(name)
				return true
			}
		}
		waitFor{
			NuclosWebElement element = $('#save-searchtemplate')
			if (element) {
				element.click()
				return true
			}
		}
	}

	static void openSearchtemplateConfigurationPanel() {
		open()
		clickIfCollapsed('#searchtemplate-preferences-header')
	}

	static void selectSearchtemplateConfiguration(final String name) {
		openSearchtemplateConfigurationPanel()
		$$('#searchtemplate-selector option').find { it.text?.trim() == name }.click()
		clickButtonOk()
	}

}
