package org.nuclos.test.webclient

import org.junit.runner.Description
import org.junit.runner.notification.Failure
import org.junit.runner.notification.RunListener

import groovy.transform.CompileStatic

/**
 * Listens for failed tests and calls the FailureHandler.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FailFastListener extends RunListener {

	void testFailure(Failure failure) throws Exception {
		try {
			FailureHandler.fail(failure.toString(), failure.exception)
		} finally {
			System.exit(1)
		}
	}

	@Override
	void testFinished(Description description) throws Exception {
	}
}
