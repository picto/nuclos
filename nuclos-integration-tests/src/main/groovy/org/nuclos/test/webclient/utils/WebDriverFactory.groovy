package org.nuclos.test.webclient.utils

import java.util.logging.Level

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.Proxy

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class WebDriverFactory {

	static RemoteWebDriver remoteWebDriver(DesiredCapabilities caps, Proxy proxy) {
		// Enable browser console logging
		LoggingPreferences logPrefs = new LoggingPreferences()
		logPrefs.enable(LogType.BROWSER, Level.ALL)
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs)
		caps.setCapability(CapabilityType.PROXY, proxy)

		return new RemoteWebDriver(new URL(AbstractWebclientTest.seleniumServer), caps)
	}

	static Closure remoteFirefox = { Proxy proxy ->
		DesiredCapabilities caps = DesiredCapabilities.firefox()
		remoteWebDriver(caps, proxy)
	}

	static Closure remoteChrome = { Proxy proxy ->
		DesiredCapabilities caps = DesiredCapabilities.chrome()
		remoteWebDriver(caps, proxy)
	}

	static Closure remotePhantomJS = { Proxy proxy ->
		DesiredCapabilities caps = DesiredCapabilities.phantomjs()
		remoteWebDriver(caps, proxy)
	}

	static Closure remoteIE = { Proxy proxy ->
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer()
		remoteWebDriver(caps, proxy)
	}

	static Closure remoteSafari = { Proxy proxy ->
		DesiredCapabilities caps = DesiredCapabilities.safari()
		remoteWebDriver(caps, proxy)
	}
}
