package org.nuclos.test.webclient

import java.text.NumberFormat
import java.util.concurrent.TimeUnit

import org.codehaus.groovy.reflection.ReflectionUtils
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.webclient.utils.Utils
import org.nuclos.test.webclient.pageobjects.AuthenticationComponent
import org.nuclos.test.webclient.pageobjects.LocaleChooser
import org.nuclos.test.webclient.pageobjects.MessageModal
import org.nuclos.test.webclient.util.RequestCounts
import org.nuclos.test.webclient.util.Screenshot
import org.openqa.selenium.By
import org.openqa.selenium.NoSuchWindowException
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic
import net.lightbody.bmp.BrowserMobProxyServer
import net.lightbody.bmp.client.ClientUtil
import net.lightbody.bmp.core.har.Har

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractWebclient2Test extends AbstractWebclientTest {

	static String nuclosWebclient2Protocol = initFromSystemProperty('nuclos.webclient2.protocol', 'http')
	static String nuclosWebclient2Host = initFromSystemProperty('nuclos.webclient2.host', nuclosWebclientHost)
	static String nuclosWebclient2Port = initFromSystemProperty('nuclos.webclient2.port', '4200')

	static String nuclosWebclient2Server = "$nuclosWebclient2Protocol://$nuclosWebclient2Host:$nuclosWebclient2Port"
	static String nuclosWebclient2BaseURL = "$nuclosWebclient2Server/index.html"
	static String jscoverageClearStorageUrl = "$nuclosWebclient2Server/jscoverage-clear-local-storage.html"

	static boolean takeScreenshots = initFromSystemProperty('nuclos.webclient2.screenshots', false)

	static BrowserMobProxyServer browserMobProxy

	/**
	 * Callback script for Selenium that waits for the Angular 2 app to become stable
	 * and have no pending macro tasks (e.g. asynchronous requests).
	 */
	final static String WAIT_FOR_ANGULAR_SCRIPT = '''
// NgZone may not be available yet, we have to retry later
if (!window['NgZone']) {
	console.error('No NgZone!');
	return false;
}

if (NgZone.isStable && !NgZone.hasPendingMacrotasks){
	return true;
} else {
	console.log('waitForAngular: has macro tasks');
	return false;
}
'''

	@BeforeClass
	static void setup() {
		setup(true, true)
	}

	static void setup(boolean withTestUserLogin, boolean setCookieAccepted) {
		Log.debug 'Setup'

		AbstractNuclosTest.setup()

		try {
			setupDriver:
			{
				Proxy proxy = null
				if (browserMobProxy) {
					InetAddress proxyHost = InetAddress.getByName(nuclosWebclient2Host)
					proxy = ClientUtil.createSeleniumProxy(browserMobProxy, proxyHost)
				}
				driver = browser.createDriver(proxy)
				driver.setFileDetector(new LocalFileDetector())
				driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS)
			}

			resizeBrowserWindow()
			getUrl(jscoverageClearStorageUrl)
			openStartpage()
			waitForAngularRequestsToFinish()

			// clear localStorage - needed for phantomjs
			executeScript('localStorage.clear();');

			initialSetup(nuclosSession)

			if (setCookieAccepted) {
				WebElement we = $('#acceptcookies')
				if (we != null && we.isDisplayed()) {
					we.click()
				}
			}

			nuclosSession.managementConsole('resetCompleteMandatorSetup')

			if (withTestUserLogin) {
				RESTHelper.createUser('test', 'test', ['Example user'], nuclosSession)
				login('test', 'test')
			}

			if (setCookieAccepted) {
				assert $('#acceptcookies') == null
			}

		}

		catch (Throwable ex) {
			fail(ex.message, ex)
		}

		Log.debug 'Setup done'
	}

	@AfterClass
	static void teardown() {
		waitForAngularRequestsToFinish()
		try {
			executeScript("jscoverage_report();")
			executeAsyncScript('var callback = arguments[arguments.length - 1];callback();')
		}
		catch (Exception ex) {
			Log.warn "Could not create jscoverage report"
		}

//		printBrowserLog()
//		printCookies()

		shutdown()
	}

	static void startProxy() {
		InetAddress host = InetAddress.getByAddress([0, 0, 0, 0] as byte[])
		browserMobProxy = new BrowserMobProxyServer()
		browserMobProxy.start(0, host)
		int port = browserMobProxy.getPort()

		println "Proxy server started on port: $port"
	}

	static RequestCounts countRequests(Closure c) {
		browserMobProxy.newHar()
		c()
		Har har = browserMobProxy.getHar()
		new RequestCounts(har)
	}

	static NuclosWebElement $(String selector) {
		Log.info("Selecting: \"$selector\"")
		waitForAngularRequestsToFinish()
		List<WebElement> elements = getDriver().findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static NuclosWebElement $(WebElement e, String selector) {
		List<WebElement> elements = e.findElements(By.cssSelector(selector))
		elements.empty ? null : new NuclosWebElement(elements.get(0))
	}

	static List<NuclosWebElement> $$(String selector) {
		Log.info("Selecting: \"$selector\"")
		getDriver().findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static List<NuclosWebElement> $$(WebElement e, String selector) {
		e.findElements(By.cssSelector(selector)).collect { new NuclosWebElement(it) }
	}

	static void openStartpage() {
		getUrl(nuclosWebclient2BaseURL)
	}

	static boolean login(String username = 'nuclos', boolean autologin = false) {
		return login(username, '', autologin)
	}

	/**
	 * Performs a Login with the given credentials and the current locale, if not logged in already.
	 *
	 * @param username
	 * @param password
	 * @param autologin
	 * @param mandator
	 * @return
	 */
	static boolean login(String username, String password, boolean autologin = false, String mandator = null) {
		if (isPresent('#logout')) {
			return false
		}

		loginUnsafe(username, password, autologin, mandator)

		assert $('#logout')
		return true
	}

	static boolean isLoggedIn() {
		isPresent('#logout')
	}

	/**
	 * Performs a Login with the given credentials and the current locale.
	 * Without any checks for login success etc.
	 *
	 * @param username
	 * @param password
	 * @param autologin
	 * @param mandator
	 * @return
	 */
	static boolean loginUnsafe(String username, String password, boolean autologin = false, String mandator = null) {
		LocaleChooser.locale = locale

		AuthenticationComponent.username = username
		AuthenticationComponent.password = password
		AuthenticationComponent.autologin = autologin

		AuthenticationComponent.submit()

		if (mandator) {
			if (mandator == 'null') {
				// superuser login without any mandator selection
				$('#mandatorless-session').click()
			} else {
				WebElement chooseMandator = $('#choose-mandator')
				Select droplist = new Select(chooseMandator);
				droplist.selectByVisibleText(mandator);
				$('#choose-mandator-button').click()
			}
		}
	}


	/**
	 * Does not fail the build, if "waitForAngular" did not succeed.
	 */
	static void tryToWaitForAngular(int timeout) {
		Closure waitClosure = {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
		if (!doWaitFor(timeout, waitClosure)) {
			Log.warn("Failed to wait for angular requests to finish")
		}
	}

	static void waitForAngularRequestsToFinish() {
		waitForAngularRequestsToFinish(30)
	}

	/**
	 * Waits for Angular-2 to finish loading by waiting for the "onStable" event of NgZone (if not already stable).
	 * NgZone is set as a global Javascript variable by the Webclient2 app component.
	 *
	 * @param timeout
	 */
	static void waitForAngularRequestsToFinish(int timeout) {
		Log.debug "Waiting for Angular requests to finish"
		waitForOrReload(timeout) {
			windowClosed || executeScript(WAIT_FOR_ANGULAR_SCRIPT)
		}
	}

	static boolean waitForOrReload(int timeout, Closure condition) {
		if (!doWaitFor(timeout, condition)) {
			Log.warn("Failed to wait for condition, refreshing and retrying...")
			getUrl(driver.currentUrl)
			return waitFor(timeout, condition)
		}
	}

	/**
	 * Waits for the given Closure to return true.
	 * Fails the build if the condition is not met within the given timeout.
	 *
	 * @param condition
	 * @param timeout
	 * @return
	 */
	static boolean waitFor(int timeoutInSeconds, Closure condition) {
		if (!doWaitFor(timeoutInSeconds, condition)) {
			String source = Utils.closureToString(condition)
			fail('Failed to wait for condition: ' + source, new Throwable(source))
			return false
		}

		return true
	}

	static boolean waitFor(Closure condition) {
		waitFor(30, condition)
	}

	/**
	 * Waits for a matching element for the given selector to appear.
	 *
	 * @param selector
	 * @return
	 */
	static NuclosWebElement waitForElement(String selector) {
		NuclosWebElement result = null
		waitFor {
			result = $(selector)
			result?.displayed && result?.enabled
		}
		return result
	}

	static String getCurrentUrl() {
		try {
			waitForAngularRequestsToFinish()
			return driver.getCurrentUrl()
		}
		catch (Throwable ex) {
			Log.error 'Could not get current URL', ex
		}

		return null
	}

	/**
	 * Takes a screenshot and saves it under the given name,
	 * if screenshots are enabled or "force" is true.
	 *
	 * @param name
	 * @param force
	 */
	static void screenshot(String name, boolean force = false) {
		if (!takeScreenshots && !force) {
			Log.debug("Skipping screenshot '$name'")
			return
		}

		waitForAngularRequestsToFinish()
		String caller = ReflectionUtils.callingClass.simpleName
		// get caller - ignore superclass in stacktrace
		for (def l = 0; l < 10; l++) {
			def callerName = ReflectionUtils.getCallingClass(l).name
			if (callerName.indexOf("AbstractWebclient") == -1) {
				caller = callerName
				break
			}
		}
		Screenshot.take(caller, name)
	}

	/**
	 * Forcefully fails the build immediately.
	 *
	 * @param failure
	 * @param t
	 */
	static void fail(String failure, Throwable t = null) {
		FailureHandler.fail(failure, t)
	}

	static boolean logout() {
		if (!isPresent('#logout')) {
			return false
		}

		waitForAngularRequestsToFinish()

		$('#user-menu .dropdown').click()
		$('#logout').click()

		waitForAngularRequestsToFinish()

		assert !$('#logout')

		return true
	}

	static boolean isPresent(String selector) {
		try {
			def element = $(selector)
			return !!element
		}
		catch (Exception ex) {
			return false
		}
	}

	/**
	 * Reloads the current page in the browser.
	 */
	static void refresh() {
		Log.info "Refreshing: $currentUrl"

		// getDriver().navigate().refresh() does not always work with PhantomJS
		if (getBrowser() != AbstractWebclientTest.Browser.PHANTOMJS) {
			driver.navigate().refresh()
		} else {
			getUrl(currentUrl)
		}

		waitForAngularRequestsToFinish()
	}

	static void getUrl(String url) {
		Log.debug "Get: $url"
		getDriver().get(url)

		// Give the browser and app some time to start loading/redirecting
		sleep(1000)
	}

	/**
	 * @param hash The URL hash without leading '#'
	 */
	static void getUrlHash(String hash) {
		Log.debug "Get hash: $hash"
		try {
			executeScript("window.location.hash='$hash'")
			waitForAngularRequestsToFinish()
		}
		catch (Exception ex) {
			Log.warn "Could not set URL hash $hash", ex
		}
	}


	static String formatValue(value) {
		if (value instanceof Date) {
			return AbstractWebclientTest.dateFormat.format(value)
		} else if (value instanceof BigDecimal) {
			return NumberFormat.getNumberInstance(AbstractWebclientTest.locale).format(value)
		}
		return "$value"
	}

	static void assertNotFound() {
		assertError('404')
	}

	static void assertAccessDenied() {
		assert driver.currentUrl.contains('/error/403')
		assertError ('403')
	}

	static void assertError(String s = '') {
		screenshot("assert-error")

		String error = errorMessage

		assert error
		assert !error.empty
		assert error.contains(s)

	}

	static String getErrorMessage() {
		return $('nuc-error .card-header')?.text
	}

	/**
	 * Returns a currently displayed modal message dialog.
	 *
	 * @return
	 */
	static MessageModal getMessageModal() {
		if ($('.modal-dialog') == null) {
			return null
		}

		String title = $('.modal-dialog .modal-title').text.trim()
		String message = $('.modal-dialog .modal-body').text.trim()

		assert !(title ==~ /.*\{\d*\}.*/), 'Message parameters were not resolved'
		assert !(message ==~ /.*\{\d*\}.*/), 'Message parameters were not resolved'

		new MessageModal(title: title, message: message)
	}

	static void assertMessageModelAndConfirm(String title, String partOfMessage) {
		MessageModal messageModal = getMessageModal()
		assert messageModal != null

		assert messageModal.title == title

		if (partOfMessage != null) {
			assert messageModal.message.contains(partOfMessage)
		}

		messageModal.confirm()

		assert $('.modal-dialog') == null
	}

	/**
	 * @return The handle of the newly opened window.
	 */
	static String duplicateCurrentWindow() {
		Set<String> handles = driver.windowHandles

		String url = currentUrl
		executeScript("window.open('$url', '_blank')")

		Set<String> newHandles = driver.windowHandles
		assert newHandles.size() == handles.size() + 1

		String newWindowHandle = (newHandles - handles).first()
		driver.switchTo().window(newWindowHandle)
		getUrl(url)

		return newWindowHandle
	}

	static boolean isWindowClosed() {
		try {
			!driver.windowHandles.contains(driver.windowHandle)
		} catch (NoSuchWindowException e) {
			return true
		}
	}
}
