package org.nuclos.test.webclient.util

import org.junit.Test

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RequestCountsTest {
	List<String> testRequests = [
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/sideviewmenuselector',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order/layouts',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&type=perspective',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&type=searchtemplate,sideviewmenu,subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order&returnSubBo=true&type=subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order',
			'OPTIONS http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/query?offset=0&gettotal=true&chunksize=40&countTotal=true&search=&withTitleAndInfo=false&attributes=orderNumber,customer&sort=',
			'POST http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/query?offset=0&gettotal=true&chunksize=40&countTotal=true&search=&withTitleAndInfo=false&attributes=orderNumber,customer&sort=',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/40055986',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_Order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/sideviewmenuselector',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_ExampleorderLO/calculated',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_10',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_80',
			'GET http://172.17.0.1:8080/nuclos-war/rest/resources/stateIcons/example_rest_ExampleorderSM_State_90',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_ExampleorderLO/rules',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_Order/subBos/example_rest_OrderPosition_order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos',
			'GET http://172.17.0.1:4200/roboto-v15-latin-regular.7e367be02cd17a96d513.woff2',
			'GET http://172.17.0.1:8080/nuclos-war/rest/meta/tableviewlayout/example_rest_Order/example_rest_OrderPosition',
			'GET http://172.17.0.1:8080/nuclos-war/rest/preferences?boMetaId=example_rest_OrderPosition&layoutId=example_rest_ExampleorderLO&type=subform-table',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Order/40055986/subBos/example_rest_OrderPosition_order',
			'GET http://172.17.0.1:8080/nuclos-war/rest/boMetas/example_rest_OrderPosition',
			'GET http://172.17.0.1:8080/nuclos-war/rest/layout/example_rest_OrderPositionLO/calculated',
			'GET http://172.17.0.1:4200/assets/nuclos-rotating-48.gif',
			'POST http://clients2.google.com/service/update2?cup2key=6:2131701652&cup2hreq=97f1ff695b6358a40bad6024ca1bdb44415f1a3f9eb1ac8cb2f74f47ea758146',
			'GET http://172.17.0.1:8080/nuclos-war/rest/bos/example_rest_Customer/40121015/subBos/example_rest_Order_customer/40121023',
	]

	@Test
	void testRequestAnalysis() {
		RequestCounts requests = new RequestCounts(testRequests)

		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_REQUEST) == 1
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_LIST_REQUEST) == 1
		assert requests.getCount(RequestCounts.REQUEST_TYPE.EO_SUBFORM_REQUEST) == 1
		assert requests.getCount(RequestCounts.REQUEST_TYPE.UNKNOWN) == 25
	}
}