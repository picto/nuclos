package org.nuclos.test.webclient.util

import java.text.SimpleDateFormat

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
class ValueParser {

	static Date parseDate(String dateString) {
		if (!dateString) {
			return null
		}

		List<String> dateFormats = ['dd.MM.yyyy', 'MM/dd/yyyy']
		for (String format : dateFormats) {
			try {
				return new SimpleDateFormat(format).parse(dateString)
			} catch (ParseException) {
			}
		}
		return null
	}
}
