package org.nuclos.test.webclient.pageobjects.viewconfiguration

import static org.nuclos.test.webclient.AbstractWebclient2Test.$
import static org.nuclos.test.webclient.AbstractWebclient2Test.$$

import org.nuclos.test.webclient.NuclosWebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SideviewConfiguration extends ViewConfigurationModal {
	static void selectMarkedColumns() {
		$('#sideviewmenu-columns-config #btn-selected-marked').click()
	}

	static void deselectMarkedColumns() {
		$('#sideviewmenu-columns-config #btn-deselected-marked').click()
	}

	static void selectColumn(String attributeFqn) {
		findUnselectedColumnOption(attributeFqn).click()
		selectMarkedColumns()
	}

	static void deselectColumn(String attributeFqn) {
		findSelectedColumnOption(attributeFqn).click()
		deselectMarkedColumns()
	}

	static NuclosWebElement findSelectedColumnOption(String attributeFqn) {
		$('#sideviewmenu-columns-config .select-box-container-selected [attr-fqn="' + attributeFqn + '"]')
	}

	static NuclosWebElement findUnselectedColumnOption(String attributeFqn) {
		$('#sideviewmenu-columns-config [attr-fqn="' + attributeFqn + '"]')
	}

	static void moveColumn(String attributeFqn, boolean right) {
		NuclosWebElement option = findSelectedColumnOption(attributeFqn)
		option.click()

		if (right) {
			option.$('.move-right').click()
		} else {
			option.$('.move-left').click()
		}
	}

	static void newSideviewConfiguration(final String name = null) {
		open()
		$('#new-sideviewmenu').click()
		if (name) {
			$('#sideviewmenu-name-input').sendKeys(name)
		}
	}

	static void saveSideviewConfiguration() {
		$('#save-sideviewmenu').click()
	}

	static void selectSideviewConfiguration(final String name) {
		openColumnConfigurationPanel()
		selectSideviewConfigurationInModal(name)
		clickButtonOk()
	}

	static void selectSideviewConfigurationInModal(final String name) {
		$$('#sideviewmenu-selector option').find{ it.text?.trim().indexOf(name) != -1}.click()
	}

	static void openColumnConfigurationPanel() {
		open()
		clickIfCollapsed('#column-preferences-header')
	}

	static String getSelectedConfigurationName() {
		new Select($('#sideviewmenu-selector')).getFirstSelectedOption()?.text?.trim()
	}

	static List<String> getSelectedColumnNames() {
		List<String> columnNames = new ArrayList<String>()
		$$('#sidebar th').collect() {
			def columnName = it.getAttribute('column-name')
			if (!columnName.equals('status')) {
				columnNames.add(columnName)
			}
		}
		columnNames
	}

	static void discardChanges() {
		$('#discard-sideviewmenu').click()
	}

}
