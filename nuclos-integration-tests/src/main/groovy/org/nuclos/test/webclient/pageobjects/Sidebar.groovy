package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclient2Test.$
import static org.nuclos.test.webclient.AbstractWebclient2Test.$$
import static org.nuclos.test.webclient.AbstractWebclient2Test.waitForAngularRequestsToFinish

import org.nuclos.test.webclient.AbstractWebclient2Test
import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SideviewConfiguration
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import groovy.transform.CompileStatic

/**
 * Represents the sidebar component.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class Sidebar extends AbstractPageObject {

	static List<String> getListEntries() {
		entryElements*.text
	}

	/**
	 * Gets the count of entries (EntityObjects) that are currently shown in the Sidebar.
	 *
	 * @return
	 */
	static int getListEntryCount() {
		entryElements.size()
	}

	static void selectEntry(int index) {
		entryElements[index].click()
	}

	static void selectEntryByText(String text) {
		NuclosWebElement row = entryElements.find {
			it.$$('td .cell-content').find {
				it.text?.trim() == text
			}
		}
		row.click()
	}

	static int getSelectedIndex() {
		entryElements.findIndexOf { NuclosWebElement it ->
			it.hasClass('tdSelectClass')
		}
	}

	static String getValue(int rowIndex, int colIndex) {
		entryElements[rowIndex].$$('td')[colIndex].$('.cell-content').text
	}

	private static List<NuclosWebElement> getEntryElements() {
		$$('.sideview-list-entry')
	}


	static void addColumn(String boMetaId, String name) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.selectColumn(attributeFqn)

		SideviewConfiguration.close()
	}

	static void moveColumnLeft(String boMetaId, String name) {
		moveColumn(boMetaId, name, false)
	}

	static void moveColumnRight(String boMetaId, String name) {
		moveColumn(boMetaId, name, true)
	}

	private static void moveColumn(String boMetaId, String name, boolean right) {
		SideviewConfiguration.open()

		String attributeFqn = boMetaId + '_' + name
		SideviewConfiguration.moveColumn(attributeFqn, right)

		SideviewConfiguration.close()
	}

	static List<String> selectedColumns() {
		def result = []
		$$('.sideview-list-resizable th').each {
			result.add(it.getAttribute('column-name'))
		}
		return result
	}

	static List<String> getColumnHeaders() {
		$$('.sideview-list-resizable th')*.text*.trim().findAll{ it }
	}

	static void resizeSidebarComponent(int newWidth) {
		NuclosWebElement e = getResizeBar()
		int x = e.getLocation().getX()

		new Actions(AbstractWebclient2Test.getDriver())
				.dragAndDropBy(e.element, newWidth - x, 0)
				.build()
				.perform()

		waitForAngularRequestsToFinish()
	}

	static NuclosWebElement getResizeBar() {
		$('#sidebar .resize-handle')
	}

	static List<NuclosWebElement> getTreeRows() {
		WebElement we = $('#sidetree')
		assert we != null

		WebElement we2 = $(we, '.ag-body-container')
		assert we2 != null

		List<NuclosWebElement> lstWe = $$(we2, '.ag-row')
		return lstWe
	}
}
