package org.nuclos.test.webclient.pageobjects

import static org.nuclos.test.webclient.AbstractWebclient2Test.*

import org.nuclos.test.webclient.NuclosWebElement
import org.nuclos.test.webclient.pageobjects.viewconfiguration.SearchtemplateConfiguration
import org.nuclos.test.webclient.pageobjects.viewconfiguration.ViewConfigurationModal
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import groovy.transform.CompileStatic

@CompileStatic
class Searchtemplate extends AbstractPageObject {

	static WebElement getDeleteSearchtemplateButton() { $('#delete-searchtemplate') }

	static WebElement getSaveSearchtemplateButton() { $('#save-searchtemplate') }

	static WebElement getNewSearchtemplateButton() { $('#new-searchtemplate') }

	static WebElement getSearchtemplateNameInput() { $('#searchtemplate-name-input') }

	static WebElement getSearchfilterValueTextInput() { $('#searchfilter-value') }

	static WebElement getSearchfilterValueDatepickerInput() {
		def elems = $$('.searchfilter-popover-content [ngbdatepicker]')
		return elems.size() > 0 ? elems.get(0) : null
	}

	static WebElement getSearchfilter() {
		$('#text-search-input')
	}
	static void clearTextSearchfilter() {
		if ($('.clear-searchfilter') != null) {
			$('.clear-searchfilter').click()
		}
	}

	static WebElement deselectButton() {
		$('#searchtemplate-columns-config #btn-deselect-all')
	}


	static void search(String searchString) {
		clearTextSearchfilter()
		searchfilter.sendKeys(searchString)

		waitForAngularRequestsToFinish()
	}

	static class SearchTemplateItem {
		String name
		String operator
		String value
	}

	static void addSearchTemplate(String name) {
		searchfilter.clear()

		SearchtemplateConfiguration.open()
		SearchtemplateConfiguration.newSearchtemplate(name)
	}

	static void selectSearchTemplate(String name) {
		SearchtemplateConfiguration.open()
		SearchtemplateConfiguration.selectSearchtemplateConfiguration(name)
	}

	static void deleteSearchTemplate(String name) {
		selectSearchTemplate(name)

		SearchtemplateConfiguration.openSearchtemplateConfigurationPanel()

		waitFor(3) { deleteSearchtemplateButton }

		deleteSearchtemplateButton.click()
	}

	private static void openSearchtemplateConfiguration() {
		SearchtemplateConfiguration.openSearchtemplateConfigurationPanel()
	}

	static void addSearchTemplateItem(String boMetaId, SearchTemplateItem searchTemplateItem) {
		openSearchtemplateConfiguration()

		searchfilter.clear()

		String attributeFqn = boMetaId + '_' + searchTemplateItem.name
		SearchtemplateConfiguration.selectSearchAttribute(attributeFqn)

		SearchtemplateConfiguration.close()

		// open popover
		$('#button-' + attributeFqn).click()

		// select operator
		if (searchTemplateItem.operator == null) { // boolean
			if (searchTemplateItem.value == 'true') {
				$('.searchfilter-popover-content input[value="true"]').click()
			}
		} else {

			$('input[value="' + searchTemplateItem.operator + '"]').click()

			List<NuclosWebElement> dropdowns = $$('.searchfilter-attribute-popover .dropdown')
			if (dropdowns.size() > 0) {
				// reference

				ListOfValues lov = new ListOfValues()
				lov.lov = dropdowns[0]
				if (!lov.open) {
					lov.open()
				}
				lov.selectEntry(searchTemplateItem.value)
			} else {
				def datepickerInput = getSearchfilterValueDatepickerInput()
				if (datepickerInput != null) {
					// datepicker
					// TODO sendKeys will not update the model ????
					datepickerInput.clear()
					datepickerInput.sendKeys('' + searchTemplateItem.value + Keys.TAB)
				} else {
					// textfield
					searchfilterValueTextInput.clear()
					searchfilterValueTextInput.sendKeys('' + searchTemplateItem.value)
				}
			}
		}
		waitForAngularRequestsToFinish()
	}

	static void removeAllSearchTemplateItems() {
		clearTextSearchfilter()

		openSearchtemplateConfiguration()

		// TODO: Move to
		boolean leave = false
		while ($$('#searchtemplate-selector option').size() > 1 && !leave) {
			int optionCount = $$('#searchtemplate-selector option').size()
			new Select($('#searchtemplate-selector')).selectByIndex(optionCount - 1)
			if (!$('#delete-searchtemplate')) {
				leave = true
				continue
			}
			$('#delete-searchtemplate').click()

		}

		ViewConfigurationModal.close()


		/* TODO
		openSearchTemplateAttributeListPopover()

		def checkboxes = $$('#searchfilter-attributelist-popover input[type="checkbox"]')
		for (def checkbox in checkboxes) {
			if (checkbox.isSelected()) {
				checkbox.click()
				waitForAngularRequestsToFinish()
			}
		}
		*/
	}

}