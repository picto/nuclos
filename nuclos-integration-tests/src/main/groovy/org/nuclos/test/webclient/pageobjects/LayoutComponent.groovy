package org.nuclos.test.webclient.pageobjects

import org.nuclos.test.webclient.AbstractWebclientTest
import org.openqa.selenium.By

import groovy.transform.CompileStatic

/**
 * TODO: Currently works only correctly if there is only 1 nuc-layout component. But there might be multiple, e.g. if a detail modal is shown.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class LayoutComponent extends AbstractPageObject {
	static boolean isHorizontalScrollbarPresent() {
		executeScript('var element = document.querySelector("nuc-layout"); return element.scrollWidth > element.clientWidth;')
	}

	static boolean isVerticalScrollbarPresent() {
		executeScript('var element = document.querySelector("nuc-layout"); return element.scrollHeight > element.clientHeight;')
	}

	static boolean isBusy() {
		!AbstractWebclientTest.driver.findElements(
				By.cssSelector('nuc-detail > nuc-layout > ng-busy > .ng-busy')
		).empty
	}
}
