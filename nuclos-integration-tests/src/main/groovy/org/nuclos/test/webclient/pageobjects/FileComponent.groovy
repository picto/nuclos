package org.nuclos.test.webclient.pageobjects

import java.util.regex.Matcher

import org.nuclos.test.webclient.NuclosWebElement

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class FileComponent extends AbstractPageObject {
	final NuclosWebElement element

	FileComponent(NuclosWebElement element) {
		this.element = element
	}

	void setFile(File file) {
		element.$('input').sendKeys(file.absolutePath)
	}

	String getImageUrl() {
		String result

		String value = fileuploadContainer.getCssValue('background')
		Matcher m = value =~ /url\("(.+?)"\)/
		if (m.find()) {
			result = m.group(1)
		}

		return result
	}

	boolean hasImage() {
		element.getCssValue('background') != 'none'
	}

	NuclosWebElement getFileuploadContainer() {
		element
	}
}
