package org.nuclos.test.webclient

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.codehaus.groovy.reflection.ReflectionUtils
import org.junit.AfterClass
import org.junit.FixMethodOrder
import org.junit.experimental.categories.Category
import org.junit.runners.MethodSorters
import org.nuclos.test.AbstractNuclosTest
import org.nuclos.test.IntegrationTest
import org.nuclos.test.log.CallTrace
import org.nuclos.test.log.Log
import org.nuclos.test.rest.SuperuserRESTClient
import org.nuclos.test.webclient.utils.Screenshot
import org.nuclos.test.webclient.utils.Utils
import org.nuclos.test.webclient.utils.WebDriverFactory
import org.openqa.selenium.*
import org.openqa.selenium.logging.LogEntries
import org.openqa.selenium.logging.LogEntry
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.Select

@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
abstract class AbstractWebclientTest extends AbstractNuclosTest {
	static RemoteWebDriver driver

	// The following settings can be overriden with System Properties
	static String nuclosWebclientProtocol = initFromSystemProperty('nuclos.webclient.protocol', 'http')
	static String nuclosWebclientHost = initFromSystemProperty('nuclos.webclient.host', '127.0.0.1')
	static String nuclosWebclientPort = initFromSystemProperty('nuclos.webclient.port', '7000')
	static String nuclosServerProtocol = initFromSystemProperty('nuclos.server.protocol', 'http')
	static String nuclosServerHost = initFromSystemProperty('nuclos.server.host', '127.0.0.1')
	static String nuclosServerPort = initFromSystemProperty('nuclos.server.port', '8080')
	static String nuclosServerContext = initFromSystemProperty('nuclos.server.context', 'nuclos-war')
	static String seleniumServer = initFromSystemProperty('selenium.server', 'http://127.0.0.1:4444/wd/hub')
	static Browser browser = initFromSystemProperty('browser', { String name ->
		Browser.find {
			"$it".toLowerCase() == name.toLowerCase()
		}
	}, Browser.FIREFOX)
	static Locale locale = initFromSystemProperty('locale', { String name ->
		DateFormat.getAvailableLocales().find {
			it.toString() == name
		}
	}, Locale.GERMANY)
	static DateFormat dateFormat = locale.language == 'en' ? new SimpleDateFormat('MM/dd/yyyy') : new SimpleDateFormat('dd.MM.yyyy')

	static String nuclosServer = "$nuclosServerProtocol://$nuclosServerHost:$nuclosServerPort/$nuclosServerContext"
	static String nuclosWebclientServer = "$nuclosWebclientProtocol://$nuclosWebclientHost:$nuclosWebclientPort"
	static String baseUrl = "$nuclosWebclientServer/index.html"
	static String jscoverageClearStorageUrl = "$nuclosWebclientServer/jscoverage-clear-local-storage.html"

	static Dimension preferredWindowSize = new Dimension(1024, 768)

	static enum Browser {
		FIREFOX(WebDriverFactory.remoteFirefox),
		CHROME(WebDriverFactory.remoteChrome),
		IE(WebDriverFactory.remoteIE),
		PHANTOMJS(WebDriverFactory.remotePhantomJS),
		SAFARI(WebDriverFactory.remoteSafari)

		Closure<RemoteWebDriver> driverFactory

		Browser(Closure driverFactory) {
			this.driverFactory = driverFactory
		}

		RemoteWebDriver createDriver(Proxy proxy) {
			return driverFactory(proxy)
		}
	}

	static <T> T initFromSystemProperty(String propertyName, T defaultValue) {
		initFromSystemProperty(propertyName, null, defaultValue)
	}

	/**
	 * Tries to read the system property with the given name.
	 *
	 * @param propertyName
	 * @param initClosure Optional, will be applied to the system property value if not null
	 * @param defaultValue
	 * @return
	 */
	static <T> T initFromSystemProperty(String propertyName, Closure initClosure, def defaultValue) {
		String property = System.getProperty(propertyName)
		def result

		if (initClosure) {
			if (property) {
				result = initClosure(property)
			}
		} else {
			result = property
		}

		result = result ?: defaultValue

		Log.info "$propertyName = $result"

		return result
	}

	static void waitForAngularRequestsToFinish() {
		waitForAngularRequestsToFinish(30)
	}

	static void waitForAngularRequestsToFinish(int timeout) {
		CallTrace.trace("Wait for Angular-1 requests to finish") {
			waitForOrReload(timeout) {
				((JavascriptExecutor) driver).executeAsyncScript('var callback = arguments[arguments.length - 1]; angular.element(document.body).injector().get(\'\$browser\').notifyWhenNoOutstandingRequests(callback);');
				return true
			}
		}
	}

	static void waitForModal() {
		// wait until modal is displayed correctly
		waitFor {
			$('.modal-backdrop')?.getCssValue('opacity') == '0.5'
		}
	}

	static void closeModal() {
		$('.modal').sendKeys(Keys.ESCAPE); // close modal

		// wait until modal is closed correctly
		waitFor {
			$('.modal') == null
		}
		sleep(500)
		waitForAngularRequestsToFinish()
	}

	static void waitForElementDisplayed(WebElement el) {
		waitFor(5) {
			el.displayed
		}
	}

	private static boolean initialSetupExecuted = false;

	static void initialSetup(SuperuserRESTClient client) {
		if (!initialSetupExecuted) {

			// will be called once for all tests

			overrideCssForTests:
			{
				Map params = [
						"WEBCLIENT_CSS": "#detailblock #sideview-statusbar-content:before {\n  opacity: 0.3;\n  content: \'statusbar with timestamp disabled for screenshot diffs\';\n}"
				]
				client.setSystemParameters(params)
			}

			getUrl(driver.currentUrl)

			initialSetupExecuted = true;
		}
	}

	@AfterClass
	static void teardown() {
		waitForAngularRequestsToFinish()
		try {
			executeScript("jscoverage_report();")
			executeAsyncScript('var callback = arguments[arguments.length - 1];callback();')
		}
		catch (Exception ex) {
			Log.warn "Could not create jscoverage report"
		}

		printBrowserLog()
		printCookies()

		shutdown()
	}

	static void shutdown() {
		try {
			driver.quit()
		}
		catch (Throwable t) {
			Log.warn "Could not quit selenium driver", t
		}
	}

	static void resizeBrowserWindow() {
		driver.manage().window().setSize(preferredWindowSize)
	}

	static void printBrowserLog() {
		try {
			LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER)
			for (LogEntry entry : logEntries) {
				Log.info "[$browser] " + new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser logs for $browser: $ex.message"
		}
	}

	static void printCookies() {
		try {
			Log.info "Browser Cookies:"
			driver.manage().cookies.each {
				Log.info it.toString()
			}
		}
		catch (Exception ex) {
			Log.warn "Failed to get browser cookies for $browser: $ex.message"
		}
	}


	static WebElement byRepeater(String repeater) {
		$('[ng-repeat*="' + repeater + '"]')
	}

	static List<WebElement> elementsByBinding(String binding) {
		$$('[ng-bind-html*="' + binding + '"]')
	}

	static List<WebElement> elementsByBinding(WebElement parent, String binding) {
		parent.findElements(By.cssSelector('[ng-bind-html*="' + binding + '"]'))
	}

	static WebElement $(String selector) {
		waitForAngularRequestsToFinish()
		List<WebElement> elements = getDriver().findElements(By.cssSelector(selector))
		return elements.empty ? null : elements.get(0)
	}

	static WebElement $(WebElement e, String selector) {
		List<WebElement> elements = e.findElements(By.cssSelector(selector))
		return elements.empty ? null : elements.get(0)
	}

	static List<WebElement> $$(String selector) {
		return getDriver().findElements(By.cssSelector(selector))
	}

	static List<WebElement> $$(WebElement e, String selector) {
		return e.findElements(By.cssSelector(selector))
	}

	static boolean hasClass(WebElement e, String className) {
		e.getAttribute('class').split(' ').contains(className)
	}

	static List<WebElement> findVisibleElements(String cssSelector) {
		List<WebElement> result = new ArrayList<WebElement>();
		List<WebElement> elements = driver.findElements(By.cssSelector(cssSelector))
		for (WebElement elem : elements) {
			println "elem: " + elem + "   " + elem.isDisplayed();
			if (elem.isDisplayed()) {
				result.add(elem);
			}
		}
		return result;
	}


	static boolean isPresent(String selector) {
		try {
			def element = $(selector)
			return !!element
		}
		catch (Exception ex) {
			return false
		}
	}

	static boolean login(String username = 'nuclos', boolean autologin = false) {
		return login(username, '', autologin, null)
	}

	/**
	 * Performs a Login with the given credentials and the current locale, if not logged in already.
	 *
	 * @param username
	 * @param password
	 * @param autologin
	 * @param mandator
	 * @return
	 */
	static boolean login(String username, String password, boolean autologin = false, String mandator = null) {
		if (isPresent('#logout')) {
			return false
		}

		if (isPresent('#acceptcookies')) {
			WebElement we = $('#acceptcookies')
			if (we.isDisplayed()) {
				we.click()
			}
		}

		loginUnsafe(username, password, autologin, mandator)

		assert $('#logout')

		return true
	}

	/**
	 * Performs a Login with the given credentials and the current locale.
	 * Without any checks for login success etc.
	 *
	 * @param username
	 * @param password
	 * @param autologin
	 * @param mandator
	 * @return
	 */
	static boolean loginUnsafe(String username, String password, boolean autologin = false, String mandator = null) {
		$('#menu-language').click()
		$("#menu-language-$locale.language").click()

		$('#username').clear()
		$('#username').sendKeys(username)
		$('input[type="password"]').sendKeys(password)

		if (autologin) {
			$('#autologin').click()
		}

		$('#submit').click()

		waitForAngularRequestsToFinish()

		if (mandator) {
			if (mandator == 'null') {
				// superuser login without any mandator selection
				$('#mandatorlessSession').click()
			} else {
				WebElement chooseMandator = $('#chooseMandator')
				Select droplist = new Select(chooseMandator);
				droplist.selectByVisibleText(mandator);
				$('#chooseMandatorButton').click()
			}

			waitForAngularRequestsToFinish()
		}
	}

	static boolean logout() {
		if (!isPresent('#logout')) {
			return false
		}

		waitForAngularRequestsToFinish()

		$('#user-dropdown .dropdown-toggle').click()
		$('#logout').click()

		waitForAngularRequestsToFinish()

		assert !$('#logout')

		return true
	}

	static String getCurrentUrl() {
		try {
			waitForAngularRequestsToFinish()
			return driver.getCurrentUrl()
		}
		catch (Throwable ex) {
			Log.error 'Could not get current URL', ex
		}

		return null
	}

	static void getUrl(String url) {
		Log.debug "Get: $url"
		getDriver().get(url)
	}

	static void getUrlHash(String hash) {
		Log.debug "Get hash: $hash"
		try {
			executeScript("window.location.hash='$hash'")
			waitForAngularRequestsToFinish()
		}
		catch (Exception ex) {
			Log.warn "Could not set URL hash $hash", ex
		}
	}

	static WebElement element(By by) {
		waitForAngularRequestsToFinish()
		return driver.findElement(by)
	}

	static List<WebElement> elements(By by) {
		waitForAngularRequestsToFinish()
		return driver.findElements(by)
	}

	/**
	 * Deprecated: Use {@link #waitFor(Closure)} or a custom WebDriverWait instead.
	 *
	 * @param millis
	 */
	@Deprecated()
	static void sleep(long millis) {
		try {
			Thread.sleep(millis)
		} catch (InterruptedException e) {
			Log.error(e.getMessage(), e)
		}
	}

	static void screenshot(String name) {
		waitForAngularRequestsToFinish()
		String caller = ReflectionUtils.callingClass.simpleName
		Screenshot.take(caller, name)
	}

	/**
	 * Switches to another browser window, if more than 1 window exists.
	 *
	 * @return
	 */
	static String switchToOtherWindow() {
		def currentWindow = driver.windowHandle
		def otherWindow = driver.windowHandles.find { it != currentWindow }

		if (otherWindow) {
			driver.switchTo().window(otherWindow)
			resizeBrowserWindow()
			return otherWindow
		}

		return null
	}

	static void closeOtherWindows() {
		def currentWindow = driver.windowHandle

		driver.windowHandles.each {
			if (it != currentWindow) {
				driver.switchTo().window(it)
				driver.close()
			}
		}

		driver.switchTo().window(currentWindow)
	}

	/**
	 * Waits up to 30 seconds for the given Closure to return true.
	 * Fails the build if the condition is not met within the timeout.
	 *
	 * @param condition
	 * @return
	 */
	static boolean waitFor(Closure condition) {
		return waitFor(30, condition)
	}

	/**
	 * Waits for the given Closure to return true.
	 * Fails the build if the condition is not met within the given timeout.
	 *
	 * @param condition
	 * @param timeout
	 * @return
	 */
	static boolean waitFor(int timeoutInSeconds, Closure condition) {
		if (!doWaitFor(timeoutInSeconds, condition)) {
			String source = Utils.closureToString(condition)
			fail('Failed to wait for condition: ' + source, new Throwable(source))
			return false
		}

		return true
	}

	/**
	 * Waits for the given Closure to return true.
	 *
	 * @param timeoutInSeconds
	 * @param condition
	 * @return true , if the condition was met within the timeout
	 */
	static boolean doWaitFor(int timeoutInSeconds, Closure condition) {
		try {
			Thread t = new Thread() {
				@Override
				void run() {
					def result = condition()
					while (!result && !isInterrupted()) {
						Log.info "Waiting for condition ${condition.toString()}..."
						sleep(500)
						result = condition()
					}
				}
			}
			t.start()
			try {
				t.join(timeoutInSeconds * 1000)
			} catch (InterruptedException ex) {
				Log.warn "Thread interrupted", ex
			}

			if (t.alive) {
				t.interrupt()
				return false
			}
			return true
		} catch (Exception ex) {
			Log.warn("Failed to wait for condition", ex)
			return false
		}
	}

	static boolean waitForOrReload(int timeout, Closure condition) {
		if (!doWaitFor(timeout, condition)) {
			Log.warn("Failed to wait for condition, refreshing and retrying...")
			getUrl(driver.currentUrl)
			return waitFor(timeout, condition)
		}
	}

	static void openStartpage() {
		getUrl(baseUrl + '#/menu')
	}

	/**
	 * Forcefully fails the build immediately.
	 *
	 * @param failure
	 * @param t
	 */
	static void fail(String failure, Throwable t = null) {
		FailureHandler.fail(failure, t)
	}

	static List<WebElement> luceneSearch(String s) {

		WebElement lucene = $('#lucenesearch')
		assert lucene != null

		WebElement input = lucene.findElement(By.className('luceneinput'))
		assert input != null

		input.clear()
		input.sendKeys(s)

		screenshot('afterLuceneSearch')

		return lucene.findElements(By.className('luceneresult'))
	}

	static void luceneTest(String s, int expected) {
		waitForAngularRequestsToFinish()

		try {
			waitFor(10) {
				luceneSearch(s).size() == expected
			}
		} catch (Exception e) {
			def size = luceneSearch(s).size()
			screenshot('beforeExceptionInLuceneTest')
			assert size == expected
		}

		screenshot('afterLuceneSearchAndWait')
	}

	static void blur() {
		executeScript('document.activeElement.blur();')
	}

	static Object executeScript(String script, Object... args) {
		((org.openqa.selenium.JavascriptExecutor) driver).executeScript(script, args)
	}

	static Object executeAsyncScript(String script, Object... args) {
		((org.openqa.selenium.JavascriptExecutor) driver).executeAsyncScript(script, args)
	}

	static boolean isHorizontalScrollbarPresent() {
		executeScript('return document.documentElement.scrollWidth > document.documentElement.clientWidth;')
	}

	static boolean isVerticalScrollbarPresent() {
		executeScript('return document.documentElement.scrollHeight > document.documentElement.clientHeight;')
	}

	static highlight(WebElement element) {
		executeScript('arguments[0].style.border="3px solid red"', element)
	}
}
