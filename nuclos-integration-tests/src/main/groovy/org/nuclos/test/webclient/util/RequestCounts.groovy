package org.nuclos.test.webclient.util

import java.util.regex.Pattern

import groovy.transform.CompileStatic
import net.lightbody.bmp.core.har.Har

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RequestCounts {
	Map<REQUEST_TYPE, Integer> requestCount = new HashMap<>()

	enum REQUEST_TYPE {
		// TODO: Define more request types (create, update, delete of EOs, preferences, etc.)
		EO_REQUEST(Pattern.compile('^(?i)(GET).*/rest/bos/(\\w+)/(\\w+)($|\\?)')),
		EO_LIST_REQUEST(Pattern.compile('^(?i)(GET|POST) .*/rest/bos/\\w+($|\\?|/query)')),
		EO_SUBFORM_REQUEST(Pattern.compile('^(?i)(GET) .*/rest/bos/\\w+/\\w+/subBos/\\w+/\\w+($|\\?)')),
		UNKNOWN(Pattern.compile('.*'))

		private final Pattern pattern

		REQUEST_TYPE(Pattern pattern) {
			this.pattern = pattern
		}

		boolean matches(String url) {
			return url.find(pattern)
		}
	}

	RequestCounts(Har har) {
		this(
				har.log.entries.collect {
					"$it.request.method $it.request.url".toString()
				}
		)
	}

	RequestCounts(List<String> requests) {
		count(requests)
	}

	/**
	 * Analyzes a list of requests in the form '<METHOD> <URL>'
	 *
	 * @param requests
	 */
	private void count(List<String> requests) {
		this.requestCount.clear()

		requests.each {
			REQUEST_TYPE type = findType(it)
			increaseCount(type)
		}
	}

	private REQUEST_TYPE findType(String methodAndUrl) {
		REQUEST_TYPE type = REQUEST_TYPE.values().find {
			REQUEST_TYPE type -> type.matches(methodAndUrl)
		}

		return type ?: REQUEST_TYPE.UNKNOWN
	}

	private void increaseCount(REQUEST_TYPE type) {
		if (!requestCount.containsKey(type)) {
			requestCount.put(type, 0)
		}

		requestCount[type]++
	}

	int getCount(REQUEST_TYPE type) {
		return requestCount[type] ?: 0
	}
}
