package org.nuclos.test

import static org.nuclos.test.log.CallTrace.trace

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.experimental.categories.Category
import org.junit.rules.TestRule
import org.junit.runners.MethodSorters
import org.nuclos.test.log.Log
import org.nuclos.test.rest.RESTHelper
import org.nuclos.test.rest.SuperuserRESTClient

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@Category(IntegrationTest.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@CompileStatic
abstract class AbstractNuclosTest {
	static SuperuserRESTClient nuclosSession

	@Rule
	public final TestRule testRule = new NuclosTestRule()

	@BeforeClass
	static void setup() {
		Log.debug 'Setup'

		truncateTablesAndCaches()
	}

	static void truncateTablesAndCaches() {
		trace('Truncate tables and Caches') {
			if (!nuclosSession) {
				trace('Login as "nuclos"') {
					nuclosSession = new SuperuserRESTClient('nuclos', '').login()
				}
			}

			Map truncateTable =
					[
							boMetaId  : 'nuclet_test_utils_TruncateTables',
							attributes: [
									'name': 'Truncate User Tables' + new Date().getTime()
							]
					]
			truncateTable = RESTHelper.createBo(truncateTable, nuclosSession)
			Map attributes = truncateTable.get('attributes') as Map
			attributes.put('nuclosState', [id: "nuclet_test_utils_TruncateTablesSM_State_20"])

			RESTHelper.updateBo(truncateTable, nuclosSession)

			Log.info nuclosSession.invalidateServerCaches()
		}
	}
}
