package org.nuclos.test.rest

import org.nuclos.test.EntityObject
import org.nuclos.test.SystemEntities

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class SuperuserRESTClient extends RESTClient {

	SuperuserRESTClient(final String user, final String password) {
		super(user, password)
	}

	@Override
	SuperuserRESTClient login() {
		return (SuperuserRESTClient) super.login()
	}

	Map<String, String> getSimpleSystemParameters() {
		RESTHelper.getSimpleSystemParameters(this)
	}

	List<EntityObject<String>> getSystemParameters(
			SuperuserRESTClient client
	) {
		RESTHelper.getEntityObjects(SystemEntities.PARAMETER, client)
	}

	List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.setSystemParameters(newParams, this)
	}

	List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams
	) {
		RESTHelper.patchSystemParameters(newParams, this)
	}

	boolean deleteUserByEmail(
			String email
	) {
		RESTHelper.deleteUserByEmail(email, this)
	}

	String invalidateServerCaches() {
		RESTHelper.invalidateServerCaches(this)
	}

	String managementConsole(
			String command,
			String arguments = null
	) {
		RESTHelper.managementConsole(command, arguments, this)
	}
}
