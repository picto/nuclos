package org.nuclos.test.rest

import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject

import groovy.transform.CompileStatic

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
@CompileStatic
class RESTClient {
	final String username
	final String password

	String sessionId

	RESTClient(final String user, final String password) {
		this.username = user
		this.password = password
	}

	RESTClient login() {
		sessionId = RESTHelper.login(this)
		return this
	}

	public <PK> void save(EntityObject<PK> eo) {
		RESTHelper.save(eo, this)
	}

	public <PK> void delete(EntityObject<PK> eo) {
		RESTHelper.delete(eo, this)
	}

	public <PK> EntityObject<PK> getEntityObject(
			EntityClass<PK> entityClass,
			PK id
	) {
		RESTHelper.getEntityObject(entityClass, id, this)
	}

	public <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			QueryOptions options = null
	) {
		RESTHelper.getEntityObjects(entityClass, this, options)
	}

	public <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId
	) {
		RESTHelper.loadDependents(eo, subEoClass, referenceAttributeId, this)
	}

	public <PK> void executeCustomRule(
			EntityObject<PK> eo,
			String ruleName
	) {
		RESTHelper.executeCustomRule(eo, ruleName, this)
	}

	Map getSessionData() {
		RESTHelper.getSessionData(this)
	}

	void saveAll(final List<EntityObject> entityObjects) {
		RESTHelper.saveAll(entityObjects, this)
	}

	public <PK> void changeState(EntityObject<PK> eo, int statusNumeral) {
		RESTHelper.changeState(eo, statusNumeral, this)
	}
}
