package org.nuclos.test.rest

/**
 * TODO: Javadoc
 * TODO: @CompileStatic
 */
class TestDataHelper {
	
	
	private static def deepcopy(orig) {
		def bos = new ByteArrayOutputStream()
		def oos = new ObjectOutputStream(bos)
		oos.writeObject(orig); oos.flush()
		def bin = new ByteArrayInputStream(bos.toByteArray())
		def ois = new ObjectInputStream(bin)
		return ois.readObject()
	}
	
	private static def updateData(object, variation) {
		if (variation != null) {
			object.each {
				it ->
					def variationValue = variation[it.key]
					if (variationValue instanceof String || variationValue instanceof Integer || variationValue instanceof Date) {
						if (variationValue != null) {
							object[it.key] = variationValue
						}
					} else {
						if (variationValue != null) {
							updateData(object[it.key], variationValue)
						}
					}
			}
		}
	}

	private static def insert(template, variation, sessionId) {
		def copy = deepcopy(template)
		
		// update template data with variation data
		updateData(copy, variation)
		return RESTHelper.createBo(copy, sessionId)
	}

	/**
	 * TODO: Javadoc
	 */
	static void insertTestData(sessionId) {
		def customer =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer',
				customerNumber: 2001,
				discount: 12.5,
				active: true
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 1',
															city   : 'Test 1',
															zipCode: '12345'
													]
									],
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 2',
															city   : 'Test 2',
															zipCode: '54321'
													]
									],
							]
					]
			]
		]
		customer = insert(customer, null, sessionId)

		def customer2 =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer 2',
				customerNumber: 22222,
				discount: 5,
				active: true
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Street 4',
															city   : 'City 4',
															zipCode: '44444'
													]
									]
							]
					]
			]
		]
		insert(customer2, null, sessionId)

		def inactiveCustomer =
		[
			boMetaId: 'example_rest_Customer',
			attributes: [
				name: 'Test-Customer inactive',
				customerNumber: 9001,
				discount: 10.0,
				active: false
			],
			'subBos': [
					insert: [
							example_rest_CustomerAddress_customer: [
									[
											_flag     : 'insert',
											boId      : null,
											attributes:
													[
															street : 'Test 3',
															city   : 'Test 3',
															zipCode: '33333'
													]
									],
							]
					]
			]
		]
		inactiveCustomer = insert(inactiveCustomer, null, sessionId)
		

		def category =
		[
			boMetaId: 'example_rest_Category',
			attributes: [
				'name': ''
			]
		]
		def categoryHardware = insert(category, [attributes: ['name': 'Hardware']], sessionId)
		def categorySoftware = insert(category, [attributes: ['name': 'Software']], sessionId)
		
		
		def article =
		[
			boMetaId: 'example_rest_Article',
			attributes: [
				'articleNumber': 1001,
				'name': 'Nuclos',
				'category': ['id': null],
				'price': '0',
				'active': true,
			]
		]
		
		def articleNuclos = insert(article, [attributes: [articleNumber: 1001, name: 'Nuclos', price: '0', category: [id: categorySoftware.boId] ]], sessionId)
		def articleTextProcessor = insert(article, [attributes: [articleNumber: 1002, name: 'Text processor', price: '200', category: [id: categorySoftware.boId] ]], sessionId)
		def articleNotebookPro = insert(article, [attributes: [articleNumber: 1003, name: 'Notebook Pro', price: '3000', category: [id: categoryHardware.boId] ]], sessionId)
		def articleMouse = insert(article, [attributes: [articleNumber: 1004, name: 'Mouse', price: '30', category: [id: categoryHardware.boId] ]], sessionId)
		
		
		def order =
		[
			boMetaId: 'example_rest_Order',
			attributes: [
				'orderNumber': 100,
				'customer': [id: customer.boId],
				'orderDate': new Date(),// TODO
				'note': ''
			],
			'subBos': [
				insert: [
					example_rest_OrderPosition_order: []
				]
			]
		]
		def subBo = [
			_flag: 'insert',
			boId: null,
			boMetaId: 'example_rest_OrderPosition',
			attributes: [
				article: [id: 0],
				quantity: 1,
				price:''
			]
		]
		
		
		for (int year = 2014; year <= 2015; year++) {
			for (int month = 0; month < 12; month += 4) {
				def orderCopy = deepcopy(order)
				Calendar cal = Calendar.instance
				cal[Calendar.YEAR] = year
				cal[Calendar.MONTH] = month
				cal[Calendar.DATE] = 1
				cal[Calendar.HOUR_OF_DAY] = 12
				updateData(orderCopy, [attributes: [
					orderNumber: '100' + year + month, 
					orderDate: cal.getTime(),
					note: (month < 7 ? 'B' : 'A')
					]
				])
				
				def quantity = 0
				def
				subBoTemp = deepcopy(subBo); quantity = year%2000; updateData(subBoTemp, [attributes: [article: [id: articleNuclos.boId], quantity: '' + quantity, price: ''+(quantity * articleNuclos.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = year%2000+10000; updateData(subBoTemp, [attributes: [article: [id: articleTextProcessor.boId], quantity: '' + quantity, price: ''+(quantity * articleTextProcessor.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = year%2000; updateData(subBoTemp, [attributes: [article: [id: articleNotebookPro.boId], quantity: '' + quantity, price: ''+(quantity * articleNotebookPro.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				subBoTemp = deepcopy(subBo); quantity = (year%2000)*(20-month); updateData(subBoTemp, [attributes: [article: [id: articleMouse.boId], quantity: '' + quantity, price: ''+(quantity * articleMouse.attributes.price)]]); orderCopy.subBos.insert.example_rest_OrderPosition_order.add(subBoTemp)
				insert(orderCopy, null, sessionId)
			}
		}
	}
}
