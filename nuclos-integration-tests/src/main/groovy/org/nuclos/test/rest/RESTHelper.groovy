package org.nuclos.test.rest

import static org.nuclos.test.log.CallTrace.trace

import org.nuclos.test.EntityClass
import org.nuclos.test.EntityObject
import org.nuclos.test.SystemEntities
import org.nuclos.test.log.Log
import org.nuclos.test.webclient.AbstractWebclientTest
import org.springframework.http.HttpMethod

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import groovy.transform.PackageScope

/**
 * Performs REST requests directly, instead of using the Webclient GUI.
 *
 */
@CompileStatic
class RESTHelper {

	static String REST_BASE_URL = AbstractWebclientTest.nuclosServerProtocol + '://' + AbstractWebclientTest.nuclosServerHost + ':' + AbstractWebclientTest.nuclosServerPort + '/' +
			AbstractWebclientTest.nuclosServerContext + '/rest'

	private static Map requestJson(String url, HttpMethod httpMethod, String sessionId = null, String json = null) {
		String response = requestString(url, httpMethod, sessionId, json)
		Map result = null
		if (response != null) {
			try {
				result = (Map) new JsonSlurper().parseText(response)
			} catch (Exception e) {
				e.printStackTrace()
			}
		}
		return result
	}

	private static String requestString(
			String url,
			HttpMethod httpMethod,
			String sessionId = null,
			String json = null
	) {
		HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
		urlConnection.setRequestMethod(httpMethod.name())
		urlConnection.setRequestProperty("Accept", "application/json")
		urlConnection.setRequestProperty("Content-Type", "application/json")

		if (sessionId != null) {
			urlConnection.setRequestProperty("Cookie", "JSESSIONID=" + sessionId)
		}

		urlConnection.doOutput = true

		if (json != null) {
			def writer = new OutputStreamWriter(urlConnection.outputStream)
			writer.write(json.trim())
			writer.flush()
			writer.close()
		}
		urlConnection.connect()

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))
			StringBuffer response = new StringBuffer()
			String inputLine
			while ((inputLine = reader.readLine()) != null) {
				response.append(inputLine)
			}
			reader.close()
			return response.toString()
		} catch (Exception e) {
			Log.warn("Could not read response: $e.message")
		}

		return null
	}

	@PackageScope
	static String login(RESTClient client) {
		String post = '{"username":"' + client.username + '", "password":"' + client.password + '"}'
		def response = requestJson(REST_BASE_URL, HttpMethod.POST, null, post)
		return response?.get('sessionId')
	}

	@PackageScope
	static Map getSessionData(RESTClient client) {
		def result = requestJson(REST_BASE_URL, HttpMethod.GET, client.sessionId, null)
		return result
	}

	private static String getString(String url, String sessionId = null) {
		requestString(url, HttpMethod.GET, sessionId)
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static Map createBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Create EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId
			Map result = requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))

			return result
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static Object createBo(
			String boMetaId,
			Map<String, Serializable> attributes,
			RESTClient client
	) {
		trace('Creat EO: ' + boMetaId) {
			def bo = [
					boMetaId  : boMetaId,
					attributes: [:]
			]

			for (Map.Entry<String, Serializable> attribute : attributes.entrySet()) {
				bo['attributes'][attribute.key] = attribute.value
			}

			def url = REST_BASE_URL + '/bos/' + boMetaId
			return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static void updateBo(Map bo, RESTClient client) {
		String entityClassId = bo['boMetaId']
		trace("Update EO ${entityClassId}") {
			def url = REST_BASE_URL + '/bos/' + entityClassId + '/' + bo['boId']
			requestJson(url, HttpMethod.PUT, client.sessionId, JsonOutput.toJson(bo))
		}
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static Map getBos(String boMetaId, RESTClient client, String searchFilterId = null) {
		def url = REST_BASE_URL + '/bos/' + boMetaId
		if (searchFilterId != null) {
			url += '?searchFilter=' + searchFilterId
		}
		return requestJson(url, HttpMethod.GET, client.sessionId, null)
	}

	/**
	 * Use {@link #getEntityObjects(org.nuclos.test.EntityClass, RESTClient)}.
	 */
	@Deprecated
	static Map getBosByQueryContext(String boMetaId, Map queryContext, RESTClient client) {
		def url = REST_BASE_URL + '/bos/' + boMetaId + '/query';
		return requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(queryContext));
	}

	/**
	 * Use the system entity "User" and save it via {@link #save(org.nuclos.test.EntityObject, RESTClient)}.
	 */
	@Deprecated
	static Map createUser(
			String name,
			String password,
			List roles,
			RESTClient client
	) {

		def url = REST_BASE_URL + '/admin/user'

		def user = requestJson(url + '/' + name, HttpMethod.GET, client.sessionId, null)
		// user  exists
		if (user) {
			return user
		}

		def userData =
				[
						"email"      : name + "@novabit.de",
						"firstname"  : name,
						"group"      : null,
						"lastname"   : name,
						"locked"     : false,
						"name"       : name,
						"newPassword": password,
						"superuser"  : false,
						"roles"      : roles
				]

		user = requestJson(url, HttpMethod.POST, client.sessionId, JsonOutput.toJson(userData))
		return user
	}

	@PackageScope
	static boolean deleteUserByEmail(
			String email,
			SuperuserRESTClient client
	) {
		QueryOptions options = new QueryOptions(queryWhere: "org_nuclos_businessentity_nuclosuser_email='$email'")
		List<EntityObject<String>> results = getEntityObjects(SystemEntities.USER, client, options)

		if (!results.empty) {
			delete(results[0], client)
			return true
		}

		return false
	}

	@PackageScope
	static void executeCustomRule(
			final EntityObject eo,
			final String ruleName,
			final RESTClient client
	) {
		eo.executeCustomRule = ruleName
		save(eo, client)
	}

	@PackageScope
	static String managementConsole(
			String command,
			String arguments,
			RESTClient client
	) {

		def url = REST_BASE_URL + '/maintenance/managementconsole/' + command + '/'
		if (arguments) {
			String urlEncodedArguments = URLEncoder.encode(arguments, "UTF-8")
			url += urlEncodedArguments + '/'
		}

		getString(url, client.sessionId)
	}

	@PackageScope
	static String invalidateServerCaches(SuperuserRESTClient client) {
		managementConsole('invalidateAllCaches', null, client)
	}

	@PackageScope
	static <PK> List<EntityObject<PK>> getEntityObjects(
			EntityClass<PK> entityClass,
			RESTClient client,
			QueryOptions options = null
	) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn

		if (options && options.queryWhere) {
			String encodedWhere = URLEncoder.encode(options.queryWhere, 'UTF-8')
			url += "/query?where=$encodedWhere"
		}

		Map json = requestJson(url, HttpMethod.GET, client.sessionId, null)
		if (json == null) {
			return null
		}

		json['bos'].collect { Map eoJson ->
			eoFromJson(entityClass, eoJson, client)
		}
	}

	@PackageScope
	static <PK, SubPK> List<EntityObject<SubPK>> loadDependents(
			EntityObject<PK> eo,
			EntityClass<SubPK> subEoClass,
			String referenceAttributeId,
			RESTClient client
	) {
		String url = REST_BASE_URL +
				'/bos/' + eo.entityClass.fqn +
				'/' + eo.id +
				'/subBos/' + referenceAttributeId

		Map json = requestJson(url, HttpMethod.GET, client.sessionId, null)
		if (json == null) {
			return null;
		}

		json['bos'].collect { Map eoJson ->
			eoFromJson(subEoClass, eoJson, client)
		}
	}

	@PackageScope
	static <PK> EntityObject<PK> getEntityObject(EntityClass<PK> entityClass, PK id, RESTClient client) {
		String url = REST_BASE_URL + '/bos/' + entityClass.fqn + '/' + id

		Map json = requestJson(url, HttpMethod.GET, client.sessionId, null)

		eoFromJson(entityClass, json, client)
	}

	@PackageScope
	static <PK> void saveAll(List<EntityObject<PK>> eos, RESTClient client) {
		// TODO: Save all at once (see NUCLOS-5650)
		eos.each {
			save(it, client)
		}
	}

	@PackageScope
	static <PK> void save(EntityObject<PK> eo, RESTClient client) {
		Map json
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn

		if (eo.new) {
			json = postEntityObject(url, eo, client)
		} else {
			url += '/' + eo.id
			json = putEntityObject(url, eo, client)
		}

		if (json != null) {
			eo.updateFromJson(json)
		}
		eo.client = client
	}

	@PackageScope
	static <PK> void delete(EntityObject<PK> eo, RESTClient client) {
		String url = REST_BASE_URL + '/bos/' + eo.getEntityClass().fqn + '/' + eo.id
		requestString(url, HttpMethod.DELETE, client.sessionId)
	}

	@PackageScope
	static <PK> void changeState(EntityObject<PK> eo, int statusNumeral, RESTClient client) {
		String url = REST_BASE_URL + '/boStateChanges/' + eo.getEntityClass().fqn + '/' + eo.getId() + '/' + statusNumeral

		requestString(url, HttpMethod.PUT, client.sessionId)
	}

	private static <PK> Map postEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		requestJson(url, HttpMethod.POST, client.sessionId, eo.toJson())
	}

	private static <PK> Map putEntityObject(String url, EntityObject<PK> eo, RESTClient client) {
		requestJson(url, HttpMethod.PUT, client.sessionId, eo.toJson())
	}

	@PackageScope
	static Map<String, String> getSimpleSystemParameters(
			SuperuserRESTClient client
	) {
		getSystemParameters(client).collectEntries {
			[(it.getAttribute('name')): it.getAttribute('value')]
		}
	}

	@PackageScope
	static List<EntityObject<String>> getSystemParameters(
			SuperuserRESTClient client
	) {
		getEntityObjects(SystemEntities.PARAMETER, client)
	}

	@PackageScope
	static List<EntityObject<String>> setSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		params.each {
			delete(it, client)
		}

		patchSystemParameters(newParams, client)
	}

	@PackageScope
	static List<EntityObject<String>> patchSystemParameters(
			Map<String, String> newParams,
			SuperuserRESTClient client
	) {
		List<EntityObject<String>> params = getSystemParameters(client)

		newParams.each { String key, String value ->
			EntityObject<String> param = params.find { it.getAttribute('name') == key }

			if (!param) {
				param = new EntityObject(SystemEntities.PARAMETER)

				param.setAttribute('name', key)
				param.setAttribute('description', key)

				params << param
			}

			param.setAttribute('value', value)
			save(param, client)
		}

		return params
	}

	private static <PK> EntityObject<PK> eoFromJson(
			EntityClass<PK> entityClass,
			Map json,
			RESTClient client
	) {
		EntityObject<PK> eo = new EntityObject(entityClass)

		eo.updateFromJson(json)
		eo.client = client

		return eo
	}
}