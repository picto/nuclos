//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer.configuration;


import javax.swing.*;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityTreeViewVO;
import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

/**
 * ConfigurationEntityTreeSubNode
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ConfigurationEntityTreeSubNode extends DefaultConfigurationNode<EntityTreeViewVO> implements Comparable<ConfigurationEntityTreeSubNode>{

	
	
	private final String label;

	
	
	public ConfigurationEntityTreeSubNode(EntityTreeViewVO vo) {
		super(vo);
		this.label = MetaProvider.getInstance().getEntity(getContent().getEntity()).getEntityName();

	}

	public ConfigurationEntityTreeSubNode(ConfigurationEntityTreeSubNode node) {
		super(node);
		this.label = MetaProvider.getInstance().getEntity(getContent().getEntity()).getEntityName();
	}

	@Override
	public String getLabel() {
		return label;
	}
	
	@Override
	public String toString() {
		return getLabel();
	}

	@Override
	public UID getEntity() {
		return getContent().getEntity();
	}
	
	@Override
	public boolean isActive() {
		return getContent().isActive();
	}
	
	@Override
	public Icon getIcon() {
		if (Boolean.TRUE.equals(getContent().getIsInheritNodes())) {
			// FIXME Iconf for inheritNodes
			return null;
		}
		return super.getIcon();
	}

	@Override
	public int compareTo(ConfigurationEntityTreeSubNode o) {
		return LangUtils.compareComparables(this.getContent(), o.getContent());
	}
}