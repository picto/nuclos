package org.nuclos.client.explorer.node.eventsupport;

import javax.swing.Icon;

import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.client.ui.Icons;
import org.nuclos.server.navigation.treenode.TreeNode;

public class EventSupportUsageExplorerNode extends ExplorerNode<EventSupportUsageNode> {

	public EventSupportUsageExplorerNode(TreeNode treenode) {
		super(treenode);
		
	}

	public Icon getIcon() {

		Icon result = null;
		EventSupportUsageNode node = ((EventSupportUsageNode) getUserObject());
		
		EventSupportTargetType treeNodeType = node.getTreeNodeType();
		
		if (treeNodeType == null)
			 return null;
		
		
		switch (treeNodeType) 
		{
		case NUCLET:
			result = Icons.getInstance().getEventSupportNucletIcon();
			break;
		case EVENTSUPPORT:			
			result = Icons.getInstance().getEventSupportRuleIcon();
			break;
		case ENTITY:
			result = Icons.getInstance().getEventSupportEntityIcon();
			break;
		case ENTITY_INTEGRATION_POINT:
			result = Icons.getInstance().getEventSupportEntityIntegrationPointIcon();
			break;
		case STATEMODEL:
			result = Icons.getInstance().getEventSupportTransitionIcon();
			break;
		case JOB:
			result = Icons.getInstance().getEventSupportJobIcon();
			break;
		case GENERATION:
			result = Icons.getInstance().getEventSupportWorkstepIcon();
			break;
		default:
			break;
		}

		return result;
	}
}
