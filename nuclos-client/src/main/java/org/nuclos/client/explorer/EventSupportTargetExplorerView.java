package org.nuclos.client.explorer;

import java.awt.*;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.explorer.node.eventsupport.EventSupportDropListener;
import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.ui.CenteringPanel;
import org.nuclos.client.ui.ColoredLabel;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.navigation.treenode.TreeNode;

public class EventSupportTargetExplorerView  extends EventSupportExplorerView {


	public EventSupportTargetExplorerView(TreeNode tn, Map<EventSupportActions, AbstractAction> actions) {
		super(tn, actions);
		getJTree().addMouseListener(new EventSupportMouseListener(getJTree(), this.getPropertyPanel()));
	}
	
	protected void addDNDFunctionality(JTree pTree) {
		EventSupportDropListener dndListener = new EventSupportDropListener();
		DropTarget drpTaget = new DropTarget(pTree, 
		        DnDConstants.ACTION_MOVE, dndListener);
	}
	
	protected String getPreferenceNode() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORTS_TARGETS;
	}
	
	protected void addToolBarComponents(JToolBar toolbar) {
		setSearchTextField(new JTextField());
		final AbstractAction abstractAction = getActions().get(EventSupportActions.ACTION_RUN_TARGETTREE_SEARCH);
		getSearchTextField().addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {}
			
			@Override
			public void keyReleased(KeyEvent e) {}
			
			@Override
			public void keyPressed(KeyEvent e) {
				JTextField txt = (JTextField) e.getSource();
				if (txt.getText().length() > 3 || txt.getText().length() == 0) {
					abstractAction.actionPerformed(new ActionEvent(e.getSource(), e.getID(), null));
				}
			}
		});
			
		getSearchTextField().setPreferredSize(new Dimension(150,20));
		
		JPanel pnlSearch = new JPanel();
		pnlSearch.setLayout(new BorderLayout());
		pnlSearch.setOpaque(false);
		pnlSearch.add(getSearchTextField(), BorderLayout.WEST);

		CenteringPanel cpSearchFilter = new CenteringPanel(pnlSearch) {

			@Override
			public Dimension getMinimumSize() {
				return this.getCenteredComponent().getMinimumSize();
			}

			@Override
			public Dimension getMaximumSize() {
				return this.getCenteredComponent().getPreferredSize();
			}

		};
		cpSearchFilter.setOpaque(false);
		ColoredLabel bl = new ColoredLabel(cpSearchFilter, SpringLocaleDelegate.getInstance().getMessage("CollectController.Search.Filter","Filter"));
		bl.setName("blChooseFilter");
		

		toolbar.add(bl);
	}
	
}
