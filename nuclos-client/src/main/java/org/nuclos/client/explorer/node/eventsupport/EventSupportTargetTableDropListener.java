package org.nuclos.client.explorer.node.eventsupport;

import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;

public class EventSupportTargetTableDropListener implements DropTargetListener {

	private static final Logger LOG = Logger.getLogger(EventSupportTargetTableDropListener.class);
	
	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		dropTargetDrag(dtde);
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		dropTargetDrag(dtde);
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		dropTargetDrag(dtde);
	}

	@Override
	public void dragExit(DropTargetEvent dte) {}

	@Override
	public void drop(DropTargetDropEvent ev) {

		JTable table = (JTable) ((DropTarget)ev.getSource()).getComponent();
		EventSupportPropertiesTableModel model = (EventSupportPropertiesTableModel) table.getModel();
		
		// Dropped at element of table
		final int targetRow = table.rowAtPoint(ev.getLocation());
		final int srcRow = table.getSelectedRow();
		
		// Switch rows
		try {
			if (srcRow >= 0 && targetRow >= 0) {
				model.switchRows(srcRow, targetRow);
			} else {
				LOG.warn("DnD only supported for property row changes");
			}
		} catch (Exception e) {
			// Ok
			LOG.error(e.getMessage(), e);
		}
	}

	void dropTargetDrag(DropTargetDragEvent ev) {
	}
}
