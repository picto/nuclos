//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.explorer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.main.Main;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.searchfilter.EntitySearchFilter;
import org.nuclos.client.searchfilter.EntitySearchFilters;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.task.TaskController;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.SF;
import org.nuclos.common.SearchConditionUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;
import org.nuclos.server.statemodel.valueobject.StateVO;

public class WorkflowWizardPanel extends JPanel {
	
	private static final Logger LOG = Logger.getLogger(WorkflowWizardPanel.class);
	
	private String sUsername;
	
	public WorkflowWizardPanel(String userName) {
		super();
		this.sUsername = userName;
		init();
	}
	
	protected void init() {			
	}
	
	public void buildSearchFilter() {
		Collection<MasterDataVO<UID>> colSearchFilter = 
				MasterDataDelegate.getInstance().getMasterData(E.SEARCHFILTER.getUID());
		
		for(EntityMeta<?> vo : Modules.getInstance().getModules()) {
			//@TODO MultiNuclet.
			UID module = null;//vo.getFieldUnsafe("entity").getUID();
			
			for(StateVO voState : StateDelegate.getInstance().getStatesByModule(module)) {
				
				Collection<MasterDataVO<UID>> colRoleAttrGroup = 
						MasterDataDelegate.getInstance().getMasterData(E.ROLEATTRIBUTEGROUP.getUID(), 
								SearchConditionUtils.newComparison(E.ROLEATTRIBUTEGROUP.state, ComparisonOperator.EQUAL, voState.getStatename(LocaleDelegate.getInstance().getLocale())));
				
				for (MasterDataVO<UID> voRoleAttrGroup : colRoleAttrGroup) {
					final EntitySearchFilters filters = EntitySearchFilters.forEntity(module);
					final EntitySearchFilter f = new EntitySearchFilter();
					final SearchFilterVO filterVo = f.getSearchFilterVO();
					
					filterVo.setFilterName(module + " im Status " + voState.getNumeral());
					
					boolean blnaddFilter = true;
					for(MasterDataVO<UID> voSearchFilter : colSearchFilter) {
						if(filterVo.getFilterName().equals(voSearchFilter.getFieldValue(E.SEARCHFILTER.name.getUID()))) {
							blnaddFilter = false;
							break;
						}							
					}
					
					if(!blnaddFilter)
						continue;
					
					filterVo.setDescription(module + " im Status " + voState.getNumeral());					
					filterVo.setEntity(module);
					filterVo.setEditable(false);
					
					f.setSearchCondition(
							SearchConditionUtils.newComparison(SF.STATENUMBER.getUID(module), ComparisonOperator.EQUAL, voState.getNumeral()));
					
					List<CollectableEntityField> lst = new ArrayList<CollectableEntityField>();
					f.setVisibleColumns(lst);					
					try {
						filters.put(f);		
						
						EntitySearchFilter filter = filters.get(filterVo.getFilterName(), sUsername, filterVo.getEntity());
						TaskController taskCtrl = Main.getInstance().getMainController().getExplorerController().getTaskController();
						taskCtrl.cmdShowFilterInTaskPanel(filter);
					}
					catch (Exception e) {
						LOG.warn("buildSearchFilter failed: " + e);
					}
					
					String sGroup = (String)voRoleAttrGroup.getFieldValue(E.ROLEATTRIBUTEGROUP.role.getUID());
					Collection<MasterDataVO<UID>> colRoleUser = 
							MasterDataDelegate.getInstance().getMasterData(E.ROLEUSER.getUID(), SearchConditionUtils.newComparison(E.ROLEUSER.role.getUID(), ComparisonOperator.EQUAL, sGroup));
					
					for(MasterDataVO<UID> voRoleUser : colRoleUser) {
						
						if(sUsername.equals(voRoleUser.getFieldValue(E.ROLEUSER.user.getUID()))) 
							continue;
						
						try {
							MasterDataVO voSearchFilter = MasterDataDelegate.getInstance().getMasterData(E.SEARCHFILTER.getUID(), 
								SearchConditionUtils.newComparison(E.SEARCHFILTER.name.getUID(), ComparisonOperator.EQUAL, 
										filterVo.getFilterName())).iterator().next();
							
							EntityMeta metaVO = MetaProvider.getInstance().getEntity(E.SEARCHFILTERUSER.getUID());
							CollectableMasterDataEntity masterDataEntity = new CollectableMasterDataEntity(metaVO);
							CollectableMasterData masterData = new CollectableMasterData(masterDataEntity, new MasterDataVO(masterDataEntity.getMeta(), false));
							MasterDataVO<UID> mdvo = masterData.getMasterDataCVO();
							mdvo.setFieldValue(E.SEARCHFILTERUSER.searchfilter.getUID(), (UID) voSearchFilter.getPrimaryKey());
							mdvo.setFieldValue(E.SEARCHFILTERUSER.user.getUID(), voRoleUser.getFieldValue(E.ROLEUSER.user.getUID()));							
							mdvo.setFieldValue(E.SEARCHFILTERUSER.compulsoryFilter, false);
							mdvo.setFieldValue(E.SEARCHFILTERUSER.editable, false);
							mdvo.setFieldValue(E.SEARCHFILTERUSER.validFrom, null);
							mdvo.setFieldValue(E.SEARCHFILTERUSER.validUntil, null);
							
							IDependentDataMap mp = voSearchFilter.getDependents();
							mp.addData(E.SEARCHFILTERUSER.searchfilter, mdvo.getEntityObject());					
							MasterDataDelegate.getInstance().update(E.SEARCHFILTER.getUID(), voSearchFilter, mp, null, false);
						}
						catch(Exception e) {
							LOG.warn("buildSearchFilter failed: " + e);
						}				
					}
				}
			}			
		}
				
	}

}
