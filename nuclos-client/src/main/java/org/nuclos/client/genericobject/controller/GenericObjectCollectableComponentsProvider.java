package org.nuclos.client.genericobject.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.common.SF;
import org.nuclos.common.UID;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class GenericObjectCollectableComponentsProvider implements
		CollectableComponentsProvider {

	private final CollectableComponentsProvider mainProvider;
	private final UID entity;
	private final LabeledCollectableComponentWithVLP searchStateBox;

	public GenericObjectCollectableComponentsProvider(UID entity, CollectableComponentsProvider provider, LabeledCollectableComponentWithVLP searchStateBox) {
		this.mainProvider = provider;
		this.searchStateBox = searchStateBox;
		this.entity = entity;
	}

	@Override
	public Collection<CollectableComponent> getCollectableComponents() {
		Collection<CollectableComponent> mainComponents = mainProvider
				.getCollectableComponents();
		mainComponents.add(searchStateBox);
		return mainComponents;
	}

	@Override
	public Collection<CollectableComponent> getCollectableComponentsFor(UID field) {
		if (SF.STATE.getUID(this.entity).equals(field)
				|| SF.STATE_UID.getUID(this.entity).equals(field)
				|| SF.STATENUMBER.getUID(this.entity).equals(field)
				|| SF.STATEICON.getUID(this.entity).equals(field)) {
			final Collection<CollectableComponent> result = new ArrayList<CollectableComponent>();
			result.addAll(mainProvider.getCollectableComponentsFor(field));
			result.add(searchStateBox);
			return result;
		}
		return mainProvider.getCollectableComponentsFor(field);
	}

	@Override
	public Collection<CollectableComponent> getCollectableLabels() {
		return mainProvider.getCollectableLabels();
	}

} // inner class GenericObjectCollectableComponentsProvider
