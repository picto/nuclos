package org.nuclos.client.genericobject.controller;

import java.util.List;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.client.statemodel.StateWrapper;
import org.nuclos.common.UsageCriteria;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.statemodel.valueobject.StateVO;
import org.slf4j.LoggerFactory;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public final class GOUtils {
	private GOUtils() {
	}

	public static final int getNewSelectedRow(int iSelectedRow, int iRowCount) {
		final int iNewSelectedRow;
		if (iSelectedRow < iRowCount - 1)
			// the selected row is not the last row: select
			// the next row
			iNewSelectedRow = iSelectedRow;
		else if (iSelectedRow > 0)
			// the selected row is not the first row: select
			// the previous row
			iNewSelectedRow = iSelectedRow - 1;
		else {
			// the selected row is the single row: don't
			// select a row
			assert iRowCount == 1;
			assert iSelectedRow == 0;
			iNewSelectedRow = -1;
		}

		return iNewSelectedRow;
	}

	public static void copySubsequentStates(final List<StateVO> lstSubsequentStates, final StateWrapper stateCurrent,
											final List<StateWrapper> lstComboEntries, final UsageCriteria uc, CollectableGenericObjectWithDependants cgowd) {
		// Copy all subsequent states to the sorting list:
		for (StateVO statevo : lstSubsequentStates)
			if (statevo == null)
				// we don't want to throw an exception here, so we just
				// log the error:
				LoggerFactory.getLogger(GOUtils.class).error("Die Liste der Folgestatus enth\u00e4lt ein null-Objekt.");
			else if (stateCurrent != null
					&& !lstComboEntries
					.contains(new StateWrapper(
							statevo.getId(),
							statevo.getNumeral(),
							statevo.getStatename(LocaleDelegate.getInstance().getLocale()),
							statevo.getIcon(),
							SpringLocaleDelegate
									.getInstance()
									.getResource(
											StateDelegate
													.getInstance()
													.getResourceSIdForDescription(
															statevo.getId()),
											statevo.getDescription(LocaleDelegate.getInstance().getLocale())),
							null, null)))
				lstComboEntries
						.add(new StateWrapper(
								statevo.getId(),
								statevo.getNumeral(),
								SpringLocaleDelegate
										.getInstance()
										.getResource(
														/*
														 * StateDelegate.getInstance
														 * (
														 * ).getResourceSIdForName
														 * (statevo.getId()
														 */
												StateDelegate
														.getInstance()
														.getStatemodelClosure(
																uc.getEntityUID())
														.getResourceSIdForLabel(
																statevo.getId()),
												statevo.getStatename(LocaleDelegate.getInstance().getLocale())),
								statevo.getIcon(),
								SpringLocaleDelegate
										.getInstance()
										.getResource(
												StateDelegate
														.getInstance()
														.getResourceSIdForDescription(
																statevo.getId()),
												statevo.getDescription(LocaleDelegate.getInstance().getLocale())),
								statevo.getColor(),
								statevo.getButtonIcon(),
								statevo.isFromAutomatic(),
								statevo.getNonStopSourceStates(),
								cgowd != null ? StateDelegate
										.getInstance()
										.getStatemodel(uc)
										.isStateReachableInDefaultPath(
												stateCurrent.getId(),
												statevo)
										: StateDelegate
										.getInstance()
										.getStatemodel(uc)
										.isStateReachableInDefaultPathByNumeral(
												stateCurrent
														.getNumeral(),
												statevo)));
	}
}
