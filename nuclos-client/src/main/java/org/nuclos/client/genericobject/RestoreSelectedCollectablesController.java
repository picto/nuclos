//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject;


import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.multiaction.MultiCollectablesActionController;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Controller for restoring multiple (selected) <code>Collectable</code>.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:corina.mandoki@novabit.de">Corina Mandoki</a>
 * @version 01.00.00
 */
public class RestoreSelectedCollectablesController<PK>
		extends MultiCollectablesActionController<PK,Collectable<PK>, Object> {
	public RestoreSelectedCollectablesController(CollectController<PK, Collectable<PK>> ctl) {
		super(ctl, SpringLocaleDelegate.getInstance().getMessage("RestoreSelectedCollectablesController.5", "Datens\u00e4tze wiederherstellen"), 
				new RestoreAction<PK>(ctl), ctl.getSelectedCollectables());
	}

}	// class RestoreSelectedCollectablesController
