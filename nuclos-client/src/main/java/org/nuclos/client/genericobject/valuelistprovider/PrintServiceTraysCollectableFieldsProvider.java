//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.printservice.PrintServiceFacadeRemote;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common2.exception.CommonBusinessException;

public class PrintServiceTraysCollectableFieldsProvider implements
		CollectableFieldsProvider {
	
	public static final String PRINT_SERVICE = "printService";
	
	private UID printServiceId;

	@Override
	public void setParameter(String sName, Object oValue) {
		if (PRINT_SERVICE.equals(sName) && oValue instanceof UID) {
			printServiceId = (UID) oValue;
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		if (printServiceId != null) {
			PrintServiceTO printService = SpringApplicationContextHolder.getBean(PrintServiceFacadeRemote.class).printService(printServiceId);
			for (PrintServiceTO.Tray tray : printService.getTrays()) {
				result.add(new CollectableValueIdField(tray.getId(), tray.getPresentation()));
			}
		}
		
		return result;
	}

}
