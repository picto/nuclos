package org.nuclos.client.genericobject.controller;

import javax.swing.*;

import org.nuclos.client.ui.collect.CollectableComponentsProvider;
import org.nuclos.client.ui.collect.DefaultEditView;
import org.nuclos.client.ui.collect.component.model.EditModel;
import org.nuclos.common2.EntityAndField;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class GenericObjectEditView extends DefaultEditView {

	public GenericObjectEditView(JComponent compRoot,
								 CollectableComponentsProvider clctcompprovider,
								 EditModel model, boolean bForSearch,
								 EntityAndField initialFocusField) {
		super(compRoot, clctcompprovider, model, initialFocusField);
	}

} // inner class GenericObjectEditView
