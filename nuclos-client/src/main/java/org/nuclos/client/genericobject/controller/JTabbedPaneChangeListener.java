package org.nuclos.client.genericobject.controller;

import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.nuclos.client.genericobject.GenericObjectCollectController;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class JTabbedPaneChangeListener implements ChangeListener {

	public static final String TABSELECTED = "tabselected";

	private final Preferences preferences;

	public JTabbedPaneChangeListener(Preferences preferences) {
		this.preferences = preferences;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JTabbedPane pane = (JTabbedPane) e.getSource();
		int idx = pane.getSelectedIndex();
		if (idx != -1) {
			String sTitle = pane.getComponentAt(idx).getName();
			if (sTitle != null) {
				preferences.put(TABSELECTED, sTitle);
			}
		}
	}
}
