//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.genericobject.valuelistprovider;

import java.lang.reflect.Constructor;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosCollectableEntityProvider;
import org.nuclos.client.datasource.DatasourceDelegate;
import org.nuclos.client.masterdata.valuelistprovider.DatasourceBasedCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.DependantMasterDataCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.GenericCollectableFieldsProvider;
import org.nuclos.client.masterdata.valuelistprovider.MasterDataCollectableFieldsProviderFactory;
import org.nuclos.client.masterdata.valuelistprovider.ParametersCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBObjectCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBObjectTypeCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.DBTypeCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.EntityCollectableFieldsProvider;
import org.nuclos.client.valuelistprovider.EntityCollectableIdFieldsProvider;
import org.nuclos.client.valuelistprovider.cache.CollectableFieldsProviderCache;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableFieldsProviderFactory;
import org.nuclos.common.collect.collectable.ValueListProviderType;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * Factory that creates <code>CollectableFieldProvider</code>s for <code>CollectableGenericObject</code>s.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class GenericObjectCollectableFieldsProviderFactory implements CollectableFieldsProviderFactory {

	private final CollectableEntity clcte;

	public GenericObjectCollectableFieldsProviderFactory(UID entityUid) {
		this.clcte = (entityUid == null) ? null :  NuclosCollectableEntityProvider.getInstance().getCollectableEntity(entityUid);
	}

	@Override
	public CollectableFieldsProvider newDefaultCollectableFieldsProvider(UID fieldUid) {
		final UID entityUid = MetaProvider.getInstance().getEntityField(fieldUid).getEntity();
		if (entityUid == null || entityUid.equals(clcte.getUID())) {
			return new GenericObjectCollectableFieldsProvider(clcte.getEntityField(fieldUid));
		} else {
			return MasterDataCollectableFieldsProviderFactory.newMasterDataCollectableFieldsProvider(entityUid, fieldUid);
		}
	}

	@Override
	public CollectableFieldsProvider newDependantCollectableFieldsProvider(UID fieldUid) {
		final UID entityUid = MetaProvider.getInstance().getEntityField(fieldUid).getEntity();
		if (entityUid == null || entityUid.equals(clcte.getUID())) {
			final CollectableEntityField clctef = clcte.getEntityField(fieldUid);
			if (!clctef.isReferencing()) {
				throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
						"GenericObjectCollectableFieldsProviderFactory.1",
						"Das Feld {0} in der Entit\u00e4t {1} referenziert keine andere Entit\u00e4t.", 
						clctef.getUID(), clcte.getUID()));
			}
			return new DependantMasterDataCollectableFieldsProvider(clcte.getUID(), fieldUid);
		}
		
		throw new NotImplementedException();
	}

	/**
	 * @deprecated Use {@link #newCustomCollectableFieldsProvider(ValueListProviderType, UID)} instead.
	 */
	@Override
	public CollectableFieldsProvider newCustomCollectableFieldsProvider(String sCustomType, UID fieldUid) {
		if (UID.isStringifiedUID(sCustomType)) { //@todo. normaly we have to give type information here.
			UID baseEntityUID = null;
			if(fieldUid != null) {
				try {
					FieldMeta<?> fmeta = MetaProvider.getInstance().getEntityField(fieldUid);
					baseEntityUID = fmeta.getForeignEntity()!=null? fmeta.getForeignEntity(): fmeta.getLookupEntity();
				} catch (CommonFatalException ex) {
					// ignore
				}
			}
			return new DatasourceBasedCollectableFieldsProvider(true, UID.parseUID(sCustomType), DatasourceDelegate.getInstance(), baseEntityUID);
		}
		return newCustomCollectableFieldsProvider(ValueListProviderType.toValueListProviderType(sCustomType), fieldUid);
	}
	
	@Override
	public CollectableFieldsProvider newCustomCollectableFieldsProvider(ValueListProviderType sCustomType, UID fieldUid) {
		if (sCustomType == null)
			throw new IllegalArgumentException("ValueListProviderType has not to be null!");

		final CollectableFieldsProvider result;
		switch (sCustomType) {
		case ENTITY:
			result = new EntityCollectableFieldsProvider();
			break;
		case ENTITY_ID:
			result = new EntityCollectableIdFieldsProvider();
			break;
		case PROCESS:
			result = new ProcessCollectableFieldsProvider();
			break;
		case MANDATOR:
			result = new MandatorCollectableFieldsProvider(null, fieldUid);
			break;
		case ATTRIBUTE:
			result = new AttributeCollectableFieldsProvider();
			break;
		case DATASOURCE:
			FieldMeta<?> fmeta = MetaProvider.getInstance().getEntityField(fieldUid);
			UID baseEntityUID = fmeta.getForeignEntity()!=null? fmeta.getForeignEntity(): fmeta.getLookupEntity();
			result = new DatasourceBasedCollectableFieldsProvider(false, DatasourceDelegate.getInstance(), baseEntityUID);
			break;
		case PARAMETERS:
			result = new ParametersCollectableFieldsProvider();
			break;
		case STATUS:
			result = new StatusCollectableFieldsProvider();
			break;
		case GENERIC:
			result = new GenericCollectableFieldsProvider();
			break;
		case DB_TYPE:
			result = new DBTypeCollectableFieldsProvider();
			break;
		case DB_OBJECT_TYPE:
			result = new DBObjectTypeCollectableFieldsProvider();
			break;
		case DB_OBJECT:
			result = new DBObjectCollectableFieldsProvider();
			break;
		default:
			throw new NuclosFatalException(SpringLocaleDelegate.getInstance().getMessage(
					"GenericObjectCollectableFieldsProviderFactory.2", "Unbekannter valuelist-provider Typ: {0}", sCustomType));
		}
		return result;
	}

	public static CollectableFieldsProviderFactory newFactory(UID entity) {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getGenericObjectCollectableFieldsProviderFactoryClassName(),
					GenericObjectCollectableFieldsProviderFactory.class.getName());

			final Class<? extends GenericObjectCollectableFieldsProviderFactory> clsgocfpf 
				= LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).asSubclass(GenericObjectCollectableFieldsProviderFactory.class);
			final Constructor<? extends GenericObjectCollectableFieldsProviderFactory> ctor = clsgocfpf.getConstructor(UID.class);

			return ctor.newInstance(entity);
		}
		catch (Exception ex) {
			throw new CommonFatalException("GenericObjectCollectableFieldsProviderFactory cannot be created.", ex);
		}
	}
	
	public static CollectableFieldsProviderFactory newFactory(UID entity, CollectableFieldsProviderCache cache) {
		CollectableFieldsProviderFactory factory = newFactory(entity);
		return (cache != null) ? cache.makeCachingFieldsProviderFactory(factory) : factory;
	}

}	// class GenericObjectCollectableFieldsProviderFactory
