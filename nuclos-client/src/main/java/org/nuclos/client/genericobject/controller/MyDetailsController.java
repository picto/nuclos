package org.nuclos.client.genericobject.controller;

import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectCollectController;
import org.nuclos.client.ui.collect.detail.DetailsController;
import org.nuclos.client.ui.collect.toolbar.DeleteOrRestoreToggleAction;

/**
 * Created by Oliver Brausch on 18.07.17.
 */
public class MyDetailsController extends DetailsController<Long, CollectableGenericObjectWithDependants> {

	private final DeleteOrRestoreToggleAction actDeleteCurrentCollectableInDetails;

	public MyDetailsController(GenericObjectCollectController controller, DeleteOrRestoreToggleAction actDeleteCurrentCollectableInDetails) {
		super(controller);
		this.actDeleteCurrentCollectableInDetails = actDeleteCurrentCollectableInDetails;
	}

	@Override
	public DeleteOrRestoreToggleAction getDeleteCurrentCollectableAction() {
		return actDeleteCurrentCollectableInDetails;
	}
}
