package org.nuclos.client.layout.wysiwyg;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LayoutXMLContentWriter implements LayoutMLConstants{

	private static final Logger LOG = Logger.getLogger(LayoutXMLContentWriter.class);
	private static final String SYSTEMID = "http://www.novabit.de/technologies/layoutml/layoutml.dtd";
	private static final String RESOURCE_PATH = "org/nuclos/schema/layout/layoutml/layoutml.dtd";
	
	private Document xmlDocument;
	
	public LayoutXMLContentWriter() {
		this.xmlDocument = null;
	}
	
	public synchronized void parse(String layoutAsString) throws ParserConfigurationException, SAXException, IOException{

		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		
		builder.setEntityResolver(new EntityResolver() {
			
			@Override
			public InputSource resolveEntity(String publicId, String systemId)
					throws SAXException, IOException {
				InputSource result = null;
				if (systemId.equals(SYSTEMID)) {
					final URL url = LangUtils.getClassLoaderThatWorksForWebStart().getResource(RESOURCE_PATH);
					if (url == null) {
						NuclosFatalException nuclosFatalException = new NuclosFatalException("Missing DTD for SYSTEMID " + SYSTEMID);
						LOG.error(nuclosFatalException);
						throw nuclosFatalException;
					}
					result = new InputSource(new BufferedInputStream(url.openStream()));
				}
				return result;
			}
		});
		
		// Parse document
		this.xmlDocument= builder.parse(new InputSource(new StringReader(layoutAsString)));	
	}
	
	public NodeList getButtons() throws IllegalStateException{
		
		NodeList retVal = null;
		
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		retVal = this.xmlDocument.getElementsByTagName(ELEMENT_BUTTON);
	
		return retVal;
	}
	
	public Node getAttribute(Node elm, String attribute)throws IllegalStateException{
		
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		if (elm == null) 
			throw new IllegalStateException("Node is null");
		
		if (attribute == null) 
			throw new IllegalStateException("Attribute is null");
		
		if (!elm.hasAttributes())
			return null;
		
		return getAttributes(elm).getNamedItem(attribute);
	}
	
	public NamedNodeMap getAttributes(Node elm) throws IllegalStateException{
		
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		return elm.getAttributes();
	}
	
	public void setAttribute(Node node, Node attribute) throws IllegalStateException {
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		
		if (node == null) 
			throw new IllegalStateException("Node is null");
		
		if (attribute == null) 
			throw new IllegalStateException("New attribute is null");
		
		
		node.getAttributes().setNamedItem(attribute);
	}

	public Node getChild(Node parent, String nodeName) throws IllegalStateException {
		if (this.xmlDocument == null) 
			throw new IllegalStateException("XML Document is null");
		if (parent == null) 
			throw new IllegalStateException("Node is null");
		if (nodeName == null) 
			throw new IllegalStateException("NodeName is null");
		
		if (!parent.hasChildNodes())
			return null;
		
		NodeList childNodes = parent.getChildNodes();
		for (int idx=0; idx < childNodes.getLength(); idx++) {
			Node item = childNodes.item(idx);
			if(nodeName.equals(item.getNodeName())) {
				return item;
			}
		}
		return null;
		
	}
	
}
