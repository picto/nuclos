//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout;

import org.apache.fop.layoutmgr.LayoutContext;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.layout.LayoutComponent.LayoutComponentType;
import org.nuclos.common.UID;

/**
 * Default implementation for layout context {@link LayoutContext}
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class LayoutComponentContextImpl implements
		LayoutComponentContext {
	

	private final UID entity;
	private final LayoutComponentType layoutComponentType;

	public LayoutComponentContextImpl(final UID entity, final LayoutComponentType layoutComponentType) {
		this.entity = entity;
		this.layoutComponentType = layoutComponentType;
	}
	
	@Override
	public UID getEntityUid() {
		return this.entity;
	}

	@Override
	public LayoutComponentType getType() {
		return this.layoutComponentType;
	}

}
