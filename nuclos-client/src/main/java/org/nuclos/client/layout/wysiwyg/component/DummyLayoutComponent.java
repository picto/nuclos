package org.nuclos.client.layout.wysiwyg.component;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;

import javax.swing.*;

import org.nuclos.api.Preferences;
import org.nuclos.api.Property;
import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentListener;
import org.nuclos.api.ui.layout.TabController;

public class DummyLayoutComponent<PK> extends JLabel implements LayoutComponent<PK> {
	
	private LayoutComponentContext context;

	public DummyLayoutComponent(String label, LayoutComponentContext context) {
		super("LayoutComponent " + label);
		this.context = context;
	}

	@Override
	public JComponent getComponent(LayoutComponentType type) {
		return this;
	}

	@Override
	public void setPreferences(Preferences prefs) {
	}

	@Override
	public void setProperty(String name, Object value) {
	}

	@Override
	public Property[] getComponentProperties() {
		return null;
	}

	@Override
	public String[] getComponentPropertyLabels() {
		return null;
	}

	@Override
	public LayoutComponentContext getContext() {
		return context;
	}

	@Override
	public EnumSet<LayoutComponentType> getSupportedTypes() {
		// TODO Auto-generated method stub
		return EnumSet.of(LayoutComponentType.DESIGN, LayoutComponentType.DETAIL);
	}

	@Override
	public Collection<LayoutComponentListener<PK>> getLayoutComponentListeners() {
		return Collections.emptyList();
	}

	@Override
	public void addLayoutComponentListener(LayoutComponentListener<PK> listener) {
	}

	@Override
	public void removeLayoutComponentListener(LayoutComponentListener<PK> listener) {
	}

	@Override
	public void setTabController(TabController tabController) {
	}

}
