package org.nuclos.client.layout.wysiwyg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.nuclos.client.layout.wysiwyg.WYSIWYGStringsAndLabels.STATIC_BUTTON;
import org.nuclos.common2.layoutml.LayoutMLConstants;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class LayoutXMLContentHandler implements ContentHandler, LayoutMLConstants {

	private static final Logger LOG = Logger.getLogger(LayoutXMLContentHandler.class);
	
	private Map<String, ElementProcessor> mapProcessors;

	private Stack<RuleOnStack> stack = new Stack<RuleOnStack>();


	/**
	 * The WYSIWYGEditorPanel inculdes the TableLayoutPanel. So these 2
	 * panels would be represented in the LayoutML with 2 &lt;panel&gt; tags. But
	 * when the Editor loads the LayoutML we have to "ignore" one of this
	 * tags because with creating the WYSIWYGEditorPanel a TableLayoutPanel
	 * is also created.
	 */
	public LayoutXMLContentHandler() {
	
		this.mapProcessors = new HashMap<String, ElementProcessor>();
		this.mapProcessors.put(ELEMENT_BUTTON, new ButtonElementProcessor());
		this.mapProcessors.put(ELEMENT_PROPERTY, new PropertyElementProcessor());
	}


	/**
	 * Method called by the SAX Parser
	 *
	 * Element is coming in and the fitting processor is taken from
	 * {@link #mapProcessors} and the Attributes are passed.
	 */
	@Override
	public synchronized void startElement(String uri, String localName, String name, Attributes atts) throws SAXException {
		ElementProcessor ep = mapProcessors.get(name);
		if (ep != null) {
			ep.startElement(atts);
		} 
	}

	public List<RuleOnStack> getAllRules() {
		return this.stack;
	}
	/**
	 * Method called by the SAX Parser
	 *
	 * Element has ended and the fitting ElementProcessor is taken from {@link #mapProcessors} 
	 * and calls {@link ElementProcessor#closeElement()}.
	 */
	@Override
	public synchronized void endElement(String uri, String localName, String name) throws SAXException {
		ElementProcessor ep = mapProcessors.get(name);
		if (ep != null) {
			ep.closeElement();
		}
	}

	/**
	 * the chars that are collected between start and end tags.
	 */
	private StringBuffer sbChars;

	@Override
	public synchronized void characters(char[] ch, int start, int length) throws SAXException {
		if (this.sbChars != null) {
			this.sbChars.append(ch, start, length);
		}
	}

	@Override
	public void endDocument() throws SAXException {}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {}

	@Override
	public void processingInstruction(String target, String data) throws SAXException {}

	@Override
	public void setDocumentLocator(Locator locator) {}

	@Override
	public void skippedEntity(String name) throws SAXException {}

	@Override
	public void startDocument() throws SAXException {}

	@Override
	public void startPrefixMapping(String prefix, String uri) throws SAXException {}

	private class PropertyElementProcessor implements ElementProcessor  {
		@Override
		public void startElement(Attributes atts) throws SAXException {
			String btnRuleName = atts.getValue("name");
			if (btnRuleName.equals(STATIC_BUTTON.EXECUTE_RULE_ACTION_ARGUMENT)) {
				String btnRuleValue = atts.getValue("value");	
				stack.get(stack.size()-1).setName(btnRuleValue);
			}
		}

		@Override
		public void closeElement() throws SAXException {
			
		}
	}
	private class ButtonElementProcessor implements ElementProcessor {

		@Override
		public void startElement(Attributes atts) throws SAXException {
			String btnName = atts.getValue("name");
			String btnAction = atts.getValue("actioncommand");
			String btnLabel = atts.getValue("label");
			String btnEnabled = atts.getValue("enabled");
		
			stack.add(new RuleOnStack(btnName, btnAction, btnLabel, btnEnabled));
		}

		@Override
		public void closeElement() throws SAXException {
			
		}
	}
	
	private class RuleOnStack {
		
		private String name;
		private String btnName;
		private String btnaction;
		private String btnLabel;
		private String btnEnabled;
		
		public RuleOnStack(String btnName, String btnaction, String btnLabel,
				String btnEnabled) {
			super();
			this.btnName = btnName;
			this.btnaction = btnaction;
			this.btnLabel = btnLabel;
			this.btnEnabled = btnEnabled;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getBtnName() {
			return btnName;
		}

		public String getBtnaction() {
			return btnaction;
		}

		public String getBtnLabel() {
			return btnLabel;
		}

		public String getBtnEnabled() {
			return btnEnabled;
		}
	}
}

/**
 * Small Interface used by the Parser for processing the SAX XML Events
 */
interface ElementProcessor {
	
	void startElement(Attributes atts) throws SAXException;
	
	void closeElement() throws SAXException;
}
