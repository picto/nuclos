//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.util.valueobjects;

import java.io.Serializable;
import java.util.Vector;

import org.nuclos.common2.LangUtils;

/**
 * This Class collects multiple Properties.
 * 
 * 
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * 
 * @author <a href="mailto:hartmut.beckschulze@novabit.de">hartmut.beckschulze</a>
 * @version 01.00.00
 */
public class WYSIWYGProperty implements Cloneable, Serializable{
	
	public Vector<WYSIWYGPropertySet> properties = null;

	public WYSIWYGProperty(){
		properties = new Vector<WYSIWYGPropertySet>(1);
	}
	
	/** 
	 * Add a {@link WYSIWYGPropertySet} to this {@link WYSIWYGProperty}
	 * @param wysiwygPropertySet to add
	 */
	public void addWYSIYWYGPropertySet(WYSIWYGPropertySet wysiwygPropertySet){
		for (WYSIWYGPropertySet propertySet : new Vector<WYSIWYGPropertySet>(properties)) {
			if (propertySet.getPropertyName().equals(wysiwygPropertySet.getPropertyName()))
				removeWYSIYWYGPropertySet(propertySet);
		}
		properties.add(wysiwygPropertySet);
	}
	
	/**
	 * Remove a {@link WYSIWYGPropertySet} from this {@link WYSIWYGProperty}
	 * @param wysiwygPropertySet to remove
	 */
	public void removeWYSIYWYGPropertySet(WYSIWYGPropertySet wysiwygPropertySet){
		properties.remove(wysiwygPropertySet);
	}
	
	/** 
	 * @return all {@link WYSIWYGPropertySet} stored in this {@link WYSIWYGProperty}
	 */
	public Vector<WYSIWYGPropertySet> getAllPropertyEntries(){
		return this.properties;
	}
	
	/**
	 * @return the Number of {@link WYSIWYGPropertySet} stored
	 */
	public int getSize() {
		return properties.size();
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof WYSIWYGProperty))
			return false;
		WYSIWYGProperty that = (WYSIWYGProperty)obj;
		
		if(that.getSize() != this.getSize())
			return false;
		
		if(that.getAllPropertyEntries().containsAll(getAllPropertyEntries()))
			return true;
		
		return false;		
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hashCode(properties);
	}

	/** 
	 * Overwritten clone Method to create a new Instance of this {@link WYSIWYGProperty}.
	 * calls {@link WYSIWYGPropertySet#clone()}
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		WYSIWYGProperty clonedProperty = new WYSIWYGProperty();
		
		for (WYSIWYGPropertySet propertySet : properties) {
			clonedProperty.addWYSIYWYGPropertySet((WYSIWYGPropertySet)propertySet.clone());
		}
		
		return clonedProperty ;
	}
}
