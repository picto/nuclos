//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.layout.wysiwyg.editor.util;

import javax.swing.event.EventListenerList;

import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.LayoutMLRuleEditorDialog;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleAttachEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleListEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.RuleRemoveEvent;
import org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event.listener.RuleListListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * LayoutML Rule Editor Controller
 * 
 * controller for Layout ML Editor events
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 */
public class LayoutMLRuleEditorController {
	private final static Logger LOG = LoggerFactory.getLogger(LayoutMLRuleEditorController.class);
	private final LayoutMLRuleEditorDialog editor;
	private final EventListenerList listenerList;
	public LayoutMLRuleEditorController(final LayoutMLRuleEditorDialog editor) {
		this.editor = editor;
		this.listenerList = new EventListenerList();
	}

	/**
	 * add rule list listener
	 * 
	 * @param listener {@link RuleListListener}
	 */
	public void addRuleListListener(final RuleListListener listener) {
		LOG.trace("registered  {} as {}", listener.getClass(), RuleListListener.class.getName());
		listenerList.add(RuleListListener.class, listener);
	}

	/**
	 * remove rule list listener
	 * 
	 * @param listener {@link RuleListListener}
	 */
	public void removeRuleListListener(final RuleListListener listener) {
		LOG.trace("removed {} from {}", listener.getClass(), RuleListListener.class.getName());
		listenerList.remove(RuleListListener.class, listener);
	}

	/**
	 * firef rule list event
	 * 
	 * @param evt	{@link RuleListEvent} list event i.e. {@link RuleAttachEvent} or {@link RuleRemoveEvent}
	 */
	public void fireRuleListEvent(RuleListEvent evt) {
		LOG.trace("fire rule list modified {}", evt.toString());
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i = i+2) {
			if (listeners[i] == RuleListListener.class) {
				((RuleListListener) listeners[i+1]).ruleListEvent(evt);
			}
		}
	}

	/**
	 * get rule list editor
	 * 
	 * @return
	 */
	public LayoutMLRuleEditorDialog getEditor() {
		return editor;
	}
	
	
	
}
