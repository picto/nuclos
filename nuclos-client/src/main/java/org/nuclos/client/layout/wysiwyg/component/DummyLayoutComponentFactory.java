package org.nuclos.client.layout.wysiwyg.component;

import javax.swing.*;

import org.nuclos.api.context.LayoutComponentContext;
import org.nuclos.api.ui.Alignment;
import org.nuclos.api.ui.layout.LayoutComponent;
import org.nuclos.api.ui.layout.LayoutComponentFactory;

public class DummyLayoutComponentFactory implements LayoutComponentFactory {

	private final String label;
	
	public DummyLayoutComponentFactory(String label) {
		super();
		this.label = label;
	}

	@Override
	public LayoutComponent newInstance(LayoutComponentContext context) {
		return new DummyLayoutComponent(label, context);
	}

	@Override
	public String getName() {
		return "Dummy";
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	@Override
	public Alignment getDefaulAlignment() {
		return null;
	}

	@Override
	public Object getDefaultPropertyValue(String property) {
		return null;
	}

}
