package org.nuclos.client.layout.wysiwyg.editor.ui.panels.layoutmlrule.event;

/**
 * Rule List Event 
 * various rule list actions i.e. attach or remove rule
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface RuleListEvent {}
