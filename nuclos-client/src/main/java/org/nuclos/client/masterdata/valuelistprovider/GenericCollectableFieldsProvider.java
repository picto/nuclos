//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.entityobject.EntityFacadeDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * generic valuelistprovider for all entities (genericobject and masterdata).
 *
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 01.00.00
 */
public class GenericCollectableFieldsProvider implements CollectableFieldsProvider, MandatorRestrictionCollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(GenericCollectableFieldsProvider.class);
	
	public static final String STRINGIFIED_FIELD_DEFINITION = "field";
	
	public static final String VALID = "valid";
	
	//
	
	private UID entityUid;
	private String stringifiedFieldDefinition;
	private boolean valid = false;
	private UID mandator;
	
	/**
	 * @deprecated
	 */
	public GenericCollectableFieldsProvider() {
	}
	
	public GenericCollectableFieldsProvider(UID entityUid, String stringifiedFieldDefinition, boolean valid) {
		setParameter(NuclosConstants.VLP_ENTITY_UID_PARAMETER, entityUid);
		setParameter(STRINGIFIED_FIELD_DEFINITION, stringifiedFieldDefinition);
		setParameter(VALID, Boolean.valueOf(valid));
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String name, Object value) {
		if (NuclosConstants.VLP_ENTITY_UID_PARAMETER.equals(name)) {
			entityUid = (UID) value;
		} else if (STRINGIFIED_FIELD_DEFINITION.equals(name)) {
			stringifiedFieldDefinition = value.toString();
		} else if (VALID.equals(name)) {
			valid = Boolean.parseBoolean(value.toString());
		} else if (NuclosConstants.VLP_MANDATOR_PARAMETER.equals(name)) {
			mandator = (UID) value;
		} else {
			LOG.info("Unknown parameter " + name + " with value " + value);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		final List<CollectableField> result;

		if (Modules.getInstance().isModule(entityUid)) {
		
			UID datalanguage = DataLanguageContext.getDataUserLanguage() != null ?
					DataLanguageContext.getDataUserLanguage() : DataLanguageContext.getDataSystemLanguage();
		
			result = EntityFacadeDelegate.getInstance().getCollectableFieldsByName(entityUid, stringifiedFieldDefinition, false, mandator, datalanguage);
		}
		else {
			final CollectableFieldsProvider masterdataCollectableFieldsProvider =
					new MasterDataCollectableFieldsProvider(entityUid, stringifiedFieldDefinition, !valid, mandator);
			result = masterdataCollectableFieldsProvider.getCollectableFields();
		}

		return result;
	}

	@Override
	public UID getMandator() {
		return mandator;
	}
}
