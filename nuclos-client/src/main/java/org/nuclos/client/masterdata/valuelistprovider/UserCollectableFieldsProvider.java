//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * {@link CollectableFieldsProvider} which is filled with the values of a given Enum class.
 */
public class UserCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(UserCollectableFieldsProvider.class);
	
	private UID userEntityUid = E.USER.getUID();
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		List<MasterDataVO<?>> users = MasterDataCache.getInstance().get(userEntityUid);
        List<LocalizedCollectableValueField> lcfs = CollectionUtils.transform(users, new Transformer<MasterDataVO<?>,LocalizedCollectableValueField>(){
    		@Override
    		public LocalizedCollectableValueField transform(MasterDataVO<?> mvo) {
    			return new LocalizedCollectableValueField(mvo.getFieldValue(E.USER.name.getUID(), String.class), 
    					mvo.getFieldValue(E.USER.lastname.getUID(), String.class)
    					+ "," + mvo.getFieldValue(E.USER.firstname.getUID(), String.class));
    		}}
        );
        result.addAll(lcfs);
		Collections.sort(result);
		return result;
	}

}	// class RoleActionsCollectableFieldsProvider
