//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get importable masterdata.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:thomas.schiffmann@novabit.de">thomas.schiffmann</a>
 * @version 00.01.000
 */
public class ImportEntityCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(ImportEntityCollectableFieldsProvider.class);
	
	public ImportEntityCollectableFieldsProvider() {
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		Collection<EntityMeta<?>> colemdvo = new ArrayList<EntityMeta<?>>();
		for (EntityMeta<?> eMeta : MetaProvider.getInstance().getAllEntities()) {
			if (E.isNuclosEntity(eMeta.getUID())) {
				continue;
			}
			colemdvo.add(eMeta);
		}
		final List<CollectableField> result = CollectionUtils.transform(colemdvo, new Transformer<EntityMeta<?>, CollectableField>() {
			@Override
			public CollectableField transform(EntityMeta<?> emdVO) {
				return new CollectableValueIdField(emdVO.getUID(), emdVO.getEntityName());
			}
		});
		Collections.sort(result);
		return result;
	}

}
