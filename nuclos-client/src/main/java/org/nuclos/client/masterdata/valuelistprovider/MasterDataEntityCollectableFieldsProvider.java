//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.LocalizedCollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Value list provider to get all masterdata entities containing a menupath.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class MasterDataEntityCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(MasterDataEntityCollectableFieldsProvider.class);
	
	public static final String ENTITY_WITH_STATEMODEL = "module";
	
	//

	private boolean bModule = false;
	
	/**
	 * @deprecated
	 */
	MasterDataEntityCollectableFieldsProvider() {
	}
	
	public MasterDataEntityCollectableFieldsProvider(boolean isEntityWithStatemodel) {
		this.bModule = isEntityWithStatemodel;
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		//modules for rule usages
		if (sName.equals(ENTITY_WITH_STATEMODEL)) {
			bModule = Boolean.valueOf(oValue.toString());
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final MetaProvider mProv = MetaProvider.getInstance();
		final Set<EntityMeta<?>> colmdmVO_menupath = new HashSet<EntityMeta<?>>();
		final Collection<EntityMeta<?>> colmdmVO = mProv.getAllEntities();

		// get and add entities with menupath
		for(EntityMeta<?> mdmVO : colmdmVO) {
			if (bModule) {
				if (Modules.getInstance().isModule(mdmVO.getUID())) {
					if (mdmVO.getLocaleResourceIdForMenuPath() != null) {
						colmdmVO_menupath.add(mdmVO);
					} else {
						// add entites without an menupath too if not a system entity
						if (!E.isNuclosEntity(mdmVO.getUID()) && !MasterDataDelegate.getInstance().isSubformEntity(mdmVO.getUID())) {
							colmdmVO_menupath.add(mdmVO);
						}
					}
				}
			}
			if (mdmVO.getLocaleResourceIdForMenuPath() != null) {
				colmdmVO_menupath.add(mdmVO);
			} else {
				// add entites without an menupath too if not a system entity
				if (!E.isNuclosEntity(mdmVO.getUID()) && !MasterDataDelegate.getInstance().isSubformEntity(mdmVO.getUID())) {
					colmdmVO_menupath.add(mdmVO);
				}
			}
		}

		colmdmVO_menupath.add(E.USER);
		colmdmVO_menupath.add(E.ROLE);
		colmdmVO_menupath.add(E.SEARCHFILTER);
		colmdmVO_menupath.add(E.JOBCONTROLLER);
		colmdmVO_menupath.add(E.PARAMETER);
		colmdmVO_menupath.add(E.LDAPSERVER);
		colmdmVO_menupath.add(E.WEBSERVICE);
		colmdmVO_menupath.add(E.LAYOUT);
		colmdmVO_menupath.add(E.STATEMODEL);
		colmdmVO_menupath.add(E.GENERATION);
		colmdmVO_menupath.add(E.GENERATIONSUBENTITY);
		colmdmVO_menupath.add(E.REPORT);
		colmdmVO_menupath.add(E.NUCLET);
		
		final List<CollectableField> result = CollectionUtils.transform(colmdmVO_menupath, new Transformer<EntityMeta<?>, CollectableField>() {
			@Override
			public CollectableField transform(EntityMeta<?> mdmVO) {
				String label = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(mdmVO);
				return new LocalizedCollectableValueIdField(mdmVO.getUID(), mdmVO.getEntityName(), label);
			}
		});
		Collections.sort(result);
		return result;
	}
}
