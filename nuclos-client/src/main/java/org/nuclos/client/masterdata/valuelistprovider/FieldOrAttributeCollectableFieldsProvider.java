//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * Value list provider to get all fields or attributes for an entity.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:martin.weber@novabit.de">Martin Weber</a>
 * @version 00.01.000
 */
public class FieldOrAttributeCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(FieldOrAttributeCollectableFieldsProvider.class);
	
	//
	
	private UID entityUid = null;
	
	/**
	 * @deprecated
	 */
	FieldOrAttributeCollectableFieldsProvider() {
	}
	
	public FieldOrAttributeCollectableFieldsProvider(UID entityUid) {
		setParameter(NuclosConstants.VLP_ENTITY_UID_PARAMETER, entityUid);
	}
	
	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		if (NuclosConstants.VLP_ENTITY_UID_PARAMETER.equals(sName) && oValue != null) {
			this.entityUid = (UID) oValue;
		} else {
			LOG.info("Unknown parameter " + sName + " with value " + oValue);
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		// LOG.debug("getCollectableFields");
		final List<CollectableField> result = new ArrayList<CollectableField>();
		if (entityUid == null) {
			return result;
		}
		final MetaProvider mProv = MetaProvider.getInstance();
		final EntityMeta<?> entityMeta = mProv.getEntity(entityUid);
		if (entityMeta.isStateModel()) {
			final Collection<AttributeCVO> collattrcvo = GenericObjectMetaDataCache.getInstance().getAttributeCVOsByModule(entityUid, false);
			for (AttributeCVO attributeCVO : collattrcvo) {
				result.add(new CollectableValueField(attributeCVO.getPrimaryKey()));
			}
		}
		else {
			for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
				result.add(new CollectableValueField(fieldMeta.getUID()));
			}
		}

		Collections.sort(result);

		return result;
	}
}
