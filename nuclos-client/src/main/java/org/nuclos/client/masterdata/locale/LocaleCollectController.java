//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.locale;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataSubFormController;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.model.CollectableComponentModel;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.DependentDataMap;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class LocaleCollectController extends MasterDataCollectController<UID> {

	private static final Logger LOG = Logger.getLogger(LocaleCollectController.class);
	
	private Collection<String> collresid;

	private LocaleDelegate delegate = LocaleDelegate.getInstance();


	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public LocaleCollectController(MainFrameTab tabIfAny) {
		super(E.LOCALE, tabIfAny, null);
	}

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public LocaleCollectController(Collection<String> collresids, MainFrameTab tabIfAny) {
		this(tabIfAny);
		this.collresid = collresids;
	}

	
	public void runViewSingleCollectableWithLocale(LocaleInfo localeInfo) throws CommonBusinessException{
		this.runViewSingleCollectableWithId(localeInfo.getLocale());
	}

	
	public UID getCurrentCollectableId() {
		if (this.getCollectState().isDetailsModeNew()) {
			return null;
		}
		else {
			return getSelectedCollectableId();
		}
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew) throws CommonBusinessException {
		if (clctNew.getId() != null) {
			throw new IllegalArgumentException("clctNew");
		}
		final MasterDataVO<UID> mdvoInserted = mddelegate.create(
				getEntityUid(), clctNew.getMasterDataCVO(), new DependentDataMap(), 
				ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY));

		return new CollectableMasterDataWithDependants<UID>(
				clctNew.getCollectableEntity(), new MasterDataVO<UID>(mdvoInserted, readDependants(mdvoInserted.getId())));
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCurrentCollectable(CollectableMasterDataWithDependants<UID> clctCurrent) throws CommonBusinessException {
		return updateCollectable(clctCurrent, null, null);
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct, Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
	   final UID oId = (UID) delegate.update(clct.getMasterDataCVO(), getAllSubFormData(clct.getId()).toDependentDataMap());
	   final MasterDataVO<UID> mdvoUpdated = mddelegate.get(getEntityUid(), oId);
	   return new CollectableMasterDataWithDependants<UID>(
			   clct.getCollectableEntity(), new MasterDataVO<UID>(mdvoUpdated, readDependants(mdvoUpdated.getId())));
	}


	@Override
	protected void unsafeFillDetailsPanel(CollectableMasterDataWithDependants<UID> clct) throws NuclosBusinessException {
		for (UID fieldUid : getDetailsPanel().getLayoutRoot().getOrderedFieldUIDs()) {
			LOG.debug("sFieldName = " + fieldUid);

			// iterate over the models rather than over the components:
			final CollectableComponentModel clctcompmodel = getDetailsPanel().getLayoutRoot().getCollectableComponentModelFor(fieldUid);
			clctcompmodel.setField(clct.getField(fieldUid));
		}

		CollectableComponent parentComp = CollectionUtils.getFirst(getDetailCollectableComponentsFor(E.LOCALE.parent.getUID()));
		try {
			String parent = (String) parentComp.getField().getValue();
			loadLocaleResources(parent, clct.getPrimaryKey());
		}
		catch (CommonBusinessException ex) {
			throw new NuclosBusinessException(ex.getMessage(), ex);
		}
	}


	private void loadLocaleResources(String sParentLocale, UID localeUid) throws CommonFinderException, CommonPermissionException, NuclosBusinessException {
		if (!(localeUid == null && sParentLocale == null)) {
			if (this.collresid != null && !this.collresid.isEmpty()) {
				this.fillLocaleResourceSubForm(localeUid, this.getResourcesForDefaultLocaleByIntId(collresid));
			}
		}
	}

	public void fillLocaleResourceSubForm(UID localeUid, Collection<LocaleResource> collres)  throws NuclosBusinessException {
		((MasterDataSubFormController<UID>) getSubFormController(E.LOCALERESOURCE.getUID())).fillSubForm(localeUid, getLocaleResourceAsVO(collres));
	}

	/**
	 * make <code>LocaleResource</code> as <code>MasterDataVO</code>
	 * @param collres collection of <code>LocaleResource</code> objects
	 * @return collection of <code>MasterDataVO</code>
	 */
	private Collection<EntityObjectVO<UID>> getLocaleResourceAsVO(Collection<LocaleResource> collres){
		List<EntityObjectVO<UID>> result = new ArrayList<EntityObjectVO<UID>>();
		for (LocaleResource lr : collres) {
			EntityObjectVO<UID> eo = lr.getMasterDataVO().getEntityObject();
			eo.setFieldValue(E.LOCALERESOURCE.resourceID.getUID(), lr.getResourceId());
			eo.setFieldValue(E.LOCALERESOURCE.text.getUID(), lr.getResource());
			//@TODO MultiNuclet. mdvo.setFieldValue(F_LOCALETEXT, lr.getTranslatedResource());
			result.add(eo);
		}
		return result;
	}

	/**
	 * this method is used only for default locale,
	 * if locale CollectController has been invoked from another CollectController (e.g. AttributeCollectController)
	 * @param collIds
	 * @return
	 */
	private Collection<LocaleResource> getResourcesForDefaultLocaleByIntId(Collection<String> collIds) {
		Collection<LocaleResource> result = new ArrayList<LocaleResource>();
		for (MasterDataVO<UID> mdvo : delegate.getDefaultResourcesAsVO(collIds)) {
			String parentText = delegate.getDefaultResource((String)mdvo.getFieldValue(E.LOCALERESOURCE.resourceID.getUID()));
			result.add(new LocaleResource(mdvo, (String)mdvo.getFieldValue(E.LOCALERESOURCE.resourceID.getUID()), parentText, (String)mdvo.getFieldValue(E.LOCALERESOURCE.text.getUID())));
		}
		return result;
	}

	public class LocaleResource {
		
		private MasterDataVO<UID> mdvo;
		private String sResourceId;
		private String sResource;
		private String sTranslatedResource;

		LocaleResource(MasterDataVO<UID> mdvo, String sResourceId, String sResource, String sTranslatedResource) {
			this.mdvo = mdvo;
			this.sResourceId = sResourceId;
			this.sResource = sResource;
			this.sTranslatedResource = sTranslatedResource;

		}

		public MasterDataVO<UID> getMasterDataVO() {
			return this.mdvo;
		}

		public void setMasterDataVO(MasterDataVO<UID> mdvo) {
			this.mdvo = mdvo;
		}

		public String getResourceId() {
			return this.sResourceId;
		}

		public String getResource() {
			return this.sResource;
		}

		public String getTranslatedResource() {
			return this.sTranslatedResource;
		}

		public void setResourceId(String sReosurceId) {
			this.sResourceId = sReosurceId;
		}

		public void setResourceText(String sResource) {
			this.sResource = sResource;
		}

		public void setTranslatedResource(String sTranslatedResource) {
			this.sTranslatedResource = sTranslatedResource;
		}
		
	}
	
}
