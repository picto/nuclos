//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.layout.LayoutDelegate;
import org.nuclos.client.valuelistprovider.cache.ManagedCollectableFieldsProvider;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonBusinessException;

/**
 * Collectable fields provider for history view entity-field and system-field filter.
 * Retrieves fields from all sub entities aswell.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:kai.fibr@nuclos.de">Kai Fibr</a>
 * @version 01.00.00
 */
public class HistoryEntityFieldFieldsProvider  extends ManagedCollectableFieldsProvider implements CacheableCollectableFieldsProvider {
	
	private final UID entityUid;
	
	private final boolean systemFields;
	
	/**
	 * Constructs a HistoryEntityFieldFieldsProvider
	 * @param entityUid	UID of historicized entity
	 * @param systemFields if true, find only system fields, else only non-system fields
	 */
	public HistoryEntityFieldFieldsProvider(UID entityUid, boolean systemFields) {
		this.entityUid = entityUid;
		this.systemFields = systemFields;
	}

	@Override
	public void setParameter(String sName, Object oValue) {
	}

	@Override
	public List<CollectableField> getCollectableFields()
			throws CommonBusinessException {
		
		List<CollectableField> fieldList = new ArrayList<>();
		Set<UID> entityUids = new HashSet<UID>();
		entityUids.add(entityUid);
		// find all layouts
		Set<UID> layoutUids = LayoutDelegate.getInstance().getAllLayoutUidsForEntity(entityUid);
		if (layoutUids != null) {
			for (UID layoutUid : layoutUids) {
				// find all subform entities
				Map<EntityAndField, UID> subFormEntities = LayoutDelegate.getInstance().getSubFormEntityAndParentSubFormEntityNamesByLayoutId(layoutUid);
				for (EntityAndField eaf : subFormEntities.keySet()) {
					entityUids.add(eaf.getEntity());
				}
			}
			Set<FieldMeta<?>> fields = new HashSet<>();
			for (UID entityUid : entityUids) {
				// find all fields
				for (FieldMeta<?> fieldMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entityUid).values()) {
					if (systemFields) { // only system fields
						if (SF.isEOField(fieldMeta.getFieldName())) {
							fields.add(fieldMeta);
						}
					}
					else { // only non-system fields
						if (!SF.isEOField(fieldMeta.getFieldName())) {
							fields.add(fieldMeta);
						}
					}
				}
			}
			for (FieldMeta<?> field : fields) {
				fieldList.add(new CollectableValueIdField(field.getUID(), field.getFieldName()));
			}
		}
		return fieldList;
	}

	@Override
	public Object getCacheKey() {
		return entityUid;
	}
}
