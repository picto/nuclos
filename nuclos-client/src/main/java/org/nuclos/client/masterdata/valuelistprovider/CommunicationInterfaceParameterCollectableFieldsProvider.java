//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonBusinessException;

public class CommunicationInterfaceParameterCollectableFieldsProvider implements
		CollectableFieldsProvider {
	
	public static final String COMMUNICATION_INTERFACE = "communicationInterface";
	
	private UID communicationInterfaceId;

	@Override
	public void setParameter(String sName, Object oValue) {
		if (COMMUNICATION_INTERFACE.equals(sName) && oValue instanceof UID) {
			communicationInterfaceId = (UID) oValue;
		}
	}

	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		if (communicationInterfaceId != null) {
			Collection<EntityObjectVO<UID>> eoParameters = MasterDataDelegate.getInstance().getDependentDataCollection(E.COMMUNICATION_INTERFACE_PARAMETER.getUID(), 
					E.COMMUNICATION_INTERFACE_PARAMETER.communicationInterface.getUID(), null, communicationInterfaceId);
			for (EntityObjectVO<UID> eo : eoParameters) {
				result.add(new CollectableValueIdField(eo.getPrimaryKey(), eo.getFieldValue(E.COMMUNICATION_INTERFACE_PARAMETER.name)));				
			}
		}
		
		return result;
	}

}
