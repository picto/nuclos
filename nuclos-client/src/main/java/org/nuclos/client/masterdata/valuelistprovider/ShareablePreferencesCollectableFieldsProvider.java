//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.masterdata.valuelistprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableFieldsProvider;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.searchcondition.CollectableIsNullCondition;
import org.nuclos.common.masterdata.CollectableMasterDataForeignKeyEntityField;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.nuclet.content.NucletContentPreferenceTreeNode;

public class ShareablePreferencesCollectableFieldsProvider implements CollectableFieldsProvider {

	private static final Logger LOG = Logger.getLogger(ShareablePreferencesCollectableFieldsProvider.class);
	
	//
	
	public ShareablePreferencesCollectableFieldsProvider() {
	}
	
	@Override
	public List<CollectableField> getCollectableFields() throws CommonBusinessException {
		return _getCollectableFields();
	}
	
	private <PK> List<CollectableField> _getCollectableFields() throws CommonBusinessException {
		List<CollectableField> result = new ArrayList<CollectableField>();
		
		for (MasterDataVO<?> mdvo : MasterDataDelegate.getInstance().getMasterData(E.PREFERENCE.getUID(), 
				new CollectableIsNullCondition(new CollectableMasterDataForeignKeyEntityField<UID>(E.PREFERENCE.user)))) {
			String name = NucletContentPreferenceTreeNode.getName(mdvo.getEntityObject());
			result.add(new CollectableValueIdField(mdvo.getPrimaryKey(), name));
		}

		Collections.sort(result);
		return result;
	}

	/**
	 * @deprecated Use constructor for parameter setting.
	 */
	@Override
	public void setParameter(String sName, Object oValue) {
		LOG.info("Unknown parameter " + sName + " with value " + oValue);
	}
	
}	// class AssignableWorkspaceCollectableFieldsProvider
