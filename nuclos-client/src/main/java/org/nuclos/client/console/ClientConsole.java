//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.console;

import java.util.Arrays;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.nuclos.client.main.Main;
import org.nuclos.client.main.Main.ExitResult;
import org.nuclos.client.security.NuclosRemoteServerSession;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.CommonConsole;
import org.nuclos.common.ConsoleConstants;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Management console for Nuclos.
 */
@Configurable
public class ClientConsole extends ConsoleConstants {

    /**
	 * this list is used by NuclosConsoleGui to show the available commands
	 */
	public static final List<String> LSTCOMMANDS = Arrays.asList(
			CMD_INVALIDATE_ALL_CACHES,
			CMD_REBUILD_CLASSES,
			CMD_CLEAR_USER_PREFERENCES,
			CMD_COMPILE_DB_OBJECTS,
			CMD_SEND_MESSAGE, CMD_KILL_SESSION,
			CMD_REBUILD_CONSTRAINTS, CMD_REBUILD_INDEXES, CMD_REBUILD_CONSTRAINTS_AND_INDEXES, CMD_SET_MANDATOR_LEVEL,
            CMD_GENERATE_BO_UID_LIST, CMD_CHECK_DOCUMENT_FILES, CMD_CLEANUP_DUPLICATE_DOCUMENTS
	);
	
	@Autowired
	private NuclosRemoteServerSession nuclosRemoteServerSession;

	private static ClientConsole INSTANCE;

	private static String sUserName;
	
	public static ClientConsole getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	protected static ClientConsole newClientConsole() {
		try {
			final String sClassName = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getConsoleClassName(),
					ClientConsole.class.getName());

			INSTANCE = (ClientConsole) LangUtils.getClassLoaderThatWorksForWebStart().loadClass(sClassName).newInstance();
			return INSTANCE;
		}
		catch (Exception ex) {
			throw new CommonFatalException("Console could not be created.", ex);
		}
	}

	protected ClientConsole() {
	}
	
	private String login(String sUser, String sPassword) throws LoginException {
		 sUserName = nuclosRemoteServerSession.login(sUser, sPassword);
		 return sUserName;
	}

	private void logout() throws LoginException {
		nuclosRemoteServerSession.logout();
	}

	/**
	 * called if real console is required (shell)
	 * @param asArgs
	 */
	public static void main(String[] asArgs) {
		final ClientConsole dut = ClientConsole.getInstance();
		try {
			if (asArgs.length < 4) {
				System.out.println("Missing command.\n\n");
				System.out.println("Usage: NuclosConsole <username> <password> <serverconfig> <command>");
				System.out.println("The configuration for the application server must be one contained in the file nuclos-client.properties, which must be in the CLASSPATH.");
				System.out.println("<command> is one of the following:");
				System.out.println("==========================================================================");
				System.out.println(dut.getUsage());
				Main.getInstance().exit(ExitResult.ABNORMAL);
			}

			dut.login(asArgs[0], asArgs[1]);
			try {
				final String[] asParamsWithoutLoginInfo = new String[asArgs.length - 3];
				for (int i = 3; i < asArgs.length; i++) {
					asParamsWithoutLoginInfo[i - 3] = asArgs[i];
				}
				ClientConsole.getInstance().parseAndInvoke(asParamsWithoutLoginInfo);
			}
			finally {
				dut.logout();
			}
			Main.getInstance().exit(ExitResult.NORMAL);
		}
		catch (CommonBusinessException ex) {
			// Ok! (tp)
			System.err.println(ex.getMessage());
			ex.printStackTrace();
			Main.getInstance().exit(ExitResult.ABNORMAL);
		}
		catch (Exception ex) {
			// Ok! (tp)
			ex.printStackTrace(System.err);
			Main.getInstance().exit(ExitResult.ABNORMAL);
		}
	}

	public void parseAndInvoke(String[] asArgs) throws Exception {
		new CommonConsole().parseAndInvoke(asArgs);
	}
	
	public void parseAndInvoke(String sCommand, String sArguments) throws Exception {
		new CommonConsole().parseAndInvoke(sCommand, sArguments);
	}

	private String getUsage() {
		return new CommonConsole().getUsage();
	}

}	// class NuclosConsole
