//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.statemodel.admin;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectable;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonFatalException;
import org.nuclos.server.statemodel.valueobject.AttributegroupPermissionVO;

/**
 * User role for a state (for state dependent user rights).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */

public class CollectableStateRoleAttributeGroup extends AbstractCollectable<UID> {
	public static final UID FIELDNAME_ROLE = E.ROLEATTRIBUTEGROUP.role.getUID();
	public static final UID FIELDNAME_ATTRIBUTEGROUP = E.ROLEATTRIBUTEGROUP.attributegroup.getUID();
	public static final UID FIELDNAME_WRITEABLE = E.ROLEATTRIBUTEGROUP.readwrite.getUID();
	
	public static final UID entity = E.ROLEATTRIBUTEGROUP.getUID();

	public static class Entity extends AbstractCollectableEntity {
		public Entity() {
			super(entity, SpringLocaleDelegate.getInstance().getMessage("CollectableStateRoleAttributeGroup.3","Attributgruppe f\u00fcr statusabh\u00e4ngige Rechte"));
						
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_ROLE, String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.4","Benutzergruppe"),
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.9","\u00dcbergeordnete Benutzergruppe (Rolle)"), 
					255, null, false, CollectableField.TYPE_VALUEIDFIELD, null, null, entity, null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_ATTRIBUTEGROUP, String.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.1","Attributgruppe"), 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.2","Attributgruppe"), 
					255, null, false, CollectableField.TYPE_VALUEIDFIELD, null, null, entity, null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_WRITEABLE, Boolean.class, 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.8","Schreibrecht?"), 
					getSpringLocaleDelegate().getMessage("CollectableStateRoleAttributeGroup.7","Schreiben erlaubt?"), 
					null, null, false, CollectableField.TYPE_VALUEFIELD, null, null, entity, null));
		}
	}

	public static final CollectableEntity clcte = new Entity();

	private CollectableField clctfRole;
	private final AttributegroupPermissionVO agpvo;

	public CollectableStateRoleAttributeGroup(CollectableField clctfRole, AttributegroupPermissionVO agpvo) {
		this.clctfRole = clctfRole;
		this.agpvo = agpvo;
	}

	protected CollectableEntity getCollectableEntity() {
		return clcte;
	}

	public AttributegroupPermissionVO getAttributegroupPermissionVO() {
		return this.agpvo;
	}

	@Override
	public UID getId() {
		return agpvo.getId();
	}

	@Override
	public int getVersion() {
		return agpvo.getVersion();
	}

	@Override
	public CollectableField getField(UID fieldUid) throws CommonFatalException {
		final CollectableField result;

		if (fieldUid.equals(FIELDNAME_ROLE)) {
			result = this.clctfRole;
		}
		else if (fieldUid.equals(FIELDNAME_ATTRIBUTEGROUP)) {
			result = new CollectableValueIdField(agpvo.getAttributegroupUID(), agpvo.getAttributegroupUID());
		}
		else if (fieldUid.equals(FIELDNAME_WRITEABLE)) {
			result = new CollectableValueField(Boolean.valueOf(agpvo.isWritable()));
		}
		else {
			throw new IllegalArgumentException(SpringLocaleDelegate.getInstance().getMessage(
					"CollectableStateRoleAttributeGroup.5","Feld nicht vorhanden: ") + fieldUid);
		}
		return result;
	}

	@Override
	public void setField(UID fieldUid, CollectableField clctfValue) {
		if (fieldUid.equals(FIELDNAME_ROLE)) {
			clctfRole = clctfValue;
		}
		else if (fieldUid.equals(FIELDNAME_ATTRIBUTEGROUP)) {
			agpvo.setAttributegroupUID((UID) clctfValue.getValueId());
			//agpvo.setAttributegroup((String) clctfValue.getValue());
		}
		else if (fieldUid.equals(FIELDNAME_WRITEABLE)) {
			agpvo.setWritable(((Boolean) clctfValue.getValue()).booleanValue());
		}
		else {
			throw new IllegalArgumentException(SpringLocaleDelegate.getInstance().getMessage(
					"CollectableStateRoleAttributeGroup.6","Feld nicht vorhanden: ") + fieldUid);
		}
		
		assert this.getField(fieldUid).equals(clctfValue);
		this.bDirty = true;
	}

	@Override
	public UID getEntityUID() {
		return clcte.getUID();
	}

	@Override
	public UID getPrimaryKey() {
		return agpvo.getPrimaryKey();
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("entity=").append(getCollectableEntity());
		result.append(",aGroupPermVo=").append(getAttributegroupPermissionVO());
		result.append(",id=").append(getId());
		//Note: Swing (since J1.7 more than before) calls "toString()" often for whatever reason. getIdentifierLabel() shouldn't
		//be called therefore as it calls LocaleDelegate and thus uses lot of CPU Power.
//				result.append(",label=").append(getIdentifierLabel());
		result.append(",complete=").append(isComplete());
		result.append("]");
		return result.toString();
	}
	
	public static class MakeCollectable implements Transformer<AttributegroupPermissionVO, CollectableStateRoleAttributeGroup> {
		private final CollectableField clctfRole;

		public MakeCollectable(CollectableField clctfRole) {
			this.clctfRole = clctfRole;
		}

		@Override
		public CollectableStateRoleAttributeGroup transform(AttributegroupPermissionVO agpvo) {
			return new CollectableStateRoleAttributeGroup(clctfRole, agpvo);
		}
	}	// inner class MakeCollectable

}	// class CollectableStateRoleAttributeGroup
