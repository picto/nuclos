package org.nuclos.client.customcomp.resplan;

import org.nuclos.common.collect.collectable.Collectable;

public class EntryWPElem<C> {

	 final private ClientPlanElement planElement;
	 private Collectable collectable;
	
	public EntryWPElem(Collectable collectable, ClientPlanElement planElement) {
		super();
		this.collectable = collectable;
		this.planElement = planElement;
	}

	public Collectable getCollectable() {
		return collectable;
	}

	public ClientPlanElement getPlanElement() {
		return planElement;
	}
	
	public void setCollectable(Collectable coll) {
		this.collectable = coll;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof EntryWPElem) {
			return hashCode() == o.hashCode();
 		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return collectable.hashCode() ^ planElement.hashCode();
	}
}
