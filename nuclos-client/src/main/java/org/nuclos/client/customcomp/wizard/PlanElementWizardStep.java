package org.nuclos.client.customcomp.wizard;

import static info.clearthought.layout.TableLayoutConstants.FILL;
import static info.clearthought.layout.TableLayoutConstants.PREFERRED;
import static info.clearthought.layout.TableLayoutConstants.TOP;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.combobox.ListComboBoxModel;
import org.nuclos.client.common.EntityUtils;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.customcomp.resplan.ClientPlanElement;
import org.nuclos.client.customcomp.resplan.ClientResPlanConfigVO;
import org.nuclos.client.customcomp.resplan.EntryTranslationTableModel;
import org.nuclos.client.customcomp.resplan.PlanElementsTableModel;
import org.nuclos.client.customcomp.wizard.CustomComponentWizardModel.LocalTimeSpanPane;
import org.nuclos.client.customcomp.wizard.CustomComponentWizardModel.ResourceCellEditor;
import org.nuclos.client.ui.Bubble;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Pair;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common.time.LocalTime;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.LocaleInfo;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.pietschy.wizard.InvalidStateException;
import org.pietschy.wizard.PanelWizardStep;

//Version
public class PlanElementWizardStep<PK,R,C extends Collectable<PK>> extends PanelWizardStep implements ItemListener, ChangeListener {
	
	private static final Logger LOG = Logger.getLogger(PlanElementWizardStep.class);
	
	private PlanElement<R> planElement;
	private ClientResPlanConfigVO configVO;
	private SaveCancelListener saveCancelListener;
	
	private JPanel typePanel;
	private JPanel entryPanel;
	private JPanel relationPanel;
	private JPanel buttonPanel;
	private JPanel timePanel;
	
	private SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
	
	JComboBox typeComboBox;
	JComboBox entryEntityComboBox;
	JComboBox refFieldComboBox;
	JComboBox bookerFieldComboBox;

	JComboBox dateFromFieldComboBox;
	JComboBox dateUntilFieldComboBox;

	JCheckBox withTimeCheckBox;
	JComboBox timeFromFieldComboBox;
	JComboBox timeUntilFieldComboBox;
	LocalTimeSpanPane timeSpanPane;
	
	JTextField colorEntry;
	JTextField sIcon;
	JTextField fixedExtend;
	
	JScrollPane scrollPane2;
	JTable translationTable;
	
	Collection<LocaleInfo> locales;
	EntryTranslationTableModel tablemodel2;
	
	JComboBox relationEntityComboBox;
	JComboBox relationFromFieldComboBox;
	JComboBox relationToFieldComboBox;
	JCheckBox newRelationFromControllerCheckBox;
	
	JRadioButton relationPresentationOrthogonal;
	JRadioButton relationPresentationStraight;
	
	JRadioButton relationFromPlain;
	JRadioButton relationFromDot;
	JRadioButton relationFromArrow;
	
	JRadioButton relationToPlain;
	JRadioButton relationToDot;
	JRadioButton relationToArrow;
	
	JTextField colorRelation;
	JTextField sIconFrom;
	JTextField sIconTo;
	
	JButton btnSave;
	JButton btnCancel;
	JTextField order;
	JCheckBox bCascade;

	JLabel lblDateUntil;
	JLabel lblBookerField;
	
	public PlanElementWizardStep(PlanElement<R> planElement, ClientResPlanConfigVO configVO, SaveCancelListener scListener) {
		super("nuclos.resplan.wizard.step3c.title", "nuclos.resplan.wizard.step3c.summary");
		
		this.planElement = planElement;
		this.configVO = configVO;
		this.saveCancelListener = scListener;
		
		typeComboBox = createJComboBox(30);
		entryEntityComboBox = createJComboBox(30);
		refFieldComboBox = createJComboBox(30);
		bookerFieldComboBox = createJComboBox(30);

		dateFromFieldComboBox = createJComboBox(20);
		dateUntilFieldComboBox = createJComboBox(20);
		timeFromFieldComboBox = createJComboBox(20);
		timeUntilFieldComboBox = createJComboBox(20);

		withTimeCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.wizard.step3.withTimeSpans", null));
		withTimeCheckBox.addItemListener(this);
		timeSpanPane = new LocalTimeSpanPane();
		timeSpanPane.addChangeListener(this);
		
		colorEntry = new JTextField(14);
		sIcon = new JTextField(20);
		fixedExtend = new JTextField(10);
		order = new JTextField(10);
		bCascade = new JCheckBox();
		
		locales = LocaleDelegate.getInstance().getAllLocales(false);
		tablemodel2 = new EntryTranslationTableModel(locales);

		translationTable = new JTable(tablemodel2);
		TableCellEditor editor = new ResourceCellEditor();

		for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
			col.setCellEditor(editor);
		}

		translationTable.getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				stopCellEditing();
			}
		});

		translationTable.setRowHeight(50);
		scrollPane2 = new JScrollPane(translationTable);
		scrollPane2.setSize(new Dimension(540, 180));
		scrollPane2.setPreferredSize(new Dimension(540, 180));

		typePanel = new JPanel();
		entryPanel = new JPanel();
		relationPanel = new JPanel();
		buttonPanel = new JPanel();
		setLayout(new BorderLayout());
		
		ITableLayoutBuilder tlb1 = new TableLayoutBuilder(typePanel).columns(100, PREFERRED, 20, PREFERRED, PREFERRED, PREFERRED, FILL).gaps(5, 5);
		tlb1.newRow().addLocalizedLabel("wizard.step.planelemtablecol.2").add(typeComboBox)
		.newRow(6);
		
		timePanel = new JPanel();
		ITableLayoutBuilder tlb3 = new TableLayoutBuilder(timePanel).columns(PREFERRED, FILL).gaps(5, 5);
		tlb3
		.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.timeFromField").add(timeFromFieldComboBox)
		.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.timeUntilField").add(timeUntilFieldComboBox)
		.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.timeSpans", "nuclos.resplan.wizard.step3.description", TOP)
		.newRow().add(timeSpanPane, 2);
		
		String stextcascade = localeDelegate.getMessage("wizard.step3.resplancascading.1", "Cascading");
		bCascade.setText(stextcascade);

		lblDateUntil = new JLabel(SpringLocaleDelegate.getInstance().getMessage("nuclos.resplan.wizard.step3.dateUntilField", null));
		lblBookerField = new JLabel(SpringLocaleDelegate.getInstance().getMessage("nuclos.resplan.wizard.step3.bookerField", null));

		ITableLayoutBuilder tlb2 = new TableLayoutBuilder(entryPanel).columns(150, PREFERRED, 20, 100,
				PREFERRED, 16, FILL).gaps(5, 5);
		tlb2
			.newRow(6).add(new JSeparator(), 7)
			.newRow().addLocalizedLabel("wizard.step3.resplanrefentity.1").add(entryEntityComboBox)
			.skip().addLocalizedLabel("nuclos.resplan.wizard.step3.dateFromField").add(dateFromFieldComboBox)
			.skip().add(bCascade)
			.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.referenceField").add(refFieldComboBox)
			.skip().add(lblDateUntil, 1, TableLayoutBuilder.FULL, TableLayoutBuilder.CENTER).add(dateUntilFieldComboBox)
			.skip().add(withTimeCheckBox)
			.newRow().add(lblBookerField, 1, TableLayoutBuilder.FULL, TableLayoutBuilder.CENTER).add(bookerFieldComboBox)
			.newRow()
			.newRow(6).add(new JSeparator(), 5)
			.newRow().add(scrollPane2, 5).skip().add(timePanel)
			.newRow()
			.newRow(6).add(new JSeparator(), 7)			
			.newRow().addLocalizedLabel("wizard.step3.resplancolor.1").add(colorEntry).skip()
					.addLocalizedLabel("wizard.step3.resplanicon.1").add(sIcon)
			.newRow().addLocalizedLabel("wizard.step3.resplanorder.1").add(order).skip()
					.addLocalizedLabel("wizard.step3.resplanfixext.1").add(fixedExtend)
			.newRow(10)
		;
		
		relationEntityComboBox = createJComboBox(30);
		relationFromFieldComboBox = createJComboBox(30);
		relationToFieldComboBox = createJComboBox(30);
		newRelationFromControllerCheckBox = new JCheckBox(localeDelegate.getText("nuclos.resplan.wizard.step3.newRelationFromController", null));			

		relationPresentationOrthogonal = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationPresentationOrthogonal"));
		relationPresentationStraight = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationPresentationStraight"));
		ButtonGroup bgRelationPresentation = new ButtonGroup();
		bgRelationPresentation.add(relationPresentationOrthogonal);
		bgRelationPresentation.add(relationPresentationStraight);
		
		relationFromPlain = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointPlain"));
		relationFromDot = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointDot"));
		relationFromArrow = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointArrow"));
		ButtonGroup bgRelationFrom = new ButtonGroup();
		bgRelationFrom.add(relationFromPlain);
		bgRelationFrom.add(relationFromDot);
		bgRelationFrom.add(relationFromArrow);
		
		relationToPlain = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointPlain"));
		relationToDot = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointDot"));
		relationToArrow = new JRadioButton(localeDelegate.getText("nuclos.resplan.wizard.step4.relationEndpointArrow"));
		ButtonGroup bgRelationTo = new ButtonGroup();
		bgRelationTo.add(relationToPlain);
		bgRelationTo.add(relationToDot);
		bgRelationTo.add(relationToArrow);

		colorRelation = new JTextField(14);
		sIconFrom = new JTextField(20);
		sIconTo = new JTextField(20);
		
		ITableLayoutBuilder tlb = new TableLayoutBuilder(relationPanel).columns(PREFERRED, PREFERRED, PREFERRED, FILL).gaps(5, 5);
		//tlb.newRow().add(withRelationCheckBox)
		tlb.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.relationEntity").add(relationEntityComboBox, 2)
		.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.relationFromField").add(relationFromFieldComboBox, 2)
		.newRow().addLocalizedLabel("nuclos.resplan.wizard.step3.relationToField").add(relationToFieldComboBox, 2)
		.newRow().add(newRelationFromControllerCheckBox)
;

		JPanel jpnRelation = new JPanel();
		ITableLayoutBuilder tbllayRelation = new TableLayoutBuilder(jpnRelation).columns(PREFERRED, PREFERRED, PREFERRED, PREFERRED).gaps(5, 5);
		tbllayRelation.newRow();
		
		JPanel jpnRelationFrom = new JPanel();
		jpnRelationFrom.setBorder(BorderFactory.createTitledBorder(localeDelegate.getText("nuclos.resplan.wizard.step4.relationFrom")));
		ITableLayoutBuilder tbllayRelationFrom = new TableLayoutBuilder(jpnRelationFrom).columns(PREFERRED, PREFERRED);
		tbllayRelationFrom.newRow().add(relationFromPlain);
		final ClassLoader cl = LangUtils.getClassLoaderThatWorksForWebStart();
		tbllayRelationFrom.newRow().add(relationFromDot).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/oval_start.gif"))));
		tbllayRelationFrom.newRow().add(relationFromArrow).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/classic_start.gif"))));
		tbllayRelation.add(jpnRelationFrom);
		
		JPanel jpnRelationPresentation = new JPanel();
		jpnRelationPresentation.setBorder(BorderFactory.createTitledBorder(localeDelegate.getText("nuclos.resplan.wizard.step4.relationPresentation")));
		ITableLayoutBuilder tbllayRelationPresentation = new TableLayoutBuilder(jpnRelationPresentation).columns(PREFERRED, PREFERRED);
		tbllayRelationPresentation.newRow().add(relationPresentationStraight).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/straight.png"))));
		tbllayRelationPresentation.newRow().add(relationPresentationOrthogonal).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/vertical.png"))));
		tbllayRelation.add(jpnRelationPresentation);
		
		JPanel jpnRelationTo = new JPanel();
		jpnRelationTo.setBorder(BorderFactory.createTitledBorder(localeDelegate.getText("nuclos.resplan.wizard.step4.relationTo")));
		ITableLayoutBuilder tbllayRelationTo = new TableLayoutBuilder(jpnRelationTo).columns(PREFERRED, PREFERRED);
		tbllayRelationTo.newRow().add(relationToPlain);
		tbllayRelationTo.newRow().add(relationToDot).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/oval_end.gif"))));
		tbllayRelationTo.newRow().add(relationToArrow).add(new JLabel(new ImageIcon(cl.getResource("org/nuclos/client/relation/images/classic_end.gif"))));
		tbllayRelation.add(jpnRelationTo);
		tbllayRelation.newRow().addLabel("Color").add(colorRelation).addLabel(" e.g. 0xBC2234");
		tbllayRelation.newRow().addLabel("FromIcon").add(sIconFrom).addLabel("ToIcon").add(sIconTo);

		tlb.newRow().addLocalizedLabel("nuclos.resplan.wizard.step4.relation", TOP).addFullSpan(jpnRelation);
		
		btnCancel = new JButton(localeDelegate.getText("nuclos.resplan.wizard.step4.cancel"));
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveCancelListener.cancel();
			}
		});
		btnSave = new JButton(localeDelegate.getText("nuclos.resplan.wizard.step4.save"));
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					applyState();
				} catch (InvalidStateException e) {
					LOG.warn("applyState failed: " + e, e);
				}
				saveCancelListener.save();
			}
		});
		buttonPanel.add(btnCancel);
		buttonPanel.add(btnSave);
		this.add(typePanel, BorderLayout.NORTH);
		this.add(entryPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
		timePanel.setVisible(withTimeCheckBox.isSelected());
	}

	private void stopCellEditing() {
        for(TableColumn col : CollectionUtils.iterableEnum(translationTable.getColumnModel().getColumns())) {
        	TableCellEditor cellEditor = col.getCellEditor();
			if(cellEditor != null) {
        		cellEditor.stopCellEditing();
			}
        }
	}
	
	private JComboBox createJComboBox(int width) {
		JComboBox comboBox = new JComboBox();
		if (width > 0) {
			char ch [] = new char[width]; Arrays.fill(ch, 'x');
			comboBox.setPrototypeDisplayValue(new String(ch));
		}
		comboBox.addItemListener(this);
		return comboBox;
	}
	
	private static String getBackFromStringifiedUid(String s, Collection<FieldMeta<?>> fields) {
		if (StringUtils.isNullOrEmpty(s)) {
			return s;
		}
		for (FieldMeta<?> fm : fields) {
			s = s.replace(fm.getUID().getStringifiedDefinition(), "${" + fm.getFieldName() + "}");
		}
		return s;
	}

	@Override
	public void prepare() {
		typeComboBox.setModel(new ListComboBoxModel<String>(PlanElementsTableModel.getTypes()));
		typeComboBox.setSelectedIndex(planElement.getType() - 1);
		Set<UID> resEntities = new HashSet<UID>();
		resEntities.add(configVO.getResourceEntity());
		entryEntityComboBox.setModel(new ListComboBoxModel<EntityMeta<?>>(getSlaveEntities(resEntities, 1)));
		entryEntityComboBox.setSelectedItem(planElement.getEntity() == null
				? EntityMeta.NULL : MetaProvider.getInstance().getEntity(planElement.getEntity()));

		configureReferenceFieldComboBox();
		configureBookerFieldComboBox();
		refFieldComboBox.setSelectedItem(planElement.getPrimaryField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getPrimaryField()));
		bookerFieldComboBox.setSelectedItem(planElement.getBookerField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getBookerField()));

		withTimeCheckBox.setSelected(!StringUtils.looksEmpty(planElement.getTimePeriodsString()));
		configureDateTimeComboBoxes(false);
		dateFromFieldComboBox.setSelectedItem(planElement.getDateFromField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getDateFromField()));
		dateUntilFieldComboBox.setSelectedItem(planElement.getDateUntilField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getDateUntilField()));
		timeFromFieldComboBox.setSelectedItem(planElement.getTimeFromField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getTimeFromField()));
		timeUntilFieldComboBox.setSelectedItem(planElement.getTimeUntilField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getTimeUntilField()));
		timeSpanPane.setText(planElement.getTimePeriodsString());
		
		colorEntry.setText(planElement.getColor());
		sIcon.setText(planElement.getFromIcon());
		fixedExtend.setText(String.valueOf(planElement.getFixedExtend()));
		order.setText(String.valueOf(planElement.getOrder()));
		bCascade.setSelected(planElement.isCascade());
		
		Collection<FieldMeta<?>> fields = MetaProvider.getInstance().getAllEntityFieldsByEntity(planElement.getEntity()).values();
		
		List<PlanElementLocaleVO> resources = new ArrayList<PlanElementLocaleVO>();
		for (LocaleInfo locale : locales) {
			boolean found = false;
			if (planElement.getPlanElementLocaleVO() != null) {
				for (PlanElementLocaleVO vo : (List<PlanElementLocaleVO>) planElement.getPlanElementLocaleVO()) {
					if (locale.getLocale().equals(vo.getLocaleId())) {
						found = true;
						resources.add(vo);
						
						vo.setBookingLabel(getBackFromStringifiedUid(vo.getBookingLabel(), fields));
						vo.setBookingTooltip(getBackFromStringifiedUid(vo.getBookingTooltip(), fields));
						
					}
				}
			}
			if (!found) {
				PlanElementLocaleVO vo = new PlanElementLocaleVO();
				vo.setLocaleId(locale.getLocale());
				resources.add(vo);
			}
		}
		tablemodel2.setRows(resources);
		
		configureRelationEntityComboBox();
		configureRelationFieldComboBoxes();
		relationEntityComboBox.setSelectedItem(planElement.getEntity() == null
				? EntityMeta.NULL : MetaProvider.getInstance().getEntity(planElement.getEntity()));
		relationFromFieldComboBox.setSelectedItem(planElement.getPrimaryField() == null
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getPrimaryField()));
		relationToFieldComboBox.setSelectedItem(planElement.getSecondaryField() == null 
				? FieldMeta.NULL : MetaProvider.getInstance().getEntityField(planElement.getSecondaryField()));
		newRelationFromControllerCheckBox.setSelected(planElement.isNewRelationFromController());
		
		switch (planElement.getPresentation()) {
			case ClientResPlanConfigVO.RELATION_PRESENTATION_STRAIGHT: relationPresentationStraight.setSelected(true); break;
			default: relationPresentationOrthogonal.setSelected(true);
		}
		
		switch (planElement.getFromPresentation()) {
			case ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_PLAIN: relationFromPlain.setSelected(true); break;
			case ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_ARROW: relationFromArrow.setSelected(true); break;
			default: relationFromDot.setSelected(true);
		}
		
		switch (planElement.getToPresentation()) {
			case ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_PLAIN: relationToPlain.setSelected(true); break;
			case ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_DOT: relationToDot.setSelected(true); break;
			default: relationToArrow.setSelected(true);
		}
		
		colorRelation.setText(planElement.getColor());
		sIconFrom.setText(planElement.getFromIcon());
		sIconTo.setText(planElement.getToIcon());
		bCascade.setSelected(planElement.isCascade());
		updateState();
	}
	
	private boolean isRelation() {
		int itype = typeComboBox.getSelectedIndex() + 1;
		return itype == PlanElement.RELATION;
	}
	
	private boolean isEntry() {
		int itype = typeComboBox.getSelectedIndex() + 1;
		return itype == PlanElement.ENTRY;		
	}
	
	private void configureTypeComboBox() {
		this.removeAll();
		this.add(typePanel, BorderLayout.NORTH);
		if (isRelation()) {
			this.add(relationPanel, BorderLayout.CENTER);			
		} else {
			this.add(entryPanel, BorderLayout.CENTER);
			withTimeCheckBox.setVisible(isEntry());
			withTimeCheckBox.setEnabled(isEntry());
			scrollPane2.setVisible(isEntry());
			timePanel.setVisible(isEntry());
			timeSpanPane.setVisible(isEntry());
			lblDateUntil.setVisible(isEntry());
			dateUntilFieldComboBox.setVisible(isEntry());
			timeFromFieldComboBox.setVisible(isEntry());
			timeUntilFieldComboBox.setVisible(isEntry());
			lblBookerField.setVisible(isEntry());
			bookerFieldComboBox.setVisible(isEntry());
		}
		this.add(buttonPanel, BorderLayout.SOUTH);
		this.revalidate();
		this.repaint();
	}

	private void configureReferenceFieldComboBox() {
		final UID entryEntity = entryEntityComboBox.getSelectedItem() == null
				? null : ((EntityMeta<?>) entryEntityComboBox.getSelectedItem()).getUID();
		final UID resEntity = configVO.getResourceEntity();
		
		final List<FieldMeta<?>> refFieldNames = new ArrayList<FieldMeta<?>>();
		if (resEntity != null && entryEntity != null) {
			for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entryEntity).values()) {
				if (resEntity.equals(field.getForeignEntity()))
					refFieldNames.add(EntityUtils.wrapMetaData(field));
			}
		}
		
		Collections.sort(refFieldNames, EntityUtils.getMetaComparator(FieldMeta.class));
		
		refFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(refFieldNames));
	}
	
	private void configureBookerFieldComboBox() {
		final UID entryEntity = entryEntityComboBox.getSelectedItem() == null
				? null : ((EntityMeta<?>) entryEntityComboBox.getSelectedItem()).getUID();
		final UID resEntity = configVO.getResourceEntity();
		
		final List<FieldMeta<?>> bookerFieldNames = new ArrayList<FieldMeta<?>>();
		bookerFieldNames.add(FieldMeta.NULL);
		if (resEntity != null && entryEntity != null) {
			for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entryEntity).values()) {
				if (E._User.UID.equals(field.getForeignEntity())) {
					bookerFieldNames.add(EntityUtils.wrapMetaData(field));
				}
			}
		}
		
		Collections.sort(bookerFieldNames, EntityUtils.getMetaComparator(FieldMeta.class));
		
		bookerFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(bookerFieldNames));
	}

	private void configureDateTimeComboBoxes(boolean timeOnly) {
		boolean bWithTime = withTimeCheckBox.isSelected();
		
		timePanel.setVisible(bWithTime);
		
		final UID entryEntity = entryEntityComboBox.getSelectedItem() == null
				? null : ((EntityMeta<?>) entryEntityComboBox.getSelectedItem()).getUID();
		
		List<FieldMeta<?>> dateFields = new ArrayList<FieldMeta<?>>();
		List<FieldMeta<?>> timeFields = new ArrayList<FieldMeta<?>>();
		if (entryEntity != null) {
			for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(entryEntity).values()) {
				if (field.getForeignEntity() != null)
					continue;
				if (java.util.Date.class.getName().equals(field.getDataType())
						|| org.nuclos.common2.InternalTimestamp.class.getName().equals(field.getDataType())) {
					dateFields.add(EntityUtils.wrapMetaData(field));
				}
				if (bWithTime && java.lang.String.class.getName().equals(field.getDataType())) {
					timeFields.add(EntityUtils.wrapMetaData(field));
				}
			}

			Collections.sort(dateFields, EntityUtils.getMetaComparator(FieldMeta.class));
			Collections.sort(timeFields, EntityUtils.getMetaComparator(FieldMeta.class));
		}
		dateFields.add(0, FieldMeta.NULL);
		timeFields.add(0, FieldMeta.NULL);
		
		if (!timeOnly) {
			dateFromFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(dateFields));
			dateUntilFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(dateFields));
		}
		timeFromFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(timeFields));
		timeUntilFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(timeFields));

		timeFromFieldComboBox.setEnabled(bWithTime);
		timeUntilFieldComboBox.setEnabled(bWithTime);
		timeSpanPane.setEnabled(bWithTime);
		if (!bWithTime) {
			timeSpanPane.setText("");
		}
	}
	
	private void configureRelationEntityComboBox() {
		final List<EntityMeta<?>> relationEntities = new ArrayList<EntityMeta<?>>();
		
		Set<UID> entryEntities = new HashSet<UID>();
		for (Object o: configVO.getListEntriesOrMileStones(false)) {
			ClientPlanElement<PK,R,Collectable<PK>> rpEntry = (ClientPlanElement<PK,R,Collectable<PK>>) o;
			entryEntities.add(rpEntry.getEntity());
		}
		if (entryEntities.size() > 0) {
			relationEntities.add(EntityMeta.NULL);
			relationEntities.addAll(getSlaveEntities(entryEntities, 2));			
		}
		
		Collections.sort(relationEntities, EntityUtils.getMetaComparator(EntityMeta.class));
		relationEntityComboBox.setModel(new ListComboBoxModel<EntityMeta<?>>(relationEntities));
		
		relationEntityComboBox.setEnabled(true);
		newRelationFromControllerCheckBox.setEnabled(true);
	}
	
	private List<EntityMeta<?>> getSlaveEntities(Set<UID> masterEntities, int minForeignKeys) {
		final List<EntityMeta<?>> entities = new ArrayList<EntityMeta<?>>();
		for (EntityMeta<?> entity : MetaProvider.getInstance().getAllEntities()) {
			if (E.isNuclosEntity(entity.getUID()))
				continue;
			
			int countFieldsToResource = 0;
			for (FieldMeta<?> efMeta : MetaProvider.getInstance().getAllEntityFieldsByEntity(entity.getUID()).values()) {
				if (masterEntities.contains(efMeta.getForeignEntity())) {
					countFieldsToResource++;
				}
			}
			if (countFieldsToResource >= minForeignKeys) {
				entities.add(EntityUtils.wrapMetaData(entity));
			}
		}
		
		Collections.sort(entities, EntityUtils.getMetaComparator(EntityMeta.class));
		
		return entities;
	}

	private void configureRelationFieldComboBoxes() {
		final UID relEntity = relationEntityComboBox.getSelectedItem() == null
				? null : ((EntityMeta<?>) relationEntityComboBox.getSelectedItem()).getUID();
		
		final List<FieldMeta<?>> fieldNames = new ArrayList<FieldMeta<?>>();
		if (relEntity != null) {
			for (Object o: configVO.getListEntriesOrMileStones(false)) {
				final ClientPlanElement<PK,R,Collectable<PK>> rpEntry = (ClientPlanElement<PK,R,Collectable<PK>>) o;
				final UID entryEntity = rpEntry.getEntity();
				for (FieldMeta<?> field : MetaProvider.getInstance().getAllEntityFieldsByEntity(relEntity).values()) {
					if (LangUtils.equal(entryEntity, field.getForeignEntity())) {
						fieldNames.add(EntityUtils.wrapMetaData(field));
					}
				}
			}	
		}
		Collections.sort(fieldNames, EntityUtils.getMetaComparator(FieldMeta.class));
		
		fieldNames.add(0, FieldMeta.NULL);
		
		relationFromFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(fieldNames));
		relationToFieldComboBox.setModel(new ListComboBoxModel<FieldMeta<?>>(fieldNames));
		relationFromFieldComboBox.setEnabled(true);
		relationToFieldComboBox.setEnabled(true);
	}
	
	private void updateRelationFromFieldComboBox() {
		final FieldMeta<?> relToField = (FieldMeta<?>) relationToFieldComboBox.getSelectedItem();
		final FieldMeta<?> relFromField = (FieldMeta<?>) relationFromFieldComboBox.getSelectedItem();
		if (relToField == null && relFromField == null) {
			if (relationFromFieldComboBox.getItemCount() > 1) {
				relationFromFieldComboBox.setSelectedIndex(1);
			}
		} else if (LangUtils.equal(relToField, relFromField)) {
			relationFromFieldComboBox.setSelectedIndex(0);
		} else if (relFromField == null) {
			for (int i = 1; i < relationFromFieldComboBox.getModel().getSize(); i++) {
				if (!LangUtils.equal(relToField, relationFromFieldComboBox.getModel().getElementAt(i))) {
					relationFromFieldComboBox.setSelectedIndex(i);
				}
			}
		}
	}
	
	private void updateRelationToFieldComboBox() {
		final FieldMeta<?> relToField = (FieldMeta<?>) relationToFieldComboBox.getSelectedItem();
		final FieldMeta<?> relFromField = (FieldMeta<?>) relationFromFieldComboBox.getSelectedItem();
		if (LangUtils.equal(relToField, relFromField)) {
			relationToFieldComboBox.setSelectedIndex(0);
		} else if (relToField == null) {
			for (int i = 1; i < relationToFieldComboBox.getModel().getSize(); i++) {
				if (!LangUtils.equal(relFromField, relationToFieldComboBox.getModel().getElementAt(i))) {
					relationToFieldComboBox.setSelectedIndex(i);
				}
			}
		}
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		ItemSelectable source = e.getItemSelectable();
		if (source == typeComboBox) {
			configureTypeComboBox();
			configureRelationEntityComboBox();
			configureRelationFieldComboBoxes();
		}
		if (source == entryEntityComboBox) {
			configureReferenceFieldComboBox();
			configureBookerFieldComboBox();
		}
		if (source == entryEntityComboBox || source == withTimeCheckBox) {
			configureDateTimeComboBoxes(source == withTimeCheckBox);
		}
		if (source == relationEntityComboBox) {
			configureRelationFieldComboBoxes();
			updateRelationFromFieldComboBox();
			updateRelationToFieldComboBox();
		}
		if (source == relationFromFieldComboBox) {
			updateRelationToFieldComboBox();
		}
		if (source == relationToFieldComboBox) {
			updateRelationFromFieldComboBox();
		}
		updateState();
	}
	
	@Override
	public void stateChanged(ChangeEvent e) {
		updateState();
	}

	protected void updateState() {
		boolean complete = true;
		if (isRelation()) {
			complete &= (relationEntityComboBox.getSelectedItem() != null && !LangUtils.equal(relationEntityComboBox.getSelectedItem(), EntityMeta.NULL))
					&& (relationFromFieldComboBox.getSelectedItem() != null && !LangUtils.equal(relationFromFieldComboBox.getSelectedItem(), FieldMeta.NULL))
						&& (relationToFieldComboBox.getSelectedItem() != null && !LangUtils.equal(relationToFieldComboBox.getSelectedItem(), FieldMeta.NULL));

			boolean withRelation = complete;
			relationPresentationOrthogonal.setEnabled(withRelation);
			relationPresentationStraight.setEnabled(withRelation);
			relationFromPlain.setEnabled(withRelation);
			relationFromDot.setEnabled(withRelation);
			relationFromArrow.setEnabled(withRelation);
			relationToPlain.setEnabled(withRelation);
			relationToDot.setEnabled(withRelation);
			relationToArrow.setEnabled(withRelation);			
		}
		complete &= 
				(entryEntityComboBox.getSelectedItem() != null && !LangUtils.equal(entryEntityComboBox.getSelectedItem(), EntityMeta.NULL))
					&& (refFieldComboBox.getSelectedItem() != null && !LangUtils.equal(refFieldComboBox.getSelectedItem(), FieldMeta.NULL))
						&& (dateFromFieldComboBox.getSelectedItem() != null && !LangUtils.equal(dateFromFieldComboBox.getSelectedItem(), FieldMeta.NULL))
							&& (dateUntilFieldComboBox.getSelectedItem() != null && !LangUtils.equal(dateUntilFieldComboBox.getSelectedItem(), FieldMeta.NULL));
		if (withTimeCheckBox.isSelected()) {
			complete &= !timeSpanPane.getText().isEmpty()
				&& (timeFromFieldComboBox.getSelectedItem() != null && !LangUtils.equal(timeFromFieldComboBox.getSelectedItem(), FieldMeta.NULL))
					&& (timeUntilFieldComboBox.getSelectedItem() != null && !LangUtils.equal(timeUntilFieldComboBox.getSelectedItem(), FieldMeta.NULL));
		}
		setComplete(complete);
	}

	protected void invalidStateLocalized(JComponent comp, String resourceId, Object...args) throws InvalidStateException {
		invalidState(comp, localeDelegate.getMessage(resourceId, null, args));
	}

	protected void invalidState(JComponent comp, String message) throws InvalidStateException {
		Bubble.Position position = Bubble.Position.SE;
		if (comp == null) {
			comp = this;
			position = Bubble.Position.UPPER;
		}
		new Bubble(comp, message, 8, position).setVisible(true);
		throw new InvalidStateException(message, false);
	}

	@Override
	public void applyState() throws InvalidStateException {
		planElement.setType(typeComboBox.getSelectedIndex() + 1);
		if (isRelation()) {
			planElement.setEntity(((EntityMeta<?>) relationEntityComboBox.getSelectedItem()).getUID());
			planElement.setPrimaryField(((FieldMeta<?>) relationFromFieldComboBox.getSelectedItem()).getUID());
			planElement.setSecondaryField(((FieldMeta<?>) relationToFieldComboBox.getSelectedItem()).getUID());
			planElement.setNewRelationFromController(newRelationFromControllerCheckBox.isSelected());

			if (relationPresentationStraight.isSelected()) {
				planElement.setPresentation(ClientResPlanConfigVO.RELATION_PRESENTATION_STRAIGHT);
			} else if (relationPresentationOrthogonal.isSelected()) {
				planElement.setPresentation(ClientResPlanConfigVO.RELATION_PRESENTATION_ORTHOGONAL);
			} 
			
			if (relationFromPlain.isSelected()) {
				planElement.setFromPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_PLAIN);
			} else if (relationFromDot.isSelected()) {
				planElement.setFromPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_DOT);
			} else if (relationFromArrow.isSelected()) {
				planElement.setFromPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_ARROW);
			}
			
			if (relationToPlain.isSelected()) {
				planElement.setToPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_PLAIN);
			} else if (relationToDot.isSelected()) {
				planElement.setToPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_DOT);
			} else if (relationToArrow.isSelected()) {
				planElement.setToPresentation(ClientResPlanConfigVO.RELATION_ENDPOINT_PRESENTATION_ARROW);
			}
			planElement.setColor(colorRelation.getText());
			planElement.setFromIcon(sIconFrom.getText());
			planElement.setToIcon(sIconTo.getText());
			return;
		}
		
		stopCellEditing();
		
		planElement.setEntity(entryEntityComboBox.getSelectedItem() == null
				? null : ((EntityMeta<?>) entryEntityComboBox.getSelectedItem()).getUID());
		planElement.setPrimaryField(refFieldComboBox.getSelectedItem() == null
				? null : ((FieldMeta<?>) refFieldComboBox.getSelectedItem()).getUID());
		planElement.setBookerField(bookerFieldComboBox.getSelectedItem() == null
				? null : ((FieldMeta<?>) bookerFieldComboBox.getSelectedItem()).getUID());
		planElement.setDateFromField(dateFromFieldComboBox.getSelectedItem() == null
				? null : ((FieldMeta<?>) dateFromFieldComboBox.getSelectedItem()).getUID());
		planElement.setColor(colorEntry.getText());
		planElement.setFromIcon(sIcon.getText());
		int iFixedExtend = 0;
		try {
			iFixedExtend = Integer.parseInt(fixedExtend.getText());
		} catch (Exception e) {
		}
		planElement.setFixedExtend(iFixedExtend);
		int iOrder = 0;
		try {
			iOrder = Integer.parseInt(order.getText());
		} catch (Exception e) {
		}
		planElement.setOrder(iOrder);
		planElement.setCascade(bCascade.isSelected());
		
		if (isEntry()) {
			planElement.setDateUntilField(dateUntilFieldComboBox.getSelectedItem() == null
					? null : ((FieldMeta<?>) dateUntilFieldComboBox.getSelectedItem()).getUID());
			if (withTimeCheckBox.isSelected()) {
				planElement.setTimeFromField(timeFromFieldComboBox.getSelectedItem() == null
						? null : ((FieldMeta<?>) timeFromFieldComboBox.getSelectedItem()).getUID());
				planElement.setTimeUntilField(timeUntilFieldComboBox.getSelectedItem() == null
						? null : ((FieldMeta<?>) timeUntilFieldComboBox.getSelectedItem()).getUID());
				planElement.setTimePeriodsString(timeSpanPane.getText());
			} else {
				planElement.setTimePeriodsString(null);
				planElement.setTimeFromField(null);
				planElement.setTimeUntilField(null);
			}	
	
			planElement.setPlanElementLocaleVO(tablemodel2.getRows());
//			if (LangUtils.equal(dateFromFieldComboBox.getSelectedItem(), dateUntilFieldComboBox.getSelectedItem()))
//				invalidStateLocalized(dateUntilFieldComboBox, "nuclos.resplan.wizard.step3.check.differentDateFiels");
	
			if (withTimeCheckBox.isSelected()) {
				if (LangUtils.equal(timeFromFieldComboBox.getSelectedItem(), timeUntilFieldComboBox.getSelectedItem())) {
					invalidStateLocalized(dateUntilFieldComboBox, "nuclos.resplan.wizard.step3.check.differentTimeFiels");
				}
				LocalTime startTime = null;
				LocalTime time = null;
				boolean dayNightTransition = false;
				for (Pair<LocalTime, LocalTime> p : timeSpanPane.getLocalTimeSpans()) {
					if (startTime == null) {
						startTime = p.x;
					}
					int xToY = p.x.compareTo(p.y);
					if (xToY == 0) {
						invalidStateLocalized(timeSpanPane, "nuclos.resplan.wizard.step3.check.emptyTimespan", p.x, p.y);
					}
					boolean valid = (xToY <= 0);
					if (!valid && !dayNightTransition && p.y.compareTo(startTime) <= 0) {
						dayNightTransition = true;
						valid = true;
					}
					if (time != null && time.compareTo(p.x) > 0) {
						valid = false;
					}
					if (!valid) {
						invalidStateLocalized(timeSpanPane, "nuclos.resplan.wizard.step3.check.invalidTimespan", p.x, p.y);
					}
					time = p.y;
				}
			}
		}
	}
}
