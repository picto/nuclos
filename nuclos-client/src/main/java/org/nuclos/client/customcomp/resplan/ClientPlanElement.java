package org.nuclos.client.customcomp.resplan;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.customcomp.resplan.PlanElement;
import org.nuclos.common.customcomp.resplan.PlanElementLocaleVO;
import org.nuclos.common2.LocaleInfo;

/**
 * @author Oliver Brausch
 */

//Version
@SuppressWarnings("serial")
@XmlType
@XmlRootElement(name="planElement")
public class ClientPlanElement<PK,R,C extends Collectable<PK>> extends PlanElement<R> implements HasCollHelper<PK,R,C> {
	
	//Transient stuff
	private transient CollectableHelper<PK,R,C> collHelper;
	
	public ClientPlanElement() {
		super();
	}

	public ClientPlanElement(PlanElement ple) {
		super(ple);
	}

	@Override
	public CollectableHelper<PK,R,C> getCollHelper() {
		if (collHelper == null) {
			collHelper = (CollectableHelper<PK,R,C>) CollectableHelper.getForEntity(getEntity());
		}
		return collHelper;
	}

	public PlanElementLocaleVO getPlanElementLocale(LocaleInfo li) {
		for (LocaleInfo locale : LocaleDelegate.getInstance().getParentChain()) {
			for (PlanElementLocaleVO result : getPlanElementLocaleVO()) {
				if (locale.getLocale().equals(result.getLocaleId())) {
					return result;
				}
			}
		}
		// if no resources can be determined, return an empty resource set
		return new PlanElementLocaleVO();
	}
	
}