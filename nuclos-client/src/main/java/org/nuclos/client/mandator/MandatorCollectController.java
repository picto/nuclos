//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.mandator;

import java.awt.event.ActionEvent;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.client.common.NuclosCollectControllerFactory;
import org.nuclos.client.common.NuclosResultPanel;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.resource.NuclosResourceCache;
import org.nuclos.client.ui.Errors;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;

public class MandatorCollectController extends MasterDataCollectController<UID> {
	
	private static final Logger LOG = Logger.getLogger(MandatorCollectController.class);
	
	private final JButton btnOpenLevel;

	public MandatorCollectController(MainFrameTab tabIfAny) {
		super(E.MANDATOR, tabIfAny, null);
		btnOpenLevel = new JButton(new AbstractAction(getSpringLocaleDelegate().getMessage("nuclos.entity.mandatorLevel.label", "Ebene"), 
				MainFrame.resizeAndCacheTabIcon(NuclosResourceCache.getNuclosResourceIcon("org.nuclos.client.resource.icon.glyphish-blue.189-plant.png"))) {
			@Override
			public void actionPerformed(ActionEvent e) {
				MasterDataCollectController<?> mdctrl = NuclosCollectControllerFactory.getInstance().newMasterDataCollectController(E.MANDATOR_LEVEL.getUID(), null, null);
				try {
					mdctrl.run();
				} catch (CommonBusinessException e1) {
					LOG.error(e1.getMessage(), e1);
					Errors.getInstance().showExceptionDialog(getTab(), e1);
				}
			}
		});
	}
	
	@Override
	protected void setupResultToolBar() {
		super.setupResultToolBar();
		final NuclosResultPanel<UID, CollectableMasterDataWithDependants<UID>> rp = getResultPanel();
		rp.addToolBarComponent(btnOpenLevel);
	}

}
