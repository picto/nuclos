//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.startup;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.log4j.Logger;
import org.nuclos.client.LocalUserProperties;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.caching.NBCache.LookupProvider;
import org.nuclos.common.caching.TimedCache;
import org.nuclos.common2.LangUtils;
import org.nuclos.server.common.ejb3.LocalUserCachesFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Local user properties that cannot be stored on the server, because they are needed before
 * the client is connected to the server, such as the user name and the look &amp; feel.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class LoadLocalUserCaches {

	private static final Logger LOG = Logger.getLogger(LoadLocalUserCaches.class);
	
	private static final long MAX_LOAD_MS = 5000;

	static final boolean USE_HASH = false;
	static final boolean USE_ENCRYPTION = true;
	static final String LOCALUSERCACHES_HASH = "localusercaches.hash";
	private static final String LOCALUSERCACHES_APP_VERSION = "localusercaches.app.version";

	private transient LocalUserPayload payload;
	
	// Spring injection
	
	private transient LocalUserCachesFacadeRemote remoteInterface;
	
	@Autowired
	private SecurityDelegate securityDelegate;
	
	@Autowired
	private LocalUserProperties localUserProperties;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	// end of Spring injection

	private static LoadLocalUserCaches INSTANCE;

	LoadLocalUserCaches() {
		INSTANCE = this;
	}

	/**
	 * Spring injection setter.
	 */
	public final void setLocalUserCachesService(LocalUserCachesFacadeRemote service) {
		this.remoteInterface = service;
	}

	@PostConstruct
	void init() {
		final File cacheFile = getCachesFile(applicationProperties);
		boolean deleteCache = false;
		InputStream in = null;
		try {
			if (USE_HASH && !LangUtils.equal(localUserProperties.get(LOCALUSERCACHES_HASH), getHash(cacheFile))) {
				LOG.info("hash missmatch. skipping.");
			} else {
				if (USE_ENCRYPTION) {
					in = new BufferedInputStream(new CipherInputStream(
							new FileInputStream(cacheFile), createCipher(
									Cipher.DECRYPT_MODE, securityDelegate.getCurrentApplicationInfoOnServer())));
				} else {
					in = new BufferedInputStream(new FileInputStream(cacheFile));
				}
				final ObjectInputStream oin = new ObjectInputStream(in);

				// Local class
				final class MyRunnable implements Runnable {

					private boolean succeeded = false;

					private MyRunnable() {
					}

					private boolean isSucceeded() {
						return succeeded;
					}

					@Override
					public void run() {
						try {
							payload = (LocalUserPayload) oin.readObject();
							succeeded = true;
						} catch (IOException e) {
							// other exception... just log and start with empty or default values.
							LOG.warn("Lokale Caches konnten nicht geladen werden: " + e);
						} catch (ClassNotFoundException e) {
							// other exception... just log and start with empty or default values.
							LOG.warn("Lokale Caches konnten nicht geladen werden: " + e);
						} finally {
							try {
								oin.close();								
							} catch (IOException io) {
								LOG.warn("Konnte ObjectInputStream nicht schliessen: " + io);
							}
						}
					}
				}

				final MyRunnable run = new MyRunnable();
				final Thread thread = new Thread(run, "LoadLocalUserCaches");
				thread.start();
				thread.join(MAX_LOAD_MS);
				deleteCache = !run.isSucceeded();

				if (payload == null) {
					LOG.warn("Unable to load local user caches from file " + cacheFile
							+ " within " + MAX_LOAD_MS + "ms");
				} else {
					// compare current version
					if (!LangUtils.equal(E.getSchemaVersion(), payload.getSchemaVersion())) {
						LOG.info("version missmatch. skipping.");
						payload = null;
					}
				}
			}
		} catch (StreamCorruptedException ex) {
			// local caches are tampered
			LOG.info("Lokale Caches konnten nicht geladen werden, da " + cacheFile + " das falsche Format hat");
			deleteCache = true;
		} catch (FileNotFoundException ex) {
			// The properties file doesn't exist. or other exception...
			// So we start with empty or default values.
			LOG.info("Lokale Caches konnten nicht geladen werden, da " + cacheFile + " nicht vorhanden ist");
		} catch (GeneralSecurityException ex) {
			// other exception... just log and start with empty or default values.
			LOG.warn("Lokale Caches konnten nicht geladen werden: " + ex);
			deleteCache = true;
		} catch (IllegalArgumentException ex) {
			// other exception... just log and start with empty or default values.
			LOG.warn("Lokale Caches konnten nicht geladen werden: " + ex);
			deleteCache = true;
		} catch (Exception ex) {
			// other exception... just log and start with empty or default values.
			LOG.error("Lokale Caches konnten nicht geladen werden: " + ex, ex);
			deleteCache = true;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// Caused by: java.io.IOException: javax.crypto.BadPaddingException: Given final block not properly padded
					// ignore
					LOG.warn("Problem bei der Methode close() der lokalen Caches: " + e, e);
					deleteCache = true;
				}
			}
		}
		if (deleteCache) {
			if (cacheFile != null) {
				cacheFile.delete();
				LOG.warn("Deleted " + cacheFile + " as there were errors reading it");
			}
		}
		if (payload != null) {
			LOG.info("Payload " + payload + " read from " + cacheFile
					+ ": schema=" + payload.getSchemaVersion() + " time=" + new Date(payload.getCachingTime()));
		}
	}

	public LocalUserCache load(Class<? extends LocalUserCache> clazz) {
		if (payload == null) {
			return null;
		}
		final String name = clazz.getName();
		final Iterator<String> it1 = payload.getClassNames().iterator();
		final Iterator<LocalUserCache> it2 = payload.getCaches().iterator();
		while (it1.hasNext()) {
			final String n = it1.next();
			final LocalUserCache luc = it2.next();
			luc.setDeserialized(true);
			luc.setLoadLocalUserCaches(this);
			if (name.equals(n)) {
				LOG.info("Sucessfully resurrected " + luc + " from local user cache payload:"
						+ " deserialized=" + luc.wasDeserialized());
				return luc;
			}
		}
		return null;
	}

	public boolean checkValid(LocalUserCache cache) {
		return checkValid(cache.getCachingTopic());
	}
	
	private boolean checkValid(String topic) {
		final Map<String, Date> mpRevalidationTimes = timedCache.get("");
		final Date revalidationTime = mpRevalidationTimes.get(topic);
		if (revalidationTime == null) {
			return true; // no date from server... must be valid - no changes.
		} else {
			return payload.getCachingTime() > revalidationTime.getTime();
		}		
	}

	private final TimedCache<String, Map<String, Date>> timedCache = 
			new TimedCache<String, Map<String, Date>>(new GetLocalCacheRevalidationProvider(), 1);

	private final class GetLocalCacheRevalidationProvider implements LookupProvider<String, Map<String, Date>>{		
		@Override
		public Map<String, Date> lookup(String key) {
			String topic = key == null || key.isEmpty() ? null : key;
			return remoteInterface.queryLocalCacheRevalidation(topic);
		}		
	}
	
	static File getCachesFile(ApplicationProperties applicationProperties) {
		File fileHomeDir = new File(System.getProperty("user.home"));
		String fileName = applicationProperties.getAppId() + ".caches";
		fileName = System.getProperty("local.caches.filename", fileName);
		return new File(fileHomeDir, fileName);
	}

	static Cipher createCipher(int mode, String password) throws GeneralSecurityException {
		String alg = "PBEWithSHA1AndDESede"; //BouncyCastle has better algorithms
		PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(alg);
		SecretKey secretKey = keyFactory.generateSecret(keySpec);

		Cipher cipher = Cipher.getInstance("PBEWithSHA1AndDESede");
		// TODO: A fixed salt doesn't help anything.
		cipher.init(mode, secretKey, new PBEParameterSpec("saltsalt".getBytes(), 2000));

		return cipher;
	}

	static String getHash(File cacheFile) throws GeneralSecurityException, IOException {
		final MessageDigest md = MessageDigest.getInstance("SHA-256");
		final InputStream fis = new BufferedInputStream(new FileInputStream(cacheFile));
		final byte[] dataBytes = new byte[1024 * 8];
		try {
			int nread = 0;
			while ((nread = fis.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			}
		} finally {
			fis.close();
		}
		byte[] mdbytes = md.digest();

		//convert the byte to hex format method 1
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		//convert the byte to hex format method 2
		final StringBuilder hexString = new StringBuilder();
		for (int i = 0; i < mdbytes.length; i++) {
			hexString.append(Integer.toHexString(0xFF & mdbytes[i]));
		}

		return hexString.toString();
	}

} // class LocalUserCaches
