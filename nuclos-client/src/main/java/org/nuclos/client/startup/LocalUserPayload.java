package org.nuclos.client.startup;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class LocalUserPayload implements Serializable {

	private String schemaVersion;

	// List of Caches to store on disk

	/*
	private MetaProvider metaProvider;
	private ResourceCache resourceCache;
	private GenericObjectMetaDataCache genericObjectMetaDataCache;
	private MasterDataCache masterDataCache;
	private MetaDataCache metaDataCache;
	private GenericObjectLayoutCache genericObjectLayoutCache;
	private CustomComponentCache customComponentCache;
	private ClientParameterProvider clientParameterProvider;
	private LocaleDelegate localeDelegate;
	private StateDelegate stateDelegate;
	*/

	private final long cachingTime;
	
	private final List<String> classNames;

	private final List<LocalUserCache> caches;
	
	LocalUserPayload() {
		this.classNames = new LinkedList<String>();
		this.caches = new LinkedList<LocalUserCache>();
		this.cachingTime = System.currentTimeMillis();
	}
	
	void storeObject(LocalUserCache object) {
		if (object != null) {
			final Class<? extends LocalUserCache> clazz = object.getClass();
			object.setDeserialized(false);
			classNames.add(clazz.getName());
			caches.add(object);
		}
	}

	String getSchemaVersion() {
		return schemaVersion;
	}

	void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	List<String> getClassNames() {
		return classNames;
	}

	long getCachingTime() {
		return cachingTime;
	}

	List<LocalUserCache> getCaches() {
		return caches;
	}

}
