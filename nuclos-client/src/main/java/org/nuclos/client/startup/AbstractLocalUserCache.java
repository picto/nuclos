package org.nuclos.client.startup;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractLocalUserCache implements LocalUserCache {
	
	private transient boolean blnDeserialized = false;
	
	// Spring injection
	
	@Autowired
	private transient LoadLocalUserCaches loadLocalUserCaches;
	
	// end of Spring injection
	
	protected AbstractLocalUserCache() {
	}
	
	@Override
	public final void setDeserialized(boolean blnDeserialized) {
		this.blnDeserialized = blnDeserialized;
	}
	
	@Override
	public final boolean wasDeserialized() {
		return blnDeserialized;
	}
	
	@Override
	public final boolean isValid() {
		return wasDeserialized() && loadLocalUserCaches.checkValid(this);
	}

	@Override
	public void setLoadLocalUserCaches(LoadLocalUserCaches loadLocalUserCaches) {
		this.loadLocalUserCaches = loadLocalUserCaches;
	}
}
