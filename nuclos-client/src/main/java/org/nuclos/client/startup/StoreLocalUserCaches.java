//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.startup;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;

import org.apache.log4j.Logger;
import org.nuclos.client.LocalUserProperties;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.customcomp.CustomComponentCache;
import org.nuclos.client.genericobject.GenericObjectLayoutCache;
import org.nuclos.client.genericobject.GenericObjectMetaDataCache;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.resource.ResourceCache;
import org.nuclos.client.statemodel.StateDelegate;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.E;
import org.nuclos.common.ParameterProvider;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Local user properties that cannot be stored on the server, because they are needed before
 * the client is connected to the server, such as the user name and the look &amp; feel.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public class StoreLocalUserCaches {

	private static final Logger LOG = Logger.getLogger(StoreLocalUserCaches.class);

	// Spring injection
	
	@Autowired
	private SecurityDelegate securityDelegate;
	
	@Autowired
	private LocalUserProperties localUserProperties;
	
	@Autowired
	private ApplicationProperties applicationProperties;
	
	// Caches to (de)serialize
	
	@Autowired
	private MetaProvider metaProvider;
	
	@Autowired
	private ResourceCache resourceCache;
	
	@Autowired
	private GenericObjectMetaDataCache genericObjectMetaDataCache;
	
	@Autowired
	private MasterDataCache masterDataCache;
	
	@Autowired
	private GenericObjectLayoutCache genericObjectLayoutCache;
	
	@Autowired
	private CustomComponentCache customComponentCache;
	
	@Autowired
	private ClientParameterProvider clientParameterProvider;
	
	@Autowired
	private LocaleDelegate localeDelegate;
	
	@Autowired
	private StateDelegate stateDelegate;	
	
	// end of Spring injection

	private static StoreLocalUserCaches INSTANCE;

	StoreLocalUserCaches() {
		INSTANCE = this;
	}

	public void store() {
		try {
			final File cacheFile = LoadLocalUserCaches.getCachesFile(applicationProperties);
			final boolean disabled = Boolean.valueOf(
					clientParameterProvider.getValue(ParameterProvider.KEY_DISABLE_CLIENT_LOCAL_USER_CACHE));
			if (disabled) {
				if (cacheFile.exists()) {
					cacheFile.deleteOnExit();
					cacheFile.delete();
					LOG.info("client local user cache is DISABLED, thus deleting " + cacheFile);
				}
			} else {
				final BufferedOutputStream out;
	
				if (LoadLocalUserCaches.USE_ENCRYPTION) {
					out = new BufferedOutputStream(
							new CipherOutputStream(new FileOutputStream(cacheFile),
									LoadLocalUserCaches.createCipher(Cipher.ENCRYPT_MODE, 
											securityDelegate.getCurrentApplicationInfoOnServer())));
				}
				else {
					out = new BufferedOutputStream(new FileOutputStream(cacheFile));
				}
	
				try {
					final LocalUserPayload payload = new LocalUserPayload();
					payload.setSchemaVersion(E.getSchemaVersion());
					
					payload.storeObject(metaProvider);
					//xstoreObject(AttributeCache.getInstance());
					payload.storeObject(resourceCache);
					payload.storeObject(genericObjectMetaDataCache);
					payload.storeObject(masterDataCache);
					// payload.storeObject(metaDataCache);
					payload.storeObject(genericObjectLayoutCache);
					//xstoreObject(SearchFilterCache.getInstance());
					payload.storeObject(customComponentCache);
					payload.storeObject(clientParameterProvider);
					payload.storeObject(localeDelegate);
					//xstoreObject(TasklistCache.getInstance());
					payload.storeObject(stateDelegate);
	
					// store(out, ApplicationProperties.getInstance().getAppId() + " Local User Caches");
					
					final ObjectOutputStream oout = new ObjectOutputStream(out);
					oout.writeObject(payload);
					LOG.info("Sucessfull stored local user cache payload in " + cacheFile
							+ ": schema=" + payload.getSchemaVersion());
				} finally {
					out.close();
				}
	
				if (LoadLocalUserCaches.USE_HASH) {
					// get sha hash.
					localUserProperties.put(LoadLocalUserCaches.LOCALUSERCACHES_HASH, LoadLocalUserCaches.getHash(cacheFile));
					localUserProperties.store();
				}
			}
		} catch (GeneralSecurityException e) {
			final String sMessage = "Lokale Caches konnten nicht gespeichert werden: " + e;
			LOG.error(sMessage, e);
		} catch (IOException e) {
			final String sMessage = "Lokale Caches konnten nicht gespeichert werden: " + e;
			LOG.error(sMessage, e);
		} catch (Exception ex) {
			final String sMessage = "Lokale Caches konnten nicht gespeichert werden: " + ex;
			LOG.error(sMessage, ex);
		}
	} // store
	
} // class LocalUserCaches
