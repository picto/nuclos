package org.nuclos.client;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common2.LangUtils;

public class StartIcons {

	private static final Logger LOG = Logger.getLogger(NuclosIcons.class);
	
	private static StartIcons INSTANCE;

	StartIcons() {
		INSTANCE = this;
	}
	
	/**
	 * @return the one and only instance of <code>Icons</code>
	 */
	public static StartIcons getInstance() {
		if (INSTANCE == null) INSTANCE= new StartIcons();
		return INSTANCE;
	}
	
	private static enum StartIcon{BIG_TRANSPARENT_APP_ICON_512,
		CUSTOMER_ICON, DEFAULT_FRAME_ICON};
	
	/**
	 * icon cache
	 */
	private final Map<StartIcon, ImageIcon> mpIcons = new HashMap<StartIcon, ImageIcon>(); 
	
	/**
	 * @return a transparent (preferably) 512x512 or greater icon.
	 */
	public ImageIcon getBigTransparentApplicationIcon512() {
		if (!mpIcons.containsKey(StartIcon.BIG_TRANSPARENT_APP_ICON_512)) {
			// @todo Use a Nucleus icon as default
			String sBigIconFilename = "org/nuclos/client/images/nucleus-96x96-transparent.png";
			if (ApplicationProperties.arePropertiesAvailaible()) {
				sBigIconFilename = LangUtils.defaultIfNull(
						ApplicationProperties.getInstance().getBigTransparentIcon512FileName(), 
						sBigIconFilename);			
			}
			mpIcons.put(StartIcon.BIG_TRANSPARENT_APP_ICON_512, getImageIcon(sBigIconFilename));
		}
		return mpIcons.get(StartIcon.BIG_TRANSPARENT_APP_ICON_512);
	}
	
	public Icon getIconCustomer() {
		if (!mpIcons.containsKey(StartIcon.CUSTOMER_ICON)) {
			String sCustomerIconFilename = "org/nuclos/client/images/eplus-logo-scaled.png";
			if (ApplicationProperties.arePropertiesAvailaible()) {
				sCustomerIconFilename = LangUtils.defaultIfNull(
					ApplicationProperties.getInstance().getCustomerIconFileName(),
					sCustomerIconFilename);
			}
			mpIcons.put(StartIcon.CUSTOMER_ICON, getImageIcon(sCustomerIconFilename));
		}
		return mpIcons.get(StartIcon.CUSTOMER_ICON);
	}

	public ImageIcon getDefaultFrameIcon() {
		if (!mpIcons.containsKey(StartIcon.DEFAULT_FRAME_ICON)) {
			String sFrameIconFileName = "org/nuclos/client/images/nucleus-16x16-whitecircle.png";
			if (ApplicationProperties.arePropertiesAvailaible()) {
				sFrameIconFileName = LangUtils.defaultIfNull(
						ApplicationProperties.getInstance().getFrameIconFileName(),
						sFrameIconFileName);
			}
			mpIcons.put(StartIcon.DEFAULT_FRAME_ICON, getImageIcon(sFrameIconFileName));
		}
		return mpIcons.get(StartIcon.DEFAULT_FRAME_ICON);
	}
	
	private ImageIcon getImageIcon(String sFileName) {
		try {
			return new ImageIcon(LangUtils.getClassLoaderThatWorksForWebStart().getResource(sFileName));
		}
		catch(NullPointerException e) {
			LOG.error("Can't find resource '" + sFileName + "' for icon");
			return new ImageIcon(Toolkit.getDefaultToolkit().createImage(sFileName));
		}
	}
}
