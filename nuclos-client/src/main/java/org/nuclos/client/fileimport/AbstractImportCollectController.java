//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.fileimport;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.common.LocaleDelegate;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.ui.CommonAbstractAction;
import org.nuclos.client.ui.Icons;
import org.nuclos.common.ProgressNotification;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.fileimport.ImportResult;
import org.nuclos.common2.KeyEnum;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;

import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

/**
 * Special masterdata collect controller for generic object file import.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
public abstract class AbstractImportCollectController extends MasterDataCollectController<UID> implements
		MessageListener {

	private static final Logger LOG = Logger.getLogger(AbstractImportCollectController.class);

	public static final String COMPONENTNAME_PROGRESSPANEL = "progressPanel";

	private final Action actImport = new CommonAbstractAction(Icons.getInstance().getIconPlay16(),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectImportCollectController.import", "Importieren")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdImport();
		}
	};

	private final Action actStop = new CommonAbstractAction(Icons.getInstance().getIconStop16(),
			getSpringLocaleDelegate().getMessage(
					"GenericObjectImportCollectController.stopimport", "Import abbrechen")) {

		@Override
		public void actionPerformed(ActionEvent ev) {
			cmdCancelImport();
		}
	};

	final ResourceBundle bundle;

	JButton btnStart;
	JButton btnStop;

	JProgressBar progressBar;

	ProgressNotification lastnotification;

	TopicNotificationReceiver tnr;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<code><pre>
	 * ResultController<~> rc = new ResultController<~>();
	 * *CollectController<~> cc = new *CollectController<~>(.., rc);
	 * </code></pre>
	 */
	AbstractImportCollectController(UID entityUid, MainFrameTab tabIfAny) {
		super(entityUid, tabIfAny, null);

		progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setSize(200, 100);

		Component placeHolder = getPlaceHolder(getDetailsPanel(), "lblPlaceholder1");
		// You only can replace the place holder once. (tp)
		if (placeHolder != null) {
			Container container = placeHolder.getParent();
			TableLayout layoutManager = (TableLayout) container.getLayout();
			TableLayoutConstraints constraints = layoutManager.getConstraints(placeHolder);

			container.remove(placeHolder);
			container.add(progressBar, constraints);
		}

		this.bundle = LocaleDelegate.getInstance().getResourceBundle();
	}

	abstract void cmdImport();

	abstract void cmdCancelImport();

	abstract void setResultLastRunInDetails();

	abstract void setCellRendererInResultTable();

	@Override
	public void init() {
		super.init();
		setCellRendererInResultTable();
		getResultTable().getModel().addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				setCellRendererInResultTable();
			}
		});
	}

	final TopicNotificationReceiver getTopicNotificationReceiver() {
		if (tnr == null) {
			tnr = SpringApplicationContextHolder.getBean(TopicNotificationReceiver.class);
		}
		return tnr;
	}

	void resetProgressBars() {
		try {
			if (lastnotification != null) {
				AbstractImportCollectController.this.progressBar.setString(getSpringLocaleDelegate()
						.getMessageFromResource(
								lastnotification.getMessage()));
				AbstractImportCollectController.this.progressBar.setMinimum(lastnotification.getProgressMinimum());
				AbstractImportCollectController.this.progressBar.setMaximum(lastnotification.getProgressMaximum());
				AbstractImportCollectController.this.progressBar.setValue(lastnotification.getValue());
			}
			else {
				AbstractImportCollectController.this.progressBar.setString(getSpringLocaleDelegate()
						.getMessageFromResource(
								"GenericObjectImportCollectController.18"));
				AbstractImportCollectController.this.progressBar.setMinimum(0);
				AbstractImportCollectController.this.progressBar.setMaximum(0);
				AbstractImportCollectController.this.progressBar.setValue(0);
			}
		}
		finally {
			lastnotification = null;
		}
	}

	static Component getPlaceHolder(Component component, String name) {
		if (name.equals(component.getName())) {
			return component;
		}

		if (component instanceof Container) {
			Container container = (Container) component;
			for (Component c : container.getComponents()) {
				Component result = getPlaceHolder(c, name);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	void setupDetailsToolBar(boolean running, boolean editmode) {
		//final JToolBar toolbar = UIUtils.createNonFloatableToolBar();

		if (btnStart != null) {
			this.getDetailsPanel().removeToolBarComponent(btnStart);
		}
		btnStart = new JButton(this.actImport);
		btnStart.setName("btnImport");
		btnStart.setText(getSpringLocaleDelegate().getMessage("GenericObjectImportCollectController.import",
				"Importieren"));
		btnStart.setEnabled(!running && !editmode);
		//toolbar.add(btnStart);
		this.getDetailsPanel().addToolBarComponent(btnStart);

		if (btnStop != null) {
			this.getDetailsPanel().removeToolBarComponent(btnStop);
		}
		btnStop = new JButton(this.actStop);
		btnStop.setName("btnStop");
		btnStop.setText(getSpringLocaleDelegate().getMessage("GenericObjectImportCollectController.stopimport",
				"Import abbrechen"));
		btnStop.setEnabled(running && !editmode);
		//toolbar.add(btnStop);
		this.getDetailsPanel().addToolBarComponent(btnStop);

		//this.getDetailsPanel().setCustomToolBarArea(toolbar);
	}

	Icon getIconForImportResult(String resultLastRun) {
		ImportResult ir = KeyEnum.Utils.findEnum(ImportResult.class, resultLastRun);
		if (ImportResult.OK.equals(ir)) {
			return Icons.getInstance().getIconJobSuccessful();
		}
		else if (ImportResult.ERROR.equals(ir)) {
			return Icons.getInstance().getIconJobError();
		}
		else if (ImportResult.INCOMPLETE.equals(ir)) {
			return Icons.getInstance().getIconJobWarning();
		}
		else {
			return Icons.getInstance().getIconJobUnknown();
		}
	}

	@Override
	public void onMessage(Message message) {
		try {
			if (message instanceof ObjectMessage) {
				ObjectMessage objectMessage = (ObjectMessage) message;
				if (objectMessage.getObject() instanceof ProgressNotification) {
					final ProgressNotification notification = (ProgressNotification) objectMessage.getObject();
					if (notification.getState() == ProgressNotification.RUNNING) {
						LOG.info("onMessage " + this + " progressing...");
						AbstractImportCollectController.this.btnStart.setEnabled(false);
						AbstractImportCollectController.this.btnStop.setEnabled(true);

						AbstractImportCollectController.this.progressBar.setString(
								getSpringLocaleDelegate().getMessageFromResource(notification.getMessage()));
						AbstractImportCollectController.this.progressBar.setMinimum(notification.getProgressMinimum());
						AbstractImportCollectController.this.progressBar.setMaximum(notification.getProgressMaximum());
						AbstractImportCollectController.this.progressBar.setValue(notification.getValue());
					}
					else {
						try {
							LOG.info("onMessage " + this + " refreshCurrentCollectable...");
							this.lastnotification = notification;
							refreshCurrentCollectable(false);
						}
						catch (CommonBusinessException e) {
							LOG.error("onMessage failed: " + e, e);
						}
					}
				}
			}
		}
		catch (JMSException ex) {
			LOG.error(ex);
		}
	}

	class ResultMessageCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected,
				boolean bHasFocus, int iRow, int iColumn) {
			final JLabel jLabel = (JLabel) super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus,
					iRow, iColumn);

			final String resultLastRun = oValue == null ? "" : oValue.toString();
			final StringBuffer resultMessage = new StringBuffer();
			final String[] results = resultLastRun.split("\n");
			for (int i = 0; i < results.length; i++) {
				resultMessage.append(localize(results[i]));
				if (i < results.length - 1)
					resultMessage.append(" ");
			}

			jLabel.setText(resultMessage.toString());

			return jLabel;
		}
	}

	class TrafficLightCellRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table, Object oValue, boolean bSelected,
				boolean bHasFocus, int iRow, int iColumn) {
			final JLabel jLabel = (JLabel) super.getTableCellRendererComponent(table, oValue, bSelected, bHasFocus,
					iRow, iColumn);

			jLabel.setText("");
			jLabel.setHorizontalAlignment(SwingConstants.CENTER);

			final String sValue = (String) ((CollectableField) oValue).getValue();
			jLabel.setIcon(getIconForImportResult(sValue));
			return jLabel;
		}
	}

	String localize(String message) {
		String resText = null;
		try {
			resText = bundle.getString(StringUtils.getFirstSubString(message, CollectionUtils.asSet("{", " ")));
		}
		catch (RuntimeException e) {
			// text seems to be no resourceId
		}

		if (resText != null) {
			Pattern REF_PATTERN = Pattern.compile("\\{([^\\}]*?)\\}");
			List<String> paramList = new ArrayList<String>();
			StringBuffer rb = new StringBuffer();

			Matcher m = REF_PATTERN.matcher(message);
			while (m.find()) {
				String param = m.group(1);
				try {
					if (param.length() > 0 && bundle.containsKey(param.trim())) {
						param = bundle.getString(param.trim());
					}
				}
				catch (RuntimeException e) {
					// param seems to be no resourceId
				}
				paramList.add(param);
			}

			m = REF_PATTERN.matcher(resText);
			while (m.find()) {
				if (paramList.size() > Integer.valueOf(m.group(1))) {
					m.appendReplacement(rb, paramList.get(Integer.valueOf(m.group(1))));
				}
			}
			m.appendTail(rb);
			return rb.toString();
		}
		else {
			return message;
		}
	}

} // class AbstractImportCollectController
