//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.fileimport;

import java.io.Serializable;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;
import org.nuclos.client.common.DependantCollectableMasterDataMap;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateConstants;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.component.CollectableTextArea;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.client.ui.collect.component.LabeledCollectableComponentWithVLP;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.dal.vo.IDependentDataMap;
import org.nuclos.common.fileimport.ImportMode;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.fileimport.NuclosFileImportException;
import org.nuclos.server.fileimport.ejb3.CsvImportFacadeRemote;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Special masterdata collect controller for generic object file import.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 */
@Configurable
public class CsvImportCollectController extends AbstractImportCollectController {

	private static final Logger LOG = Logger.getLogger(CsvImportCollectController.class);

	// Spring injection

	@Autowired
	CsvImportFacadeRemote importFacadeRemote;

	// end of Spring injection

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<code><pre>
	 * ResultController<~> rc = new ResultController<~>();
	 * *CollectController<~> cc = new *CollectController<~>(.., rc);
	 * </code></pre>
	 */
	public CsvImportCollectController(MainFrameTab tabIfAny) {
		super(E.IMPORTFILE.getUID(), tabIfAny);
	}

	@Override
	public void init() {
		super.init();
		getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				if (!SecurityCache.getInstance().isSuperUser()) {
					for (CollectableComponent c : getDetailCollectableComponentsFor(E.IMPORTFILE.mode.getUID())) {
						if (c instanceof LabeledCollectableComponentWithVLP) {
							c.setEnabled(false);
						}
					}
				}
				resetProgressBars();

				if (ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_VIEW) {
					setupDetailsToolBar(false, false);
				}
				else {
					setupDetailsToolBar(false, true);
				}
				if (ev.getNewCollectState().getInnerState() != CollectStateConstants.DETAILSMODE_NEW) {
					setResultLastRunInDetails();
				}
			}

			@Override
			public void detailsModeLeft(CollectStateEvent ev) throws CommonBusinessException {
				getTopicNotificationReceiver().unsubscribe(CsvImportCollectController.this);
			}
		});
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> newCollectableWithDefaultValues(boolean forInsert) {
		final CollectableMasterDataWithDependants<UID> result = super.newCollectableWithDefaultValues(forInsert);
		result.setField(E.IMPORTFILE.mode.getUID(), new CollectableValueField(ImportMode.NUCLOSIMPORT.getValue()));
		return result;
	}

	@Override
	public CollectableMasterDataWithDependants<UID> insertCollectable(CollectableMasterDataWithDependants<UID> clctNew)
			throws CommonBusinessException {
		if (clctNew.getPrimaryKey() != null) {
			throw new IllegalArgumentException("clctNew");
		}

		// We have to clear the ids for cloned objects:
		/**
		 * @todo eliminate this workaround - this is the wrong place. The right
		 *       place is the Clone action!
		 */
		final IDependentDataMap mpmdvoDependants = org.nuclos.common.Utils.clearIds(this.getAllSubFormData(null)
				.toDependentDataMap());
		clctNew.getMasterDataCVO().getEntityObject().setDependents(mpmdvoDependants);
		final MasterDataVO<UID> mdvoInserted = importFacadeRemote.createFileImport(new MasterDataVO<UID>(clctNew
				.getMasterDataCVO().getEntityObject()));
		mdvoInserted.getEntityObject().setDependents(this.readDependants(mdvoInserted.getPrimaryKey()));

		return new CollectableMasterDataWithDependants(clctNew.getCollectableEntity(),
				new MasterDataVO<UID>(mdvoInserted.getEntityObject()));
	}

	@Override
	protected CollectableMasterDataWithDependants<UID> updateCollectable(CollectableMasterDataWithDependants<UID> clct,
			Object oAdditionalData, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		final DependantCollectableMasterDataMap mpclctDependants = (DependantCollectableMasterDataMap) oAdditionalData;
		clct.getMasterDataCVO().getEntityObject().setDependents(mpclctDependants.toDependentDataMap());
		final Object oId = importFacadeRemote.modifyFileImport(
				new MasterDataVO<UID>(clct.getMasterDataCVO().getEntityObject()));

		final MasterDataVO<UID> mdvoUpdated = this.mddelegate.get(this.getEntityUid(), (UID) oId);
		return new CollectableMasterDataWithDependants(clct.getCollectableEntity(),
				new MasterDataVO<UID>(mdvoUpdated.getEntityObject()));
	}

	@Override
	protected void deleteCollectable(CollectableMasterDataWithDependants<UID> clct, final Map<String, Serializable> applyMultiEditContext) throws CommonBusinessException {
		importFacadeRemote.removeFileImport(clct.getMasterDataCVO());
	}

	void cmdImport() {
		UID importfileId = getSelectedCollectableId();
		try {
			final String correlationId;
			try {
				correlationId = importFacadeRemote.doImport(importfileId);
			}
			catch (RuntimeException ex) {
				throw new NuclosFatalException(ex);
			}
			if (correlationId != null) {
				CsvImportCollectController.this.progressBar.setString("Warte auf Status");
				setupDetailsToolBar(true, false);
				getTopicNotificationReceiver().subscribe(JMSConstants.TOPICNAME_PROGRESSNOTIFICATION,
						correlationId, CsvImportCollectController.this);
			}
		}
		catch (CommonBusinessException e) {
			JOptionPane.showMessageDialog(CsvImportCollectController.this.getDetailsPanel(),
					getSpringLocaleDelegate().getMessageFromResource(e.getMessage()), "", JOptionPane.ERROR_MESSAGE);
		}
	}

	void cmdCancelImport() {
		UID importfileId = getSelectedCollectableId();
		try {
			importFacadeRemote.stopImport(importfileId);
		}
		catch (NuclosFileImportException e) {
			LOG.error("cmdCancelImport failed: " + e, e);
			JOptionPane.showMessageDialog(CsvImportCollectController.this.getDetailsPanel(),
					getSpringLocaleDelegate().getMessageFromResource(e.getMessage()), "", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void setResultLastRunInDetails() {
		for (CollectableComponent clct : this.getDetailCollectableComponentsFor(E.IMPORTFILE.result.getUID())) {
			if (clct instanceof CollectableTextArea) {
				final String resultLastRun = this.getSelectedCollectable() != null
						? (this.getSelectedCollectable().getValue(E.IMPORTFILE.result.getUID()) != null
								? (String) this.getSelectedCollectable().getValue(E.IMPORTFILE.result.getUID()) : null)
						: null;
				final StringBuffer resultMessage = new StringBuffer();
				final String[] results = resultLastRun == null ? new String[0] : resultLastRun.split("\n");
				for (int i = 0; i < results.length; i++) {
					resultMessage.append(localize(results[i]));
					if (i < results.length - 1)
						resultMessage.append("\n");
				}
				((CollectableTextArea) clct).getModel().setFieldInitial(
						new CollectableValueField(resultMessage.toString()), true, !isDetailmodeAnyNew());
			}
		}
		for (CollectableComponent clct : this.getDetailCollectableComponentsFor(E.IMPORTFILE.laststate.getUID())) {
			if (clct instanceof CollectableTextField) {
				((CollectableTextField) clct).getJTextField().setVisible(false);
				((CollectableTextField) clct).getJLabel().setVisible(true);
				((CollectableTextField) clct).getJLabel().setText("");
				final String resultLastRun = this.getSelectedCollectable() != null
						? (this.getSelectedCollectable().getValue(E.IMPORTFILE.laststate.getUID()) != null
								? (String) this.getSelectedCollectable().getValue(E.IMPORTFILE.laststate.getUID())
								: null) : null;
				((CollectableTextField) clct).getJLabel().setIcon(getIconForImportResult(resultLastRun));
				clct.setToolTipText(resultLastRun);
			}
		}
	}

	void setCellRendererInResultTable() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				final int idx_result = getResultTableModel().findColumnByFieldUid(E.IMPORTFILE.result.getUID());
				if (idx_result >= 0) {
					final TableColumn column = getResultTable().getColumnModel().getColumn(idx_result);
					column.setCellRenderer(new ResultMessageCellRenderer());
				}
				final int idx_laststate = getResultTableModel().findColumnByFieldUid(E.IMPORTFILE.laststate.getUID());
				if (idx_laststate >= 0) {
					final TableColumn column = getResultTable().getColumnModel().getColumn(idx_laststate);
					column.setCellRenderer(new TrafficLightCellRenderer());
				}
			}
		});
	}

} // class GenericObjectImportCollectController
