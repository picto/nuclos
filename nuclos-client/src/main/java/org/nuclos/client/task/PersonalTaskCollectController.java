//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.client.common.DetailsSubFormController;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.common.NuclosDropTargetListener;
import org.nuclos.client.common.NuclosDropTargetVisitor;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.client.common.security.SecurityDelegate;
import org.nuclos.client.entityobject.CollectableEntityObject;
import org.nuclos.client.genericobject.CollectableGenericObjectWithDependants;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.datatransfer.GenericObjectIdModuleProcess;
import org.nuclos.client.genericobject.datatransfer.TransferableGenericObjects;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.masterdata.CollectableMasterData;
import org.nuclos.client.masterdata.CollectableMasterDataWithDependants;
import org.nuclos.client.masterdata.MasterDataCollectController;
import org.nuclos.client.masterdata.MasterDataDelegate;
import org.nuclos.client.masterdata.datatransfer.MasterDataIdAndEntity;
import org.nuclos.client.ui.collect.CollectState;
import org.nuclos.client.ui.collect.CollectStateAdapter;
import org.nuclos.client.ui.collect.CollectStateEvent;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.ToolbarFunction;
import org.nuclos.client.ui.collect.subform.ToolbarFunctionState;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.dnd.TableRowCopyTransferHandler;
import org.nuclos.client.ui.dnd.TableRowCopyTransferHandler.RowTransferable;
import org.nuclos.client.ui.dnd.TransferableWrapper;
import org.nuclos.client.ui.table.TableCellRendererProvider;
import org.nuclos.common.E;
import org.nuclos.common.NuclosBusinessException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SF;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.masterdata.CollectableMasterDataEntity;
import org.nuclos.common2.InternalTimestamp;
import org.nuclos.common2.LangUtils;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.server.common.valueobject.TaskVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class PersonalTaskCollectController extends MasterDataCollectController<Long> {

	private static final Logger LOG = Logger.getLogger(PersonalTaskCollectController.class);

	private static class CustomTableCellRendererProvider implements TableCellRendererProvider {

		DetailsSubFormController<?,?> subcontroller;

		public CustomTableCellRendererProvider(DetailsSubFormController<?,?> subcontroller){
			this.subcontroller = subcontroller;
		}

		@Override
		public TableCellRenderer getTableCellRenderer(CollectableEntityField clctef) {
			return subcontroller.getSubForm().getTableCellRenderer(clctef);
		}
	}
	
	private JButton sSingletaskButton;

	/**
	 * You should use {@link org.nuclos.client.ui.collect.CollectControllerFactorySingleton} 
	 * to get an instance.
	 * 
	 * @deprecated You should normally do sth. like this:<pre><code>
	 * ResultController rc = new ResultController();
	 * *CollectController cc = new *CollectController(.., rc);
	 * </code></pre>
	 */
	public PersonalTaskCollectController(MainFrameTab tabIfAny) {
		super(E.TODOLIST.getUID(), tabIfAny, null);
//		setupDetailsToolBar();
		if(SecurityCache.getInstance().isSuperUser()){
			List<CollectableComponent> delegatorCollectableComponents = getDetailCollectableComponentsFor(E.TODOLIST.taskdelegator.getUID());
			for(CollectableComponent component : delegatorCollectableComponents){
				component.setEnabled(true);
			}
		}

		getSubFormController(E.TODOOBJECT.getUID()).getSubForm().getSubformTable().setTableCellRendererProvider(
				new CustomTableCellRendererProvider(getSubFormController(E.TODOOBJECT.getUID())));
		getSubFormController(E.TODOOBJECT.getUID()).getSubForm().setToolbarFunctionState(ToolbarFunction.REMOVE, ToolbarFunctionState.ACTIVE);
		getSubFormController(E.TASKOWNER.getUID()).getSubForm().getSubformTable().getModel().addTableModelListener( new TableModelListener() {
 			@Override
			public void tableChanged(TableModelEvent e) {
 				if(sSingletaskButton != null){
 					sSingletaskButton.setEnabled(getUserUIDs().size() > 1);
 				}
			}
        });
		this.addCollectableEventListener(Main.getInstance().getMainController().getTaskController().getPersonalTaskController());

		getCollectStateModel().addCollectStateListener(new CollectStateAdapter() {
			@Override
			public void detailsModeEntered(CollectStateEvent ev) throws CommonBusinessException {
				super.detailsModeEntered(ev);
				if (ev.getNewCollectState().isDetailsMode()) {
					PersonalTaskCollectController.this.setDetailsChangedIgnored(true);
					if (ev.getNewCollectState().getInnerState() == CollectState.DETAILSMODE_NEW) {
						for (DetailsSubFormController<Long,CollectableEntityObject<Long>> sf : getSubFormControllersInDetails()) {
							if (sf.getCollectableEntity().getUID().equals(E.TASKOWNER.getUID())) {
								CollectableMasterData<Long> md = sf.insertNewRow();
								String sUser = Main.getInstance().getMainController().getUserName();
								UID userUid = TaskDelegate.getInstance().getUserUid(sUser);
								md.setField(E.TASKOWNER.user.getUID(), new CollectableValueIdField(userUid, sUser));
							}
						}
					}
					PersonalTaskCollectController.this.setDetailsChangedIgnored(false);
				}
			}
		});
		
		final DetailsSubFormController subformCtrlTodoObject = getSubFormController(E.TODOOBJECT.getUID());
		final DropTarget dt = new DropTarget(subformCtrlTodoObject.getSubForm().getJTable(), new NuclosDropTargetListener(new NuclosDropTargetVisitor() {
			@Override
			public void visitDrop(DropTargetDropEvent dtde) { 
				if ((dtde.getSourceActions() & DnDConstants.ACTION_COPY_OR_MOVE) == 0)  {
					dtde.rejectDrop();
					return;
				}
				
				try {
					// NUCLOS-1477, NUCLOS-1478
					if (dtde.getCurrentDataFlavorsAsList().contains(TableRowCopyTransferHandler.RowTransferable.getDataFlavor())) {
						final TransferableWrapper<SubFormTable, RowTransferable> wrapper = 
								(TransferableWrapper<SubFormTable, RowTransferable>) dtde.getTransferable().getTransferData(TableRowCopyTransferHandler.RowTransferable.getDataFlavor());
						final int idxInsert = subformCtrlTodoObject.getSubForm().getJTable().rowAtPoint(dtde.getLocation());
						return;
					}
					dtde.acceptDrop(dtde.getDropAction());
					Transferable trans = dtde.getTransferable();

					// check for TransferableGenericObjects or MasterDataVO support
					List<?> lstloim = null;
					if (dtde.isDataFlavorSupported(TransferableGenericObjects.dataFlavor)) {
						lstloim = (List<?>) trans.getTransferData(TransferableGenericObjects.dataFlavor);
					} else if (dtde.isDataFlavorSupported(MasterDataIdAndEntity.dataFlavor)){
						lstloim = (List<?>) trans.getTransferData(MasterDataIdAndEntity.dataFlavor);
					}

					UID entityId = null;
					String entityLabel = null;

					for (Object o : lstloim) {
						Collectable<?> clct = null;
						if (o instanceof GenericObjectIdModuleProcess) {
							GenericObjectIdModuleProcess goimp = (GenericObjectIdModuleProcess) o;
		            		entityId = goimp.getModuleUid();
		            		
							try {
		            			clct = new CollectableGenericObjectWithDependants(
		            					GenericObjectDelegate.getInstance().getWithDependants(goimp.getGenericObjectId(), 
		            					ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_LAYOUT_CUSTOM_KEY)));
							}
							catch(Exception e) {
								LOG.error("visitDrop failed: " + e, e);
							}
						} else if (o instanceof MasterDataIdAndEntity) {
		            		MasterDataIdAndEntity<?> mdiae = (MasterDataIdAndEntity<?>) o;
		            		entityId = mdiae.getEntityUid();
							try {
		            			clct = new CollectableMasterData(new CollectableMasterDataEntity(
		            					MasterDataDelegate.getInstance().getMetaData(mdiae.getEntityUid())), 
		            					MasterDataDelegate.getInstance().get(mdiae.getEntityUid(), mdiae.getId()));
							}
							catch(CommonBusinessException e) {
								LOG.error("visitDrop failed: " + e, e);
							}
						}

						entityLabel = SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(MetaProvider.getInstance().getEntity(entityId));

						if (clct != null) {
							for (Object taskobject : subformCtrlTodoObject.getCollectables()) {
								if (LangUtils.equal(((Collectable)taskobject).getField(E.TODOOBJECT.entity.getUID()).getValueId(), entityId)
										&& LangUtils.equal(((Collectable)taskobject).getField(E.TODOOBJECT.objectId.getUID()).getValueId(), clct.getId()))
									continue;
							}
							
							try {
								Collectable newClct = subformCtrlTodoObject.newCollectable();
								newClct.setField(E.TODOOBJECT.entity.getUID(), new CollectableValueIdField(entityId, entityLabel));
								newClct.setField(E.TODOOBJECT.objectId.getUID(), new CollectableValueIdField(clct.getPrimaryKey(), clct.getPrimaryKey()));
								newClct.setField(SF.CREATEDAT.getUID(E.TODOOBJECT), new CollectableValueField(new InternalTimestamp(System.currentTimeMillis())));
								newClct.setField(SF.CREATEDBY.getUID(E.TODOOBJECT), new CollectableValueField(SecurityCache.getInstance().getUsername()));
								newClct.setField(SF.CHANGEDAT.getUID(E.TODOOBJECT), new CollectableValueField(new InternalTimestamp(System.currentTimeMillis())));
								newClct.setField(SF.CHANGEDBY.getUID(E.TODOOBJECT), new CollectableValueField(SecurityCache.getInstance().getUsername()));
								newClct.setField(SF.VERSION.getUID(E.TODOOBJECT), new CollectableValueField(new Integer(1)));
								
								final UID foreignKeyFieldUid = subformCtrlTodoObject.getForeignKeyFieldUID();
								if (!LangUtils.equal(newClct.getField(foreignKeyFieldUid).getValueId(), subformCtrlTodoObject.getParentId())) {
									newClct.setField(foreignKeyFieldUid, new CollectableValueIdField(subformCtrlTodoObject.getParentId(), null));
								}
								subformCtrlTodoObject.insertExistingRow(newClct);
							} catch (NuclosBusinessException e) {
									LOG.debug("visitDrop: " + e);
							}
						}
					}
				} catch (Exception e) {
					LOG.warn("visitDrop fails: " + e);
				}
			}
			@Override
			public void visitDragOver(DropTargetDragEvent dtde) {
				if (dtde.isDataFlavorSupported(TransferableGenericObjects.dataFlavor)
					|| dtde.isDataFlavorSupported(MasterDataIdAndEntity.dataFlavor)) {
					dtde.acceptDrag(dtde.getDropAction());
				} else {
					dtde.rejectDrag();
				}
			}
			@Override
			public void visitDropActionChanged(DropTargetDragEvent dtde) { }
			@Override
			public void visitDragExit(DropTargetEvent dte) { }
			@Override
			public void visitDragEnter(DropTargetDragEvent dtde) { }
		}));
		dt.setActive(true);
		
	}

	protected void setupDetailsToolBar() {
		super.setupDetailsToolBar();
		// final JToolBar toolbar = (JToolBar)this.getDetailsPanel().getCustomToolBarArea();
		final String sSingletaskButtonName = getSpringLocaleDelegate().getMessage("EditPersonalTaskDefinitionPanel.Button.Singletask",null);
		sSingletaskButton = new JButton(
			new AbstractAction(sSingletaskButtonName) {

				@Override
				public void actionPerformed(ActionEvent e) {
					CollectableMasterDataWithDependants<Long> selectedCollectable = PersonalTaskCollectController.this.getSelectedCollectable();
					MasterDataVO<Long> mdvo = selectedCollectable.getMasterDataWithDependantsCVO();
					try {
						boolean bSplitted = splitTaskForOwners(mdvo.getId() == null, mdvo, sSingletaskButtonName);
						if(bSplitted){
							broadcastCollectableEvent(selectedCollectable, MessageType.EDIT_DONE);
						}
						refreshCurrentCollectable();
					} catch(CommonBusinessException ex) {
						throw new NuclosFatalException(ex);
					}
				}
			}
		);
		// toolbar.add(sSingletaskButton, null, 0);
		//getDetailsPanel().setCustomToolBarArea(toolbar);

		getDetailsPanel().addToolBarComponent(sSingletaskButton);
	}

	@Override
	protected CollectableMasterDataWithDependants<Long> newCollectableWithDefaultValues(boolean forInsert) {
		CollectableMasterDataWithDependants<Long> newCollectableWithDefaultValues = super.newCollectableWithDefaultValues(forInsert);
		newCollectableWithDefaultValues.setField(CollectableTask.FIELDNAME_DELEGATOR, 
				new CollectableValueIdField(SecurityDelegate.getInstance().getUserUid(
						Main.getInstance().getMainController().getUserName()), ""));
		return newCollectableWithDefaultValues;
	}

	private boolean splitTaskForOwners(boolean createMode, MasterDataVO<Long> mdvo, final String sSingletaskButtonName)
		throws CommonBusinessException {
		boolean result = false;
		final String sMessage = getSpringLocaleDelegate().getMessage("EditPersonalTaskDefinitionPanel.Singletask.Secure", null);
		final String sSecureTitle = sSingletaskButtonName;
		int createSingleTasksSecure = JOptionPane.showConfirmDialog(this.getTab(), sMessage, sSecureTitle, JOptionPane.YES_NO_OPTION);
		switch(createSingleTasksSecure) {
			case JOptionPane.YES_OPTION:
				Collection<TaskVO> taskvos = null;
				if(createMode){
					taskvos = TaskDelegate.getInstance().create(mdvo, getUserUIDs(), true);
				} else {
					taskvos = TaskDelegate.getInstance().update(mdvo, getUserUIDs(), true);
				}
				result = (taskvos != null && !taskvos.isEmpty());
				break;
			case JOptionPane.NO_OPTION:
			default: 
		}
		return result;
	}

	public Set<UID> getUserUIDs() {
		// A potential problem here is that the result set could contain null as element.
		// Perhaps it would be better to further restrict the transformer? (Thomas Pasch)
		List<UID> userIds = CollectionUtils.transform(getSubFormController(E.TASKOWNER.getUID()).getCollectables(),
			new Transformer<CollectableEntityObject<Long>, UID>(){
				@Override
				public UID transform(CollectableEntityObject<Long> clmd) {
					return (UID) clmd.getField(E.TASKOWNER.user.getUID()).getValueId();
				}}
		);
		return new HashSet<UID>(userIds);
	}

}
