//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.task;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.entityobject.CollectableEntityObjectField;
import org.nuclos.client.genericobject.GenericObjectDelegate;
import org.nuclos.client.genericobject.Modules;
import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.client.ui.CommonJTextField;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.client.ui.collect.component.CollectableTextField;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.attribute.DynamicAttributeVO;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collection.Pair;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.genericobject.valueobject.GenericObjectVO;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public class CollectableTaskObjectTextField extends CollectableTextField {
	
	private static final Logger LOG = Logger.getLogger(CollectableTaskObjectTextField.class);

	public CollectableTaskObjectTextField(CollectableEntityField clctef) {
		super(clctef);
	}

	public CollectableTaskObjectTextField(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, bSearchable);
	}

	@Override
	public TableCellRenderer getTableCellRenderer(boolean subform) {
		final TableCellRenderer parentRenderer = super.getTableCellRenderer(subform);
		final CommonJTextField ntf = getJTextField();
		return new CollectableTextFieldCellRenderer(parentRenderer, ntf);
	}

		private static class CollectableTextFieldCellRenderer implements TableCellRenderer {
			
			private final TableCellRenderer parentRenderer;
			private final int horizontalAlignment;

			private static final Map<Pair<UID, Long>, String> mpMappings = new HashMap<Pair<UID, Long>, String>();
			
			private CollectableTextFieldCellRenderer(TableCellRenderer parentRenderer, CommonJTextField ntf) {
				this.parentRenderer = parentRenderer;
				this.horizontalAlignment = ntf.getHorizontalAlignment();
			}
			@Override
			public Component getTableCellRendererComponent(JTable tbl, Object oValue, boolean bSelected, boolean bHasFocus, int iRow, int iColumn) {
				final Component comp = parentRenderer.getTableCellRendererComponent(tbl, oValue, bSelected, bHasFocus, iRow, iColumn);
				if (comp instanceof JLabel) {
					final JLabel lb = (JLabel) comp;
					if (oValue instanceof CollectableEntityObjectField) {
						final CollectableEntityObjectField ceof = (CollectableEntityObjectField) oValue;
						if (ceof.getValue() instanceof Long) {
							if (tbl instanceof SubFormTable) {
								final SubFormTableModel mdl = ((SubFormTable)tbl).getSubFormModel();
								final UID entityUid = (UID) ((CollectableEntityObjectField)mdl.getValueAt(iRow, mdl.findColumnByFieldUid(E.TODOOBJECT.entity.getUID()))).getValue();
								
								final Pair<UID, Long> key = new Pair<UID, Long>(entityUid, (Long) ceof.getValue());
								if (!mpMappings.containsKey(key)) {
									try {
										if (Modules.getInstance().isModule(entityUid)) {
											final GenericObjectVO genVO = GenericObjectDelegate.getInstance().get((Long) ceof.getValue());
											
											// extract values
											final Map<UID, Object> values = new HashMap<UID, Object>();
											for (DynamicAttributeVO att : genVO.getAttributes()) {
												values.put(att.getAttributeUID(), att.getValue());
											}											
											SpringLocaleDelegate.getInstance().getTreeViewLabel(values, genVO.getModule(), MetaProvider.getInstance(), null);
											
										} else {
											final MasterDataVO<?> mdVO = MasterDataCache.getInstance().get(entityUid, (Long) ceof.getValue());
											mpMappings.put(key, SpringLocaleDelegate.getInstance().getTreeViewLabel(mdVO.getFieldValues(), mdVO.getEntityObject().getDalEntity(), MetaProvider.getInstance(), null));
										}
									} catch (Exception e) {
										LOG.warn("getTableCellRendererComponent failed: " + e, e);
										//ignore.
									}
								}
								lb.setText(mpMappings.get(key));
							}
						}
					}
					lb.setHorizontalAlignment(horizontalAlignment);
				}
				return comp;
			}
		}
}

