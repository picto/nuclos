package org.nuclos.client.task;

import javax.swing.*;

public interface ScheduledRefreshable {

	void setRefreshInterval(int sec);
	
	int getRefreshInterval();
	
	int[] getRefreshIntervals();
	
	String[] getRefreshIntervalLabels();
	
	JRadioButtonMenuItem[] getRefreshIntervalRadioButtons();
	
	JPanel getFilterPanel();
}
