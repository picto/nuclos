package org.nuclos.client.task;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;
import org.nuclos.client.command.CommonClientWorkerSelfExecutable;
import org.nuclos.client.main.mainframe.MainFrame;
import org.nuclos.client.main.mainframe.MainFrameTabbedPane;
import org.nuclos.client.ui.Controller;
import org.nuclos.client.ui.UIUtils;
import org.nuclos.client.ui.layer.LayerLock;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonBusinessException;

public abstract class RefreshableTaskController extends Controller<MainFrameTabbedPane> {

    private static final Logger LOG = Logger.getLogger(RefreshableTaskController.class);

    protected ScheduledExecutorService scheduler = null;
    protected Map<ScheduledRefreshable, ScheduledFuture<?>> refreshandles = null;

    public RefreshableTaskController() {
        super(null);
        refreshandles = new HashMap<ScheduledRefreshable, ScheduledFuture<?>>();
    }

    @Override
	public MainFrameTabbedPane getParent() {
		return MainFrame.getHomePane(); // TODO PersonalTask Home in future here !!!
	}

    public MainFrameTabbedPane getTabbedPane() {
		return getParent();
	}

	public abstract ScheduledRefreshable getSingleScheduledRefreshableView();

    public abstract void refreshScheduled(ScheduledRefreshable sRefreshable);

    public int getScheduledThreadPoolSize(){
    	return 1;
    }
    
    public void addRefreshIntervalActionsToSingleScheduledRefreshable() {
    		addRefreshIntervalActions(getSingleScheduledRefreshableView());
    }
    
    public void addRefreshIntervalActions(final ScheduledRefreshable sr) {
    		final int refreshInterval = sr.getRefreshInterval();
    		for (int i = 0; i < sr.getRefreshIntervals().length; i++) {
    			final int min = sr.getRefreshIntervals()[i];
    			sr.getRefreshIntervalRadioButtons()[i].setAction(new AbstractAction(sr.getRefreshIntervalLabels()[i]) {

					@Override
					public void actionPerformed(ActionEvent e) {
						setRefreshIntervalForMultiViewRefreshable(sr, min);
					}
				});
    			if ((i == 0 && refreshInterval <= 0) || 
    				(i >= 1 && refreshInterval == sr.getRefreshIntervals()[i])) {
    				sr.getRefreshIntervalRadioButtons()[i].setSelected(true);
    			}
    		}
    }
    
    public void setRefreshIntervalForSingleViewRefreshable(int min){
    	setRefreshIntervalForMultiViewRefreshable(getSingleScheduledRefreshableView(), min);
    }

    public void setRefreshIntervalForMultiViewRefreshable(ScheduledRefreshable sRefreshable, int min){
        if(sRefreshable == null) { throw new NuclosFatalException("ScheduledRefreshable is null"); }
        if(min >= 0){
	        sRefreshable.setRefreshInterval(min);
	        if(this.scheduler == null){
	            this.scheduler = Executors.newScheduledThreadPool(getScheduledThreadPoolSize());
	        }
        }
        scheduleRefreshRunner(sRefreshable);
    }
    
    private void scheduleRefreshRunner(ScheduledRefreshable sRefreshable){
        if(sRefreshable == null) { throw new NuclosFatalException("ScheduledRefreshable is null"); }
    	ScheduledFuture<?> old_Refreshandle = this.refreshandles.get(sRefreshable);
		if(old_Refreshandle != null){
    		old_Refreshandle.cancel(false);
    	}        
        int refreshInterval = sRefreshable.getRefreshInterval();
        if(refreshInterval > 0){
			this.refreshandles.put(sRefreshable, scheduler.scheduleWithFixedDelay(new RefreshViewRunnable(sRefreshable),
	            refreshInterval, refreshInterval, TimeUnit.MINUTES));
        } else {
        	this.refreshandles.remove(sRefreshable);
        	if(refreshandles.isEmpty() && this.scheduler != null){
        		this.scheduler.shutdown();
        		this.scheduler = null;
        	}
        }
    }

    private class RefreshViewRunnable implements Runnable {
    	
    	ScheduledRefreshable sRefreshable;
    	
    	public RefreshViewRunnable(ScheduledRefreshable isRefreshable){
    		sRefreshable = isRefreshable;
    	}
    	
        @Override
		public void run() {
            refreshScheduled(sRefreshable);
        }
    }	// inner class RefreshRunnable

	protected static abstract class CommonClientWorkerTaskControllerAdapter implements CommonClientWorkerSelfExecutable {
		private Future<LayerLock> lock = null;
		RefreshableTaskController taskCtrl = null;
		UID taskControllerUID;

		public CommonClientWorkerTaskControllerAdapter(RefreshableTaskController taskCtrl, UID taskControllerUID) {
			this.taskCtrl = taskCtrl;
			this.taskControllerUID = taskControllerUID;
		}

		protected abstract TaskView getTaskView(UID taskControllerUID);

		protected abstract List<Future<LayerLock>> getAllLocksList();

		@Override
		public void runInCallerThread() throws CommonBusinessException {
			init();
			work();

			final PaintRunner pr = new PaintRunner();
			SwingUtilities.invokeLater(pr);

			if (pr.occurredBusinessException != null) {
				throw pr.occurredBusinessException;
			}
		}

		@Override
		public void init() throws CommonBusinessException {
			UIUtils.setWaitCursor();
			final Future<LayerLock> lock;
			lock = UIUtils.lockFrame(getTaskView(taskControllerUID).getTable());
			if (lock != null) {
				getAllLocksList().add(lock);
			}
			this.lock = lock;
		}

		@Override
		public abstract void work() throws CommonBusinessException;

		@Override
		public void paint() throws CommonBusinessException {
			unlock(lock);
			UIUtils.setDefaultCursor();
		}

		@Override
		public void handleError(Exception ex) {
			unlock(lock);
			LOG.error("handleError: " + ex, ex);
		}

		@Override
		public JComponent getResultsComponent() {
			return getTaskView(taskControllerUID).getTable();
		}

		class PaintRunner implements Runnable {

			private CommonBusinessException occurredBusinessException;

			@Override
			public void run() {
				try {
					paint();
				}
				catch(CommonBusinessException e) {
					LOG.warn("PaintRunner.run failed: " + e, e);
					occurredBusinessException = e;
				}
				catch(Exception e) {
					LOG.error("PaintRunner.run failed: " + e, e);
				}
			}

		}

		private void unlock(Future<LayerLock> lock) {
			UIUtils.unLockFrame(getTaskView(taskControllerUID).getTable(), lock);
			getAllLocksList().remove(lock);
			this.lock = null;
		}
	}

}
