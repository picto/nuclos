//Copyright (C) 2011  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.remote.http;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.client.i18n.language.data.DataLanguageContext;
import org.nuclos.client.main.Main;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.mandator.ClientMandatorContext;
import org.nuclos.client.remote.NuclosHttpInvokerAttributeContext;
import org.nuclos.common.ApplicationProperties;
import org.nuclos.common.NuclosConstants;
import org.nuclos.common.NuclosRemoteInvocationResult;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.remoting.support.RemoteInvocation;
import org.springframework.remoting.support.RemoteInvocationResult;

/**
 * An extension to Spring HttpInvokerProxyFactoryBean used for remote calls from 
 * nuclos client to server.
 */
public class NuclosHttpInvokerProxyFactoryBean extends HttpInvokerProxyFactoryBean {

	private static final Logger LOG = Logger.getLogger(NuclosHttpInvokerProxyFactoryBean.class);
	
	/**
	 * Whether to turn INFO logging of profiling data on.
	 * It should be true only in production environments.
	 */
	private static final boolean PROFILE = false;
	
	/**
	 * Minimum delay for calls in order to appear in LOG. 
	 */
	private static final long PROFILE_MIN_MS = 300L;
	
	//
	
	private NuclosHttpInvokerAttributeContext ctx;
	
	public NuclosHttpInvokerProxyFactoryBean() {
	}
	
	// @Autowired
	public void setNuclosHttpInvokerAttributeContext(NuclosHttpInvokerAttributeContext ctx) {
		this.ctx = ctx;
	}

	@Override
	protected RemoteInvocationResult executeRequest(RemoteInvocation invocation) throws Exception {
		if (ClientMandatorContext.getMandatorUID() != null) {
			invocation.addAttribute(NuclosConstants.MANDATOR, ClientMandatorContext.getMandatorUID().getString());
		}
		if (DataLanguageContext.getDataUserLanguage() != null) {
			invocation.addAttribute(NuclosConstants.DATA_LOCALE, DataLanguageContext.getDataUserLanguage().toString());			
		}
		
		invocation.addAttribute(NuclosConstants.USER_TIMEZONE, Main.getInitialTimeZone());
		invocation.addAttribute(NuclosConstants.INPUT_CONTEXT_SUPPORTED, ctx.isSupported());
		final HashMap<String, Serializable> map = ctx.get();
		invocation.addAttribute(NuclosConstants.INPUT_CONTEXT, map);
		if (LOG.isDebugEnabled() && map.size() > 0) {
			LOG.debug("Sending call with dynamic context:");
			for (Map.Entry<String, Serializable> entry : map.entrySet()) {
				LOG.debug(entry.getKey() + ": " + String.valueOf(entry.getValue()));
			}
		}
		invocation.addAttribute(NuclosConstants.MESSAGE_RECEIVER_CONTEXT, ctx.getMessageReceiver());
		MainFrameTab target = null;
		if (ctx.getMessageReceiver() != null) {
			target = MainFrameTab.getMainFrameTab((Integer) ctx.getMessageReceiver());
			if (target != null) target.acceptLockMessages(true);
		}
		if(ApplicationProperties.getInstance().isFunctionBlockDev()) {
			invocation.addAttribute(NuclosConstants.CLIENT_STACK_TRACE, Thread.currentThread().getStackTrace());
		}
		
		final long before;
		if (PROFILE) {
			before = System.currentTimeMillis();
		}
		else {
			before = 0L;
		}
		
		try {
			final RemoteInvocationResult result = super.executeRequest(invocation);
			if(result instanceof NuclosRemoteInvocationResult) {
				NuclosRemoteInvocationResult nuclosResult = (NuclosRemoteInvocationResult) result;
				// space for future use of this feature
			}			
			return result;
		} catch (OutOfMemoryError e) {
			Throwable thr = e;
			while (thr.getCause() != null) {
				thr = thr.getCause();
			}
			LOG.error("remote invocation of " + invocation + " failed: " + e, thr);
			throw e;
		} catch (Exception e) {
			throw e;
		} finally {
			if (PROFILE) {
				final long call = System.currentTimeMillis() - before;
				if (call >= PROFILE_MIN_MS) {
					LOG.info("remote invocation of " + invocation + " on " + getServiceInterface() + " took " + call + " ms");
				}
			}
			
			if (target != null) {
				target.acceptLockMessages(false);
			}
		}
	}

}
