package org.nuclos.client.remote;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ClientParameterProvider;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.http.NuclosHttpClientFactory;
import org.nuclos.common.http.NuclosHttpClientFactory.ControlledHttpParams;
import org.nuclos.common2.StringUtils;

public class NoConnectionTimeoutRunner {
	
	private final static Logger LOG = Logger.getLogger(NoConnectionTimeoutRunner.class);
	
	public final static Integer DEFAULT_TIMEOUT = 1800;

	public static interface NoConnectionTimeoutRunnable<T> {
		public T run() throws Exception;
	}
	
	public static synchronized <T> T runSynchronized(NoConnectionTimeoutRunnable<T> runnable) throws Exception {
		String sParamTimeout = ClientParameterProvider.getInstance().getValue(ParameterProvider.KEY_CLIENT_READ_TIMEOUT);
		if ("0".equals(sParamTimeout)) {
			return runnable.run();
		} else {
			setTimeout("0");
			try {
				return runnable.run(); 
			} finally {
				setTimeout(StringUtils.defaultIfNull(sParamTimeout, DEFAULT_TIMEOUT.toString()));
			}
		}
	}
	
	private static synchronized void setTimeout(String sTimeout) {
		LOG.info("Set client read timeout to " + sTimeout + "...");
		int iReadTimeout = Integer.valueOf(sTimeout);
		NuclosHttpClientFactory clientFactory = SpringApplicationContextHolder.getBean(NuclosHttpClientFactory.class);
		ControlledHttpParams httpParams = (ControlledHttpParams) clientFactory.getObject().getParams();
		httpParams.setClientReadTimeout(iReadTimeout * 1000);
	}
	
}
