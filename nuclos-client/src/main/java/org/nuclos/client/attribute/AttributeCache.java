//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.attribute;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.startup.AbstractLocalUserCache;
import org.nuclos.common.AttributeProvider;
import org.nuclos.common.CacheableListener;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosAttributeNotFoundException;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.server.attribute.valueobject.AttributeCVO;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Client cache for all attributes (Singleton pattern).
 * The cache must be initialized before its first use.
 * <p>
 * §todo It is possible to change the name of an AttributeCVO outside the cache and thus to make the
 * cache inconsistent. Fix is deferred until the attribute administration is implemented by using
 * the masterdata mechanism.
 * <p>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
@ManagedResource(
		objectName="nuclos.client.cache:name=AttributeCache",
		description="Nuclos Client AttributeCache",
		currencyTimeLimit=15,
		log=false)
public class AttributeCache extends AbstractLocalUserCache implements AttributeProvider {
	
	private static final Logger LOG = Logger.getLogger(AttributeCache.class);
	
	/**
	 * the one (and only) instance of AttributeCache
	 */
	private static AttributeCache INSTANCE;
	
	private transient AttributeDelegate attributeDelegate;
	
	//

	/** 
	 * §todo change this to an unmodifiable collection 
	 */
    private final Map<UID, AttributeCVO> mpAttributesByIds = new ConcurrentHashMap<UID, AttributeCVO>();

	private transient final LinkedList<CacheableListener> lstCacheableListeners = new LinkedList<CacheableListener>();
	
	/**
	 * @return the one (and only) instance of AttributeCache
	 */
	public static AttributeCache getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("too early");
		}
		return INSTANCE;
	}

	/**
	 * creates the cache. Fills in all the attributes from the server.
	 * @throws NuclosFatalException
	 * 
	 * @deprecated Must be protected for Spring JMX - but never invoke this constructor.
	 */
	protected AttributeCache() {
		INSTANCE = this;
	}
	
	public final void setAttributeDelegate(AttributeDelegate attributeDelegate) {
		this.attributeDelegate = attributeDelegate;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		// Constructor might not be called - as this instance might be deserialized (tp)
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		// we can not do this here. attribute cache fills to early.
		/*if (!wasDeserialized())
			fill();
		*/
		mpAttributesByIds.clear();
	}
	
	@Override
	public String getCachingTopic() {
		return JMSConstants.TOPICNAME_METADATACACHE; // @todo. is this right?
	}

	/**
	 * revalidates this cache: clears it, then fills in all the attributes 
	 * from the server again.
	 * <p>
	 * Must be synchronized to avoid attributecache.uniquekey.id.error in 
	 * {@link #addImpl(AttributeCVO)}.
	 * </p>
	 */
	@ManagedOperation(description="invalidate/clear cache and fill it again")
	public synchronized void revalidate() {
		mpAttributesByIds.clear();
		LOG.info("Cleared cache " + this);
		fill();
	}

	/**
	 * §postcondition result != null
	 * @param iAttributeId
	 */
	@Override
    public AttributeCVO getAttribute(UID iAttributeId) {
		if (mpAttributesByIds.isEmpty()) {
			fill();
		}
		final AttributeCVO result = mpAttributesByIds.get(iAttributeId);
		if (result == null) {
			throw new NuclosAttributeNotFoundException(iAttributeId);
		}

		assert result != null;
		return result;
	}

	/**
	 * @param iAttributeId
	 * @return Does this cache contain an attribute with the given id?
	 */
	public boolean contains(UID iAttributeId) {
		return mpAttributesByIds.containsKey(iAttributeId);
	}

	/**
	 * @deprecated Use MetaProvider.getInstance().getEntityField(entity, field)
	 */
	@Override
	public FieldMeta<?> getEntityField(UID field) throws NuclosAttributeNotFoundException {
		return MetaProvider.getInstance().getEntityField(field);
	}

	/**
	 * §precondition attrcvo != null
	 * adds a single attribute to this cache and notifies CacheableListeners.
	 * @param attrcvo
	 */
	public void add(AttributeCVO attrcvo) {
		addImpl(attrcvo);
		fireCacheableChanged();
	}

	/**
	 * adds a single attribute to this cache.
	 * <p>
	 * Must be synchronized to avoid attributecache.uniquekey.id.error in 
	 * {@link #fill()}.
	 * </p>
	 * §precondition attrcvo != null
	 * @param attrcvo
	 */
	private synchronized void addImpl(AttributeCVO attrcvo) {
		if (attrcvo == null) {
			throw new NullArgumentException("attrcvo");
		}
		if (mpAttributesByIds.containsKey(attrcvo.getId())) {
			//"Ein Attribut mit dieser Id ist schon im Cache vorhanden.");
			//throw new NuclosFatalException("attributecache.uniquekey.id.error");
		} else {
			mpAttributesByIds.put(attrcvo.getId(), attrcvo);
		}
		// old entry (attribute which has the same id) is overwritten
	}

	/**
	 * removes the attribute with the given id from the cache and notifies CacheableListeners.
	 * Does nothing if the cache doesn't contain an attribute with the given id.
	 * Note that the name of the given attribute is ignored. Only the id is relevant.
	 * §postcondition !this.contains(iAttributeId)
	 * @param iAttributeId
	 */
	public void remove(UID iAttributeId) {
		removeImpl(iAttributeId);
		fireCacheableChanged();
		assert !this.contains(iAttributeId);
	}

	/**
	 * removes the attribute with the given id from the cache.
	 * Does nothing if the cache doesn't contain an attribute with the given id.
	 * Note that the name of the given attribute is ignored. Only the id is relevant.
	 * §postcondition !this.contains(iAttributeId)
	 * @param iAttributeId
	 */
	private void removeImpl(UID iAttributeId) {
		if (iAttributeId == null) {
			throw new NullArgumentException("iAttributeId");
		}
		mpAttributesByIds.remove(iAttributeId);
		// postcondition:
		assert !this.contains(iAttributeId);
	}

	/**
	 * @return Collection&lt;AttributeCVO&gt; a collection containing all attributes in the cache.
	 */
	@Override
    public Collection<AttributeCVO> getAttributes() {
		return mpAttributesByIds.values();
	}

	/**
	 * fills this cache.
	 * <p>
	 * Must be synchronized to avoid attributecache.uniquekey.id.error in 
	 * {@link #addImpl(AttributeCVO)}.
	 * </p>
	 * @throws NuclosFatalException
	 */
	public synchronized void fill() throws NuclosFatalException {
		for (AttributeCVO attrcvo : attributeDelegate.getAllAttributeCVOs(null)) {
			addImpl(attrcvo);
		}
		LOG.info("Validated (filled) cache " + this);
	}

	public void addCacheableListener(CacheableListener cacheablelistener) {
		synchronized(lstCacheableListeners) {
			lstCacheableListeners.add(cacheablelistener);
		}
	}

	public void removeCacheableListener(CacheableListener cacheablelistener) {
		synchronized (lstCacheableListeners) {
			lstCacheableListeners.remove(cacheablelistener);
		}
	}

	private void fireCacheableChanged() {
		// defensive copy
		final LinkedList<CacheableListener> clone;
		synchronized (lstCacheableListeners) {
			clone = (LinkedList<CacheableListener>) lstCacheableListeners.clone();
		}
		/** @todo Is this still necessary? Won't the GenericObjectMetaDataCache always fireCachableChanged() anyway? */
		for (CacheableListener listener : clone) {
			listener.cacheableChanged();
		}
	}
	
	@ManagedAttribute(description="get the size (number of entries/entities) of mpAttributesByIds")
	public int getNumberOfEntitiesInCache() {
		return mpAttributesByIds.size();
	}

}	// class AttributeCache
