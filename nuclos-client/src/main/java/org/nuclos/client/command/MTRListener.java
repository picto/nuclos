package org.nuclos.client.command;

public interface MTRListener {
	
	void error(Exception ex);
	
	void done();
}

