package org.nuclos.client.login;

import java.awt.*;

import javax.swing.*;

import org.nuclos.client.LocalUserProperties;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class SSLCheckLabel extends JLabel {
	enum STATE {
		NONE(null, LocalUserProperties.KEY_LAB_SSLCHECK_NONE, Color.GRAY),
		INPROGRESS("\u231B", LocalUserProperties.KEY_LAB_SSLCHECK_INPROGRESS, Color.BLACK),
		TRUSTED("\u2713", LocalUserProperties.KEY_LAB_SSLCHECK_TRUSTED, new Color(0, 160, 0)),
		UNTRUSTED("\u26A0", LocalUserProperties.KEY_LAB_SSLCHECK_UNTRUSTED, new Color(230, 150, 0)),
		INVALID("\u26A0", LocalUserProperties.KEY_LAB_SSLCHECK_INVALID, new Color(190, 0, 0)),
		INSECURE("\u26A0", LocalUserProperties.KEY_LAB_SSLCHECK_INSECURE, new Color(230, 150, 0));

		final String prefix;
		final String resourceKey;
		final Color color;

		STATE(final String prefix, final String resourceKey, final Color color) {
			this.prefix = prefix;
			this.resourceKey = resourceKey;
			this.color = color;
		}

		public String getPrefix() {
			return prefix == null ? "" : prefix + " ";
		}

		public String getLabel() {
			return LocalUserProperties.getInstance().getLoginResource(resourceKey);
		}

		public Color getColor() {
			return color;
		}
	}

	public SSLCheckLabel() {
		setState(STATE.NONE);
		setHorizontalAlignment(CENTER);
	}

	public void setState(STATE state) {
		setText(state.getPrefix() + state.getLabel());
		setForeground(state.getColor());
	}
}
