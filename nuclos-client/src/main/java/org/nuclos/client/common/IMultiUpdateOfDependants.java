package org.nuclos.client.common;

import org.nuclos.client.ui.collect.subform.SubForm;
import org.nuclos.client.ui.table.TableCellRendererProvider;

public interface IMultiUpdateOfDependants<PK> extends TableCellRendererProvider {

	void transfer(SubForm subForm, DetailsSubFormController<PK,?> dsfCtl);
	
	boolean isTransferPossible(SubForm subForm);
	
	void close(SubFormController sfCtl);

}
