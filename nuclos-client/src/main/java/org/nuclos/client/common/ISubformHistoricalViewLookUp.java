package org.nuclos.client.common;

import org.nuclos.common.UID;

public interface ISubformHistoricalViewLookUp {
	
	public boolean inHistoricalView(UID subformEntityUID, UID subformFieldUID, UID foreignKeyFieldToParent, int row);
	
}
