//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.common;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.swing.SwingUtilities;

import org.nuclos.common.UID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TimedPreferencesUpdater {

	private static final Logger LOG = LoggerFactory.getLogger(TimedPreferencesUpdater.class);

	private static final ConcurrentMap<PreferencesStoreWorker, Long> workers = new ConcurrentHashMap<>();

	private static final Thread t = new Thread("TimedPreferencesUpdater") {
		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					LOG.error(e.getMessage(), e);
					break;
				}
				try {
					final Iterator<PreferencesStoreWorker> itWorkers = workers.keySet().iterator();
					while (itWorkers.hasNext()) {
						final PreferencesStoreWorker worker = itWorkers.next();
						if (workers.get(worker) + 1000 < System.currentTimeMillis()) {
							// start worker and remove
							workers.remove(worker);
							SwingUtilities.invokeLater(new Runnable() {
								@Override
								public void run() {
									try {
									worker.storePreferences();
									} catch (Exception ex) {
										LOG.error(ex.getMessage(), ex);
									}
								}
							});
						}
					}
				} catch (Exception ex) {
					LOG.error(ex.getMessage(), ex);
				}
			}
		}
	};

	static {
		t.start();
	}

	public static abstract class PreferencesStoreWorker {

		private final UID uid;

		protected PreferencesStoreWorker(final UID uid) {
			this.uid = uid;
		}

		public abstract void storePreferences();

		@Override
		public boolean equals(final Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			final PreferencesStoreWorker that = (PreferencesStoreWorker) o;

			return uid != null ? uid.equals(that.uid) : that.uid == null;
		}

		@Override
		public int hashCode() {
			return uid != null ? uid.hashCode() : 0;
		}

		@Override
		public String toString() {
			return "PreferencesStoreWorker{" +
					"uid=" + uid +
					'}';
		}

	}

	protected void registerWorker(PreferencesStoreWorker worker) {
		workers.put(worker, System.currentTimeMillis());
	}

}
