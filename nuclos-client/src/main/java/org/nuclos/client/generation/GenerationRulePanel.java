package org.nuclos.client.generation;

import java.awt.*;

import javax.swing.*;

import org.apache.log4j.Logger;
import org.nuclos.api.rule.GenerateFinalRule;
import org.nuclos.api.rule.GenerateRule;
import org.nuclos.client.rule.server.panel.EventSupportSelectionDataChangeListener;
import org.nuclos.client.rule.server.panel.EventSupportSelectionExceptionListener;
import org.nuclos.client.rule.server.panel.EventSupportSelectionPanel;
import org.nuclos.client.rule.server.panel.EventSupportSelectionTableModel;
import org.nuclos.client.rule.server.panel.EventSupportTypeConverter;
import org.nuclos.client.ui.Errors;
import org.nuclos.server.eventsupport.valueobject.EventSupportGenerationVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;

public class GenerationRulePanel extends JPanel {

	private EventSupportSelectionPanel<EventSupportGenerationVO> pnlGenerations;
	private EventSupportSelectionPanel<EventSupportGenerationVO> pnlGenerationsFinal;

	private GenerationCollectController controller;

	private static final Logger LOG = Logger.getLogger(GenerationRulePanel.class);

	
	public GenerationRulePanel(GenerationCollectController controller) {
		super(new BorderLayout());
		
		this.controller = controller;
		
		// Rules
		this.add(createRulesPanel(), BorderLayout.CENTER);
	}

	private JComponent createRulesPanel() {
		
		// Exception Listener
		EventSupportSelectionExceptionListener listener = new 
				EventSupportSelectionExceptionListener() {
					@Override
					public void fireException(Exception firedException) {
						Errors.getInstance().showExceptionDialog(
								GenerationRulePanel.this, firedException.getMessage(), firedException);
					}
		};
		
		// StateChangeRules
		pnlGenerations = new EventSupportSelectionPanel<EventSupportGenerationVO>(controller, "Regeln vor dem Objektgenerator");
		pnlGenerations.addEventSupportSelectionExceptionListener(listener);
		pnlGenerations.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportGenerationVO>() {
			
			@Override
			public EventSupportGenerationVO convertData(EventSupportSourceVO esVO) {
				Integer order= GenerationRulePanel.this.pnlGenerations.getModel().getRowCount() + 1;
				return new EventSupportGenerationVO(order, 
						GenerationRulePanel.this.controller.getSelectedCollectableId(),
						esVO.getClassname(), GenerateRule.class.getCanonicalName());
			}

			@Override
			public String getRuleTypeAsString() {
				return GenerateRule.class.getCanonicalName();
			}
		});
		
		// StateChangeFinalRules		
		pnlGenerationsFinal = new EventSupportSelectionPanel<EventSupportGenerationVO>(
				controller, "Regeln nach dem Objektgenerator");
		pnlGenerationsFinal.addEventSupportSelectionExceptionListener(listener);
		
		pnlGenerationsFinal.setEventSupportTypeConverter(new EventSupportTypeConverter<EventSupportGenerationVO>() {
			
			@Override
			public String getRuleTypeAsString() {
				return GenerateFinalRule.class.getCanonicalName();
			}
			
			@Override
			public EventSupportGenerationVO convertData(EventSupportSourceVO esVO) {
				Integer order= GenerationRulePanel.this.pnlGenerationsFinal.getModel().getRowCount() + 1;
				return new EventSupportGenerationVO(order, 
						GenerationRulePanel.this.controller.getSelectedCollectableId(),
						esVO.getClassname(), GenerateFinalRule.class.getCanonicalName());
			}
		});
		
	
		JPanel p = new JPanel(new GridLayout(2,1));
		p.add(pnlGenerations);
		p.add(pnlGenerationsFinal);
		return p;
	}

	public EventSupportSelectionTableModel<EventSupportGenerationVO> getGenerationRuleModel() {
		return this.pnlGenerations.getModel();
	}
	public EventSupportSelectionTableModel<EventSupportGenerationVO> getGenerationFinalRuleModel() {
		return this.pnlGenerationsFinal.getModel();
	}
	
	public void addEventSupportSelectionDataChangeListener(EventSupportSelectionDataChangeListener listener) {
		// Listener for tables
		pnlGenerations.addEventSupportDataChangeListener(listener);
		pnlGenerationsFinal.addEventSupportDataChangeListener(listener);
	}
	
	public void setButtonWritable(boolean isWriteAllowed) {
		this.pnlGenerations.setButtonWritable(isWriteAllowed);
		this.pnlGenerationsFinal.setButtonWritable(isWriteAllowed);
	}
}
