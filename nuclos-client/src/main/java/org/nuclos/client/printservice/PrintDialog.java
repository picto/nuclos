//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.EventListenerList;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.TreePath;

import org.nuclos.api.report.OutputFormat;
import org.nuclos.client.main.mainframe.MainFrameTab;
import org.nuclos.client.printservice.ui.ExtendedAbstractTreeTable;
import org.nuclos.client.printservice.ui.IPrintoutCellEditorHelper;
import org.nuclos.client.printservice.ui.NuclosOutputFormatSelectionComponent;
import org.nuclos.client.printservice.ui.OutputFormatSelectionComponent;
import org.nuclos.client.printservice.ui.OutputFormatTreeNode;
import org.nuclos.client.report.PrintEvents;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.report.valueobject.PrintServiceTO;
import org.nuclos.common.report.valueobject.PrintServiceTO.Tray;
import org.nuclos.common2.SpringLocaleDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PrintDialog} display {@link OutputFormat} selection
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class PrintDialog extends JPanel implements PropertyChangeListener {
	private final static Logger LOG = LoggerFactory.getLogger(PrintDialog.class);
	
	private static final long serialVersionUID = 1L;
	
	private final WeakReference<MainFrameTab> parent;
	private final WeakReference<IPrintoutCellEditorHelper> cellEditorHelper;
	private final int preferredWidth;
	
	private final SpringLocaleDelegate localeDelegate; 
	private EventListenerList listener;

	private NuclosOutputFormatSelectionComponent outputSelectionComponent;
	private JCheckBox attachDocuments;
	private JButton btnCollapseTree;

	private final List<PrintServiceTO> printservices;
	private final List<PrintServiceTO> printservicesClient;

	private PrintServiceCellEditor printServiceCellEditor;
	private PrintServiceCellRenderer printServiceCellRenderer;
	
	private TrayCellEditor trayCellEditor;
	private TrayCellRenderer trayCellRenderer;
	
	private static class TrayCellEditor extends DefaultCellEditor {

		private final DefaultComboBoxModel cmbxModel;
		private final WeakReference<IPrintoutCellEditorHelper> cellEditorHelper;
		
		public TrayCellEditor(final DefaultComboBoxModel cmbxModel, final WeakReference<IPrintoutCellEditorHelper> cellEditorHelper) {
			super(createCmbbox(cmbxModel));
			this.cmbxModel = cmbxModel;
			this.cellEditorHelper = cellEditorHelper;
		}
		
		private static JComboBox createCmbbox(DefaultComboBoxModel cmbxModel) {
			JComboBox result = new JComboBox(cmbxModel);
			final ListCellRenderer defaultRenderer = result.getRenderer();
			result.setRenderer(new ListCellRenderer() {
				@Override
				public Component getListCellRendererComponent(JList list, Object value,
						int index, boolean isSelected, boolean cellHasFocus) {
					JLabel lbl = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
					if (value != null) {
						lbl.setText(((PrintServiceTO.Tray) value).getPresentation());
					} else {
						lbl.setText("   ");
					}
					return lbl;
				}
			});
			return result;
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			
			cmbxModel.removeAllElements();
			cmbxModel.addElement(null);
			
			IPrintoutCellEditorHelper helper = cellEditorHelper.get();
			if (helper != null) {
				PrintServiceTO printService = helper.printServiceAtRow(row);
				if (printService != null) {
					for (final PrintServiceTO.Tray tray : printService.getTrays()) {
						cmbxModel.addElement(tray);
					}
				}
			}
			
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}
		
	}
	
	private static class TrayCellRenderer extends DefaultTableCellRenderer {


		public TrayCellRenderer() {
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			
			final JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			
			if(value != null) {
				PrintServiceTO.Tray tray = (Tray) value;
				label.setText(tray.getPresentation());
			}
		
			return label;
		}
		
	}
	
	private static class PrintServiceCellEditor extends DefaultCellEditor implements PropertyChangeListener {

		private final WeakReference<ExtendedAbstractTreeTable> weakTable;  
		
		private final DefaultComboBoxModel cmbxModelPrintServices;
		private final List<PrintServiceTO> printservices;
		private final List<PrintServiceTO> printservicesClient;
		
		public PrintServiceCellEditor(final ExtendedAbstractTreeTable treeTable, final List<PrintServiceTO> printservicesClient, final DefaultComboBoxModel cmbxModelPrintServices) {
			super(new JComboBox(cmbxModelPrintServices));
			this.weakTable = new WeakReference<ExtendedAbstractTreeTable>(treeTable);
			this.printservices = new ArrayList<PrintServiceTO>();
			this.printservicesClient = printservicesClient;
			this.cmbxModelPrintServices = cmbxModelPrintServices;
		}
		
		@Override
		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int column) {
			
			ExtendedAbstractTreeTable treeTable = weakTable.get();
			if (treeTable != null) {
				final TreePath pathForRow = treeTable.getPathForRow(row);
				if (pathForRow.getLastPathComponent() instanceof OutputFormatTreeNode) {
					OutputFormatTreeNode outputNode = (OutputFormatTreeNode) pathForRow.getLastPathComponent();
					switch (outputNode.getOutputFormat().getDestination()) {
					case DEFAULT_PRINTER_SERVER:
					case PRINTER_SERVER:
						cmbxModelPrintServices.removeAllElements();
						cmbxModelPrintServices.addElement(null);
						for (final PrintServiceTO printService : printservices) {
							cmbxModelPrintServices.addElement(printService);
						}
						break;
					case DEFAULT_PRINTER_CLIENT:
					case PRINTER_CLIENT:
						cmbxModelPrintServices.removeAllElements();
						cmbxModelPrintServices.addElement(null);
						for (final PrintServiceTO printService : printservicesClient) {
							cmbxModelPrintServices.addElement(printService);
						}
						break;
					default:
						return null;
					}
				}
			} 
			
			return super.getTableCellEditorComponent(table, value, isSelected, row, column);
		}
		
		@Override
		public void propertyChange(PropertyChangeEvent evt) {
			for (final PrintServiceTO printService : (List<PrintServiceTO>) evt.getNewValue()) {
				printservices.add(printService);
			}
		}
		
	}
	
	private static class PrintServiceCellRenderer extends DefaultTableCellRenderer {

		public PrintServiceCellRenderer() {
		}
		
		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			
			final JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
			if(value != null) {
				label.setText(((PrintServiceTO)value).getPresentation());
			}
		
			return label;
		}
		
	}

	/**
	 * {@link PrintDialog} provides environment for {@link OutputFormatSelectionComponent} 
	 * @param prefferedColumnWidths 
	 * @param tab 
	 * 
	 */
	public PrintDialog(MainFrameTab parent, int[] preferredColumnWidths, List<PrintServiceTO> printservicesClient, IPrintoutCellEditorHelper cellEditorHelper) {
		super(new GridBagLayout());
		this.parent = new WeakReference<MainFrameTab>(parent);
		this.localeDelegate = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		this.cellEditorHelper = new WeakReference<IPrintoutCellEditorHelper>(cellEditorHelper);
		this.listener = new EventListenerList();
		this.printservicesClient = printservicesClient;
		this.printservices = new ArrayList<PrintServiceTO>();
		this.attachDocuments = new JCheckBox(localeDelegate.getMsg("attach.documents"));
		this.btnCollapseTree = new JButton(localeDelegate.getMsg("collapse.tree"));
		this.outputSelectionComponent = new NuclosOutputFormatSelectionComponent();
		
		int preferredWidth = 30; // Scrollbar etc.
		for (int width : preferredColumnWidths) {
			preferredWidth += width;
		}
		this.preferredWidth = preferredWidth;
		
		initialize();

	}

	/**
	 * initialize
	 * 
	 */
	protected void initialize() {

		final ExtendedAbstractTreeTable selectionComponent = this.outputSelectionComponent.getControlComponent();
	
		selectionComponent.getDefaultEditor(Integer.class);
		selectionComponent.getDefaultEditor(Object.class);

		/*
		int idxColumnPrintServices = selectionComponent.getColumnModel().getColumnIndex(E.REPORTOUTPUT.printservice.getFieldName());
		final TableColumn columnPrintServices = selectionComponent.getColumn(idxColumnPrintServices);
		this.printServiceCellEditor = new PrintServiceCellEditor(cmbxModelPrintservices, 
				columnPrintServices.getCellEditor()
				);
		
		this.printServiceCellRenderer = new PrintServiceCellRenderer();
		
		columnPrintServices.setCellEditor(this.printServiceCellEditor);
		columnPrintServices.setCellRenderer(this.printServiceCellRenderer);
		
		*/
		this.printServiceCellEditor = new PrintServiceCellEditor(selectionComponent, printservicesClient, new DefaultComboBoxModel());
		this.printServiceCellRenderer= new PrintServiceCellRenderer();
		
		this.trayCellEditor = new TrayCellEditor(new DefaultComboBoxModel(), cellEditorHelper);
		this.trayCellRenderer = new TrayCellRenderer();
		
		selectionComponent.setDefaultEditor(PrintServiceTO.class, this.printServiceCellEditor);		
		selectionComponent.setDefaultRenderer(PrintServiceTO.class, this.printServiceCellRenderer);
		selectionComponent.setDefaultEditor(PrintServiceTO.Tray.class, this.trayCellEditor);		
		selectionComponent.setDefaultRenderer(PrintServiceTO.Tray.class, this.trayCellRenderer);
		final GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 4;
		c.ipady = 40;
		c.weightx = 1;
		c.weighty = 1;
		c.insets = new Insets(10, 10, 2, 10);
		JScrollPane scroll = new JScrollPane(selectionComponent);
		scroll.setBackground(Color.WHITE);
		this.add(scroll,c);
		c.fill = GridBagConstraints.NONE;
		c.gridx = 3;
		c.gridy = 1;
		c.gridwidth=1;
		c.ipady = 0;
		c.weightx = 0;
		c.weighty = 0;
		c.anchor = GridBagConstraints.LAST_LINE_END;
		c.insets = new Insets(2, 10, 10, 10);
		this.add(attachDocuments, c);
		c.anchor = GridBagConstraints.LAST_LINE_START;
		c.gridx = 1;
		this.add(btnCollapseTree, c);
		this.setVisible(true);
	}
/*
 * model gets updated by the PrintController directly
	public void updateModel(PropertyChangeEvent evt) {
		final List<PrintoutTO> printouts = (List<PrintoutTO>)evt.getNewValue();
		
		if (!(this.outputSelectionComponent
				.getControlComponent()
				.getTreeTableModel() instanceof DefaultPrintoutModel)) {
			Log.warn("ignore model update");
			return;
		}
		LOG.info("updateModel execution");
		DefaultPrintoutModel m = (DefaultPrintoutModel)this.outputSelectionComponent
		.getControlComponent()
		.getTreeTableModel();
		m.clear();
		for (final PrintoutTO printout: printouts ) {
			m.addPrintout(printout);
		}

		
	}
	*/


	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (PrintEvents.PROPERTY_MODEL_PRINTSERVICES.equals(evt.getPropertyName())) {
			printServiceCellEditor.propertyChange(evt);
		}
		/*
		else if (PrintEvents.PROPERTY_MODEL_PRINTOUTS.equals(evt.getPropertyName())) {
			//updateModel(evt);
		}
		*/
		
	}
	
	@Override
	public Dimension getPreferredSize() {
		int parentWidth = 600;
		MainFrameTab parent = this.parent.get();
		if (parent != null) {
			parentWidth = parent.getWidth() - 40;
		}
		return new Dimension(Math.min(parentWidth, preferredWidth), 400);
	}


	public NuclosOutputFormatSelectionComponent getOutputSelectionComponent() {
		return outputSelectionComponent;
	}
	
	public JCheckBox getAttachDocumentCheckBox() {
		return attachDocuments;
	}
	
	public JButton getCollapseTreeButton() {
		return btnCollapseTree;
	}
}
