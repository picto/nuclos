//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.report.valueobject.PrintServiceTO;


/**
 * meta description for field
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class NuclosFieldColumn implements PrintoutColumn {
	
	private final FieldMeta<?> metaField;
	private final boolean isEditable;

	public NuclosFieldColumn(final FieldMeta<?> metaField, final boolean isEditable) {
		this.metaField = metaField;
		this.isEditable = isEditable;
	}
	public FieldMeta<?> getField() {
		return this.metaField;
	}

	@Override
	public boolean isEditable() {
		return this.isEditable;
	}

	@Override
	public String getName() {
		return metaField.getLocaleResourceIdForLabel();
	}

	@Override
	public String getDescription() {
		return metaField.getLocaleResourceIdForDescription();
	}

	@Override
	public Class<?> getJavaClass() {
		if (E.PRINTSERVICE.checkEntityUID(metaField.getForeignEntity())) {
			return PrintServiceTO.class;
		}
		if (E.PRINTSERVICE_TRAY.checkEntityUID(metaField.getForeignEntity())) {
			return PrintServiceTO.Tray.class;
		}
		return metaField.getJavaClass();
	}
	@Override
	public UID getId() {
		return metaField.getUID();
	}	
}