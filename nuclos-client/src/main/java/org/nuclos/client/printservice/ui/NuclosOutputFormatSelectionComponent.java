//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.printservice.ui;

import java.awt.event.MouseListener;

import org.apache.commons.lang.NotImplementedException;
import org.nuclos.api.report.OutputFormat;
import org.nuclos.client.printservice.PrintDialog;

/**
 * {@link NuclosOutputFormatSelectionComponent} 
 * 
 * selection component for {@link OutputFormat}
 * 
 * @see PrintDialog
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class NuclosOutputFormatSelectionComponent implements
		OutputFormatSelectionComponent {

	private ExtendedAbstractTreeTable controlComponent;
	
	public NuclosOutputFormatSelectionComponent() {
		this.initialize();
	}
	
	protected void initialize() {
		controlComponent = new 	ExtendedAbstractTreeTable();
		controlComponent.setRootVisible(false);
	}
	
	@Override
	public void addMouseListener(MouseListener l) {
		this.controlComponent.addMouseListener(l);
	}

	@Override
	public ExtendedAbstractTreeTable getControlComponent() {
		return this.controlComponent;
	}

	@Override
	public AbstractPrintoutModel getModel() {
		throw new NotImplementedException("getModel not implemented yet");
	}


}
