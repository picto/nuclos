package org.nuclos.client.printservice.ui;

import java.awt.event.MouseListener;

import javax.swing.*;

/**
 * description for print profile chooser components
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface OutputFormatSelectionComponent {

	public void addMouseListener(MouseListener l);

	/**
	 * get data model
	 * 
	 * @return {@link AbstractPrintoutModel} model
	 */
	public AbstractPrintoutModel getModel();
	
	/**
	 * get control component
	 * 
	 * @return {@link JComponent} 
	 */
	public <T extends JComponent> T getControlComponent();
}