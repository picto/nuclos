package org.nuclos.client;

import org.apache.log4j.Logger;
import org.nuclos.common.NuclosFatalException;

import com.jacob.com.LibraryLoader;

import de.novabit.NativeLibLoader;

/**
 * Loads platform-dependent libraries.
 *
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class ClientLibLoader {
	private static final Logger LOG = Logger.getLogger(ClientLibLoader.class);

	private static volatile boolean jacobLoaded = false;
	private static volatile boolean jacobUnavailable = false;

	/**
	 * Tries to load the jacob lib only once.
	 *
	 * @return
	 * @throws NuclosFatalException
	 */
	public static synchronized boolean loadJacob() throws NuclosFatalException {
		if(jacobUnavailable) {
			return false;
		}
		if(!jacobLoaded) {
			try {
				final String lib = LibraryLoader.getPreferredDLLName() + ".dll";
				NativeLibLoader.load(lib);
				jacobLoaded = true;
			} catch (SecurityException e) {
				jacobUnavailable = true;
				LOG.error("Error loading jacob lib", e);
				throw new NuclosFatalException("nuclos.jacob.linkerror", e);
			} catch (UnsatisfiedLinkError e) {
				jacobUnavailable = true;
				LOG.error("Error loading jacob lib", e);
				throw new NuclosFatalException("nuclos.jacob.linkerror", e);
			}
		}
		return true;
	}
}
