package org.nuclos.client.ui.error;

import java.awt.*;

public class SimpleExceptionForDisplay extends Exception implements INuclosExceptionForDisplay {
	
	private final boolean expected;
	
	private final boolean business;
	
	private final ESeverity severity;
	
	private final Object[] params;
	
	private Component component;
	
	private EDisplayType displayType;
	
	public SimpleExceptionForDisplay(Exception cause, boolean expected, boolean business, ESeverity severity, String msg, Object... params) {
		super(msg, cause);
		
		this.expected = expected;
		this.business = business;
		this.severity = severity;
		this.params = params;
		
		if (severity == null) {
			throw new NullPointerException();
		}
	}

	@Override
	public Object[] getParams() {
		return params;
	}

	@Override
	public String getResourceId() {
		return getMessage();
	}

	@Override
	public boolean isExpected() {
		return expected;
	}

	@Override
	public boolean isBusinesssException() {
		return business;
	}

	@Override
	public ESeverity getSeverity() {
		return severity;
	}

	@Override
	public Component getAssociatedComponent() {
		return component;
	}
	
	public void setAssociatedComponent(Component comp) {
		this.component = comp;
	}

	@Override
	public EDisplayType getDisplayType() {
		return displayType;
	}
	
	public void setDisplayType(EDisplayType displayType) {
		this.displayType = displayType;
	}

}
