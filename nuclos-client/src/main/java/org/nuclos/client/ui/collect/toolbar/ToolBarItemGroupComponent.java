//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.toolbar;

import java.util.Collection;

import javax.swing.*;

import org.nuclos.common.collect.ToolBarItem;

public abstract class ToolBarItemGroupComponent {

	public static final String PROPERTY_KEY = "nuclos_ToolBarItemGroupComponent";
	
	private final JComponent toolBarComponent;
	
	public ToolBarItemGroupComponent(JComponent toolBarComponent) {
		this.toolBarComponent = toolBarComponent;
	}
	
	abstract protected JComponent[] createStatic(ToolBarItem item);
	
	abstract protected JComponent[] create(ToolBarItem item, Action action);
	
	abstract protected boolean isHideLabelPossible();
	
	protected void clear(Collection<JComponent> components) {
		for (JComponent jcomp : components) {
			toolBarComponent.remove(jcomp);
		}
	}
	
	protected void addInFront(JComponent jcomp) {
		toolBarComponent.add(jcomp, 0);
	}
	
	protected void invalidate() {
		toolBarComponent.invalidate();
		toolBarComponent.revalidate();
		toolBarComponent.repaint();
	}
	
}
