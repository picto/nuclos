//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.

package org.nuclos.client.ui.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.client.ui.collect.subform.SubFormTableModel;
import org.nuclos.common.collect.collectable.CollectableEntityField;

/**
 * EntityFieldUtils
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class EntityFieldUtils {

	/**
	 * Find fields from lstMaster also contained in lstSlave
	 * 
	 * @param lstMaster	origin fields	
	 * @param lstSlave	target fields
	 * @return 
	 */
	public static Map<Integer, Integer> createFieldMatching(final List<CollectableEntityField> lstMaster, final List<CollectableEntityField> lstSlave) {
		final Map<Integer, Integer> mpMatch = new HashMap<Integer, Integer>();
		int idxMaster = 0;
		for (final CollectableEntityField cltEfMaster : lstMaster) {
			// lookup field with matching name & datatype

			int idxMatch = -1;
			int idxSlave = 0;
			for (final CollectableEntityField cltEfSlave : lstSlave) {
				// match if name & datatype are the same
				boolean match = cltEfSlave.getUID().equals(cltEfMaster.getUID());
				if (match) {
					idxMatch = idxSlave;
					break;
				} 
				++idxSlave;
			}



			// check if idx matched
			if (idxMatch > -1) {
				mpMatch.put(idxMaster, idxMatch);
			}
			++idxMaster;
		}
		return Collections.unmodifiableMap(mpMatch);
	}

	/**
	 * List of collectable entity fields in model
	 * 
	 * @param sfTblModel	subform table model
	 * @return
	 */
	public static List<CollectableEntityField> getFieldsFromModel(final SubFormTableModel sfTblModel) {
		final List<CollectableEntityField> lstFields = new ArrayList<CollectableEntityField>();
		for (int iColumn = 0; iColumn < sfTblModel.getColumnCount(); iColumn++) {
			lstFields.add(sfTblModel.getCollectableEntityField(iColumn));
		}
		return Collections.unmodifiableList(lstFields);
	}
}
