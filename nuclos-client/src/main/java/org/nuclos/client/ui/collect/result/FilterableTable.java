package org.nuclos.client.ui.collect.result;

import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.table.CommonJTable;

/**
 * Created by guenthse on 3/2/2017.
 */
public class FilterableTable extends CommonJTable {

    public final CollectableEntityFieldBasedTableModel getCollectableEntityFieldBasedTableModel() {
        return (CollectableEntityFieldBasedTableModel) super.getModel();
    }
}
