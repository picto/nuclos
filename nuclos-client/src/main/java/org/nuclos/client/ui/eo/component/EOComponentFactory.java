//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.util.ArrayList;
import java.util.Collection;

import javax.swing.*;

import org.nuclos.client.common.MetaProvider;
import org.nuclos.client.ui.eo.renderer.SimpleEntityObjectValueListCellRenderer;
import org.nuclos.client.ui.model.SimpleCollectionComboBoxModel;
import org.nuclos.client.ui.model.SortedListModel;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.EntityObjectValueComparator;
import org.springframework.beans.factory.annotation.Autowired;

public class EOComponentFactory {
	
	private static EOComponentFactory INSTANCE;
	
	// Spring injection
	
	@Autowired
	private TypeConverterFactory typeConverterFactory;
	
	@Autowired
	private MetaProvider metaProvider;
	
	// end of Spring injection
	
	EOComponentFactory() {
		INSTANCE = this;
	}
	
	/**
	 * @deprecated Use spring injection instead.
	 */
	public static EOComponentFactory getInstance() {
		return INSTANCE;
	}
	
	public <PK> JComboBox newSimpleEoComboBox(Collection<EntityObjectVO<PK>> items, UID fieldToDisplay) {
		final SortedListModel<EntityObjectVO<PK>> model = new SortedListModel<EntityObjectVO<PK>>(
				items, new EntityObjectValueComparator<PK>(fieldToDisplay), true);
		final JComboBox result = new JComboBox(model);
		result.setRenderer(new SimpleEntityObjectValueListCellRenderer<PK>(typeConverterFactory, fieldToDisplay));
		return result;
	}

	public <PK> EoRefJComboBox<PK> newRefComboBox(Collection<EntityObjectVO<PK>> items, UID fieldToDisplay, UID writeRefTo, boolean addEmpty) {
		if (addEmpty) {
			if (items == null || items.isEmpty()) {
				items = new ArrayList<EntityObjectVO<PK>>();
				final FieldMeta<?> fm = metaProvider.getEntityField(fieldToDisplay);
				items.add(new EntityObjectVO<PK>(fm.getEntity()));
			} else {
				final EntityObjectVO<PK> first = items.iterator().next();
				// avoid UnsupportedOperationException
				items = new ArrayList<EntityObjectVO<PK>>(items);
				items.add(new EntityObjectVO<PK>(first.getDalEntity()));
			}
		}
		final SortedListModel<EntityObjectVO<PK>> model = new SortedListModel<EntityObjectVO<PK>>(
				items, new EntityObjectValueComparator<PK>(fieldToDisplay), true);
		final EoRefJComboBox<PK> result = new EoRefJComboBox<PK>(model, writeRefTo);
		result.setRenderer(new SimpleEntityObjectValueListCellRenderer<PK>(typeConverterFactory, fieldToDisplay));
		return result;
	}
	
	public <PK> EoValueJComboBox<PK> newValueComboBox(Collection<EntityObjectVO<PK>> items, UID fieldToDisplay, Object defaultValue) {
		final SortedListModel<EntityObjectVO<PK>> model = new SortedListModel<EntityObjectVO<PK>>(
				items, new EntityObjectValueComparator<PK>(fieldToDisplay), true);
		final EoValueJComboBox<PK> result = new EoValueJComboBox<PK>(typeConverterFactory, model, fieldToDisplay, defaultValue);
		result.setRenderer(new SimpleEntityObjectValueListCellRenderer<PK>(typeConverterFactory, fieldToDisplay));
		result.setAssociatedEntityObject(null);
		result.fromModelToComponent();
		return result;
	}
	
	public <PK,T> SimpleValueJComboBox<PK> newSimpleValueComboBox(Collection<T> items, UID fieldInModel, EntityObjectVO<PK> modelEo, Object defaultValue) {
		final SimpleCollectionComboBoxModel<T> model = new SimpleCollectionComboBoxModel<T>(items);
		final SimpleValueJComboBox<PK> result = new SimpleValueJComboBox<PK>(typeConverterFactory, model, fieldInModel, defaultValue);
		result.setAssociatedEntityObject(modelEo);
		result.fromModelToComponent();
		return result;
	}
	
	public <PK> EoValueJLabel<PK> newJLabel(EntityObjectVO<PK> eo, UID fieldToDisplay) {
		final EoValueJLabel<PK> result = new EoValueJLabel<PK>(typeConverterFactory, fieldToDisplay);
		result.setAssociatedEntityObject(eo);
		result.fromModelToComponent();
		return result;
	}

	public <PK> EoValueJTextField<PK> newJTextField(EntityObjectVO<PK> eo, int columns, UID fieldToDisplay) {
		final FieldMeta<?> fieldMeta = metaProvider.getEntityField(fieldToDisplay);
		final EoValueJTextField<PK> result = new EoValueJTextField<PK>(typeConverterFactory, columns, fieldMeta);
		result.setAssociatedEntityObject(eo);
		result.fromModelToComponent();
		return result;
	}

}
