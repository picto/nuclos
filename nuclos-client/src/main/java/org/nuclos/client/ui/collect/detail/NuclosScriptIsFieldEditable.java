//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>
package org.nuclos.client.ui.collect.detail;

import java.lang.reflect.InvocationTargetException;

import org.nuclos.client.common.AbstractDetailsSubFormController;
import org.nuclos.client.scripting.ScriptEvaluator;
import org.nuclos.client.scripting.context.SubFormFieldScriptContext;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * is field editable evaluation
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 * @param <PK> primary key
 * @param <Clct> {@link Collectable}
 */
public class NuclosScriptIsFieldEditable<PK,Clct extends Collectable<PK>>  implements FieldEvaluator<Boolean> {

	private static final Logger LOG = LoggerFactory.getLogger(NuclosScriptIsFieldEditable.class);

	private final AbstractDetailsSubFormController<?, ?> parentSubformCtl;
	private final NuclosScript script;

	public NuclosScriptIsFieldEditable(
			final AbstractDetailsSubFormController<?, ?> parentSubformCtl,
			final NuclosScript script) {
		if (null == script) 
			throw new IllegalArgumentException("script must not be null");
		
		this.parentSubformCtl = parentSubformCtl;
		this.script = script;
	}

	@Override
	public Boolean evaluate(CollectController<?, ?> parentClctl,
			AbstractDetailsSubFormController<?, ?> subformCtl,
			Collectable<?> collectable,
			CollectableEntityField collectableEntityField) {
		Object o = Boolean.TRUE;
		try {
			o = ScriptEvaluator.getInstance().eval(script, 
					new SubFormFieldScriptContext(parentClctl, parentSubformCtl, subformCtl, collectable,collectableEntityField));
			if (o instanceof Boolean) {
				return (Boolean) o;
			}

		} catch (InvocationTargetException e) {
			LOG.warn("Failed to evaluate script expression: " + e, e.getCause());
			return false;
		} catch (Exception e) {
			LOG.warn("Failed to evaluate script expression: " + e, e);
			return false;
		}
		LOG.warn("expected expression result to be boolean: " + o);
		return true;
	}


}
