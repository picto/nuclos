package org.nuclos.client.ui.collect;

import java.awt.event.KeyEvent;

import javax.swing.*;

import org.nuclos.client.ui.LayoutNavigationCollectable;

public class DefaultLayoutNavigationSupportContext implements LayoutNavigationSupportContext {

	private boolean pressed;
	private KeyStroke ks;
	private KeyEvent e;
	private int condition;
	private JComponent component;
	private boolean hasBeenProcessed;
	private LayoutNavigationCollectable lnc;

	public DefaultLayoutNavigationSupportContext(boolean pressed, KeyStroke ks,
			KeyEvent e, int condition, JComponent component, LayoutNavigationCollectable lnc) {
		super();
		this.pressed = pressed;
		this.ks = ks;
		this.e = e;
		this.condition = condition;
		this.component = component;
		this.hasBeenProcessed = false;
		this.lnc = lnc;
	}

	@Override
	public boolean getPressed() {
		return pressed;
	}

	@Override
	public KeyStroke getKeyStroke() {
		return ks;
	}

	@Override
	public KeyEvent getKeyEvent() {
		return e;
	}

	@Override
	public int getCondition() {
		return condition;
	}

	@Override
	public JComponent getComponent() {
		return component;
	}

	public void setProcessed(boolean processed) {
		this.hasBeenProcessed = processed;
	}
	
	@Override
	public boolean hasBeenProcessed() {
		return this.hasBeenProcessed;
	}

	@Override
	public LayoutNavigationCollectable getLayoutNavigationCollectable() {
		return this.lnc;
	}

}
