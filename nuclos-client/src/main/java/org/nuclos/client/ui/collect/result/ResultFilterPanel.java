package org.nuclos.client.ui.collect.result;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.io.Closeable;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.HorizontalLayout;
import org.jdesktop.swingx.JXCollapsiblePane;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.collect.subform.Column;
import org.nuclos.client.ui.collect.subform.SubFormTable;
import org.nuclos.client.ui.collect.component.CollectableComboBox;
import org.nuclos.client.ui.collect.component.CollectableComponent;
import org.nuclos.client.ui.collect.model.CollectableEntityFieldBasedTableModel;
import org.nuclos.client.ui.collect.subform.SubformRowHeader;
import org.nuclos.common.UID;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common2.SpringLocaleDelegate;

/**
 * Created by guenthse on 3/2/2017.
 */
public class ResultFilterPanel extends JXCollapsiblePane implements Closeable {

    private static final Logger LOG = Logger.getLogger(ResultFilterPanel.class);

    private JPanel filterPanel = new JPanel(new HorizontalLayout());
    private JPanel resetFilterButtonPanel = new JPanel(new BorderLayout());
    private final JButton resetFilterButton = new JButton();

    private TableColumnModel columnModel;
    private Boolean fixedTable = false;
    private JTable table;

    private Map<UID, CollectableComponent> column2component = new HashMap<UID, CollectableComponent>();

    private boolean closed = false;

    public ResultFilterPanel(JTable table, TableColumnModel columnModel, Map<UID, CollectableComponent> column2component, Boolean fixedTable) {
        setLayout(new BorderLayout());
        this.columnModel = columnModel;
        this.column2component = column2component;
        this.fixedTable = fixedTable;
        this.table = table;
        

        add(filterPanel, BorderLayout.CENTER);
        init();
    }

    private void init() {
        setCollapsed(true);

        if (fixedTable) {
            resetFilterButtonPanel.setPreferredSize(new Dimension(SubformRowHeader.COLUMN_SIZE, 20));
            resetFilterButton.setIcon(Icons.getInstance().getIconClearSearch16());
            resetFilterButton.setToolTipText(SpringLocaleDelegate.getInstance().getMessage(
                    "subform.filter.tooltip", "Filterkriterien l\u00f6schen"));
            resetFilterButtonPanel.add(resetFilterButton, BorderLayout.CENTER);
        }

        arrangeFilterComponents();

        addListener();
        // subformFilterPanel is invisible by default. @see NUCLOSINT-1630
        setVisible(false);
    }
    @Override
    public void setCollapsed(boolean val) {
        super.setCollapsed(val);
    }

    /**
     * adds a listener on the columnmodel to get informed, when e.g. a column margin was changed
     * and to arrange the filter components again
     */
    public void addListener() {
        TableColumnModelListener listener = new TableColumnModelListener() {

            @Override
            public void columnAdded(TableColumnModelEvent e) {
                // do nothing
            }

            @Override
            public void columnMarginChanged(ChangeEvent e) {
                arrangeFilterComponents();
            }

            @Override
            public void columnMoved(TableColumnModelEvent e) {
                arrangeFilterComponents();
            }

            @Override
            public void columnRemoved(TableColumnModelEvent e) {
                // do nothing
            }

            @Override
            public void columnSelectionChanged(ListSelectionEvent e) {
                //arrangeFilterComponents();
            }
        };

        columnModel.addColumnModelListener(listener);
    }

    /**
     * arranges all filter components, because e.g a column was moved,
     * so that the filter components stay directly above the corresponding columns
     */
    final void arrangeFilterComponents() {
        int index = 0;
        filterPanel.removeAll();
        if(this.fixedTable) {
            filterPanel.add(this.resetFilterButtonPanel);
            index++;
        }

        for(TableColumn tc : CollectionUtils.iterableEnum(columnModel.getColumns())) {
            if(tc.getIdentifier() == null)
                continue;            
            UID identifier = tc.getIdentifier() instanceof UID ? (UID) tc.getIdentifier() : table.getModel() instanceof CollectableEntityFieldBasedTableModel ? ((CollectableEntityFieldBasedTableModel) table.getModel()).getColumnFieldUid(tc.getModelIndex()) : null;
            CollectableComponent clctcomp = identifier != null ? getCollectableComponentByName(identifier) : null;
            JComponent comp = (clctcomp == null) ? null : clctcomp.getControlComponent();

            
            if(clctcomp instanceof CollectableComboBox)
           	{
               	if(table instanceof SubFormTable)
               	{                		
               		Column col=((SubFormTable)table).getSubForm().getColumn(identifier);
               		if (col != null) {
               			((CollectableComboBox)clctcomp).setMultiSelect(col.isMultiselectsearchfilter());
               		}
                }
            }
            
            if(comp != null) {
                // place checkboxes in the center
                if (comp instanceof JCheckBox) {
                    JPanel p = new JPanel(new GridBagLayout());
                    p.add(comp);
                    comp = p;
                }

                comp.setPreferredSize(new Dimension(tc.getWidth(), 20));
                filterPanel.add(comp);
                index++;
            }
        }
        filterPanel.repaint();
        repaint();
    }

    private CollectableComponent getCollectableComponentByName(UID name) {
        if (org.nuclos.common.UID.UID_NULL.equals(name) || null == name) {
            return null;
        }

        for (CollectableComponent comp : column2component.values()) {
            if (comp.getEntityField().getUID().equals(name)) {
                return comp;
            }
        }

        return null;
    }

    public JButton getResetFilterButton() {
        return this.resetFilterButton;
    }

    /**
     * @return the active filter components
     */
    public Map<UID, CollectableComponent> getActiveFilterComponents() {
        Map<UID, CollectableComponent> activeActiveFilterComponents = new HashMap<UID, CollectableComponent>();

        for (int index=0 ; index<columnModel.getColumnCount() ; index++) {
            TableColumn column = columnModel.getColumn(index);
            if(column.getIdentifier() == null)
                continue;
            UID identifier = column.getIdentifier() instanceof UID ? (UID) column.getIdentifier() : table.getModel() instanceof CollectableEntityFieldBasedTableModel ? ((CollectableEntityFieldBasedTableModel) table.getModel()).getColumnFieldUid(column.getModelIndex()) : null;
            if (identifier != null) {
                CollectableComponent comp = column2component.get(identifier);

                if (!this.fixedTable) {
                    activeActiveFilterComponents.put(identifier, comp);
                }
                else if (!"".equals(identifier)) {
                    activeActiveFilterComponents.put(identifier, comp);
                }
            }
        }

        return activeActiveFilterComponents;
    }

    @Override
    public void close() {
        // Close is needed for avoiding memory leaks
        // If you want to change something here, please consult me (tp).
        if (!closed) {
            LOG.debug("close(): " + this);
            filterPanel = null;
            columnModel = null;
            column2component.clear();
            column2component = null;

            // JXCollapsiblePane
            removeAll();
            // setContentPane(null);

            closed = true;
        }
    }
}
