package org.nuclos.client.ui.error;

/**
 * Defines the way a exception is displayed to the user.
 * 
 * @author Thomas Pasch
 * @since Nuclos 4.4.1
 */
public enum EDisplayType {
	
	/**
	 * This is the ONLY value implemented so far.
	 */
	MODAL_DIALOG,
	
	BUBBLE,
	
	OVERLAY;

}
