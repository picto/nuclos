//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.collect.component;

import org.apache.log4j.Logger;
import org.nuclos.client.entityobject.CollectableEntityObjectField;
import org.nuclos.client.ui.HyperlinkRefField;
import org.nuclos.client.ui.labeled.LabeledComponentSupport;
import org.nuclos.client.ui.labeled.LabeledHyperlink;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableField;

/**
 * <code>CollectableComponent</code> to display/enter a hyperlink. <br>
 * <br>
 * Created by Novabit Informationssysteme GmbH <br>
 * Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author <a href="mailto:andreas.laemmlein@nuclos.de">Andreas Lämmlein</a>
 * @version 01.00.00
 */
public class CollectableHyperlinkRef extends CollectableHyperlink {

	private static final Logger LOG = Logger.getLogger(CollectableHyperlinkRef.class);

	/**
	 * @param clctef
	 * @param bSearchable
	 */
	public CollectableHyperlinkRef(CollectableEntityField clctef, boolean bSearchable) {
		super(clctef, bSearchable,
				new LabeledHyperlink(new LabeledComponentSupport(),
						clctef.isNullable(), bSearchable,
						new HyperlinkRefField<Long>(clctef, new LabeledComponentSupport(), bSearchable)));

	}

	@SuppressWarnings("unchecked")
	@Override
	public HyperlinkRefField<Long> getHyperlink() {
		return (HyperlinkRefField<Long>) super.getHyperlink();
	}

	@Override
	protected void updateView(CollectableField clctfValue) {
		super.updateView(clctfValue);

		getHyperlink().setReferencedObjectId(getId(clctfValue));
	}

	private Long getId(CollectableField clctfValue) {
		if (clctfValue instanceof CollectableEntityObjectField) {
			return (Long) ((CollectableEntityObjectField) clctfValue).getEntityObjectId();
		}

		return (Long) clctfValue.getValueId();
	}
}
