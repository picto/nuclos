//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EasySelectController<T> extends SelectObjectsController<T> {

	protected static class EasySelectPanel<T> extends DefaultSelectObjectsPanel<T> {
		protected EasySelectPanel(String text1, String text2, String text3, String text4) {
			this.labAvailableColumns.setText(text1);
			this.labSelectedColumns.setText(text2);

			this.btnLeft.setToolTipText(text3);
			this.btnRight.setToolTipText(text4);

			this.btnUp.setVisible(false);
			this.btnDown.setVisible(false);

			this.btnLeft.setVisible(true);
			this.btnRight.setVisible(true);
		}
	}  // inner class SelectColumnsPanel

	public EasySelectController(Component parent, String text1, String text2, String text3, String text4, String tooltiptxt) {
		super(parent, new EasySelectPanel(text1, text2, text3, text4));
		final EasySelectPanel pnl = (EasySelectPanel) getPanel();

		String tooltipTxt = "<html>";
		tooltipTxt += tooltiptxt;
		tooltipTxt += "</html>";
		pnl.getJListAvailableObjects().setToolTipText(tooltipTxt);
		pnl.getJListSelectedObjects().setToolTipText(tooltipTxt);
	}
	
	protected EasySelectController(Component parent, SelectObjectsPanel<T> panel) {
		super(parent, panel);
	}

	public List<String> getSelectedStrings() {
		List<String> strings = new ArrayList<String>();
		for (T sel : getSelectedObjects()) strings.add(sel.toString());
		return strings;
	}
}
