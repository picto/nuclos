//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.eo.component;

import java.text.ParseException;

import javax.swing.*;

import org.nuclos.client.ui.eo.mvc.IModelAwareGuiComponent;
import org.nuclos.client.ui.eo.mvc.ITypeConverter;
import org.nuclos.client.ui.model.SortedListModel;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonValidationException;

public class EoValueJComboBox<PK> extends JComboBox implements IModelAwareGuiComponent<PK> {
	
	private final UID fieldUid;
	
	private final Object defaultValue;
	
	private final ITypeConverter converter;
	
	private EntityObjectVO<PK> associatedEo;
	
	public EoValueJComboBox(TypeConverterFactory tcFactory, SortedListModel<EntityObjectVO<PK>> model, UID fieldUid, Object defaultValue) {
		super(model);
		this.fieldUid = fieldUid;
		this.defaultValue = defaultValue;
		this.converter = tcFactory.getTypeConverterForField(fieldUid);
	}
	
	@Override
    public void setModel(ComboBoxModel aModel) {
		super.setModel((SortedListModel<EntityObjectVO<PK>>) aModel);
	}

	@Override
	public void fromComponentToModel() throws CommonValidationException, ParseException {
		final EntityObjectVO<PK> selected = (EntityObjectVO<PK>) getSelectedItem();
		if (selected != null) {
			final Object guiValue = selected.getFieldValue(fieldUid);
			if (guiValue != null) {
				EntityObjectUtils.setOrUpdateValue(associatedEo, fieldUid, converter.fromComponentToModel(guiValue));
				// to.setFieldValue(fieldUid, converter.fromComponentToModel(guiValue));
			} else {
				EntityObjectUtils.setOrUpdateValue(associatedEo, fieldUid, defaultValue);
				// to.setFieldValue(fieldUid, defaultValue);
			}
		} else {
			EntityObjectUtils.setOrUpdateValue(associatedEo, fieldUid, defaultValue);
			// to.setFieldValue(fieldUid, defaultValue);
		}
	}

	@Override
	public void fromModelToComponent() {
		final Object modelValue = associatedEo.getFieldValue(fieldUid);
		if (modelValue != null) {
			// TODO: I guess we should search here in the model...
			setSelectedItem(converter.fromModelToComponent(modelValue));
		} else {
			setSelectedItem(defaultValue);
		}
	}

	@Override
	public void setAssociatedEntityObject(EntityObjectVO<PK> eo) {
		this.associatedEo = eo;
	}

	@Override
	public EntityObjectVO<PK> getAssociatedEntityObject() {
		return associatedEo;
	}

}
