package org.nuclos.client.ui;

import javax.swing.*;

public interface EditorContainer {
	
	void initLayoutNavigationSupport(LayoutNavigationCollectable lnc);
	
	JComponent getInnerEditor();
}
