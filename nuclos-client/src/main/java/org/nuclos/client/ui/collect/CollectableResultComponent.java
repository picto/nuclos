package org.nuclos.client.ui.collect;

import java.awt.LayoutManager;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.nuclos.client.ui.collect.component.CollectableComponentType;
import org.nuclos.common.UID;

/**
 * Created by guenthse on 3/2/2017.
 */
public abstract class CollectableResultComponent extends JPanel {

    protected UID entityUid;

    private final Map<UID, ResultComponentColumn> mpColumns = new LinkedHashMap<UID, ResultComponentColumn>();

    public CollectableResultComponent(LayoutManager layout) {
        super(layout);
    }

    public UID getEntityUID() {
        return entityUid;
    }

    public ResultComponentColumn getColumn(UID sColumnName) {
        return mpColumns.get(sColumnName);
    }

    /**
     * @param sColumnName
     * @return the <code>CollectableComponentType</code> of the column with the given name (default: null).
     */
    public CollectableComponentType getCollectableComponentType(UID sColumnName, boolean bSearchable) {
        final ResultComponentColumn column = this.getColumn(sColumnName);
        return (column != null) ? column.getCollectableComponentType() : null;
    }

    public abstract JScrollPane getScrollPane();

	public abstract UID getLayoutUID();

	public static class ResultComponentColumn {

        private final UID uid;
        private final CollectableComponentType clctcomptype;

        public ResultComponentColumn(UID uid, CollectableComponentType clctcomptype) {
            this.uid = uid;
            this.clctcomptype = clctcomptype;
        }

        public UID getUID() {
            return this.uid;
        }

        public CollectableComponentType getCollectableComponentType() {
            return clctcomptype;
        }
    }
}
