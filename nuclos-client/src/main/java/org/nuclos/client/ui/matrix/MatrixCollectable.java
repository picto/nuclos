package org.nuclos.client.ui.matrix;

import java.util.Date;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common.dal.vo.IDataLanguageMap;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonFatalException;

/**
 * TODO
 * temp class, should be removed
 * @author marc.finke
 *
 */

public class MatrixCollectable implements Collectable, Comparable<MatrixCollectable> {

	Long iId;
	Object value;
	int version;
	boolean bRemoved = false;
	boolean bModified = false;
	UID sField;
	//EntityFieldMetaDataVO metaVo;
	FieldMeta<Object> metaVo;
	EntityObjectVO vo;
	private String dataType;
	
	public MatrixCollectable(Object value) {
		this.iId = null;
		this.value = value;
	}
	
	public MatrixCollectable(Long iId, Object value) {
		this.iId = iId;
		this.value = value;
	}
	
	public MatrixCollectable(Long iId, Object value, int iVersion, String dataType) {
		this(iId, value, iVersion);
		this.dataType = dataType;
	}
	
	public MatrixCollectable(Long iId, Object value, int iVersion) {
		this.iId = iId;
		this.value = value;
		this.version = iVersion;
	}
	
	public void setVO(EntityObjectVO vo) {
		this.vo = vo;
	}
	
	public EntityObjectVO getVO() {
		return this.vo;
	}
	
	public void setEntityFieldMetaDataVO(FieldMeta<Object> vo) {
		this.metaVo = vo;
	}
	
	public FieldMeta<Object> getEntityFieldMetaDataVO() {
		return this.metaVo;
	}
	
	public void setField(UID field) {
		this.sField = field;		
	}
	
	public UID getField() {
		return this.sField;
	}
	
	@Override
	public int getVersion() {
		if(version < 0)
			return 0;
		return version;
	}

	@Override
	public Object getId() {
		return this.iId;
	}

	
	public String getIdentifierLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getValue(UID sFieldName) {
		return value;
	}

	@Override
	public Object getValueId(UID sFieldName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectableField getField(UID sFieldName)
			throws CommonFatalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setField(UID sFieldName, CollectableField clctfValue) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isComplete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String toString() {
		if(getValue(UID.UID_NULL) == null)
			return null;
		return getValue(UID.UID_NULL).toString();
	}

	@Override
	public int hashCode() {
		if(getValue(UID.UID_NULL) == null)
			return 0;
		return getValue(UID.UID_NULL).toString().hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof MatrixCollectable)) return false;
		
		MatrixCollectable that = (MatrixCollectable)obj;		
		if(that.getId() != null && getId() != null) {
			return that.getId().equals(getId());
		}
		return ObjectUtils.equals(that.getValue(UID.UID_NULL), this.getValue(UID.UID_NULL));
	}
	
	public void markAsModified() {
		bModified = true;
	}
	
	public boolean isModified() {
		return bModified;
	}
	
	public void markAsRemoved() {
		bRemoved = true;
	}
	
	public boolean isRemoved() {
		return bRemoved;
	}

	public String getDataType() {
		return dataType;
	}

	@Override
	public int compareTo(MatrixCollectable o) {
		if(o == null)
			return 0;
		
		if(o.getValue(UID.UID_NULL) instanceof Boolean) {
			Boolean bValueThis = (Boolean) getValue(UID.UID_NULL);
			Boolean bValueThat = (Boolean) o.getValue(UID.UID_NULL);
			return ObjectUtils.compare(bValueThis, bValueThat);
		}
		else if (o.getValue(UID.UID_NULL) instanceof String) {		
			String sValueThis = (String) getValue(UID.UID_NULL);
			String sValueThat = (String) o.getValue(UID.UID_NULL);
			return StringUtils.compareIgnoreCase(sValueThis, sValueThat);
		}
		else if(o.getValue(UID.UID_NULL) instanceof Date) {
			Date dateThis = (Date)getValue(UID.UID_NULL);
			Date dateThat = (Date)o.getValue(UID.UID_NULL);
			return ObjectUtils.compare(dateThis, dateThat);
		}
		else if(o.getValue(UID.UID_NULL) instanceof Integer) {
			Integer intThis = (Integer)getValue(UID.UID_NULL);
			Integer intThat = (Integer)o.getValue(UID.UID_NULL);
			return ObjectUtils.compare(intThis, intThat);
		}
		else {
			return 0;
		}
	}

	@Override
	public Object getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UID getEntityUID() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isDirty() {
		return bModified || bRemoved;
	}

	@Override
	public Object getLocalizedValue(UID field, UID language) {
		return null;
	};
	
	public IDataLanguageMap getDataLanguageMap(){
		// Override if needed
		return null;
	}
	
}
