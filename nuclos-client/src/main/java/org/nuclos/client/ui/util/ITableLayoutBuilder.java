//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui.util;

import java.awt.Component;

import info.clearthought.layout.TableLayout;

/**
 * ATTENTION: 
 * This interface should only contain <em>runtime, non-config, none-global</em> methods 
 * because it was created to support <em>inserts</em> into the layout 'table'.
 * 
 * For example org.nuclos.client.ui.util.TableLayoutBuilder.columns(double...) is not 
 * eligible to be included because it is configuration. Or, 
 * org.nuclos.client.ui.util.TableLayoutBuilder.getRowSize() is not eligible because
 * it returns the <em>global</em> size of the layout 'table'.
 * 
 * @since Nuclos 4.3.1
 * @author Thomas Pasch
 */
public interface ITableLayoutBuilder {

	ITableLayoutBuilder newRow();

	ITableLayoutBuilder newRow(double height);

	ITableLayoutBuilder skip();

	ITableLayoutBuilder skip(int colspan);

	ITableLayoutBuilder add(Component c);

	ITableLayoutBuilder add(Component c, int colspan);

	ITableLayoutBuilder add(Component c, int colspan, int halign, int valign);

	ITableLayoutBuilder addFullSpan(Component c);

	ITableLayoutBuilder addLabel(String text);

	ITableLayoutBuilder addLabel(String text, int valign);

	ITableLayoutBuilder addLabel(String text, String toolTip, int valign);

	ITableLayoutBuilder addLocalizedLabel(String resourceId);

	ITableLayoutBuilder addLocalizedLabel(String resourceId, int valign);

	ITableLayoutBuilder addLocalizedLabel(String resourceId, String toolTipResourceId);

	ITableLayoutBuilder addLocalizedLabel(String resourceId, String toolTipResourceId, int valign);

	TableLayout getTableLayout();
}
