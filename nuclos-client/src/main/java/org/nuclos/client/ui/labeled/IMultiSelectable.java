package org.nuclos.client.ui.labeled;

public interface IMultiSelectable {
	
	void setMultiSelect(Boolean multiSelect);
	
	Boolean isMultiSelect();

}
