//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ObjectUtils;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;

/**
 * ReferenceUtils
 * 
 * utilities for entity references
 * 
 * @author Moritz Neuhäuser &lt;moritz.neuhaeuser@nuclos.de&gt;
 */
public class ReferenceUtils {

	/**
	 * find first level (direct) references to entity
	 * 
	 * @param uidEntity entity id	
	 * @return map containing {@link EntityMeta} and referencing {@link FieldMeta}
	 */
	public static Map<EntityMeta, List<FieldMeta<?>>> findReferencingEntities(final UID uidEntity) {
		final MetaProvider prov = MetaProvider.getInstance();
		final Map<EntityMeta, List<FieldMeta<?>>> mpResult = new HashMap<EntityMeta, List<FieldMeta<?>>>();
		Collection<EntityMeta<?>> colAllEntities = prov.getAllEntities();
		
		for (final EntityMeta entityMetaVO : colAllEntities) {
			List<FieldMeta<?>> lstFieldMeta = null;
			Map<UID, FieldMeta<?>> colEntityFields = prov.getAllEntityFieldsByEntity(entityMetaVO.getUID());
			for (final FieldMeta<?> fieldMetaVO : colEntityFields.values()) {
				if (ObjectUtils.equals(fieldMetaVO.getForeignEntity(), uidEntity)) {
					if (mpResult.containsKey(entityMetaVO)) {
						lstFieldMeta = mpResult.get(entityMetaVO);
					} else {
						lstFieldMeta = new ArrayList<FieldMeta<?>>();
					}
					lstFieldMeta.add(fieldMetaVO);
					
					mpResult.put(entityMetaVO, lstFieldMeta);
				}
			}
		}
		
		return mpResult;
	}
}
