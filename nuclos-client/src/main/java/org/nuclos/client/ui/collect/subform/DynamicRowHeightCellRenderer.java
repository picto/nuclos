package org.nuclos.client.ui.collect.subform;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import org.nuclos.client.ui.collect.DynamicRowHeightSupport;

/**
 * Created by Oliver Brausch on 17.07.17.
 */
public class DynamicRowHeightCellRenderer implements TableCellRenderer {

	private final TableCellRenderer mainRenderer;

	DynamicRowHeightSupport support;

	private final int col;

	private final RowHeightController ctrl;

	public DynamicRowHeightCellRenderer(TableCellRenderer mainRenderer, DynamicRowHeightSupport support, int col, RowHeightController ctrl) {
		super();
		this.mainRenderer = mainRenderer;
		this.support = support;
		this.col = col;
		this.ctrl = ctrl;
	}

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		Component c = mainRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		ctrl.setHeight(col, row, support.getHeight(c));
		return c;
	}
}
