package org.nuclos.client.ui.labeled;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.ListCellRenderer;

import org.nuclos.common.collect.collectable.CollectableValueIdField;

public class CheckCombo<E> extends JComboBox implements ItemsProvider {
	
	private static long LAST_WHEN = 0;

	private boolean opensAutomaticallyWithMultiselect;
	private boolean multiSelect = false;
	private CheckComboRenderer<E> renderer;
	private ListCellRenderer oldrenderer;
		
	private Container fallbackParent;
	
    public CheckCombo(boolean opensAutomaticallyWithMultiselect) {
    	this.opensAutomaticallyWithMultiselect = opensAutomaticallyWithMultiselect;
    	renderer = new CheckComboRenderer<E>();
    	oldrenderer = (ListCellRenderer) getRenderer();
    	addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				if (!multiSelect) {
					return;
				}
				if (event.getModifiers() == 0) {
					return;
				}
			      
				Object selected = getSelectedItem();
				if (selected instanceof CollectableValueIdField) {
					//Such an ugly hack, because LabeledComboxBox is chaotic.
					if (event.getWhen() - LAST_WHEN > 100L) {
						CollectableValueIdField valueIdField = (CollectableValueIdField) selected;
						valueIdField.setSelected(!valueIdField.isSelected());
						LAST_WHEN = event.getWhen();
					}
					//repaint(); 
				}
			}
		});
    }
    
	/**
	 * Needed for https://support.novabit.de/browse/BMWFDM-728.
	 */
	public void setFallbackParent(Container parent) {
		this.fallbackParent = parent;
	}

	/**
	 * Needed for https://support.novabit.de/browse/BMWFDM-728.
	 */
	public Container getFallbackParent() {
		if (fallbackParent == null) {
			return super.getParent();
		}
		return fallbackParent;
	}

	public void setMultiSelect(Boolean bMultiEdit) {
		multiSelect = bMultiEdit != null ? bMultiEdit : false;
		if (bMultiEdit) {
			setRenderer(renderer);
		} else {
			setRenderer(oldrenderer);
		}
	}
    
    public Boolean isMultiSelect() {
    	return multiSelect;
    }

    @Override
    public void setPopupVisible(boolean flag) {
    	if (!multiSelect || (opensAutomaticallyWithMultiselect && flag)) {
    		super.setPopupVisible(flag);
    	}
    }
}
