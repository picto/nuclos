package org.nuclos.client.report.reportrunner;

import java.util.concurrent.Future;

import javax.swing.*;

import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.collect.CollectController;
import org.nuclos.client.ui.layer.LayerLock;

public class LockGuiProcessListener implements ProcessListener {
	
	private final CollectController controller;
	
	private final Future<LayerLock> lock;
	
	private final boolean messageOnDone;
	
	public LockGuiProcessListener(CollectController controller, boolean messageOnDone) {
		this.controller = controller;
		this.lock = controller.lockFrame();
		this.messageOnDone = messageOnDone;
	}

	@Override
	public void onDone(ProcessEvent event) {
		final BackgroundProcessInfo info = event.getBackgroundProcessInfo();
		if (info.getException() != null) {
			Errors.getInstance().showExceptionDialog(controller.getTab(), info.getMessage(), info.getException());
		} else if (messageOnDone) {
			JOptionPane.showMessageDialog(controller.getTab(), info.getMessage());
		}
		process();
	}

	@Override
	public void onCancel(ProcessEvent event) {
		final BackgroundProcessInfo info = event.getBackgroundProcessInfo();
		Errors.getInstance().showExceptionDialog(controller.getTab(), info.getMessage(), info.getException());
		process();
	}

	@Override
	public void onError(ProcessEvent event) {
		final BackgroundProcessInfo info = event.getBackgroundProcessInfo();
		Errors.getInstance().showExceptionDialog(controller.getTab(), info.getMessage(), info.getException());
		process();
	}
	
	private void process() {
		controller.unLockFrame(lock);
	}
	
}
