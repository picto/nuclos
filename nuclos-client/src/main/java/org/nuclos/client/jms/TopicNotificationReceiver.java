//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.jms;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PreDestroy;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.jms.TopicConnection;

import org.apache.log4j.Logger;
import org.nuclos.client.common.ShutdownActions;
import org.nuclos.client.jms.WeakReferenceMessageListener.ReferenceWithCleanup;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.NuclosFatalException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Receives topic based JMS messages in the client.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
// @Component
public class TopicNotificationReceiver implements InitializingBean, ApplicationContextAware {

	private static final Logger LOG = Logger.getLogger(TopicNotificationReceiver.class);
	
	static final DummyMessageListener dummyListener = new DummyMessageListener();

	private static TopicNotificationReceiver INSTANCE;
	
	//
	
	/**
	 * Subscribed to TOPICNAME_HEARTBEAT.
	 * 
	 * @see #init()
	 */
	private final HeartBeatMessageListener heartBeatListener = new HeartBeatMessageListener();
	
	/**
	 * realSubscribe must only be called after the Spring clientContext has initialized.
	 */
	private boolean readyToSubscribe = false;
	
	private boolean deferredSubscribe = true;

	private TopicConnection topicconn;
	
	private List<TopicInfo> infos = new ArrayList<TopicInfo>();
	
	// Spring injection
	
	private ConnectionFactory jmsFactory;
	
	private Timer timer;
	
	private ShutdownActions shutdownActions;
	
	// end of Spring injection
	
	// ApplicationContextAware
	
	private ClassPathXmlApplicationContext startupContext;

	/**
	 * list that holds all registered listeners for this topic receiver
	 */
	private List<WeakReferenceMessageListener> weakmessagelistener = new LinkedList<WeakReferenceMessageListener>();
	
	private ReferenceQueue<MessageListener> refqueue = new ReferenceQueue<MessageListener>();
	
	/**
	 * Last heartbeat number received. Is positive after the first heartbeat has arrived.
	 */
	private int last = -1;

	TopicNotificationReceiver() {
		INSTANCE = this;
	}
	
	// @PostConstruct
	public final void afterPropertiesSet() {
		try {
			// final ConnectionFactory connectionFactory = (ConnectionFactory) SpringApplicationContextHolder.getBean("jmsFactory");
			topicconn = (TopicConnection) jmsFactory.createConnection();


			shutdownActions.registerShutdownAction(ShutdownActions.SHUTDOWNORDER_JMS_TOPICS,
				new Thread() {
				@Override
				public void run() {
					unsubscribeAll();
					try {
						topicconn.close();
						topicconn = null;
					}
					catch(JMSException e) {
						throw new NuclosFatalException("Can't shutdown JMS connection", e);
					}
				}
			});

			topicconn.start();
			
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					int i = 0;
					Reference<? extends MessageListener> mlref;
					while ((mlref = refqueue.poll()) != null) {
						ReferenceWithCleanup weakmlref = (ReferenceWithCleanup) mlref;
						if (weakmlref.unsubscribeIfNecessary()) {
							i++;
						}
						weakmessagelistener.remove(weakmlref.getWeakReferenceMessageListener());
					}
					mlref = null;
					if (i > 0) {
						LOG.info("ReferenceQueue: Found and cleaned up " + i + " weak message listener");
					}
				}
			}, 10000l, 10000l);
			
			subscribe(JMSConstants.TOPICNAME_HEARTBEAT, heartBeatListener);
			
			timer.schedule(new TimerTask() {
				
				private int old = -1;
				private Date now;
				
				@Override
				public void run() {
					try {
						if (old != -1) {
							if (old == last) {
								// no heartbeat for received, but expected
								LOG.error("No heartbeat from server: Last beat " + old + " received on " + now);							
							}
						}
						old = last;
						now = new Date();
					} catch (Exception e) {
						LOG.error("Heartbeat timer task failed: " + e, e);
					}
				}
			}, 
			JMSConstants.JMS_HEARTBEAT_INTERVAL / 2, JMSConstants.JMS_HEARTBEAT_INTERVAL);
		}
		catch (JMSException e) {
			throw new NuclosFatalException("Can't establish JMS connection", e);
		}
	}
	
	// @Autowired
	public final void setConnectionFactory(ConnectionFactory jmsFactory) {
		this.jmsFactory = jmsFactory;
	}
	
	// @Autowired
	public final void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	// @Autowired
	public final void setShutdownActions(ShutdownActions shutdownActions) {
		this.shutdownActions = shutdownActions;
	}
	
	public synchronized final void setReadyToSubscribe(boolean readyToSubscribe) {
		this.readyToSubscribe = readyToSubscribe;
		if (readyToSubscribe && deferredSubscribe) {
			realSubscribe();
		}
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		startupContext = (ClassPathXmlApplicationContext) applicationContext;
	}
	
	public static TopicNotificationReceiver getInstance() {
		return INSTANCE;
	}
	
	public TopicConnection getTopicConnection() {
		return topicconn;
	}

	/**
	 * subscribes to the JMS topic with a JMS selector
	 */
	public void subscribe(String sTopicName, String correlationId, MessageListener messagelistener) {
		if (sTopicName == null) {
			throw new NullPointerException("No topic name");
		}
		if (messagelistener == null) {
			throw new NullPointerException("No MessageListener");
		}
		
		synchronized (this)
		{
			infos.add(new TopicInfo(sTopicName, correlationId, messagelistener));
			if (!deferredSubscribe) {
				realSubscribe();
			}
		}
	}
	
	public synchronized final void realSubscribe() {
		if (!readyToSubscribe || infos.isEmpty()) return;
		
		final List<TopicInfo> copy = new ArrayList<TopicInfo>(infos);
		infos.clear();
		assert deferredSubscribe || infos.isEmpty();
		if (copy.size() < 3) {
			for (TopicInfo i : copy) {
				WeakReferenceMessageListener weakrefmsglistener = new WeakReferenceMessageListener(i, startupContext, refqueue);
				weakrefmsglistener.subscribe();
				weakmessagelistener.add(weakrefmsglistener);
			}
			deferredSubscribe = false;
		}
		else {
			final Runnable run = new Runnable() {

				@Override
				public void run() {
						for (TopicInfo i : copy) {
							WeakReferenceMessageListener weakrefmsglistener = new WeakReferenceMessageListener(i, startupContext, refqueue);
							weakrefmsglistener.subscribe();
							synchronized (TopicNotificationReceiver.this) {
								weakmessagelistener.add(weakrefmsglistener);
							}
						}
						synchronized (TopicNotificationReceiver.this) {
							deferredSubscribe = false;
						}
				}
			};
			new Thread(run, "TopicNotificationReceiver.realSubscribe").start();
		}
		assert deferredSubscribe || infos.isEmpty();
	}

	/**
	 * subscribes to the JMS topic
	 * @param sTopicName
	 * @param messagelistener
	 */
	public void subscribe(String sTopicName, MessageListener messagelistener) {
		subscribe(sTopicName, null, messagelistener);
	}

	/**
	 * unsubscribes all registered JMS topics
	 */
	@PreDestroy
	synchronized void unsubscribeAll() {
		List<WeakReferenceMessageListener> tmp = new LinkedList<WeakReferenceMessageListener>(weakmessagelistener);
		for (WeakReferenceMessageListener ref : tmp) {
			unsubscribe(ref.getReference().get());
		}
		weakmessagelistener.clear();
	}

	/**
	 * unsubscribes the given <code>MessageListener</code> from the topic receiver
	 * @param messagelistener
	 */
	public synchronized void unsubscribe(MessageListener messagelistener) {
		List<WeakReferenceMessageListener> tmp = new LinkedList<WeakReferenceMessageListener>(weakmessagelistener);
		for (WeakReferenceMessageListener ref : tmp) {
			if (ref.getReference().get() == messagelistener) {
				ref.unsubscribe();
				weakmessagelistener.remove(ref);
				ref = null;
				break;
			}
		}
	}

	private class HeartBeatMessageListener implements MessageListener {
		
		public HeartBeatMessageListener() {
		}

		@Override
		public void onMessage(Message message) {
			LOG.info("onMessage: Received heartbeat message " + message);
			if (message instanceof TextMessage) {
				final TextMessage m = (TextMessage) message;
				try {
					final int i = Integer.valueOf(m.getText());
					if (last != -1 && last + 1 != i) {
						// we missed a beat
						LOG.error("Missed heartbeat: Last beat received: " + last + " beat now: " + i);
					}
					last = i;
				}
				catch (NumberFormatException e) {
					// ignore
				}
				catch (JMSException e) {
					// ignore
				}
			}
		}		
		
	}
	
	private static class DummyMessageListener implements MessageListener {
		
		public DummyMessageListener() {
		}

		@Override
		public void onMessage(Message message) {
			LOG.info("onMessage: Dummy listener received and ignores message " + message);
		}		
		
	}

}	// class TopicNotificationReceiver
