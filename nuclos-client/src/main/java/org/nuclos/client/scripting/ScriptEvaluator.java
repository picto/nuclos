//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;
import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.common.security.SecurityCache;
import org.nuclos.common.NuclosScript;
import org.springframework.beans.factory.annotation.Autowired;

import groovy.lang.Binding;

// @Component
public class ScriptEvaluator {

	private static final String VARIABLE_CONTEXT = "context";
	private static final String VARIABLE_LOG = "log";
	private static final String VARIABLE_USERNAME = "username";

	private static final Logger LOG = Logger.getLogger(ScriptEvaluator.class);
	
	//
	
	// Spring injection
	
	@Autowired
	private SecurityCache securityCache;
	
	@Autowired
	private GroovySupport groovySupport;
	
	// end of Spring injection

	private static ScriptEvaluator INSTANCE;
	
	//

	/**
	 * It seems that this is the a reason for PermGen space memory problems.
	 * Should be avoided altogether.
	 * 
	 * @author Thomas Pasch (javadocs)
	 */
	// private final ScriptEngine engine;

	ScriptEvaluator() {
		INSTANCE = this;
		// engine = new ScriptEngineManager().getEngineByName("groovy");
		
		// Create a dummy binding - as this will initialize 
		// the groovy support (Steffens Stacktrace of 07.05.2014) (tp)
		final Binding b = new Binding();
	}

	public static final ScriptEvaluator getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("Too early");
		}
		return INSTANCE;
	}

	public Object eval(NuclosScript script, ScriptContext context) throws InvocationTargetException {
		String sCurrentUsername = securityCache.getUsername();
		// final Bindings b = engine.createBindings();
		final Binding b = new Binding();
		b.setVariable(VARIABLE_CONTEXT, context);
		b.setVariable(VARIABLE_LOG, LOG);
		b.setVariable(VARIABLE_USERNAME, sCurrentUsername);

		// http://groovy.codehaus.org/Scripts+and+Classes
		final CompiledGroovy cg = groovySupport.compileScript(script);
		return cg.runScript(b);
	}
	
	/*
	public Object eval(NuclosScript script, ScriptContext context, Object defaultValue) {
		String sCurrentUsername = securityCache.getUsername();
		final Bindings b = engine.createBindings();
		b.put(VARIABLE_CONTEXT, context);
		b.put(VARIABLE_LOG, LOG);
		b.put(VARIABLE_USERNAME, sCurrentUsername);

        try {
			return engine.eval(script.getSource(), b);
		}
        catch (ScriptException e) {
        	LOG.error(e.getMessage(), NuclosExceptions.getCause(e));
			return defaultValue;
		}
	}
	 */
	
}
