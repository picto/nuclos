//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.apache.log4j.Logger;
import org.codehaus.groovy.runtime.DefaultGroovyMethods;
import org.nuclos.client.common.MetaProvider;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosScript;
import org.nuclos.common.UID;
import org.nuclos.common.caching.NBCache.LookupProvider;
import org.nuclos.common.caching.TimedCache;
import org.nuclos.common.collect.collectable.Collectable;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common2.LangUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import groovy.lang.GroovyClassLoader;

/**
 * Support for using groovy.
 * <p>
 * Within Nuclos, groovy can be used within the swing client for (a) client evaluated
 * rules [http://wiki.nuclos.de/pages/viewpage.action?pageId=327846] and for (script) 
 * conditions for displaying certain GUI elements (e.g. the 'new line' icon on a 
 * subform).
 * <p>
 * This class enables compiling groovy source to (java byte code) Classes. This lane is 
 * taken to avoid PermGen space problem in the GC. (Evaluationg groovy source directly 
 * would dynamically construct classes over and over again.)
 * <p>
 * To further reduce the problem, the GroovyClassLoader is cached as well as the 
 * compiled Classes (w.r.t. the groovy source).
 * 
 * @author Thomas Pasch (javadoc)
 */
// @Component
public class GroovySupport {

	private static final Logger LOG = Logger.getLogger(GroovySupport.class);
	
	private static final Cache<ClassLoader, GroovyClassLoader> classLoaderCache;
	
	static {
		CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder()
				.initialCapacity(3).maximumSize(3).weakKeys();
		classLoaderCache = builder.build();
	}
	
	private static GroovySupport INSTANCE;

	GroovySupport() {
		INSTANCE = this;
	}
	
	public static GroovySupport getInstance() {
		if (INSTANCE == null) {
			throw new IllegalStateException("Too early");
		}
		return INSTANCE;
	}

	private CompiledGroovy actualCompile(String text) {
		final GroovyClassLoader loader = getGroovyClassLoader();
		DefaultGroovyMethods.mixin(Collectable.class, CollectableMixin.class);
		try {
			final Class<?> groovyClass = loader.parseClass(text);
			return new CompiledGroovy(groovyClass);
		} catch (OutOfMemoryError e) {
			LOG.error("Compiling of the following groovy class fails:\n" + text, e);
			throw e;
		}
	}

	//@Cacheable(value="compiledGroovy") caching the timed cache value is useless IMO
	public CompiledGroovy compile(String text) {
		return TIMEDGOOVYCACHE.get(text);
	}

	public CompiledGroovy compileScript(NuclosScript script) {
		return compile(script.getSource());
	}
	
	private static final TimedCache<String, CompiledGroovy> TIMEDGOOVYCACHE = 
			new TimedCache<String, CompiledGroovy>(new GetGroovyProvider(), 10);
	
	private static class GetGroovyProvider implements LookupProvider<String, CompiledGroovy>{
		@Override
		public CompiledGroovy lookup(String text) {
			return getInstance().actualCompile(text);
		}
	}
	
	private GroovyClassLoader getGroovyClassLoader() {
		final ClassLoader parent = Thread.currentThread().getContextClassLoader();
		try {
			return classLoaderCache.get(parent, new Callable<GroovyClassLoader>() {

				@Override
				public GroovyClassLoader call() throws Exception {
					final ClassLoader pcl = LangUtils.getClassLoaderThatWorksForWebStart();
					return new GroovyClassLoader(pcl);
				}
			});
		} catch (ExecutionException e) {
			throw new IllegalStateException(e);
		}		
	}
	
	public static class CollectableMixin {
		
		public CollectableMixin() {
		}
		
		public static Object getAt(Collectable<?> clct, String name) {
			return clct.getValue(findFieldUID(clct.getEntityUID(), name));
		}

		public static CollectableField propertyMissing(Collectable<?> clct, String name) {
			return clct.getField(findFieldUID(clct.getEntityUID(), name));
		}
		
		private static UID findFieldUID(UID entityUID, String fieldname) {
			if (fieldname == null) {
				return null;
			}
			EntityMeta<?> eMeta = MetaProvider.getInstance().getEntity(entityUID);
			for (FieldMeta<?> fMeta : eMeta.getFields()) {
				if (fieldname.equals(fMeta.getFieldName())) {
					return fMeta.getUID();
				}
			}
			return null;
		}
	}
}
