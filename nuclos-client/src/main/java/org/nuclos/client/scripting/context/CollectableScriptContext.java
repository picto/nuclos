//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.scripting.context;

import java.util.List;

import org.nuclos.api.context.ScriptContext;
import org.nuclos.client.scripting.expressions.EntityExpression;
import org.nuclos.client.scripting.expressions.FieldIdExpression;
import org.nuclos.client.scripting.expressions.FieldPkExpression;
import org.nuclos.client.scripting.expressions.FieldRefObjectExpression;
import org.nuclos.client.scripting.expressions.FieldUidExpression;
import org.nuclos.client.scripting.expressions.FieldValueExpression;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.Collectable;

public class CollectableScriptContext<PK> extends AbstractScriptContext<PK> {

	private final Collectable<PK> c;

	public CollectableScriptContext(Collectable<PK> c) {
		this.c = c;
	}

	@Override
	public Object evaluate(FieldValueExpression exp) {
		return c.getValue(exp.getField());
	}

	@Override
	public PK evaluate(FieldIdExpression exp) {
		return (PK) c.getValueId(exp.getField());
	}

	@Override
	public UID evaluate(FieldUidExpression exp) {
		return (UID) c.getValue(exp.getField());
	}

	@Override
	public ScriptContext evaluate(FieldRefObjectExpression exp) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<ScriptContext> evaluate(EntityExpression exp) {
		throw new UnsupportedOperationException(EntityExpression.class.getName());
	}

	@Override
	public PK evaluate(FieldPkExpression exp) {
		return c.getPrimaryKey();
	}

}