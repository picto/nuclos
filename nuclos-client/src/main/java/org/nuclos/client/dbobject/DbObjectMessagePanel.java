//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.dbobject;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.font.TextAttribute;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

import org.apache.commons.collections15.Closure;
import org.nuclos.client.dbtransfer.DBTransferUtils;
import org.nuclos.client.ui.Errors;
import org.nuclos.client.ui.Icons;
import org.nuclos.client.ui.util.ITableLayoutBuilder;
import org.nuclos.client.ui.util.TableLayoutBuilder;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.dblayer.DbObjectMessage;
import org.nuclos.common2.SpringLocaleDelegate;

import info.clearthought.layout.TableLayout;

public class DbObjectMessagePanel extends JPanel {

	private final DbObjectMessage message;
	
	private final SpringLocaleDelegate locale;
	
	public DbObjectMessagePanel(DbObjectMessage message) {
		this.message = message;
		this.locale = SpringApplicationContextHolder.getBean(SpringLocaleDelegate.class);
		init();
	}
	
	private void init() {
		setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
		boolean hasWarninigs = message.getWarnings().length() > 0;
		String result = hasWarninigs ? 
				locale.getMessage("DbObjectMessagePanel.withErrors", "Mit Fehlern"):
				locale.getMessage("DbObjectMessagePanel.successful", "Erfolgreich");
		ITableLayoutBuilder tbllay = new TableLayoutBuilder(this).columns(TableLayout.FILL, TableLayout.FILL);
		tbllay.newRow(30).add(new JLabel(result), 1, TableLayout.LEFT, TableLayout.CENTER);
		
		JButton btnSaveScript = createButton(new AbstractAction(locale.getMessage("DbObjectMessagePanel.saveScript", "Script speichern"), Icons.getInstance().getIconSaveS16()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveScript();
			}
		});
		
		if (!hasWarninigs) {
			tbllay.add(btnSaveScript, 1, TableLayout.RIGHT, TableLayout.CENTER);
		} else {
			JEditorPane warnings = new JEditorPane();
			warnings.setEditable(false);
			warnings.setContentType("text/html");
			warnings.setText("<html>"+message.getWarnings().toString()+"</html>");
			final JScrollPane scroll = new JScrollPane(warnings, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			scroll.setPreferredSize(new Dimension(600, 300));
			tbllay.newRow().addFullSpan(scroll);
			tbllay.newRow();
			JButton btnSaveLog = createButton(new AbstractAction(locale.getMessage("DbObjectMessagePanel.saveLog", "Log speichern"), Icons.getInstance().getIconSaveS16()) {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveLog();
				}
			});
			tbllay.add(btnSaveLog);
			tbllay.add(btnSaveScript);
		} 
			
	}
	
	private JButton createButton(Action act) {
		JButton result = new JButton(act);
		result.setBorderPainted(false);
		Map<TextAttribute, Object> fontAttr = new HashMap<TextAttribute, Object>(result.getFont().getAttributes());
		fontAttr.put(TextAttribute.UNDERLINE, 1);
		result.setFont(new Font(fontAttr));
		return result;
	}
	
	private void saveLog() {
		String result = message.getWarnings().toString().replace("<br />", "\n");
		result = result.replace("<[^>]*>", "");
		saveTxt(result);
	}
	
	private void saveScript() {
		final StringBuffer sbSql = new StringBuffer();
		org.apache.commons.collections15.CollectionUtils.forAllDo(message.getScript(), new Closure<Object>() {
			@Override
			public void execute(Object element) {
				sbSql.append("<DDL>" + element + "</DDL>\n\n");
			}
		});
		saveTxt(sbSql.toString());
	}
	
	private void saveTxt(String txt) {
		try {
			final JFileChooser filechooser = DBTransferUtils.getFileChooser("TXT", ".txt", false);
			final int iBtn = filechooser.showSaveDialog(this);

			if (iBtn == JFileChooser.APPROVE_OPTION) {
				final File file = filechooser.getSelectedFile();
				if (file != null) {
					String fileName = file.getPath();
					
					if (!fileName.toLowerCase().endsWith(".txt")) {
						fileName += ".txt";
					}

					File outFile = new File(fileName);
					final Writer out = new BufferedWriter(new FileWriter(outFile));
					try {
						out.write(txt);
					}
					finally {
						out.close();
					}
				}
			}
		}
		catch (Exception e) {
			Errors.getInstance().showExceptionDialog(this, e);
		}
	}
}
