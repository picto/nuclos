//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.client.nuclet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuclos.api.UID;
import org.nuclos.api.ui.DesktopItemFactory;
import org.nuclos.api.ui.MenuItem;
import org.nuclos.api.ui.ResultToolbarItemFactory;
import org.nuclos.api.ui.UserSettingsEditor;
import org.nuclos.api.ui.dnd.DropHandlerProvider;
import org.nuclos.api.ui.layout.LayoutComponentFactory;
import org.nuclos.common.collection.CollectionUtils;

// @Component
public class NucletComponentRepository {

	private List<MenuItem> menuItems = new ArrayList<MenuItem>();
	private List<DesktopItemFactory> desktopItemFactories = new ArrayList<DesktopItemFactory>();
	private List<LayoutComponentFactory> layoutComponentFactories = new ArrayList<LayoutComponentFactory>();
	private List<UserSettingsEditor> userSettingsEditors = new ArrayList<UserSettingsEditor>();
	private List<DropHandlerProvider> dropHandlerProvider = new ArrayList<DropHandlerProvider>();
	private Map<UID, List<ResultToolbarItemFactory>> resultToolbarItemFactories = new HashMap<UID, List<ResultToolbarItemFactory>>();
	
	public NucletComponentRepository() {
	}
	
	public void addMenuItem(MenuItem mi) {
		menuItems.add(mi);
	}
	
	public List<MenuItem> getMenuItems() {
		return new ArrayList<MenuItem>(menuItems);
	}
	
	public void addDesktopItemFactory(DesktopItemFactory di) {
		desktopItemFactories.add(di);
	}
	
	public List<DesktopItemFactory> getDesktopItemFactories() {
		return new ArrayList<DesktopItemFactory>(desktopItemFactories);
	}
	
	public void addLayoutComponentFactory(LayoutComponentFactory lc) {
		layoutComponentFactories.add(lc);
	}
	
	public List<LayoutComponentFactory> getLayoutComponentFactories() {
		return new ArrayList<LayoutComponentFactory>(layoutComponentFactories);
	}
	
	public void addUserSettingsEditor(UserSettingsEditor use) {
		userSettingsEditors.add(use);
	}
	
	public List<UserSettingsEditor> getUserSettingsEditors() {
		return new ArrayList<UserSettingsEditor>(userSettingsEditors);
	}
	
	public void addDropHandlerProvider(DropHandlerProvider dhp) {
		dropHandlerProvider.add(dhp);
	}
	
	public List<DropHandlerProvider> getDropHandlerProvider() {
		return dropHandlerProvider;
	}
	
	public void addResultToolbarItemFactory(ResultToolbarItemFactory rti) {
		if (!resultToolbarItemFactories.containsKey(rti.getEntityUid())) {
			resultToolbarItemFactories.put(rti.getEntityUid(), new ArrayList<ResultToolbarItemFactory>());
		}
		resultToolbarItemFactories.get(rti.getEntityUid()).add(rti);
	}
	
	public List<ResultToolbarItemFactory> getResultToolbarItemFactory(UID bo) {
		List<ResultToolbarItemFactory> result = resultToolbarItemFactories.get(null) != null ? CollectionUtils.copyWithoutDublicates(resultToolbarItemFactories.get(null)) : new ArrayList<ResultToolbarItemFactory>();
		if (result == null) {
			result = new ArrayList<ResultToolbarItemFactory>();
		}
		List<ResultToolbarItemFactory> tmp = resultToolbarItemFactories.get(bo);
		if (tmp != null) {
			result.addAll(tmp);
		}
		return result;
	}
}
