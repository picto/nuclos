package org.nuclos.client.customcode;

import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.AbstractCollectableBean;
import org.nuclos.common.collect.collectable.AbstractCollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.DefaultCollectableEntityField;
import org.nuclos.server.customcode.valueobject.CodeVO;

public class CollectableCode extends AbstractCollectableBean<CodeVO,UID> {
	
	public static final UID FIELDNAME_NAME = E.SERVERCODE.name.getUID();
	public static final UID FIELDNAME_DESCRIPTION = E.SERVERCODE.description.getUID();
	public static final UID FIELDNAME_RULESOURCE = E.SERVERCODE.source.getUID();
	public static final UID FIELDNAME_ACTIVE = E.SERVERCODE.active.getUID();
	public static final UID FIELDNAME_DEBUG = E.SERVERCODE.debug.getUID();
	public static final UID FIELDNAME_NUCLET = E.SERVERCODE.nuclet.getUID();
	
	public static CodeCollectableEntity entity = new CodeCollectableEntity();
	
	public CollectableCode(CodeVO cvo) {
		super(cvo);
	}

	@Override
	public UID getId() {
		return getBean().getId();
	}

	@Override
	public UID getPrimaryKey() {
		return getBean().getPrimaryKey();
	}

	@Override
	public int getVersion() {
		return getBean().getVersion();
	}

	@Override
	protected CollectableEntity getCollectableEntity() {
		return this.entity;
	}

	@Override
	public UID getEntityUID() {
		return E.SERVERCODE.getUID();
	}
	
	public static class CodeCollectableEntity extends AbstractCollectableEntity {

		private CodeCollectableEntity() {
			super(E.SERVERCODE.getUID(), "Serverregel");

			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_NAME, String.class, "Name",
					"Name der Regel", 255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, E.SERVERCODE.getUID(), null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_DESCRIPTION, String.class,
					"Beschreibung", "Beschreibung der Regel", 4000, null, true, CollectableField.TYPE_VALUEFIELD, null, null, E.SERVERCODE.getUID(), null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_RULESOURCE, String.class, "Code",
					"Quellcode der Regel", 255, null, false, CollectableField.TYPE_VALUEFIELD, null, null, E.SERVERCODE.getUID(), null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_ACTIVE, Boolean.class,
					"Aktiv?", "Aktivkennzeichen der Regel", null, null, false, CollectableField.TYPE_VALUEFIELD, null, null, E.SERVERCODE.getUID(), null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_DEBUG, Boolean.class,
					"Debug?", "Debug-Flag der Regel", null, null, false, CollectableField.TYPE_VALUEFIELD, null, null, E.SERVERCODE.getUID(), null));
			this.addCollectableEntityField(new DefaultCollectableEntityField(FIELDNAME_NUCLET, String.class,
					"Nuclet", "", 255, null, true, CollectableField.TYPE_VALUEIDFIELD, null, null, E.SERVERCODE.getUID(), null));
		}
	}
	
}
