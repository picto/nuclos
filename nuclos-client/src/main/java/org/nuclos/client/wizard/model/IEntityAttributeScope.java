package org.nuclos.client.wizard.model;

public interface IEntityAttributeScope {
	
	boolean canBeSetToUnique(Attribute a);

}
