package org.nuclos.client.wizard;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.nuclos.client.main.Main;
import org.nuclos.common2.SpringLocaleDelegate;
import org.pietschy.wizard.ButtonBar;
import org.pietschy.wizard.Wizard;

public abstract class NuclosWizardAbstractButtonBar extends ButtonBar {

	private Wizard mywizard;

	private JButton btPrev;
	private JButton btNext;
	private JButton btFinish;
	private JButton btCancel;
//	private JButton btLast;

	private boolean cancelWithESC = false;

	public NuclosWizardAbstractButtonBar(final Wizard wizard) {
		super(wizard);
		this.mywizard = wizard;

		post();
	}

	public void setCancelWithESC() {
		cancelWithESC = true;
	}

	private void post() {
		if (mywizard.getModel() instanceof NuclosWizardStaticModel) {
			final SpringLocaleDelegate localeDelegate = SpringLocaleDelegate.getInstance();
			final NuclosWizardStaticModel model = (NuclosWizardStaticModel) mywizard.getModel();

			btNext.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "nextStep");
			btNext.getActionMap().put("nextStep", model.getActionNextStep());
			btNext.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "last");
			btNext.getActionMap().put("last", model.getActionLast());

			btPrev.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | KeyEvent.SHIFT_DOWN_MASK), "prevStep");
			btPrev.getActionMap().put("prevStep", model.getActionPrevStep());

			btFinish.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "finish");
			btFinish.getActionMap().put("finish", model.getActionFinish());

			btCancel.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "shortcutCancel");
			btCancel.getActionMap().put("shortcutCancel", new AbstractAction() {
				@Override
				public void actionPerformed(final ActionEvent e) {
					shortcutCancel();
				}
			});

			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					btNext.setToolTipText(Main.getKeyboardShortcutDescription(true, false, false, "Y", localeDelegate.getLocale()));
					btPrev.setToolTipText(Main.getKeyboardShortcutDescription(true, true, false, "Y", localeDelegate.getLocale()));
					btFinish.setToolTipText(Main.getKeyboardShortcutDescription(true, false, false, "S", localeDelegate.getLocale()));
				}
			});
		}
	}

	@Override
	protected void layoutButtons(JButton helpButton,
								 final JButton previousButton, JButton nextButton, JButton lastButton,
								 JButton finishButton, JButton cancelButton, JButton closeButton) {
		super.layoutButtons(helpButton, previousButton, nextButton, lastButton,
				finishButton, cancelButton, closeButton);

		this.btPrev = previousButton;
		this.btNext = nextButton;
		this.btFinish = finishButton;
		this.btCancel = cancelButton;
	}

	private void shortcutCancel() {
		if (cancelWithESC && mywizard != null) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					mywizard.cancel();
				}
			});
		}
	}
}
