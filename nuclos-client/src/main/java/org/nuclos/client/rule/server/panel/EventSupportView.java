package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.Border;

import org.nuclos.client.explorer.node.eventsupport.EventSupportTargetTreeNode;
import org.nuclos.client.explorer.node.eventsupport.EventSupportTreeNode;
import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;

public class EventSupportView extends JPanel {

	private EventSupportTreeNode treeEventSupports;
	
	private EventSupportTargetTreeNode treeEventSupportTargets;
	
	private Map<EventSupportActions, AbstractAction> actionMap;
	
	private JSplitPane splitpn;
	
	public EventSupportView(EventSupportTreeNode pTreeEventSupports, EventSupportTargetTreeNode pTreeEventSupportTargets)
	{
		super(new BorderLayout());

		this.treeEventSupports = pTreeEventSupports;
		this.treeEventSupportTargets = pTreeEventSupportTargets;
	}

	public void setActionMap(Map<EventSupportActions, AbstractAction> acts) {
		this.actionMap = acts;
	}
	
	public Map<EventSupportActions, AbstractAction> getActionsMap() {
		return actionMap;
	}

	public void showGui() {
		
		splitpn = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	
		Border b1 = BorderFactory.createMatteBorder(0, 0, 1, 1, Color.LIGHT_GRAY);
		Border b2 = BorderFactory.createMatteBorder(0, 1, 1, 0, Color.LIGHT_GRAY);
		
		splitpn.setTopComponent(
				new EventSupportSourceView(this, b1));
		splitpn.setBottomComponent(
				new EventSupportTargetView(this, b2));
		
		int sliderSize = EventSupportPreferenceHandler.getInstance().getSliderSize(EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_MAIN_SLIDER, "mainSlider");
		if (sliderSize > 0) {
			splitpn.setDividerLocation(sliderSize);
		}
		else {
			splitpn.setResizeWeight(0.5);
			splitpn.setDividerLocation(0.5);			
		}
		
		splitpn.addPropertyChangeListener(new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals("dividerLocation")) {
					if (evt.getNewValue() != null) {
						EventSupportPreferenceHandler.getInstance().addSliderSize(
								EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_MAIN_SLIDER, "mainSlider", (Integer) evt.getNewValue()); 
					}
				}
			}
		});
		
		this.add(splitpn, BorderLayout.CENTER);
	}

	public EventSupportSourceView getSourceViewPanel() {
		return (EventSupportSourceView) splitpn.getTopComponent();
	}
	
	public EventSupportTargetView getTargetViewPanel() {
		return (EventSupportTargetView) splitpn.getBottomComponent();
	}
	
	public EventSupportTreeNode getTreeEventSupports() {
		return treeEventSupports;
	}

	public EventSupportTargetTreeNode getTreeEventSupportTargets() {
		return treeEventSupportTargets;
	}
}
