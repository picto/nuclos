package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.util.Map;

import javax.swing.*;

import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;
import org.nuclos.client.rule.server.model.EventSupportStatePropertiesTableModel;

public class EventSupportStatePropertyPanel extends AbstractEventSupportPropertyPanel {
	
	private EventSupportStatePropertiesTableModel model;
	private Map<EventSupportActions, AbstractAction> actionMapping;
	
	public EventSupportStatePropertyPanel(Map<EventSupportActions, AbstractAction> pActionMapping) {
		this.model = new EventSupportStatePropertiesTableModel(this);
		this.actionMapping = pActionMapping;
		
		setLayout(new BorderLayout());
		
		createPropertiesTable();		
	}

	public void reloadTransitions() {
		getPropertyTable().getColumnModel().getColumn(1).setCellEditor(
				new DefaultCellEditor(new JComboBox(model.getTransitionsAsArray())));
	}
	
	@Override
	public EventSupportPropertiesTableModel getPropertyModel() {
		return model;
	}

	@Override
	public Map<EventSupportActions, AbstractAction> getActionMapping() {
		return this.actionMapping;
	}

	@Override
	protected ActionToolBar[] getActionToolbarMapping() {
		return new ActionToolBar[] {
				new ActionToolBar(EventSupportActions.ACTION_DELETE_STATETRANSITION, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_UP_STATETRANSITION, true),
				new ActionToolBar(EventSupportActions.ACTION_MOVE_DOWN_STATETRANSITION, true) };
	}

	@Override
	protected String getPreferenceNodeName() {
		return EventSupportPreferenceHandler.PREF_NODE_EVENTSUPPORT_TARGET_PROPERTIES_STATEMODEL;
	}
	
}
