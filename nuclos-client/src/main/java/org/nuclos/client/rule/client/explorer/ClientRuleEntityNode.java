package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.client.rule.client.ClientRuleRepository;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleEntityNode implements TreeNode {

	private EntityObjectVO<UID> entity;
	private List<ClientRuleFieldNode> subnodes;
	
	public ClientRuleEntityNode(EntityObjectVO<UID> entity) {
		this.entity = entity;
	}
	
	@Override
	public Object getId() {
		return entity.getPrimaryKey();
	}

	@Override
	public Object getRootId() {
		return null;
	}

	@Override
	public UID getNodeId() {
		return entity.getPrimaryKey();
	}

	@Override
	public UID getEntityUID() {
		return E.ENTITY.getUID();
	}

	@Override
	public String getLabel() {
		return entity.getFieldValue(E.ENTITY.entity);
	}

	@Override
	public String getDescription() {
		return entity.getFieldValue(E.ENTITY.entity);
	}

	@Override
	public String getIdentifier() {
		return entity.getPrimaryKey().getString();
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		if (this.subnodes == null) {
			this.subnodes = getAllFields();
		}
		
		return this.subnodes;
	}

	
	private List<ClientRuleFieldNode> getAllFields() {
		
		List<ClientRuleFieldNode> retVal = new ArrayList<ClientRuleFieldNode>();
			
		for (FieldMeta field : ClientRuleRepository.getInstance().getFieldsByBO(this.entity.getPrimaryKey())) {
			// Only show fields of the current entity
			if (field.getForeignEntity() == null && this.entity.getPrimaryKey().equals(field.getEntity())) {
				retVal.add(new ClientRuleFieldNode(field, this));				
			}
		}
		
		return retVal;
	}

	@Override
	public Boolean hasSubNodes() {
		return this.subnodes != null ? this.subnodes.size() > 0 : Boolean.FALSE;
	}

	@Override
	public void removeSubNodes() {
		if (this.subnodes != null)
			this.subnodes.clear();
	}

	@Override
	@Deprecated
	public void refresh() {}

	@Override
	public boolean implementsNewRefreshMethod() {
		return true;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	public boolean needsParent() {
		return false;
	}
	
	@Override
	 public String toString() {
		return this.getLabel();
	}

}
