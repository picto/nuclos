package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.swing.*;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXTable;
import org.nuclos.client.customcode.ServerCodeCollectController;
import org.nuclos.client.jms.TopicNotificationReceiver;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.rule.server.model.EventSupportCompileErrorsTableModel;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.common.E;
import org.nuclos.common.JMSConstants;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common2.exception.NuclosCompileException.ErrorMessage;

public class EventSupportCompileErrorDialog extends JPanel {

	private static final Logger LOG = Logger.getLogger(EventSupportCompileErrorDialog.class);	
	
	private EventSupportCompileErrorsTableModel model;
	
	private final EventSupportMessageListener ruleMessagelistener = new EventSupportMessageListener();

	public EventSupportCompileErrorDialog(Collection<ErrorMessage> errs) {
		setLayout(new BorderLayout());		
		
		model = new EventSupportCompileErrorsTableModel(
			new ArrayList<ErrorMessage>(errs));
		
		JPanel pnl = new JPanel();
		pnl.setLayout(new BorderLayout());
		pnl.add(getInfoLabel(), BorderLayout.NORTH);
		pnl.add(loadCompilationErrorsTable(), BorderLayout.CENTER);
		add(pnl);
		
		if (!ruleMessagelistener.isSubscripted()) {
			SpringApplicationContextHolder.getBean(TopicNotificationReceiver.class).subscribe(
					JMSConstants.TOPICNAME_RULECOMPILATION, this.ruleMessagelistener);
			ruleMessagelistener.setSubscripted(true);
		}	
	}
	
	private JPanel getInfoLabel() {
		JPanel pnl = new JPanel();
		pnl.setLayout(new BorderLayout());
		
		pnl.setPreferredSize(new Dimension(0, 40));
		
		pnl.add(new JLabel("Das Kompilieren der Regeln ist fehlgeschlagen. Folgende Klassen sind fehlerhaft:"), BorderLayout.CENTER);
		
		return pnl;
	}
	
	public void refresh() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				model.loadMessages(new ArrayList<ErrorMessage>(EventSupportRepository.getInstance().getCompileExceptionMessages(true).values()));
			}
		});
	}
	
	private JPanel loadCompilationErrorsTable() {
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());
		
		final JXTable table = new JXTable(model);
		
		table.setFillsViewportHeight(true);
		table.setRowSelectionAllowed(true);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JScrollPane scrollPane = new JScrollPane(table);
	
		table.setSortable(false);
		
		setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.LIGHT_GRAY));
		tablePanel.add(scrollPane, BorderLayout.CENTER);
		table.getColumnModel().getColumn(0).setMinWidth(40);
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(0).setMaxWidth(60);
		
		table.getColumnModel().getColumn(1).setMinWidth(60);
		table.getColumnModel().getColumn(1).setPreferredWidth(220);
		table.getColumnModel().getColumn(1).setMaxWidth(600);
		
		table.getColumnModel().getColumn(2).setMinWidth(60);
		table.getColumnModel().getColumn(2).setPreferredWidth(80);
		table.getColumnModel().getColumn(2).setMaxWidth(200);
		
		table.getColumnModel().getColumn(3).setPreferredWidth(250);
		
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					if (e.getSource() != null && e.getSource() instanceof JXTable) {
						JXTable table = (JXTable) e.getSource();
						EventSupportCompileErrorsTableModel model = (EventSupportCompileErrorsTableModel) table.getModel();
						ErrorMessage row = model.getRow(table.getSelectedRow());
						if (row.getUid() != null) {
							try {
								final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
								final ServerCodeCollectController rcc = factory.newServerCodeCollectController(E.SERVERCODE.getUID(), null);
								rcc.runViewSingleCollectableWithId(row.getUid());
							} catch (Exception exp) {
								// ignore
							}
						}
					}
				}
			}
		});
		
		return tablePanel;
	}

	private class EventSupportMessageListener implements MessageListener {
		private boolean isSubscripted = false;
		
		
		public boolean isSubscripted() {
			return isSubscripted;
		}


		public void setSubscripted(boolean isSubscripted) {
			this.isSubscripted = isSubscripted;
		}
		
		@Override
		public void onMessage(javax.jms.Message msg) {
			
			try {
				if (msg != null) {
					if (msg instanceof TextMessage) {
						refresh();
					}
				}
				 
			} catch (Exception e) {
				LOG.error("Cannot refresh list of compile errors", e);
			}
		}

	};
}
