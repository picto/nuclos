package org.nuclos.client.rule.client.explorer;

import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleRootExplorerNode extends
		ExplorerNode<ClientRuleRootNode> {

	public ClientRuleRootExplorerNode(TreeNode treenode) {
		super(treenode);
	}
	
}
