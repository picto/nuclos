package org.nuclos.client.rule.client.explorer;

import org.nuclos.client.explorer.ExplorerNode;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleFieldRuleExplorerNode extends
		ExplorerNode<ClientRuleFieldRuleNode> {

	protected ClientRuleFieldRuleExplorerNode(TreeNode treenode) {
		super(treenode);
	}

}
