package org.nuclos.client.rule.server;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import org.nuclos.client.ui.Icons;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.server.customcode.ejb3.CodeFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;


public class AutomaticRuleCompilationButtonFactory {
	
	private final List<Reference<AutomaticRuleCompilationButton>> buttons = 
			new LinkedList<Reference<AutomaticRuleCompilationButton>>();
	
	private boolean init = false;
	
	private Icon enabled;
	
	private Icon disabled;
	
	private String compile;
	
	private String nocompile;
	
	// Spring injection
	
	@Autowired
	private SpringLocaleDelegate localDelegate;
	
	@Autowired
	private CodeFacadeRemote codeFacade;
	
	// end of Spring injection
	
	public AutomaticRuleCompilationButtonFactory() {
	}
	
	void init() {
		if (!init) {
			enabled = Icons.getInstance().getIconPlus16();
			disabled = Icons.getInstance().getIconMinus16();
			compile = localDelegate.getMessage("RuleCollectController.compile", "RuleCollectController.compile");
			nocompile = localDelegate.getMessage("RuleCollectController.nocompile", "RuleCollectController.nocompile");
			init = true;
		}
	}
	
	public AutomaticRuleCompilationButton newMenuItem() {
		final AutomaticRuleCompilationButton result = new AutomaticRuleCompilationButton(this);
		buttons.add(new WeakReference<AutomaticRuleCompilationButton>(result));
		return result;
	}
	
	public boolean isAutomaticRuleCompilation() {
		return codeFacade.isAutomaticRuleCompilation();
	}
	
	public void setAutomaticRuleCompilation(boolean ruleCompilation) {
		final boolean automaticRuleCompilation = isAutomaticRuleCompilation();
		if (automaticRuleCompilation != ruleCompilation) {
			codeFacade.setAutomaticRuleCompilation(ruleCompilation);
			
			final Iterator<Reference<AutomaticRuleCompilationButton>> it = buttons.iterator();
			while (it.hasNext()) {
				final Reference<AutomaticRuleCompilationButton> ref = it.next();
				final AutomaticRuleCompilationButton b = ref.get();
				if (b == null) {
					it.remove();
				}
				else {
					b.toggle(ruleCompilation);
				}
			}
		}
	}
	
	SpringLocaleDelegate getLocaleDelegate() {
		return localDelegate;
	}
	
	Icon getDisabledIcon() {
		init();
		return disabled;
	}
	
	Icon getEnabledIcon() {
		init();
		return enabled;
	}
	
	String getCompileString() {
		init();
		return compile;
	}
	
	String getNocompileString() {
		init();
		return nocompile;
	}
	
}
