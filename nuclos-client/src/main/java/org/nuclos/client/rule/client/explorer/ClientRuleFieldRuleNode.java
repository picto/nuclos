package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleFieldRuleNode implements TreeNode {

	private String rule;
	
	public ClientRuleFieldRuleNode(String rule) {
		this.rule = rule;
	}
	
	@Override
	public Object getId() {
		return this.rule;
	}

	@Override
	public Object getRootId() {
		return null;
	}

	@Override
	public UID getNodeId() {
		return null;
	}

	@Override
	public UID getEntityUID() {
		return null;
	}

	@Override
	public String getLabel() {
		return this.rule;
	}

	@Override
	public String getDescription() {
		return this.rule;
	}

	@Override
	public String getIdentifier() {
		return this.rule;
	}

	@Override
	public List<? extends TreeNode> getSubNodes() {
		return new ArrayList<TreeNode>();
	}

	@Override
	public Boolean hasSubNodes() {
		return Boolean.FALSE;
	}

	@Override
	public void removeSubNodes() {}

	@Override
	@Deprecated
	public void refresh() {}

	@Override
	public boolean implementsNewRefreshMethod() {
		return false;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	public boolean needsParent() {
		return false;
	}

}
