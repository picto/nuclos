package org.nuclos.client.rule.server.panel;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import org.nuclos.client.customcode.ServerCodeCollectController;
import org.nuclos.client.rule.server.EventSupportActionHandler.EventSupportActions;
import org.nuclos.client.rule.server.EventSupportPreferenceHandler;
import org.nuclos.client.rule.server.EventSupportRepository;
import org.nuclos.client.rule.server.model.EventSupportPropertiesTableModel;
import org.nuclos.client.ui.collect.CollectControllerFactorySingleton;
import org.nuclos.common.E;
import org.nuclos.server.eventsupport.valueobject.EventSupportSourceVO;
import org.nuclos.server.eventsupport.valueobject.EventSupportVO;

public abstract class AbstractEventSupportPropertyPanel extends JPanel {
	
	private static class EventSupportTcmListener implements TableColumnModelListener {
		
		private final String preferenceNodeName;
		
		private EventSupportTcmListener(String preferenceNodeName) {
			this.preferenceNodeName = preferenceNodeName;
		}
		
		@Override
		public void columnSelectionChanged(ListSelectionEvent e) {}
		
		@Override
		public void columnRemoved(TableColumnModelEvent e) {}
		
		@Override
		public void columnMoved(TableColumnModelEvent e) {}
		
		@Override
		public void columnMarginChanged(ChangeEvent e) {
			DefaultTableColumnModel colModel = (DefaultTableColumnModel) e.getSource();
			Enumeration<TableColumn> columns = colModel.getColumns();
			while (columns.hasMoreElements()) {
				TableColumn col = columns.nextElement();
				EventSupportPreferenceHandler.getInstance().addColumnWidth(
						preferenceNodeName, col.getHeaderValue().toString(), col.getWidth());
			}
		}
		
		@Override
		public void columnAdded(TableColumnModelEvent e) {}
	
	}

	protected abstract EventSupportPropertiesTableModel getPropertyModel();
	protected abstract Map<EventSupportActions, AbstractAction> getActionMapping();
	protected abstract ActionToolBar[] getActionToolbarMapping();
	protected abstract String getPreferenceNodeName();
	
	private JTable propertyTable;
	
	protected void createPropertiesTable() {
		final EventSupportPropertiesTableModel propertyModel = getPropertyModel();

		final JTable table = new JTable(propertyModel);
		final List<JButton> lstOfEnablabeTBButtons = new ArrayList<JButton>();
		final JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		final Map<EventSupportActions, AbstractAction> actionsMap = getActionMapping();
		
		propertyModel.addTableRowMoveListener(new TableRowMoveListener() {

			@Override
			public void rowSelected(int rowId) {
				table.setRowSelectionInterval(rowId, rowId);
			}
		});
		toolBar.setBorderPainted(false);	
		if (getActionToolbarMapping() != null) {
			for (ActionToolBar actionInMap: getActionToolbarMapping()) {
				final JButton btnActionElement = new JButton(actionsMap.get(actionInMap.getAction()));
				if (actionInMap.isEditableInToolbar()) {
					btnActionElement.setEnabled(false);
					lstOfEnablabeTBButtons.add(btnActionElement);
				}
				toolBar.add(btnActionElement);				
			}
		}
		table.setFillsViewportHeight(true);
		table.setRowSelectionAllowed(true);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()) {
					boolean enableButtons = false;
					if (table.getSelectedRow() >= 0) {
						enableButtons = true;
					}
					for (JButton tbButton: lstOfEnablabeTBButtons) {
						toolBar.getComponent(toolBar.getComponentIndex(tbButton)).setEnabled(enableButtons);
					}
				}
			}
		});
		table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
		table.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					JTable table = (JTable) e.getSource();
					EventSupportPropertiesTableModel model = (EventSupportPropertiesTableModel) table.getModel();
					EventSupportVO entryByRowIndex = model.getEntryByRowIndex(table.getSelectedRow());
					
					try {
						EventSupportSourceVO eventSupportByClassname = 
								EventSupportRepository.getInstance().getEventSupportByClassname(entryByRowIndex.getEventSupportClass());
						
						if (eventSupportByClassname != null && entryByRowIndex != null && eventSupportByClassname.getId() != null) {
							
							final CollectControllerFactorySingleton factory = CollectControllerFactorySingleton.getInstance();
							final ServerCodeCollectController rcc = factory.newServerCodeCollectController(E.SERVERCODE.getUID(), null);
							rcc.runViewSingleCollectableWithId(eventSupportByClassname.getId());					
						}
					} catch (Exception ex) {
						//...
					}
				}
			}
		});
		
		final JScrollPane scrollPane = new JScrollPane(table);
		setBorder(BorderFactory.createMatteBorder(1, 1, 0, 1, Color.LIGHT_GRAY));
		add(scrollPane, BorderLayout.CENTER);
		
		if (getActionToolbarMapping() != null && getActionToolbarMapping().length > 0) {
			add(toolBar, BorderLayout.EAST);
		}
		setPreferredSize(new Dimension(0, 250));
	
		// Store resized width into preferences
		addColumnResizeListener(table);
		
		this.propertyTable = table;
	}

	
	private void addColumnResizeListener(JTable table) {
		for (int idx=0; idx < table.getColumnCount(); idx++) {
			String columnName = getPropertyModel().getColumnName(idx);
			int newColSize = EventSupportPreferenceHandler.getInstance().getColumnWidth(
					getPreferenceNodeName(), columnName);
			table.getColumnModel().getColumn(idx).setPreferredWidth(newColSize);
		}

		table.getColumnModel().addColumnModelListener(new EventSupportTcmListener(getPreferenceNodeName()));
	}
	
	public JTable getPropertyTable() {
		return this.propertyTable;
	}
	
	/**
	 * Must be static cause of gc issues. (tp)
	 */
	public static class ActionToolBar {
		
		private EventSupportActions aAction;
		private boolean bEditableInToolbar;
		
		public ActionToolBar (EventSupportActions pAction, boolean pEditableInToolbar) {
			this.aAction = pAction;
			this.bEditableInToolbar = pEditableInToolbar;
		}
		
		public EventSupportActions getAction() {
			return this.aAction;
		}
		
		public boolean isEditableInToolbar() {
			return this.bEditableInToolbar;
		}
	}

	/**
	 * Must be static cause of gc issues. (tp)
	 */
	public static interface TableRowMoveListener {
		
		void rowSelected(int rowId);
		
	}
}
