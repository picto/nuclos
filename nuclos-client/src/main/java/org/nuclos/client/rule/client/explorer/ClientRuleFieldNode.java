package org.nuclos.client.rule.client.explorer;

import java.util.ArrayList;
import java.util.List;

import org.nuclos.client.masterdata.MasterDataCache;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;
import org.nuclos.server.navigation.treenode.TreeNode;

public class ClientRuleFieldNode implements TreeNode {

	private FieldMeta field;
	private ClientRuleEntityNode entityNode;
	
	public ClientRuleFieldNode(FieldMeta field, ClientRuleEntityNode entityNode) {
		this.field = field;
		this.entityNode = entityNode;
	}
	
	@Override
	public Object getId() {
		return field.getUID();
	}

	@Override
	public Object getRootId() {
		return null;
	}

	@Override
	public UID getNodeId() {
		return field.getUID();
	}

	@Override
	public UID getEntityUID() {
		return E.ENTITYFIELD.getUID();
	}

	@Override
	public String getLabel() {
		return field.getFieldName();
	}

	@Override
	public String getDescription() {
		return field.getFieldName();
	}

	@Override
	public String getIdentifier() {
		return field.getUID().getString();
	}

	@Override
	public List<ClientRuleFieldRuleNode> getSubNodes() {
		List<ClientRuleFieldRuleNode> retVal = new ArrayList<ClientRuleFieldRuleNode>();
		
		MasterDataVO<UID> mdvo = MasterDataCache.getInstance().get(E.ENTITYFIELD.getUID(), field.getUID());
		
		if (mdvo.getFieldValue(E.ENTITYFIELD.ruleBackgroundColor) != null)
			retVal.add(new ClientRuleFieldRuleNode(mdvo.getFieldValue(E.ENTITYFIELD.ruleBackgroundColor)));
		if (mdvo.getFieldValue(E.ENTITYFIELD.ruleCalculation) != null)
			retVal.add(new ClientRuleFieldRuleNode(mdvo.getFieldValue(E.ENTITYFIELD.ruleCalculation)));
		
		return retVal;
	}

	@Override
	public Boolean hasSubNodes() {
		return Boolean.FALSE;
	}

	@Override
	public void removeSubNodes() {
		
	}

	@Override
	@Deprecated
	public void refresh() {}

	@Override
	public boolean implementsNewRefreshMethod() {
		return true;
	}

	@Override
	public TreeNode refreshed() throws CommonFinderException {
		return this;
	}

	@Override
	public boolean needsParent() {
		return false;
	}

	public FieldMeta getField() {
		return this.field;
	}
	
	public  ClientRuleEntityNode getEntityNode() {
		return this.entityNode;
	}
	
	@Override
	 public String toString() {
		return this.getLabel();
	}
}
