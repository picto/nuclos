package org.nuclos.server.masterdata.ejb3;

import javax.annotation.security.RolesAllowed;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.TruncatableCollection;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

public interface CommonMasterDataFacade {

	/**
	 * method to get master data records for a given entity and search condition
	 * 
	 * §postcondition result != null
	 * §todo restrict permissions by entity name
	 * 
	 * @param entityUID UID of the entity to get master data records for
	 * @param cond search condition
	 * @return TruncatableCollection&lt;MasterDataVO&gt; collection of master data value objects
	 */
	@RolesAllowed("Login")
	<PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(
		UID entityUID, CollectableSearchCondition cond, boolean bAll);
	
	@RolesAllowed("Login")
	<PK> TruncatableCollection<MasterDataVO<PK>> getMasterData(
		EntityMeta<PK> entity, CollectableSearchCondition cond, boolean bAll);
	
}
