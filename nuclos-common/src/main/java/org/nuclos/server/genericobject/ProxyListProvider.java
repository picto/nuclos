package org.nuclos.server.genericobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.ParameterProvider;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Pair;

@SuppressWarnings("serial")
public class ProxyListProvider implements Serializable {
	private int chunkSize;
	private Map<UID, Collection<UID>> essentialFields;
	private String countResultList;
	
	public ProxyListProvider(ParameterProvider parProvider, IMetaProvider mdProvider) {
		fetchChunkSize(parProvider);
		fetchEssentialFields(parProvider, mdProvider);
		countResultList = parProvider.getValue(ParameterProvider.COUNT_RESULT_LIST);
	}
	
	private void fetchChunkSize(ParameterProvider parProvider) {
		chunkSize = -1;
		String value = parProvider.getValue(ParameterProvider.DATA_CHUNK_SIZE);
		if (value != null) try {
			chunkSize = Integer.parseInt(value);
		} catch (NumberFormatException nfe) {
		}		
	}
	
	public int overrideChunksize(int defaultSize) {
		if (chunkSize > 0) return chunkSize;
		return defaultSize;
	}
	
	public void addEssentialFields(Collection<UID> fields, UID entity) {
		Collection<UID> lstEssentialFields = essentialFields.get(entity);
		if (lstEssentialFields == null) return;
		for (UID essentialField : lstEssentialFields) {
			if (!fields.contains(essentialField)) {
				fields.add(essentialField);
			}
		}
	}
	
	private void fetchEssentialFields(ParameterProvider parProvider, IMetaProvider mdProvider) {
		essentialFields = new HashMap<UID, Collection<UID>>();
		String value = parProvider.getValue(ParameterProvider.ESSENTIAL_ENTITY_FIELDS);
		if (value != null) {
			List<Pair<String, String>> lstEssentialFields = getEssentialFields(value);
			for (Pair<String, String> essentialField : lstEssentialFields) {
				FieldMeta<?> efmdvo = mdProvider.getEntityField(new UID(essentialField.y));
				if (efmdvo == null) continue;
				EntityMeta<?> emdvo = mdProvider.getEntity(efmdvo.getEntity());
				Collection<UID> lstFields = essentialFields.get(emdvo.getUID());
				if (lstFields == null) {
					essentialFields.put(emdvo.getUID(), lstFields = new HashSet<UID>());
				}
				lstFields.add(efmdvo.getUID());
			}
		}
	}
	
	private static List<Pair<String, String>> getEssentialFields(String s) {
		List<Pair<String, String>> lstEssentialFields = new ArrayList<Pair<String,String>>();
		StringTokenizer st = new StringTokenizer(s, ",");
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			int n = token.indexOf('.');
			if (n > -1) {
				String entity = token.substring(0, n);
				String field = token.substring(n + 1);
				lstEssentialFields.add(new Pair<String, String>(entity, field));
			}
		}
		return lstEssentialFields;
	}
	
	public boolean isCountResultListOff() {
		return "off".equals(countResultList);
	}
}
