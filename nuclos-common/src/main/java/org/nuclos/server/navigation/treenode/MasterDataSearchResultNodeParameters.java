package org.nuclos.server.navigation.treenode;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.CollectableSearchCondition;
import org.nuclos.common2.LangUtils;

public class MasterDataSearchResultNodeParameters implements Serializable {
	final private UID sEntity;
	final private CollectableSearchCondition cond;
	final private String sFilterName;
	
	public MasterDataSearchResultNodeParameters(UID sEntity,
			CollectableSearchCondition cond, String sFilterName) {
		super();
		this.sEntity = sEntity;
		this.cond = cond;
		this.sFilterName = sFilterName;
	}
	
	public UID getsEntity() {
		return sEntity;
	}
	
	public CollectableSearchCondition getCond() {
		return cond;
	}
	
	public String getsFilterName() {
		return sFilterName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MasterDataSearchResultNodeParameters) {
			MasterDataSearchResultNodeParameters that = (MasterDataSearchResultNodeParameters) obj;
			return LangUtils.equal(sEntity, that.sEntity) && LangUtils.equal(cond, that.cond) && LangUtils.equal(sFilterName, that.sFilterName);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(sEntity, cond, sFilterName);
	}
	
}
