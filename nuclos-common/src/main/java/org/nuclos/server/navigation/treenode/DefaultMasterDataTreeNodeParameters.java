package org.nuclos.server.navigation.treenode;

import java.io.Serializable;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class DefaultMasterDataTreeNodeParameters<PK> implements Serializable {
	
	private final UID sEntity;
	private final PK id;
	private final UID uidNode;
	private final Object idRoot;
	private final String label;
	private final String description;

	
	public DefaultMasterDataTreeNodeParameters(UID sEntity, PK id, UID uidNode, Object idRoot, String label, String description) {
		this.sEntity = sEntity;
		this.id = id;
		this.uidNode = uidNode;
		this.idRoot = idRoot;
		this.label = label;
		this.description = description;
	}
	
	public UID getsEntity() {
		return sEntity;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof DefaultMasterDataTreeNodeParameters) {
			DefaultMasterDataTreeNodeParameters that = (DefaultMasterDataTreeNodeParameters) obj;
			return LangUtils.equal(sEntity, that.sEntity) && LangUtils.equal(id, that.id) 
					&& LangUtils.equal(uidNode, that.uidNode) && LangUtils.equal(idRoot, that.idRoot);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(sEntity, id, uidNode, idRoot);
	}

	public PK getId() {
		return id;
	}

	public UID getUidNode() {
		return uidNode;
	}

	public Object getIdRoot() {
		return idRoot;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}
	
}
