//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common2.security;

import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;
import org.nuclos.common.NuclosFatalException;

/**
 * Calculate a crypto hash on byte arrays.
 *  
 * @author Thomas Pasch
 * @since Nuclos 3.14.18, 3.15.18, 4.0.15
 */
public class MessageDigest {
	
	private MessageDigest() {
		// Never invoked.
	}
	
	public static byte[] digest(String algo, byte[] content) throws NuclosFatalException {
		try {
			final java.security.MessageDigest digest = java.security.MessageDigest.getInstance(algo);
			digest.update(content);
			return digest.digest();
		} catch (NoSuchAlgorithmException e) {
			throw new NuclosFatalException(e);
		}
	}
	
	public static String digestAsHex(String algo, byte[] content) throws NuclosFatalException {
		return new String(bytesToHex(digest(algo, content)));
	}

	public static String digestAsBase64(String algo, byte[] content) throws NuclosFatalException {
		return Base64.encodeBase64String(digest(algo, content));
	}

	// http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
	private static final char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static char[] bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return hexChars;
	}

}
