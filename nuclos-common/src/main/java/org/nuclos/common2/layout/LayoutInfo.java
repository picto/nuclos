package org.nuclos.common2.layout;

import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;
import org.xml.sax.InputSource;

public class LayoutInfo {
	private final UID layoutUID;
	private final String sLayoutML;
	private final boolean restrictionsMustIgnoreGroovyRules;
	private final Collection<UID> entities;
	private final boolean doesLayoutChangesWithProcess;
	
	public LayoutInfo(UID layoutUID, String sLayoutML, boolean restrictionsMustIgnoreGroovyRules, Collection<UID> entities,
			boolean doesLayoutChangesWithProcess) {
		this.layoutUID = layoutUID;
		this.sLayoutML = sLayoutML;
		this.restrictionsMustIgnoreGroovyRules = restrictionsMustIgnoreGroovyRules;
		this.entities = entities;
		this.doesLayoutChangesWithProcess = doesLayoutChangesWithProcess;
	}

	public UID getLayoutUID() {
		return layoutUID;
	}

	public String getLayoutML() {
		return sLayoutML;
	}
	
	public boolean isRestrictionsMustIgnoreGroovyRules() {
		return restrictionsMustIgnoreGroovyRules;
	}
	
	public Collection<UID> getEntities() {
		return Collections.unmodifiableCollection(entities);
	}
	
	public boolean doesLayoutChangesWithProcess() {
		return doesLayoutChangesWithProcess;
	}
	
	public InputSource layoutInSource() {
		return new InputSource(new StringReader(sLayoutML));
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LayoutInfo) {
			LayoutInfo that = (LayoutInfo) obj;
			return LangUtils.equal(layoutUID, that.layoutUID) &&
					LangUtils.equal(restrictionsMustIgnoreGroovyRules, that.restrictionsMustIgnoreGroovyRules);
			
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return LangUtils.hash(layoutUID, restrictionsMustIgnoreGroovyRules);
	}
	
}
