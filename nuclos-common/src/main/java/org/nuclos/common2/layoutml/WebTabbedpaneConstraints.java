package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebTabbedpaneConstraints implements IWebConstraints {
	private final String title;
	private final boolean enabled;
	private final String internalname;

	public WebTabbedpaneConstraints(Attributes attributes) {
		this.title = attributes.getValue(ATTRIBUTE_TITLE);
		this.enabled = !ATTRIBUTEVALUE_NO.equals(attributes.getValue(ATTRIBUTE_ENABLED));
		this.internalname = attributes.getValue(ATTRIBUTE_INTERNALNAME);
	}
	
	public String getTitle() {
		return title;
	}

	public String getInternalname() {
		return internalname;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	@Override
	public String toString() {
		return "Title:" + title + " Enabled:" + enabled + " IntnlName:" + internalname;
	}
	
	@Override
	public boolean isQuasiEmpty() {
		return false;
	}

	@Override
	public void setSize(int type, int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
}
