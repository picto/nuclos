package org.nuclos.common2.layoutml;

import java.util.HashMap;
import java.util.Map;

import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public class NuclosSubformColumn implements LayoutMLConstants, ISupportsValueListProvider {
	
	
	private final Map<String, String> mpProperties = new HashMap<String, String>();

	protected void addProperty(String key, String value) {
		mpProperties.put(key, value);
	}
	
	protected void addProperty(String key, Attributes attributes) {
		String value = attributes.getValue(key);
		if (value != null) {
			mpProperties.put(key, value);			
		}
	}
	
	public Map<String, String> getProperties() {
		return mpProperties;
	}

	
	private final UID name;
	private final String label;
	
	private final String visible;
	private final String enabled;
	
	private final UID subform;
	private WebValueListProvider valueListProvider;
	
	NuclosSubformColumn(Attributes attributes, UID subform) {
		name = UID.parseUID(attributes.getValue(ATTRIBUTE_NAME));
		label = attributes.getValue(ATTRIBUTE_LABEL);
		visible = attributes.getValue(ATTRIBUTE_VISIBLE);
		enabled = attributes.getValue(ATTRIBUTE_ENABLED);
		this.subform = subform;
	}
	
	public UID getName() {
		return name;
	}
	
	public String getLabel() {
		return label;
	}

	public boolean isVisible() {
		return !ATTRIBUTEVALUE_NO.equals(visible);
	}

	public boolean isEnabled() {
		return !ATTRIBUTEVALUE_NO.equals(enabled);
	}
	
	public UID getSubform() {
		return subform;
	}

	@Override
	public WebValueListProvider getValueListProvider() {
		return valueListProvider;
	}

	@Override
	public void setValueListProvider(WebValueListProvider valueListProvider) {
		this.valueListProvider = valueListProvider;
	}
	
	@Override
	public String toString() {
		return "SubColumn=" + label;
	}
}
