package org.nuclos.common2.layoutml;

import org.xml.sax.Attributes;

public class WebSplitPane extends AbstractWebContainer {

	public WebSplitPane(Attributes attributes) {
		super(attributes);
		addProperty(ATTRIBUTE_ORIENTATION, attributes);
		addProperty(ATTRIBUTE_EXPANDABLE, attributes);
		addProperty(ATTRIBUTE_DIVIDERSIZE, attributes);
		addProperty(ATTRIBUTE_RESIZEWEIGHT, attributes);
		addProperty(ATTRIBUTE_CONTINUOUSLAYOUT, attributes);
	}

	@Override
	public String getTitle() {
		return getName();
	}

	@Override
	public String getType() {
		return "SPLITPANE";
	}

	@Override
	protected void setWebTableLayout(WebTableLayout layout) {
		
	}

}
