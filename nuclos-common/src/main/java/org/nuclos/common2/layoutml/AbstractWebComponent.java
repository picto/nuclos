package org.nuclos.common2.layoutml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.nuclos.common.UID;
import org.xml.sax.Attributes;

public abstract class AbstractWebComponent implements IWebComponent, LayoutMLConstants, Comparable<AbstractWebComponent> {
	protected static final Logger LOG = Logger.getLogger(AbstractWebComponent.class);
	
	protected final String enabled;
	protected final String nextfocusfield;
	protected final String nextcomponent;
	private final Map<String, String> mpProperties = new HashMap<String, String>();
	
	private boolean forceDisable = false;
	private IWebConstraints constraints = null;
	private IWebContainer container = null;
	private Integer tabindex = null;
	private String background;

	
	protected AbstractWebComponent(Attributes attributes) {
		enabled = attributes.getValue(ATTRIBUTE_ENABLED);
		nextfocusfield = attributes.getValue(ATTRIBUTE_NEXTFOCUSFIELD);
		nextcomponent = attributes.getValue(ATTRIBUTE_NEXTFOCUSCOMPONENT);
	}
	
	public final boolean isEnabled() {
		if (ATTRIBUTEVALUE_NO.equals(enabled) || forceDisable) {
			return false;
		}
		if (container != null && !container.isEnabled()) {
			return false;
		}
		if (constraints instanceof WebTabbedpaneConstraints) {
			return ((WebTabbedpaneConstraints)constraints).isEnabled();
		}
		return true;
	}
	
	public final String getNextfocusfield() {
		return nextfocusfield;
	}
	
	public final String getNextcomponent() {
		return nextcomponent;
	}
	
	protected void addProperty(String key, String value) {
		mpProperties.put(key, value);
	}
	
	protected void addProperty(String key, Attributes attributes) {
		String value = attributes.getValue(key);
		if (value != null) {
			mpProperties.put(key, value);			
		}
	}

	@Override
	public Map<String, String> getProperties() {
		return Collections.unmodifiableMap(mpProperties);
	}
	
	public void setForceDisable(boolean b) {
		forceDisable = b;
	}
	
	public boolean isVisible() {
		return true;
	}
	
	public boolean isMemo() {
		return false;
	}

	public boolean isJustLabel() {
		return false;
	}

	@Override
	public IWebConstraints getConstraints() {
		return constraints;
	}

	public void setConstraints(IWebConstraints constraints) {
		this.constraints = constraints;
	}
	
	public void setSize(int type, Attributes attributes) {
		if (this.constraints != null) {
			final int iWidth = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_WIDTH, 0);
			final int iHeight = MyLayoutConstants.getIntValue(attributes, ATTRIBUTE_HEIGHT, 0);

			this.constraints.setSize(type, iWidth, iHeight);
		}
	}

	@Override
	public IWebContainer getContainer() {
		return container;
	}

	public void setContainer(IWebContainer container) {
		this.container = container;
	}
	
	public UID getUID() {
		return null;
	}
	
	public Integer getTabIndex() {
		return tabindex;
	}

	public void setTabindex(Integer tabindex) {
		this.tabindex = tabindex;
	}

	public void setBackground(String background) {
		this.background = background;
	}
	
	public String getBackground() {
		return background;
	}

	@Override
	public int compareTo(AbstractWebComponent other) {
		
		IWebConstraints constraints1 = getConstraints();
		IWebConstraints constraints2 = other.getConstraints();
		
		if (constraints1 instanceof WebTableLayoutConstraints && constraints2 instanceof WebTableLayoutConstraints) {
			WebTableLayoutConstraints webTableLayoutConstraints1 = (WebTableLayoutConstraints)constraints1;
			WebTableLayoutConstraints webTableLayoutConstraints2 = (WebTableLayoutConstraints)constraints2;
			
			Integer row1 = webTableLayoutConstraints1.getRow1();
			Integer row2 = webTableLayoutConstraints2.getRow1();
			
			if (row1 != null && !row1.equals(row2)) {
				return row1.compareTo(row2);
			}

			Integer col1 = webTableLayoutConstraints1.getColumn1();
			Integer col2 = webTableLayoutConstraints2.getColumn1();
			
			return col1.compareTo(col2);
		}
		
		//TODO: Implement a nice sorting rule if there are no constraints.
		return 0;
	}
}
