package org.nuclos.build;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.NoWorkTreeException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

public class JGitBuildInfo implements IBuildInfo {
	
	private static final String ENCODING = "UTF-8";

	private final File wc;
	
	private final File gitDir;
	
	private final FileRepository repository;
	
	private final Git git;

	public JGitBuildInfo(File wc) throws IOException {
		if (!wc.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + wc);
		}
		gitDir = new File(wc, ".git");
		if (!gitDir.isDirectory()) {
			throw new IllegalArgumentException("No git directory: " + gitDir);
		}
		// this.wc = new File(wc, "/.");
		this.wc = wc;
		
		// see http://wiki.eclipse.org/JGit/User_Guide
		final FileRepositoryBuilder frb = new FileRepositoryBuilder();
		repository = (FileRepository) frb.setGitDir(gitDir).readEnvironment().findGitDir().build();
		git = new Git(repository);
	}

	public void info(File infoFile) throws IOException {
		final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(infoFile), ENCODING));
		final ListBranchCommand listBranchCmd = git.branchList();
		final LogCommand logCmd = git.log();
		
		listBranchCmd.setListMode(ListMode.ALL);
		logCmd.setMaxCount(3);
		try {
			out.write("repository: " + repository);
			out.newLine();
			out.write("current branch: " + repository.getBranch());
			out.newLine();
			out.write("repository state: " + repository.getRepositoryState());
			out.newLine();
			out.newLine();
			
			final List<Ref> refs = listBranchCmd.call();
			out.write("git branch list:\n");
			for (Ref r: refs) {
				out.write(r.toString());
				out.newLine();
			}
			out.newLine();
			
			final Iterable<RevCommit> iter = logCmd.call();
			out.write("log of last 3 commits:\n");
			for (Iterator<RevCommit> it = iter.iterator(); it.hasNext();) {
				final RevCommit rc = it.next();
				out.write(rc.toString());
				out.newLine();
			}
			out.newLine();
		}
		catch (GitAPIException e) {
			throw new IOException(e);
		}
		finally {
			out.close();
		}
	}

	public void status(File statusFile) throws IOException {
		final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(statusFile), ENCODING));
		final StatusCommand statusCmd = git.status();
		
		try {
			final Status status = statusCmd.call();
			out.write("Status: \n");
			write(out, "Added:", status.getAdded());
			write(out, "Changed:", status.getChanged());
			write(out, "Modified:", status.getModified());
			write(out, "Removed:", status.getRemoved());
			write(out, "Untracked:", status.getUntracked());
			write(out, "Untracked dirs:", status.getUntrackedFolders());
			write(out, "Conflicting:", status.getConflicting());
			
			out.newLine();
		}
		catch (NoWorkTreeException e) {
			throw new IOException(e);
		}
		catch (GitAPIException e) {
			throw new IOException(e);
		}
		finally {
			out.close();
		}
	}
	
	private void write(BufferedWriter out, String title, Set<String> set) throws IOException {
		if (set != null && !set.isEmpty()) {
			out.write(title);
			out.newLine();
			for (String s: set) {
				out.write(s);
				out.newLine();
			}
			out.newLine();
		}
	}

	public void dispose() {
		repository.close();
	}

}
