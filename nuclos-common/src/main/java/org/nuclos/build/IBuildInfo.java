package org.nuclos.build;

import java.io.File;
import java.io.IOException;

public interface IBuildInfo {
	
	void status(File statusFile) throws IOException;
	
	void info(File infoFile) throws IOException;
	
	void dispose();

}
