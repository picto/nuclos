//Copyright (C) 2012  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.http;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.DefaultHttpRoutePlanner;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;
import org.nuclos.common.tls.CustomSecureProtocolSocketFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;

/**
 * A Spring BeanFactory that creates a customized (apache httpcomponents) HttpClient instance.
 * <p>
 * The created instance uses the {@link CustomSecureProtocolSocketFactory} for SSL/TLS connections.
 * In addition the http(s) pool settings could be tweaked.
 * </p><p>
 * This instance is used for both remote and JMS calls. 
 * </p>
 * @author Thomas Pasch
 */
public class NuclosHttpClientFactory implements FactoryBean<HttpClient>, DisposableBean, Closeable {
	
	private static final Logger LOG = Logger.getLogger(NuclosHttpClientFactory.class);
	
	private static final int DEFAULT_MAX_TOTAL_CONNECTIONS = 100;

	private static final int DEFAULT_MAX_CONNECTIONS_PER_ROUTE = 50;
	
	// 30min
	public static final int SO_TIMEOUT_MILLIS = 30 * 60 * 1000;
	
	// 20sec
	public static final int CONNECTION_TIMEOUT_MILLIS = 20 * 1000;
	
	public static final int RETRY_HTTP_REQUESTS = 90;

	private static NuclosHttpClientFactory INSTANCE;

	private HttpClient httpClient;
	
	private final PoolingClientConnectionManager connectionManager;
	
	public NuclosHttpClientFactory() {
		if (INSTANCE == null) {
			INSTANCE = this;
		}
		LOG.info("Register CustomSecureProtocolSocketFactory for HTTPS (modern apache http component)");
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
		// schemeRegistry.register(new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));
		schemeRegistry.register(new Scheme("https", 443, new CustomSecureProtocolSocketFactory()));

		connectionManager = new PoolingClientConnectionManager(schemeRegistry);
		connectionManager.setMaxTotal(DEFAULT_MAX_TOTAL_CONNECTIONS);
		connectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTIONS_PER_ROUTE);

		httpClient = new DefaultHttpClient(connectionManager);
		final HttpParams params = httpClient.getParams();
		
		// see http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html
		params.setIntParameter(HttpConnectionParams.SO_TIMEOUT, SO_TIMEOUT_MILLIS);
		params.setIntParameter(HttpConnectionParams.CONNECTION_TIMEOUT, CONNECTION_TIMEOUT_MILLIS);
		
		((DefaultHttpClient) httpClient).setParams(new ControlledHttpParams(params));
		
		((DefaultHttpClient) httpClient).setRoutePlanner(new DefaultHttpRoutePlanner(schemeRegistry) {
			@Override
			public HttpRoute determineRoute(HttpHost target, HttpRequest request, HttpContext context)
					throws HttpException {
				URI location;
				try {
					location = new URI(target.getSchemeName(), target.getHostName(), null, null);
					System.setProperty("java.net.useSystemProxies", "true");
					List<Proxy> proxies = ProxySelector.getDefault().select(location);
					for (Proxy proxy : proxies) {
						if (proxy.type() == Type.HTTP) {
							InetSocketAddress addr = (InetSocketAddress) proxy.address();

							if (addr == null) {
								LOG.warn("HTTP Proxy address is null");
							} else {
								HttpHost proxyHost = new HttpHost(addr.getHostName(), addr.getPort());

								LOG.debug("Using HTTP proxy " + proxyHost + " for target " + location);

								final boolean secure = "https".equalsIgnoreCase(location.getScheme());
								return new HttpRoute(target, null, proxyHost, secure);
							}
						}
					}
				} catch (URISyntaxException e) {
				}

				return super.determineRoute(target, request, context);
			}
		});

		if (RETRY_HTTP_REQUESTS > 0) {
			((DefaultHttpClient)httpClient).setHttpRequestRetryHandler(new HttpRequestRetryHandler() {
			    @Override
			    public boolean retryRequest(IOException exception, int executionCount, 
			                                HttpContext context) {
			        if (executionCount > RETRY_HTTP_REQUESTS) {
			           LOG.warn("Maximum tries reached for client http pool ");
			                return false;
			        }
			        if (exception instanceof org.apache.http.NoHttpResponseException) {
			            LOG.warn("No response from server on " + executionCount + " call");
			            return true;
			        }
			        return false;
			      }
			}); 
		}
	}

	@Deprecated
	public static NuclosHttpClientFactory getInstance() {
		return INSTANCE;
	}
	
	@Override
	public HttpClient getObject() {
		return httpClient;
	}

	@Override
	public Class<?> getObjectType() {
		return HttpClient.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void destroy() {
		INSTANCE = null;
		connectionManager.shutdown();
	}

	@Override
	public void close() {
		connectionManager.shutdown();
	}
	
	public static class ControlledHttpParams implements HttpParams {
		
		private final HttpParams wrapped;
		
		private ControlledHttpParams(HttpParams wrapped) {
			this.wrapped = wrapped;
		}
		
		private <T> T checkParameter(final String name, T value) {
			if (name.equals(HttpConnectionParams.SO_TIMEOUT) || name.equals(HttpConnectionParams.CONNECTION_TIMEOUT)) {
				final T newValue = value;
				value = (T) wrapped.getParameter(name);
				if (!value.equals(newValue)) {
					LOG.debug("No change of parameter " + name + " allowed: sticking to " + value 
							+ ", discarding " + newValue);
				}
			}
			return value;
		}

		@Override
		public Object getParameter(String name) {
			return wrapped.getParameter(name);
		}

		@Override
		public HttpParams setParameter(String name, Object value) {
			wrapped.setParameter(name, checkParameter(name, value));
			return this;
		}
		
		public void setClientReadTimeout(int iReadTimeout) {
			wrapped.setParameter(HttpConnectionParams.SO_TIMEOUT, iReadTimeout);
		}

		@Override
		@Deprecated
		public HttpParams copy() {
			return new ControlledHttpParams(wrapped);
		}

		@Override
		public boolean removeParameter(String name) {
			final Object value = wrapped.getParameter(name);
			LOG.warn("No remove of parameter " + name + " allowed: sticking to " + value);
			// wrapped.removeParameter(name);
			return false;
		}

		@Override
		public long getLongParameter(String name, long defaultValue) {
			return wrapped.getLongParameter(name, defaultValue);
		}

		@Override
		public HttpParams setLongParameter(String name, long value) {
			wrapped.setLongParameter(name, checkParameter(name, value));
			return this;
		}

		@Override
		public int getIntParameter(String name, int defaultValue) {
			return wrapped.getIntParameter(name, defaultValue);
		}

		@Override
		public HttpParams setIntParameter(String name, int value) {
			wrapped.setIntParameter(name, checkParameter(name, value));
			return this;
		}

		@Override
		public double getDoubleParameter(String name, double defaultValue) {
			return wrapped.getDoubleParameter(name, defaultValue);
		}

		@Override
		public HttpParams setDoubleParameter(String name, double value) {
			wrapped.setDoubleParameter(name, checkParameter(name, value));
			return this;
		}

		@Override
		public boolean getBooleanParameter(String name, boolean defaultValue) {
			return wrapped.getBooleanParameter(name, defaultValue);
		}

		@Override
		public HttpParams setBooleanParameter(String name, boolean value) {
			wrapped.setBooleanParameter(name, checkParameter(name, value));
			return this;
		}

		@Override
		public boolean isParameterTrue(String name) {
			return wrapped.isParameterTrue(name);
		}

		@Override
		public boolean isParameterFalse(String name) {
			return wrapped.isParameterFalse(name);
		}
		
	}
}
