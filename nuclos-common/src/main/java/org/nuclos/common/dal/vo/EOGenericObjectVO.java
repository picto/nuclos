//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import org.nuclos.common.E;
import org.nuclos.common.UID;

public class EOGenericObjectVO extends AbstractDalVOWithVersion<Long> {
	
	public EOGenericObjectVO() {
		this(E.GENERICOBJECT.getUID());
	}
	
	public EOGenericObjectVO(UID dalEntity) {
		super(dalEntity);
	}

	private UID entityUID;

	public UID getEntityUID() {
		return entityUID;
	}

	public void setEntityUID(UID entityUID) {
		this.entityUID = entityUID;
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("EOGenericObjectVO[");
		result.append("pk=").append(getPrimaryKey());
		appendState(result.append(","));
		result.append(",entity=").append(getDalEntity());
		result.append(",version=").append(getVersion());
		result.append("]");
		return result.toString();
	}
	
}
