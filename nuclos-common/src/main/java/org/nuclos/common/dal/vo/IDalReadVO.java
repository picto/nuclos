package org.nuclos.common.dal.vo;

import java.util.Map;

import org.nuclos.common.UID;

public interface IDalReadVO<PK> extends HasPrimaryKey<PK> {

	UID getDalEntity();
	
	int getFlag();
	
	Map<UID, Object> getFieldValues();
	
	Map<UID, Long> getFieldIds();
	
	Map<UID, UID> getFieldUids();
	
}
