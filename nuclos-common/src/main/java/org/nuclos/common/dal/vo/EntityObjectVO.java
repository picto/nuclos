//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dal.vo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.nuclos.api.businessobject.Flag;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.NuclosImage;
import org.nuclos.common.RigidUtils;
import org.nuclos.common.SF;
import org.nuclos.common.SFValueable;
import org.nuclos.common.UID;
import org.nuclos.common.collection.Transformer;
import org.nuclos.common.report.ByteArrayCarrier;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Entity object vo
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:maik.stueker@novabit.de">Maik.Stueker</a>
 * @version 01.00.00
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityObjectVO<PK> extends AbstractDalVOWithFields<Object, PK> implements IDalWithDependentsVO<Object, PK>, IDalReadVO<PK> 
//TODO MULTINUCLET: Enable for backwards compatibility after refactoring... 
//, IDalWithDependantsVO<T, PK>
{

	private static final Logger LOG = Logger.getLogger(EntityObjectVO.class);

	private Object canWrite;
	private Object canStateChange;
	private Object canDelete;

	// map for dependant child subform data
	private IDependentDataMap mpDependents = new DependentDataMap();		
	// map for dataLanguages
	private IDataLanguageMap mpDataLanguages = new DataLanguageMap();
	
	public EntityObjectVO(EntityMeta<?> entity) {
		this(entity.getUID());
	}
	
	public EntityObjectVO(UID entityUID) {
		super(entityUID);
	}
	
	/**
	 * 
	 * @return the primary key of this object
	 *  
	 * @deprecated use getPrimaryKey()
	 */
	@Deprecated
	public PK getId() {
		return getPrimaryKey();
	}

	public void setDependents(IDependentDataMap mpDependents) {
		if (mpDependents == null) {
			// NUCLOS-2758 --> Otherwise NullPointer
			this.mpDependents.clear();
		} else {
			this.mpDependents = mpDependents;
		}
	}

	@Override
	public IDependentDataMap getDependents() {
		return this.mpDependents;
	}
	
	public void setDataLanguageMap(IDataLanguageMap map) {
		if (map == null) {
			this.mpDataLanguages.clear();
		} else {
			this.mpDataLanguages = map;
		}
	}
	
	public IDataLanguageMap getDataLanguageMap() {
		return this.mpDataLanguages;
	}
	
	public <T> void setFieldValue(FieldMeta.Valueable<T> entityField, T obj) {
		super.setFieldValue(entityField.getUID(), obj);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getFieldValue(FieldMeta.Valueable<T> entityField) {
		final Object ret = super.getFieldValue(entityField.getUID()); 
		return ret == null ? null : (T) ret;
	}
	
	public <T> void setFieldValue(SFValueable<T> staticField, T obj) {
		super.setFieldValue(staticField.getUID(getDalEntity()), obj);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getFieldValue(SFValueable<T> staticField) {
		final Object ret = super.getFieldValue(staticField.getUID(getDalEntity()));
		return ret == null ? null : (T) ret;
	}
	
	public void setFieldUid(FieldMeta<UID> entityField, UID uid) {
		super.setFieldUid(entityField.getUID(), uid);
	}
	
	public UID getFieldUid(FieldMeta<UID> entityField) {
		return super.getFieldUid(entityField.getUID());
	}
	
	public void setFieldUid(SF<UID> staticField, UID uid) {
		super.setFieldUid(staticField.getUID(getDalEntity()), uid);
	}
	
	public UID getFieldUid(SF<UID> staticField) {
		return super.getFieldUid(staticField.getUID(getDalEntity()));
	}
	
	public void setFieldId(FieldMeta<Long> entityField, Long id) {
		super.setFieldId(entityField.getUID(), id);
	}
	
	public Long getFieldId(FieldMeta<Long> entityField) {
		return super.getFieldId(entityField.getUID());
	}
	
	public void setFieldId(SF<Long> staticField, Long id) {
		super.setFieldId(staticField.getUID(getDalEntity()), id);
	}
	
	public Long getFieldId(SF<Long> staticField) {
		return super.getFieldId(staticField.getUID(getDalEntity()));
	}

	/**
	 * Copy is <em>with</em> dependent objects!
	 */
	public EntityObjectVO<PK> copy() {
		EntityObjectVO<PK> vo = new EntityObjectVO<PK>(getDalEntity());
		vo.setDependents(this.getDependents());
		vo.setDataLanguageMap(this.getDataLanguageMap().copy());
		vo.mapField.putAll(this.mapField);
		vo.mapFieldId.putAll(this.mapFieldId);
		vo.mapFieldUid.putAll(this.mapFieldUid);
		vo.setComplete(this.isComplete());
		return vo;
	}

	/**
	 * Copy is <em>with</em> dependent objects!
	 */
	public EntityObjectVO<PK> copyFlat() {
		EntityObjectVO<PK> vo = new EntityObjectVO<PK>(getDalEntity());
		vo.mapField.putAll(this.mapField);
		vo.mapFieldId.putAll(this.mapFieldId);
		vo.mapFieldUid.putAll(this.mapFieldUid);
		vo.setComplete(this.isComplete());
		return vo;
	}

	
	/**
	 * Transformer: gets the field with the given name, casted to the given type.
	 */
	public static class GetTypedField<T> implements Transformer<EntityObjectVO<?>, T> {
		private final UID fieldUID;
		private final Class<T> cls;

		public GetTypedField(UID fieldUID, Class<T> cls) {
			this.fieldUID = fieldUID;
			this.cls = cls;
		}

		/**
		 * @param mdvo
		 * @throws ClassCastException if the value of the field doesn't have the given type.
		 */
		@Override
        public T transform(EntityObjectVO<?> mdvo) {
			return (T) mdvo.getFieldValue(fieldUID, cls);
		}
	}

	/**
	 * @deprecated Not compatible with multi-nuclet.
	 */
	public static EntityObjectVO<?> newObject(UID entityUid) {
		EntityObjectVO<?> vo = new EntityObjectVO<Object>(entityUid);
		vo.flagNew();

		return vo;
	}

	public boolean canWrite() {
		return (canWrite == null ? Boolean.TRUE.booleanValue() : isTrue(canWrite));
	}
	public Object getCanWrite() {
		return canWrite;
	}
	public void setCanWrite(Object canWrite) {
		this.canWrite = canWrite;
	}
	
	/**
	 * true in canStateChange overwrites a false in canWrite in RecardGrant
	 * @return
	 */
	public boolean canStateChange() {
		return (canStateChange == null ? canWrite() : isTrue(canStateChange));
	}
	public Object getCanStateChange() {
		return canStateChange;
	}
	public void setCanStateChange(Object canStateChange) {
		this.canStateChange = canStateChange;
	}

	public boolean canDelete() {
		return (canDelete == null ? Boolean.TRUE.booleanValue() : isTrue(canDelete));
	}
	public Object getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Object canDelete) {
		this.canDelete = canDelete;
	}
	
	private boolean isTrue(Object o) {
		if (o instanceof Boolean) {
			return (Boolean)o;
		} else if (o instanceof String) {
			if ("false".equalsIgnoreCase((String)o) || "0".equals(o)) {
				return false;
			}
		} else if (o instanceof Number) {
			if (((Number)o).longValue() == 0) {
				return false;
			}
		}
		return true;
	}

	public String getDebugInfo() {
		final StringBuffer sb = new StringBuffer();
		sb.append("EntityObjectVO {");
		sb.append(toFlagsAndId());
		sb.append("- Fields: ");
		for (Iterator<UID> iter = this.getFieldValues().keySet().iterator(); iter.hasNext();) {
			final UID sFieldUID = iter.next();
			sb.append("{" + sFieldUID + ": " + this.getFieldValue(sFieldUID) + "}");
			if (iter.hasNext()) {
				sb.append(", ");
			}
		}
		sb.append("}");
		return sb.toString();
	}

	public String toFlagsAndId() {
		final StringBuilder result = new StringBuilder();
		result.append(" version=").append(getVersion());
		result.append(" flags=");
		if (isComplete()) {
			result.append("c");
		}
		if (isFlagNew()) {
			result.append("n");
		}
		if (isFlagRemoved()) {
			result.append("r");
		}
		if (isFlagUpdated()) {
			result.append("u");
		}
		if (isFlagUnchanged()) {
			result.append("0");
		}
		if (isSkipLoadingFromDB()) {
			result.append("S");
		}
		if (canDelete()) {
			result.append("D");
		}
		if (canWrite()) {
			result.append("W");
		}
		result.append(" pk=").append(getPrimaryKey());
		return result.toString();
	}
	
	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("EntityObjectVO[");
		result.append("entityUID=").append(getDalEntity());
		result.append(toFlagsAndId());
		// appendState(result.append(" "));
		result.append(" fields=").append(getFieldValues());
		result.append(" fieldIds=").append(getFieldIds());
		result.append(" depEntities=").append(getDependents() != null ? getDependents().getReferencingFieldUids() : "null");
		// result.append(" pk=").append(getPrimaryKey());
		result.append("]");
		return result.toString();
	}
	
	public static class GetId<PK> implements Transformer<EntityObjectVO<PK>, PK> {
		@Override
        public PK transform(EntityObjectVO<PK> mdvo) {
			return mdvo.getPrimaryKey();
		}
	}

	public void clearFields(Collection<UID> fields) {
		if (fields == null) {
			mapField.clear();
			mapFieldId.clear();
			mapFieldUid.clear();
		} else {
			for (UID field : fields) {
				mapField.remove(field);
				mapFieldId.remove(field);
				mapFieldUid.remove(field);
			}
		}
	}

	public void clearDependents() {
		mpDependents.clear();
	}

	@Override
	public int getFlag() {
		return super.getState();
	}
	
	/**
	 * 
	 * @param other
	 * @param includeEditing (created and changed information)
	 * @return
	 */
	public boolean equalFields(EntityObjectVO<PK> other, boolean includeEditing) {
		if (other == null) {
			return false;
		}
		if (!getDalEntity().equals(other.getDalEntity())) {
			return false;
		}
		if (includeEditing) {
			if (!RigidUtils.equal(getCreatedAt(), other.getCreatedAt())
					|| !RigidUtils.equal(getCreatedBy(), other.getCreatedBy())
					|| !RigidUtils.equal(getChangedAt(), other.getChangedAt())
					|| !RigidUtils.equal(getChangedBy(), other.getChangedBy())) {
				return false;
			}
		}
		if (!RigidUtils.equal(getVersion(), other.getVersion())) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
					+ ", F=" + SF.VERSION.getFieldName() + ", value=[" + getVersion() + ", " + other.getVersion() + "]");
			}
			return false;
		} 
		for (FieldMeta<?> fMeta : getMetaProvider().getAllEntityFieldsByEntity(getDalEntity()).values()) {
			UID fuid = fMeta.getUID();
			if (SF.VERSION.checkField(getDalEntity(), fuid)
					|| SF.CREATEDAT.checkField(getDalEntity(), fuid)
					|| SF.CREATEDBY.checkField(getDalEntity(), fuid)
					|| SF.CHANGEDAT.checkField(getDalEntity(), fuid)
					|| SF.CHANGEDBY.checkField(getDalEntity(), fuid)
					// No equality check for calculated attributes, see NUCLOS-5099 / ThinObjects
					|| fMeta.isCalculated()) {
				continue;
			}
			if (fMeta.getForeignEntity() != null || fMeta.getUnreferencedForeignEntity() != null) {
				if (!RigidUtils.equal(getFieldId(fuid), other.getFieldId(fuid))
						|| !RigidUtils.equal(getFieldUid(fuid), other.getFieldUid(fuid))) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
							+ ", F=" + fMeta.getFieldName() + ", (U)ID=[" + getFieldId(fuid) + ", " + other.getFieldId(fuid)
							+ ", " + getFieldUid(fuid) + ", " + other.getFieldUid(fuid) + "]");
					}
					return false;
				}
			} else {
				if (fMeta.getJavaClass().equals(ByteArrayCarrier.class) || fMeta.getJavaClass().equals(byte[].class) || 
						fMeta.getJavaClass().equals(NuclosImage.class) || fMeta.getJavaClass().equals(org.nuclos.api.NuclosImage.class)) {
					byte[] barry1 = null;
					byte[] barry2 = null;
					if (fMeta.getJavaClass().equals(ByteArrayCarrier.class)) {
						ByteArrayCarrier bac = getFieldValue(fuid, ByteArrayCarrier.class);
						if (bac != null) {
							barry1 = bac.getData();
						}
						bac = other.getFieldValue(fuid, ByteArrayCarrier.class);
						if (bac != null) {
							barry2 = bac.getData();
						}
					} else if (fMeta.getJavaClass().equals(NuclosImage.class) || 
						  fMeta.getJavaClass().equals(org.nuclos.api.NuclosImage.class)) {
						org.nuclos.api.NuclosImage nuclosImage = getFieldValue(fuid, org.nuclos.api.NuclosImage.class);
						if (nuclosImage != null) {
							barry1 = nuclosImage.getContent();
						}
						nuclosImage = other.getFieldValue(fuid, org.nuclos.api.NuclosImage.class);
						if (nuclosImage != null) {
							barry2 = nuclosImage.getContent();
						}
					} else {
						barry1 = getFieldValue(fuid, byte[].class);
						barry2 = other.getFieldValue(fuid, byte[].class);
					}
					if (!Arrays.equals(barry1, barry2)) {
						if (LOG.isDebugEnabled()) {
							LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
								+ ", F=" + fMeta.getFieldName() + ", value=[" + barry1 + ", " + barry2 + "]");
						}
						return false;
					}
				} else {
					if (!RigidUtils.equal(getFieldValue(fuid), other.getFieldValue(fuid))) {
						if (LOG.isDebugEnabled()) {
							LOG.debug("Not equalFields: E=" + getMetaProvider().getEntity(getDalEntity()).getEntityName()
								+ ", F=" + fMeta.getFieldName() + ", value=[" + getFieldValue(fuid) + ", " + other.getFieldValue(fuid) + "]");
						}
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Returns true, if this EO matches any of the given {@link Flag}s.
	 * Returns false, if no {@link Flag}s are given.
	 *
	 * @param flags
	 * @return
	 */
	public boolean matches(Flag... flags) {
		for (Flag flag: flags) {
			if (matches(flag)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Returns true, if this EO matches the given {@link Flag}.
	 *
	 * @param flag
	 * @return
	 */
	private boolean matches(Flag flag) {
		switch (flag) {
			case INSERT:
				return isFlagNew();
			case UPDATE:
				return isFlagUpdated();
			case DELETE:
				return isFlagRemoved();
			case NONE:
				// TODO: Next line should probably be: return isFlagUnchanged();
				return !isFlagNew() && !isFlagRemoved() && !isFlagUpdated();
			default:
				throw new IllegalArgumentException("Unknown Flag: " + flag);
		}
	}
}
