package org.nuclos.common.dal.vo;

import java.io.Serializable;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.server.i18n.language.data.DataLanguageLocalizedEntityEntry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public interface IDataLanguageMap extends Serializable {

	Map<UID, DataLanguageLocalizedEntityEntry> getLanguageMap();
	
	void setLanguageMap(Map<UID, DataLanguageLocalizedEntityEntry> dataLanguageMap);
	
	void setDataLanguage(UID DataLanguage, DataLanguageLocalizedEntityEntry entity);
	
	DataLanguageLocalizedEntityEntry getDataLanguage(UID DataLanguage);
	
	void clear();
	
	IDataLanguageMap copy();
	
	IDataLanguageMap clone();
}
