package org.nuclos.common.security;

import java.io.Serializable;

/**
 * Created by oliver brausch on 14.07.17.
 */
public interface IPermission extends Serializable {

	boolean includesReading();

	boolean includesWriting();

}
