//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.security;

import org.nuclos.common.UID;
import org.nuclos.common2.LangUtils;

public class PermissionKey {
	/**
	 * defines a unique key to get the permission for an attribute used in a module within a status numeral
	 */
	public static class AttributePermissionKey {
		private UID entityUID;
		private UID attributeUID;
		private UID stateUID;

		public AttributePermissionKey(UID entityUID, UID attributeUID, UID stateUID) {
			this.entityUID = entityUID;
			this.attributeUID = attributeUID;
			this.stateUID = stateUID;
		}

		public UID getEntityUID() {
			return this.entityUID;
		}

		public UID getAttributeUID() {
			return this.attributeUID;
		}

		public UID getStateUID() {
			return this.stateUID;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final AttributePermissionKey that = (AttributePermissionKey) o;
			return LangUtils.equal(this.attributeUID, that.attributeUID) &&
			LangUtils.equal(this.entityUID, that.entityUID) &&
			LangUtils.equal(this.stateUID, that.stateUID);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.entityUID) ^ LangUtils.hashCode(this.attributeUID) ^ LangUtils.hashCode(this.stateUID);
		}
	}	// class AttributePermissionKey
	
	/**
	 * defines a unique key to get the permission for a subform used in an entity
	 */
	public static class SubFormPermissionKey {
		
		private UID entityUid;

		public SubFormPermissionKey(UID entityUid) {
			this.entityUid = entityUid;
		}

		public UID getEntityUid() {
			return entityUid;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final SubFormPermissionKey that = (SubFormPermissionKey) o;
			return LangUtils.equal(this.entityUid, that.entityUid);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.entityUid) ^ LangUtils.hashCode(this.entityUid);
		}
	}	// class SubFormPermissionKey
	
	
	public static class ModulePermissionKey {
		
		private Long genericObjectId;
		private UID moduleUid;
		
		public ModulePermissionKey(Long genericObjectId, UID entityName) {
			this.genericObjectId = genericObjectId;
			this.moduleUid = entityName;
		}
		
		public Long getGenericObjectId() {
			return genericObjectId;
		}
		
		public UID getModuleUid() {
			return moduleUid;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final ModulePermissionKey that = (ModulePermissionKey) o;
			return LangUtils.equal(this.genericObjectId, that.genericObjectId) &&
				LangUtils.equal(this.moduleUid, that.moduleUid);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.genericObjectId) ^ LangUtils.hashCode(this.moduleUid);
		}
	}
	
	public static class MasterDataPermissionKey {
		
		private UID entityUid;
		
		public MasterDataPermissionKey(UID entityUid) {
			this.entityUid = entityUid;
		}
		
		public UID getEntityUid() {
			return entityUid;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || (this.getClass() != o.getClass())) {
				return false;
			}
			final MasterDataPermissionKey that = (MasterDataPermissionKey) o;
			return LangUtils.equal(this.entityUid, that.entityUid);
		}

		@Override
		public int hashCode() {
			return LangUtils.hashCode(this.entityUid);
		}
	}
}
