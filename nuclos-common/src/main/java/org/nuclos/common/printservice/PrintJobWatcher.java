//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.printservice;

import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

public class PrintJobWatcher extends PrintJobAdapter {
	
	private boolean done;
	
	public void printJobCanceled(PrintJobEvent pje) {
        allDone();
    }
    public void printJobCompleted(PrintJobEvent pje) {
        allDone();
    }
    public void printJobFailed(PrintJobEvent pje) {
        allDone();
    }
    public void printJobNoMoreEvents(PrintJobEvent pje) {
        allDone();
    }
    void allDone() {
        synchronized (PrintJobWatcher.this) {
            done = true;
            PrintJobWatcher.this.notify();
        }
    }
    
    public synchronized void waitForDone() {
        try {
            while (!done) {
                wait();
            }
        } catch (InterruptedException e) {
        }
    }
}
