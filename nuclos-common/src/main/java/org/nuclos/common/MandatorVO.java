//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;

import org.nuclos.common.collection.Transformer;
import org.nuclos.common2.StringUtils;

public class MandatorVO implements Serializable, Comparable<MandatorVO> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3131200026335127052L;
	
	public static final String PATH_SEPARATOR = "/";
	
	private final UID uid;
	private final String name;
	private final String path;
	private final UID parent;
	private final UID level;
	private final String color;
	
	public MandatorVO(UID uid, String name, String path, UID parent, UID level, String color) {
		super();
		this.uid = uid;
		this.name = name;
		this.parent = parent;
		this.path = path;
		this.level = level;
		this.color = color;
	}
	
	public UID getUID() {
		return uid;
	}
	public String getName() {
		return name;
	}
	public String getPath() {
		return path;
	}
	public UID getParentUID() {
		return parent;
	}
	public UID getLevelUID() {
		return level;
	}
	public String getColor() {
		return color;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof MandatorVO && uid != null) {
			return uid.equals(((MandatorVO)obj).getUID());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (uid != null) {
			return uid.hashCode();
		}
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		return path==null?(name==null?uid.toString():name):path;
	}

	@Override
	public int compareTo(MandatorVO o) {
		return StringUtils.compareIgnoreCase(path, o.path);
	}
	
	public static class ToUid implements Transformer<MandatorVO, UID> {
		@Override
		public UID transform(MandatorVO i) {
			return i.getUID();
		}
	}

}
