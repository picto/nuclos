//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.nuclos.common.dal.vo.EntityObjectVO;
import org.nuclos.common2.DateUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.NuclosCompileException;
import org.nuclos.common2.fileimport.CommonParseException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.console.ejb3.ConsoleFacadeRemote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * Management console for Nuclos.
 */
@Configurable
public class CommonConsole extends ConsoleConstants {

	@Autowired
	private PreferencesFacadeRemote preferencesFacadeRemote;
	
	@Autowired
	private ConsoleFacadeRemote consoleFacadeRemote;
	
	@Autowired
	private IMetaProvider metaProvider;
	
	public String getUsage() {
		final StringBuilder sb = new StringBuilder();

		TreeMap<String, String[]> sortedUsage = new TreeMap<>();
		
		addUsage(sortedUsage, CMD_INVALIDATE_ALL_CACHES, null, 
				"Invalidates all system caches (Like Nuclet import)");
		addUsage(sortedUsage, CMD_REBUILD_CLASSES, null, 
				"Rebuilds all java classes (Like Nuclet import)");
		addUsage(sortedUsage, CMD_CLEAR_USER_PREFERENCES, "<user name>", 
				"Resets the preferences for the user with the given name.");
		addUsage(sortedUsage, CMD_SEND_MESSAGE, "-message <message text> [-user <username>] [-priority <high, normal, low>] [-author <author>]", 
				"Send a message to the specified user (or all users if no one is specified).");
		addUsage(sortedUsage, CMD_KILL_SESSION, "[-user <username>]", 
				"Kill the client of the specified user (or all users if no one is specified).");
		addUsage(sortedUsage, CMD_COMPILE_DB_OBJECTS, null, 
				"Compile all invalid db functins and views.");
		addUsage(sortedUsage, CMD_REBUILD_CONSTRAINTS, null, 
				"Rebuilds all unique and foreign constraints.");
		addUsage(sortedUsage, CMD_REBUILD_INDEXES, null, 
				"Rebuilds all indexes.");
		addUsage(sortedUsage, CMD_REBUILD_CONSTRAINTS_AND_INDEXES, null, 
				"Rebuilds both at once.");
		addUsage(sortedUsage, CMD_SET_MANDATOR_LEVEL, "-package <nuclet package> -bo <name of the business object> -level <mandator level (1, 2, ...)> [-initial <path of the initial mandator>]", 
				"Set mandator level in multiple business objects.",
				"Do not change order of the arguments. Separate multiple updates with a blank or a new line.");
		addUsage(sortedUsage, CMD_CHECK_DOCUMENT_FILES, "[-deleteUnusedFiles] [-deleteDatabaseRecords <full qualified business object name 1> <fqbon 2> ...]", 
				"Checks the validity of all document attachments.");
		addUsage(sortedUsage, CMD_CLEANUP_DUPLICATE_DOCUMENTS, null, 
				"Generates a hash value per file and remove duplicates.");
		
		for (String sCommand : sortedUsage.keySet()) {
			sb.append("\t").append(sCommand).append("\n");
			String[] sUsageLines = sortedUsage.get(sCommand);
			for (String sLine : sUsageLines) {
				sb.append("\t\t").append(sLine).append("\n");
			}
		}
		
		return sb.toString();
	}
	
	private static void addUsage(TreeMap<String, String[]> sortedUsage, String sCommand, String sArguments, String...sUsageLine) {
		sortedUsage.put(sCommand + (sArguments != null ? " " + sArguments : ""), sUsageLine);
	}
	
	public CommonConsole() {
	}

	/**
	 * directly called by GUI (already logged in) - called by console's main method
	 * @param asArgs
	 */
	public void parseAndInvoke(String[] asArgs) throws Exception {
		final String sCommand;
		if (asArgs != null && asArgs.length > 0) {
			sCommand = asArgs[0];
		} else {
			sCommand = null;
		}
		parseAndInvoke(sCommand, asArgs);
	}
	
	/**
	 * 
	 * @param sCommand
	 * @param sArguments
	 * @throws Exception
	 */
	public void parseAndInvoke(final String sCommand, final String sArguments) throws Exception {
		// asArgs
		final List<String> lstAsArgs = new ArrayList<>();
		if (sCommand != null) {
			lstAsArgs.add(sCommand);
		}
		// Added handling of blanks in arguments, i.g. for path and file names
		if (sArguments != null) {
			StringTokenizer stArgument = new StringTokenizer(sArguments, "\"", true);
			boolean bHasSpaceCharacters = false;
			String sArgumentContent;
	
			while (stArgument.hasMoreTokens()) {
				sArgumentContent = stArgument.nextToken();
				if (sArgumentContent.equals("\"")) {
					bHasSpaceCharacters = !bHasSpaceCharacters;
				}
				else {
					if (bHasSpaceCharacters) {
						lstAsArgs.add(sArgumentContent);
					}
					else {
						StringTokenizer stToken = new StringTokenizer(sArgumentContent, " ");
						while (stToken.hasMoreTokens()) {
							lstAsArgs.add(stToken.nextToken());
						}
					}
				}
			}
		}
		
		parseAndInvoke(sCommand, lstAsArgs.toArray(new String[]{}));
	}

	protected void parseAndInvoke(final String sCommand, final String[] asArgs) 
			throws CommonBusinessException, IOException, SQLException, InterruptedException {
		
		final long lStartTime = new Date().getTime();
		if (sCommand == null) {
			System.out.println(getUsage());
			return;
		}
		
		final String sCommandLowerCase = sCommand.toLowerCase();
        if (sCommandLowerCase.equals(CMD_GENERATE_BO_UID_LIST.toLowerCase())) {
            generateBoUidList(asArgs);
        }
		else if (sCommandLowerCase.equals(CMD_CLEAR_USER_PREFERENCES.toLowerCase())) {
			if (asArgs.length < 2) {
				System.out.println(getUsage());
			}
			final String sUserName = asArgs[1];
			clearUserPreferences(sUserName);
		}
		else if (sCommandLowerCase.equals(CMD_COMPILE_DB_OBJECTS.toLowerCase())) {
			compileDbObjects();
		}
		else if (sCommandLowerCase.equals(CMD_REBUILD_CONSTRAINTS.toLowerCase())) {
			rebuildConstraints();
		}
		else if (sCommandLowerCase.equals(CMD_REBUILD_INDEXES.toLowerCase())) {
			rebuildIndexes();
		}
		else if (sCommandLowerCase.equals(CMD_REBUILD_CONSTRAINTS_AND_INDEXES.toLowerCase())) {
			rebuildConstraints();
			rebuildIndexes();
		}
		else if (sCommandLowerCase.equals(CMD_SET_MANDATOR_LEVEL.toLowerCase())) {
			setMandatorLevel(asArgs);
		}
		else if (sCommandLowerCase.equals(CMD_SEND_MESSAGE.toLowerCase())) {
			String sMessage = null;
			String sUser = null;
			String sAuthor = "Administrator";
			Priority priority = Priority.HIGH;

			if (asArgs.length >= 2) {
				for (int i = 1; i < asArgs.length; i++) {
					if (asArgs[i].equalsIgnoreCase("-user") && i + 1 < asArgs.length) {
						sUser = asArgs[i + 1];
					}
					else if (asArgs[i].equalsIgnoreCase("-priority") && i + 1 < asArgs.length) {
						String sPrio = asArgs[i + 1];
						if (sPrio.equalsIgnoreCase("hoch") || sPrio.equalsIgnoreCase("high")) {
							priority = Priority.HIGH;
						}
						else if (sPrio.equalsIgnoreCase("normal")) {
							priority = Priority.NORMAL;
						}
						else if (sPrio.equalsIgnoreCase("niedrig") || sPrio.equalsIgnoreCase("low")) {
							priority = Priority.LOW;
						}
						else {
							throw new CommonParseException(
									"Unknown priority \"" + sPrio + "\". Use one of: " + Priority.HIGH + ", " + Priority.NORMAL + ", " + Priority.LOW);
						}
					}
					else if (asArgs[i].equalsIgnoreCase("-message") && i + 1 < asArgs.length) {
						sMessage = asArgs[i + 1];
					}
					else if (asArgs[i].equalsIgnoreCase("-author") && i + 1 < asArgs.length) {
						sAuthor = asArgs[i + 1];
					}
				}
				if (sMessage != null) {
					sendMessage(sMessage, sUser, priority, sAuthor);
				}
				else {
					throw new CommonParseException("Missing argument -message\n");
				}
			}
			else {
				throw new CommonParseException("Missing arguments for sendMessage\n" + getUsage());
			}
		}
		else if (sCommandLowerCase.equals(CMD_KILL_SESSION.toLowerCase())) {
			String sUser = null;
			if (asArgs.length >= 2) {
				for (int i = 1; i < asArgs.length; i++) {
					if (asArgs[i].equalsIgnoreCase("-user") && i + 1 < asArgs.length) {
						sUser = asArgs[i + 1];
					}
				}
			}
			killSession(sUser);
		}
		else if (sCommandLowerCase.equals(CMD_INVALIDATE_ALL_CACHES.toLowerCase())) {
			invalidateAllCaches();
		}
		else if (sCommandLowerCase.equals(CMD_REBUILD_CLASSES.toLowerCase())) {
			rebuildClasses();
		}
		else if (sCommandLowerCase.equals(CMD_CHECK_DOCUMENT_FILES.toLowerCase())) {
			boolean deleteUnusedFiles = false;
			Set<String> deleteEntityObjects = new HashSet<String>();
			if (asArgs.length > 1) {
				for (int i = 1; i < asArgs.length; i++) {
					if (asArgs[i].equalsIgnoreCase("-deleteUnusedFiles")) {
						deleteUnusedFiles = true;						
					} else {
						if (!asArgs[i].equalsIgnoreCase("-deleteDatabaseRecords")) {
							deleteEntityObjects.add(asArgs[i]);
						}
					}
				}
			}
			checkDocumentAttachments(deleteUnusedFiles, deleteEntityObjects);
		}
		else if (sCommandLowerCase.equals(CMD_CLEANUP_DUPLICATE_DOCUMENTS.toLowerCase())) {
			cleanupDuplicateDocuments();
		}
		else if (sCommandLowerCase.equals(CMD_RESET_COMPLETE_MANDATOR_SETUP.toLowerCase())) {
			resetCompleteMandatorSetup();
		}
		else if (sCommandLowerCase.equals(CMD_DISABLE_INDEXER.toLowerCase())) {
			consoleFacadeRemote.setIndexerEnabled(false);
			System.out.println("Indexer disabled.");
		}
		else if (sCommandLowerCase.equals(CMD_ENABLE_INDEXER.toLowerCase())) {
			consoleFacadeRemote.setIndexerEnabled(true);
			System.out.println("Indexer enabled.");
		}
		else {
			throw new CommonParseException("Unknown command: " + sCommand + "\n" + getUsage());
		}
        System.out.println("NuclosConsole finished in " + (DateUtils.now().getTime() - lStartTime) + " ms");
	}

	
    private void generateBoUidList(String[] args) {
    	
    	String wordseparator = args.length > 1 ? args[1] : "";
    	
        System.out.println("// Generating BO UID list ...");
        StringBuilder builder = new StringBuilder();

        List<EntityObjectVO<UID>> nuclets = metaProvider.getNuclets();
        builder
        	.append("/** ").append(new Date()).append(" */\n")
		    .append("import org.nuclos.common.UID;\n\n");
        
        builder.append("public final class BoUidMapping {\n\n");

        
        for (EntityObjectVO<UID> nuclet : nuclets) {
			UID nucletId = nuclet.getPrimaryKey();
	        builder.append("\tpublic static final class " + format(nuclet.getFieldValue(E.NUCLET.name).toString(), wordseparator) + " {\n");
	        
	        for (EntityMeta<?> entityMeta : metaProvider.getAllEntities()) {

	        	if (entityMeta.getNuclet() == null || !entityMeta.getNuclet().equals(nucletId) || E.isNuclosEntity(entityMeta.getUID())) {
	                continue;
	            }
	            appendClass(builder, entityMeta, wordseparator);
	        }
	        builder.append("\t}\n\n");
        }

        
        // Objects which are not assigned to a nuclet
        builder.append("\tpublic static final class UnasignedEntities {\n");

        for (EntityMeta<?> entityMeta : metaProvider.getAllEntities()) {
        	if (entityMeta.getNuclet() != null || E.isNuclosEntity(entityMeta.getUID())) {
                continue;
            }
            appendClass(builder, entityMeta, wordseparator);
        }

        builder.append("\t}\n\n");
        builder.append("}\n\n");
        
        System.out.println(builder.toString());
        System.out.println("// done");
        System.out.print("//");
    }

	private static String format(String name, String wordseparator) {
		return name.replaceAll(" ", wordseparator).replaceAll("-", wordseparator);
	}
	
	private void appendClass(StringBuilder builder, EntityMeta<?> entityMeta, String wordseparator) {
		builder
		    .append("\t\tpublic static final class ").append(format(entityMeta.getEntityName(), wordseparator)).append(" {\n")
		    .append("\t\t\tpublic static final UID ID = new UID(\"").append(entityMeta.getUID()).append("\");\n")
		    .append("\t\t\tpublic static final class Field {\n");
		for (FieldMeta<?> fieldMeta : entityMeta.getFields()) {
		    builder
		        .append("\t\t\t\t/** ").append(fieldMeta.getDataType()).append(" ").append(fieldMeta.getDbColumn()).append(" */\n")
		        .append("\t\t\t\tpublic static final UID ")
		        .append(format(fieldMeta.getFieldName(), wordseparator))
		        .append(" = new UID(\"").append(fieldMeta.getUID()).append("\");\n");
		}
		builder.append("\t\t\t}\n");
		builder.append("\t\t}\n\n");
	}
	
	private void clearUserPreferences(String sUserName) throws CommonFinderException {
		preferencesFacadeRemote.setPreferencesForUser(sUserName, null);
	}

    private void invalidateAllCaches() {
		System.out.println("Invalidating all caches...");
		consoleFacadeRemote.invalidateAllCaches();
		System.out.println("done");
	}
    
    private void rebuildClasses() throws NuclosCompileException, InterruptedException {
		System.out.println("Rebuild all Java Classes...");
		consoleFacadeRemote.rebuildAllClasses();
		System.out.println("done");
	}

	private void killSession(String user) {
		System.out.println("Kill session " + user + "...");
		consoleFacadeRemote.killSession(user);
		System.out.println("done");
	}

	private void sendMessage(String sMessage, String sUser, Priority priority, String sAuthor) {
		System.out.println("sendMessage...");
		consoleFacadeRemote.sendClientNotification(sMessage, sUser, priority, sAuthor);
		System.out.println("done");
	}

	private void compileDbObjects() throws SQLException {
		System.out.println("compileInvalidDbObjects...");
		consoleFacadeRemote.compileInvalidDbObjects();
		System.out.println("done");
	}
	
	private void rebuildConstraints() {
		System.out.println("rebuildConstraints...");
		String[] result = consoleFacadeRemote.rebuildConstraints(); 
		if (result != null) {
			for (String s : result) {
				System.out.println(s);
			}
		}
		System.out.println("done");
	}
	
	private void rebuildIndexes() {
		System.out.println("rebuildIndexes...");
		String[] result = consoleFacadeRemote.rebuildIndexes(); 
		if (result != null) {
			for (String s : result) {
				System.out.println(s);
			}
		}
		System.out.println("done");
	}
	
	private void setMandatorLevel(String[] asArgs) {
		System.out.println("setMandatorLevel...");
		String[] result = consoleFacadeRemote.setMandatorLevel(asArgs); 
		if (result != null) {
			for (String s : result) {
				System.out.println(s);
			}
		}
		System.out.println("done");
	}
	
	private void checkDocumentAttachments(boolean deleteUnusedFiles, Set<String> deleteEntityObjects) {
		System.out.println("checkDocumentAttachments...");
		String[] result = consoleFacadeRemote.checkDocumentAttachments(deleteUnusedFiles, deleteEntityObjects); 
		if (result != null) {
			for (String s : result) {
				System.out.println(s);
			}
			if (result.length == 0) {
				System.out.println("No problems found.");
			}
		}
	}
	
	private void cleanupDuplicateDocuments() {
		System.out.println("cleanupDuplicateDocuments...");
		String result = consoleFacadeRemote.cleanupDuplicateDocuments();
		System.out.println("cleanupDuplicateDocuments finished.");
		System.out.println(result);
	}
	
	private void resetCompleteMandatorSetup() {
		System.out.println("resetCompleteMandatorSetup...");
		String[] result = consoleFacadeRemote.resetCompleteMandatorSetup(); 
		if (result != null) {
			for (String s : result) {
				System.out.println(s);
			}
		}
		System.out.println("done");
	}

}	// class NuclosConsole
