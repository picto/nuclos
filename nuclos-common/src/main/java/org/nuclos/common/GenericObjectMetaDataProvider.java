//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.util.Collection;
import java.util.Set;

import org.nuclos.common2.EntityAndField;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.server.attribute.valueobject.AttributeCVO;

/**
 * provides meta information about leased objects (including attributes, layouts and modules).
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public interface GenericObjectMetaDataProvider extends AttributeProvider {

	/**
	 * §postcondition result != null
	 * 
	 * @param module UID may be <code>null</code>
	 * @param bSearchable
	 * @return Collection&lt;AttributeCVO&gt; all attributes used in the module with the given uid.
	 * Note that layouts used for Details and Search are taken into account here.
	 */
	Collection<AttributeCVO> getAttributeCVOsByModule(UID module,
			Boolean bSearchable);

	/**
	 * §postcondition result != null
	 * @param layout
	 * @return Set&lt;UID&gt;
	 */
	Set<UID> getSubFormEntityByLayout(UID layout);

	Set<UID> getAllSubFormEntitiesByEntity(UID entityUid);

	/**
	 * §postcondition result != null
	 * 
	 * @param layout UID
	 * @return Collection&lt;EntityAndField&gt;
	 */
	Collection<EntityAndField> getSubFormEntityAndForeignKeyFieldsByLayout(UID layout);

	/**
	 * §postcondition result != null
	 * @param usagecriteria
	 * @return Set&lt;String&gt; the names of the attributes contained in the best matching layout for the given usagecriteria.
	 * @throws CommonFinderException
	 */
	Set<UID> getBestMatchingLayoutAttributes(UsageCriteria usagecriteria) throws CommonFinderException;

	/**
	 * @param usagecriteria
	 * @param bSearchScreen
	 * @return the id of the best matching layout for the given usage criteria
	 * @throws CommonFinderException if there is no matching layout for the given usage criteria at all.
	 */
	UID getBestMatchingLayout(UsageCriteria usagecriteria, boolean bSearchScreen) throws CommonFinderException;

	/**
	 * @param layout UID
	 * @return the LayoutML definition of the layout with the given uid.
	 */
	String getLayoutML(UID layout);
	
	Set<UID> getLayoutsByModule(UID layout, boolean bSearchScreen);

}	// interface GenericObjectMetaDataProvider
