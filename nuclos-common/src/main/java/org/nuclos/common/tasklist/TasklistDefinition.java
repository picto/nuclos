//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.tasklist;

import java.io.Serializable;

import org.nuclos.common.UID;

@SuppressWarnings("serial")
public class TasklistDefinition implements Serializable {

	private final UID tasklistUid;
	private String name;
	private String description;
	private String labelResourceId;
	private String descriptionResourceId;
	private String menupathResourceId;
	private UID dynamicTasklistId;
	private UID taskEntityUid;
	@Deprecated // TODO new requirement is a INTID column. For backwards compatibility only:
	private UID dynamicTasklistIdFieldUid;
	private UID dynamicTasklistEntityFieldUid;

	public TasklistDefinition(final UID tasklistUid) {
		this.tasklistUid = tasklistUid;
	}

	public UID getId() {
		return tasklistUid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabelResourceId() {
		return labelResourceId;
	}

	public void setLabelResourceId(String labelResourceId) {
		this.labelResourceId = labelResourceId;
	}

	public String getDescriptionResourceId() {
		return descriptionResourceId;
	}

	public void setDescriptionResourceId(String descriptionResourceId) {
		this.descriptionResourceId = descriptionResourceId;
	}

	public String getMenupathResourceId() {
		return menupathResourceId;
	}

	public void setMenupathResourceId(String menupathResourceId) {
		this.menupathResourceId = menupathResourceId;
	}

	public UID getDynamicTasklistUID() {
		return dynamicTasklistId;
	}

	public void setDynamicTasklistUID(final UID dynamicTasklistUid) {
		this.dynamicTasklistId = dynamicTasklistUid;
	}

	public UID getTaskEntityUID() {
		return taskEntityUid;
	}

	public void setTaskEntityUID(final UID taskEntityUid) {
		this.taskEntityUid = taskEntityUid;
	}

	@Deprecated // TODO new requirement is a INTID column. For backwards compatibility only:
	public UID getDynamicTasklistIdFieldUid() {
		return dynamicTasklistIdFieldUid;
	}

	@Deprecated // TODO new requirement is a INTID column. For backwards compatibility only:
	public void setDynamicTasklistIdFieldUid(UID dynamicTasklistIdFieldUid) {
		this.dynamicTasklistIdFieldUid = dynamicTasklistIdFieldUid;
	}

	public UID getDynamicTasklistEntityFieldUid() {
		return dynamicTasklistEntityFieldUid;
	}

	public void setDynamicTasklistEntityFieldUid(UID dynamicTasklistEntityFieldUid) {
		this.dynamicTasklistEntityFieldUid = dynamicTasklistEntityFieldUid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tasklistUid == null) ? 0 : tasklistUid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TasklistDefinition other = (TasklistDefinition) obj;
		if (tasklistUid == null) {
			if (other.tasklistUid != null)
				return false;
		}
		else if (!tasklistUid.equals(other.tasklistUid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TasklistDefinition [id=" + tasklistUid + ", name=" + name + ", description=" + description + ", labelResourceId=" + labelResourceId + ", descriptionResourceId=" + descriptionResourceId
				+ ", menupathResourceId=" + menupathResourceId + ", dynamicTasklistId=" + dynamicTasklistId + ", dynamicTasklistIdFieldname=" + dynamicTasklistIdFieldUid
				+ ", dynamicTasklistEntityFieldname=" + dynamicTasklistEntityFieldUid + "]";
	}
}
