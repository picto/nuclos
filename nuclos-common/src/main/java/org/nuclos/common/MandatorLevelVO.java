//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nuclos.common.dal.vo.EntityObjectVO;

public class MandatorLevelVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8699560008714202394L;
	
	private final UID uid;
	private final String name;
	private final UID parent;
	private final boolean showName;
	
	public MandatorLevelVO(EntityObjectVO<UID> eo) {
		this(eo.getPrimaryKey(), eo.getFieldValue(E.MANDATOR_LEVEL.name), eo.getFieldUid(E.MANDATOR_LEVEL.parentLevel), eo.getFieldValue(E.MANDATOR_LEVEL.showName));
	}
	
	public MandatorLevelVO(UID uid, String name, UID parent, boolean showName) {
		super();
		this.uid = uid;
		this.name = name;
		this.parent = parent;
		this.showName = showName;
	}
	
	public UID getUID() {
		return uid;
	}
	public String getName() {
		return name;
	}
	public UID getParentUID() {
		return parent;
	}
	public boolean isShowName() {
		return showName;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (obj instanceof MandatorLevelVO && uid != null) {
			return uid.equals(((MandatorLevelVO)obj).getUID());
		}
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if (uid != null) {
			return uid.hashCode();
		}
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		return name==null?uid.toString():name;
	}
	
	public static void sort(final List<MandatorLevelVO> lst) {
		
		Map<UID, MandatorLevelVO> pkMap = new HashMap<UID, MandatorLevelVO>();
		final Map<MandatorLevelVO, Integer> orderMap = new HashMap<MandatorLevelVO, Integer>();
		
		for (MandatorLevelVO level : lst) {
			pkMap.put(level.getUID(), level);
		}
		
		for (MandatorLevelVO level : lst) {
			if (!orderMap.containsKey(level)) {
				if (level.getParentUID() == null) {
					orderMap.put(level, 0);
				} else {
					// get order
					int iLevel = 0;
					Set<UID> check = new HashSet<UID>();
					UID nextParent = level.getParentUID();
					while (nextParent != null) {
						if (check.contains(nextParent)) {
							// endless loop detected
							break;
						}
						check.add(nextParent);
						MandatorLevelVO parent = pkMap.get(nextParent);
						if (parent == null) {
							nextParent = null;
						} else {
							nextParent = parent.getParentUID();
						}
						iLevel++;
					}
					orderMap.put(level, iLevel);
				}
			}
		}
		
		Collections.sort(lst, new Comparator<MandatorLevelVO>() {
			@Override
			public int compare(MandatorLevelVO o1, MandatorLevelVO o2) {
				Integer i1 = orderMap.get(o1);
				Integer i2 = orderMap.get(o2);
				if (i1 != null && i2 != null) {
					return i1.compareTo(i2);
				}
				return 0;
			}
		});
	}

}
