//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.UID;

public class TablePreferences implements Serializable {

	private static final long serialVersionUID = 6637996725938917463L;

	private NuclosPreferenceType type;
	private UID id;

	private List<ColumnPreferences> selectedColumnPreferences;
	private List<ColumnSortingPreferences> columnSorting;
	private boolean isDynamicRowHeight;
	private String name;
	private boolean selected;
	private boolean shared;
	private boolean customized;
	private Integer tableWidth;
	private boolean userdefinedName;
	private UID layout;
	private UID taskList;
	private UID searchFilter;

	public String getName() {
		return name;
	}

	public TablePreferences(NuclosPreferenceType type, UID id) {
		this.type = type;
		this.id = id;
	}

	public UID getUID() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}

	private List<ColumnPreferences> _getSelectedColumnPreferences() {
		if (this.selectedColumnPreferences == null)
			this.selectedColumnPreferences = new ArrayList<ColumnPreferences>();
		return this.selectedColumnPreferences;
	}
	public List<ColumnPreferences> getSelectedColumnPreferences() {
		ArrayList<ColumnPreferences> preferences = new ArrayList<ColumnPreferences>(this._getSelectedColumnPreferences());
		preferences.removeAll(Collections.singleton(null));
		return preferences;
	}
	public void addSelectedColumnPreferences(ColumnPreferences cp) {
		this._getSelectedColumnPreferences().add(cp);
	}
	public void addSelectedColumnPreferencesInFront(ColumnPreferences cp) {
		this._getSelectedColumnPreferences().add(0, cp);
	}
	public void addAllSelectedColumnPreferencesInFront(List<ColumnPreferences> cps) {
		this._getSelectedColumnPreferences().addAll(0, cps);
	}
	public void addAllSelectedColumnPreferences(List<ColumnPreferences> cps) {
		this._getSelectedColumnPreferences().addAll(cps);
	}
	public void removeSelectedColumnPreferences(ColumnPreferences cp) {
		this._getSelectedColumnPreferences().remove(cp);
	}
	public void removeAllSelectedColumnPreferences() {
		this._getSelectedColumnPreferences().clear();
	}

	private List<ColumnSortingPreferences> _getColumnSortings() {
		if (this.columnSorting == null)
			this.columnSorting = new ArrayList<ColumnSortingPreferences>();
		return this.columnSorting;
	}
	public List<ColumnSortingPreferences> getColumnSortings() {
		return new ArrayList<ColumnSortingPreferences>(this._getColumnSortings());
	}
	public void addColumnSorting(ColumnSortingPreferences cs) {
		this._getColumnSortings().add(cs);
	}
	public void addAllColumnSortings(List<ColumnSortingPreferences> css) {
		this._getColumnSortings().addAll(css);
	}
	public void removeColumnSorting(ColumnSortingPreferences cs) {
		this._getColumnSortings().remove(cs);
	}
	public void removeAllColumnSortings() {
		this._getColumnSortings().clear();
	}

	public boolean isDynamicRowHeight() {
		return isDynamicRowHeight;
	}

	public void setDynamicRowHeight(boolean isDynamicRowHeight) {
		this.isDynamicRowHeight = isDynamicRowHeight;
	}

	public void setSelected(boolean isActive) {
		this.selected = isActive;
	}

	public boolean isSelected() {
		return this.selected;
	}

	public TablePreferences copy() {
		TablePreferences result = new TablePreferences(getType(), getUID());

		for (ColumnPreferences cp : this._getSelectedColumnPreferences())
			result.addSelectedColumnPreferences(cp.copy());
		for (ColumnSortingPreferences cs : this._getColumnSortings())
			result.addColumnSorting(cs.copy());

		result.setName(this.getName());
		result.setDynamicRowHeight(this.isDynamicRowHeight());
		result.setUserdefinedName(isUserdefinedName());
		result.setSelected(isSelected());
		result.setTableWidth(getTableWidth());
		result.setLayout(getLayout());
		result.setCustomized(isCustomized());
		result.setShared(isShared());

		return result;
	}

	public void clear() {
		this.removeAllSelectedColumnPreferences();
		this.removeAllColumnSortings();
		this.setName(null);
		this.setDynamicRowHeight(false);
		this.setUserdefinedName(false);
		this.setSelected(false);
		this.setTableWidth(null);
		this.setLayout(null);
		this.setShared(false);
		this.setCustomized(false);
	}

	public void clearAndImport(TablePreferences tp) {
		clear();
		final TablePreferences toImportPrefs = tp.copy();
		this.addAllSelectedColumnPreferences(toImportPrefs.getSelectedColumnPreferences());
		this.addAllColumnSortings(toImportPrefs.getColumnSortings());

		this.type = tp.getType();
		this.setUID(toImportPrefs.getUID());
		this.setName(toImportPrefs.getName());
		this.setDynamicRowHeight(toImportPrefs.isDynamicRowHeight());
		this.setUserdefinedName(toImportPrefs.isUserdefinedName());
		this.setSelected(toImportPrefs.isSelected());
		this.setTableWidth(toImportPrefs.getTableWidth());
		this.setLayout(toImportPrefs.getLayout());
		this.setShared(toImportPrefs.isShared());
		this.setCustomized(toImportPrefs.isCustomized());
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(final boolean shared) {
		this.shared = shared;
	}

	public void setCustomized(final boolean customized) {
		this.customized = customized;
	}

	public boolean isCustomized() {
		return customized;
	}

	public Integer getTableWidth() {
		return tableWidth;
	}

	public void setTableWidth(Integer tableWidth) {
		this.tableWidth = tableWidth;
	}

	public boolean isUserdefinedName() {
		return userdefinedName;
	}

	public void setUserdefinedName(final boolean userdefinedName) {
		this.userdefinedName = userdefinedName;
	}

	public UID getLayout() {
		return layout;
	}

	public void setLayout(final UID layout) {
		this.layout = layout;
	}

	public boolean isEntity() {
		return type == NuclosPreferenceType.TABLE;
	}

	public boolean isSubform() {
		return type == NuclosPreferenceType.SUBFORMTABLE;
	}

	public boolean isTaskList() {
		return type == NuclosPreferenceType.TASKLIST_TABLE;
	}

	public boolean isSearchFilterTaskList() {
		return type == NuclosPreferenceType.SEARCHFILTER_TASKLIST_TABLE;
	}

	public boolean isEntityBased() {
		return isSubform() || isEntity() || isSearchFilterTaskList();
	}

	public NuclosPreferenceType getType() {
		return type;
	}

	public void setUID(final UID id) {
		this.id = id;
	}

	public UID getTaskList() {
		return taskList;
	}

	public void setTaskList(final UID taskList) {
		this.taskList = taskList;
	}

	public UID getSearchFilter() {
		return searchFilter;
	}

	public void setSearchFilter(final UID searchFilter) {
		this.searchFilter = searchFilter;
	}

	public void setType(final NuclosPreferenceType type) {
		this.type = type;
	}
}
