package org.nuclos.common.preferences;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class ExplorerTreeNodeStateImpl implements IExplorerTreeNodeState, Serializable {
	private final List<String> lstExpandedPaths;
	private final int[] selectedRows;
	
	public ExplorerTreeNodeStateImpl(List<String> lstExpandedPaths,
			int[] selectedRows) {
		this.lstExpandedPaths = lstExpandedPaths;
		this.selectedRows = selectedRows;
	}
	
	@Override
	public List<String> getLstExpandedPaths() {
		return lstExpandedPaths;
	}

	@Override
	public int[] getSelectedRows() {
		return selectedRows;
	}

}
