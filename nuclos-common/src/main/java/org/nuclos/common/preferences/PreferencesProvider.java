//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.nuclos.common.CommonSecurityCache;
import org.nuclos.common.E;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.NuclosPreferenceType;
import org.nuclos.common.ProfileUtils;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.searchcondition.ComparisonOperator;
import org.nuclos.common2.exception.CommonFinderException;
import org.nuclos.common2.exception.CommonPermissionException;
import org.nuclos.server.common.ejb3.PreferencesFacadeRemote;
import org.nuclos.server.searchfilter.valueobject.SearchFilterVO;

public class PreferencesProvider implements IPreferencesProvider {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PreferencesProvider.class);

	private static PreferencesProvider INSTANCE;

	// Spring injection

	private PreferencesFacadeRemote prefsFacade;

	private IMetaProvider metaProv;

	private CommonSecurityCache securityCache;

	// end of Spring injection

	public final void setPreferencesFacadeRemote(PreferencesFacadeRemote preferencesFacadeRemote) {
		this.prefsFacade = preferencesFacadeRemote;
	}

	public void setMetaProvider(final IMetaProvider metaProv) {
		this.metaProv = metaProv;
	}

	public void setSecurityCache(final CommonSecurityCache securityCache) {
		this.securityCache = securityCache;
	}

	PreferencesProvider() {
		INSTANCE = this;
	}

	public static IPreferencesProvider getInstance() {
		return INSTANCE;
	}

	@Override
	public TablePreferencesManager getTablePreferencesManagerForEntity(final UID entityUID, final String userName, final UID mandatorUID) {
		TablePreferencesManager result = new TablePreferencesManager(entityUID, null, null, NuclosPreferenceType.TABLE, this, metaProv, securityCache, userName, mandatorUID);
		return result;
	}

	@Override
	public TablePreferencesManager getTablePreferencesManagerForSubformEntity(final UID entityUID, final UID layoutUID, final String userName, final UID mandatorUID) {
		TablePreferencesManager result = new TablePreferencesManager(entityUID, layoutUID, null, NuclosPreferenceType.SUBFORMTABLE, this, metaProv, securityCache, userName, mandatorUID);
		return result;
	}

	@Override
	public TablePreferencesManager getTablePreferencesManagerForTaskList(final UID taskListUID, final String userName, final UID mandatorUID) {
		TablePreferencesManager result = new TablePreferencesManager(null, null, taskListUID, NuclosPreferenceType.TASKLIST_TABLE, this, metaProv, securityCache, userName, mandatorUID);
		return result;
	}

	@Override
	public SearchFilterTaskListTablePreferencesManager getTablePreferencesManagerForSearchFilterTaskList(final SearchFilterVO searchFilterVO, final String userName, final UID mandatorUID) {
		SearchFilterTaskListTablePreferencesManager result = new SearchFilterTaskListTablePreferencesManager(searchFilterVO, this, metaProv, securityCache, userName, mandatorUID);
		return result;
	}

	@Override
	public void insert(final TablePreferences tp, final UID entityUID) throws CommonPermissionException {
		prefsFacade.insertPreference(transform(tp, entityUID));
	}

	@Override
	public void select(final TablePreferences tp, final UID layoutUID) {
		prefsFacade.selectPreference(tp.getUID(), layoutUID);
	}

	@Override
	public void update(final TablePreferences tp, final UID entityUID) throws CommonFinderException, CommonPermissionException {
		prefsFacade.updatePreference(transform(tp, entityUID));
	}

	@Override
	public void delete(final TablePreferences tp) throws CommonFinderException, CommonPermissionException {
		prefsFacade.deletePreference(tp.getUID());
	}

	@Override
	public List<TablePreferences> getTablePreferences(final String app, final NuclosPreferenceType type, final UID entityUID, final UID layoutUID) {
		final List<Preference> vos = prefsFacade.getPreferences(app, type.getType(), entityUID, layoutUID, layoutUID != null, null, false);
		final List<TablePreferences> result = new ArrayList<>();
		for (Preference vo : vos) {
			result.add(transform(vo));
		}
		return result;
	}

	@Override
	public TablePreferences getTablePreference(final UID uid) throws CommonFinderException, CommonPermissionException {
		TablePreferences result;
		final Preference vo = prefsFacade.getPreference(uid);
		result = transform(vo);
		return result;
	}

	@Override
	public void resetAllCustomizedTablePreferences() {
		prefsFacade.resetCustomizedPreferences(null, NuclosPreferenceType.TABLE.getType(),
				NuclosPreferenceType.SUBFORMTABLE.getType(),
				NuclosPreferenceType.TASKLIST_TABLE.getType(),
				NuclosPreferenceType.SEARCHFILTER_TASKLIST_TABLE.getType());
	}

	private TablePreferences transform(final Preference vo) {
		final TablePreferences tp = new TablePreferences(NuclosPreferenceType.get(vo.getType()), vo.getUID());
		tp.setName(vo.getName());
		tp.setSelected(vo.isSelected());
		tp.setLayout(vo.getLayout());
		tp.setShared(vo.isShared());
		tp.setCustomized(vo.isCustomized());

		final JsonObject json = vo.getJson();
		if (json.containsKey("sideviewMenuWidth")) {
			tp.setTableWidth(json.getInt("sideviewMenuWidth"));
		}
		if (json.containsKey("userdefinedName")) {
			tp.setUserdefinedName(json.getBoolean("userdefinedName"));
		}
		if (json.containsKey("taskListId")) {
			tp.setTaskList(UID.parseUID(json.getString("taskListId")));
		}
		if (json.containsKey("searchFilterId")) {
			tp.setSearchFilter(UID.parseUID(json.getString("searchFilterId")));
		}
		if (json.containsKey("columns")) {
			final JsonArray jsonColumns = json.getJsonArray("columns");
			final SortedMap<Integer, ColumnPreferences> sortedColumns = new TreeMap<>();
			final SortedMap<Integer, ColumnSortingPreferences> sortedSorts = new TreeMap<>();
			for (int i = 0; i < jsonColumns.size(); i++) {
				final JsonObject jsonColumn = jsonColumns.getJsonObject(i);
				final ColumnPreferences cp = new ColumnPreferences();
				FieldMeta<?> efMeta = null;
				final UID columnUID;
				if (jsonColumn.containsKey("boAttrId")) {
					columnUID = UID.parseUID(jsonColumn.getString("boAttrId"));
					if (tp.isEntityBased()) {
						try {
							efMeta = metaProv.getEntityField(columnUID);
						} catch (Exception ex) {
							// ignore old / unknown attributes
							continue;
						}
					}
				} else {
					columnUID = new UID(jsonColumn.getString("column"));
				}
				cp.setColumn(columnUID);
				cp.setEntity(jsonColumn.containsKey("boMetaId") ?
						UID.parseUID(jsonColumn.getString("boMetaId")) : null);
				final int iWidth;
				if (!jsonColumn.containsKey("width") || jsonColumn.isNull("width")) {
					if (tp.isEntityBased()) {
						iWidth = ProfileUtils.getMinimumColumnWidth(efMeta.getJavaClass());
					} else {
						iWidth = 75;
					}
				} else {
					iWidth = jsonColumn.getInt("width");
				}
				cp.setWidth(iWidth);
				if (jsonColumn.containsKey("fixed")) {
					cp.setFixed(jsonColumn.getBoolean("fixed", false));
				}
				readColumnSortFromPreferences(sortedSorts, jsonColumn, columnUID);
				readFilterFromPreferences(cp, jsonColumn);
				if (jsonColumn.containsKey("selected")) {
					// working flag from webclient.
					cp.setWebclientSelected(jsonColumn.getBoolean("selected"));
				}
				final int position = jsonColumn.getInt("position", i);
				sortedColumns.put(position, cp);
			}
			for (Integer position : sortedColumns.keySet()) {
				tp.addSelectedColumnPreferences(sortedColumns.get(position));
			}
			for (Integer position : sortedSorts.keySet()) {
				tp.addColumnSorting(sortedSorts.get(position));
			}
		}

		return tp;
	}

	private void readFilterFromPreferences(final ColumnPreferences cp, final JsonObject jsonColumn) {
		if (jsonColumn.containsKey("filterOp")) {
			if (jsonColumn.containsKey("filterValueInteger")) {
				cp.setColumnFilter(Integer.valueOf(jsonColumn.getString("filterValueInteger")));
			} else if (jsonColumn.containsKey("filterValueLong")) {
				cp.setColumnFilter(Long.valueOf(jsonColumn.getString("filterValueLong")));
			} else if (jsonColumn.containsKey("filterValueDouble")) {
				cp.setColumnFilter(Double.valueOf(jsonColumn.getString("filterValueDouble")));
			} else if (jsonColumn.containsKey("filterValueString")) {
				cp.setColumnFilter(jsonColumn.getString("filterValueString"));
			} else if (jsonColumn.containsKey("filterValueBoolean")) {
				cp.setColumnFilter(Boolean.valueOf(jsonColumn.getString("filterValueBoolean")));
			} else if (jsonColumn.containsKey("filterValueDate")) {
				final String filterValueDate = jsonColumn.getString("filterValueDate");
				try {
					cp.setColumnFilter(new SimpleDateFormat("yyyy-MM-dd").parse(filterValueDate));
				} catch (ParseException e) {
					LOG.warn("FilterValueDate \"" + filterValueDate + "\" is not parsable: " + e.getMessage());
					return;
				}
			} else {
				LOG.warn("No filterValue found: " + jsonColumn.toString());
				return;
			}


			cp.setColumnFilterOp(ComparisonOperator.valueOf(jsonColumn.getString("filterOp")));
		}
	}

	private void readColumnSortFromPreferences(final SortedMap<Integer, ColumnSortingPreferences> mapSorts, final JsonObject jsonColumn, final UID columnUID) {
		if (jsonColumn.containsKey("sort")) {
			final JsonObject jsonSort = jsonColumn.getJsonObject("sort");
			if (jsonSort.containsKey("prio")) {
				final int prio = jsonSort.getInt("prio");
				ColumnSortingPreferences sort = new ColumnSortingPreferences();
				sort.setAsc("asc".equals(jsonSort.getString("direction", "asc")));
				sort.setColumn(columnUID);
				mapSorts.put(prio, sort);
			}
		}
	}

	private Preference.WritablePreference transform(final TablePreferences tp, final UID entityUID) {
		final Preference.WritablePreference wpref = new Preference.WritablePreference();
		wpref.setType(tp.getType().getType());
		wpref.setUID(tp.getUID());
		wpref.setName(tp.getName());
		wpref.setSelected(tp.isSelected());
		wpref.setEntity(entityUID);
		wpref.setLayout(tp.getLayout());

		final JsonObjectBuilder json = Json.createObjectBuilder();
		if (tp.getTableWidth() != null) {
			json.add("sideviewMenuWidth", tp.getTableWidth());
		}
		json.add("userdefinedName", tp.isUserdefinedName());
		if (tp.getTaskList() != null) {
			json.add("taskListId", tp.getTaskList().getStringifiedDefinitionWithEntity(E.TASKLIST));
		}
		if (tp.getSearchFilter() != null) {
			json.add("searchFilterId", tp.getSearchFilter().getStringifiedDefinitionWithEntity(E.SEARCHFILTER));
		}

		final JsonArrayBuilder jsonColumns = Json.createArrayBuilder();

		final List<ColumnSortingPreferences> columnSortings = tp.getColumnSortings();
		final List<ColumnPreferences> selectedColumnPreferences = tp.getSelectedColumnPreferences();

		for (int i = 0; i < selectedColumnPreferences.size(); i++) {
			final ColumnPreferences cp = selectedColumnPreferences.get(i);
			final JsonObjectBuilder jsonColumn = Json.createObjectBuilder();
			FieldMeta<?> efMeta = null;
			if (tp.isEntityBased()) {
				try {
					efMeta = metaProv.getEntityField(cp.getColumn());
				} catch (Exception ex) {
					// ignore old / unknown attributes
					continue;
				}
			}
			jsonColumn.add("name", efMeta != null ? efMeta.getFieldName() : cp.getColumn().getString());
			if (tp.isEntityBased()) {
				jsonColumn.add("boAttrId", cp.getColumn().getStringifiedDefinitionWithEntity(E.ENTITYFIELD));
			} else {
				jsonColumn.add("column", cp.getColumn().getString());
			}
			if (cp.getEntity() != null && !cp.getEntity().equals(entityUID)) {
				jsonColumn.add("boMetaId", cp.getEntity().getStringifiedDefinitionWithEntity(E.ENTITY));
			}
			if (tp.isEntityBased()) {
				if (ProfileUtils.getMinimumColumnWidth(efMeta.getJavaClass()) != cp.getWidth()) {
					// only set non default widths... for webclient
					jsonColumn.add("width", cp.getWidth());
				}
			} else {
				jsonColumn.add("width", cp.getWidth());
			}
			if (cp.isFixed()) {
				jsonColumn.add("fixed", true);
			}
			if (cp.getWebclientSelected() != null) {
				jsonColumn.add("selected", cp.getWebclientSelected());
			}
			jsonColumn.add("position", i);
			readColumnSortFromPreferences(jsonColumn, cp.getColumn(), columnSortings);
			readFilterFromPreferences(jsonColumn, cp);
			jsonColumns.add(jsonColumn);
		}
		json.add("columns", jsonColumns);

		final JsonObject jsonObject = json.build();

		wpref.setJson(jsonObject);
		return wpref;
	}

	private void readFilterFromPreferences(final JsonObjectBuilder jsonColumn, final ColumnPreferences cp) {
		final Object value = cp.getColumnFilter();
		try {
			if (value != null) {
				if (value instanceof Integer) {
					jsonColumn.add("filterValueInteger", String.valueOf(value));
				} else if (value instanceof Long) {
					jsonColumn.add("filterValueLong", String.valueOf(value));
				} else if (value instanceof Double) {
					jsonColumn.add("filterValueDouble", String.valueOf(value));
				} else if (value instanceof String) {
					jsonColumn.add("filterValueString", String.valueOf(value));
				} else if (value instanceof Boolean) {
					jsonColumn.add("filterValueBoolean", String.valueOf(value));
				} else if (value instanceof Date) {
					jsonColumn.add("filterValueDate", new SimpleDateFormat("yyyy-MM-dd").format(value));
				} else {
					LOG.warn("Filter \"" + value + "\" for column " + cp.getColumn() + " not stored in preferences. Class " + value.getClass().getCanonicalName() + " is not supported!");
					return;
				}
				jsonColumn.add("filterOp", cp.getColumnFilterOp().name());
			}
		} catch (Exception ex) {
			LOG.error("Error writing column filter value [" + value + "] to prefs: " + ex.getMessage(), ex);
		}
	}

	private void readColumnSortFromPreferences(final JsonObjectBuilder jsonColumn, UID columnUID, List<ColumnSortingPreferences> columnSortings) {
		for (int i = 0; i< columnSortings.size(); i++) {
			ColumnSortingPreferences sort = columnSortings.get(i);
			if (sort.getColumn().equals(columnUID)) {
				final JsonObjectBuilder jsonSort = Json.createObjectBuilder();
				jsonSort.add("direction", sort.isAsc() ? "asc" : "desc");
				jsonSort.add("prio", i + 1);
				jsonColumn.add("sort", jsonSort);
			}
		}
	}

}
