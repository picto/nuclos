package org.nuclos.common.preferences;

import java.util.List;

public interface IExplorerTreeNodeState {

	List<String> getLstExpandedPaths();
	
	int[] getSelectedRows();
	
}
