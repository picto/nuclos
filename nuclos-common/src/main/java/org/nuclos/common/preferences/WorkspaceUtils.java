package org.nuclos.common.preferences;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.WorkspaceDescription2;
import org.nuclos.common.WorkspaceDescription2.MutableContent;
import org.nuclos.common.WorkspaceDescription2.NestedContent;

public class WorkspaceUtils {
	private static final Logger LOG = Logger.getLogger(WorkspaceUtils.class);
	
	public static Set<UID> getTaskList(WorkspaceDescription2 wd, UID mandator) {
		Set<UID> lstTask = new HashSet<UID>();
		for (WorkspaceDescription2.Frame wdFrame : wd.getFrames()) {
			fillTaskList(wdFrame.getContent(), lstTask, mandator);
		}
		return lstTask;
	}
	
	public static void fillTaskList(MutableContent mContent, Set<UID> lstTask, UID mandator) {
		NestedContent content = mContent.getContent();
		if (content instanceof WorkspaceDescription2.Split) {
			WorkspaceDescription2.Split wdSplit = (WorkspaceDescription2.Split) content;
			fillTaskList(wdSplit.getContentA(), lstTask, mandator);
			fillTaskList(wdSplit.getContentB(), lstTask, mandator);
		} else if (content instanceof WorkspaceDescription2.Tabbed) {
			WorkspaceDescription2.Tabbed wdTabbed = (WorkspaceDescription2.Tabbed) content;
			for (WorkspaceDescription2.Tab wdTab : wdTabbed.getTabs(mandator)) {
				String prefXML = wdTab.getPreferencesXML();
				try {
					RestorePreferences rp = RestorePreferences.fromXML(prefXML);
					if (rp.type == RestorePreferences.GENERIC) {
						UID rpFilter = rp.searchFilterId;
						if (rpFilter != null) {
							lstTask.add(rpFilter);
						}
					}
				} catch (Exception e) {
					LOG.warn(e.getMessage());
					LOG.debug(e.getMessage(), e);
				}
			}
		}		
	}
	
	public static WorkspaceDescription2.Tab searchWDTab(WorkspaceDescription2 wd, String search, UID mandator) {
		for (WorkspaceDescription2.Frame wdFrame : wd.getFrames()) {
			WorkspaceDescription2.Tab res1 = searchWDTab(wdFrame.getContent(), search, mandator);
			if (res1 != null) {
				return res1;
			}
		}
		return null;
	}
	
	public static WorkspaceDescription2.Tab searchWDTab(MutableContent mContent, String search, UID mandator) {
		NestedContent content = mContent.getContent();
		if (content instanceof WorkspaceDescription2.Split) {
			WorkspaceDescription2.Split wdSplit = (WorkspaceDescription2.Split) content;
			WorkspaceDescription2.Tab res1 = searchWDTab(wdSplit.getContentA(), search, mandator);
			if (res1 != null) {
				return res1;
			}
			return searchWDTab(wdSplit.getContentB(), search, mandator);
		} else if (content instanceof WorkspaceDescription2.Tabbed) {
			WorkspaceDescription2.Tabbed wdTabbed = (WorkspaceDescription2.Tabbed) content;
			for (WorkspaceDescription2.Tab wdTab : wdTabbed.getTabs(mandator)) {
				if (search.equals(wdTab.getLabel())) {
					return wdTab;
				}
			}
		}		
		return null;
	}

	// Our webclient does not handle hidden columns.
	// So we have to disable the 'addNewColumns' feature
//	public static List<UID> addNewColumns(final List<UID> selectedFields, final TablePreferences tp, final UID entity, final boolean ignoreFixed) {
//		try {
//			for (FieldMeta efMeta : CollectionUtils.sorted( // order by intid
//					getMetaProvider().getAllEntityFieldsByEntity(entity).values(),
//					new Comparator<FieldMeta>() {
//						@Override
//						public int compare(FieldMeta o1, FieldMeta o2) {
//							return RigidUtils.compareComparables(o1.getOrder(), o2.getOrder());
//						}
//					})) {
//				if (SF.isEOField(efMeta.getEntity(), efMeta.getUID())) {
//					// do not add system fields
//					continue;
//				}
//				if (selectedFields.contains(efMeta.getUID())) {
//					// field already selected
//					continue;
//				}
//				if (tp.getHiddenColumns().contains(efMeta.getUID())) {
//					// field is hidden
//					continue;
//				}
//				if (efMeta.isCalculated()) {
//					// field is calculated
//					continue;
//				}
//				if (String.class.getName().equals(efMeta.getDataType()) &&
//						(efMeta.getScale() == null || efMeta.getScale() > 255)) {
//					// NUCLOS-1798: field is memo or clob
//					continue;
//				}
//				if (ignoreFixed) {
//					boolean bContinue = false;
//					for (ColumnPreferences cp : tp.getSelectedColumnPreferences()) {
//						if (cp.isFixed() &&
//								LangUtils.equal(cp.getColumn(), efMeta.getUID())) {
//							// field is fixed
//							bContinue = true;
//							break;
//						}
//					}
//					if (bContinue) {
//						continue;
//					}
//				}
//
//				// field is new
//				selectedFields.add(efMeta.getUID());
//			}
//		} catch (Exception ex) {
//			// not a meta data entity
//		}
//		return selectedFields;
//	}
	
	protected static IMetaProvider getMetaProvider() {
		if (SpringApplicationContextHolder.isSpringReady()) {
			return (IMetaProvider) SpringApplicationContextHolder.getBean("metaDataProvider");
		} else {
			throw new IllegalStateException("too early");
		}
	}
}
