package org.nuclos.common.preferences;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.prefs.AbstractPreferences;
import java.util.prefs.BackingStoreException;

import javax.xml.parsers.DocumentBuilder;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * A DOM based in-memory implementation of the Java preferences API.
 * 
 * see http://www.roseindia.net/java/example/java/util/importing-preferences.shtml
 * 
 * @author Thomas Pasch
 * @since Nuclos 3.9
 */
public class DOMPreferences extends AbstractPreferences {
	
	private static final Logger LOG = Logger.getLogger(DOMPreferences.class);
	
	static final String DOCTYPE_NAME = "preferences";
	
	static final String DOCTYPE_SYSTEMID = "http://java.sun.com/dtd/preferences.dtd";
	
	static final String DOM_ROOT = "preferences";
	
	private static final String[] EMPTY_STRING_ARRAY = new String[0];
	
	//

	private final Document document;
	
	private Element currentElement;
	
	/**
	 * Create a complete preferences tree (?xml/!DOCTYPE/preferences/root/...).
	 */
	DOMPreferences(DocumentBuilder builder, InputStream is, boolean user, boolean createRoot) throws IOException {
		super(null, "");
		try {
			this.document = builder.parse(is);
			// http://stackoverflow.com/questions/8438105/how-to-remove-standalone-attribute-declaration-in-xml-document
			this.document.setXmlStandalone(true);
		}
		catch (SAXException e) {
			LOG.warn("preferences parse error: " + e, e);
			throw new IOException(e);
		}
		if (createRoot) {
			this.currentElement = createRoot(user);
		} else {
			this.currentElement = this.document.getDocumentElement();
			// Not a preferences tag node? -> create an preferences tag envelope
			if (!DOM_ROOT.equals(this.currentElement.getNodeName())) {
				insertPreferencesNode(builder);
			} else {
				// go to first child (of preferences tag)
				this.currentElement = getFirstChildElement(this.currentElement);				
			}
		}
	}
	
	/**
	 * Create a complete preferences tree (?xml/!DOCTYPE/preferences/root/...).
	 */
	DOMPreferences(Document doc, boolean user) {
		super(null, "");
		this.document = doc;
		this.currentElement = null;
		this.currentElement = createRoot(user);
	}
	
	/**
	 * Create a child node of parent preferences.
	 */
	DOMPreferences(DOMPreferences parent, Element element) {
		super(parent, getNodeName(element));
		this.document = parent.document;
		this.currentElement = element;
		
		parent.currentElement.appendChild(element);
		// ???
		newNode = true;
	}
	
	private static String getNodeName(Element node) {
		assert "node".equals(node.getNodeName());
		return node.getAttribute("name");
	}
	
	private Element createMap() {
		Element first = getFirstChildElement(currentElement);
		if (first == null) {
			first = (Element) currentElement.appendChild(document.createElement("map"));
		}
		else {
			assert DOM_ROOT.equals(currentElement.getNodeName()) || "map".equals(first.getNodeName());
		}
		return first;
	}
	
	private Element createRoot(boolean user) {
		Element prefs = createPreferencesNode();
		Element first = getFirstChildElement(prefs);
		if (first == null) {
			first = (Element) currentElement.appendChild(document.createElement("root"));
		}
		else {
			assert "root".equals(first.getNodeName());
		}
		if (user) {
			first.setAttribute("type", "user");
		}
		else {
			first.setAttribute("type", "system");
		}
		return first;
	}
	
	private static Element getFirstChildElement(Element parent) {
		final NodeList nl = parent.getChildNodes();
		final int len = nl.getLength();
		Node result;
		for (int i = 0; i < len; ++i) {
			result = nl.item(i);
			if (result.getNodeType() == Node.ELEMENT_NODE) {
				return (Element) result;
			}
		}
		return null;
	}
	
	private static Element getFirstChildElement(Element parent, String localName) {
		final NodeList nl = parent.getChildNodes();
		final int len = nl.getLength();
		Node node;
		for (int i = 0; i < len; ++i) {
			node = nl.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				final Element result = (Element) node;
				if (localName.equals(result.getNodeName())) {
					return result;
				}
			}
		}
		return null;
	}
	
	private static Element getNextElement(Element e) {
		if (e == null) return null;
		Node next = e;
		do {
			next = next.getNextSibling();
			if (next != null) {
				if (next.getNodeType() == Node.ELEMENT_NODE) {
					break;
				}
			}
		} while (next != null);
		return (Element) next;
	}
	
	private static Element getNextElement(Element e, String localName) {
		if (e == null) return null;
		Node next = e;
		do {
			next = next.getNextSibling();
			if (next != null) {
				if (next.getNodeType() == Node.ELEMENT_NODE) {
					if (localName.equals(next.getNodeName())) {
						break;
					}
				}
			}
		} while (next != null);
		return (Element) next;
	}
	
	/**
	 * Create the parent/root &lt;preferences&gt; tag and sets the doctype.
	 * @return
	 */
	private Element createPreferencesNode() {
		Element prefs = (Element) document.getDocumentElement();
		if (prefs == null) {
			prefs = (Element) document.appendChild(document.createElement(DOM_ROOT));
			prefs.setAttribute("EXTERNAL_XML_VERSION", "1.0");
		}
		else {
			assert DOM_ROOT.equals(prefs.getNodeName());
		}
		
		DocumentType dt = document.getDoctype();
		if (dt == null) {
			throw new IllegalStateException("No document type");
		}
		if (!DOCTYPE_NAME.equals(dt.getName())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}
		if (!DOCTYPE_SYSTEMID.equals(dt.getSystemId())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}
		
		if (currentElement == null) {
			currentElement = prefs;
		}
		return prefs;
	}
	
	/**
	 * Create the parent/root &lt;preferences&gt; tag and sets the doctype.
	 * @return
	 */
	private void insertPreferencesNode(DocumentBuilder db) {
		final Element docElement = (Element) document.getDocumentElement();
		final Element prefs;
		if (!DOM_ROOT.equals(docElement.getNodeName())) {
			prefs = document.createElement(DOM_ROOT);
			prefs.setAttribute("EXTERNAL_XML_VERSION", "1.0");
		
			// make original docElement child of preferences node
			document.replaceChild(prefs, docElement);
			prefs.appendChild(docElement);
		} else {
			prefs = docElement;
		}
		
		DocumentType dt = document.getDoctype();
		if (dt == null) {
			dt = db.getDOMImplementation().createDocumentType(DOCTYPE_NAME, null, DOCTYPE_SYSTEMID);
			document.insertBefore(dt, prefs);
		}
		if (!DOCTYPE_NAME.equals(dt.getName())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}
		if (!DOCTYPE_SYSTEMID.equals(dt.getSystemId())) {
			throw new IllegalStateException("Wrong document type: " + dt);
		}
	}
	
	// New methods
	
	Element getDOMElement() {
		return currentElement;
	}
	
	Document getDOMDocument() {
		return document;
	}
	
	// Overridden methods
	
    public static void importPreferences(InputStream is) {
    	throw new UnsupportedOperationException();
    }
    
    @Override
    public void exportNode(OutputStream os)
            throws IOException, BackingStoreException {
    	
    	throw new UnsupportedOperationException();
    	// DOMPreferencesFactory.getInstance().save(this);
    }

	@Override
	public void exportSubtree(OutputStream os)
			throws IOException, BackingStoreException {

		if ("root".equals(currentElement.getNodeName())) {
			DOMPreferencesFactory.getInstance().write(this, os, false);
		} else {
			// ATTENTION: 
			// This is NOT according to the Preferences API spec, but more compact.
			// The spec would also write all parent node (up to root) to the stream and
			// adding doctype and the preferences tag. (tp)
			DOMPreferencesFactory.getInstance().writeRawSubtree(currentElement, os, false);
		}
	}

	@Override
    public boolean isUserNode() {
		// ???
		final Element root = createRoot(true);
		return "user".equals(root.getAttribute("type"));
    }
	
	@Override
	public void sync() {
		// ??? do nothing
	}
	
	@Override
	public void flush() {
		// ??? do nothing
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DOMPreferences)) return false;
		final DOMPreferences other = (DOMPreferences) o;
		return currentElement.equals(other.currentElement) && document.equals(other.document);
	}
	
	@Override 
	public int hashCode() {
		return 7 * currentElement.hashCode() + 931;
	}
	
	// Implementation of SPI 
	
	@Override
	protected void putSpi(String key, String value) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) child;
				assert "entry".equals(ce.getNodeName());
				if (key.equals(ce.getAttribute("key"))) {
					entry = ce;
					break;
				}
			}
		}
		if (entry == null) {
			entry = (Element) map.appendChild(document.createElement("entry"));
			entry.setAttribute("key", key);
		}
		assert "entry".equals(entry.getNodeName());
		entry.setAttribute("value", value);
	}

	@Override
	protected String getSpi(String key) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) child;
				assert "entry".equals(ce.getNodeName());
				if (key.equals(ce.getAttribute("key"))) {
					entry = ce;
					break;
				}
			}
		}
		final String result;
		if (entry == null) {
			result = null;
		}
		else {
			assert "entry".equals(entry.getNodeName());
			result = entry.getAttribute("value");
		}
		return result;
	}

	@Override
	protected void removeSpi(String key) {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		Element entry = null;
		for (int i = 0; i < children.getLength(); ++i) {
			final Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) child;
				assert "entry".equals(ce.getNodeName());
				if (key.equals(ce.getAttribute("key"))) {
					entry = ce;
					break;
				}
			}
		}
		if (entry != null) {
			assert "entry".equals(entry.getNodeName());
			final Element el = (Element) map.removeChild(entry);
			assert el == entry;
		}
	}

	@Override
	protected void removeNodeSpi() throws BackingStoreException {
		final DOMPreferences parent = (DOMPreferences) parent();
		final Element c = (Element) parent.currentElement.removeChild(currentElement);
		assert c == currentElement;
	}

	@Override
	protected String[] keysSpi() throws BackingStoreException {
		final Element map = createMap();
		final NodeList children = map.getChildNodes();
		final ArrayList<String> result = new ArrayList<String>(children.getLength());
		for (int i = 0; i < children.getLength(); ++i) {
			final Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) child;
				assert "entry".equals(ce.getNodeName());
				result.add(ce.getAttribute("key"));
			}
		}
		return result.toArray(EMPTY_STRING_ARRAY);
	}

	@Override
	protected String[] childrenNamesSpi() throws BackingStoreException {
		final Element map = createMap();
		final NodeList children = currentElement.getChildNodes();
		final ArrayList<String> result = new ArrayList<String>(children.getLength());
		for (int i = 0; i < children.getLength(); ++i) {
			final Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				final Element ce = (Element) child;
				if ("node".equals(ce.getNodeName())) {
					result.add(ce.getAttribute("name"));
				}
			}
		}
		return result.toArray(EMPTY_STRING_ARRAY);
	}

	@Override
	protected AbstractPreferences childSpi(String name) {
		final Element map = createMap();
		// first entry is the 'map' element
		Element entry = getFirstChildElement(currentElement, "node");
		while (entry != null) {
			assert "node".equals(entry.getNodeName()): "expected node named 'node' but got " + entry;
			if (name.equals(entry.getAttribute("name"))) {
				break;
			}
			entry = getNextElement(entry, "node");
		}
		if (entry == null) {
			// create
			entry = document.createElement("node");
			entry.setAttribute("name", name);
		}
		// Implicit append
		return new DOMPreferences(this, entry);
	}

	@Override
	protected void syncSpi() throws BackingStoreException {
		// ??? do nothing
	}

	@Override
	protected void flushSpi() throws BackingStoreException {
		// ??? do nothing
	}

}
