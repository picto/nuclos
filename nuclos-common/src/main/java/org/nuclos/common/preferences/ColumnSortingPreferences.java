//Copyright (C) 2017  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.preferences;

import java.io.Serializable;

import org.nuclos.common.RigidUtils;
import org.nuclos.common.UID;

public class ColumnSortingPreferences implements Serializable {
	private static final long serialVersionUID = 6637996725938917463L;

	private UID column;
	private boolean asc = true;

	public UID getColumn() {
		return column;
	}
	public void setColumn(UID column) {
		this.column = column;
	}
	public boolean isAsc() {
		return asc;
	}
	public void setAsc(boolean asc) {
		this.asc = asc;
	}

	@Override
	public int hashCode() {
		if (column == null)
			return 0;
		return column.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ColumnSortingPreferences) {
			ColumnSortingPreferences other = (ColumnSortingPreferences) obj;
			RigidUtils.equal(getColumn(), other.getColumn());
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		if (column == null)
			return "null";
		return column.toString();
	}

	public ColumnSortingPreferences copy() {
		ColumnSortingPreferences result = new ColumnSortingPreferences();
		result.setColumn(getColumn());
		result.setAsc(isAsc());
		return result;
	}
}