package org.nuclos.common.valuelistprovider;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class VLPQuery implements Serializable {

	private final UID valuelistProviderUid;

	private Map<String, Object> queryParams;
	private Map<String, String> mapFirstLstParam;
	private List<Integer> lstFieldIds;
	private String nameField;
	private String idFieldName;
	private UID baseEntityUID;
	private UID mandatorUID;
	private Integer maxRowCount;
	private UID languageUID;
	private String quickSearchInput;

	public VLPQuery(final UID valuelistProviderUid) {
		this.valuelistProviderUid = valuelistProviderUid;
	}

	public UID getValuelistProviderUid() {
		return valuelistProviderUid;
	}

	public Map<String, Object> getQueryParams() {
		return queryParams;
	}

	public VLPQuery setQueryParams(final Map<String, Object> queryParams) {
		this.queryParams = queryParams;
		return this;
	}

	public Map<String, String> getMapFirstLstParam() {
		return mapFirstLstParam;
	}

	public VLPQuery setMapFirstLstParam(final Map<String, String> mapFirstLstParam) {
		this.mapFirstLstParam = mapFirstLstParam;
		return this;
	}

	public List<Integer> getLstFieldIds() {
		return lstFieldIds;
	}

	public VLPQuery setLstFieldIds(final List<Integer> lstFieldIds) {
		this.lstFieldIds = lstFieldIds;
		return this;
	}

	public String getIdFieldName() {
		return idFieldName;
	}

	public VLPQuery setIdFieldName(final String idFieldName) {
		this.idFieldName = idFieldName;
		return this;
	}

	public UID getBaseEntityUID() {
		return baseEntityUID;
	}

	public VLPQuery setBaseEntityUID(final UID baseEntityUID) {
		this.baseEntityUID = baseEntityUID;
		return this;
	}

	public UID getMandatorUID() {
		return mandatorUID;
	}

	public VLPQuery setMandatorUID(final UID mandatorUID) {
		this.mandatorUID = mandatorUID;
		return this;
	}

	public Integer getMaxRowCount() {
		return maxRowCount;
	}

	public VLPQuery setMaxRowCount(final Integer maxRowCount) {
		this.maxRowCount = maxRowCount;
		return this;
	}

	public UID getLanguageUID() {
		return languageUID;
	}

	public VLPQuery setLanguageUID(final UID languageUID) {
		this.languageUID = languageUID;
		return this;
	}

	public String getNameField() {
		return nameField;
	}

	public VLPQuery setNameField(final String nameField) {
		this.nameField = nameField;
		return this;
	}

	public String getQuickSearchInput() {
		return quickSearchInput;
	}

	public VLPQuery setQuickSearchInput(final String quickSearchInput) {
		this.quickSearchInput = quickSearchInput;
		return this;
	}
}
