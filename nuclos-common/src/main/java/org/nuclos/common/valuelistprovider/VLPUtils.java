package org.nuclos.common.valuelistprovider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collect.collectable.CollectableValueIdField;
import org.nuclos.common.querybuilder.DatasourceUtils;
import org.nuclos.common.report.valueobject.ResultColumnVO;
import org.nuclos.common.report.valueobject.ResultVO;
import org.nuclos.common.report.valueobject.ValuelistProviderVO;
import org.nuclos.common2.SpringLocaleDelegate;
import org.nuclos.common2.StringUtils;
import org.nuclos.common2.exception.CommonBusinessException;
import org.nuclos.common2.exception.CommonFatalException;

public final class VLPUtils {
	private VLPUtils () {
	}
	
	public static String getNameField(Object oValue, Map<String, Object> mpParameters) {
		String sValueFieldName = extractFieldName((String) oValue);
		mpParameters.put(ValuelistProviderVO.DATASOURCE_NAMEFIELD, sValueFieldName);
		return sValueFieldName;
		
	}
	
	public static String getIdField(Object oValue, Map<String, Object> mpParameters, UID valuelistProviderUid, IDataSource dataSource) {
		String sIdFieldName = extractFieldName((String) oValue);
		mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
		
		try {
			if (StringUtils.isNullOrEmpty(sIdFieldName) && valuelistProviderUid != null) {
				for (String sColumn : DatasourceUtils.getColumnsWithoutQuotes(
						dataSource.getColumnsFromVLP(valuelistProviderUid, Number.class))) {
					
					final String sColumnTmp = extractFieldName(sColumn);
					if ("intid".equalsIgnoreCase(sColumnTmp)) {
						sIdFieldName = sColumnTmp;
						mpParameters.put(ValuelistProviderVO.DATASOURCE_IDFIELD, sIdFieldName);
						break;
						
					}
				}
			}
		} catch (CommonBusinessException e) {
			throw new CommonFatalException(
					SpringLocaleDelegate.getInstance().getMessage(
							"datasource.collectable.fieldsprovider", "Fehler beim Laden der Datenquelle ''{0}'':\n", oValue), e);
		}
		
		return sIdFieldName;
	}
	
	public static List<CollectableField> getVLPDataFromResult(ResultVO result, String sValueFieldName, String sIdFieldName, String sDefaultMarkerFieldName) {
		final List<CollectableField> lstFields = new ArrayList<CollectableField>();

		int iIndexValue = -1;
		int iIndexId = -1;
		int iIndexDefaultMarker = -1;
		final List<ResultColumnVO> columns = result.getColumns();
		final int len = columns.size();
		for (int iIndex = 0; iIndex < len; ++iIndex) {
			final ResultColumnVO rcvo = columns.get(iIndex);
			final String label = rcvo.getColumnLabel();
			if (label.equalsIgnoreCase(sValueFieldName)) {
				iIndexValue = iIndex;
			}

			if (label.equalsIgnoreCase(sDefaultMarkerFieldName)) {
				iIndexDefaultMarker = iIndex;
			}

			// NUCLOS-5545: an "else" here would have the effect that the sIdFieldName lookup would be
			// skipped if it were equal to sValueFieldName => no id index
			if (label.equalsIgnoreCase(sIdFieldName)) {
				iIndexId = iIndex;
			}
			
		}
		
		// If there was no sValueFieldName given, assume it's the first column of the ResultVO
		// TODO NUCLOS-5545 The Stringified Reference, confingured within BO-Wizard shall be used
		if (sValueFieldName == null && len >= 1) {
			iIndexValue = 0;
		} 
		
		if (iIndexValue < 0) {
			throw new IllegalArgumentException("In VLP, there is no field '" + sValueFieldName + "'.");
		}

		for (Object[] oValue : result.getRows()) {
			//NUCLOS-4324
			if (iIndexDefaultMarker > 0) {
				if (!Boolean.TRUE.equals(oValue[iIndexDefaultMarker])) {
					continue;
				}
			}
			
			Object value = oValue[iIndexValue];
			if (value != null) {
				CollectableField cf;
				if (iIndexId == -1) {
					cf = new CollectableValueField(value);
					
				} else {
					Object id = oValue[iIndexId];
					if (id != null) {
						if (id instanceof Number) {
							id = ((Number)id).longValue();
						} else if (id instanceof String) {
							id = new UID((String)id);
						}
						cf = new CollectableValueIdField(id, value);							
					} else {
						cf = CollectableValueIdField.NULL;							
					}
					
				}
				lstFields.add(cf);
			}
		}
		
		return lstFields;
	}
	
	public static String extractFieldName(String value) {
		// extract label if no alias is set. we strip something like T1."strname" @see NUCLOS-645
		if (value != null) {
			int idxDot = value.indexOf('.');
			if (idxDot != -1) {
				value = value.substring(idxDot + 1);
			}
			value = value.replaceAll("\"", "");				
		}
		return value;
	}

}
