//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.caching;

import org.apache.commons.collections15.map.LRUMap;
import org.nuclos.common.spring.GuavaCacheFactoryBean;
import org.springframework.cache.Cache;

/**
 * @author Thomas Pasch
 *
 * @param <K> key type
 * @param <V> value type
 * 
 * @deprecated Use {@link GuavaCacheFactoryBean} instead.
 */
public class LRUCache<K,V> implements Cache {
	
	private String name = "LRUCache";
	
	private LRUMap<K,V> wrapped;
	
	public LRUCache(int maxSize) {
		wrapped = new LRUMap<K,V>(maxSize);
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Object getNativeCache() {
		return wrapped;
	}

	@Override
	public ValueWrapper get(Object key) {
		final V value;
		synchronized (wrapped) {
			value = wrapped.get(key);			
		}
		if (value == null) {
			return null;
		}
		return new ValueWrapperImpl<V>(value);
	}
	
	// @Override
	public <T> T get(Object key, Class<T> type) {
		final V value;
		synchronized (wrapped) {
			value = wrapped.get(key);			
		}
		return (T) value;
	}

	@Override
	public void put(Object key, Object value) {
		synchronized (wrapped) {
			wrapped.put((K) key, (V) value);
		}
	}

	@Override
	public void evict(Object key) {
		synchronized (wrapped) {
			wrapped.remove((K) key);
		}
	}

	@Override
	public void clear() {
		synchronized (wrapped) {
			wrapped.clear();
		}
	}
	
	private static class ValueWrapperImpl<V> implements ValueWrapper {
		
		private final V value;
		
		private ValueWrapperImpl(V value) {
			this.value = value;
		}

		@Override
		public Object get() {
			return value;
		}
		
	}

	/**
	 * Non-atomic implementation based on org.apache.commons.collections15.map.LRUMap.
	 * 
	 * @see http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/cache/Cache.html
	 */
	// @Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		final ValueWrapper existingValue = get(key);
		if (existingValue == null) {
			wrapped.put((K) key, (V) value);
			return null;
		} else {
			return existingValue;
		}
	}

}
