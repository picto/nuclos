//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;


import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.UID;
import org.nuclos.common.report.ByteArrayCarrier;
import org.nuclos.common.report.valueobject.ReportOutputVO.Destination;
import org.nuclos.common.report.valueobject.ReportOutputVO.Format;
import org.nuclos.server.common.valueobject.NuclosValueObject;

/**
 * 
 * used to initialize a {@link ReportOutputVO}
 * 
 * @author Moritz Neuhaeuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public interface ReportOutputVOContext {
	public <T extends  NuclosValueObject<UID>> T getNuclosVO();
	public <T extends UID> T getDataSourceId();
	public <T extends UID> T getReportId();
	public <T extends UID> T getRoleId();
	public <T extends UID> T getUserId();
	public Format getFormat();
	public Destination getDestination();
	public String getParameter();
	public String getFilename();
	public String getSourceFile();
	public ByteArrayCarrier getReportCLS();
	public ByteArrayCarrier getSourceFileContent();
	public String getSheetName();
	public String getDescription();
	public String getLocale();
	public Boolean getAttach();
	public String getCustomParameter();
	public boolean getIsMandatory();
	public PrintProperties getProperties();
	
}
