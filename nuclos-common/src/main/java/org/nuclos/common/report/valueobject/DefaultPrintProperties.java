package org.nuclos.common.report.valueobject;

import java.io.Serializable;

import org.nuclos.api.UID;
import org.nuclos.api.print.PrintProperties;
import org.nuclos.common.E;
import org.nuclos.common.EntityMeta;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

/**
 * create {@link PrintProperties} from {@link MasterDataVO}
 * 
 * @author Moritz Neuhäuser <moritz.neuhaeuser@nuclos.de>
 *
 */
public class DefaultPrintProperties extends PrintProperties implements Serializable {
		
	private final MasterDataVO<UID> mdVO;
	private final EntityMeta<UID> meta;


	public <T extends UID>DefaultPrintProperties(final MasterDataVO<T> mdVO, final EntityMeta<T> meta) {
		this.mdVO = (MasterDataVO<UID>) mdVO;
		this.meta = (EntityMeta<UID>) meta;
	}

	@Override
	public void setTrayId(UID tray) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setPrintServiceId(final UID idPrintService) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setDuplex(boolean isDuplex) {
		throw new UnsupportedOperationException();

	}

	@Override
	public void setCopies(int quantity) {
		throw new UnsupportedOperationException();

	}

	@Override
	public UID getTrayId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.FORMOUTPUT.tray);
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return mdVO.getFieldUid(E.REPORTOUTPUT.tray);
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public UID getPrintServiceId() {
		if (E.FORMOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldUid(E.FORMOUTPUT.printservice), super.getPrintServiceId());
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldUid(E.REPORTOUTPUT.printservice), super.getPrintServiceId());
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public boolean isDuplex() {
		if (E.FORMOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.FORMOUTPUT.isDuplex), super.isDuplex());
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.REPORTOUTPUT.isDuplex), super.isDuplex());
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}

	@Override
	public int getCopies() {
		if (E.FORMOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.FORMOUTPUT.copies), super.getCopies());
		} else if (E.REPORTOUTPUT.equals(meta)) {
			return defaultIfNull(mdVO.getFieldValue(E.REPORTOUTPUT.copies), super.getCopies());
		} else {
			throw new IllegalArgumentException("illegal meta data " + meta);
		}
	}
	
	private <T> T defaultIfNull(T value, T defaultValue) {
		if (null == value) {
			return (T) defaultValue;
		}
		return (T)value;
	}

}
