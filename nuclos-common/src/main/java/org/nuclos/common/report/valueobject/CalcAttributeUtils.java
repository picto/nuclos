//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.report.valueobject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.nuclos.common.E;
import org.nuclos.common.NuclosFatalException;
import org.nuclos.common.UID;
import org.nuclos.common2.exception.CommonValidationException;

public class CalcAttributeUtils {

	public static final String AND = " && ";
	
	public static final String NULL = " IS [NULL]";
	public static final String STRING = " = STR[";
	public static final String INTEGER = " = INT[";
	public static final String LONG = " = LNG[";
	public static final String DOUBLE = " = DBL[";
	public static final String BOOLEAN = " = BLN[";
	public static final String DATE = " = DAT[";
	
	public static final String CALCATTRIBUTE_CUSTOMIZATION_UID_PREFIX = UID.getPrefixForStringifiedDefinitionWithEntity(E.ENTITYFIELDCUSTOMIZATION.getUID()).toUpperCase();
	
	public static boolean checkParameterNameOrValue(String nameOrValue) {
		if (nameOrValue == null) {
			return true;
		}
		if (nameOrValue.contains(AND)) {
			return false;
		}
		if (nameOrValue.contains(NULL)) {
			return false;
		}
		if (nameOrValue.contains(STRING)) {
			return false;
		}
		if (nameOrValue.contains(INTEGER)) {
			return false;
		}
		if (nameOrValue.contains(LONG)) {
			return false;
		}
		if (nameOrValue.contains(DOUBLE)) {
			return false;
		}
		if (nameOrValue.contains(BOOLEAN)) {
			return false;
		}
		if (nameOrValue.contains(DATE)) {
			return false;
		}
		return true;
	}
	
	public static String getAllChecks() {
		return "'" + AND + "', '" + NULL + "', '" + STRING + "', '" + INTEGER + "', '" + LONG + "', '" + DOUBLE + "', '" + BOOLEAN + "', '" + DATE + "'";
	}
	
	public static String mapToString(Map<String, Object> mp) throws CommonValidationException {
		StringBuffer result = new StringBuffer();
		if (mp != null) {
			mp = new TreeMap<String, Object>(mp);
			Iterator<Entry<String, Object>> it = mp.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, Object> entry = it.next();
				if (result.length() > 0) {
					result.append(AND);
				}
				result.append(entry.getKey());
				try {
					result.append(valueToString(entry.getValue()));
				} catch (CommonValidationException valex)  {
					throw new CommonValidationException("Parameter " + entry.getKey() + " contains illegal value! Do not use reserved expressions: " + CalcAttributeUtils.getAllChecks());
				}
			}
		}
		return result.toString();
	}
	
	public static Map<String, Object> stringToMap(String string) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (string != null) {
			String[] keyValuePairs = string.split(AND);
			for (String sKVP : keyValuePairs) {
				Object[] aKVP = stringToKeyValue(sKVP);
				result.put((String) aKVP[0], aKVP[1]);
			}
		}
		return result;
	}
	
	public static String valueToString(Object value) throws CommonValidationException {
		if (value == null) {
			return NULL;
		} else if (value instanceof String) {
			if (!checkParameterNameOrValue((String) value)) {
				throw new CommonValidationException();
			}
			return STRING  + (String) value + ']';
		} else if (value instanceof Integer) {
			return INTEGER + ((Integer)value).toString() + ']';
		} else if (value instanceof Long) {
			return LONG + ((Long)value).toString() + ']';
		} else if (value instanceof Double) {
			return DOUBLE + ((Double)value).toString() + ']';
		} else if (value instanceof Boolean) {
			return BOOLEAN + (Boolean.TRUE.equals(value) ? '1' : '0') + ']';
		} else if (value instanceof Date) {
			return DATE + new SimpleDateFormat("yyyy-MM-dd").format(value) + ']';
		} else {
			throw new NuclosFatalException("Unsupported type " + value.getClass().getCanonicalName());
		}
	}
	
	public static Object[] stringToKeyValue(String string) {
		if (string.contains(NULL)) {
			return new Object[]{string.substring(0, string.length()-NULL.length()), null};
		} else if (string.contains(STRING)) {
			String[] s = string.split(Pattern.quote(STRING));
			s[1] = s[1].substring(0, s[1].length()-1);
			return s;
		} else if (string.contains(INTEGER)) {
			String[] s = string.split(Pattern.quote(INTEGER));
			s[1] = s[1].substring(0, s[1].length()-1);
			return new Object[]{s[0], new Integer(s[1])};
		} else if (string.contains(LONG)) {
			String[] s = string.split(Pattern.quote(LONG));
			s[1] = s[1].substring(0, s[1].length()-1);
			return new Object[]{s[0], new Long(s[1])};
		} else if (string.contains(DOUBLE)) {
			String[] s = string.split(Pattern.quote(DOUBLE));
			s[1] = s[1].substring(0, s[1].length()-1);
			return new Object[]{s[0], new Double(s[1])};
		} else if (string.contains(BOOLEAN)) {
			String[] s = string.split(Pattern.quote(BOOLEAN));
			s[1] = s[1].substring(0, s[1].length()-1);
			return new Object[]{s[0], "1".equals(s[1])};
		} else if (string.contains(DATE)) {
			String[] s = string.split(Pattern.quote(DATE));
			s[1] = s[1].substring(0, s[1].length()-1);
			try {
				return new Object[]{s[0], new SimpleDateFormat("yyyy-MM-dd").parse(s[1])};
			} catch (ParseException e) {
				throw new NuclosFatalException("Unsupported date value " + string + ": " + e.getMessage());
			}
		} else {
			throw new NuclosFatalException("Unsupported type " + string);
		}
	}
	
	public static boolean isCalcAttributeCustomization(UID fieldUID) {
		if (fieldUID == null) {
			return false;
		}
		final String sUID = fieldUID.getString();
		return sUID.startsWith(CALCATTRIBUTE_CUSTOMIZATION_UID_PREFIX);
	}
	
	public static UID createNewCalcAttributeCustomizationUID() {
		return new UID(new UID().getBaseForStringifiedDefinitionWithEntity(E.ENTITYFIELDCUSTOMIZATION.getUID()).toUpperCase());
	}
	
	public static String getColumnName(UID calcAttributeCustomizationUID) {
		return calcAttributeCustomizationUID.getString().replace(UID.SEPARATOR_CHAR, '_').toUpperCase();
	}
	
}
