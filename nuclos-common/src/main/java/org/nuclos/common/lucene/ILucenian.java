package org.nuclos.common.lucene;

import org.nuclos.common.dal.vo.Delete;
import org.nuclos.common.dal.vo.IDalVO;

public interface ILucenian<PK> {
	
	public static int DEL = 0;
	public static int INDEX = 1;
	public static int STORE = 2;
	
	void index(IDalVO<PK> dalVO);
	void del(Delete<PK> del);
	void store();
	void notifyClusterCloud(boolean notify);
}
