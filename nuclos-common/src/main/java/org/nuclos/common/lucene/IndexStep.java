package org.nuclos.common.lucene;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class IndexStep implements Serializable {
	private final String transactId;

	public IndexStep(String transactId) {
		this.transactId = transactId;
	}

	public String getTransactId() {
		return transactId;
	}
	
}
