//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

public class ToolBarItemGroup extends ToolBarItem {

	private static final long serialVersionUID = -5188609802546367717L;
	
	private static final Logger LOG = Logger.getLogger(ToolBarItemGroup.class);
	
	private final List<ToolBarItem> content = new ArrayList<ToolBarItem>();

	public ToolBarItemGroup(String key) {
		super(Type.GROUP, key);
	}
	
	public ToolBarItemGroup addItem(ToolBarItem item) {
		if (item != null) {
			content.add(item);
		}
		return this;
	}
	
	public List<ToolBarItem> getItems() {
		return Collections.unmodifiableList(content);
	}

}
