package org.nuclos.common.collect.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.nuclos.common.NuclosFatalException;

public class RunnableTask implements Callable<RunnableTaskResult> {
	
	private String name;
	private int secondsToTimeout;
    private List<Callable<RunnableTaskResult>> lstCallables;
    private List<Runnable> lstRunnables;

    private static final int TIMEOUT_DEFAULT = 60;
    
    public RunnableTask(String pName) {
    	this(pName, TIMEOUT_DEFAULT);
    }
    
    public RunnableTask(String pName, int secondsToTimeout) {
        this.name = pName;
        this.lstCallables = new ArrayList<Callable<RunnableTaskResult>>();
        this.lstRunnables = new ArrayList<Runnable>();
        this.secondsToTimeout = secondsToTimeout;        
    }

    public void addCallable(Callable<RunnableTaskResult> callable ) {
    	lstCallables.add(callable);
    }

    public void addCallables(List<Callable<RunnableTaskResult>> callables) {
    	for (Callable<RunnableTaskResult> c : callables)
    		lstCallables.add(c);
    }
    
    public void addRunnable(Runnable runable ) {
        lstRunnables.add(runable);
    }

    public void addRunnables(List<Runnable> runables) {
    	for (Runnable r : runables) 
    		lstRunnables.add(r);
    }
    
    public RunnableTaskResult run() throws InterruptedException, NuclosFatalException{
    	
    	RunnableTaskResult result = new RunnableTaskResult(this.name);
    	
    	ExecutorService newFixedThreadPool =
                Executors.newFixedThreadPool(1);
    	
    	result.addFuture(newFixedThreadPool.submit(this));
    	
    	newFixedThreadPool.shutdown();

    	boolean retVal = newFixedThreadPool.awaitTermination(
    			this.secondsToTimeout, TimeUnit.SECONDS);

    	if (!retVal)
        	throw new NuclosFatalException("Timeout elapsed before termination of RunnableTasks '" + this.name + "'");
    	
        return result;
    }
    
    @Override
    public RunnableTaskResult call() throws Exception {

    	RunnableTaskResult result = new RunnableTaskResult(this.name);
    	
        int maxSize = lstRunnables.size() + lstCallables.size();

        ExecutorService newFixedThreadPool =
                Executors.newFixedThreadPool(maxSize);

        for (Runnable r : lstRunnables) {
        	result.addFuture(
        			newFixedThreadPool.submit(r));
        }

        for (Callable<RunnableTaskResult> c : lstCallables) {
        	result.addFuture(
        			newFixedThreadPool.submit(c));
        }

        newFixedThreadPool.shutdown();

        try {
            boolean retVal = newFixedThreadPool.awaitTermination(
            		this.secondsToTimeout, TimeUnit.SECONDS);
            if (!retVal)
            	throw new NuclosFatalException("Timeout elapsed before termination of RunnableTasks '" + this.name + "'");
            	
        } catch (InterruptedException e) {
            throw new Exception(e);
        }

        return result;
    }
}
