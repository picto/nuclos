//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.collect.collectable;

import java.io.Serializable;

import org.nuclos.common.FieldMeta;
import org.nuclos.common.UID;
import org.nuclos.common.dal.vo.SystemFields;

/**
 * Sorting (German: "Sortierung") of a <code>Collectable</code>, consisting of a field name and a direction.
 * Typically, this is an element in a <code>List</code> containing the complete sorting order.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * <p>
 * TODO: Incorporate a real (foreign) table ref instead of just the entity.
 * </p><p>
 * TODO: Consider including an entity field rather than the mere field name.
 * </p>
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version	01.00.00
 */
public class CollectableSorting implements Serializable {
	
	private static final long serialVersionUID = 1002927873365829614L;

	private final UID entity;
	
	private final boolean isBaseEntity;
	
	private final UID field;
	
	// maybe null
	private final FieldMeta<?> mdField;
	
	private final boolean asc;
	
	// lazy initialization needed
	private String tableAlias = null;

	/**
	 * §precondition sFieldName != null
	 * 
	 * @param field name of the field to sort
	 * @param asc Sort ascending? (false: sort descending)
	 */
	public CollectableSorting(String tableAlias, UID entity, boolean isBaseEntity, UID field, boolean asc) {
		if (field == null) throw new NullPointerException();
		this.tableAlias = tableAlias;
		this.entity = entity;
		this.isBaseEntity = isBaseEntity;
		this.field = field;
		this.mdField = null;
		this.asc = asc;
	}
	
	public CollectableSorting(UID field, boolean asc) {
		this(SystemFields.BASE_ALIAS, null, true, field, asc);
	}

	/**
	 * §postcondition result != null
	 * 
	 * @return uid of the field to sort.
	 */
	public UID getField() {
		return this.field;
	}

	/**
	 * @return Sort ascending? (false: sort descending)
	 */
	public boolean isAscending() {
		return this.asc;
	}
	
	/**
	 * Attention: A call to this method is only valid on the <em>server</em> side!
	 */
	private static String initTableAlias(FieldMeta<?> mdField) {
		final String result;
		if (mdField == null) {
			throw new IllegalStateException();
		}
		else {
			result = SystemFields.BASE_ALIAS;
		}
		return result;
	}
	
	public String getTableAlias() {
		if (tableAlias == null) {
			tableAlias = initTableAlias(mdField);
		}
		return tableAlias;
	}
	
	public UID getEntity() {
		return entity;
	}

	public boolean isBaseEntity() {
		return isBaseEntity;
	}	
	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof CollectableSorting)) return false;
		final CollectableSorting other = (CollectableSorting) o;
		return entity.equals(other.entity) && field.equals(other.field);
	}
	
	public int hashCode() {
		int result = 3 * entity.hashCode() + 7;
		result += 11 * field.hashCode();
		return result;
	}

    @Override
    public String toString() {
    	final StringBuilder result = new StringBuilder();
    	result.append("CollectableSorting[");
    	result.append(stringRepresentation(this));
    	result.append("]");
    	return result.toString();
    }
    
    public static final String SORTING_ASCENDING = "asc";
    public static final String SORTING_DESCENDING = "desc";
    
    public static String stringRepresentation(CollectableSorting sorting) {
    	if (sorting == null || sorting.field == null) {
    		return null;
    	}
    	return sorting.field.getString() + " " + (sorting.asc ? SORTING_ASCENDING : SORTING_DESCENDING);
    }
    
    public static CollectableSorting createSorting(String representation) {
    	if (representation == null || representation.isEmpty()) {
    		return null;
    	}
    	
    	boolean asc = true;
    	String fieldUid = representation.trim();
    	
    	int n = fieldUid.indexOf(' ');
    	if (n > -1) {
    		asc = !fieldUid.substring(n + 1).equals(SORTING_DESCENDING);
    		fieldUid = fieldUid.substring(0, n);
    	}
    	
    	return createSorting(new UID(fieldUid), asc);
    }
    
    public static CollectableSorting createSorting(UID field, boolean sorting) {
    	return new CollectableSorting(field, sorting);
    }


}  // class CollectableSorting
