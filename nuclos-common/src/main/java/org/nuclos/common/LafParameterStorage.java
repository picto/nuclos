//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import org.nuclos.common.collect.ToolBarConfiguration.Context;

/**
 * ATTENTION: 
 * Sequence of enums matters. Preferred loading/storing place must appear first.
 */
public enum LafParameterStorage {
	
	/**
	 * Look at entity storage fist...
	 */
	ENTITY, 
	
	/**
	 * .. than at the workspace ...
	 */
	WORKSPACE, 
	
	/**
	 * .. and finally on the system settings.
	 */
	SYSTEMPARAMETER;
	
	public Context context(UID entityUid) {
		final Context result;
		switch (this) {
		case WORKSPACE:
		case SYSTEMPARAMETER:
			result = Context.UNSPECIFIED;
			break;
		case ENTITY:
			final IMetaProvider mprov = SpringApplicationContextHolder.getBean(IMetaProvider.class);
			EntityMeta<?> meta = mprov.getEntity(entityUid);
			result = meta.isStateModel() ? Context.ENTITY_MODULE : Context.ENTITY_MASTERDATA;
			break;
		default:
			throw new IllegalArgumentException();	
		}
		return result;
	}
}
