//Copyright (C) 2014  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.spring;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.StringUtils;

import com.google.common.cache.CacheBuilder;

/**
/**
 * {@link FactoryBean} for easy configuration of a {@link GuavaCache}
 * when used within a Spring container. Can be configured through bean properties;
 * uses the assigned Spring bean name as the default cache name.
 * <p>
 * This is a production ready implementation for Spring cache support with
 * many properties to tune.
 * 
 * @see org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean
 * @since Nuclos 4.0.18
 * @author Thomas Pasch
 */
public class GuavaCacheFactoryBean
		implements FactoryBean<GuavaCache>, BeanNameAware, InitializingBean {

	private String name = "";

	private GuavaCache cache;
	
	// 
	
	private int concurrencyLevel = -1;
	
	private int initialCapacity = -1;
	
	private long maximumSize = -1;
	
	private long expireAfterAccessValue = -1;
	
	private TimeUnit expireAfterAccessUnit = TimeUnit.MINUTES;
	
	private long expireAfterWriteValue = -1;
	
	private TimeUnit expireAfterWriteUnit = TimeUnit.MINUTES;
	
	private boolean weakKeys = false;
	
	private boolean weakValues = false;
	
	private boolean softValues = false;
	
	private boolean recordStats = true;

	/**
	 * Specify the name of the cache.
	 * <p>Default is "" (empty String).
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setBeanName(String beanName) {
		if (!StringUtils.hasLength(this.name)) {
			setName(beanName);
		}
	}

	@Override
	public void afterPropertiesSet() {
		CacheBuilder builder = CacheBuilder.newBuilder();
		if (concurrencyLevel > 0) {
			builder = builder.concurrencyLevel(concurrencyLevel);
		}
		if (initialCapacity > 0) {
			builder = builder.initialCapacity(initialCapacity);
		}
		if (expireAfterAccessValue > 0) {
			builder = builder.expireAfterAccess(expireAfterAccessValue, expireAfterAccessUnit);
		}
		if (expireAfterWriteValue > 0) {
			builder = builder.expireAfterWrite(expireAfterWriteValue, expireAfterWriteUnit);
		}
		if (maximumSize > 0) {
			builder = builder.maximumSize(maximumSize);
		}
		if (weakKeys) {
			builder = builder.weakKeys();
		}
		if (weakValues) {
			builder = builder.weakValues();
		}
		if (softValues) {
			builder = builder.softValues();
		}
		if (recordStats) {
			builder = builder.recordStats();
		}
		this.cache = new GuavaCache(name, builder.build());
	}


	@Override
	public GuavaCache getObject() {
		return this.cache;
	}

	@Override
	public Class<?> getObjectType() {
		return GuavaCache.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
	
	//

	public void setConcurrencyLevel(int concurrencyLevel) {
		this.concurrencyLevel = concurrencyLevel;
	}

	public void setInitialCapacity(int initialCapacity) {
		this.initialCapacity = initialCapacity;
	}

	public void setMaximumSize(long maximumSize) {
		this.maximumSize = maximumSize;
	}

	public void setExpireAfterAccessValue(long expireAfterAccessValue) {
		this.expireAfterAccessValue = expireAfterAccessValue;
	}

	public void setExpireAfterAccessUnit(TimeUnit expireAfterAccessUnit) {
		this.expireAfterAccessUnit = expireAfterAccessUnit;
	}

	public void setExpireAfterWriteValue(long expireAfterWriteValue) {
		this.expireAfterWriteValue = expireAfterWriteValue;
	}

	public void setExpireAfterWriteUnit(TimeUnit expireAfterWriteUnit) {
		this.expireAfterWriteUnit = expireAfterWriteUnit;
	}

	public void setWeakKeys(boolean weakKeys) {
		this.weakKeys = weakKeys;
	}

	public void setWeakValues(boolean weakValues) {
		this.weakValues = weakValues;
	}

	public void setSoftValues(boolean softValues) {
		this.softValues = softValues;
	}

	public void setRecordStats(boolean recordStats) {
		this.recordStats = recordStats;
	}

}
