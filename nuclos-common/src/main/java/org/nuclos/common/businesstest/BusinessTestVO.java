package org.nuclos.common.businesstest;

import org.apache.commons.lang.StringUtils;
import org.nuclos.common.E;
import org.nuclos.common.UID;
import org.nuclos.common2.DateTime;
import org.nuclos.server.masterdata.valueobject.MasterDataVO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessTestVO extends MasterDataVO<UID> {
	/**  */
	private static final long serialVersionUID = -9147877538320998426L;

	public enum STATE {
		NULL, GREEN, YELLOW, RED
	}

	public BusinessTestVO() {
		super(E.BUSINESSTEST, false);
	}

	public BusinessTestVO(MasterDataVO<UID> mdvo) {
		super(mdvo);
	}

	/**
	 * Resets the execution data of this test.
	 */
	public void reset() {
		setResult(null);
		setLog("");
		setWarningCount(0);
		setErrorCount(0);
		setState(STATE.NULL);
		setDuration(null);
		setStartdate(null);
		setEnddate(null);
		setWarningLine(null);
		setErrorLine(null);
	}

	public String getName() {
		return getFieldValue(E.BUSINESSTEST.name);
	}

	public void setName(String name) {
		setFieldValue(E.BUSINESSTEST.name, name);
	}

	public String getDescription() {
		return getFieldValue(E.BUSINESSTEST.description);
	}

	public void setDescription(String description) {
		setFieldValue(E.BUSINESSTEST.description, description);
	}

	public String getSource() {
		return getFieldValue(E.BUSINESSTEST.source);
	}

	public void setSource(String source) {
		setFieldValue(E.BUSINESSTEST.source, source);
	}

	public STATE getState() {
		if (getFieldValue(E.BUSINESSTEST.state) != null) {
			return STATE.valueOf(getFieldValue(E.BUSINESSTEST.state));
		}
		return null;
	}

	public void setState(STATE state) {
		if (state != null) {
			setFieldValue(E.BUSINESSTEST.state, state.name());
		} else {
			setFieldValue(E.BUSINESSTEST.state, null);
		}
	}

	public DateTime getStartdate() {
		return getFieldValue(E.BUSINESSTEST.startdate);
	}

	public void setStartdate(DateTime startdate) {
		setFieldValue(E.BUSINESSTEST.startdate, startdate);
	}

	public DateTime getEnddate() {
		return getFieldValue(E.BUSINESSTEST.enddate);
	}

	public void setEnddate(DateTime enddate) {
		setFieldValue(E.BUSINESSTEST.enddate, enddate);
	}

	public Long getDuration() {
		return getFieldValue(E.BUSINESSTEST.duration);
	}

	public void setDuration(Long duration) {
		setFieldValue(E.BUSINESSTEST.duration, duration);
	}

	public String getResult() {
		return getFieldValue(E.BUSINESSTEST.result);
	}

	public void setResult(String result) {
		setFieldValue(E.BUSINESSTEST.result, StringUtils.substring(result, 0, 255));
	}

	public String getLog() {
		return getFieldValue(E.BUSINESSTEST.log);
	}

	public void setLog(String log) {
		setFieldValue(E.BUSINESSTEST.log, log);
	}

	public void appendLog(String log) {
		if (StringUtils.isNotBlank(getLog())) {
			log = getLog() + "\n" + log;
		}
		setFieldValue(E.BUSINESSTEST.log, log);
	}

	public UID getEntity() {
		return getFieldUid(E.BUSINESSTEST.entity);
	}

	public void setEntity(UID entity) {
		setFieldUid(E.BUSINESSTEST.entity, entity);
	}

	public UID getNuclet() {
		return getFieldUid(E.BUSINESSTEST.nuclet);
	}

	public void setNuclet(UID nuclet) {
		setFieldUid(E.BUSINESSTEST.nuclet, nuclet);
	}

	public Integer getWarningCount() {
		return getFieldValue(E.BUSINESSTEST.warningCount);
	}

	public void setWarningCount(Integer warningCount) {
		setFieldValue(E.BUSINESSTEST.warningCount, warningCount);
	}

	public Integer getErrorCount() {
		return getFieldValue(E.BUSINESSTEST.errorCount);
	}

	public void setErrorCount(Integer errorCount) {
		setFieldValue(E.BUSINESSTEST.errorCount, errorCount);
	}

	public Integer getWarningLine() {
		return getFieldValue(E.BUSINESSTEST.warningLine);
	}

	public void setWarningLine(Integer warningLine) {
		setFieldValue(E.BUSINESSTEST.warningLine, warningLine);
	}

	public Integer getErrorLine() {
		return getFieldValue(E.BUSINESSTEST.errorLine);
	}

	public void setErrorLine(Integer errorLine) {
		setFieldValue(E.BUSINESSTEST.errorLine, errorLine);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
