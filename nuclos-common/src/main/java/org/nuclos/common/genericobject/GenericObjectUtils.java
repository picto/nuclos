//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.genericobject;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.nuclos.common.AttributeProvider;
import org.nuclos.common.CollectableEntityFieldWithEntityForExternal;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.collect.collectable.CollectableValueField;
import org.nuclos.common.collection.CollectionUtils;
import org.nuclos.common.dal.vo.EntityObjectVO;

/**
 * Utility methods for leased objects, client and server usage.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 * @author	<a href="mailto:uwe.allner@novabit.de">uwe.allner</a>
 * @version 01.00.00
 */
public class GenericObjectUtils {

	private GenericObjectUtils() {
	}

	/**
	 * §todo refactor using CollectionUtils.getSeparatedList(Iterable, String)
	 * 
	 * @param collmdvo May be null.
	 * @param field
	 * @return the concatenated values of the field with the given uid, for each element of the given collection.
	 */
	public static String getConcatenatedValue(Collection<EntityObjectVO> collmdvo, UID field) {
		final StringBuffer sb = new StringBuffer();
		if (collmdvo != null) {
			for (Iterator<EntityObjectVO> iter = collmdvo.iterator(); iter.hasNext();) {
				final EntityObjectVO mdvo = iter.next();
				sb.append(new CollectableValueField(mdvo.getFieldValue(field, Object.class)).toString());
				if (iter.hasNext()) {
					sb.append(" ");
				}
			}
		}
		return sb.toString();
	}

	/**
	 * §postcondition result != null
	 * 
	 * @param lstclctefwe
	 * @param mainEntity UID of the main entity
	 * @return the attribute uids of the selected fields that belong to the main entity
	 * 
	 * @deprecated As AttributeProvider is deprecated, this is deprecated as well.
	 */
	public static List<UID> getAttributeUids(List<? extends CollectableEntityField> lstclctefwe, UID mainEntity, AttributeProvider ap) {
		final List<? extends CollectableEntityField> lstclctefMain = CollectionUtils.select(lstclctefwe, new CollectableEntityField.HasEntity(mainEntity));
		return CollectionUtils.transform(lstclctefMain, new CollectableEntityField.GetUID());
	}

	/**
	 * @param clcte the entity of the field
	 * @param field the uid of the field
	 * @param clcteMain the main entity
	 * @return a <code>CollectableEntityField</code> for the Result tab with the given entity and field name.
	 */
	public static CollectableEntityFieldWithEntityForExternal getCollectableEntityFieldForResult(final CollectableEntity clcte, UID field, CollectableEntity clcteMain) {
		final UID mainEntity = clcteMain.getUID();
		final boolean bFieldBelongsToMainEntity = clcte.getUID().equals(mainEntity);
		final boolean bFieldBelongsToSubEntity = !(bFieldBelongsToMainEntity);
		final CollectableEntityFieldWithEntityForExternal clctefwefe = new CollectableEntityFieldWithEntityForExternal(clcte, field, bFieldBelongsToSubEntity, bFieldBelongsToMainEntity);

		// set security agent, to check whether the user has the right to see the data in the result panel
		// setSecurityAgent(clcte, clctefwefe, bFieldBelongsToSubEntity);
		
		return clctefwefe;
	}

}	// class GenericObjectUtils

