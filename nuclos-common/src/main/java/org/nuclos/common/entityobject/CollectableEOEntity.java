//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.entityobject;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.NullArgumentException;
import org.nuclos.common.EntityMeta;
import org.nuclos.common.FieldMeta;
import org.nuclos.common.IMetaProvider;
import org.nuclos.common.SF;
import org.nuclos.common.SpringApplicationContextHolder;
import org.nuclos.common.UID;
import org.nuclos.common.collect.collectable.CollectableEntity;
import org.nuclos.common.collect.collectable.CollectableEntityField;
import org.nuclos.common.dal.util.DalTransformations;
import org.nuclos.common2.SpringLocaleDelegate;

public class CollectableEOEntity implements CollectableEntity {
	
	private final EntityMeta<?> eMeta;
	private final Map<UID, FieldMeta<?>> mapFields;

	public CollectableEOEntity(EntityMeta<?> eMeta) {
		if (eMeta == null) {
			throw new NullArgumentException("eMeta");
		}
		this.eMeta = eMeta;
		mapFields = DalTransformations.transformFieldMap(eMeta.getFields());
	}

	@Override
	public CollectableEntityField getEntityField(UID field) {
		FieldMeta<?> efVO = mapFields.get(field);
		if (efVO == null) {
			if (SF.isEOField(getMeta().getUID(), field)) {
				for (SF<?> curfield : SF.getAllFields()) {
					if (curfield.getUID(getMeta().getUID()).equals(field)) {
						if (SF.PK_ID.getUID(getMeta().getUID()).equals(field)) {
							if (getMeta().isUidEntity()) {
								efVO = SF.PK_UID.getMetaData(getMeta().getUID());
							} else {
								efVO = SF.PK_ID.getMetaData(getMeta().getUID());
							}
						} else {
							efVO = curfield.getMetaData(getMeta().getUID());
						}
							
						break;
					}
				}
			}
		}

		if (efVO == null) {
			// Possibly an autonumber field (not part of the normal EO fields)
			efVO = SpringApplicationContextHolder.getBean(IMetaProvider.class).getEntityField(field);
			mapFields.put(field, efVO);
		}

		if (efVO == null) {
			throw new IllegalArgumentException("No such field '" + field + 
					"' in entity '" + eMeta.getEntityName() + "', fields are: " + mapFields.keySet());
		}
		
		CollectableEntityField result = new CollectableEOEntityField(efVO);
		result.setCollectableEntity(this);
		return result;
	}

	@Override
	public int getFieldCount() {
		return eMeta.getFields().size();
	}

	@Override
	public Set<UID> getFieldUIDs() {
		return mapFields.keySet();
	}

	@Override
	public String getLabel() {
		return SpringLocaleDelegate.getInstance().getLabelFromMetaDataVO(eMeta);
	}

	/**
	 * @deprecated use {@link #getMeta()}.getEntity().
	 */
	@Override
	public UID getUID() {
		return eMeta.getUID();
	}

	public EntityMeta<?> getMeta() {
		return eMeta;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append(getClass().getName()).append("[");
		result.append("meta=").append(eMeta);
		result.append(", fields=").append(eMeta.getFields());
		result.append("]");
		return result.toString();
	}

}
