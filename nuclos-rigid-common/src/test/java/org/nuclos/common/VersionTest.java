package org.nuclos.common;

import java.util.Date;

import org.junit.Test;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 */
public class VersionTest {
	@Test
	public void testVersionNumger() {
		Version v = new Version("nuclos", "Nuclos ERP", "4.11.0-SNAPSHOT", new Date().toString());
		assert v.isSnapshot();

		String versionString = v.getVersionNumber();
		assert "4.11.0-SNAPSHOT".equals(versionString);

		v = new Version("nuclos", "Nuclos ERP", "4.11.0", new Date().toString());
		assert !v.isSnapshot();

		versionString = v.getVersionNumber();

		assert "4.11.0".equals(versionString);
	}
}