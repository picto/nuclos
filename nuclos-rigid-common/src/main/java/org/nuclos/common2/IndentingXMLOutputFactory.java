package org.nuclos.common2;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamWriter;

import javanet.staxutils.IndentingXMLEventWriter;
import javanet.staxutils.IndentingXMLStreamWriter;
import javanet.staxutils.helpers.FilterXMLOutputFactory;

/**
 * An modified XMLOutputFactory that writes out pritty-printed (i.e. indented) 
 * XML with the help of stax-utils.
 * 
 * @author Thomas Pasch
 */
public class IndentingXMLOutputFactory extends FilterXMLOutputFactory {

	@Override
	protected XMLEventWriter filter(XMLEventWriter writer) {
		return new IndentingXMLEventWriter(writer);
	}

	@Override
	protected XMLStreamWriter filter(XMLStreamWriter writer) {
		return new IndentingXMLStreamWriter(writer);
	}

}
