//Copyright (C) 2010  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

/**
 * Provides system parameters.
 * <br>
 * <br>Created by Novabit Informationssysteme GmbH
 * <br>Please visit <a href="http://www.novabit.de">www.novabit.de</a>
 *
 * @author	<a href="mailto:Christoph.Radig@novabit.de">Christoph.Radig</a>
 * @version 01.00.00
 */
public interface ParameterProvider {

	String KEY_ATTRIBUTE_AS_HEADLINE_IN_LOGBOOK = "Attribute as headline in logbook";

	String KEY_SERVER_VALIDATES_ATTRIBUTEVALUES = "Server validates attribute values";
	String KEY_CLIENT_VALIDATES_ATTRIBUTEVALUES = "Client validates attribute values";
	
	/**
	 * @deprecated Multinuclet: MasterDataVO has no method validate() any more.
	 */
	String KEY_SERVER_VALIDATES_MASTERDATAVALUES = "Server validates masterdata values";
	
	String KEY_CLIENT_VALIDATES_MASTERDATAVALUES = "Client validates masterdata values";
	String KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TREE = "Max row count for search result in tree";
	String KEY_MAX_ROWCOUNT_FOR_SEARCHRESULT_IN_TASKLIST = "Max row count for search result in tasklist";

	String KEY_EXCEL_SHEET_NAME = "Excel Sheet Name 4pm Report";
	String KEY_DEFAULT_GENERATION_COUNT_ATTRIBUTE = "Default generation count attribute";

	String KEY_REPORT_MAXROWCOUNT = "Report Max Row Count";

	String KEY_FOCUSSED_ITEM_BACKGROUND_COLOR = "Focussed item background color";
	String KEY_MANDATORY_ITEM_BACKGROUND_COLOR = "Mandatory item background color";
	String KEY_MANDATORY_ADDED_ITEM_BACKGROUND_COLOR = "Mandatory added item background color";

	String KEY_NOTIFICATION_SENDER = "Notification Sender";
	String KEY_HISTORICAL_STATE_CHANGED_COLOR = "Historical state changed color";
	String KEY_HISTORICAL_STATE_NOT_TRACKED_COLOR = "Historical state not tracked color";
	String KEY_ADDITIONAL_IMPORTS_FOR_RULES = "Additional Imports for Rules";
	String KEY_USE_LDAP = "USE_LDAP";
	String USE_LDAP_SERVER_NAME = "USE_LDAP_SERVER_NAME";
	String KEY_LDAP_P_URL = "LDAP_P_URL";
	String KEY_LDAP_P_USERNAME = "USE_LDAP_SERVER_USERNAME";
	String KEY_LDAP_P_PASSWORD = "USE_LDAP_SERVER_PASSWORD";
	String KEY_LDAP_P_SEARCH_CONTEXT = "LDAP_P_SEARCH_CONTEXT";
	String KEY_LDAP_P_SEARCH_FILTER = "LDAP_P_SEARCH_FILTER";
	String KEY_LDAP_P_SEARCH_SCOPE = "LDAP_P_SEARCH_SCOPE";

	String KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_UPDATE = "Generic Object Import Excluded Attributes For Update";
	String KEY_GENERICOBJECT_IMPORT_EXCLUDED_ATTRIBUTES_FOR_CREATE = "Generic Object Import Excluded Attributes For Create";

	String KEY_GENERICOBJECT_IMPORT_PATH = "Generic Object Import Path";
	String KEY_GENERICOBJECT_ARCHIVE_PATH = "Generic Object Archive Path";

	String KEY_CLIENT_VALIDATION_LAYER_PAINTER_NAME = "Painter name for client validation layer";
	String KEY_BLUR_FILTER = "Blur Filter";

	String KEY_WIKI_BASE_URL = "Wiki base url";
	String KEY_WIKI_INVALID_CHARACTERS = "Wiki invalid Characters";
	String KEY_WIKI_MAPPING_ENABLED = "Wiki Mapping";
	String KEY_WIKI_DEFAULT_PAGE = "Wiki defaultpage";
	String KEY_WIKI_STARTPAGE = "Wiki startpage";

	String KEY_DEFAULT_LOCALE = "Default Locale";
	String KEY_DEFAULT_ENCODING = "Default Encoding";
	String SORT_CALCULATED_ATTRIBUTES = "SORT_CALCULATED_ATTRIBUTES";
	String DATA_CHUNK_SIZE = "DATA_CHUNK_SIZE";
	String ESSENTIAL_ENTITY_FIELDS = "ESSENTIAL_ENTITY_FIELDS";
	String QUICKSEARCH_DELAY_TIME = "QUICKSEARCH_DELAY_TIME";
	String CACHE_VLP_FOR_SEARCH_CB = "CACHE_VLP_FOR_SEARCH_CB";
	String NUMBER_MAX_SORT_ATTRIBUTES = "NUMBER_MAX_SORT_ATTRIBUTES";
	String REST_MENU_ENTITIES = "REST_MENU_ENTITIES";
	String KEY_FILECHOOSER_DONT_REMEMBER_PATH = "FILECHOOSER_DONT_REMEMBER_PATH";
	String QUERY_TIMEOUT = "QUERY_TIMEOUT";
	String USE_PK_SUBSELECT = "USE_PK_SUBSELECT";
	String USE_REF_SUBSELECT = "USE_REF_SUBSELECT";
	String COUNT_RESULT_LIST = "COUNT_RESULT_LIST";
	String KEY_ESTIMATE_COUNT = "ESTIMATE_COUNT";
	
	/**
	 * Should the client local user cache (aka file &lt;home&gt;/nuclos.caches) be disabled?
	 * <p>
	 * If unset or empty, the client local user cache <em>is</em> used.
	 * If "true", then the client local user cache is <em>not</em> used.
	 * <p>
	 * This property only effects the <b>writing</b> to the cache on client termination.
	 * Hence, even when this property is set to "true" the cache is read if it exists!
	 * 
	 * @author Thomas Pasch
	 * @since Nuclos 4.0.10
	 */
	String KEY_DISABLE_CLIENT_LOCAL_USER_CACHE = "client.local.user.cache.disable";

	/**
	 * JARs to add to the JasperReports compile classpath.
	 * JARs are selected by (fully qualified) classnames, separated by spaces.
	 * <p>
	 * This is needed for JasperReports scriptlets.
	 * </p><p>
	 * Default (and example): 
	 * 'net.sf.jasperreports.engine.JasperReport org.nuclos.server.report.api.JRNuclosDataSource'
	 * </p>
	 * @author Thomas Pasch
	 * @since Nuclos 3.9
	 */
	String JASPER_REPORTS_COMPILE_CPJAR_BYCLASSES 
		= "nuclos.jasper.reports.compile.classpath.jars.byclasses";
	
	/**
	 * Default value of {@link #JASPER_REPORTS_COMPILE_CPJAR_BYCLASSES} is no value is given.
	 * @since Nuclos 3.2.9
	 */
	String JASPER_REPORTS_DEFAULT_COMPILE_CPJAR_BYCLASSES_VALUE 
		= "net.sf.jasperreports.engine.JasperReport org.nuclos.server.report.api.JRNuclosDataSource";
	
	/**
	 * @deprecated Get rid of this!
	 */
	String JMS_MESSAGE_ALL_PARAMETERS_ARE_REVALIDATED = "All parameters are revalidated.";
	
	String	KEY_TEMPLATE_USER	= "PREFERENCES_TEMPLATE_USER";

	String KEY_DATASOURCE_TABLE_FILTER = "Datasource Table Filter";

	String KEY_SMTP_AUTHENTICATION = "SMTP Authentication";
	String KEY_SMTP_USERNAME = "SMTP Username";
	String KEY_SMTP_PASSWORD = "SMTP Password";
	String KEY_SMTP_SENDER = "SMTP Sender";
	String KEY_SMTP_SERVER = "SMTP Server";
	String KEY_SMTP_PORT = "SMTP Port";
	String KEY_SMTP_SSL = "SMTP SSL";

	String KEY_POP3_USERNAME = "POP3 Username";
	String KEY_POP3_PASSWORD = "POP3 Password";
	String KEY_POP3_SERVER = "POP3 Server";
	String KEY_POP3_PORT = "POP3 Port";
	
	String KEY_IMAP_PROTOCOL = "IMAP Protocol";
	String KEY_IMAP_USERNAME = "IMAP Username";
	String KEY_IMAP_PASSWORD = "IMAP Password";
	String KEY_IMAP_SERVER = "IMAP Server";
	String KEY_IMAP_PORT = "IMAP Port";
	String KEY_IMAP_FOLDER_FROM = "IMAP Folder From";
	String KEY_IMAP_FOLDER_TO = "IMAP Folder To";
	
	String KEY_MAILGET_SSL = "MAILGET SSL";
	
	String KEY_WSDL_RULECOMPILE_PATH = "WSDL_RULECOMPILE_PATH";
	String KEY_WSDL_GENERATOR_COMPILED_CLASSES_PATH = "WSDL_GENERATOR_COMPILED_CLASSES_PATH";
	String KEY_WSDL_IMPORT_PATH = "WSDL_IMPORT_PATH";

	String KEY_CIPHER = "server.cryptfield.cipher";

	String KEY_RESPLAN_RESOURCE_LIMIT = "ResPlan Resource Limit";

	String KEY_THUMBAIL_SIZE = "THUMBNAIL_SIZE";

	String KEY_TIMELIMIT_RULE_USER = "Timelimit Rule User";

	String KEY_DRAG_CURSOR_HOLDING_TIME = "DRAG_CURSOR_HOLDING_TIME";

	String KEY_SECURITY_LOCK_DAYS = "SECURITY_LOCK_USER_PERIOD";
	String KEY_SECURITY_LOCK_ATTEMPTS = "SECURITY_LOCK_ATTEMPTS";
	String KEY_SECURITY_PASSWORD_INTERVAL = "SECURITY_PASSWORD_INTERVAL";
	String KEY_SECURITY_PASSWORD_HISTORY_NUMBER = "SECURITY_PASSWORD_HISTORY_NUMBER";
	String KEY_SECURITY_PASSWORD_HISTORY_DAYS = "SECURITY_PASSWORD_HISTORY_DAYS";
	String KEY_SECURITY_PASSWORD_STRENGTH_LENGTH = "SECURITY_PASSWORD_STRENGTH_LENGTH";
	String KEY_SECURITY_PASSWORD_STRENGTH_REGEXP = "SECURITY_PASSWORD_STRENGTH_REGEXP";
	String KEY_SECURITY_PASSWORD_STRENGTH_DESCRIPTION = "SECURITY_PASSWORD_STRENGTH_DESCRIPTION";

	String KEY_SHOW_INTERNAL_TIMESTAMP_WITH_TIME = "SHOW_INTERNAL_TIMESTAMP_WITH_TIME";
	
	String KEY_DEFAULT_NUCLOS_THEME = "DEFAULT_NUCLOS_THEME";
	String KEY_NUCLOS_FONT_FAMILY = "NUCLOS_FONT_FAMILY";
	String KEY_ONLINE_HELP = "NUCLOS_ONLINE_HELP";
	String KEY_NUCLOS_INSTANCE_NAME = "NUCLOS_INSTANCE_NAME";
	String KEY_LAYOUT_CUSTOM_KEY = "LAYOUT_CUSTOM_KEY";

	String KEY_JOBRUNMESSAGES_LIMIT = "JobRun Messages Limit";
	
	String KEY_ACTIVATE_LOGBOOK  = "ACTIVATE_LOGBOOK";
	
	String KEY_ROOTPANE_BACKGROUND_COLOR = "ROOTPANE_BACKGROUND_COLOR";
	
	String KEY_VLP_LOADINGTHREAD_KEEP_ALIVE = "VLP_LOADING_THREAD_KEEP_ALIVE";
	String KEY_VLP_RESULTCACHE_EXPIRATION = "VLP_RESULT_CACHE_EXPIRATION";

	String KEY_CLIENT_READ_TIMEOUT = "CLIENT_READ_TIMEOUT";
	
	String KEY_SUPPRESS_USER_NOTIFICATION_EMAIL = "SUPPRESS_USER_NOTIFICATION_EMAIL";
	
	/**
	 * System default is true.
	 * @since Nuclos 4.6.0
	 */
	String KEY_DEFAULT_SUBFORM_MULTIEDITING = "DEFAULT_SUBFORM_MULTIEDITING";
	
	/**
	 * System default is true.
	 * @since Nuclos 4.9.0
	 */
	String KEY_DEFAULT_FIELD_MULTIEDITING = "DEFAULT_FIELD_MULTIEDITING";
	
	/**
	 * System default is "512m".
	 */
	String KEY_CLIENT_MAX_HEAP_SIZE = "CLIENT_MAX_HEAP_SIZE";
	
	/**
	 * Activate cross origin resource sharing for the REST service. Default: false
	 */
	String KEY_REST_ACTIVATE_CORS = "REST_ACTIVATE_CORS";

	/**
	 * Activate User Access
	 */
	public static final String KEY_ANONYMOUS_USER_ACCESS_ENABLED = "ANONYMOUS_USER_ACCESS_ENABLED";
	
	public static final String KEY_WEBCLIENT_CSS = "WEBCLIENT_CSS";
	
	/**
	 * Activate self registration and define role name for new users
	 */
	public static final String KEY_ROLE_FOR_SELF_REGISTERED_USERS = "ROLE_FOR_SELF_REGISTERED_USERS";
	
	/**
	 * Activate self registration and define role name for new users
	 */
	public static final String KEY_ACTIVATION_EMAIL_SUBJECT = "ACTIVATION_EMAIL_SUBJECT";

	/**
	 * Activate self registration and define role name for new users
	 */
	public static final String KEY_ACTIVATION_EMAIL_MESSAGE = "ACTIVATION_EMAIL_MESSAGE";

	/**
	 * Remember username for self registration users
	 */
	public static final String KEY_USERNAME_EMAIL_SUBJECT = "USERNAME_EMAIL_SUBJECT";

	/**
	 * Remember username for self registration users
	 */
	public static final String KEY_USERNAME_EMAIL_MESSAGE = "USERNAME_EMAIL_MESSAGE";

	/**
	 * Reset password for self registration users
	 */
	public static final String KEY_RESET_PW_EMAIL_SUBJECT = "RESET_PW_EMAIL_SUBJECT";

	/**
	 * Reset password for self registration users
	 */
	public static final String KEY_RESET_PW_EMAIL_MESSAGE = "RESET_PW_EMAIL_MESSAGE";
	
	public static final String EMAIL_SIGNATURE = "EMAIL_SIGNATURE";
	
	public static final String INITIAL_ENTRY = "INITIAL_ENTRY";
	
	public static final String LOCALE_OPTIONS = "LOCALE_OPTIONS";

	/**
	 * http://support.nuclos.de/browse/NUCLOS-6200
	 */
	public static final String DEPRECATED_2017_ALLOWED = "DEPRECATED_2017_ALLOWED";

	/**
	 * @param sParameterName
	 * @return the value for the parameter with the given name, if any.
	 */
	String getValue(String sParameterName);

	/**
	 * Gets an int value.
	 * 
	 * @param sParameterName name of parameter
	 * @param iDefaultValue default value for parameter if not set
	 * @return the int value from the parameters or the default value.
	 */
	int getIntValue(String sParameterName, int iDefaultValue);
	
	/**
	 * 
	 * @param sParameterName
	 * @return the boolean value from the parameters or the default [false] if parameter not set
	 */
	boolean isEnabled(String sParameterName);

	/**
	 *
	 * @param sParameterName
	 * @param bDefaultValue default value for parameter if not set
	 * @return the boolean value from the parameters or the default if parameter not set
	 */
	boolean isEnabled(String sParameterName, boolean bDefaultValue);
	
	/**
	 * 
	 * @param sParameterName
	 * @return
	 */
	String[] getList(String sParameterName);
	
	String getJRClassPath();

}	// interface ParameterProvider
