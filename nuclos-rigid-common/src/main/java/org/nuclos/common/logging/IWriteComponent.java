package org.nuclos.common.logging;

public interface IWriteComponent {
	
	void write(String s);

}
