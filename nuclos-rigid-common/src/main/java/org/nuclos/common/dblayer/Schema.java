//Copyright (C) 2013  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common.dblayer;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Nuclos database schema information
 *  
 * @author Maik Stueker
 * @since Nuclos 4.0.00
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Schema {

	public static final String AUTO_VERSION = "AUTO";

	/**
	 * 
	 * @return the database schema version
	 * default is "AUTO"
	 */
	String version() default AUTO_VERSION;

	/**
	 *
	 * @return the counter part for the complete schema version. Only used if version returns "AUTO" (default)
	 * default is "0000"
	 */
	String changeCount() default "0000";
	
	/**
	 * 
	 * @return the database helper version
	 */
	SchemaHelperVersion helper();
	
}
