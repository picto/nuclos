package org.nuclos.common;

/**
 * @author Andreas Lämmlein <andreas.laemmlein@nuclos.de>
 * @see <a href="http://support.nuclos.de/browse/NUCLOS-2790">NUCLOS-2790</a>
 */
public interface INuclosEntityImport {
	Class<?> getImportClass();
}
