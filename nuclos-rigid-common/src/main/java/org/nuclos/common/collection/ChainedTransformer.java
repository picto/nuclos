package org.nuclos.common.collection;

public class ChainedTransformer<I, M, O> implements Transformer<I, O> {

	private final Transformer<I, ? extends M> transformer1;
	private final Transformer<M, O> transformer2;

	ChainedTransformer(Transformer<I, ? extends M> transformer1, Transformer<M, O> transformer2) {
		this.transformer1 = transformer1;
		this.transformer2 = transformer2;
	}

	@Override
	public O transform(I i) {
		return transformer2.transform(transformer1.transform(i));
	}
	
	public static <I,M,O> ChainedTransformer<I, M, O> chained(Transformer<I, ? extends M> transformer1, Transformer<M, O> transformer2) {
		return new ChainedTransformer<I, M, O>(transformer1, transformer2);
	}

}
