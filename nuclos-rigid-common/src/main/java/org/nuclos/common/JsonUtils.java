//Copyright (C) 2015  Novabit Informationssysteme GmbH
//
//This file is part of Nuclos.
//
//Nuclos is free software: you can redistribute it and/or modify
//it under the terms of the GNU Affero General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Nuclos is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU Affero General Public License for more details.
//
//You should have received a copy of the GNU Affero General Public License
//along with Nuclos.  If not, see <http://www.gnu.org/licenses/>.
package org.nuclos.common;

import java.io.StringReader;
import java.io.StringWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;

public class JsonUtils {

	/**
	 * 
	 * @param jsonObject
	 * @return stringified json object
	 */
	public static String objectToString(JsonObject jsonObject) {
		JsonWriter jsonWriter = null;
		try {
			StringWriter writer = new StringWriter();
			jsonWriter = Json.createWriter(writer);
			jsonWriter.writeObject(jsonObject);
			return writer.toString();
		} finally {
			if (jsonWriter != null) {
				jsonWriter.close();
			}
		}
	}
	
	/**
	 * 
	 * @param jsonObjectString (stringified json object)
	 * @return a JsonObject
	 */
	public static JsonObject stringToObject(String jsonObjectString) {
		JsonReader jsonReader = null;
		try {
			jsonReader = Json.createReader(new StringReader(jsonObjectString));
			return jsonReader.readObject();
		} finally {
			if (jsonReader != null) {
				jsonReader.close();
			}
		}
	}
	
	/**
	 * 
	 * @param jsonObject
	 * @return pretty printed jsonObject
	 */
	public static String prettyPrint(JsonObject jsonObject) {
		String jsonObjectString = objectToString(jsonObject);
		return prettyPrint(jsonObjectString);
	}
	
	/**
	 * 
	 * @param jsonString
	 * @return pretty printed jsonString
	 */
	public static String prettyPrint(String jsonString) {
		String result = com.cedarsoftware.util.io.JsonWriter.formatJson(jsonString);
		return result;
	}
}
